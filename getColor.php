<?  // Получение URL-ов фоток
list($r, $g, $b) = sscanf('#'.$_GET['color_code'], "#%02x%02x%02x");

header('Content-Type: image/png');

// Создание картинки-холста $dest
$dest = imagecreate(20, 20);

// Окрашивание холста в цвет
$bgc = imagecolorallocate($dest, $r, $g, $b);

// Вывод изображения
imagepng($dest);

// Освобождение памяти
imagedestroy($dest); ?>