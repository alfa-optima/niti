<?php

namespace Migrate;

use RedBeanPHP\Facade as R;
use System\All;

class MigrationOldProject
{

    public static $translitProductCache;
    public static $start = 0;
    public $orders;
    public $models_all_id;
    private $newStatuses = [
        8 => [1, 7],
        1 => [2, 7],
        4 => [1, 7],
        11 => [6, 10],
        7 => [2, 9],
        14 => [2, 9],
        9 => [3, 7],
        5 => [5, 7],
        //9 => [5,7,1],
        //7 => [2,9],
        10 => [2, 8],
        2 => [2, 9],
        12 => [2, 9],
        13 => [1, 7],
        6 => [5, 7]
    ];

    public function getOrdersIdErrorProduct()
    {

        $list_orders = R::getAll("SELECT * FROM `orders` ORDER BY `orders`.`id` DESC LIMIT 0, 10000");
        //var_dump($list_orders);

        $error_orders = [];

        foreach ($list_orders as $order) {

            $products = R::getAll("SELECT `product_id` FROM `order_products` WHERE `order_id` = :order_id", ['order_id' => $order['id']]);
            //var_dump($products);

            if(empty($products)){
                $error_orders[] = $order['id'];
            }

/*
            foreach ($products as $product) {

                //var_dump($product['product_id']);

                $status_count = R::getCol("SELECT COUNT(`model_id`) as `count` FROM `catalog_product` WHERE `product_id` = :product_id", [
                    'product_id' => $product['product_id']
                ])[0];

                if ($status_count == '0') {
                    $error_orders[] = $order['id'];
                }
                //var_dump($status_count);

            }

            */


        }

        //var_dump($error_orders);

        echo 'count:  ' . count($error_orders);

        file_put_contents('s.txt', '');

        foreach ($error_orders as $error_order) {
            $all = $error_order;
            $fp = fopen("s.txt", "a");
            fputs($fp, $all . "\r\n");
        }

    }


    public function getOrders()
    {

        $opts = array('http' =>
            array(
                'method' => 'GET',
                'timeout' => 1200000
            )
        );

        $context = stream_context_create($opts);
        $result = file_get_contents('http://niti-niti.ru/admin2/results.json', false, $context);
        $this->orders = json_decode($result, true);

    }

    public function getOrdersNow()
    {

        $opts = array('http' =>
            array(
                'method' => 'GET',
                'timeout' => 1200000
            )
        );

        $context = stream_context_create($opts);
        $result = file_get_contents('http://niti-niti.ru/admin2/migration_orders_get.php?start=' . self::$start, false, $context);
        //$result = file_get_contents('http://niti-niti.ru/admin2/migration_orders_get__custom.php', false, $context);
        $this->orders = json_decode($result, true);

    }

    public function createOrder()
    {

        if (empty($this->orders)) return true;


        //        R::exec('DELETE FROM `orders` WHERE `id` > ' . self::$start);
//        R::exec('DELETE FROM `order_status_value` WHERE `order_id` > ' . self::$start);
//        R::exec('DELETE FROM `orders_managers` WHERE `id_order` > ' . self::$start);
//        R::exec('DELETE FROM `orders_payment` WHERE `order_id` > ' . self::$start);
//        R::exec('DELETE FROM `order_products` WHERE `order_id` > ' . self::$start);
//        R::exec('DELETE FROM `comments` WHERE `order_id` > ' . self::$start);
//
///
//        R::exec('DELETE FROM `orders` WHERE `id` = 71878 ');
//        R::exec('DELETE FROM `order_status_value` WHERE `order_id` = 71878');
//        R::exec('DELETE FROM `orders_managers` WHERE `id_order` = 71878 ');
//        R::exec('DELETE FROM `orders_payment` WHERE `order_id` = 71878 ');
//        R::exec('DELETE FROM `order_products` WHERE `order_id` = 71878 ');
//        R::exec('DELETE FROM `comments` WHERE `order_id` = 71878 ');



        //exit;

//        R::exec('TRUNCATE `orders`');
//        R::exec('TRUNCATE `order_status_value`');
//        R::exec('TRUNCATE `orders_managers`');
//        R::exec('TRUNCATE `orders_payment`');
//        R::exec('TRUNCATE `order_products`');
//        R::exec('TRUNCATE `comments`');
        $iii = 1;

        foreach ($this->orders as $order) {

            //echo $iii . ' -- ' . $order['main']['id'] . PHP_EOL;

            //echo $iii . ' -- ' . $order['main']['roistat'] . PHP_EOL;

            if($order['main']['fio'] == 'SYSTEM'){
                echo 'STOP' . PHP_EOL;
                continue;
            }


            $check = R::getRow('SELECT Count(id) as count FROM `orders` WHERE `id` = ' . $order['main']['id'])['count'];
            if($check == 1){
                continue;
            }

            echo $iii . ' --! ' . $order['main']['id'] . PHP_EOL;
            echo PHP_EOL;

            //continue;

            $iii++;

//            R::exec('DELETE FROM `orders` WHERE `id` = ' . $order['main']['id']);
//            R::exec('DELETE FROM `order_status_value` WHERE `order_id` = ' . $order['main']['id']);
//            R::exec('DELETE FROM `orders_managers` WHERE `id_order` = ' . $order['main']['id']);
//            R::exec('DELETE FROM `orders_payment` WHERE `order_id` = ' . $order['main']['id']);
//            R::exec('DELETE FROM `order_products` WHERE `order_id` = ' . $order['main']['id']);
//            R::exec('DELETE FROM `comments` WHERE `order_id` = ' . $order['main']['id']);
//
            //continue;

            //var_dump($order);

            $date = date("Y-m-d H:i:s", strtotime($order['main']['date'] . ' ' . $order['main']['time']));

            $order['main']['sell'] = intval($order['main']['sell']);

            if ($order['main']['sell'] > 100 || $order['main']['sell'] < 0) $order['main']['sell'] = 0;

            //R::fancyDebug();
            R::exec('INSERT INTO `orders` (
          `id`,
          `time`,
          `fio`,
          `email`,
          `number`,
          `address`,
          `amount`,
          `comment`,
          `show`,
          `id_stores`,
          `discount_percent`,
  		  `roistat`,
  		  `carrotquest_uid`)
  		VALUES (
  		  :id,
  		  :time,
  		  :fio, 
  		  :email,
  		  :number,
  		  :address,
  		  :amount,
  		  :comment,
  		  :show,
  		  :id_stores,
  		  :discount_percent,
  		  :roistat,
  		  :carrotquest_uid)', [
                ':id' => $order['main']['id'],
                ':time' => $date,
                ':fio' => $order['main']['fio'],
                ':email' => $order['main']['email'],
                ':number' => $order['main']['number'],
                ':address' => $order['main']['addres'],
                ':amount' => $order['main']['summa'],
                ':comment' => $order['main']['comment'],
                ':show' => $order['main']['show'],
                ':id_stores' => $order['main']['id_stores'],
                ':discount_percent' => $order['main']['sell'],
                ':roistat' => $order['main']['roistat'],
                ':carrotquest_uid' => $order['main']['carrotquest_uid']
            ]);

            $orderNew_id = R::getInsertID();

            /*
             * Status
             */
            $status_calculate = $this->getNewStatuses($order['main']['status']);
            if ($status_calculate != null) {
                R::exec('INSERT INTO `order_status_value` (order_id, main, delivery, payment)
	  				VALUES (:order_id, :main, :delivery, :payment)', [
                    ':order_id' => $orderNew_id,
                    ':main' => $status_calculate[0],
                    ':delivery' => $status_calculate[1],
                    ':payment' => 12

                ]);
            }

            /*
             * Manager
             */
            if (!empty($order['managers'])) {
                R::exec('INSERT INTO `orders_managers` (`id_order`, `id_managers`) VALUES (:id_order, :id_managers)', [
                    ':id_order' => $orderNew_id,
                    ':id_managers' => $order['managers']
                ]);
            }

            /*
             * Оплата
             */

            if (!empty($order['payment'])) {


                foreach ($order['payment'] as $item) {

                    R::exec("INSERT INTO `orders_payment` (
                `id`,
                `order_id`,
                `time`,
                `amount`,
                `type`,
                `json`)

                VALUES (

                NULL,
                :order_id,
                :time,
                :amount,
                '1',
                :json)", [
                        ':order_id' => $orderNew_id,
                        ':time' => date("Y-m-d H:i:s", strtotime($item['paymentDatetime'])),
                        ':amount' => $item['orderSumAmount'],
                        ':json' => json_encode($item)
                    ]);

                }


            }


            /*
             * Comments
             */
            if (!empty($order['commnets'])) {

                foreach ($order['commnets'] as $item) {

                    R::exec("INSERT INTO `comments` (
                        `id`,
                        `parent_id`,
                        `comment`,
                        `time`,
                        `user_id`,
                        `order_id`,
                        `task_id`) VALUES (
                        NULL,
                        '0',
                        :text,
                        :time,
                        '0',
                        :id_order,
                        NULL)", [
                        ':id_order' => $orderNew_id,
                        ':text' => $item['text'],
                        ':time' => date("Y-m-d H:i:s", strtotime($item['date'] . ' ' . $item['time']))
                    ]);

                }

            }

            /*
             * Products
             */
            if (!empty($order['products'])) {

                foreach ($order['products'] as $item) {

                    $product_id = self::translitProduct($item['model_id'], $item['color'], $item['size']);

                    if ($product_id !== false) {

                        //All::printr($product_id, 0, $order['main']['id']);

                        for ($i = 0; $i < $item['count']; $i++) {

                            R::exec('INSERT INTO `order_products` (`id`, `order_id`, `product_id`, `price`, `json`) VALUES (NULL, :order_id, :product_id, :price, NULL)', [
                                ':order_id' => $order['main']['id'],
                                ':product_id' => $product_id,
                                ':price' => $item['price'],
                            ]);


                        }


                    }

                }

            }

        }

    }

    public function createOrderCustom()
    {

        if (empty($this->orders)) return true;

        $iii = 1;

        foreach ($this->orders as $order) {

            echo $iii . ' -- ' . $order['main']['id'] . PHP_EOL;

            //var_dump($order['products']);

            //return;
            $iii++;

//            R::exec('DELETE FROM `orders` WHERE `id` = ' . $order['main']['id']);
//            R::exec('DELETE FROM `order_status_value` WHERE `order_id` = ' . $order['main']['id']);
//            R::exec('DELETE FROM `orders_managers` WHERE `id_order` = ' . $order['main']['id']);
//            R::exec('DELETE FROM `orders_payment` WHERE `order_id` = ' . $order['main']['id']);
            //R::exec('DELETE FROM `order_products` WHERE `order_id` = ' . $order['main']['id']);
//            R::exec('DELETE FROM `comments` WHERE `order_id` = ' . $order['main']['id']);
//
            //continue;

            $date = date("Y-m-d H:i:s", strtotime($order['main']['date'] . ' ' . $order['main']['time']));

            $order['main']['sell'] = intval($order['main']['sell']);

            if ($order['main']['sell'] > 100 || $order['main']['sell'] < 0) $order['main']['sell'] = 0;

            /*
             * Products
             */
            if (!empty($order['products'])) {

                foreach ($order['products'] as $item) {

                    $product_id = self::translitProduct($item['model_id'], $item['color'], $item['size']);

                    if ($product_id !== false) {

                        //All::printr($product_id, 0, $order['main']['id']);

                        for ($i = 0; $i < $item['count']; $i++) {

                            R::exec('INSERT INTO `order_products` (`id`, `order_id`, `product_id`, `price`, `json`) VALUES (NULL, :order_id, :product_id, :price, NULL)', [
                                ':order_id' => $order['main']['id'],
                                ':product_id' => $product_id,
                                ':price' => $item['price'],
                            ]);


                        }


                    }

                }

            }

        }

    }


    private function getNewStatuses($old_status)
    {
        return isset($this->newStatuses[$old_status]) ? $this->newStatuses[$old_status] : null;
    }

    public static function translitProduct($model_id, $color, $size)
    {

        if (empty(self::$translitProductCache[$model_id . '|' . $color . '|' . $size])):

            self::$translitProductCache[$model_id . '|' . $color . '|' . $size] = false;

            $sql_result = R::getAll("SELECT * FROM `catalog_product` a LEFT JOIN `catalog_param_element` b USING(product_id)
        LEFT JOIN `catalog_param_select` c ON c.param_id = b.param_id and c.value = b.value
        WHERE a.model_id = :model_id AND ( c.name IN (:color, :size) || c.name IS NULL)", [
                ':model_id' => $model_id,
                ':size' => $size,
                ':color' => $color
            ]);

            if (!empty($sql_result)):

                if (count($sql_result) > 1):
                    foreach ($sql_result as $item) {

                        $products_temp[$item['product_id']][] = $item;

                        if (count($products_temp[$item['product_id']]) > 1) {
                            self::$translitProductCache[$model_id . '|' . $color . '|' . $size] = $item['product_id'];
                            break;
                        }

                    }
                else :
                    self::$translitProductCache[$model_id . '|' . $color . '|' . $size] = $sql_result[0]['product_id'];

                endif;


            endif;

        endif;


        return self::$translitProductCache[$model_id . '|' . $color . '|' . $size];


        /*
        SELECT * FROM `catalog_product` a LEFT JOIN `catalog_param_element` b USING(product_id)
        LEFT JOIN `catalog_param_select` c ON c.param_id = b.param_id and c.value = b.value
        WHERE a.model_id = 7639 AND (c.name IN ('Голубой', 'L') || c.name IS NULL)

         */

    }

    public function getPaymentNow()
    {

        $opts = array('http' =>
            array(
                'method' => 'GET',
                'timeout' => 1200000
            )
        );

        $context = stream_context_create($opts);
        $result = file_get_contents('http://niti-niti.ru/admin2/migration_orders_payment_get.php', false, $context);
        return json_decode($result, true);

    }

    public function removeModelNoneCompliance(){


        $opts = array('http' =>
            array(
                'method' => 'GET',
                'timeout' => 1200000
            )
        );

        $context = stream_context_create($opts);
        $result = file_get_contents('http://niti-niti.ru/admin2/get_id_model.php', false, $context);
        $old_s = json_decode($result, true);

//        foreach ($old_s as $model) {
//
//            $old_model[$model] = $model;
//
//        }
        //var_dump($old_model);


        $this->models_all_id = $result_sql = R::getCol('SELECT model_id FROM `catalog_model`');
        //var_dump($result_sql);

        $diff = array_diff($result_sql, $old_s);
        //All::printr($diff);
        $result = [];
        foreach ($diff as $model) {
            $result[$model] = $model;
        }

        return $result;

//        foreach ($result_sql as $key => $model) {
//
//            if(!empty($old_model[$model])){
//
//            }
//
//            if(empty($result_sql)){
//                $compliance[] = $model;
//            }
//
//            //return;
//
//        }



    }

    public function checkComplianceModel()
    {

        $opts = array('http' =>
            array(
                'method' => 'GET',
                'timeout' => 1200000
            )
        );

        $context = stream_context_create($opts);
        $result = file_get_contents('http://niti-niti.ru/admin2/get_id_model.php', false, $context);
        $old_s = json_decode($result, true);

        $compliance = [];

        foreach ($old_s as $model) {

            $result_sql = R::getCell('SELECT model_id FROM `catalog_model` WHERE `model_id` = ' . $model . ' LIMIT 1');
            //All::printr($result_sql);
            if(empty($result_sql)){
                $compliance[] = $model;
            }

            //return;

        }


        $ids = join("','",$compliance);
        $sql = "UPDATE `products` SET  `update_last` =  '2017-12-15 16:10:25' WHERE id_product IN ('$ids')";

 // WHERE  `products`.`id_product` =11561;

        return $sql;
        //return $compliance;


    }


}
