<? require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

header('Access-Control-Allow-Origin: *');
echo project\search_products::request($_GET);
