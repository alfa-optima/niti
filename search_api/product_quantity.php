<? require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if( $_GET['method'] == 'get' ){
	echo project\offerQuantity::get($_GET);
} else if( $_GET['method'] == 'set'  ){
	echo project\offerQuantity::set($_GET);
}