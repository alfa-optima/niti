<h3>Быстро доставим любой ваш заказ по всей России</h3>

<p>
	Доставка заказов по Российской Федерации осуществляется Почтой России наложенным платежом. Сумма отправки заказов оплачивается отдельно при получении на Почте России она зависит от габаритов посылки и ее веса.
</p>

<p>
	При отправке Почтой России в заказе необходимо указать Фамилию Имя Отчество полностью, а также индекс Вашего населенного пункта.
</p>