<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$search_array = array();

if( strlen(param_get('term')) > 3 ){

	\Bitrix\Main\Loader::includeModule("search");
	$obSearch = new \CSearch;
	$obSearch->Search(array("QUERY" => param_get('term'), "SITE_ID" => LANG, array( array( "=MODULE_ID" => "iblock", "!ITEM_ID" => "S%", "PARAM2" => project\site::CATALOG_IBLOCK_ID))), array("TITLE_RANK"=>"DESC"), array('STEMMING' => true));
	if ($obSearch->errorno != 0){} else {
		while($item = $obSearch->GetNext()){
			// Получаем закешированное инфо о товаре
			$el = tools\el::info($item['ITEM_ID']);
			if (
				intval($el['ID']) > 0
				&&
				$el['ACTIVE'] == 'Y'
			){
				$search_array[] = array(
					'label' => $el['NAME'],
					'value' => $el['DETAIL_PAGE_URL']
				);
			}
		}
	}
	
	if( count($search_array) == 0 ){
		$obSearch = new \CSearch;
		$obSearch->Search(array("QUERY" => param_get('term'), "SITE_ID" => LANG, array( array( "=MODULE_ID" => "iblock", "!ITEM_ID" => "S%", "PARAM2" => project\site::CATALOG_IBLOCK_ID))), array("TITLE_RANK"=>"DESC"), array('STEMMING' => false));
		if ($obSearch->errorno != 0){} else {
			while($item = $obSearch->GetNext()){
				// Получаем закешированное инфо о товаре
				$el = tools\el::info($item['ITEM_ID']);
				if (
					intval($el['ID']) > 0
					&&
					$el['ACTIVE'] == 'Y'
				){
					$search_array[] = array(
						'label' => $el['NAME'],
						'value' => $el['DETAIL_PAGE_URL']
					);
				}
			}
		}
	}

}

echo json_encode($search_array);


require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php"); ?>