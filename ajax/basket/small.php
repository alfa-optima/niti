<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET,POST,PUT,DELETE,OPTIONS');
header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');


\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;
\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$bInfo = project\user_basket::info();

$basket = [
    'items' => [],
    'info' => [
        'sum' => $bInfo['basket_sum'],
        'sum_format' => $bInfo['basket_sum_format'],
        'disc_sum' => $bInfo['basket_disc_sum'],
        'disc_sum_format' => $bInfo['basket_disc_sum_format'],
        'discount' => $bInfo['basket_discount'],
        'discount_format' => $bInfo['basket_discount_format'],
        'quantity' => $bInfo['basket_cnt'],
    ]
];

foreach ( $bInfo['arBasket'] as $key => $bItem ){

    if( intval($bItem->el['PROPERTY_CML2_LINK_VALUE']) > 0 ){
        $bItem->product = tools\el::info($bItem->el['PROPERTY_CML2_LINK_VALUE']);
    } else {
        $bItem->product = $bItem->el;
    }

    $item = [
        'ID' => $bInfo['userBasket'][$key]->getId(),
        'PRODUCT' => [
            'ID' => $bItem->el['ID'],
            'NAME' => html_entity_decode($bItem->product['NAME']),
            'URL' => $bItem->product['DETAIL_PAGE_URL'],
            'PICTURE_ID' => intval($bItem->el['PROPERTY_PHOTO_ID_VALUE'])>0?$bItem->el['PROPERTY_PHOTO_ID_VALUE']:$bItem->el['DETAIL_PICTURE'],
        ],
        'QUANTITY' => $bItem->getQuantity(),
        'PRICE' => $bItem->discPrice,
        'PRICE_FORMAT' => $bItem->discPriceFormat,
    ];

    if ( intval($bItem->el['PROPERTY_COLOR_VALUE']) > 0 ){
        $color = tools\el::info($bItem->el['PROPERTY_COLOR_VALUE']);
        if( intval($color['ID']) > 0 ){
            $item['PRODUCT']['COLOR'] = $color;
        }
    }

    if ( intval($bItem->el['PROPERTY_SIZE_VALUE']) > 0 ){
        $size = tools\el::info($bItem->el['PROPERTY_SIZE_VALUE']);
        if( intval($size['ID']) > 0 ){
            $item['PRODUCT']['SIZE'] = $size;
        }
    }

    $basket['items'][] = $item;
}

//echo count($basket['items']);
//exit;


if(count($basket['items'])) {

}

/*
echo '
{"items":[{"ID":0,"PRODUCT":{"ID":"243914","NAME":"\u0413\u0435\u0440\u0430\u043a\u043b","URL":"\/catalog\/detail.php?ID=243914","COLOR":{"ID":"241590","ACTIVE":"Y","NAME":"\u0421\u0435\u0440\u044b\u0439-\u043c\u0435\u043b\u0430\u043d\u0436","~NAME":"\u0421\u0435\u0440\u044b\u0439-\u043c\u0435\u043b\u0430\u043d\u0436","CODE":"seryy_melanzh","XML_ID":"241590","DETAIL_PAGE_URL":"\/sprav\/detail.php?ID=241590","IBLOCK_EXTERNAL_ID":"","DATE_CREATE":"13.02.2018 19:31:04","IBLOCK_ID":"6","PREVIEW_PICTURE":"556","DETAIL_PICTURE":null,"PREVIEW_TEXT":"","~PREVIEW_TEXT":"","PROPERTY_COLOR_CODE_VALUE":"#b3b3b3","PROPERTY_COLOR_CODE_VALUE_ID":"241590:8","LANG_DIR":"\/","SORT":"500","EXTERNAL_ID":"241590","IBLOCK_SECTION_ID":null,"~IBLOCK_SECTION_ID":null,"IBLOCK_TYPE_ID":"sprav","IBLOCK_CODE":"colors","LID":"s1","PREVIEW_TEXT_TYPE":"text"}},"QUANTITY":1,"PRICE":1521,"PRICE_FORMAT":"1 521,00"},{"ID":0,"PRODUCT":{"ID":"243927","NAME":"\u0413\u0435\u0440\u0430\u043a\u043b","URL":"\/catalog\/detail.php?ID=243927","COLOR":{"ID":"241591","ACTIVE":"Y","NAME":"\u0421\u0435\u0440\u044b\u0439 \u0441 \u0447\u0435\u0440\u043d\u044b\u043c\u0438 \u0440\u0443\u043a\u0430\u0432\u0430\u043c\u0438","~NAME":"\u0421\u0435\u0440\u044b\u0439 \u0441 \u0447\u0435\u0440\u043d\u044b\u043c\u0438 \u0440\u0443\u043a\u0430\u0432\u0430\u043c\u0438","CODE":"seryy_s_chernymi_rukavami","XML_ID":"241591","DETAIL_PAGE_URL":"\/sprav\/detail.php?ID=241591","IBLOCK_EXTERNAL_ID":"","DATE_CREATE":"13.02.2018 19:31:04","IBLOCK_ID":"6","PREVIEW_PICTURE":"555","DETAIL_PICTURE":null,"PREVIEW_TEXT":"","~PREVIEW_TEXT":"","PROPERTY_COLOR_CODE_VALUE":"#b8b8b8","PROPERTY_COLOR_CODE_VALUE_ID":"241591:8","LANG_DIR":"\/","SORT":"500","EXTERNAL_ID":"241591","IBLOCK_SECTION_ID":null,"~IBLOCK_SECTION_ID":null,"IBLOCK_TYPE_ID":"sprav","IBLOCK_CODE":"colors","LID":"s1","PREVIEW_TEXT_TYPE":"text"}},"QUANTITY":1,"PRICE":1521,"PRICE_FORMAT":"1 521,00"}],"info":{"sum":3380,"sum_format":"3 380,00","disc_sum":3042,"disc_sum_format":"3 042,00","discount":338,"discount_format":"338,00","quantity":2}}';

exit;
*/

// Ответ
echo json_encode($basket);
return;
