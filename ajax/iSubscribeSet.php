<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$arResult = array('status' => 'ok', 'active_ills' => array());

if( 
	$USER->IsAuthorized()
	&&
	count( param_post('ills') ) > 0
){
	
	$ills = array_unique(param_post('ills'));

	foreach ($ills as $id){
		if( project\i_subscribe::getEl($id, $USER->GetID()) ){
			$arResult['active_ills'][] = $id;
		}
	}
	
}


echo json_encode($arResult);




require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");