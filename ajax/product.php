<? require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); 

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools; ?>

<div class="modal quick_block_<?=strip_tags(param_get('id'))?>" id="product_modal">

	<? if ( intval( param_get('id') ) > 0 ){
		
		// Инфо о товаре
		$el = tools\el::info(param_get('id')); 
		
		if ( intval( $el['ID'] ) > 0 ){
			
			$picture = false;
			$price = false;
			$sku_id = false;
		
			// Инфо по ТП
			$offersInfo = getTovOffers($el['ID']);
			
			$first_offer_key = false;
			if( count($offersInfo['all_sku']) > 0 ){
				$keys = array_keys($offersInfo['all_sku']);
				$first_offer_key = $keys[0];
			}
			if(	
				$first_offer_key
				&&
				file_exists($_SERVER['DOCUMENT_ROOT'].$offersInfo['all_sku'][$first_offer_key]['PROPERTY_PHTO_VALUE']) 
			){
				$picture = $offersInfo['all_sku'][$first_offer_key]['PROPERTY_PHTO_VALUE'];
			}
			
			if( count($offersInfo['color_list']) > 0 ){

				$keys = array_keys($offersInfo['color_list']);
				$default_color_key = $keys[0];

				$default_color_id = $offersInfo['color_list'][$default_color_key]['color']['ID'];
				
				$price = $offersInfo['color_min_prices'][$offersInfo['color_list'][$default_color_key]['color']['ID']];

				$defaultPriceIsEq = isEqualPrices($offersInfo['color_prices'], $default_color_id);

			} else {
				
				if ( count($offersInfo['sizes']) > 0 ){

					$prices = array();
					foreach ($offersInfo['sizes'] as $size => $sizeInfo){
						if( !$price ){   $price = $offersInfo['sizes'][$size]['PRICE'];   }
						$prices[] = $sizeInfo['PRICE'];
					}
					$prices = $offersInfo['prices'];
					$min_price = min($prices); 
					$max_price = max($prices);
					
					$defaultPriceIsEq = $max_price==$min_price?1:0;
				
				} else {
					
					if( count($offersInfo['all_sku']) > 0 ){
						$cnt = 0;
						foreach ( $offersInfo['all_sku'] as $offer ){ $cnt++;
							$prices[] = $offer['DISC_PRICE'];
							if( $cnt == 1 ){     $sku_id = $offer['ID'];     }
						}
						$price = min($prices); 
						$max_price = max($prices); 
						
						$defaultPriceIsEq = $max_price==$price?1:0;

					}
					
				}
				
			} ?>
			

			<div class="product_info">
			
				<div class="images left">
				
					<div class="big right modal_photo___block" style="display:none">
					
						<? if( $picture ){ ?>
						
							<div class="slider owl-carousel">
								<div class="slide">
									<a href="<?=rIMG($picture, 1, 800, 700)?>" class="fancy_img" data-fancybox="gallery">
										<img src="<?=rIMG($picture, 5, 428, 490)?>">
									</a>
								</div>
							</div>
							
						<? } ?>
						
						<? global $APPLICATION;
						$color_id = $APPLICATION->get_cookie('color_id'); ?>
						
						<script type="text/javascript">
						setTimeout(function(){
							<? if( intval($color_id) > 0 ){ ?>
								if( $('.modal_color___label[color_id=<?=$color_id?>]').length > 0 ){
									$('.modal_color___label[color_id=<?=$color_id?>]').trigger('click');
								} else {
									$('.modal_color___label').eq(0).trigger('click');
								}
							<? } else { ?>
								$('.modal_color___label').eq(0).trigger('click');
							<? } ?>	
							$('.modal_photo___block').show();
						}, 400)
						</script>
						
					</div>
					
					<div class="clear"></div>
				</div>


				<div class="data right">
				
					<div class="prod_name"><?=$el['~NAME']?></div>

					<? if( strlen($el['PROPERTY_CML2_ARTICLE_VALUE']) > 0 ){ ?>
						<div class="articul">Артикул: <span><?=$el['PROPERTY_CML2_ARTICLE_VALUE']?></span></div>
					<? } ?>

					<? if( $price ){ ?>
						<div class="price modal_price___block">
							<div class="new">
								<? if ( $defaultPriceIsEq == 0 ){ echo 'от '; }?>
								<?=number_format($price, 0, ",", " ")?> <span class="currency">:</span>
							</div>
								
							<div class="old modal_old___price_block" style="display:none"></span></div>
							<div class="percents modal_discount___block" style="display:none"></div>
							
						</div>
					<? } ?>

					<? if(
						is_array($offersInfo['color_list'])
						&&
						count($offersInfo['color_list']) > 0
					){ ?>
					
						<div class="color">
							<div class="title">Цвет</div>

							<div class="grid">
							
								<? // Вывод всех цветов
								$k = 0;
								foreach ( $offersInfo['color_list'] as $key => $arColor ){ $k++; 
								
									$isEqPrices = isEqualPrices($offersInfo['color_prices'], $arColor['color']['ID']); ?>
								
									<input type="radio" name="modal_color" id="modal_prod_color<?=$k?>">
									<label for="modal_prod_color<?=$k?>" class="modal_color___label" equal_prices="<?=$isEqPrices?>" min_price="<?=$offersInfo['color_min_prices'][$arColor['color']['ID']]?>" color_id="<?=$arColor['color']['ID']?>" photo="<?=rIMG($arColor['PROPERTY_PHTO_VALUE'], 5, 428, 490)?>" photo_big="<?=rIMG($arColor['PROPERTY_PHTO_VALUE'], 1, 800, 700)?>">
										<? if( intval($arColor['color']['PREVIEW_PICTURE']) > 0 ){ ?>
											<img src="<?=\CFile::GetPath($arColor['color']['PREVIEW_PICTURE'])?>">
										<? } else { ?>
											<img src="/getColor.php?color_code=<?=str_replace("#", "", $arColor['color']['PROPERTY_COLOR_VALUE']?$arColor['color']['PROPERTY_COLOR_VALUE']:$arColor['color']['PROPERTY_COLOR_CODE_VALUE'])?>">
										<? } ?>
									</label>

								<? } ?>
							
							</div>
						</div>
							
						<? if(
							is_array($offersInfo['color_sizes'])
							&&
							count($offersInfo['color_sizes']) > 0
						){ ?>
						
							<div class="size">

								<div class="title">Размер</div>

								<div class="modalErrorBoxYellow-empty">
									<p class="modal___error_p"></p>
									<? // Выводим блоки с размерами
									$k = 0;
									foreach ( $offersInfo['color_sizes'] as $color_id => $sizes ){ ?>
									
										<div class="grid modal_colorSizesBlock" color_id="<?=$color_id?>" style="display:<? if( $color_id == $default_color_id ){ ?>flex<? } else { ?>none<? } ?>">
										
											<? foreach ( $sizes as $size ){ $k++; ?>
										
												<input type="radio" name="modal_size" id="modal_prod_size<?=$k?>" value="<?=$offersInfo['color_prices'][$color_id][$size]['ID']?>">
												<label for="modal_prod_size<?=$k?>" class="modal_size___label" price="<?=$offersInfo['color_prices'][$color_id][$size]['PRICE']?>" old_price="<?=$offersInfo['color_prices'][$color_id][$size]['OLD_PRICE']?>"><?=$size?></label>

											<? } ?>
											
										</div>
										
									<? } ?>
								</div>

							</div>
							
						<? } ?>
						
					<? } else {
					
						if(
							is_array($offersInfo['sizes'])
							&&
							count($offersInfo['sizes']) > 0
						){ ?>
							<div class="size">
							
								<div class="title">Размер</div>

								<div class="grid colorSizesBlock" color_id="">
								
									<? $k = 0;
									foreach ( $offersInfo['sizes'] as $size => $sizeInfo ){ $k++; ?>
								
										<input type="radio" name="modal_size" id="prod_size<?=$k?>" value="<?=$sizeInfo['ID']?>">
										
										<label for="prod_size<?=$k?>" class="size___label" price="<?=$sizeInfo['PRICE']?>" old_price="<?=$sizeInfo['OLD_PRICE']?>"><?=$size?></label>

									<? } ?>
									
								</div>
								
							</div>
						<? }
					
					} ?>

					<div class="buy left">
					
						<? if( $price ){ ?>
							<a tov_name="<?=$el['NAME']?>" <? if( $sku_id ){ ?>sku_id="<?=$sku_id?>"<? } ?> style="cursor:pointer" class="buy_link modal_add_to_basket to___process">
								<img src="<?=SITE_TEMPLATE_PATH?>/images/ic_buy_link.png" alt="">в корзину
							</a>
						<? } ?>

						<a href="#buy_1_click_modal" class="modal_link quike_buy_link" tov_id="<?=$el['ID']?>">купить в 1 клик</a>

					</div>

					<a style="cursor:pointer" class="favorite_link left fav___link to___process" tov_id="<?=$el['ID']?>">
						<div class="hover add_hover">Добавить в избранное</div>
						<div class="hover del_hover">Убрать из избранного</div>
					</a>

					<a href="<?=$el['DETAIL_PAGE_URL']?>" class="details left">подробнее</a>
					<div class="clear"></div>
				</div>
				<div class="clear"></div>
			</div>


			<? // Получим ID раздела, в котором находится товар
			$el_sections = tools\el::sections($el['ID']);
			$section_id = $el_sections[0]['ID'];
		
			// Получим все товары данного раздела
			$section_elements = section_elements($section_id);
			// Определим пред. и след. товары
			$prev_id = false;    $next_id = false;
			foreach ( $section_elements as $key => $id ){
				if ( $id == param_get('id') ){
					if ( $section_elements[$key-1] ){
						$prev_id = $section_elements[$key-1];
					}
					if ( $section_elements[$key+1] ){
						$next_id = $section_elements[$key+1];
					}
				}
			}
			
			if( $prev_id || $next_id ){ ?>
			
				<div class="prev_next">
				
					<? if( $prev_id ){
						
						// Инфо по элементу
						$prev_el = tools\el::info($prev_id); ?>
						
						<a onclick="$(this).parents('.quick_block_<?=strip_tags(param_get('id'))?>').find('.fancybox-close-small').trigger('click');" href="javascript:;" class="prev left quike_view_link" data-fancybox data-src="/ajax/product.php?id=<?=$prev_id?>" data-type="ajax"><?=$prev_el['NAME']?></a>
						
					<? }
					
					if( $next_id ){
						
						// Инфо по элементу
						$next_el = tools\el::info($next_id); ?>
						
						<a onclick="$(this).parents('.quick_block_<?=strip_tags(param_get('id'))?>').find('.fancybox-close-small').trigger('click');" href="javascript:;" class="next right quike_view_link" data-fancybox data-src="/ajax/product.php?id=<?=$next_id?>" data-type="ajax"><?=$next_el['NAME']?></a>
						
					<? } ?>
					
				</div>
				
			<? } ?>

			<script>
			
				setFavorites();
			
				// Карточка товара
				$prouctCarousel = $('#product_modal .images .big .slider').owlCarousel({
					loop: false,
					nav: false,
					navSpeed: 500,
					smartSpeed: 500,
					autoplaySpeed: 500,
					items: 1,
					dots: false,
					margin: 30,
					onTranslate: function(event){
						$('#product_modal .images .thumbs a').removeClass('active')
						$('#product_modal .images .thumbs a:eq('+ event.item.index +')').addClass('active')
					}
				})

				/*$('#product_modal .images .thumbs a').click(function(e) {
					e.preventDefault()

					$('#product_modal .images .thumbs a').removeClass('active')
					$(this).addClass('active')

					$prouctCarousel.trigger('to.owl.carousel', $(this).attr('data-slide-index'))
				})*/
				
				
				// Смена цвета в (модальной) карточке товара
				$(document).on('click', '.modal_color___label', function(){
					var attr_for = $(this).attr('for');
					if ( !$('input[id='+attr_for+']').prop('checked') ){
						$.noty.closeAll();
						var photo = $(this).attr('photo');
						var photo_big = $(this).attr('photo_big');
						var color_id = $(this).attr('color_id');
						var min_price = $(this).attr('min_price');
						var equalPrices = $(this).attr('equal_prices');
						$('.modal_colorSizesBlock').hide();
						$('.modal_colorSizesBlock[color_id='+color_id+']').show();
						$('input[name=modal_size]').prop('checked', false);
						$('.modal_price___block div.new').html( ((equalPrices==0)?'от ':'') + number_format(min_price, 0, ",", " ") + ' <span class="currency">:</span>' );
						$('.modal_old___price_block').hide();
						$('.modal_discount___block').hide();
						if( photo.length > 0 ){
							$prouctCarousel.trigger('replace.owl.carousel', '<div class="slider owl-carousel"><div class="slide"><a href="'+photo_big+'" class="fancy_img" data-fancybox="gallery"><img src="'+photo+'"></a></div></div>');
							$('.fancy_img').fancybox({
								transitionEffect : 'slide',
								animationEffect : 'fade',
								i18n : {
									'en' : {
										CLOSE : 'Закрыть'
									}
								}
							})
						}
					}
				})


				$('.ajax_modal').click(function(e){
					e.preventDefault()

					$.fancybox.close()

					$.fancybox.open({
						src  : $(this).attr('href'),
						type : 'ajax',
						opts : {
							speed: 300,
							autoFocus : false,
							i18n : {
								'en' : {
									CLOSE : 'Закрыть'
								}
							}
						}
					})
				})
			</script>
			
			<input type="hidden" name="tovar_id" value="<?=$el['ID']?>">
		
		<? }
	} ?>
	
</div>



<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php"); ?>