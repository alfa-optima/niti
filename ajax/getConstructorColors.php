<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$colors = array();   $html = '';


$filter = Array(
	"IBLOCK_ID" => project\site::CONSTR_SKU_IBLOCK_ID,
	"ACTIVE" => "Y",
	"PROPERTY_CML2_LINK" => param_post('odezhda')
);
$fields = Array( "ID", "PROPERTY_CML2_LINK", "PROPERTY_COLOR_ID" );
$dbElements = \CIBlockElement::GetList(
	array("SORT"=>"ASC"), $filter, false, false, $fields
);
while ($element = $dbElements->GetNext()){
	$colors[] = $element['PROPERTY_COLOR_ID_VALUE'];
}


$selected_colors = array();
if( param_post('selected_colors') ){
	$selected_colors = explode('|', param_post('selected_colors'));
}


// Цвета
foreach ($colors as $color_id){ 
	$color = tools\el::info($color_id); 
	if( intval($color['ID']) > 0 ){
		$checked = '';
		if( in_array($color['ID'], $selected_colors) ){    $checked = 'checked';    }
		$html .= '<div class="item_wrap">';
			$html .= '<input type="checkbox" name="color" '.$checked.' id="color_'.$color['ID'].'" value="'.$color['ID'].'">';
			$html .= '<label for="color_'.$color['ID'].'" value="'.$color['ID'].'" title="'.$color['NAME'].'">';
			if( intval($color['PREVIEW_PICTURE']) > 0 ){
				$html .= '<img src="'.\CFile::GetPath($color['PREVIEW_PICTURE']).'">';
			} else {
				$html .= '<img src="/getColor.php?color_code='.str_replace("#", "", $color['PROPERTY_COLOR_VALUE']?$color['PROPERTY_COLOR_VALUE']:$color['PROPERTY_COLOR_CODE_VALUE']).'">';
			}
			$html .= '</label>';
		$html .= '</div>';
	}
}


// Ответ
echo json_encode( Array(
	"status" => "ok",
	"html" => $html
) );


require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");