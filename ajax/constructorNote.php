<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET,POST,PUT,DELETE,OPTIONS');
header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;


if (empty($_POST)) {
    $_POST = json_decode(file_get_contents('php://input'), true);
}


$result = array('status' => 'error');

if (
    project\constructor::checkUser()
    &&
    count($_POST) > 0
) {

    $note_id = strip_tags($_POST['id']);
    $params = strip_tags($_POST['params']);
    $page = strip_tags($_POST['page']);
    $limit = strip_tags($_POST['limit']);
	$page_el_cnt = strip_tags($_POST['page_el_cnt']);


    // Создание заявки
    if ($_POST['action'] == 'add') {



        $id = project\constructor::addNote($params);
        if (intval($id) > 0) {
            $result = array('status' => 'ok', 'id' => $id);
        }


        // Редактирование заявки
    } else if ($_POST['action'] == 'update') {


        $res = project\constructor::updateNote($note_id, $params);
        if ($res) {
            $result = array('status' => 'ok');
        }


        // Инфо о заявке
    } else if ($_POST['action'] == 'get') {


        $note = project\constructor::getNote($note_id);
        if ($note){
            $result = array(
                'status' => 'ok',
                'info' => $note
            );
        }


        // Список заявок пользователя
    } else if ($_POST['action'] == 'list') {


        $listRes = project\constructor::getNotesList(false, $page, $limit, $page_el_cnt);
        if ($listRes) {
            $result = array(
                'status' => 'ok',
                'list' => $listRes['list'],
				'pages' => $listRes['pages']
            );
			if( !empty($_POST['show_all_list']) ){
				 $result['all_list'] = $listRes['all_list'];
			}
        }


        // Удаление заявки
    } else if ($_POST['action'] == 'delete') {


        $res = project\constructor::deleteNote($note_id);
        if ($res) {
            $result = array('status' => 'ok');
        }


    }


}

if ($result['status'] === 'error') {
    //CHTTP::SetStatus("422 Unprocessable Entity");
    //header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
    //header("HTTP/1.1 422 Unprocessable Entity");
}

$result['post'] = $_POST;

//tools\funcs::pre($result);
echo json_encode($result); 

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php"); ?>
