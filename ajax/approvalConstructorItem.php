<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if ( project\constructor::checkUser() ){

    //var_dump($_GET['id']);
    $post['ID'] = $_GET['id'];
    //var_dump($post['ID']);
    //exit();

    if( intval($post['ID']) > 0 ){

        $res = project\constructor::approvalItem(intval($post['ID']));
        echo json_encode($res);

    } else {
        // Ответ
        echo json_encode( array('status' => 'error', 'text' => 'Ошибка данных') );
    }

} else {
	// Ответ
	echo json_encode( array('status' => 'error', 'text' => 'Ошибка авторизации') );
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
