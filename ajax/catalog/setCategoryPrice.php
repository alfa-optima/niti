<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET,POST,PUT,DELETE,OPTIONS');
header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');


\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if ( empty($_POST) ) {
    $_POST = json_decode(file_get_contents('php://input'), true);
}

$result = array('status' => 'error');

if (
    project\constructor::checkUser()
    &&
    count($_POST) > 0
) {

    $category_id = strip_tags($_POST['category_id']);
    $price = strip_tags($_POST['price']);

    if( is_int($category_id) && intval($category_id) > 0 ){
        if( is_float($price) ){

            $res = project\catalog_niti::saveCategoryPrice( intval($category_id), $price );

            if( $res ){
                $result['status'] = 'ok';
            }

        } else {
            $result['text'] = 'Не передана цена (либо передана в неверном формате)';
        }
    } else {
        $result['text'] = 'Не передан ID категории';
    }


}


echo json_encode($result);


require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php"); ?>
