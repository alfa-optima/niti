<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET,POST,PUT,DELETE,OPTIONS');
header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');

\Bitrix\Main\Loader::includeModule('iblock');
\Bitrix\Main\Loader::includeModule('aoptima.tools');
\Bitrix\Main\Loader::includeModule('aoptima.project');

use AOptima\Project as project;
use AOptima\Tools as tools;

$result = array('status' => 'ok');

if (project\constructor::checkUser()) {

    // ЦВЕТА
    $result['COLORS'] = array();

    $filter = Array(
        "IBLOCK_ID" => project\site::COLOR_IBLOCK_ID,
        "ACTIVE" => "Y",
        "PROPERTY_IN_CONSTRUCTOR_VALUE" => 'Y'
    );
    $fields = Array(
        "ID", "NAME", "PREVIEW_PICTURE",
        "PROPERTY_IN_CONSTRUCTOR", "PROPERTY_COLOR_CODE"
    );
    $colors = \CIBlockElement::GetList(
        array("NAME" => "ASC"), $filter, false, false, $fields
    );
    while ($color = $colors->GetNext()) {
        $result['COLORS'][$color['ID']] = array(
            'ID' => $color['ID'],
            'NAME' => $color['NAME'],
            'HEX_CODE' => $color['PROPERTY_COLOR_CODE_VALUE'],
            'PICTURE' => $color['PREVIEW_PICTURE'] ? \CFile::GetPath($color['PREVIEW_PICTURE']) : false,
        );
    }

    // ГРУППЫ ТОВАРОВ
    $result['GROUPS'] = array(
        'LIST' => array(),
        'ITEMS' => array()
    );

    $groups = \CIBlockSection::GetList(
        Array("SORT" => "ASC"),
        Array("ACTIVE" => "Y", "IBLOCK_ID" => project\site::CONSTR_GROUPS_IBLOCK_ID)
    );

    while ($group = $groups->GetNext()) {

        $arGroup = array(
            'ID' => $group['ID'],
            'SORT' => $group['SORT'],
            'NAME' => $group['NAME'],
            'ITEMS' => array()
        );

        // Элементы группы
        $filter = Array(
            "IBLOCK_ID" => project\site::CONSTR_GROUPS_IBLOCK_ID,
            "ACTIVE" => "Y",
            "SECTION_ID" => $group['ID']
        );
        $fields = [
            "ID", "NAME", "PREVIEW_PICTURE", "DETAIL_PICTURE",
            "PROPERTY_X", "PROPERTY_Y", "PROPERTY_W",
            "PROPERTY_H", "PROPERTY_NEED_FINAL_PICTURE"
        ];
        $elements = \CIBlockElement::GetList(
            array("SORT" => "ASC"), $filter, false, false, $fields
        );
        while ($el = $elements->GetNext()) {

            $arElement = array(
                'ID' => $el['ID'],
                'NAME' => $el['NAME'],
                'NEED_FINAL_PICTURE' => $el['PROPERTY_NEED_FINAL_PICTURE_VALUE']?'Y':'N',
                'BASE_PICTURE' => $el['PREVIEW_PICTURE'] ? \CFile::GetPath($el['PREVIEW_PICTURE']) : false,
                'ICON_PICTURE' => $el['DETAIL_PICTURE'] ? \CFile::GetPath($el['DETAIL_PICTURE']) : false,
                'X' => $el['PROPERTY_X_VALUE'],
                'Y' => $el['PROPERTY_Y_VALUE'],
                'W' => $el['PROPERTY_W_VALUE'],
                'H' => $el['PROPERTY_H_VALUE'],
                'COLOR_ITEMS' => array()
            );

            // SKU элемента
            $skuFilter = Array(
                "IBLOCK_ID" => project\site::CONSTR_SKU_IBLOCK_ID,
                "ACTIVE" => "Y",
                "PROPERTY_CML2_LINK" => $el['ID']
            );
            $skuFields = Array(
                "ID", "NAME", "PREVIEW_PICTURE",
                "PROPERTY_CML2_LINK", "PROPERTY_COLOR_ID"
            );
            $skus = \CIBlockElement::GetList(
                array("SORT" => "ASC"), $skuFilter, false, false, $skuFields
            );
            while ($sku = $skus->GetNext()) {

                $color = tools\el::info($sku['PROPERTY_COLOR_ID_VALUE']);

                $arElement['COLOR_ITEMS'][$sku['ID']] = array(
                    'ID' => $sku['ID'],
                    'NAME' => $sku['NAME'],
                    'PICTURE' => $sku['PREVIEW_PICTURE'] ? \CFile::GetPath($sku['PREVIEW_PICTURE']) : false,
                    'COLOR_ID' => $color['ID']
                );
            }

            $arGroup['ITEMS'][] = $el['ID'];
            $result['GROUPS']['ITEMS'][$el['ID']] = $arElement;
        }

        $result['GROUPS']['LIST'][$group['ID']] = $arGroup;
    }

    // КОЛЛЕКЦИИ
    $result['COLLECTIONS'] = array();
    foreach (project\collection::getList() as $collection) {
        $result['COLLECTIONS'][$collection['ID']] = array(
            'ID' => $collection['ID'],
            'NAME' => $collection['NAME'],
            'CODE' => $collection['CODE'],
            'PARENT_ID' => $collection['PROPERTY_PARENT_ID_VALUE'],
            'PICTURE' => $collection['DETAIL_PICTURE'] ? \CFile::GetPath($collection['DETAIL_PICTURE']) : false
        );
    }

} else {
    $result['status'] = 'error';
}

//tools\funcs::pre($result);
echo json_encode($result);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php"); ?>
