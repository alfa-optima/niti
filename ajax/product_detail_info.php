<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

if ( intval(param_post('item_id')) > 0 ){

	$el = tools\el::info(param_post('item_id'));
	  
	if( intval($el['ID']) > 0 ){

		ob_start(); 
			$APPLICATION->IncludeComponent(
				"my:related_right_block", "",
				array('el' => $el)
			);
			$related_right_block = ob_get_contents();
		ob_end_clean();
		
		// Ответ
		echo json_encode(array(
			'status' => 'ok',
			'related_right_block' => $related_right_block
		)); 
		  
	} else {
		// Ответ
		echo json_encode( array('status' => 'error', 'text' => 'Ошибка данных') ); 
	}

} else {
	// Ответ
	echo json_encode( array('status' => 'error', 'text' => 'Ошибка данных') );
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");