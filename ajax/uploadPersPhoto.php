<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if ($USER->IsAuthorized()){

	$persPhoto = new project\pers_photo($_FILES);
	$persPhoto->upload(); 

}

