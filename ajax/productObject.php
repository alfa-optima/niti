<? require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$product_id = strip_tags(trim($_POST['id']));
if( intval($_GET['id']) > 0 ){
    $product_id = strip_tags(trim($_GET['id']));
}

$DATA_OBJ = project\product::getObject($product_id);

echo json_encode($DATA_OBJ, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
