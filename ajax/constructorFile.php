<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET,POST,PUT,DELETE,OPTIONS');
header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');

if( $_POST['isJS'] != 'Y' ){
	$_POST = json_decode(file_get_contents('php://input'), true);
}

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;


$result = array('status' => 'error');


if( 
	project\constructor::checkUser()
	&&
	count($_POST) > 0
){

	$id = strip_tags($_POST['id']);
	$base64 = strip_tags($_POST['base64']);
	
	
	// Создание файла
	if( $_POST['action'] == 'add' ){
		
		
		$result = project\constructor::base64_to_file($base64);
		
		
	// Инфо о файле
	} else if( $_POST['action'] == 'get' ){
		
		
		$res = project\constructor::getFile($id);
		if( $res ){
			$result = $res;
			$result['status'] = 'ok';
		} else {
			$result['text'] = 'Файл не найден';
		}
		
		
	// Удаление файла из системы
	} else if( $_POST['action'] == 'delete' ){
		
		
		\CFile::Delete($file_id);
		$result['status'] = 'ok';
		
		
	} 
	


	

}



/*if( $result['status'] == 'error' ){
	CHTTP::SetStatus("422 Unprocessable Entity");
}*/


$result['post'] = $_POST;


	//tools\funcs::pre($result);
	echo json_encode($result);

	

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php"); ?>