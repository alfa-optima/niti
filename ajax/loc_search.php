<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('sale');
use \Bitrix\Sale;

$arResult = array();

$term = strip_tags(param_get('term'));

$locs = \Bitrix\Sale\Location\LocationTable::getList(array(
	'filter' => array( 
		'NAME.NAME' => $term."%", 
		'=NAME.LANGUAGE_ID' => 'ru',
		"!TYPE.CODE" => array('COUNTRY', 'COUNTRY_DISTRICT', 'REGION', 'SUBREGION', 'STREET'),
		'=PARENT.NAME.LANGUAGE_ID' => 'ru',
	),
	'select' => array(
		'*', 'NAME_RU' => 'NAME.NAME', 'TYPE_CODE' => 'TYPE.CODE',
		'PARENT_NAME_RU' => 'PARENT.NAME.NAME'
	),
	'limit' => 100
));
while( $loc = $locs->fetch() ){
	
	$arResult[$loc['ID']] = array(
		'label' => $loc['NAME_RU'].' ('.$loc['PARENT_NAME_RU'].')',
		"value" => $loc['ID']
	);
	
	$params['filter']['=LOCATION_ID'] = $loc['ID'];
	$params['filter']['=SERVICE.CODE'] = 'ZIP';
	$extRes = \Bitrix\Sale\Location\ExternalTable::getList($params);
	if( $extItem = $extRes->fetch() ){
		$arResult[$loc['ID']]['zip'] = $extItem['XML_ID'];
	}
	
}

echo json_encode($arResult);



require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php"); ?>