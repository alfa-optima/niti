<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET,POST,PUT,DELETE,OPTIONS');
header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;


if (empty($_POST)) {
    $_POST = json_decode(file_get_contents('php://input'), true);
}


$result = array('status' => 'error');

if (
    project\constructor::checkUser()
    &&
    count($_POST) > 0
) {


    // Создание товаров
    if ( $_POST['action'] == 'add' ) {


        $result = project\constructor::executeNote( $_POST['note_id'] );


    }
	
	
}


//tools\funcs::pre($result);
echo json_encode($result); 


require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php"); ?>
