<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main,
    Bitrix\Main\Localization\Loc as Loc,
    Bitrix\Main\Loader,
    Bitrix\Main\Config\Option,
    Bitrix\Main\Application,
    Bitrix\Sale\Delivery,
    Bitrix\Sale\PaySystem,
    Bitrix\Sale,
    Bitrix\Sale\Basket,
    Bitrix\Sale\Order,
    Bitrix\Sale\DiscountCouponsManager,
    Bitrix\Main\Context;

\Bitrix\Main\Loader::includeModule("main");
\Bitrix\Main\Loader::includeModule("iblock");
\Bitrix\Main\Loader::includeModule("catalog");
\Bitrix\Main\Loader::includeModule("sale");

$action = param_post("action");

if (empty($_POST['action'])) {
    $_POST = json_decode(file_get_contents('php://input'), true);
    $action = $_POST['action'];
}

//echo '<pre style="font-size: 17px;background-color: #363636;color: #fff;padding: 20px;line-height: 26px;">';
//print_r($_POST);
//echo '</pre>';
//echo $action;
//exit;

\Bitrix\Main\Loader::includeModule('aoptima.tools');

use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');

use AOptima\Project as project;

if ($action == "change_gender_type") {
    $gender_type = param_post("gender_type");
    if (strlen($gender_type) > 0) {
        foreach (project\gender_type::list() as $type) {
            if ($gender_type == $type['CODE']) {
                $_SESSION['gender_type'] = $type;
                $arResult = array('status' => 'ok');
                echo json_encode($arResult);
            }
        }
    }
}

if ($action == "row_cnt_change") {

    if (strlen(param_post('new_row_cnt')) > 0) {

        $_SESSION['row_cnt'] = param_post('new_row_cnt');
        $arResult = array(
            'status' => 'ok',
            'new_row_cnt' => $_SESSION['row_cnt']
        );
        echo json_encode($arResult);

    }

}

if ($action == "mob_row_cnt_change") {

    if (strlen(param_post('new_row_cnt')) > 0) {

        $_SESSION['mob_row_cnt'] = param_post('new_row_cnt');
        $arResult = array(
            'status' => 'ok',
            'new_row_cnt' => $_SESSION['mob_row_cnt']
        );
        echo json_encode($arResult);

    }

}

// Получение списка ПВЗ
if ($action == "getPVZ") {
    if (strlen(param_post('boxberry_loc_id')) > 0) {
        $boxberry_pvz_list = project\deliv_boxberry::checkPVZ(param_post('boxberry_loc_id'));
        if ($boxberry_pvz_list[0]['err']) {
            $boxberry_pvz_list = array();
        } else if (
            $boxberry_pvz_list
            &&
            is_array($boxberry_pvz_list)
            &&
            count($boxberry_pvz_list) > 0
        ) {
            foreach ($boxberry_pvz_list as $key => $pvz) {
                if (strlen($pvz['GPS']) > 0) {
                } else {
                    unset($boxberry_pvz_list[$key]);
                }
            }
        }
        $arResult = array(
            'status' => 'ok',
            'boxberry_pvz_list' => $boxberry_pvz_list
        );
        echo json_encode($arResult);
    }
}

// Расчёт стоимости доставки в ПВЗ
if ($action == "calcPVZprice") {
    if (intval(param_post('pvz_code')) > 0) {
        $arResult = array("status" => "ok");

        $basketParams = basketParams();

        $result = project\deliv_boxberry::calcPVZPrice(param_post('pvz_code'), $basketParams);
        if ($result['price']) {
            $arResult['price'] = $result['price'];
        }
        if ($result['delivery_period']) {
            $arResult['delivery_period'] = $result['delivery_period'];
        }
        // Ответ
        echo json_encode($arResult);
    } else {
        echo json_encode(array("status" => "error", "text" => "Ошибка данных"));
    }
}

// Запрос блока доставки
if ($action == "getDeliveryVariants") {
    if (intval(param_post('loc_id')) > 0) {

        ob_start();
        $APPLICATION->IncludeComponent(
            "my:order_delivery_block", "",
            array(
                'loc_id' => param_post('loc_id'),
                'zip' => param_post('zip')
            )
        );
        $delivery_block_html = ob_get_contents();
        ob_end_clean();

        // Ответ
        echo json_encode(array(
            "status" => "ok",
            "delivery_block_html" => $delivery_block_html
        ));

    } else {
        echo json_encode(array("status" => "error", "text" => "Ошибка данных"));
    }
}

// Отклонение
if ($action == "cancelConstructorItem") {
    if (
        $USER->IsAuthorized()
        &&
        (
            $USER->IsAdmin()
            ||
            in_array(6, \CUser::GetUserGroup($USER->GetID()))
        )
    ) {
        // Переводим заявку в статус Отклонено
        $set_prop = array("STATUS" => 9);
        \CIBlockElement::SetPropertyValuesEx($item_id, project\site::CONSTR_ITEMS_IBLOCK_ID, $set_prop);
        echo json_encode(array('status' => 'ok'));

        //$res = project\constructor::delItem( param_post('item_id') );
        //echo json_encode($res);
    }
}

// Удаление фото в ЛК
if ($action == "persPhotoDelete") {
    if ($USER->IsAuthorized()) {
        $user_id = $USER->getID();
        $user = new \CUser;
        $fields = array('PERSONAL_PHOTO' => array('del' => 'Y'));
        $res = $user->Update($user_id, $fields);
        if ($res) {
            $html = '<div class="img">
				<a style="cursor:pointer" class="persPhotoSelect to___process">
					<img src="' . SITE_TEMPLATE_PATH . '/images/load_image.jpg">
				</a>
			</div>';
            // Ответ
            echo json_encode(array('status' => 'ok', 'html' => $html));
        } else {
            // Ответ
            echo json_encode(array('status' => 'error', 'error_text' => 'Ошибка сохранения профиля'));
        }
    }
}

// Загрузка фото в ЛК
if ($action == "getPersPhotoBlock") {
    if ($USER->IsAuthorized()) {
        $arUser = tools\user::info($USER->getID());
        ob_start();
        $APPLICATION->IncludeComponent("my:pers_photo_block", "", array('arUser' => $arUser));
        $html = ob_get_contents();
        ob_end_clean();
        // Ответ
        echo json_encode(array('status' => 'ok', 'html' => $html));
    }
}

// Загрузка фото в ЛК
if ($action == "getPrintPhotoBlock") {
    if ($USER->IsAuthorized()) {
        ob_start();
        $APPLICATION->IncludeComponent("my:print_photo_block", "", array('photo_id' => param_post('fid')));
        $html = ob_get_contents();
        ob_end_clean();
        // Ответ
        echo json_encode(array('status' => 'ok', 'html' => $html));
    } else {
        // Ответ
        echo json_encode(array('status' => 'error', 'text' => 'Ошибка авторизации'));
    }
}

// Создание принта
if ($action == "addPrint") {
    if ($USER->IsAuthorized()) {
        $arFields = array();
        parse_str(param_post("form_data"), $arFields);
        $catalogPrint = new project\catalog_print();
        $res = $catalogPrint->add($arFields['print_name'], $arFields['photo_id']);
        if ($res) {
            ob_start();
            $APPLICATION->IncludeComponent("my:print_photo_block", "");
            $html = ob_get_contents();
            ob_end_clean();
            // Ответ
            echo json_encode(array('status' => 'ok', 'html' => $html));
        } else {
            // Ответ
            echo json_encode(array('status' => 'error', 'text' => 'Ошибка создания принта'));
        }
    } else {
        // Ответ
        echo json_encode(array('status' => 'error', 'text' => 'Ошибка авторизации'));
    }
}

// Лайк за иллюстратора
if ($action == "user_like") {
    if (intval(param_post("user_id")) > 0) {
        $user_id = intval(param_post("user_id"));
        if (!in_array($user_id, $_SESSION[project\site::USER_LIKES_SESSION])) {
            $arUser = tools\user::info($el['PROPERTY_USER_ID_VALUE']);
            $likes = intval($arUser['UF_LIKES']);
            $likes++;
            $arFields['UF_LIKES'] = $likes;
            $user = new CUser;
            $res = $user->Update($user_id, $arFields);
            if ($res) {
                $_SESSION[project\site::USER_LIKES_SESSION][] = $user_id;
                $arResult = array("status" => "ok", "likes" => $likes);
            } else {
                $arResult = array("status" => "error");
            }
        } else {
            $arResult = array("status" => "no");
        }
    } else {
        $arResult = array("status" => "error");
    }
    // Ответ
    echo json_encode($arResult);
}

// Обновление правой корзины
if ($action == "update_right_basket") {

    $delivery_price = param_post("delivery_price");

    ob_start();
    // basket_right
    $params = array('delivery_price' => $delivery_price);
    $APPLICATION->IncludeComponent("my:basket_right", "", $params);
    $html = ob_get_contents();
    ob_end_clean();

    // Ответ
    echo json_encode(
        array(
            "status" => "ok",
            'html' => $html
        )
    );

}

// Попытка активации купона
if ($action == "set_coupon") {

    $promo_success = 0;
    $deact = 0;

    $coupon = trim(param_post("coupon"));

    $message = '';

    if (strlen($coupon) > 0) {

        // Успешная проверка купона
        if (project\my_coupon::check($coupon)) {
            project\my_coupon::set($coupon);
            $promo_success = 1;
            $message = 'Промо-код применён!';
        } else {
            project\my_coupon::clear();
            $message = 'Ошибочный промо-код';
        }

    } else {
        project\my_coupon::clear();
        if (project\my_coupon::get_active_coupon()) {
            $deact = 1;
            $message = 'Промо-код деактивирован';
        }
    }

    ob_start();
    $APPLICATION->IncludeComponent("my:basket_items", "", array('new_coupon' => $coupon));
    $basket_itmes_html = ob_get_contents();
    ob_end_clean();

    // Ответ
    echo json_encode(
        array(
            "status" => "ok",
            'basket_itmes_html' => $basket_itmes_html,
            'promo_success' => $promo_success,
            'message' => $message,
            'deact' => $deact
        )
    );

}

// Подгрузка последних поступлений товаров
if ($action == "more_latest_arrival") {

    $section_id = param_post("section_id");

    $stop_ids_str = param_post("stop_ids_str");
    $stop_ids = explode('|', $stop_ids_str);

    ob_start();
    $GLOBALS['section_latest_arrival_Filter']['!ID'] = $stop_ids;
    $GLOBALS['section_latest_arrival_Filter']['PROPERTY_NEW_VALUE'] = 'Да';
    $APPLICATION->IncludeComponent(
        "bitrix:catalog.section", "section_latest_arrival_load",
        array(
            "IBLOCK_TYPE" => "content",
            "IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
            "SECTION_ID" => $section_id,
            "SECTION_USER_FIELDS" => array(),
            "ELEMENT_SORT_FIELD" => "NAME",
            "ELEMENT_SORT_ORDER" => "ASC",
            "ELEMENT_SORT_FIELD2" => "NAME",
            "ELEMENT_SORT_ORDER2" => "ASC",
            "FILTER_NAME" => "section_latest_arrival_Filter",
            "HIDE_NOT_AVAILABLE" => "N",
            "PAGE_ELEMENT_COUNT" => (project\site::LATEST_ARRIVAL_CNT + 1),
            "LINE_ELEMENT_COUNT" => "3",
            "PROPERTY_CODE" => array('NEW', 'HIT', 'SALE'),
            "OFFERS_LIMIT" => "1000",
            "TEMPLATE_THEME" => "",
            "PRODUCT_SUBSCRIPTION" => "N",
            "SHOW_DISCOUNT_PERCENT" => "N",
            "SHOW_OLD_PRICE" => "N",
            "MESS_BTN_BUY" => "Купить",
            "MESS_BTN_ADD_TO_BASKET" => "В корзину",
            "MESS_BTN_SUBSCRIBE" => "Подписаться",
            "MESS_BTN_DETAIL" => "Подробнее",
            "MESS_NOT_AVAILABLE" => "Нет в наличии",
            "SECTION_URL" => "",
            "DETAIL_URL" => "",
            "SECTION_ID_VARIABLE" => "SECTION_ID",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "36000000",
            "CACHE_GROUPS" => "Y",
            "SET_META_KEYWORDS" => "N",
            "META_KEYWORDS" => "",
            "SET_META_DESCRIPTION" => "N",
            "META_DESCRIPTION" => "",
            "BROWSER_TITLE" => "-",
            "ADD_SECTIONS_CHAIN" => "N",
            "DISPLAY_COMPARE" => "N",
            "SET_TITLE" => "N",
            "SET_STATUS_404" => "N",
            "CACHE_FILTER" => "Y",
            "PRICE_CODE" => array('BASE'),
            "USE_PRICE_COUNT" => "N",
            "SHOW_PRICE_COUNT" => "1",
            "PRICE_VAT_INCLUDE" => "Y",
            "CONVERT_CURRENCY" => "N",
            "BASKET_URL" => "/personal/basket.php",
            "ACTION_VARIABLE" => "action",
            "PRODUCT_ID_VARIABLE" => "id",
            "USE_PRODUCT_QUANTITY" => "N",
            "ADD_PROPERTIES_TO_BASKET" => "Y",
            "PRODUCT_PROPS_VARIABLE" => "prop",
            "PARTIAL_PRODUCT_PROPERTIES" => "N",
            "PRODUCT_PROPERTIES" => "",
            "PAGER_TEMPLATE" => "",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "PAGER_TITLE" => "Товары",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "Y",
            "ADD_PICT_PROP" => "-",
            "LABEL_PROP" => "-",
            'INCLUDE_SUBSECTIONS' => "Y",
            'SHOW_ALL_WO_SECTION' => "Y"
        )
    );
    $html = ob_get_contents();
    ob_end_clean();

    // Ответ
    echo json_encode(
        array(
            "status" => "ok",
            'html' => $html
        )
    );

}

// Покупка в 1 клик
if ($action == "buy_1_click") {

    // данные в массив
    $arFields = array();
    parse_str($_POST["form_data"], $arFields);

    $tovar_id = intval($_POST['tovar_id']);
    $tovar = tools\el::info($tovar_id);
    $name = strip_tags($arFields["name"]);
    $email = strip_tags($arFields["email"]);
    $phone = project\site::PHONE_PREFIX . ' ' . strip_tags($arFields["phone"]);
    $quantity = 1;

//    $PROP[20] = $name;
//    $PROP[21] = $email;
//    $PROP[22] = $phone;
//    $PROP[23] = $tovar_id;
//
//    $el = new \CIBlockElement;
//    $fields = array(
//        "IBLOCK_ID" => project\site::BUY_1_CLICK_IBLOCK_ID,
//        "PROPERTY_VALUES" => $PROP,
//        "NAME" => 'Новая заявка',
//        "ACTIVE" => "Y"
//    );
//    if ($ID = $el->Add($fields)) {
//        // отправка инфы на почту
//        $arEventFields = array(
//            "NAME" => $arFields["name"],
//            "EMAIL" => $arFields["email"],
//            "PHONE" => $phone,
//            "TOV_LINK" => '<a href="http://' . $_SERVER['HTTP_HOST'] . $tovar['DETAIL_PAGE_URL'] . '">' . $tovar['NAME'] . '</a>'
//        );
//        \CEvent::SendImmediate("BUY_1_CLICK", "s1", $arEventFields);
//        // Ответ
//        echo json_encode(array("status" => 'ok'));
//    }

	\Bitrix\Main\Loader::includeModule('sale');

	// Инфо по текущей корзине
	$basket_info = project\user_basket::info();
	$cur_basket = [];
	foreach ( $basket_info['basket'] as $key => $basketItem ){
		$cur_basket[$basketItem->getProductId()] = $basketItem->getQuantity();
	}

	// Очистим текущую корзину
	\CSaleBasket::DeleteAll(\CSaleBasket::GetBasketUserID());

	// Добавим товар в корзину
	$basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
	$item = $basket->createItem('catalog', $tovar_id);
	$item->setFields(array(
		'QUANTITY' => $quantity,
		'CURRENCY' => \Bitrix\Currency\CurrencyManager::getBaseCurrency(),
		'LID' => \Bitrix\Main\Context::getCurrent()->getSite(),
		'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider'
	));
	$basket->save();

	// ID пользователя
	$userID = project\site::SERVICE_USER_ID;
	if ($USER->IsAuthorized()){    $userID = $USER->GetID();    }

	$rounding = project\catalog_niti::PRICE_ROUND;

	// Инфо по новой корзине
	$basketInfo = project\user_basket::info();
	$basket_cnt = $basketInfo['basket_cnt'];
	$arBasket = $basketInfo['arBasket'];
	$basket_disc_sum = $basketInfo['basket_disc_sum'];
	$discount = $basketInfo['discount'];
	$pay_sum = $basket_disc_sum;

	// ID варианта оплаты
	$ps_id = 6;
	// ID варианта доставки
	$ds_id = 10;

	// СОЗДАЁМ ЗАКАЗ
	$order = Sale\Order::create('s1', $userID);

	// Передаём корзину в заказ
	$basket = Basket::create('s1');
	foreach( $arBasket as $basketItem ){
		$item = $basket->createItem('catalog', $basketItem->getField('PRODUCT_ID'));
		$el = tools\el::info($basketItem->getField('PRODUCT_ID'));
		$obIblocks = \CIBlock::GetByID($el['IBLOCK_ID']);
		if($iblock = $obIblocks->GetNext()){     $iblock_xml_id = $iblock['XML_ID'];     }
		$item->setFields(array(
			'QUANTITY' => $basketItem->getField('QUANTITY'),
			'CURRENCY' => 'RUB',
			'LID' => 's1',
			'PRODUCT_XML_ID' => $el['XML_ID'],
			'CATALOG_XML_ID' => $iblock_xml_id,
			'PRODUCT_PROVIDER_CLASS' => '\CCatalogProductProvider',
		));
	}
	$order->setBasket($basket);

	// Тип плательщика
	$order->setPersonTypeId(1);

	// Оплата
	$paymentCollection = $order->getPaymentCollection();
	$payment = $paymentCollection->createItem(Sale\PaySystem\Manager::getObjectById( $ps_id ));
	$payment->setField('SUM', $pay_sum); // Сумма к оплате
	$payment->setField('CURRENCY', 'RUB');

	// Доставка
	$shipmentCollection = $order->getShipmentCollection();
	$shipment = $shipmentCollection->createItem(Sale\Delivery\Services\Manager::getObjectById( $ds_id ));
	$shipmentItemCollection = $shipment->getShipmentItemCollection();
	foreach ($basket as $basketItem){
		$item = $shipmentItemCollection->createItem($basketItem);
		$item->setQuantity($basketItem->getQuantity());
	}

	// Заполняем свойства заказа
	$propertyCollection = $order->getPropertyCollection();
	$prop = $propertyCollection->getItemByOrderPropertyId(44);  $prop->setValue('заказ в 1 клик');
	$prop = $propertyCollection->getItemByOrderPropertyId(1);  $prop->setValue($name);
	$prop = $propertyCollection->getItemByOrderPropertyId(4);  $prop->setValue($phone);
	$prop = $propertyCollection->getItemByOrderPropertyId(3);  $prop->setValue($email);

	// Сохраняем новый заказ
	$order->doFinalAction(true);
	$result = $order->save();

	// Определяем ID нового заказа
	$order_id = $order->GetId();

	if (intval($order_id) > 0){

		// Очищаем корзину
		\CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());

		$basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
		foreach ( $cur_basket as $id => $q ){
			$item = $basket->createItem('catalog', $id);
			$item->setFields(array(
				'QUANTITY' => $q,
				'CURRENCY' => \Bitrix\Currency\CurrencyManager::getBaseCurrency(),
				'LID' => \Bitrix\Main\Context::getCurrent()->getSite(),
				'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider'
			));
			$basket->save();
		}

		$mail_html = '<p>Новый заказ в 1 клик на сайте '.\Bitrix\Main\Config\Option::get('main', 'server_name').'</p>';
		$mail_html .= '<p>Имя: '.$name.';</p>';
		$mail_html .= '<p>Телефон: '.$phone.';</p>';
		$mail_html .= '<p>Email: '.$email.';</p>';

		$mail_html .= '<table width="100%" border="1" cellpadding="4" cellspacing="4" style="border-collapse:collapse; font-size: 13px;">';
		$mail_html .= '<tr>
                <td><b>Товар</b></td>
                <td><b>Цена за ед., руб.</b></td>
                <td><b>Количество</b></td>
                <td><b>Сумма, руб.</b></td>
            </tr>';
		foreach ($arBasket as $basketItem){
			// Инфо о товаре
			$product = $basketItem->product;
			$price = $basketItem->price;
			$discPrice = $basketItem->discPrice;
			$quantity = $basketItem->getField('QUANTITY');
			$mail_html .= '<tr>
                    <td>'.$product['NAME'].'</td>
                    <td>'.number_format($discPrice, $rounding, ",", " ").'</td>
                    <td>'.round($quantity, 0).'</td>
                    <td>'.number_format($discPrice * $quantity, $rounding, ",", " ").'</td>
                </tr>';
		}
		$mail_html .= '</table>';

		$disc_sum = number_format($basket_disc_sum, $rounding, ",", " ");
		$delivery_price = number_format(0, $rounding, ",", " ");
		$pay_sum = number_format($pay_sum, $rounding, ",", " ");

		$mail_title = 'Новый заказ в 1 клик (сайт '.\Bitrix\Main\Config\Option::get('main', 'server_name').')';
		$email_to = \Bitrix\Main\Config\Option::get('aoptima.project', 'QUICK_ORDER_EMAIL');
		if( !$email_to ){
			$email_to = \Bitrix\Main\Config\Option::get('main', 'email_from');
		}
		tools\feedback::sendMainEvent( $mail_title, $mail_html, $email_to );

		// Ответ
		echo json_encode([
			"status" => "ok",
			"order_id" => $order_id
		]);
	}


}

// Лайк за коммент
if ($action == "like_comment") {
    $result = project\comment::like($_POST["comment_id"]);
    echo json_encode($result);
}

// Подгрузка комментариев
if ($action == "more_comments") {

    $tov_id = param_post("tov_id");

    $stop_ids_str = param_post("stop_ids_str");
    $stop_ids = explode('|', $stop_ids_str);

    $comment = new project\comment($tov_id);
    $comments = $comment->GetList(min($stop_ids));

    $html = '';

    if (count($comments) > 0) {
        $cnt = 0;
        foreach ($comments as $arItem) {
            $cnt++;

            if ($cnt <= project\comment::CNT) {

                // Инфо по пользователю
                $user_id = $arItem['PROPERTY_USER_ID_VALUE'];
                $q = \CUser::GetByID($user_id);
                $arUser = $q->GetNext();

                // Получим тип пользователя
                $user_type_name = false;
                if ($arUser['UF_USER_TYPE'] != 1) {
                    $enums = \CUserFieldEnum::GetList(array(), array(
                        "ID" => $arUser['UF_USER_TYPE']
                    ));
                    if ($enum = $enums->GetNext()) {
                        $user_type_name = $enum['VALUE'];
                    }
                }

                $html .= '<div class="item comment___item" item_id="' . $arItem['ID'] . '">';

                $html .= '<div class="avatar left">';
                if (intval($arUser['PERSONAL_PHOTO']) > 0) {
                    $html .= '<img src="' . rIMGG($arUser['PERSONAL_PHOTO'], 5, 70, 70) . '">';
                } else {
                    $html .= '<img src="' . SITE_TEMPLATE_PATH . '/images/avatar_img.jpg">';
                }
                $html .= '</div>';

                $html .= '<div class="info right">';
                $html .= '<div class="author left">' . ($arUser['NAME'] ? $arUser['NAME'] : $arUser['LOGIN']) . ' ';
                if ($user_type_name) {
                    $html .= '<span>(' . $user_type_name . ')</span>';
                }
                $html .= '</div>';

                $html .= '<div class="likes left">' . $arItem['PROPERTY_LIKES_VALUE'] . '</div>';

                $html .= '<div class="clear"></div>';

                $html .= '<div class="date">' . ConvertDateTime($arItem['DATE_CREATE'], "DD.MM.YYYY", "ru") . ' в ' . ConvertDateTime($arItem['DATE_CREATE'], "HH:MI", "ru") . '</div>';

                $html .= '<div class="text">' . $arItem['PREVIEW_TEXT'] . '</div>';
                $html .= '</div>';
                $html .= '<div class="clear"></div>';
                $html .= '</div>';

            }
        }
    }

    // Ответ
    echo json_encode(
        array(
            "status" => "ok",
            'html' => $html,
            'more_exist' => ($cnt > project\comment::CNT) ? 1 : 0
        )
    );

}

// Новый комментарий
if ($action == "new_comment") {

    if ($USER->IsAuthorized()) {

        $comment = new project\comment(param_post("tov_id"));
        $res = $comment->add(param_post("message"));
        if ($res) {
            // Ответ
            echo json_encode(array("status" => "ok", 'isAdmin' => $USER->IsAdmin() ? "Y" : "N"));
        } else {
            // Ответ
            echo json_encode(array("status" => "error", 'error_text' => 'Ошибка создания комментария'));
        }

    } else {
        // Ответ
        echo json_encode(array("status" => "error", 'error_text' => 'Необходимо авторизоваться!'));
    }

}

// Работа с избранным
if ($action == "changeFavorites") {

    global $APPLICATION;

    $tov_id = $_POST["tov_id"];
    $act = $_POST["act"];

    $favorites = project\favorites::list();

    $final_favorites = array();
    if (count($favorites) > 0) {
        foreach ($favorites as $id) {
            $el = tools\el::info($id);
            if (
                intval($el['ID']) > 0
                &&
                $id != $tov_id
            ) {
                $final_favorites[] = $id;
            }
        }
    }

    if ($act == 'add') {
        $final_favorites[] = $tov_id;
    }

    $favoritesStr = implode('|', $final_favorites);

    global $APPLICATION;
    $APPLICATION->set_cookie('favorites', $favoritesStr, time() + 2592000);

    // Ответ
    echo json_encode(["status" => "ok", "act" => $act]);

}

// Удаление из Избранного
if ($action == "favorite_remove") {

    $item_id = param_post("item_id");

    $favorites = project\favorites::list();

    $final_favorites = array();
    if (count($favorites) > 0) {
        foreach ($favorites as $id) {
            $el = tools\el::info($id);
            if (
                intval($el['ID']) > 0
                &&
                $id != $item_id
            ) {
                $final_favorites[] = $id;
            }
        }
    }

    if (count($final_favorites) > 0) {

        $favoritesStr = implode('|', $final_favorites);
        global $APPLICATION;
        $APPLICATION->set_cookie('favorites', $favoritesStr, time() + 2592000);

        ob_start();
        $GLOBALS['favorites_Filter']['ID'] = $final_favorites;
        $GLOBALS['favorites_Filter']['!ID'] = $item_id;
        //$GLOBALS['favorites_Filter']['PROPERTY_HAS_SKU'] = 1;
        $APPLICATION->IncludeComponent(
            "bitrix:catalog.section", "favorites",
            array(
                "IBLOCK_TYPE" => "content",
                "IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
                "SECTION_USER_FIELDS" => array(),
                "ELEMENT_SORT_FIELD" => "NAME",
                "ELEMENT_SORT_ORDER" => "ASC",
                "ELEMENT_SORT_FIELD2" => "NAME",
                "ELEMENT_SORT_ORDER2" => "ASC",
                "FILTER_NAME" => "favorites_Filter",
                "HIDE_NOT_AVAILABLE" => "N",
                "PAGE_ELEMENT_COUNT" => "1000",
                "LINE_ELEMENT_COUNT" => "3",
                "PROPERTY_CODE" => array(),
                "OFFERS_CART_PROPERTIES" => array(),
                "OFFERS_FIELD_CODE" => array(
                    0 => 'ID',
                    1 => 'CODE',
                    2 => 'XML_ID',
                    3 => 'NAME',
                    4 => 'TAGS',
                    5 => 'SORT',
                    6 => 'PREVIEW_TEXT',
                    7 => 'PREVIEW_PICTURE',
                    8 => 'DETAIL_TEXT',
                    9 => 'DETAIL_PICTURE',
                    10 => 'DATE_ACTIVE_FROM',
                    11 => 'ACTIVE_FROM',
                    12 => 'DATE_ACTIVE_TO',
                    13 => 'ACTIVE_TO',
                    14 => 'SHOW_COUNTER',
                    15 => 'SHOW_COUNTER_START',
                    16 => 'IBLOCK_TYPE_ID',
                    17 => 'IBLOCK_ID',
                    18 => 'IBLOCK_CODE',
                    19 => 'IBLOCK_NAME',
                    20 => 'IBLOCK_EXTERNAL_ID',
                    21 => 'DATE_CREATE',
                    22 => 'CREATED_BY',
                    23 => 'CREATED_USER_NAME',
                    24 => 'TIMESTAMP_X',
                    25 => 'MODIFIED_BY',
                    26 => 'USER_NAME'
                ),
                "OFFERS_PROPERTY_CODE" => array(
                    0 => 'SIZE',
                    1 => 'COLOR'
                ),
                "OFFERS_SORT_FIELD" => 'sort',
                "OFFERS_SORT_ORDER" => 'asc',
                "OFFERS_SORT_FIELD2" => 'id',
                "OFFERS_SORT_ORDER2" => 'desc',
                "OFFERS_LIMIT" => 1000,
                "TEMPLATE_THEME" => "",
                "PRODUCT_SUBSCRIPTION" => "N",
                "SHOW_DISCOUNT_PERCENT" => "N",
                "SHOW_OLD_PRICE" => "N",
                "MESS_BTN_BUY" => "Купить",
                "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                "MESS_BTN_SUBSCRIBE" => "Подписаться",
                "MESS_BTN_DETAIL" => "Подробнее",
                "MESS_NOT_AVAILABLE" => "Нет в наличии",
                "SECTION_URL" => "",
                "DETAIL_URL" => "",
                "SECTION_ID_VARIABLE" => "SECTION_ID",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000000",
                "CACHE_GROUPS" => "Y",
                "SET_META_KEYWORDS" => "N",
                "META_KEYWORDS" => "",
                "SET_META_DESCRIPTION" => "N",
                "META_DESCRIPTION" => "",
                "BROWSER_TITLE" => "-",
                "ADD_SECTIONS_CHAIN" => "N",
                "DISPLAY_COMPARE" => "N",
                "SET_TITLE" => "N",
                "SET_STATUS_404" => "N",
                "CACHE_FILTER" => "Y",
                "PRICE_CODE" => array('BASE'),
                "USE_PRICE_COUNT" => "N",
                "SHOW_PRICE_COUNT" => "1",
                "PRICE_VAT_INCLUDE" => "Y",
                "CONVERT_CURRENCY" => "N",
                "BASKET_URL" => "/personal/basket.php",
                "ACTION_VARIABLE" => "action",
                "PRODUCT_ID_VARIABLE" => "id",
                "USE_PRODUCT_QUANTITY" => "N",
                "ADD_PROPERTIES_TO_BASKET" => "Y",
                "PRODUCT_PROPS_VARIABLE" => "prop",
                "PARTIAL_PRODUCT_PROPERTIES" => "N",
                "PRODUCT_PROPERTIES" => "",
                "PAGER_TEMPLATE" => "",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "Товары",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "Y",
                "ADD_PICT_PROP" => "-",
                "LABEL_PROP" => "-",
                'INCLUDE_SUBSECTIONS' => "Y",
                'SHOW_ALL_WO_SECTION' => "Y"
            )
        );
        $favorites_html = ob_get_contents();
        ob_end_clean();

    } else {

        global $APPLICATION;
        $APPLICATION->set_cookie('favorites', "", time() - 100);

        $favorites_html = '<p class="no___count">Ваш список избранного пуст</p>';

    }

    // Ответ
    echo json_encode(array(
        "status" => "ok",
        "favorites_html" => $favorites_html
    ));

}

// Редактирование личных данных
if ($action == "personal_edit") {

    // данные в массив
    $arFields = array();
    parse_str(param_post("form_data"), $arFields);
    unset($arFields['ID']);
    unset($arFields['LOGIN']);
    unset($arFields['GROUP_ID']);

    if ($arFields["EMAIL"]) {

        if (!$USER->IsAdmin()) {
            $arFields['LOGIN'] = $arFields['EMAIL'];
        }

        // проверяем EMAIL на логин
        $rsUsers = \CUser::GetByLogin($arFields["EMAIL"]);
        if ($user = $rsUsers->GetNext()) {
            if ($user['ID'] != $USER->GetID()) {
                // Если логин уже есть
                echo json_encode(array("status" => "error", "error_text" => "Данный Email уже зарегистрирован на&nbsp;сайте!"));
                return;
            }
        }

        // проверяем EMAIL на наличие у других пользователей
        $filter = array("=EMAIL" => $arFields["EMAIL"]);
        $rsUsers = \CUser::GetList(($by = "id"), ($order = "desc"), $filter); // выбираем пользователей
        if ($user = $rsUsers->GetNext()) {
            if ($user['ID'] != $USER->GetID()) {
                // Если Email уже есть
                echo json_encode(array("status" => "error", "error_text" => "Данный Email уже зарегистрирован на&nbsp;сайте!"));
                return;
            }
        }

    }

    $user = new \CUser;
    $res = $user->Update($USER->GetID(), $arFields);
    if ($res) {
        // Ответ
        echo json_encode(array("status" => "ok"));
    } else {
        // Ответ
        echo json_encode(array("status" => 'error', "text" => $user->LAST_ERROR));
    }

}

// Авторизация
if ($action == "auth") {
    $result = $USER->Login(param_post("login"), param_post("password"), "Y");
    if (!is_array($result)) {
        $USER->Authorize($USER->GetID());
        // Ответ
        echo json_encode(array("status" => "ok"));
    } else {
        // Ответ
        echo json_encode(array("status" => "error", "error_text" => "Неверный логин или пароль"));
    }
}

// Регистрация
if ($action == "register") {
    // данные в массив
    $arFields = array();
    parse_str(param_post("form_data"), $arFields);
    // проверяем EMAIL на логин
    $rsUsers = \CUser::GetByLogin($arFields["EMAIL"]);
    if ($user = $rsUsers->GetNext()) {
        // Если логин уже есть
        echo json_encode(array("status" => "error", "error_text" => "Данный Email уже зарегистрирован на&nbsp;сайте!"));
        return;
    }
    // проверяем EMAIL на наличие у других пользователей
    $filter = array("=EMAIL" => $arFields["EMAIL"]);
    $rsUsers = \CUser::GetList(($by = "id"), ($order = "desc"), $filter); // выбираем пользователей
    if ($user = $rsUsers->GetNext()) {
        // Если Email уже есть
        echo json_encode(array("status" => "error", "error_text" => "Данный Email уже зарегистрирован на&nbsp;сайте!"));
        return;
    }
    // Регистрируем пользователя
    $user = new \CUser;
    $arUserFields = array(
        "ACTIVE" => "Y",
        "LOGIN" => $arFields["EMAIL"],
        "EMAIL" => $arFields["EMAIL"],
        "NAME" => $arFields["NAME"],
        "PASSWORD" => $arFields["PASSWORD"],
        "CONFIRM_PASSWORD" => $arFields["CONFIRM_PASSWORD"],
        "GROUP_ID" => array(5),
        "UF_USER_TYPE" => $arFields["user_type"]
    );

    /*$enums = \CUserFieldEnum::GetList(array(), array(
        "USER_FIELD_ID" => 13,
        "XML_ID" => $arFields["UF_PAYER_TYPE"]
    ));
    while($enum = $enums->GetNext()){
        $arUserFields['UF_PAYER_TYPE'] = $enum["ID"];
    }*/

    $arUserFields['PERSONAL_GENDER'] = $arFields["PERSONAL_GENDER"];
    /*if ( $arFields['UF_PAYER_TYPE'] == 'fiz' ){
        $arUserFields['PERSONAL_GENDER'] = $arFields["PERSONAL_GENDER"];
    } else if ( $arFields['UF_PAYER_TYPE'] == 'yur' ){
        $arUserFields['UF_COMPANY_NAME'] = $arFields["UF_COMPANY_NAME"];
    }*/

    $userID = $user->Add($arUserFields);
    if (intval($userID) > 0) {

        $email_html = '<p><img src="http://' . $_SERVER['HTTP_HOST'] . '/local/templates/my/images/logo.png"></p>
		<p>Здравствуйте, ' . $arFields["NAME"] . '!<br>
		Вы успешно зарегистрированы на сайте ' . $_SERVER['HTTP_HOST'] . ':</p>
		<hr>
		<p>ID пользователя: ' . $userID . '</p>
		<p>Логин: ' . $arFields["EMAIL"] . '</p>';

        $email_html .= '<p>Имя: ' . $arFields["NAME"] . '</p>';
        /*if ( $arFields['UF_PAYER_TYPE'] == 'fiz' ){
            $email_html .= '<p>Имя: '.$arFields["NAME"].'</p>';
        } else if ( $arFields['UF_PAYER_TYPE'] == 'yur' ){
            $email_html .= '<p>Название компании: '.$arFields["UF_COMPANY_NAME"].'</p>
            <p>Имя контактного лица: '.$arFields["NAME"].'</p>';
        }*/

        // отправка инфы на почту
        $arEventFields = array(
            "EMAIL" => $arFields["EMAIL"],
            "HTML" => $email_html
        );
        \CEvent::SendImmediate("SEND_USER_INFO", "s1", $arEventFields);

        BXClearCache(true, "/illustrators/");

        // Ответ
        echo json_encode(array("status" => 'ok'));
    } else {
        // Ответ
        echo json_encode(array("status" => 'error', "error_text" => $user->LAST_ERROR));
    }
}

// Формирование контрольной строки для восстановления пароля
if ($action == "password_recovery") {
    // данные в массив
    $arFields = array();
    parse_str(param_post("form_data"), $arFields);
    $user_id = false;
    // Если какие-то данные определены
    if (strlen($arFields['email']) > 0) {
        // проверяем EMAIL на наличие у других пользователей
        $filter = array("=EMAIL" => $arFields["email"]);
        $users = \CUser::GetList(($by = "id"), ($order = "desc"), $filter); // выбираем пользователей
        if ($user = $users->GetNext()) {

            $kod = md5(time() . rand(1, 10000));
            // Заносим код в профиль пользователя
            $obUser = new \CUser;
            $res = $obUser->Update($user['ID'], array('UF_RECOVERY_CODE' => $kod));
            if ($res) {

                $user_id = $user['ID'];

                $link = '<a href="' . $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . '/password_recovery/?code=' . $kod . '">' . $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . '/password_recovery/?code=' . $kod . '</a>';

                // отправка инфы на почту
                $arEventFields = array(
                    "EMAIL" => $arFields["email"],
                    "LINK" => $link
                );

                \CEvent::SendImmediate("RECOVERY_STRING", "s1", $arEventFields);
                // Ответ
                echo json_encode(array("status" => "ok"));
                return;
            }
        }
        if (!$user_id) {
            // Ответ
            echo json_encode(array("status" => "error", "text" => "Такой Email на сайте не зарегистрирован"));
            return;
        }
    }
}

// Изменение пароля
if ($action == "personal_password") {
    // данные в массив
    $arFields = array();
    parse_str(param_post("form_data"), $arFields);
    // Ищем пользователя с таким кодом
    $filter = array("=UF_RECOVERY_CODE" => $arFields['code']);
    $rsUsers = \CUser::GetList(($by = "id"), ($order = "desc"), $filter,
        array(
            'FIELDS' => array('ID'),
            'SELECT' => array('UF_*')
        )
    );
    while ($arUser = $rsUsers->GetNext()) {
        $user_id = $arUser['ID'];
    }
    // Если какие-то данные определены
    if (intval($user_id) > 0) {
        // Заносим в профиль пользователя
        $fields['UF_RECOVERY_CODE'] = '';
        $fields['PASSWORD'] = $arFields['PASSWORD'];
        $fields['CONFIRM_PASSWORD'] = $arFields['CONFIRM_PASSWORD'];
        $user = new \CUser;
        $res = $user->Update($user_id, $fields);
        if ($res) {
            $_SESSION['password_ok'] = 1;
            $USER->Logout();
            // Ответ
            echo json_encode(array("status" => "ok", 'html' => $html));
        } else {
            // Ответ
            echo json_encode(array("status" => 'error', "text" => $user->LAST_ERROR));
        }
    } else {
        // Ответ
        echo json_encode(array("status" => 'error', "text" => 'Ошибочный код'));
    }
}

// todo Добавляем товар в корзину
if ($action == "add_to_basket") {

    $arResult = array(
        "status" => "error",
        "text" => "Ошибка добавление в корзину",
        "$_POST" => $_POST
    );

    $tov_id = $_POST['tov_id'];

    if( intval($tov_id) > 0 ){

        $tov = tools\el::info($tov_id);

        $quantity = 1;
        $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), \Bitrix\Main\Context::getCurrent()->getSite());
        if ($item = $basket->getExistsItem('catalog', $tov_id)) {
            $item->setField('QUANTITY', $item->getQuantity() + $quantity);
        } else {
            $item = $basket->createItem('catalog', $tov_id);
            $item->setFields(array(
                'QUANTITY' => $quantity,
                'CURRENCY' => \Bitrix\Currency\CurrencyManager::getBaseCurrency(),
                'LID' => \Bitrix\Main\Context::getCurrent()->getSite(),
                'PRODUCT_PROVIDER_CLASS' => '\CCatalogProductProvider',
            ));
        }
        $basket->save();

        ob_start();
            $APPLICATION->IncludeComponent("my:basket_small", "");
            $top_basket_html = ob_get_contents();
        ob_end_clean();

        // Добавление товара в лог для хитов на главной
        project\home_page_hits::addToLog($tov_id);

        $arResult = array(
            "status" => "ok",
            "tov_name" => $tov['~NAME'],
            "top_basket_html" => $top_basket_html
        );

    }

    // Ответ
    echo json_encode($arResult);
}

// todo Изменение количества в корзине
if ($action == "recalc_qt") {
    if (intval(param_post("item_id")) > 0) {

        $pureURL = param_post("pure_url");

        \Bitrix\Main\Loader::includeModule("sale");
        $arFields = array("QUANTITY" => param_post("quantity"));
        \CSaleBasket::Update(param_post("item_id"), $arFields);

        ob_start();
        $APPLICATION->IncludeComponent("my:basket_small", "", array('pureURL' => $pureURL));
        $top_basket_html = ob_get_contents();
        ob_end_clean();

        ob_start();
        $APPLICATION->IncludeComponent("my:basket_items", "");
        $basket_itmes_html = ob_get_contents();
        ob_end_clean();

        // Ответ
        $arResult = array(
            "status" => "ok",
            "top_basket_html" => $top_basket_html,
            "basket_itmes_html" => $basket_itmes_html
        );
        echo json_encode($arResult);
    }
}

// todo Удаляем элемент корзины
if ($action == "unset_item") {


    if (intval($_POST['item_id']) > 0) {

        $pureURL = $_POST['pure_url'];
        //$pureURL = param_post("pure_url");

        \Bitrix\Main\Loader::includeModule("sale");
        \CSaleBasket::Delete($_POST["item_id"]);

        ob_start();
        $APPLICATION->IncludeComponent("my:basket_small", "", ['pureURL' => $pureURL]);
        $top_basket_html = ob_get_contents();
        ob_end_clean();

        ob_start();
        $APPLICATION->IncludeComponent("my:basket_items", "");
        $basket_itmes_html = ob_get_contents();
        ob_end_clean();

        // Ответ
        $arResult = array(
            "status" => "ok",
            "top_basket_html" => $top_basket_html,
            "basket_itmes_html" => $basket_itmes_html
        );
        echo json_encode($arResult);
    }
}

if ($action == "clear_basket") {
    \Bitrix\Main\Loader::includeModule("sale");
    \CSaleBasket::DeleteAll(\CSaleBasket::GetBasketUserID());
    // Ответ
    echo json_encode(array("status" => "ok"));
}

// *** ФОРМИРОВАНИЕ ЗАКАЗА***
if ($action == "order") {

    // данные в массив
    $arFields = array();
    parse_str(param_post("form_data"), $arFields);

    // Инфо по корзине
    $products = array();
    $cupon = project\my_coupon::get_active_coupon();
    $basketInfo = basketInfo($cupon);
    $basket_cnt = $basketInfo['basket_cnt'];
    $basket = $basketInfo['basket'];
    $arBasket = $basketInfo['arBasket'];
    $basket_disc_sum = $basketInfo['basket_disc_sum'];
    $discount = $basketInfo['discount'];
    $pay_sum = $basket_disc_sum + $arFields['DELIVERY_PRICE'];

    // Проверяем активность купона
    $active_coupon = project\my_coupon::get_active_coupon();
    if ($active_coupon) {
        DiscountCouponsManager::init();
        DiscountCouponsManager::add($active_coupon);
    } else {
        DiscountCouponsManager::clear();
    }

    // Если пользователь авторизован
    if ($USER->IsAuthorized()) {
        // ID пользователя
        $userID = $USER->GetID();
        // Если пользователь НЕ авторизован
    } else {

        // РЕГИСТРИРУЕМ
        // проверяем EMAIL на логин
        $email_error = false;
        $rsUsers = \CUser::GetByLogin($arFields["EMAIL"]);
        if ($user = $rsUsers->GetNext()) {
            $email_error = true;
            $userID = $user['ID'];
        }
        if (!$email_error) {
            // проверяем EMAIL на наличие у других пользователей
            $filter = array("=EMAIL" => $arFields["EMAIL"]);
            $rsUsers = \CUser::GetList(($by = "id"), ($order = "desc"), $filter); // выбираем пользователей
            if ($user = $rsUsers->GetNext()) {
                $email_error = true;
                $userID = $user['ID'];
            }
        }

        if ($email_error) {

            if (strlen($arFields["password"]) > 0) {

                $result = $USER->Login($arFields["EMAIL"], $arFields["password"]);
                if (!is_array($result)) {
                    $USER->Authorize($userID);
                } else {
                    // Если логин уже есть
                    echo json_encode(array("status" => "password_error", "error_text" => "Неверный логин или пароль!"));
                    return;
                }

            } else {

                // Если логин уже есть
                echo json_encode(array("status" => "email_error"));
                return;

            }

        }

        if (!$email_error) {

            // Формируем пароль
            $password = randString(12, array("abcdefghijklmnopqrstuvwxyz", "0123456789"));

            // Регистрируем пользователя
            $user = new \CUser;
            $arUserFields = array(
                "ACTIVE" => "Y",
                "LOGIN" => $arFields["EMAIL"],
                "EMAIL" => $arFields["EMAIL"],
                "NAME" => $arFields["NAME"],
                "LAST_NAME" => $arFields["LAST_NAME"],
                "PERSONAL_PHONE" => $arFields["PERSONAL_PHONE"],
                "PASSWORD" => $password,
                "CONFIRM_PASSWORD" => $password,
                "GROUP_ID" => array(5),
                "UF_USER_TYPE" => 1,
                "UF_PAYER_TYPE" => 3
            );

            $userID = $user->Add($arUserFields);
            if (intval($userID) > 0) {

                $email_html = '<p><img src="http://' . $_SERVER['HTTP_HOST'] . '/local/templates/my/images/logo.png"></p>
				<p>Здравствуйте, ' . $arFields["NAME"] . '!<br>
				Вы успешно зарегистрированы на сайте ' . $_SERVER['HTTP_HOST'] . ':</p>
				<hr>
				<p>ID пользователя: ' . $userID . '</p>
				<p>Логин: ' . $arFields["EMAIL"] . '</p>
				<p>Пароль: ' . $password . '</p>';

                $email_html .= '<p>Имя: ' . $arFields["NAME"] . '</p>';

                // отправка инфы на почту
                $arEventFields = array(
                    "EMAIL" => $arFields["EMAIL"],
                    "HTML" => $email_html
                );
                \CEvent::SendImmediate("SEND_USER_INFO", "s1", $arEventFields);

                $USER->Authorize($userID);

            } else {
                // Ответ
                echo json_encode(
                    array("status" => 'error', "error_text" => $user->LAST_ERROR)
                );
                return;
            }

        }

    }

    // СОЗДАЁМ ЗАКАЗ
    $order = Sale\Order::create('s1', $userID);

    // Передаём корзину в заказ
    $order->setBasket($basket);

    // Тип плательщика
    $order->setPersonTypeId(1);

    // Оплата
    $paymentCollection = $order->getPaymentCollection();
    $payment = $paymentCollection->createItem(Sale\PaySystem\Manager::getObjectById($arFields['ps']));
    $payment->setField('SUM', $pay_sum); // Сумма к оплате
    $payment->setField('CURRENCY', 'RUB');

    // Доставка
    $shipmentCollection = $order->getShipmentCollection();
    $shipment = $shipmentCollection->createItem(Sale\Delivery\Services\Manager::getObjectById($arFields['ds']));
    if (!is_null($arFields['DELIVERY_PRICE']) && $arFields['DELIVERY_PRICE'] != '-') {
        $shipment->setField('CUSTOM_PRICE_DELIVERY', 'Y');
        $shipment->setField('PRICE_DELIVERY', $arFields['DELIVERY_PRICE']);
    }
    $shipmentItemCollection = $shipment->getShipmentItemCollection();
    foreach ($basket as $basketItem) {
        $item = $shipmentItemCollection->createItem($basketItem);
        $item->setQuantity($basketItem->getQuantity());
    }
    $order->doFinalAction(true);

    // Заполняем свойства заказа
    $propertyCollection = $order->getPropertyCollection();

    $prop = $propertyCollection->getItemByOrderPropertyId(1);
    $prop->setValue($arFields["NAME"]);
    $prop = $propertyCollection->getItemByOrderPropertyId(2);
    $prop->setValue($arFields["LAST_NAME"]);
    $prop = $propertyCollection->getItemByOrderPropertyId(3);
    $prop->setValue($arFields["EMAIL"]);
    $prop = $propertyCollection->getItemByOrderPropertyId(4);
    $prop->setValue(project\site::PHONE_PREFIX . $arFields["PERSONAL_PHONE"]);

    $prop = $propertyCollection->getItemByOrderPropertyId(6);
    $prop->setValue($arFields["LOCATION_NAME"] . ' [' . $arFields["LOCATION_ID"] . ']');
    $prop = $propertyCollection->getItemByOrderPropertyId(7);
    $prop->setValue($arFields["LOCATION_ZIP"]);
    if (strlen($arFields["STREET"]) > 0) {
        $prop = $propertyCollection->getItemByOrderPropertyId(8);
        $prop->setValue($arFields["STREET"]);
    }
    if (strlen($arFields["HOUSE"]) > 0) {
        $prop = $propertyCollection->getItemByOrderPropertyId(9);
        $prop->setValue($arFields["HOUSE"]);
    }
    if (strlen($arFields["CORPUS"]) > 0) {
        $prop = $propertyCollection->getItemByOrderPropertyId(10);
        $prop->setValue($arFields["CORPUS"]);
    }
    if (strlen($arFields["NUMBER"]) > 0) {
        $prop = $propertyCollection->getItemByOrderPropertyId(12);
        $prop->setValue($arFields["NUMBER"]);
    }
    if (strlen($arFields["order_pvz_address"]) > 0) {
        $prop = $propertyCollection->getItemByOrderPropertyId(42);
        $prop->setValue($arFields["order_pvz_address"]);
    }
    if (strlen($arFields["order_pvz_code"]) > 0) {
        $prop = $propertyCollection->getItemByOrderPropertyId(43);
        $prop->setValue($arFields["order_pvz_code"]);
    }

    // Сохраняем новый заказ
    $order->save();

    // Определяем ID нового заказа
    $order_id = $order->GetId();

    if (intval($order_id) > 0) {

        // Очищаем корзину
        \CSaleBasket::DeleteAll(\CSaleBasket::GetBasketUserID());

        $arDS = \CSaleDelivery::GetByID($arFields['ds']);
        $arPS = \CSalePaySystem::GetByID($arFields["ps"]);

        $userProps = '<h2>Контактная информация</h2>';
        $userProps .= '<p>Имя: ' . $arFields["NAME"] . ';</p>';
        $userProps .= '<p>Фамилия: ' . $arFields["LAST_NAME"] . ';</p>';
        $userProps .= '<p>Email: ' . $arFields["EMAIL"] . ';</p>';
        $userProps .= '<p>Телефон: ' . project\site::PHONE_PREFIX . $arFields["PERSONAL_PHONE"] . ';</p>';

        $userProps .= '<h2>Информация по доставке</h2>';
        $userProps .= '<p>Вариант доставки: ' . $arDS['NAME'] . ';</p>';
        if (strlen($arFields["order_pvz_address"]) > 0) {
            $userProps .= '<p>Адрес ПВЗ: ' . $arFields["order_pvz_address"] . ';</p>';
        }
        if (strlen($arFields["order_pvz_code"]) > 0) {
            $userProps .= '<p>Код ПВЗ: ' . $arFields["order_pvz_code"] . ';</p>';
        }
        $userProps .= '<p>Населённый пункт: ' . $arFields["LOCATION_NAME"] . ';</p>';
        $userProps .= '<p>Индекс: ' . $arFields["LOCATION_ZIP"] . ';</p>';
        if (strlen($arFields["STREET"]) > 0) {
            $userProps .= '<p>Улица: ' . $arFields["STREET"] . ';</p>';
        }
        if (strlen($arFields["HOUSE"]) > 0) {
            $userProps .= '<p>Дом: ' . $arFields["HOUSE"] . ';</p>';
        }
        if (strlen($arFields["CORPUS"]) > 0) {
            $userProps .= '<p>Корпус: ' . $arFields["CORPUS"] . ';</p>';
        }
        if (strlen($arFields["NUMBER"]) > 0) {
            $userProps .= '<p>Квартира/офис: ' . $arFields["NUMBER"] . ';</p>';
        }

        $userProps .= '<h2>Информация о заказе</h2>';
        $userProps .= '<p>Вариант оплаты: ' . $arPS['NAME'] . ';</p>';
        if ($active_coupon) {
            $userProps .= '<p>Применён купон: ' . $active_coupon . ';</p>';
        }

        // Формируем таблицу для письма
        $arProductsTable = '<table width="100%" border="1" cellpadding="4" cellspacing="4" style="border-collapse:collapse; font-size: 13px;">';
        $arProductsTable .= '<tr>
			<td><b>Товар</b></td>
			<td><b>Цена за ед., руб.</b></td>
			<td><b>Количество</b></td>
			<td><b>Сумма, руб.</b></td>
		</tr>';

        foreach ($arBasket as $basketItem) {

            // Инфо о товаре
            $sku = $basketItem->el;
            $product = $basketItem->product;

            $size = $sku['PROPERTY_SIZE_VALUE'];

            $color = tools\el::info($sku['PROPERTY_COLOR_VALUE'])['NAME'];

            $price = $basketItem->price;
            $discPrice = $basketItem->discPrice;
            $quantity = $basketItem->getField('QUANTITY');

            $color_size = array();
            if ($color) {
                $color_size[] = $color;
            }
            if ($size) {
                $color_size[] = $size;
            }
            if (count($color_size) > 0) {
                $color_size = ' (' . implode('/', $color_size) . ')';
            } else {
                $color_size = '';
            }
            $arProductsTable .= '<tr>
				<td>' . $product['NAME'] . $color_size . '</td>
				<td>' . number_format($discPrice, 0, ",", " ") . '</td>
				<td>' . round($quantity, 0) . '</td>
				<td>' . number_format($discPrice * $quantity, 0, ",", " ") . '</td>
			</tr>';
        }
        $arProductsTable .= '</table>';

        $basket_disc_sum = number_format($basket_disc_sum, 2, ",", " ") . ' руб.';
        $pay_sum = number_format($pay_sum, 2, ",", " ") . ' руб.';


        $delivery_price = '-';
        if ($arFields['DELIVERY_PRICE'] != null) {
            if (in_array($arFields['ds'], project\site::$calc_ds_list)) {
                if ($arFields['DELIVERY_PRICE'] == '-') {
                    $delivery_price = '<i>не удалось определить</i>';
                } else {
                    $delivery_price = number_format($arFields['DELIVERY_PRICE'], 2, ",", " ") . ' руб.';
                }
            } else {
                if ($arFields['DELIVERY_PRICE'] != '-') {
                    $delivery_price = number_format($arFields['DELIVERY_PRICE'], 2, ",", " ") . ' руб.';
                }
            }
        }

        $mail_fields = array(
            "NAME" => $arFields["NAME"],
            "EMAIL" => $arFields["EMAIL"],
            "ORDER_ID" => $order_id,
            "FIELDS" => $userProps,
            "TABLE" => $arProductsTable,
            "BASKET_SUM" => $basket_disc_sum,
            "DELIVERY_PRICE" => $delivery_price,
            "PAY_SUM" => $pay_sum,
        );
        \CEvent::SendImmediate("ORDER_INFO", "s1", $mail_fields);

        // Деактивируем купон (если он был)
        if ($active_coupon) {
            project\my_coupon::deactivate($active_coupon);
            project\my_coupon::clear();
        }

        // Отправляем заказ в CRM
        project\order_export::send($order_id);

        // Ответ
        echo json_encode(array(
            "status" => "ok",
            "order_id" => $order_id,
            "ps" => $arFields["ps"]
        ));

    }
}

// Подписка на рассылку
if ($action == "subscribe") {
    \Bitrix\Main\Loader::includeModule("subscribe");
    $dbSubscr = \CSubscription::GetByEmail(param_post("email"));
    if ($dbSubscr->SelectedRowsCount() <= 0) {
        $subscr = new \CSubscription;
        $arFields = array(
            "USER_ID" => false,
            "FORMAT" => "html",
            "EMAIL" => param_post("email"),
            "ACTIVE" => "Y",
            "CONFIRMED" => "Y",
            "RUB_ID" => array(1),
            "SEND_CONFIRM" => "N"
        );
        $subscr = new \CSubscription;
        $res = $subscr->Add($arFields);
        if ($res) {
            // Ответ
            echo json_encode(array("status" => "ok"));
        }
    } else {
        // Ответ
        echo json_encode(array("status" => "error"));
    }

}

// Обратная связь
if ($action == "feedback") {
    // данные в массив
    $arFields = array();
    parse_str(param_post("form_data"), $arFields);
    $PROP[38] = $arFields["name"];
    $PROP[39] = project\site::PHONE_PREFIX . $arFields["phone"];
    $PROP[40] = $arFields["email"];
    $el = new \CIBlockElement;
    $arLoadProductArray = array(
        "IBLOCK_ID" => project\site::FEEDBACK_IBLOCK_ID,
        "PROPERTY_VALUES" => $PROP,
        "NAME" => 'Новое сообщение',
        "PREVIEW_TEXT" => $arFields["message"],
        "ACTIVE" => "Y"
    );
    if ($ID = $el->Add($arLoadProductArray)) {
        // отправка инфы на почту
        $arEventFields = array(
            "NAME" => $arFields["name"],
            "PHONE" => project\site::PHONE_PREFIX . $arFields["phone"],
            "EMAIL" => $arFields["email"],
            "MESSAGE" => $arFields["message"],
        );
        \CEvent::SendImmediate("FEEDBACK", "s1", $arEventFields);
        // Ответ
        echo json_encode(array("status" => 'ok'));
    }
}


require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php"); ?>
