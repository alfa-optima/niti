<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use \Bitrix\Main,
    \Bitrix\Main\Localization\Loc as Loc,
    \Bitrix\Main\Loader,
    \Bitrix\Main\Config\Option,
    \Bitrix\Sale\Delivery,
    \Bitrix\Sale\PaySystem,
    \Bitrix\Sale,
    \Bitrix\Sale\Order,
    \Bitrix\Sale\DiscountCouponsManager,
    \Bitrix\Main\Context;
\Bitrix\Main\Loader::includeModule("main");   \Bitrix\Main\Loader::includeModule("iblock");
\Bitrix\Main\Loader::includeModule("catalog");   \Bitrix\Main\Loader::includeModule("sale");
$action = param_get("action");

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if ($action == "change_gender_type"){
	$gender_type = param_get("gender_type");

	//var_dump($gender_type);

	if( strlen($gender_type) > 0 ){
		foreach( project\gender_type::list() as $type){
			if( $gender_type == $type['CODE'] ){
				$_SESSION['gender_type'] = $type;
				header("HTTP/1.1 301 Moved Permanently");
				header("Location: http://new.niti-niti.ru/" . $gender_type . "_home/");
				exit();
			}
		}
	}
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php"); ?>
