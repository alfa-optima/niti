<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if( 
	strlen(param_post('loc_id')) > 0
	&&
	intval(param_post('ds')) > 0
	&&
	strlen(param_post('json_basket')) > 0
){

	$array_basket = json_to_array(param_post('json_basket'));

	$delivery_price = project\site::DEFAULT_DELIVERY_PRICE;
	
	foreach ( project\site::$calc_ds_list as $class => $ds_id ){
		if( param_post('ds') == $ds_id ){
			$res = $class::calcDeliveryPrice( param_post('loc_id'), $array_basket );
		}
	}
	
	if( $res && $res['status'] == 'ok' ){
		$delivery_price = (int)$res['price'];
	}
		
	ob_start(); 
		// basket_right
		$params = array('delivery_price' => $delivery_price);
		$APPLICATION->IncludeComponent("my:basket_right", "", $params);
		$right_basket = ob_get_contents();
	ob_end_clean();

	// Ответ
	$fields = Array(
		"status" => "ok",
		'right_basket' => $right_basket,
		'delivery_price' => $delivery_price,
	);
	if( $res ){    $fields['res_status'] = $res['status'];    }
	echo json_encode($fields);
	
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");