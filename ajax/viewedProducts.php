<? require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

header('Access-Control-Allow-Origin: *');

$product_id = strip_tags(intval($_GET['product_id']));

// Просмотренные товары
if (
    is_array($_SESSION['viewed_products'])
    &&
    count($_SESSION['viewed_products']) > 0
){

    $GLOBALS["viewed_products"]['ID'] = $_SESSION['viewed_products'];
    if( intval($product_id) > 0 ){
        $GLOBALS["viewed_products"]['!ID'] = $product_id;
    }

    $APPLICATION->IncludeComponent(
        "bitrix:catalog.section", "viewed_products_ajax",
        Array(
            "favorites" => project\favorites::list(),
            "IBLOCK_TYPE" => "content",
            "IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
            "SECTION_USER_FIELDS" => array(),
            "ELEMENT_SORT_FIELD" => "NAME",
            "ELEMENT_SORT_ORDER" => "ASC",
            "ELEMENT_SORT_FIELD2" => "NAME",
            "ELEMENT_SORT_ORDER2" => "ASC",
            "FILTER_NAME" => "viewed_products",
            "HIDE_NOT_AVAILABLE" => "N",
            "PAGE_ELEMENT_COUNT" => "6",
            "LINE_ELEMENT_COUNT" => "3",
            "PROPERTY_CODE" => array('PRINT_ID'),
            "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
            "OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
            "OFFERS_PROPERTY_CODE" => ['PHTO'],
            "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
            "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
            "OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
            "OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
            "OFFERS_LIMIT" => 1 /*$arParams["LIST_OFFERS_LIMIT"]*/,
            "TEMPLATE_THEME" => "",
            "PRODUCT_SUBSCRIPTION" => "N",
            "SHOW_DISCOUNT_PERCENT" => "N",
            "SHOW_OLD_PRICE" => "N",
            "MESS_BTN_BUY" => "Купить",
            "MESS_BTN_ADD_TO_BASKET" => "В корзину",
            "MESS_BTN_SUBSCRIBE" => "Подписаться",
            "MESS_BTN_DETAIL" => "Подробнее",
            "MESS_NOT_AVAILABLE" => "Нет в наличии",
            "SECTION_URL" => "",
            "DETAIL_URL" => "",
            "SECTION_ID_VARIABLE" => "SECTION_ID",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "36000000",
            "CACHE_GROUPS" => "Y",
            "SET_META_KEYWORDS" => "N",
            "META_KEYWORDS" => "",
            "SET_META_DESCRIPTION" => "N",
            "META_DESCRIPTION" => "",
            "BROWSER_TITLE" => "-",
            "ADD_SECTIONS_CHAIN" => "N",
            "DISPLAY_COMPARE" => "N",
            "SET_TITLE" => "N",
            "SET_STATUS_404" => "N",
            "CACHE_FILTER" => "Y",
            "PRICE_CODE" => array('BASE'),
            "USE_PRICE_COUNT" => "N",
            "SHOW_PRICE_COUNT" => "1",
            "PRICE_VAT_INCLUDE" => "Y",
            "CONVERT_CURRENCY" => "N",
            "BASKET_URL" => "/personal/basket.php",
            "ACTION_VARIABLE" => "action",
            "PRODUCT_ID_VARIABLE" => "id",
            "USE_PRODUCT_QUANTITY" => "N",
            "ADD_PROPERTIES_TO_BASKET" => "Y",
            "PRODUCT_PROPS_VARIABLE" => "prop",
            "PARTIAL_PRODUCT_PROPERTIES" => "N",
            "PRODUCT_PROPERTIES" => "",
            "PAGER_TEMPLATE" => "",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "PAGER_TITLE" => "Товары",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "Y",
            "ADD_PICT_PROP" => "-",
            "LABEL_PROP" => "-",
            'INCLUDE_SUBSECTIONS' => "Y",
            'SHOW_ALL_WO_SECTION' => "Y"
        )
    );
}