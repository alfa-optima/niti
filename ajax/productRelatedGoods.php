<? require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

header('Access-Control-Allow-Origin: *');

$product_id = strip_tags($_GET['product_id']);

if( intval($product_id) > 0 ){

    $APPLICATION->IncludeComponent(
        "my:related_goods", "ajax",
        array( 'product_id' => intval($product_id) )
    );

}