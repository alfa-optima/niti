<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
\Bitrix\Main\Loader::includeModule("main");   \Bitrix\Main\Loader::includeModule("iblock");
$action = param_post("action");

if( count($_POST) > 0 ){
		
	if(
		$USER->IsAuthorized()
		&&
		$USER->IsAdmin()
	){
		
		// данные в массив
		$postSettings = Array();
		parse_str(param_post("form_data"), $postSettings);
		
		if(
			$postSettings
			&&
			is_array($postSettings)
			&&
			count($postSettings) > 0
		){
			
			$site = new site();
			$defaultSettings = $site->defaultSettings();
			$settingsKeys = array_keys($defaultSettings);
			
			foreach( $postSettings as $key => $value ){
				if( in_array($key, $settingsKeys) ){
					$site->updateSettings(array($key => $value));
				}
			}

			// Ответ
			echo json_encode( Array(
				"status" => "ok"
			) );
			
			
		} else {
			// Ответ
			echo json_encode( Array(
				"status" => "error",
				"text" => "Ошибка запроса"
			) );
		}
		
	} else {
		
		// Ответ
		echo json_encode( Array(
			"status" => "error",
			"text" => "Ошибка авторизации"
		) );
		
	}

} else {
	
	// Ответ
	echo json_encode( Array(
		"status" => "error",
		"text" => "Ошибка запроса"
	) );
	
}
