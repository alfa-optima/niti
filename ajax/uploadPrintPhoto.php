<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if ($USER->IsAuthorized()){

	$catalogPrintPhoto = new project\catalog_print_photo($_FILES);
	$catalogPrintPhoto->upload();

}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");