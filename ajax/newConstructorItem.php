<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if ($USER->IsAuthorized()){

  $res = project\constructor::addItem($_POST);
  echo json_encode($res);

} else {
	// Ответ
	echo json_encode( array('status' => 'error', 'text' => 'Ошибка авторизации') );
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");