<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

header('Access-Control-Allow-Origin: *');

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;



$el = tools\el::info(param_get('id'));

if( intval($el['ID']) > 0 ){

    // Комментарии
    $comment = new project\comment($el['ID']);
    $comments = $comment->GetList(strip_tags($_GET['max_comment_id']));

    $infoAdmin = \AOptima\Tools\user::info(1);

    foreach ($comments as $comment) {

        $infoUser = \AOptima\Tools\user::info($comment['PROPERTY_USER_ID_VALUE']);

        $time = strtotime($comment['DATE_CREATE']);

        $item = [
            'id' => $comment['ID'],
            'name' => $comment['NAME'],
            'text' => trim(str_replace("<br />", "\n", $comment['PREVIEW_TEXT'])),
            'like' => $comment['PROPERTY_LIKES_VALUE'],
            'countComments' => 0,
            'user' => [
                'name' => $infoUser['NAME'],
                'avatar' => $infoUser['PERSONAL_PHOTO'],
            ],
            'time' => [
                'date' => date('d.m.Y', $time),
                'hour' => date('g:i', $time)
            ]
        ];

        if (!empty($comment['DETAIL_TEXT'])) {

            $item['reply'] = [
                'id' => $comment['ID'],
                'name' => "НИТИ-НИТИ",
                'user' => [
                    // 'code' => "author",
                    'code' => "admin",
                    'title' => "администратор", //$infoAdmin
                    'avatar' => $infoAdmin['PERSONAL_PHOTO'],
                ],
                'text' => trim(str_replace("<br />", "\n", $comment['DETAIL_TEXT'])),
                'like' => 0,
                'countComments' => 0,
                'time' => [
                    'date' => null,
                    'hour' => null
                ]
            ];

            $item['countComments'] = 1;
        }

        $result[] = $item;

    }

    echo json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);

}


