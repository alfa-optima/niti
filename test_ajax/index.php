<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>

<script src="<?= SITE_TEMPLATE_PATH ?>/js/jquery-3.1.1.min.js"></script>

<script>
var steps = 0;
function testSend(){
    steps += 1;
    $('body').html('<p style="color:blue;">Процесс идёт (шагов - '+steps+')</p>');
    $.post("/test_ajax/ajax.php", {}, function(data){
        if (data.status == 'ok'){
            $('body').html('<p style="color:green;">Готово!</p>');
        } else if (data.status == 'need'){
            testSend();
        } else if (data.status == 'error'){
            $('body').html('<p style="color:red;">'+data.text+'!</p>');
        }
    }, 'json')
    .fail(function(data, status, xhr){
        $('body').html('<p style="color:red;">Ошибка запроса!</p>');
    })
    .always(function(data, status, xhr){})
}
$(document).ready(function(){
    //testSend();
})
</script>
