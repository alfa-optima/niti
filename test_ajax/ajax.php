<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

set_time_limit(0);
ini_set('memory_limit', '1024M');

\Bitrix\Main\Loader::includeModule('iblock');

$maxCNT = 30;

$cnt = 0;
$filter = Array(
	"IBLOCK_ID" => 5
);
$fields = Array( "ID" );
$dbElements = \CIBlockElement::GetList(
	array("SORT"=>"ASC"), $filter, false, array("nTopCount"=>$maxCNT), $fields
);
while ($element = $dbElements->GetNext()){
    $cnt++;
    \CIBlockElement::Delete($element['ID']);
}

if( $cnt == $maxCNT ){
    $status = 'need';
} else {
    $status = 'ok';
}

echo json_encode(['status' => $status]);

