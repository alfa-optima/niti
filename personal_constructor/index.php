<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Конструктор");


$APPLICATION->SetPageProperty("title", 'Конструктор');
$APPLICATION->AddChainItem('Конструктор', pureURL());

ob_start();
$APPLICATION->IncludeComponent(
    "bitrix:menu", "personal_menu",
    array(
        "ROOT_MENU_TYPE" => "personal_menu",
        "MENU_CACHE_TYPE" => "N",
        "MENU_CACHE_TIME" => "3600",
        "MENU_CACHE_USE_GROUPS" => "N",
        "CACHE_SELECTED_ITEMS" => "N",
        "MENU_CACHE_GET_VARS" => array(
        ),
        "MAX_LEVEL" => "1",
        "CHILD_MENU_TYPE" => "left",
        "USE_EXT" => "N",
        "DELAY" => "N",
        "ALLOW_MULTI_SELECT" => "N",
        "COMPONENT_TEMPLATE" => "footer_menu",
    ),
    false
);
$arResult['TOP_MENU_HTML'] = ob_get_contents();
ob_end_clean();

include($_SERVER["DOCUMENT_ROOT"]."/include/areas/personal_constructor.php");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>
