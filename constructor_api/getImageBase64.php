<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

header('Access-Control-Allow-Origin: *');

// Если передан файл
if( intval($_POST['file_id']) > 0 ){
	$file_path = $_SERVER['DOCUMENT_ROOT'].\CFile::GetPath($_POST['file_id']);
	if( file_exists($file_path) ){  
		base64_from_image($file_path, true);
	}
}