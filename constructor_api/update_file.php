<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

header('Access-Control-Allow-Origin: *');

$tmp_path = $_SERVER['DOCUMENT_ROOT'].project\constructor::UPDATE_FILE_PATH;
if( !file_exists($tmp_path) ){     mkdir($tmp_path, 0700);     }

$file_path = false;

// Если передан файл
if(
	$_FILES[project\constructor::UPDATE_FILE_NAME]
	&&
	in_array(
		$_FILES[project\constructor::UPDATE_FILE_NAME]['type'],
		array( 'image/jpeg', 'image/png' )
	)
	&&
	$_FILES[project\constructor::UPDATE_FILE_NAME]['error'] == 0
	&&
	$_FILES[project\constructor::UPDATE_FILE_NAME]['size'] > 0
){
	
	$file_path = $_SERVER['DOCUMENT_ROOT'].project\constructor::UPDATE_FILE_PATH.$_FILES[project\constructor::UPDATE_FILE_NAME]['name'];
	if( file_exists($file_path) ){     unlink($file_path);     }
	
	if ( move_uploaded_file($_FILES[project\constructor::UPDATE_FILE_NAME]['tmp_name'], $file_path) ){
		
		if( $_FILES[project\constructor::UPDATE_FILE_NAME]['type'] == 'image/png' ){
			$src = imagecreatefrompng($file_path);
		} else if( $_FILES[project\constructor::UPDATE_FILE_NAME]['type'] == 'image/jpeg' ){
			$src = imagecreatefromjpeg($file_path);
		}
		
		if( $_FILES[project\constructor::UPDATE_FILE_NAME]['type'] == 'image/png' ){
			imagepng($src, $file_path);
		} else if( $_FILES[project\constructor::UPDATE_FILE_NAME]['type'] == 'image/jpeg' ){
			imagejpeg($src, $file_path);
		}

		imagedestroy($src);
	}
	
// Если передан внешний URL
} else if(
	count($_POST) > 0
	&&
	strlen($_POST['url']) > 0
){
	
	$urlHeaders = @get_headers($_POST['url']);
	if(
		strpos($urlHeaders[0], '200')
		&&
		in_array(
			file_type($_POST['url']),
			array( 'image/jpeg', 'image/png' )
		)
	){
		
		// Путь к файлу
		$arURI = arURI(pureURL($_POST['url']));
		$file_path = $_SERVER['DOCUMENT_ROOT'].project\constructor::UPDATE_FILE_PATH.$arURI[count($arURI)-1];
		if( file_exists($file_path) ){     unlink($file_path);     }
		
		// Скачивание файла
		$ReadFile = fopen( $_POST['url'], "rb");
		if ( $ReadFile ){
			$WriteFile = fopen( $file_path, "wb" );
			if ( $WriteFile ){
				while( !feof($ReadFile) ){
					fwrite($WriteFile, fread($ReadFile, 4096));
				}
				fclose($WriteFile);
			}
			fclose($ReadFile);
		}
	}
}

if(
	$file_path
	&&
	file_exists($file_path)
){
	base64_from_image($file_path, true);
	unlink($file_path);
}