function Preview(){
	
	this.$canvas = $('#constructor_preview');
	this.$window = $(window);

	this.canvas = this.$canvas[0];
	this.context = this.canvas.getContext('2d');

	this.onChangeState = null;
	this.imageCache = {};

	this.update();

	var self = this;
	this.$window.on('resize', function( e ){
		self.update();
	})

	this.userZoom = 0.5;
	this.userX = 0.5;
	this.userY = 0.5;

	this.backgroundScaleMode = Preview.CONTAIN;
	this.userScaleMode = Preview.CONTAIN;
	
}


var resultBase64;



var move_interval;
var zoom_interval;
var approvalInterval;

var default_move_step = 0.005;
var max_move_step = 0.05;
var move_step_interval = 40;
var move_step_acceleration = 1.1;


var defaultZoomStep = 1.002;
var maxZoomStep = 1.2;
var zoomStepAcceleration = 1.004;
var zoomStepInterval = 40;

var defaultZoomOutStep = 0.999;
var maxZoomOutStep = 0.8;
var zoomOutStepAcceleration = 0.997;
var zoomOutStepInterval = 40;





function goToStep(step){
	$('.configurator .steps .step').removeClass('active');
	$('.configurator .steps .step[step='+step+']').addClass('active');
	$('.configurator .steps .step').removeClass('success');
	
	var success = true;
	$('.configurator .steps .step').each(function(){
		if( $(this).hasClass('active') ){   success = false;   }
		if( success ){      $(this).addClass('success');      }
	})

	$('.configurator .step_info').hide();
	$('.configurator .step_info'+step).fadeIn(300);
	
	if( step == 2 ){
		getConstructorColors();
	}
	
	
}


function getConstructorColors(selected_colors){
	var odezhda = $('input[name=odezhda]:checked').val();
	var params = {}
	params['odezhda'] = odezhda;
	if( selected_colors ){
		params['selected_colors'] = selected_colors;
	}
	$.post(
		"/ajax/getConstructorColors.php", params,
		function(data){
			if (data.status == 'ok'){
				$('.constructor_colors_block').html(data.html);
			}
		}, 'json'
	)
	.fail(function(data, status, xhr){
	})
	.always(function(data, status, xhr){
	})
	
}








Preview.LOADING = 'loading';
Preview.LOADED = 'loaded';
Preview.ERROR = 'error';
Preview.CONTAIN = 'contain';
Preview.COVER = 'cover';
Preview.IMAGE_SIZE_ERROR = 'image_size_error';
Preview.IMAGE_ERROR = 'image_error';
Preview.CHANGE_ZOOM = 'change_zoom';
Preview.CHANGE_POSITION = 'change_position';







Preview.prototype = {
	render: function(){
		if(!this.backgroundImage){
			return;
		}
		var backgroundImageRatio = this.backgroundImage.width / this.backgroundImage.height;

		var backgroundWidth;
		var backgroundHeight;

		if ((backgroundImageRatio > this.ratio && this.backgroundScaleMode == Preview.CONTAIN) || (backgroundImageRatio < this.ratio && this.backgroundScaleMode == Preview.COVER)) {
			backgroundWidth = this.width;
			backgroundHeight = this.width / backgroundImageRatio;
		} else {
			backgroundHeight = this.height;
			backgroundWidth = this.height * backgroundImageRatio;
		}

		var backgroundX = (this.width - backgroundWidth) / 2;
		var backgroundY = (this.height - backgroundHeight) / 2;

		this.context.clearRect(0,0,this.width, this.height);
		this.context.drawImage(this.backgroundImage,backgroundX,backgroundY,backgroundWidth,backgroundHeight);

		if(!this.userImage){
			return;
		}

		var userImageRatio = this.userImage.width / this.userImage.height;
		var userWidth;
		var userHeight;

		if ((userImageRatio > backgroundImageRatio && this.userScaleMode == Preview.CONTAIN) || (userImageRatio < backgroundImageRatio && this.userScaleMode == Preview.COVER)) {
			userWidth = backgroundWidth;
			userHeight = backgroundWidth / userImageRatio;
		} else {
			userHeight = backgroundHeight;
			userWidth = backgroundHeight * userImageRatio;
		}

		userWidth *= this.userZoom;
		userHeight *= this.userZoom;

		var userWayX = (this.width - userWidth);
		var userWayY = (this.height - userHeight);

		var userX = userWayX * this.userX
		var userY = userWayY * this.userY

		this.context.drawImage(this.userImage,userX,userY, userWidth,userHeight);
	},
	update: function(){
		this.width = this.canvas.width = this.$canvas.width();
		this.height = this.canvas.height = this.$canvas.height();
		this.ratio = this.width / this.height;

		this.render();
	},
	setBackground: function( src ){
		var cachedImage = this.imageCache[src];

		if(cachedImage){
			this.backgroundImage = cachedImage;
			this.render();
			return;
		}

		var self = this;

		var formData = new FormData();
		formData.append('url', src);
		
		process(true);

		$.ajax({
			url: 'http://109.120.140.6/constructor_api/update_file.php',
			type: 'POST',
			data: formData,
			processData: false,
			contentType: false,
			success: function(data){
				self.backgroundImage = new Image;
				self.backgroundImage.crossOrigin = 'Anonymous';
				self.backgroundImage.setAttribute('crossOrigin', 'anonymous');

				self.backgroundImage.onload = function(){
					self.imageCache[src] = this;
					self.onChangeState && self.onChangeState.call(null, Preview.LOADED)
					self.render();
				}
				self.backgroundImage.src = data;
				
				if( $('.configurator input[name=printBase64]').length > 0 ){
					var base64 = $('.configurator input[name=printBase64]').val();
					var x = $('.configurator input[name=x]').val();
					var y = $('.configurator input[name=y]').val();
					var zoom = $('.configurator input[name=zoom]').val();
					if(
						base64.length > 0
						&&
						x.length > 0
						&&
						y.length > 0
					){
						self.userImage = new Image;
						self.userImage.crossOrigin='Anonymous';
						self.userImage.setAttribute('crossOrigin', 'anonymous');
						self.userImage.onload = function(){
							self.onChangeState && self.onChangeState.call(null, Preview.LOADED)
							self.render();
						}
						self.userImage.src = base64;
						
						self.setPosition(x, y);
						self.setZoom( zoom )
						
					}
				}
				
			},
			error: function(data) {
				self.onChangeState && self.onChangeState.call(null, Preview.IMAGE_ERROR)
			},
			always: function(data) {
			},
		});

		this.onChangeState && this.onChangeState.call(null, Preview.LOADING)
	},
	changeBackground: function( base64, color_id ){
		var self = this;
		self.backgroundImage = new Image;
		self.backgroundImage.crossOrigin = 'Anonymous';
		self.backgroundImage.setAttribute('crossOrigin', 'anonymous');
		self.backgroundImage.onload = function(){
			self.render();
			var resBase64 = self.generateImage();
			$('input.picBase64[color_id='+color_id+']').val(resBase64);
			
			var emptyInput = self.emptyInput();
			if( emptyInput ){
				var col_id = $(emptyInput).attr('color_id');
				var b64 = $(emptyInput).val();
				self.changeBackground( b64, col_id );
			}
			
		}
		self.backgroundImage.src = base64;
	},
	generateImage: function(){
		// TODO: Scale up canvas
		return this.canvas.toDataURL();
	},
	getUserImage: function(){
		return this.userImage;
	},
	getUserImageAsBase64: function(){
		if(!this.userImage){
			return '';
		}
		var canvas = document.createElements('canvas');
		var context = canvas.getContext('2d');
		canvas.width = this.userImage.width;
		canvas.height = this.userImage.height;
		context.drawImage(this.userImage, 0, 0);
		return canvas.toDataURL();
	},
	
	uploadPrintImage: function( file_id ){
		var self = this;
		
		$('.configurator p.error___p').html('');

		var formData = new FormData();
		formData.append('file_id', file_id);
		
		process(true);
		
		$.ajax({
			url: 'http://109.120.140.6/constructor_api/getImageBase64.php',
			type: 'POST',
			data: formData,
			processData: false,
			contentType: false,
			success: function(data) {
				self.userImage = new Image;
				self.userImage.crossOrigin='Anonymous';
				self.userImage.setAttribute('crossOrigin', 'anonymous');
				self.userImage.onload = function(){
					self.onChangeState && self.onChangeState.call(null, Preview.LOADED)
					$('.constructor_buttons_block').show();
					self.render();
				}
				self.userImage.src = data;
				
			},
			error: function(data) {
				self.onChangeState && self.onChangeState.call(null, Preview.IMAGE_ERROR)
			},
			always: function(data) {
			},
		});

		this.onChangeState && this.onChangeState.call(null, Preview.LOADING)
	},
	
	uploadUserImage: function( file ){
		var self = this;
		
		var formData = new FormData();
		formData.append('file', file);
		
		process(true);

		$.ajax({
			url: 'http://109.120.140.6/constructor_api/update_file.php',
			type: 'POST',
			data: formData,
			processData: false,
			contentType: false,
			success: function(data) {
				self.userImage = new Image;
				self.userImage.crossOrigin='Anonymous';
				self.userImage.setAttribute('crossOrigin', 'anonymous');

				self.userImage.onload = function(){
					if(this.width < 1000 || this.height < 1000){
						self.userImage = null;
						self.onChangeState && self.onChangeState.call(null, Preview.IMAGE_SIZE_ERROR)
					} else {
						self.onChangeState && self.onChangeState.call(null, Preview.LOADED)
					}
					self.render();
				}
				self.userImage.src = data;
			},
			error: function(data) {
				self.onChangeState && self.onChangeState.call(null, Preview.IMAGE_ERROR)
			},
			always: function(data) {
			},
		});

		this.onChangeState && this.onChangeState.call(null, Preview.LOADING)
	},
	clearUserImage: function(){
		this.userImage = null;
		this.render();
		$('input[name=print]').prop('checked', false);
	},
	zoomIn: function(zoomStep){
		var newZoom = this.userZoom * zoomStep;
		newZoom = newZoom > 1 ? 1 : newZoom;
		if(this.userZoom != newZoom){
			var self = this;
			TweenMax.to(this, 0.15, {userZoom: newZoom, onUpdate: function(){
				self.render();
			}, onComplete: function(){
				self.onChangeState && self.onChangeState.call(null, Preview.CHANGE_ZOOM)
			}})
		}
	},
	zoomOut: function(zoomOutStep){
		var newZoom = this.userZoom * zoomOutStep;
		newZoom = newZoom < 0.1 ? 0.1 : newZoom;

		if(this.userZoom != newZoom){
			var self = this;
			TweenMax.to(this, 0.15, {userZoom: newZoom, onUpdate: function(){
				self.render();
			}, onComplete: function(){
				self.onChangeState && self.onChangeState.call(null, Preview.CHANGE_ZOOM)
			}})
		}
	},
	setZoom: function( zoom, immediate ){
		var self = this;
		TweenMax.to(this, immediate ? 0 : 0.15, {userZoom: zoom, onUpdate: function(){
			self.render();
		}, onComplete: function(){
			self.onChangeState && self.onChangeState.call(null, Preview.CHANGE_ZOOM)
		}})
	},
	move: function(addX, addY, immediate){
		var newX = this.userX + addX;
		var newY = this.userY + addY;
		newX = newX > 1 ? 1 : newX;
		newX = newX < 0 ? 0 : newX;
		newY = newY > 1 ? 1 : newY;
		newY = newY < 0 ? 0 : newY;

		var self = this;
		TweenMax.to(this, immediate ? 0 : 0.15, {userX: newX, userY: newY, onUpdate: function(){
			self.render();
		}, onComplete: function(){
			self.onChangeState && self.onChangeState.call(null, Preview.CHANGE_POSITION)
		}})
		this.render();
	},
	setPosition: function(x, y){
		var newX = x;
		var newY = y;
		newX = newX > 1 ? 1 : newX;
		newX = newX < 0 ? 0 : newX;
		newY = newY > 1 ? 1 : newY;
		newY = newY < 0 ? 0 : newY;

		var self = this;
		TweenMax.to(this, 0.15, {userX: newX, userY: newY, onUpdate: function(){
			self.render();
		}, onComplete: function(){
			self.onChangeState && self.onChangeState.call(null, Preview.CHANGE_POSITION)
		}})
		this.render();
	},
	emptyInput: function(){
		var input = false;
		if( $('input.picBase64').length > 0 ){
			var k = -1;
			$('input.picBase64').each(function(){ k++;
				var val = $(this).val();
				var color_id = $(this).attr('color_id');
				if( val.length > 0 ){} else if( !input ){
					input = $('input.item___pics[color_id='+color_id+']');
				}
			})
		}
		return input;
	}
}











$(function(){
	


	
	
	var preview = new Preview();

	var $upload = $('#upload');
	var $generate = $('#generate');
	var $setBackgrounds = $('.set-background');
	var $generateResult = $('#generate-result');
	var $zoomIn = $('#zoom-in');
	var $zoomOut = $('#zoom-out'); 

	var $moveLeft = $('#move-left'); 
	var $moveRight = $('#move-right'); 
	var $moveUp = $('#move-up'); 
	var $moveDown = $('#move-down');

	var $clearUserImage = $('#clear-user-image');
	
	
	

	$setBackgrounds.on('click', function( e ){
		//e.preventDefault();
		$('.configurator p.error___p').html('');
		preview.setBackground( $(this).attr('data-src') );
	})
	$generate.on('click', function( e ){
		var result = preview.generateImage();
		$generateResult.val(result);
		//console.log(result);
	})

	$('input[name=print]').on('change', function( e ){
		e.preventDefault();
		preview.uploadPrintImage($(this).val());
	})
	
	$upload.on('change', function( e ){
		e.preventDefault();
		preview.uploadUserImage(e.target.files[0]);
	})

	
	
	var zoomStep = defaultZoomStep;
	$zoomIn.on('mousedown', function( e ){
		e.preventDefault();
		zoom_interval = setInterval(function(){
			if( zoomStep < maxZoomStep ){
				zoomStep = zoomStep * zoomStepAcceleration;
			}
			preview.zoomIn(zoomStep);
		}, zoomStepInterval);
	})
	$zoomIn.on('mouseup', function( e ){
		zoomStep = defaultZoomStep;
		clearInterval(zoom_interval);
	})
	
	
	
	var zoomOutStep = defaultZoomOutStep;
	$zoomOut.on('mousedown', function( e ){
		e.preventDefault();
		zoom_interval = setInterval(function(){
			if( zoomOutStep > maxZoomOutStep ){
				zoomOutStep = zoomOutStep * zoomOutStepAcceleration;
			}
			preview.zoomOut(zoomOutStep);
		}, zoomStepInterval);
	})
	$zoomOut.on('mouseup', function( e ){
		zoomOutStep = defaultZoomOutStep;
		clearInterval(zoom_interval);
	})
	
	
	

	
	
	
	
	var moveStep = default_move_step;
	$moveLeft.on('mousedown', function( e ){
		if( !is_process(this) ){
			e.preventDefault();
			move_interval = setInterval(function(){
				if( moveStep < max_move_step ){
					moveStep = moveStep * move_step_acceleration;
				}
				preview.move(-moveStep, 0)
			}, move_step_interval);
		}
	})
	$moveLeft.on('mouseup', function( e ){
		if( !is_process(this) ){
			moveStep = default_move_step;
			clearInterval(move_interval);
		}
	})
	
	
	$moveRight.on('mousedown', function( e ){
		if( !is_process(this) ){
			e.preventDefault();
			move_interval = setInterval(function(){
				if( moveStep < max_move_step ){
					moveStep = moveStep * move_step_acceleration;
				}
				preview.move(moveStep, 0)
			}, move_step_interval);
		}
	})
	$moveRight.on('mouseup', function( e ){
		if( !is_process(this) ){
			moveStep = default_move_step;
			clearInterval(move_interval);
		}
	})

	
	$moveUp.on('mousedown', function( e ){
		if( !is_process(this) ){
			e.preventDefault();
			move_interval = setInterval(function(){
				if( moveStep < max_move_step ){
					moveStep = moveStep * move_step_acceleration;
				}
				preview.move(0, -moveStep)
			}, move_step_interval);
		}
	})
	$moveUp.on('mouseup', function( e ){
		if( !is_process(this) ){
			moveStep = default_move_step;
			clearInterval(move_interval);
		}
	})

	
	$moveDown.on('mousedown', function( e ){
		if( !is_process(this) ){
			e.preventDefault();
			move_interval = setInterval(function(){
				if( moveStep < max_move_step ){
					moveStep = moveStep * move_step_acceleration;
				}
				preview.move(0, moveStep)
			}, move_step_interval);
		}
	})
	$moveDown.on('mouseup', function( e ){
		if( !is_process(this) ){
			moveStep = default_move_step;
			clearInterval(move_interval);
		}
	})
	
	

	$clearUserImage.on('click', function( e ){
		e.preventDefault();
		$('.constructor_buttons_block').hide();
		$('.configurator p.error___p').html('');
		preview.clearUserImage();
	})

	
	
	$('.configurator .set-background').each(function(){
		var for_attr = $(this).attr('for');
		var input = $('.configurator input#'+for_attr)
		if( $(input).prop('checked') ){
			preview.setBackground( $(this).attr('data-src') );
		}
	})
	//preview.setBackground($setBackgrounds.first().attr('data-src'));

	
	
	preview.onChangeState = function( state ){
		//console.log(state)
		$('.configurator input[name=x]').val(preview.userX);
		$('.configurator input[name=y]').val(preview.userY);
		$('.configurator input[name=zoom]').val(preview.userZoom);
		if(state == Preview.LOADING){
			//console.log('Блокируем контролы во время загрузки изображения')
			process(true);
		} else if(state == Preview.CHANGE_ZOOM){
			//console.log(preview.userZoom)
		} else if(state == Preview.CHANGE_POSITION){
			//console.log(preview.userX, preview.userY)
		} else {
			//console.log('Разблокируем контролы');
			process(false);
		}
		if(state == Preview.IMAGE_SIZE_ERROR){
			//alert('Размер изображения должен быть больше 1000 на 1000 пикселей!')
		}
	}
	
	
	
	
	
	
	
	
	// Следующий шаг (создание)
	$(document).on('click', '.configurator .next_link_add', function(){
		if( !is_process(this) ){
			
			$('.configurator p.error___p').html('');
			
			var step = Number( $(this).attr('step') );
			var next_step = step + 1;
			
			var odezhda = $('.configurator input[name=odezhda]:checked').val();
			var print = $('.configurator input[name=print]:checked').val();
			var print_id = $('.configurator input[name=print]:checked').attr('print_id');
			var print_name = $('.configurator input[name=print]:checked').attr('print_name');
			var error = false;
			
			if( step == 1 && !odezhda ){
				error = 'Выберите, пожалуйста, вид одежды!';
			} else if( step == 2 && !print ){
				error = 'Выберите, пожалуйста, принт для наложения!';
			}
			
			if( !error ){
				
				if( step == 2 ){
					$('.configurator .print_name_span').html(print_name+' (ID '+print_id+')');
				}
				
				goToStep(next_step);
			} else {
				$('.configurator p.error___p').html(error);
			}
		}
	})
	
	
	
	// Следующий шаг (утверждение)
	$(document).on('click', '.configurator .next_link_approval', function(){
		if( !is_process(this) ){
			
			$('.configurator p.error___p').html('');
			
			var step = Number( $(this).attr('step') );
			var next_step = step + 1;
			
			var odezhda = $('.configurator input[name=odezhda]:checked').val();
			var print_id = $('.configurator input[name=print_id]').val();
			var error = false;
			
			if( step == 1 && !odezhda ){
				error = 'Ошибка выбора вида одежды';
			} else if( step == 2 && !print_id ){
				error = 'Ошибка выбора принта';
			}
			
			if( !error ){
				goToStep(next_step);
			} else {
				$('.configurator p.error___p').html(error);
			}
		}
	})
	
	
	
	
	
	// Смена шага
	$(document).on('click', '.configurator .steps .step', function(){
		if( $(this).hasClass('success') ){
			var step = Number( $(this).attr('step') );
			goToStep(step);
		}
    })
	
	
	
	
	
	// Отправка
	$(document).on('click', '.addConstructorButton', function(){
		if( !is_process(this) ){
			$('.configurator p.error___p').html('');
			var colors_cnt = $('.configurator input[name=color]:checked').length;
			var collections_cnt = $('.configurator input[name=collection]:checked').length;
			var odezhda = $('.configurator input[name=odezhda]:checked').val();
			var print = $('.configurator input[name=print]:checked').val();
			var x = $('.configurator input[name=x]').val();
			var y = $('.configurator input[name=y]').val();
			var zoom = $('.configurator input[name=zoom]').val();
			var name = $('.configurator input[name=name]').val();
			var section = $('.configurator input[name=section]:checked').val();
			
			var error = false;
			
			if( !odezhda ){
				error = 'Выберите, пожалуйста, вид одежды!';
			} else if( !print ){
				error = 'Выберите, пожалуйста, принт для наложения!';
			/*} else if( name.length == 0 ){
				error = 'Придумайте, пожалуйста, название для нового товара!';*/
			} else if( colors_cnt == 0 ){
				error = 'Выберите, пожалуйста, минимум 1 цвет для товара!';
			} else if( collections_cnt == 0 ){
				error = 'Выберите, пожалуйста, коллекцию для товара!';
			}
			if( !error ){
				process(true);
				
				var colors = [];
				$('.configurator input[name=color]:checked').each(function(){
					colors.push( $(this).val() );
				})
				colors = colors.join('|');
				
				var collections = [];
				$('.configurator input[name=collection]:checked').each(function(){
					collections.push( $(this).val() );
				})
				collections = collections.join('|');
				
				var print_id = $('.configurator input[name=print]:checked').attr('print_id');
				
				var tags = []
				if( $('.tag___item').length > 0 ){
					$('.tag___item').each(function(){
						var t = $.trim( $(this).html() );
						tags.push(t);
					})
				}
				tags = tags.join('|');
				
				$.post(
					"/ajax/newConstructorItem.php",
					{
						odezhda:odezhda,
						print_img:print,
						print_id:print_id,
						colors:colors,
						collections:collections,
						x:x,
						y:y,
						zoom:zoom,
						//name:name,
						section:section,
						tags:tags
					},
					function(data){
						if (data.status == 'ok'){
							window.location.href = document.URL;
						} else if (data.status == 'error'){
							process(false);
							$('.configurator p.error___p').html(data.text);
						}
					}, 'json'
				)
				.fail(function(data, status, xhr){
					process(false);
					$('.configurator p.error___p').html('Ошибка запроса');
				})
				.always(function(data, status, xhr){
				})
			} else {
				$('.configurator p.error___p').html(error);
			}
		}
	})
	
	
	
	
	
	// Добавление нового тега в список
	$(document).on('keyup', 'input[name=constructor_tag]', function(e){
		if( e.keyCode == 13 ){
			var tag = $(this).val();
			tag = $.trim(tag)
			if( tag.length == 0 ){
				errorWindow( 'Вы ничего не ввели' );
			} else {
				if( $('.tag___item').length <= 25 ){
					var has = false;
					$('.constructor___tags .grid a').each(function(){
						var t = $(this).html();
						if(tag == t){   has = true;   }
					})
					if( !has ){
						$('.constructor___tags .grid').append('<a class="tag___item">'+tag+'</a>');
						$(this).val('');
					} else {
						errorWindow( 'Такой тег уже есть' );
					}
				} else {
					errorWindow( 'Вы можете добавить не более 25 тегов' );
				}
			}
		}
	})
	
	// Удаление тега из списка
	$(document).on('click', '.tag___item', function(e){
		$(this).remove();
	})
	
	
	
	
	
	// Согласование
	$(document).on('click', '.approvalConstructorButton', function(){
		if( !is_process(this) ){
			
			var button = $(this);
			
			$('.configurator p.error___p').html('');
			var colors_cnt = $('.configurator input[name=color]:checked').length;
			var collections_cnt = $('.configurator input[name=collection]:checked').length;
			var odezhda = $('.configurator input[name=odezhda]:checked').val();
			var print_id = $('.configurator input[name=print_id]').val();
			var x = $('.configurator input[name=x]').val();
			var y = $('.configurator input[name=y]').val();
			var zoom = $('.configurator input[name=zoom]').val();
			//var name = $('.configurator input[name=name]').val();
			
			var price = $('.configurator input[name=price]').val();
			var section = $('.configurator input[name=section]:checked').val();
			var item_id = $('.configurator input[name=item_id]').val();
			var user_id = $('.configurator input[name=user_id]').val();
			
			var print_name = false;
			if( $('.configurator input[name=print_name]:visible').length > 0 ){
				print_name = $('.configurator input[name=print_name]:visible').val();
			}
			
			
			var error = false;
			
			if( !odezhda ){
				error = 'Выберите, пожалуйста, вид одежды!';
			} else if( !print_id ){
				error = 'Выберите, пожалуйста, принт для наложения!';
			/*} else if( name.length == 0 ){
				error = 'Придумайте, пожалуйста, название для нового товара!';*/
			} else if( 
				$('.configurator input[name=print_name]:visible').length > 0
				&&
				print_name.length == 0
			){
				error = 'Введите, пожалуйста, новое название принта!';
			} else if( price.length == 0 ){
				error = 'Укажите, пожалуйста, цену для нового товара!';
			} else if( colors_cnt == 0 ){
				error = 'Выберите, пожалуйста, минимум 1 цвет для товара!';
			} else if( collections_cnt == 0 ){
				error = 'Выберите, пожалуйста, коллекцию для товара!';
			}
			if( !error ){
				
				var n = noty({
					text        : '<b>Внимание!</b><br>Будет создан товар с торговыми предложениями!<br><br>Вы уверены, что хотите согласовать эту заявку?',
					type        : 'alert',
					dismissQueue: true,
					layout      : 'center',
					theme       : 'defaultTheme',
					buttons     : [
						{addClass: 'btn btn-primary', text: 'Да', onClick: function ($noty) {
							$noty.close();
				
				
							process(true);

							var colors = [];
							$('.configurator input[name=color]:checked').each(function(){
								colors.push( $(this).val() );
							})
							colors = colors.join('|');
							
							var collections = [];
							$('.configurator input[name=collection]:checked').each(function(){
								collections.push( $(this).val() );
							})
							collections = collections.join('|');
							
							var tags = []
							if( $('.tag___item').length > 0 ){
								$('.tag___item').each(function(){
									var t = $.trim( $(this).html() );
									tags.push(t);
								})
							}
							tags = tags.join('|');
							
							// Получаем base64 всех цветов
							var emptyInput = preview.emptyInput();
							if( emptyInput ){
								var color_id = $(emptyInput).attr('color_id');
								var base64 = $(emptyInput).val();
								preview.changeBackground( base64, color_id );
							}
							
							clearInterval(approvalInterval);
							approvalInterval = setInterval(function(){
								if( !preview.emptyInput() ){
									clearInterval(approvalInterval);
									
									var id = $('input[name=odezhda]:checked').attr('id');
									var src = $('label[for='+id+']').attr('data-src')
									preview.setBackground( src );
									
									var resultBase64 = {};
									$('.picBase64').each(function(){
										var color_id = $(this).attr('color_id');
										var base64 = $(this).val();
										resultBase64[color_id] = base64;
									})
									
									$(button).html('Ожидание...');
									
									$.post(
										"/ajax/approvalConstructorItem.php",
										{
											item_id:item_id,
											odezhda:odezhda,
											print_id:print_id,
											colors:colors,
											collections:collections,
											x:x,
											y:y,
											zoom:zoom,
											name:name,
											price:price,
											section:section,
											user_id:user_id,
											resultBase64:resultBase64,
											tags:tags,
											print_name:print_name
										},
										function(data){
											if (data.status == 'ok'){
												window.location.href = '/personal_approval/';
											} else if (data.status == 'error'){
												process(false);
												$('.configurator p.error___p').html(data.text);
												$(button).html('Согласовать');
											}
										}, 'json'
									)
									.fail(function(data, status, xhr){
										process(false);
										$('.configurator p.error___p').html('Ошибка запроса');
										$(button).html('Согласовать');
									})
									.always(function(data, status, xhr){
										
									})
								
								}
							}, 300);
							
							
						}},
						{addClass: 'btn btn-danger', text: 'Нет', onClick: function ($noty) { $noty.close(); } }
					]
				});
				

			} else {
				$('.configurator p.error___p').html(error);
			}
			
		}
	})
	
	
	
	
	
	
	// Отклонение
	$(document).on('click', '.cancelConstructorButton', function(){
		if( !is_process(this) ){
			
			var button = $(this);
			
			var n = noty({
				text        : '<b>Внимание!</b><br>Вы уверены, что хотите отклонить заявку?',
				type        : 'alert',
				dismissQueue: true,
				layout      : 'center',
				theme       : 'defaultTheme',
				buttons     : [
					{addClass: 'btn btn-primary', text: 'Да', onClick: function ($noty) {
						$noty.close();
						
						$(button).html('Ожидание...');
						
						$('.configurator p.error___p').html('');
						var item_id = $('.configurator input[name=item_id]').val();
						
						var error = false;
						
						if( !error ){
							process(true);
							
							$.post(
								"/ajax/ajax.php",
								{
									action: 'cancelConstructorItem',
									item_id:item_id,
								},
								function(data){
									if (data.status == 'ok'){
										window.location.href = '/personal_approval/';
									} else if (data.status == 'error'){
										process(false);
										$('.configurator p.error___p').html(data.text);
										$(button).html('Согласовать');
									}
								}, 'json'
							)
							.fail(function(data, status, xhr){
								process(false);
								$('.configurator p.error___p').html('Ошибка запроса');
								$(button).html('Согласовать');
							})
							.always(function(data, status, xhr){
								
							})
						} else {
							$('.configurator p.error___p').html(error);
						}
						
					}},
					{addClass: 'btn btn-danger', text: 'Нет', onClick: function ($noty) { $noty.close(); } }
				]
			});
			
		}
	})
	
	
	
	
	
	// Поиск по коллекциям
	$(document).on('keyup', 'input[name=constr_coll_search]', function(){
		var r = $(this).val();
		r = r.toLowerCase();
		if( r.length > 0 ){
			$('.collections .scroll input[type=radio]').each(function(){
				var title = $(this).attr('coll_value');
				// нет вхождения
				if( title.indexOf(r) == -1 && !$(this).prop('checked') ){
					$(this).parent('div.line').hide();
				// есть вхождение
				} else {
					$(this).parent('div.line').show();
				}
			})
		} else {
			$('.collections .scroll div.line').show();
		}
	})
	
	
	
	
	
	$(document).on('change', 'input[name=update_print_name]', function(){
		var val = $('input[name=update_print_name]:checked').val();
		if( val == 1 ){
			$('.update_print_name_block').show();
		} else {
			$('.update_print_name_block').hide();
		}
		$('.error___p').html('');
	})
	
	
	
	// Поиск по коллекциям
	$(document).on('keyup', 'input[name=price]', function(){
		minus_zero($(this));
		$('.error___p').html('');
	})
	$(document).on('blur', 'input[name=price]', function(){
		minus_zero($(this));
	})
	
	
	
	
})