
var fileInputName = 'printPhotoInput';

// Загрузка фото
function printOnResponse(d) {
	eval('var obj = ' + d + ';');
	if (obj.status == 'ok'){
		
		process(true);
		$.post("/ajax/ajax.php", {action:"getPrintPhotoBlock", fid:obj.fid}, function(data) {
			if (data.status == 'ok'){
				// вставляем фото
				$('.printPhotoSelect').replaceWith(data.html);
				// Очищаем файловый инпут
				$('input[name='+fileInputName+']').val('');
			}
		}, 'json')
		.fail(function(data, status, xhr){
			show_error( $('form.'+form_class) , 'no___field', 'Ошибка запроса');
		})
		.always(function(data, status, xhr){
			process(false);
		})
	} else{
		// Вывод ошибки
		//var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: obj.status});
		
		errorWindow( obj.status );
	}
}


$(document).ready(function(){



	// Загрузка фото
	$(document).on('click', '.printPhotoSelect', function(){
		// Форма
		var this_form = $(this).parents('form');
		$(this_form).find('p.error___p').html('');
		$('input[name='+fileInputName+']').val('');
		$('input[name='+fileInputName+']').trigger('click');
	})
	$(document).on('change', 'input[name='+fileInputName+']', function(){
		$(".printUpload___form").submit();
	})

	

	// Создание принта
	$(document).on('click', '.addPrintButton', function(){
		if (!is_process(this)){
			// Форма
			var this_form = $(this).parents('form');
			// Подготовка
			$(this_form).find("input[type=text], input[type=email], input[type=password], textarea").removeClass("is___error");
			$(this_form).find('p.error___p').html('');
			// Задаём переменные
			var print_name = $(this_form).find("input[name=print_name]").val();
			var photo_id = $(this_form).find("input[name=photo_id]").length > 0?$(this_form).find("input[name=photo_id]").val():false;
			var error = false;
			// Проверки
			if (print_name.length == 0){
				error = ['print_name',   'Введите, пожалуйста, название принта!'];
			} else if ( !photo_id || (photo_id && photo_id.length == 0) ){
				error = ['photo_id',   'Выберите фото для принта!'];
			}
			// Если нет ошибок
			if (!error){
				process(true);
				var form_data = $(this_form).serialize();
				$.post("/ajax/ajax.php", {action:"addPrint", form_data:form_data}, function(data){
					if (data.status == 'ok'){
						//var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'topRight', type: 'success', text: "Принт успешно создан!"});
						$(this_form).find("input[type=text], input[type=email], input[type=password], textarea").val('');
						
						successWindow( "Принт успешно создан!" );
						
						$('.printPhotoSelect').replaceWith(data.html);
						setTimeout(function(){
							window.location.href = document.URL;
						}, 1700)
					} else if (data.status == 'error'){
						show_error(this_form, false, data.text);
					}
				}, 'json')
				.fail(function(data, status, xhr){
				})
				.always(function(data, status, xhr){
					process(false);
				})
			// Ошибка
			} else {
				show_error(this_form, error[0], error[1]);
			}
		}
	})
	
	
	
	

});