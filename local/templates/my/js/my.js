var promo_interval
var quantity_interval
var loc_interval
var location_zip_interval
var filter_checkbox_interval

var coupon_changed
var zip_process = false

var autocomplete_dss = [3, 4]


$(document).ready(function () {

	//setFavorites();


	product_detail_info()

	open_left_menu()

	$.cookie('go_to_basket', 0, {path: '/'})


	$('.promo_banners_slider_item').show()


	$(document).on('click', '.dropped_menu .filterCollectionGroup', function () {
		$(this).parents('.dropped_menu').toggleClass('drop_active')
	})


	// Открытие левого меню
	$(document).on('click', '.left_menu_link', function (e) {
		$('.left_menu_link').removeClass('active')
		$('.left_sections_ul').hide()
		$(this).parent('li').find('.left_sections_ul').show()
		$(this).addClass('active')
	})


	var isMain = $('input[name=isMain]').val()

	var genderType = $('input[name=genderType]').val()
	if (isMain == 'Y') {
		history.replaceState({}, null, '/' + genderType + '_home/')
	}


	// Смена пола в шапке
	$(document).on('click', '.gender___type_link', function (e) {
		if (!is_process(this)) {
			var gender_type = $(this).attr('gender_type')
			$.post('/ajax/ajax.php', {action: 'change_gender_type', gender_type: gender_type}, function (data) {
				if (data.status == 'ok') {
					window.location.href = '/'
				}
			}, 'json')
		}
	})


	// Смена сортировки в каталоге
	$(document).on('change', 'select[name=sort]', function (e) {
		var sort = $(this).val()
		$.cookie('BITRIX_SM_sort', sort, {expires: 7, path: '/'})
		window.location.href = document.URL
	})


	// Лайк за иллюстратора
	$(document).on('click', '.user_like___button', function () {
		var user_id = $(this).attr('user_id')
		var button = $(this)
		process(true);
		$.post('/ajax/ajax.php', {action: 'user_like', user_id: user_id}, function (data) {
			if (data.status == 'ok') {
				$(button).find('span').html(data.likes)
			} else if (data.status == 'no') {
				errorWindow('Вы уже голосовали за этого иллюстратора')
			} else if (data.status == 'error') {
				errorWindow('Ошибка запроса...')
			}
		}, 'json')
		.fail(function (data, status, xhr) {
			errorWindow('Ошибка запроса...')
		})
		.always(function (data, status, xhr) {
			process(false)
		})
	})


	// Выбор типа плательщика в заказе
	$(document).on('change', 'input[name=payer_type]', function () {
		var payer_type = $(this).val()
		$('.order___form').hide()
		$('.order_form_' + payer_type).show()
		$('.checkout_links').show()
	})


	// Оформление заказа (переход на 3 шаг)
	$(document).on('click', '.to_step_3_button', function () {
		// Форма
		var this_form = $('form.order___form:visible')

		// Подготовка
		$(this_form).find('input[type=text], input[type=tel], input[type=email], input[type=password], textarea').removeClass('error')
		$(this_form).find('p.error___p').html('')

		// Задаём переменные
		var user_pay_type = $(this_form).find('input[name=user_pay_type]').val()
		var error = false

		var NAME = $(this_form).find('input[name=NAME]').val()
		var LAST_NAME = $(this_form).find('input[name=LAST_NAME]').val()
		var EMAIL = $(this_form).find('input[name=EMAIL]').val()
		var PERSONAL_PHONE = $(this_form).find('input[name=PERSONAL_PHONE]').val()
		var LOCATION_NAME = $(this_form).find('input[name=LOCATION_NAME]').val()
		var LOCATION_ID = $(this_form).find('input[name=LOCATION_ID]').val()
		var ds = $(this_form).find('input[name=ds]:checked').val()
		var LOCATION_ZIP = $(this_form).find('input[name=LOCATION_ZIP]').val()
		var STREET = $(this_form).find('input[name=STREET]').val()
		var HOUSE = $(this_form).find('input[name=HOUSE]').val()
		var order_pvz_code = $(this_form).find('input[name=order_pvz_code]').val()

		// Проверки
		if (NAME.length == 0) {
			error = ['NAME', 'Введите, пожалуйста, Ваше имя!']
		} else if (LAST_NAME.length == 0) {
			error = ['LAST_NAME', 'Введите, пожалуйста, Вашу фамилию!']
		} else if (EMAIL.length == 0) {
			error = ['EMAIL', 'Введите, пожалуйста, Ваш Email!']
		} else if (EMAIL.length > 0 && !(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(EMAIL))) {
			error = ['EMAIL', 'Email содержит ошибки!']
		} else if (PERSONAL_PHONE.length == 0) {
			error = ['PERSONAL_PHONE', 'Введите, пожалуйста, Ваш телефон!']
		} else if (LOCATION_ID.length == 0) {
			error = ['LOCATION_ID', 'Выберите, пожалуйста, Ваш населённый пункт!']
		} else if (LOCATION_ZIP.length == 0) {
			error = ['LOCATION_ZIP', 'Введите, пожалуйста, Ваш почтовый индекс!']
		} else if (!ds) {
			error = ['ds', 'Выберите, пожалуйста, вариант доставки!']
		} else if ($('.order_address_block:visible').length > 0) {
			if (STREET.length == 0) {
				error = ['STREET', 'Введите, пожалуйста, Вашу улицу!']
			} else if (HOUSE.length == 0) {
				error = ['HOUSE', 'Введите, пожалуйста, номер Вашего дома!']
			}
			// Boxberry ПВЗ
		} else if (ds == 5) {
			if (order_pvz_code.length == 0) {
				error = ['order_pvz_code', 'Выберите на карте подходящий пункт выдачи заказа']
			}
		}

		// Если нет ошибок
		if (!error) {
			$(this_form).submit()
			// Ошибка
		} else {
			show_error(this_form, error[0], error[1])
		}

	})


	// РАБОТА С ДОСТАВКОЙ

	$(document).on('change', 'input[name=ds]', function (e) {
		$('form.order___form:visible p.error___p').html('')
		$('form.order___form:visible input').removeClass('error')
		var price = false
		var input = $('input[name=ds]:checked')
		if (input.length > 0) {
			if ($(input)[0].hasAttribute('price')) {
				price = $(input).attr('price')
			}
		}
		$('input[name=DELIVERY_PRICE]').val(price)
		recalcRightBasket()
		///////
		map_init()
	})

	$(document).on('click', '.pvz___button', function (e) {
		if (!is_process(this)) {
			var pvz_code = $(this).attr('code')
			var pvz_address = $(this).attr('address')
			$('input[name=DELIVERY_PRICE]').val('')
			calcPVZprice(pvz_code, pvz_address)
		}
	})

	$(document).on('keyup', 'input[name=LOCATION_NAME]', function (e) {
		$(this).attr('LOCATION_NAME', '')
		$('input[name=LOCATION_ID]').val('')
		$('input[name=LOCATION_ZIP]').val('')

		$('.order_delivery_block').html('')
		$('.order_address_block').hide()
		if (orderMap) {
			orderMap.destroy()
			orderMap = null
		}
	})
	$(document).on('focus', 'input[name=LOCATION_NAME]', function () {
		var LOCATION_NAME = $(this).val()
		var LOCATION_ID = $('input[name=LOCATION_ID]').val()
		if (LOCATION_ID.length > 0) {
			$(this).attr('location_name', LOCATION_NAME)
		} else {
			$(this).attr('location_name', '')
		}
		$(this).val('')
	})
	$(document).on('focusout', 'input[name=LOCATION_NAME]', function () {
		var val = $(this).val()
		var LOCATION_NAME = $(this).attr('location_name')
		var LOCATION_ID = $('input[name=LOCATION_ID]').val()
		if (val.length == 0 && LOCATION_NAME.length > 0 && LOCATION_ID.length > 0) {
			$(this).val(LOCATION_NAME)
		}
		$(this).attr('location_name', '')
	})
	$('input[name=LOCATION_NAME]').autocomplete({
		source: '/ajax/loc_search.php',
		minLength: 3,
		delay: 600,
		select: function (event, ui) {
			var zip = ui.item.zip
			$('input[name=LOCATION_NAME]').val(ui.item.label)
			$('input[name=LOCATION_ID]').val(ui.item.value)
			$('input[name=LOCATION_ZIP]').val(zip)
			$('input[name=LOCATION_ZIP]').attr('old_value', zip)
			setTimeout(function () {
				$('input[name=LOCATION_NAME]').blur()
			}, 100)
			if (zip && zip.length > 0) {
				getDeliveryVariants(ui.item.value, zip)
			} else {
				$('.order_delivery_block').html('')
				$('.order_address_block').hide()
			}
			return false
		}
	})
	$(document).on('keyup', 'input[name=LOCATION_ZIP]', function () {
		if (!zip_process) {
			var input = $(this)
			var LOCATION_ZIP = $(input).val()
			var LOCATION_ZIP_OLD = $(input).attr('old_value')
			var LOCATION_ID = $('input[name=LOCATION_ID]').val()
			clearInterval(location_zip_interval)
			location_zip_interval = setInterval(function () {
				clearInterval(location_zip_interval)

				if (
					LOCATION_ZIP.length > 0
					&&
					LOCATION_ZIP != LOCATION_ZIP_OLD
					&&
					LOCATION_ID.length > 0
				) {

					$('input[name=LOCATION_ZIP]').attr('old_value', LOCATION_ZIP)
					$('.order_delivery_block').html('')
					$('.order_address_block').hide()
					if (orderMap) {
						orderMap.destroy()
						orderMap = null
					}
					getDeliveryVariants(LOCATION_ID, LOCATION_ZIP)

				} else if (LOCATION_ZIP.length == 0) {

					$('input[name=LOCATION_ZIP]').attr('old_value', LOCATION_ZIP)
					$('.order_delivery_block').html('')
					$('.order_address_block').hide()

				}

			}, 700)
		}
	})


	// Назад в корзину
	$(document).on('click', '.back_to_basket_button', function () {
		if (!is_process(this)) {
			$('.order___form:visible').attr('action', '/basket/')
			$('.order___form:visible').submit()
		}
	})


	// Назад на 2 шаг
	$(document).on('click', '.back_to_step_2_button', function () {
		if (!is_process(this)) {
			$('.order___form:visible').attr('action', '/order_step_2/')
			$('.order___form:visible').submit()
		}
	})

	// Назад на 3 шаг
	$(document).on('click', '.back_to_step_3_button', function () {
		if (!is_process(this)) {
			$('.order___form:visible').attr('action', '/order_step_3/')
			$('.order___form:visible').submit()
		}
	})


	// Оформление заказа (переход на 4 шаг)
	$(document).on('click', '.to_step_4_button', function () {

		// Форма
		var this_form = $('form.order___form:visible')

		// Подготовка
		$(this_form).find('input[type=text], input[type=tel], input[type=email], input[type=password], textarea').removeClass('error')
		$(this_form).find('p.error___p').html('')

		// Задаём переменные
		var ds = $(this_form).find('input[name=ds]:checked').val()

		var loc_name = $(this_form).find('input[name=loc_name]').val()
		var loc_id = $(this_form).find('input[name=loc_id]').val()

		var user_pay_type = $(this_form).find('input[name=user_pay_type]').val()

		//var UF_COUNTRY = $(this_form).find("select[name=UF_COUNTRY]").val();
		//var PERSONAL_CITY = $(this_form).find("input[name=PERSONAL_CITY]").val();
		var PERSONAL_ZIP = $(this_form).find('input[name=PERSONAL_ZIP]').val()
		var PERSONAL_STREET = $(this_form).find('input[name=PERSONAL_STREET]').val()
		var UF_HOUSE = $(this_form).find('input[name=UF_HOUSE]').val()
		var UF_KORPUS = $(this_form).find('input[name=UF_KORPUS]').val()
		var UF_PODYEZD = $(this_form).find('input[name=UF_PODYEZD]').val()
		var UF_KVARTIRA = $(this_form).find('input[name=UF_KVARTIRA]').val()

		var error = false

		// Проверки
		if (in_array(ds, autocomplete_dss)) {

			if (loc_name.length == 0 || loc_id.length == 0) {
				error = ['loc_name', 'Выберите, пожалуйста, населённый пункт!']
			}

			if (!error) {

				// ФЛ
				if (user_pay_type == 'fiz') {

					// Проверки
					if (PERSONAL_ZIP.length == 0) {
						error = ['PERSONAL_ZIP', 'Введите, пожалуйста, Ваш почтовый индекс!']
					} else if (PERSONAL_STREET.length == 0) {
						error = ['PERSONAL_STREET', 'Введите, пожалуйста, Вашу улицу!']
					} else if (UF_HOUSE.length == 0) {
						error = ['UF_HOUSE', 'Введите, пожалуйста, номер Вашего дома!']
					}

					// ЮЛ
				} else if (user_pay_type == 'yur') {

					// Проверки
					if (PERSONAL_ZIP.length == 0) {
						error = ['PERSONAL_ZIP', 'Введите, пожалуйста, Ваш почтовый индекс!']
					} else if (PERSONAL_STREET.length == 0) {
						error = ['PERSONAL_STREET', 'Введите, пожалуйста, Вашу улицу!']
					} else if (UF_HOUSE.length == 0) {
						error = ['UF_HOUSE', 'Введите, пожалуйста, номер Вашего дома!']
					}

				}

			}
		}

		// Если нет ошибок
		if (!error) {

			$(this_form).submit()

			// Ошибка
		} else {
			show_error(this_form, error[0], error[1])
		}

	})


	// ОФОРМЛЕНИЕ ЗАКАЗА
	$(document).on('click', '.order___button', function () {
		if (!is_process(this)) {
			// Форма
			var form = $('.order___form')
			process(true)
			//$.noty.closeAll();
			var form_data = $(form).serialize()
			// запрос
			$.post('/ajax/ajax.php', {action: 'order', form_data: form_data}, function (data) {
					if (data.status == 'ok') {

						var success_form = create_form('order_success', 'post', '/order_success/')
						$(success_form).append('<input type="hidden" name="order_id" value="' + data.order_id + '">')
						$(success_form).append('<input type="hidden" name="ps" value="' + data.ps + '">')
						$(success_form).submit()

					} else if (data.status == 'password_error') {

						$('.order___form input[name=password]').remove()

						$('#order_login_modal .error___p').html(data.error_text)

						//$.fancybox.close();

						if ($('#order_login_modal:visible').length == 0) {

							$.fancybox.open({
								src: '#order_login_modal',
								type: 'inline',
								opts: {
									speed: 300,
									autoFocus: false,
									i18n: {
										'en': {
											CLOSE: 'Закрыть'
										}
									}
								}
							})

						}

					} else if (data.status == 'email_error') {

						$('.order___form input[name=password]').remove()

						$.fancybox.close()
						$.fancybox.open({
							src: '#order_login_modal',
							type: 'inline',
							opts: {
								speed: 300,
								autoFocus: false,
								i18n: {
									'en': {
										CLOSE: 'Закрыть'
									}
								}
							}
						})

					} else if (data.status == 'error') {
						show_error(this_form, false, data.error_text)
					}
				}, 'json')
				.fail(function (data, status, xhr) {
					errorWindow('Ошибка запроса...')
				})
				.always(function (data, status, xhr) {
					process(false)
				})
		}
	})


	// Авторизация в заказе
	$(document).on('click', '.order_auth___button', function () {
		if (!is_process(this)) {
			var button = $(this)
			// Форма
			var this_form = $(this).parents('form')
			// Подготовка
			$(this_form).find('input[type=text], input[type=tel], input[type=email], input[type=password], textarea').removeClass('is___error')
			$(this_form).find('p.error___p').html('')
			// Задаём переменные
			var password = $(this_form).find('input[name=password]').val()
			var error = false
			// Проверки
			if (password.length == 0) {
				error = ['password', 'Введите, пожалуйста, Ваш пароль!']
			}
			// Если нет ошибок
			if (!error) {

				if ($('.order___form input[name=password]').length == 0) {
					$('.order___form').append('<input type="hidden" name="password" value="' + password + '">')
				}
				//$(button).parents('div.modal').find('.fancybox-close-small').trigger('click');
				$('.order___button').trigger('click')

				// Ошибка
			} else {
				show_error(this_form, error[0], error[1])
			}
		}
	})


	// Выбор типа плательщика (регистрация)
	$(document).on('change', 'input[name=reg_payer_type]', function () {
		var payer_type = $(this).val()
		$('.reg_fiz_form').hide()
		$('.reg_yur_form').hide()
		if (payer_type == 'fiz') {
			$('.reg_fiz_form').show()
		} else if (payer_type == 'yur') {
			$('.reg_yur_form').show()
		}
	})


	// Ловим ввод промо-кода
	$(document).on('keyup', 'input[name=promokod]', function () {
		coupon_changed = false
		var input = $(this)
		$(input).removeClass('is___error')
		clearInterval(promo_interval)
		promo_interval = setInterval(function () {
			clearInterval(promo_interval)
			$(input).change()
		}, 700)
	})
	$(document).on('change', 'input[name=promokod]', function () {

		if (!coupon_changed) {

			var input = $(this)
			var coupon = $(input).val()

			coupon_changed = true

			clearInterval(promo_interval)
			process(true)
			$.post('/ajax/ajax.php', {action: 'set_coupon', coupon: coupon}, function (data) {
					if (data.status == 'ok') {
						$('.basket___table tbody').html(data.basket_itmes_html)
						basket_sum()
						var mess = data.message
						if (mess.length > 0) {

							if (data.promo_success == 1) {
								successWindow(mess)
							} else {
								errorWindow(mess)
							}

							if (data.promo_success == 1) {
								$(input).removeClass('is___error')
							} else if (data.deact == 0) {
								$(input).addClass('is___error')
							}
						}
						if (data.promo_success == 1) {
							$('input[name=promokod]').addClass('success')
						} else {
							$('input[name=promokod]').removeClass('success')
						}
					}
				}, 'json')
				.fail(function (data, status, xhr) {
					errorWindow('Ошибка запроса...')
				})
				.always(function (data, status, xhr) {
					process(false)
				})

		}

	})


	// Поиск в фильтре
	$(document).on('keyup', 'input.filter_search_input', function () {

		var r = $(this).val()
		r = r.toLowerCase()

		$(this).parents('div.item').find('input[type=checkbox]').each(function () {

			var title = $(this).attr('prop_value')

			// нет вхождения
			if (title.indexOf(r) == -1) {
				$(this).parents('div.line').hide()
				// есть вхождение
			} else {
				$(this).parents('div.line').show()
			}

		})

	})


	// Выбор цвета в фильтре
	$(document).on('click', '.smartfilter div.color label', function (e) {


		console.log($(this).hasClass('reset_color'))
		console.log($(this).parents('item').find('.block_unset_button'))
		if ($(this).hasClass('reset_color')) {
			$(this).parents('.item').find('.block_unset_button').trigger('click')
			return
		}


		var color_id = $(this).parent().find('input[type=radio]').attr('color_id')
		$('.smartfilter div.color input').prop('checked', false)
		$('.smartfilter div.color input[color_id=' + color_id + ']').prop('checked', true)
	})


	// Чекбокс в фильтре
	$(document).on('change', '.smartfilter div.collection___block input[type=checkbox]', function (e) {
		clearInterval(filter_checkbox_interval)
		filter_checkbox_interval = setInterval(function () {
			clearInterval(filter_checkbox_interval)
			$('.smartfilter .set___button').trigger('click')
		}, 600)
	})


	// Сброс в блоках фильтра
	$(document).on('click', '.block_unset_button', function () {

		/*
		Цвет
		 */
		$(this).parents('div.item').find('input[type=checkbox]').prop('checked', false)
		$(this).parents('div.item').find('input.filter_search_input').val('')
		$(this).parents('div.item').find('div.line').show()

		/*
		Коллекции
		 */
		$(this).parents('form.smartfilter').find('.set___button').trigger('click')

	})

	// Сброс в блоках фильтра со скрытым input
	$(document).on('click', '.block_unset_button_index_hidden', function () {

		$('input[type=hidden][name="' + $(this).data('type') + '"]').remove()
		$(this).parents('form.smartfilter').find('.set___button').trigger('click')

	})


	// Чекбокс в фильтре цвет
	$(document).on('change', '.smartfilter input[type=checkbox]', function (e) {

		console.log($(this).parents('form.smartfilter'))

		$(this).parents('form.smartfilter').find('.set___button').trigger('click')

		//.submit()


		setTimeout(function () {
			//$('.smartfilter').submit()
			//$('.smartfilter .set___button').trigger('click')
		}, 500)

	})


	// Показать/скрыть подпункты коллекций
	$(document).on('click', '.side_collections__category--group .side_collections__category_link', function (event) {
		event.preventDefault()

		var el = $(this)
		var parent = el.parents('.side_collections__category--group')
		var isActive = parent.hasClass('show_list')

		$('.side_collections__category--group.show_list').removeClass('show_list')

		if (!isActive) {
			parent.addClass('show_list')
		}

	})

	$(document).on('click', '.search_result__show_more', function () {
		var el = $(this)
		el.parents('.search_result__row').find('.search_result__card_wrap').removeClass('hidden')
	})


	// Смена внешнего вида каталога
	$(document).on('click', '.row_cnt_block a', function () {
		if (
			!is_process(this)
			&&
			!$(this).hasClass('active')
		) {
			var button = $(this)
			var new_row_cnt = $(button).text()

			/*$.cookie('BITRIX_SM_row_cnt', new_row_cnt, { expires: 7, path: '/' });
			$('.row_cnt_block a').removeClass('active');
			$(this).addClass('active');
			window.location.href = document.URL;*/

			process(true)
			$.noty.closeAll()
			$.post('/ajax/ajax.php', {action: 'row_cnt_change', new_row_cnt: new_row_cnt}, function (data) {
					if (data.status == 'ok') {
						$('.row_cnt_block a').removeClass('active')
						$(button).addClass('active')
						window.location.href = document.URL
					}
				}, 'json')
				.fail(function (data, status, xhr) {
					errorWindow('Ошибка запроса...')
				})
				.always(function (data, status, xhr) {
					process(false)
				})

		}
	})


	// Смена внешнего вида каталога моб. версия
	$(document).on('click', '.sort_buttons--mob__btn', function () {
		if (
			!is_process(this)
			&&
			!$(this).hasClass('active')
		) {
			var button = $(this)
			var new_row_cnt = button.data('row_cnt')

			console.log(new_row_cnt)

			process(true)
			$.noty.closeAll()

			$.post('/ajax/ajax.php', {action: 'mob_row_cnt_change', new_row_cnt: new_row_cnt}, function (data) {
					if (data.status == 'ok') {
						$('.sort_buttons--mob__btn').removeClass('active')
						button.addClass('active')
						window.location.href = document.URL
					}
				}, 'json')
				.fail(function (data, status, xhr) {
					errorWindow('Ошибка запроса...')
				})
				.always(function (data, status, xhr) {
					process(false)
				})

		}
	})


	// Подгрузка последних поступлений
	$(document).on('click', '.latest_arrival_more_button', function () {
		if (!is_process(this)) {
			var section_id = $('input[name=section_id]').val()
			var stop_ids = []
			if ($('div.latest_arrival_block .product___block').length > 0) {
				$('div.latest_arrival_block .product___block').each(function () {
					var tov_id = $(this).attr('tov_id')
					stop_ids.push(tov_id)
				})
			}
			var params = {
				action: 'more_latest_arrival',
				stop_ids_str: stop_ids.join('|'),
				section_id: section_id
			}
			process(true)
			$.post('/ajax/ajax.php', params, function (data) {
					if (data.status == 'ok') {
						$('.latest_arrival_block').append(data.html)
						if ($('more_exists').length == 0) {
							$('.latest_arrival_more_block').hide()
						}
						$('more_exists').remove()
					}
				}, 'json')
				.fail(function (data, status, xhr) {
					errorWindow('Ошибка запроса...')
				})
				.always(function (data, status, xhr) {
					process(false)
				})
		}
	})


	// Покупка в 1 клик
	$(document).on('click', '.buy_1_click___button', function () {
		if (!is_process(this)) {
			// Форма
			var this_form = $(this).parents('form')
			// Подготовка
			$(this_form).find('input[type=text], input[type=tel], input[type=email], input[type=password], textarea').removeClass('is___error')
			$(this_form).find('p.error___p').html('')
			// Задаём переменные
			var name = $(this_form).find('input[name=name]').val()
			var email = $(this_form).find('input[name=email]').val()
			var phone = $(this_form).find('input[name=phone]').val()
			var agree = $(this_form).find('input[name=agree]:checked').val()
			var error = false
			// Проверки
			if (name.length == 0) {
				error = ['name', 'Введите, пожалуйста, Ваше имя!']
			} else if (email.length == 0) {
				error = ['email', 'Введите, пожалуйста, Ваш Email!']
			} else if (email.length > 0 && !(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(email))) {
				error = ['EMAIL', 'Email содержит ошибки!']
			} else if (phone.length == 0) {
				error = ['phone', 'Введите, пожалуйста, Ваш телефон!']
			} else if (agree != 1) {
				error = ['agree', 'Подтвердите, пожалуйста, согласие с офертой и политикой конфиденциальности!']
			}
			console.log(window.offerKey)

			// Если нет ошибок
			if (!error) {
				process(true)
				var form_data = $(this_form).serialize()
				var tovar_id = window.offerKey//$(this_form).find("input[name=tov_id]").val();
				// запрос
				$.post('/ajax/ajax.php', {action: 'buy_1_click', form_data: form_data, tovar_id: tovar_id}, function (data) {
						if (data.status == 'ok') {

							$('.fancybox-close-small').trigger('click')

							$(this_form).find('input[type=text], input[type=tel], input[type=email], input[type=password], textarea').val('')

							successWindow('Ваша заявка успешно отправлена!')

						} else if (data.status == 'error') {
							show_error(this_form, false, data.error_text)
						}
					}, 'json')
					.fail(function (data, status, xhr) {
						errorWindow('Ошибка запроса...')
					})
					.always(function (data, status, xhr) {
						process(false)
					})
				// Ошибка
			} else {
				show_error(this_form, error[0], error[1])
			}
		}
	})


	// Лайк за коммент
	$(document).on('click', '.like___button', function () {
		if (!is_process(this)) {
			var comment_id = $(this).attr('comment_id')
			process(true)
			$.post('/ajax/ajax.php', {action: 'like_comment', comment_id: comment_id}, function (data) {
					if (data.status == 'ok') {
						$('.like___button[comment_id=' + comment_id + '] div.likes').html(data.likes)
					} else if (data.status == 'error') {
						errorWindow(data.error_text)
					}
				}, 'json')
				.fail(function (data, status, xhr) {
				})
				.always(function (data, status, xhr) {
					process(false)
				})
		}
	})


	// Подгрузка комментариев
	$(document).on('click', '.more___comments', function () {
		if (!is_process(this)) {
			var tov_id = $(this).attr('tov_id')
			var stop_ids = []
			if ($('.comment___item').length > 0) {
				$('.comment___item').each(function () {
					var item_id = $(this).attr('item_id')
					stop_ids.push(item_id)
				})
			}
			var params = {
				action: 'more_comments',
				stop_ids_str: stop_ids.join('|'),
				tov_id: tov_id
			}
			process(true)
			$.post('/ajax/ajax.php', params, function (data) {
					if (data.status == 'ok') {
						$('section.comments div.items').append(data.html)
						var more_exist = data.more_exist
						if (more_exist == 0) {
							$('.more___comments').hide()
						}
					}
				}, 'json')
				.fail(function (data, status, xhr) {
					errorWindow('Ошибка запроса...')
				})
				.always(function (data, status, xhr) {
					process(false)
				})
		}
	})


	// Новый комментарий
	$(document).on('click', '.comment___button', function () {
		if (!is_process(this)) {
			// Форма
			var this_form = $(this).parents('form')
			// Подготовка
			$(this_form).find('input[type=text], input[type=tel], input[type=email], input[type=password], textarea').removeClass('is___error')
			$(this_form).find('p.error___p').html('')
			// Задаём переменные
			var tov_id = $(this).attr('tov_id')
			var message = $(this_form).find('textarea[name=message]').val()
			var error = false
			// Проверки
			if (message.length == 0) {
				error = ['message', 'Введите, пожалуйста, Ваш комментарий!']
			}
			// Если нет ошибок
			if (!error) {
				process(true)
				var form_data = $(this_form).serialize()
				$.post('/ajax/ajax.php', {action: 'new_comment', tov_id: tov_id, message: message}, function (data) {
						if (data.status == 'ok') {
							$(this_form).find('textarea').val('')

							var text = 'Комментарий отправлен!'
							if (data.isAdmin != 'Y') {
								text += '<br>Он появится после модерации'
							}

							messageWindow(text)

						} else if (data.status == 'error') {
							show_error(this_form, false, data.error_text)
						}
					}, 'json')
					.fail(function (data, status, xhr) {
						errorWindow('Ошибка запроса...')
					})
					.always(function (data, status, xhr) {
						process(false)
					})
				// Ошибка
			} else {
				show_error(this_form, error[0], error[1])
			}
		}
	})


	// Добавление/удаление товара из избранного
	$(document).on('click', '.fav___link', function () {
		if (!is_process(this)) {
			var tov_id = $(this).attr('tov_id')
			if ($(this).hasClass('active')) {
				// Удаляем из избранного
				delFromFavorites(tov_id)
			} else {
				// Добавляем в избранное
				addToFavorites(tov_id)
			}
		}
	})


	// Удаление из раздела Избранное
	$(document).on('click', '.favorite___remove', function () {
		if (!is_process(this)) {
			var item_id = $(this).attr('item_id')
			process(true)
			$.post('/ajax/ajax.php', {action: 'favorite_remove', item_id: item_id}, function (data) {
					if (data.status == 'ok') {

						$('.favorites___block').html(data.favorites_html)
					}
				}, 'json')
				.fail(function (data, status, xhr) {
					errorWindow('Ошибка запроса...')
				})
				.always(function (data, status, xhr) {
					process(false)
				})
		}
	})


	// Добавление товара в корзину
	$(document).on('click', '.add_to_basket', function () {
		if (!is_process(this)) {

			var sku_id = false
			if ($('input[name=size]').length > 0) {
				if ($('input[name=size]:checked').length == 0) {

					$('.modal___error_p').html('Выберите размер<br><a href="#">Таблица размеров</a>')
					$('.modalErrorSizeYellow-empty').removeClass('modalErrorSizeYellow')
					$('.modalErrorSizeYellow-empty').addClass('modalErrorSizeYellow')

					$.cookie('go_to_basket', 1, {path: '/'})

					return
				} else {
					sku_id = $('input[name=size]:checked').val()
				}
			} else {
				if ($(this).attr('sku_id')) {
					sku_id = $(this).attr('sku_id')
				}
			}
			if (sku_id) {
				add_to_basket(sku_id, $(this))
			} else {
				$('.modal___error_p').html('Ошибка добавления в корзину!')
			}
		}
	})


	// Добавление товара в корзину (modal)
	$(document).on('click', '.modal_add_to_basket', function () {
		if (!is_process(this)) {

			var sku_id = false
			if ($('input[name=modal_size]').length > 0) {
				if ($('input[name=modal_size]:checked').length == 0) {

					$('.modal___error_p').html('Выберите размер<br><a href="#">Таблица размеров</a>')
					$('.modalErrorBoxYellow-empty').removeClass('modalErrorBoxYellow')
					$('.modalErrorBoxYellow-empty').addClass('modalErrorBoxYellow')

					$.cookie('go_to_basket', 1, {path: '/'})

					return
				} else {
					sku_id = $('input[name=modal_size]:checked').val()
				}
			} else {
				if ($(this).attr('sku_id')) {
					sku_id = $(this).attr('sku_id')
				}
			}
			if (sku_id) {
				add_to_basket(sku_id, $(this))
			} else {
				$('.modal___error_p').html('Ошибка добавления в корзину!')
			}
		}
	})


	$(document).on('change', 'input[name=size]', function () {
		var go_to_basket = $.cookie('go_to_basket')
		if (go_to_basket == 1) {
			var sku_id = $('input[name=size]:checked').val()
			add_to_basket(sku_id)
		}
	})


	$(document).on('change', 'input[name=modal_size]', function () {
		var go_to_basket = $.cookie('go_to_basket')
		if (go_to_basket == 1) {
			var sku_id = $('input[name=modal_size]:checked').val()
			add_to_basket(sku_id)
		}
	})


	// Удаление из маленькой корзины
	$(document).on('click', '.basket_small___remove', function () {
		if (!is_process(this)) {
			var link = $(this)
			var item_id = $(this).attr('item_id')
			var pure_url = $('input[name=pureURL]').val()
			process(true)
			$.post('/ajax/ajax.php', {action: 'unset_item', item_id: item_id, pure_url: pure_url}, function (data) {
					if (data.status == 'ok') {
						$('.basket_small_block').replaceWith(data.top_basket_html)
						$('.mini_modal_link').trigger('click')
					}
				}, 'json')
				.fail(function (data, status, xhr) {
					errorWindow('Ошибка запроса...')
				})
				.always(function (data, status, xhr) {
					process(false)
				})
		}
	})


	// Удаление из корзины
	$(document).on('click', '.basket___remove', function () {
		if (!is_process(this)) {
			var link = $(this)
			var item_id = $(this).attr('item_id')
			var pure_url = $('input[name=pureURL]').val()
			process(true)
			$.post('/ajax/ajax.php', {action: 'unset_item', item_id: item_id, pure_url: pure_url}, function (data) {
					if (data.status == 'ok') {
						$('.basket_small_block').replaceWith(data.top_basket_html)
						$('.basket___table tbody').html(data.basket_itmes_html)
						basket_sum()
						var tr_cnt = $('.basket___table tbody tr').length
						if (tr_cnt == 0) {
							$('.basket___section').html('<p class="no___count">Ваша корзина пуста</p>')
						}
					}
				}, 'json')
				.fail(function (data, status, xhr) {
					errorWindow('Ошибка запроса...')
				})
				.always(function (data, status, xhr) {
					process(false)
				})
		}
	})


	// Изменение количества товара в корзине (МИНУС)
	$(document).on('click', '.amount .minus', function (e) {
		if (!is_process(this)) {
			var input = $(this).parents('tr').find('input[name=quantity]')
			var quantity = parseInt(input.val())
			var item_id = $(input).attr('item_id')
			var pure_url = $('input[name=pureURL]').val()
			if (quantity > 1) {
				quantity--
				update_basket_quantity(item_id, quantity, pure_url)
			}
		}
	})


	// Изменение количества товара в корзине (ПЛЮС)
	$(document).on('click', '.amount .plus', function (e) {
		if (!is_process(this)) {
			var input = $(this).parents('tr').find('input[name=quantity]')
			var quantity = parseInt(input.val())
			var maximum = parseInt(input.data('maximum'))
			var item_id = $(input).attr('item_id')
			var pure_url = $('input[name=pureURL]').val()
			if (quantity < maximum) {
				quantity++
				update_basket_quantity(item_id, quantity, pure_url)
			}
		}
	})


	// Изменение количества в корзине (ВВОД С КЛАВИАТУРЫ)
	$(document).on('keyup', '.basket___table input[name=quantity]', function () {
		minus_zero(this)
		var input = $(this)
		var val = $(input).val()
		clearInterval(quantity_interval)
		if (val.length > 0) {
			quantity_interval = setInterval(function () {
				clearInterval(quantity_interval)
				$(input).change()
			}, 700)
		}
	})


	$(document).on('change', '.basket___table input[name=quantity]', function () {
		minus_zero(this, true)
		var input = $(this)
		var quantity = parseInt(input.val())
		var item_id = $(input).attr('item_id')
		var pure_url = $('input[name=pureURL]').val()
		update_basket_quantity(item_id, quantity, pure_url, true)
	})


	// Выбор размера
	$(document).on('click', '.size___label', function () {
		var price = $(this).attr('price')
		var old_price = $(this).attr('old_price')
		$('.price___block div.new').html(number_format(price, 0, ',', ' ') + ' <span class="currency">:</span>')
		if (old_price != price) {
			var discount = Math.round((old_price - price) / old_price * 100)
			$('.old___price_block').html(number_format(old_price, 0, ',', ' ') + ' <span class="currency">:')
			$('.discount___block').html(discount + '%')
			$('.old___price_block').show()
			$('.discount___block').show()
		} else {
			$('.old___price_block').hide()
			$('.discount___block').hide()
		}
	})


	// Выбор размера (модальное окно)
	$(document).on('click', '.modal_size___label', function () {
		var price = $(this).attr('price')
		var old_price = $(this).attr('old_price')
		$('.modal_price___block div.new').html(number_format(price, 0, ',', ' ') + ' <span class="currency">:</span>')
		if (old_price != price) {
			var discount = Math.round((old_price - price) / old_price * 100)
			$('.modal_old___price_block').html(number_format(old_price, 0, ',', ' ') + ' <span class="currency">:')
			$('.modal_discount___block').html(discount + '%')
			$('.modal_old___price_block').show()
			$('.modal_discount___block').show()
		} else {
			$('.modal_old___price_block').hide()
			$('.modal_discount___block').hide()
		}

	})


	// Авторизация
	$(document).on('click', '.auth___button', function () {
		if (!is_process(this)) {
			// Форма
			var this_form = $(this).parents('form')
			// Подготовка
			$(this_form).find('input[type=text], input[type=tel], input[type=email], input[type=password], textarea').removeClass('is___error')
			$(this_form).find('p.error___p').html('')
			// Задаём переменные
			var login = $(this_form).find('input[name=login]').val()
			var password = $(this_form).find('input[name=password]').val()

			var remember = $(this_form).find('input[name=remember]:checked').val()

			var error = false
			// Проверки
			if (login.length == 0) {
				error = ['login', 'Введите, пожалуйста, Ваш Email!']
			} else if (password.length == 0) {
				error = ['password', 'Введите, пожалуйста, Ваш пароль!']
			}

			// Если нет ошибок
			if (!error) {
				process(true)
				//$.noty.closeAll();
				$.post('/ajax/ajax.php', {action: 'auth', login: login, password: password}, function (data) {
						if (data.status == 'ok') {
							$('.fancybox-close-small').trigger('click')

							successWindow('Успешная авторизация !')

							setTimeout(function () {
								window.location.href = document.URL
							}, 1500)

						} else if (data.status == 'error') {
							show_error(this_form, false, data.error_text)
						}
					}, 'json')
					.fail(function (data, status, xhr) {
						errorWindow('Ошибка запроса...')
					})
					.always(function (data, status, xhr) {
						process(false)
					})
				// Ошибка
			} else {
				show_error(this_form, error[0], error[1])
			}

		}
	})


	// Запрос восстановления пароля
	$(document).on('click', '.recovery___button', function () {
		if (!is_process(this)) {
			// Форма
			var this_form = $(this).parents('form')
			// Подготовка
			$(this_form).find('input[type=text], input[type=tel], input[type=email], input[type=password], textarea').removeClass('is___error')
			$(this_form).find('p.error___p').html('')
			// Задаём переменные
			var email = $(this_form).find('input[name=email]').val()

			var error = false
			// Проверки
			if (email.length == 0) {
				error = ['email', 'Введите, пожалуйста, Ваш Email!']
			} else if (email.length > 0 && !(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(email))) {
				error = ['email', 'Email содержит ошибки!']
			}

			// Если нет ошибок
			if (!error) {
				process(true)
				//$.noty.closeAll();
				var form_data = $(this_form).serialize()
				$.post('/ajax/ajax.php', {action: 'password_recovery', form_data: form_data}, function (data) {
						if (data.status == 'ok') {
							$('.fancybox-close-small').trigger('click')

							successWindow('Ссылка для восстановления пароля отправлена Вам на Email!')

						} else if (data.status == 'error') {
							show_error(this_form, false, data.text)
						}
					}, 'json')
					.fail(function (data, status, xhr) {
						errorWindow('Ошибка запроса...')
					})
					.always(function (data, status, xhr) {
						process(false)
					})
				// Ошибка
			} else {
				show_error(this_form, error[0], error[1])
			}

		}
	})


	// Новый пароль (восстановление)
	$(document).on('click', '.new_password_button', function () {
		if (!is_process(this)) {
			// Форма
			var this_form = $(this).parents('form')
			// Подготовка
			$(this_form).find('input[type=text], input[type=tel], input[type=email], input[type=password], textarea').removeClass('is___error')
			$(this_form).find('p.error___p').html('')
			// Задаём переменные
			var PASSWORD = $(this_form).find('input[name=PASSWORD]').val()
			var CONFIRM_PASSWORD = $(this_form).find('input[name=CONFIRM_PASSWORD]').val()

			var error = false
			// Проверки
			if (PASSWORD.length == 0) {
				error = ['PASSWORD', 'Придумайте, пожалуйста, пароль!']
			} else if (PASSWORD.length > 0 && PASSWORD.length < 8) {
				error = ['PASSWORD', 'Длина пароля - не менее 8 символов!']
			} else if (CONFIRM_PASSWORD.length == 0) {
				error = ['CONFIRM_PASSWORD', 'Повторите, пожалуйста, пароль!']
			} else if (PASSWORD != CONFIRM_PASSWORD) {
				error = ['CONFIRM_PASSWORD', 'Пароли отличаются!']
			}

			// Если нет ошибок
			if (!error) {
				process(true)
				var form_data = $(this_form).serialize()
				$.post('/ajax/ajax.php', {action: 'personal_password', form_data: form_data}, function (data) {
						if (data.status == 'ok') {
							window.location.href = '/personal/'
						} else if (data.status == 'error') {
							show_error(this_form, false, data.text)
						}
					}, 'json')
					.fail(function (data, status, xhr) {
						errorWindow('Ошибка запроса...')
					})
					.always(function (data, status, xhr) {
						process(false)
					})
				// Ошибка
			} else {
				show_error(this_form, error[0], error[1])
			}

		}
	})


	// Регистрация
	$(document).on('click', '.reg___button', function () {
		if (!is_process(this)) {

			// Форма
			var this_form = $(this).parents('form')
			// Подготовка
			$(this_form).find('input[type=text], input[type=tel], input[type=email], input[type=password], textarea').removeClass('is___error')
			$(this_form).find('p.error___p').html('')
			// Задаём переменные
			//var reg_payer_type = $("input[name=reg_payer_type]:checked").val();
			//var user_type = $(this_form).find("input[name=user_type]:checked").val();
			var NAME = $(this_form).find('input[name=NAME]').val()
			//var UF_COMPANY_NAME = $(this_form).find("input[name=UF_COMPANY_NAME]").val();
			var EMAIL = $(this_form).find('input[name=EMAIL]').val()
			var PASSWORD = $(this_form).find('input[name=PASSWORD]').val()
			var CONFIRM_PASSWORD = $(this_form).find('input[name=CONFIRM_PASSWORD]').val()
			var PERSONAL_GENDER = $(this_form).find('input[name=PERSONAL_GENDER]:checked').val()
			var agree = $(this_form).find('input[name=agree]:checked').length > 0 ? true : false
			var error = false

			// Проверки
			if (NAME.length == 0) {
				error = ['NAME', 'Введите, пожалуйста, Ваше имя!']
			} else if (EMAIL.length == 0) {
				error = ['EMAIL', 'Введите, пожалуйста, Ваш Email!']
			} else if (EMAIL.length > 0 && !(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(EMAIL))) {
				error = ['EMAIL', 'Email содержит ошибки!']
			} else if (PASSWORD.length == 0) {
				error = ['PASSWORD', 'Придумайте, пожалуйста, пароль!']
			} else if (PASSWORD.length > 0 && PASSWORD.length < 8) {
				error = ['PASSWORD', 'Длина пароля - не менее 8 символов!']
			} else if (CONFIRM_PASSWORD.length == 0) {
				error = ['CONFIRM_PASSWORD', 'Повторите, пожалуйста, пароль!']
			} else if (PASSWORD != CONFIRM_PASSWORD) {
				error = ['CONFIRM_PASSWORD', 'Пароли отличаются!']
			} else if (!agree) {
				error = ['agree', 'Подтвердите, пожалуйста, согласие с офертой и политикой конфиденциальности!']
			}

			/*if ( reg_payer_type == 'fiz' ){

				// Проверки
				if (NAME.length == 0){
					error = ['NAME',   'Введите, пожалуйста, Ваше имя!'];
				} else if (EMAIL.length == 0){
					error = ['EMAIL',   'Введите, пожалуйста, Ваш Email!'];
				} else if (EMAIL.length > 0 && !(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(EMAIL))){
					error = ['EMAIL',   'Email содержит ошибки!'];
				} else if (PASSWORD.length == 0){
					error = ['PASSWORD',   'Придумайте, пожалуйста, пароль!'];
				} else if (PASSWORD.length > 0 && PASSWORD.length < 8){
					error = ['PASSWORD',   'Длина пароля - не менее 8 символов!'];
				} else if (CONFIRM_PASSWORD.length == 0){
					error = ['CONFIRM_PASSWORD',   'Повторите, пожалуйста, пароль!'];
				} else if (PASSWORD != CONFIRM_PASSWORD){
					error = ['CONFIRM_PASSWORD',   'Пароли отличаются!'];
				} else if( !agree ){
					error = ['agree',   'Подтвердите, пожалуйста, согласие с офертой и политикой конфиденциальности!'];
				}

			} else if ( reg_payer_type == 'yur' ){

				// Проверки
				if (UF_COMPANY_NAME.length == 0){
					error = ['UF_COMPANY_NAME',   'Введите, пожалуйста, название компании!'];
				} else if (NAME.length == 0){
					error = ['NAME',   'Введите, пожалуйста, Ваше имя!'];
				} else if (EMAIL.length == 0){
					error = ['EMAIL',   'Введите, пожалуйста, Ваш Email!'];
				} else if (EMAIL.length > 0 && !(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(EMAIL))){
					error = ['EMAIL',   'Email содержит ошибки!'];
				} else if (PASSWORD.length == 0){
					error = ['PASSWORD',   'Придумайте, пожалуйста, пароль!'];
				} else if (PASSWORD.length > 0 && PASSWORD.length < 8){
					error = ['PASSWORD',   'Длина пароля - не менее 8 символов!'];
				} else if (CONFIRM_PASSWORD.length == 0){
					error = ['CONFIRM_PASSWORD',   'Повторите, пожалуйста, пароль!'];
				} else if (PASSWORD != CONFIRM_PASSWORD){
					error = ['CONFIRM_PASSWORD',   'Пароли отличаются!'];
				} else if( !agree ){
					error = ['agree',   'Подтвердите, пожалуйста, согласие с офертой и политикой конфиденциальности!'];
				}

			}*/

			// Если нет ошибок
			if (!error) {
				process(true)
				var form_data = $(this_form).serialize()
				$.post('/ajax/ajax.php', {action: 'register', form_data: form_data}, function (data) {
						if (data.status == 'ok') {
							$(this_form).find('input[type=text], input[type=tel], input[type=email], input[type=password], textarea').val('')
							$('.fancybox-close-small').trigger('click')

							successWindow('Вы успешно зарегистрированы!<br><br>Теперь <u><a onclick=\'loginModal()\' style="cursor:pointer">авторизуйтесь</a></u>!')

						} else if (data.status == 'error') {
							show_error(this_form, false, data.error_text)
						}
					}, 'json')
					.fail(function (data, status, xhr) {
						errorWindow('Ошибка запроса...')
					})
					.always(function (data, status, xhr) {
						process(false)
					})
				// Ошибка
			} else {
				show_error(this_form, error[0], error[1])
			}

		}
	})


	// Подписка
	$(document).on('click', '.subscribe_button', function () {
		if (!is_process(this)) {
			// Форма
			var this_form = $(this).parents('form')
			// Подготовка
			$(this_form).find('input[type=text], input[type=tel], input[type=email], input[type=password], textarea').removeClass('is___error')
			$(this_form).find('p.error___p').html('')
			// Задаём переменные
			var name = $(this_form).find('input[name=name]').val()
			var email = $(this_form).find('input[name=email]').val()
			var error = false
			// Проверки
			if (name.length == 0) {
				error = ['name', 'Введите, пожалуйста, Ваше имя!']
			} else if (email.length == 0) {
				error = ['email', 'Введите, пожалуйста, Ваш Email!']
			} else if (email.length > 0 && !(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(email))) {
				error = ['email', 'Email содержит ошибки!']
			}
			// Если нет ошибок
			if (!error) {
				process(true)
				$.post('/ajax/ajax.php', {action: 'subscribe', email: email}, function (data) {
						if (data.status == 'ok') {

							successWindow('Успешная подписка')

							$(this_form).find('input[type=text], input[type=tel], input[type=email], input[type=password], textarea').val('')
						} else if (data.status == 'error') {
							//show_error(this_form, false, 'Вы уже подписаны на рассылку', true);

							errorWindow('Вы уже подписаны на рассылку')
						}
					}, 'json')
					.fail(function (data, status, xhr) {
						errorWindow('Ошибка запроса...')
					})
					.always(function (data, status, xhr) {
						process(false)
					})
				// Ошибка
			} else {
				show_error(this_form, error[0], error[1], true)
			}
		}
	})


	// Форма обратной связи
	$(document).on('click', '.feedback___button', function () {
		if (!is_process(this)) {
			// Форма
			var this_form = $(this).parents('form')
			// Подготовка
			$(this_form).find('input[type=text], input[type=tel], input[type=email], input[type=password], textarea').removeClass('is___error')
			$(this_form).find('p.error___p').html('')
			// Задаём переменные
			var name = $(this_form).find('input[name=name]').val()
			var email = $(this_form).find('input[name=email]').val()
			var phone = $(this_form).find('input[name=phone]').val()
			var message = $(this_form).find('textarea[name=message]').val()
			var error = false
			// Проверки
			if (name.length == 0) {
				error = ['name', 'Введите, пожалуйста, Ваше имя!']
			} else if (phone.length == 0) {
				error = ['phone', 'Введите, пожалуйста, Ваш телефон!']
			} else if (phone.length > 0 && !(/^[0-9()+ -]{1,99}$/.test(phone))) {
				error = ['phone', 'В номере телефона допускаются только цифры, а также символы "+", "-", пробел и круглые скобки!']
			} else if (email.length == 0) {
				error = ['email', 'Введите, пожалуйста, Ваш Email!']
			} else if (email.length > 0 && !(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(email))) {
				error = ['email', 'Email содержит ошибки!']
			} else if (message.length == 0) {
				error = ['message', 'Введите, пожалуйста, Ваше сообщение!']
			}
			// Если нет ошибок
			if (!error) {
				process(true)
				var form_data = $(this_form).serialize()
				$.post('/ajax/ajax.php', {action: 'feedback', form_data: form_data}, function (data) {
						if (data.status == 'ok') {

							successWindow('Ваше сообщение успешно отправлено!')

							$(this_form).find('input[type=text], input[type=tel], input[type=email], input[type=password], textarea').val('')
						} else if (data.status == 'error') {
							show_error(this_form, false, data.error_text)
						}
					}, 'json')
					.fail(function (data, status, xhr) {
						errorWindow('Ошибка запроса...')
					})
					.always(function (data, status, xhr) {
						process(false)
					})
				// Ошибка
			} else {
				show_error(this_form, error[0], error[1])
			}
		}
	})


	// Блокировка Enter
	jQuery.fn.NotEnter =
		function () {
			return this.each(function () {
				$(this).keydown(function (e) {
					var key = e.charCode || e.keyCode || 0
					return (key != 13)
				})
			})
		}
	$('input.filter_search_input').NotEnter()


	jQuery.fn.ForceNumericOnly =
		function () {
			return this.each(function () {
				$(this).keydown(function (e) {
					var key = e.charCode || e.keyCode || 0
					// Разрешаем backspace, tab, delete, стрелки, обычные цифры и цифры на дополнительной клавиатуре
					return (
						key == 8 || key == 9 || key == 46 ||
						(key >= 37 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105)
					)
				})
			})
		}
	$('.basket___table input[name=quantity]').ForceNumericOnly()


	// Настройки по сайту
	$(document).on('click', '.settingsSaveButton', function () {
		if (!is_process(this)) {
			// Форма
			var this_form = $(this).parents('form')
			// Подготовка
			$(this_form).find('input, textarea').removeClass('is___error')
			$(this_form).find('p.error___p').html('')
			$(this_form).find('p.success___p').html('')
			// Задаём переменные
			var error = false
			// Проверки
			if ($('.admin___table .value_td').length > 0) {
				$('.admin___table .value_td input[type=text]').each(function () {
					var value = $(this).val()
					if (value.length == 0 && !error) {
						error = [$(this).attr('name'), 'Заполните, пожалуйста, это поле!']
					}
				})
			}
			// Если нет ошибок
			if (!error) {
				process(true)
				var form_data = $(this_form).serialize()
				$.post('/ajax/site_settings.php', {form_data: form_data}, function (data) {
						if (data.status == 'ok') {
							show_success(this_form, 'Успешное сохранение!')
						} else if (data.status == 'error') {
							show_error(this_form, false, data.text)
						}
					}, 'json')
					.fail(function (data, status, xhr) {
						//var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: "Ошибка запроса..."});

						errorWindow('Ошибка запроса...')
					})
					.always(function (data, status, xhr) {
						process(false)
					})
				// Ошибка
			} else {
				show_error(this_form, error[0], error[1])
			}
		}
	})


	$(document).on('keyup', '.admin___table .value_td input[type=text]', function () {
		var this_form = $(this).parents('form')
		minus_zero($(this))
		$(this_form).find('p.error___p').html('')
		$(this_form).find('p.success___p').html('')
	})


	$(document).on('click', '.unset___button', function (e) {
		e.preventDefault()
		var pureURL = $('input[name=pureURL]').val()
		window.location.href = pureURL
	})


	$(document).on('click', '.search_unset___button', function (e) {
		e.preventDefault()
		var pureURL = $('input[name=pureURL]').val()
		var q = $(this).attr('q')
		window.location.href = pureURL + '?q=' + q
	})


	// Кнопка оплаты заказа в ЛК
	$(document).on('click', '.order_link', function (e) {
		var order_id = $(this).attr('order_id')
		if ($(this).hasClass('active')) {
			if ($('div.pay___block[order_id=' + order_id + ']').length > 0) {
				$.post('/ajax/getPaymentButton.php', {order_id: order_id}, function (data) {
					if (data) {
						$('div.pay___block[order_id=' + order_id + ']').html(data)
						$('div.pay___block[order_id=' + order_id + '] .cloudpay_button').addClass('pay___button')
						$('div.pay___block[order_id=' + order_id + '] .cloudpay_button').addClass('lk___pay_button')
						$('div.pay___block[order_id=' + order_id + '] .cloudpay_button').html('Оплатить' + (pay_sum > 0 ? (' (' + pay_sum + ' руб.)') : ''))
					}
				})
			}
		} else {
			$('div.pay___block[order_id=' + order_id + ']').html('')
		}
	})


	// Меню в подвале
	$(document).on('click', 'footer .info .title', function () {

		$('footer .links').removeClass('active')
		$(this).parent().addClass('active')

	})


	// Быстрый просмотр
	$(document).on('click', '.quick_view__btn', function () {
		var id = $(this).data('id');
		window.flagVue.quickPreview = {id: id}
	})

})
