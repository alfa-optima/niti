$(document).ready(function () {

    $('.promo_banners_slider_item').on('mouseenter', function () {
        $('.promo_banners_overlay').animate({opacity: 0.2});
    });

    $('.promo_banners_slider_item').on('mouseleave', function () {
        $('.promo_banners_overlay').animate({opacity: 0});
    });

    $('.promo_banners_right_item').on('mouseenter', function () {
        $(this).find('.promo_banners_right_overlay').animate({opacity: 0.2});
    });

    $('.promo_banners_right_item').on('mouseleave', function () {
        $(this).find('.promo_banners_right_overlay').animate({opacity: 0});
    });

});