
function del_ISubscribe(illustrator){
	$.noty.closeAll(); 
	process(true);
	$.post("/ajax/del_ISubscribe.php", {illustrator:illustrator}, function(data){
		if (data.status == 'ok'){
			successWindow( "Подписка удалена!" );
			$('.subscribes___cnt[illustrator='+illustrator+']').html(data.cnt);
			$('.illustr_subscribe_button[illustrator='+illustrator+']').removeClass('active');
		} else if (data.status == 'error'){
			errorWindow( data.text );
		}
	}, 'json')
	.fail(function(data, status, xhr){
		errorWindow( "Ошибка запроса..." );
	})
	.always(function(data, status, xhr){
		process(false);
	})
}



function iSubscribeNeedAuthWindow(){
	$.fancybox.close();
	$.fancybox.open({
		src  : '#iSubscribeNeedAuth',
		type : 'inline',
		opts : {
			speed: 300,
			autoFocus : false,
			i18n : {
				'en' : {
					CLOSE : 'Закрыть'
				}
			}
		}
	})
}



function iSubscribesSet(){
	if( $('.illustr_subscribe_button').length > 0 ){
		$('.illustr_subscribe_button').removeClass('active');
		var ills = [];
		$('.illustr_subscribe_button').each(function(){
			var illustrator = $(this).attr('illustrator');
			ills.push(illustrator);
		})
		$.post("/ajax/iSubscribeSet.php", {ills:ills}, function(data){
			if (data.status == 'ok'){
				var active_ills = data.active_ills;
				if( active_ills.length > 0 ){
					$.each(active_ills, function(key, id) {
						$('.illustr_subscribe_button[illustrator='+id+']').addClass('active');
					});
				}
			}
		}, 'json')
		.fail(function(data, status, xhr){})
		.always(function(data, status, xhr){})
	}
}






$(document).ready(function(){
	
	
	iSubscribesSet();
	
	
	
	// Подписка на иллюстратора
	$(document).on('click', '.illustr_subscribe_button', function(){
		if (!is_process(this)){
			$.noty.closeAll(); 
			// Задаём переменные
			var illustrator = $(this).attr("illustrator");
			process(true);
			$.post("/ajax/iSubscribe.php", {illustrator:illustrator}, function(data){
				if (data.status == 'ok'){
					successWindow( "Успешная подписка!" );
					$('.subscribes___cnt[illustrator='+illustrator+']').html(data.cnt);
					$('.illustr_subscribe_button[illustrator='+illustrator+']').addClass('active');
				} else if (data.status == 'exists'){
					
					/*$.noty.closeAll(); 
					var n = noty({
						text        : data.text,
						type        : 'alert',
						dismissQueue: true,
						layout      : 'center',
						theme       : 'defaultTheme',
						buttons     : [
							{addClass: 'btn btn-primary', text: 'Да', onClick: function ($noty) {*/
							
								del_ISubscribe(illustrator);
								
							/*}},
							{addClass: 'btn btn-danger', text: 'Нет', onClick: function ($noty) { $noty.close(); } }
						]
					});*/
					
				} else if (data.status == 'needAuth'){
					iSubscribeNeedAuthWindow();
				} else if (data.status == 'error'){
					errorWindow( data.text );
				}
			}, 'json')
			.fail(function(data, status, xhr){
				errorWindow( "Ошибка запроса..." );
			})
			.always(function(data, status, xhr){
				process(false);
			})
		}
	});

	
	
	
});