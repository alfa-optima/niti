$(function(){
	// Слайдер на главной
	$('.main_slider .slider').owlCarousel({
		loop: true,
		margin: 0,
		dots: true,
		navSpeed: 750,
		dotsSpeed: 750,
		smartSpeed: 750,
		autoplaySpeed: 750,
		items: 1,
		autoplay: true,
		autoplayTimeout: 5000,
		responsive : {
			768 : {
				nav: true
			},
			0 : {
				nav: false
			}
		}
	})


	// Табы
	$('.tabs_container').each(function(){
		$(this).find('.tab_content:first').show()
	})

	// $('.tabs li').click(function() {
	/////////////////////////////////////////////
	$(document).on('click', '.tabs li', function(){
		/////////////////////////////////////////////
		var parentBox = $(this).parents('.tabs_container')

		$(parentBox).find('.tabs li').removeClass('active')
		$(this).addClass('active')
		$(parentBox).find('.tab_content').hide()

		var activeTab = $(this).find('a').attr('href')
		$(activeTab).fadeIn()
		return false
	})


	// Карусель товаров
	$('.products .slider.carousel').owlCarousel({
		loop: true,
		dots: false,
		nav: true,
		navSpeed: 500,
		smartSpeed: 500,
		autoplaySpeed: 500,
		responsive : {
			1024 : {
				items: 4,
				margin: 30
			},
			768 : {
				items: 3,
				margin: 15
			},
			480 : {
				items: 2,
				margin: 15
			},
			0 : {
				items: 2,
				margin: 15
			}
		}
	})


	$('.products .slider.carousel2').owlCarousel({
		loop: false,
		rewind: true,
		dots: false,
		nav: true,
		navSpeed: 500,
		smartSpeed: 500,
		autoplaySpeed: 500,
		responsive : {
			1200 : {
				items: 6,
				margin: 30
			},
			1024 : {
				items: 5,
				margin: 30
			},
			768 : {
				items: 4,
				margin: 15
			},
			480 : {
				items: 2,
				margin: 30
			},
			0 : {
				items: 2,
				margin: 30
			}
		}
	})


	// Карусель идюстраторов
	$('.illustrators .slider').owlCarousel({
		loop: true,
		dots: false,
		nav: true,
		navSpeed: 500,
		smartSpeed: 500,
		autoplaySpeed: 500,
		margin: 0,
		responsive : {
			1024 : {
				items: 3
			},
			768 : {
				items: 2
			},
			0 : {
				items: 1
			}
		}
	})


	// Поиск
	$('header .search .search_link').click(function(e){
		if( $(this).hasClass('active') ){
			$(this).removeClass('active').next().fadeOut(200)
		} else{
			$('.mini_modal').fadeOut(300)
			$('.mini_modal_link').removeClass('active')
			firstClick = false

			$('header .mob_menu_link').removeClass('active')
			$('header .info .col.left').slideUp()

			$(this).addClass('active').next().fadeIn(300)
		}
	})


	//Мини всплывающие окна
	firstClick = false;

	///////////////////////////////////////////////////////
	$(document).on('click', '.mini_modal_link', function(e){
		/////////////////////////////////////////////////////////

		e.preventDefault()

		var modalId = $(this).attr('data-modal-id')
		if($(modalId).is(':visible')){
			$(this).removeClass('active')
			$('.mini_modal').fadeOut(200)
			firstClick = false
		}else{
			$('.mini_modal_link').removeClass('active')
			$(this).addClass('active')

			$('.mini_modal').fadeOut(200)
			$(modalId).fadeIn(300)
			firstClick = true
		}
	})

	//Закрываем всплывашку при клике вне неё
	$(document).click(function(e){
		if (!firstClick && $(e.target).closest('.mini_modal').length == 0){
			$('.mini_modal').fadeOut(300)
			$('.mini_modal_link').removeClass('active')
		}
		firstClick = false
	})


	// Всплывающие окна
	///////////////////////////////////////////////////////
	$(document).on('click', '.modal_link', function(e){
		var tov_id = $(this).attr('tov_id');
		if ( tov_id ){
			if( $('#buy_1_click_modal form input[name=tov_id]').length > 0 ){
				$('#buy_1_click_modal form input[name=tov_id]').val(tov_id);
			} else {
				$('#buy_1_click_modal form').append('<input type="hidden" name="tov_id" value="'+tov_id+'">');
			}
		}
		e.preventDefault();
		$.fancybox.close();
		$.fancybox.open({
			src  : $(this).attr('href'),
			type : 'inline',
			opts : {
				speed: 300,
				autoFocus : false,
				i18n : {
					'en' : {
						CLOSE : 'Закрыть'
					}
				}
			}
		})
	})
	/////////////////////////////////////////////////////////


	$('.ajax_modal').click(function(e){
		e.preventDefault()

		$.fancybox.close()

		$.fancybox.open({
			src  : $(this).attr('href'),
			type : 'ajax',
			opts : {
				speed: 300,
				autoFocus : false,
				i18n : {
					'en' : {
						CLOSE : 'Закрыть'
					}
				}
			}
		})
	})


	$('.fancy_img').fancybox({
		transitionEffect : 'slide',
		animationEffect : 'fade',
		i18n : {
			'en' : {
				CLOSE : 'Закрыть'
			}
		}
	})


	// Авторизация
	$('#login_modal form').submit(function(e){
		e.preventDefault()

		$.fancybox.close()

		$.fancybox.open({
			src  : '#login_success',
			type : 'inline',
			opts : {
				speed: 300,
				autoFocus : false,
				i18n : {
					'en' : {
						CLOSE : 'Закрыть'
					}
				}
			}
		})
	})

	// Авторизация
	$('#register_modal form').submit(function(e){
		e.preventDefault()

		$.fancybox.close()

		$.fancybox.open({
			src  : '#order_success',
			type : 'inline',
			opts : {
				speed: 300,
				autoFocus : false,
				i18n : {
					'en' : {
						CLOSE : 'Закрыть'
					}
				}
			}
		})
	})

	// Товар в корзину
	/*$('.product_info .buy_link').click(function(e){
		e.preventDefault()

		$.fancybox.close()

		$.fancybox.open({
			src  : '#cart_success',
			type : 'inline',
			opts : {
				speed: 300,
				autoFocus : false,
				i18n : {
					'en' : {
						CLOSE : 'Закрыть'
					}
				}
			}
		})
	})*/

	$('.modal .close').click(function(e){
		e.preventDefault()

		$.fancybox.close()
	})





	// Личный кабинет
	$('.lk_orders table tbody .order_link > td').click(function(e){
		e.preventDefault()

		var parent = $(this).parents('.order_link')

		if( parent.hasClass('active') ) {
			parent.removeClass('active').next().fadeOut(200)
		} else {
			$('.lk_orders table tbody .order_link').removeClass('active')
			$('.lk_orders table .order_data').hide()

			parent.addClass('active').next().fadeIn(300)
		}
	})



	// Маска ввода
	$('input[type=tel]').mask('999 999 9999')


	// Кастомный select
	$('.form select, .cities select, .delivery_page .city select').niceSelect()


	// Изменение количества товара
	/*$('.amount .minus').click(function(e){
    e.preventDefault()

    var input = $(this).parents('.amount').find('input')
    var inputVal = parseInt(input.val())
    var minimum = parseInt(input.data('minimum'))

    if(inputVal > minimum){
      input.val(inputVal-1)
    }
  })

  $('.amount .plus').click(function(e){
    e.preventDefault()

    var input = $(this).parents('.amount').find('input')
    var inputVal = parseInt(input.val())
    var maximum = parseInt(input.data('maximum'))

    if(inputVal < maximum){
      input.val(inputVal-(-1))
    }
  })*/


	// Кастомный скроллинг
	$('.filter .scroll').each(function(){
		if( $(this).find('.line').size() > 7 ) {
			$(this).slimScroll({
				height: 265,
				position: 'right',
				railVisible: true,
				alwaysVisible: true,
				color: '#414141',
				size: '5px',
				distance: '0',
				railColor: '#e1e1e1',
				railOpacity: 1
			})
		}
	})

	$('.cities .scroll').each(function(){
		if( $(this).find('.item').size() > 2 ) {
			$(this).slimScroll({
				height: 307,
				position: 'right',
				railVisible: true,
				alwaysVisible: true,
				color: '#414141',
				size: '5px',
				distance: '0',
				railColor: '#e1e1e1',
				railOpacity: 1
			})
		}
	})

	$('.configurator .collections .scroll').each(function(){
		if( $(this).find('.line').size() > 7 ) {
			$(this).slimScroll({
				height: 265,
				position: 'right',
				railVisible: true,
				alwaysVisible: true,
				color: '#414141',
				size: '5px',
				distance: '0',
				railColor: '#e1e1e1',
				railOpacity: 1
			})
		}
	})


	// Карточка товара
	/*$product_info = $('.product_info .images .big .slider').owlCarousel({
		loop: false,
	    nav: false,
	    navSpeed: 500,
	    smartSpeed: 500,
	    autoplaySpeed: 500,
	    items: 1,
	    margin: 30,
	    onTranslate: function(event){
	    	$('.product_info .images .thumbs a').removeClass('active')
	    	$('.product_info .images .thumbs a:eq('+ event.item.index +')').addClass('active')
	    },
	    responsive : {
		    768 : {
		        dots: false,
		        autoHeight: false
		    },
		    0 : {
		        dots: true,
		        autoHeight: true
		    }
		}
	})

	$('.product_info .images .thumbs a').click(function(e) {
		e.preventDefault()

		$('.product_info .images .thumbs a').removeClass('active')
		$(this).addClass('active')

	    $product_info.trigger('to.owl.carousel', $(this).attr('data-slide-index'))
	})*/


	// Аккордион
	$('.accordion .name').click(function(e){
		e.preventDefault()

		if( $(this).hasClass('active') ) {
			$(this).removeClass('active').next().slideUp(200)
		} else {
			$('.accordion .name').removeClass('active')
			$('.accordion .val').slideUp(200)

			$(this).addClass('active').next().slideDown(300)
		}
	})


	// Фильтр
	if( $(window).width() < 1024 ) {
		$('aside .filter .name').click(function(e){
			e.preventDefault()

			if( $(this).hasClass('active') ) {
				$(this).removeClass('active')
				$(this).parents('.item').find('.data').slideUp(200)
			} else {
				$('aside .filter .name').removeClass('active')
				$('aside .filter .data').slideUp(200)

				$(this).addClass('active')
				$(this).parents('.item').find('.data').slideDown(300)
			}
		})
	}


	// Шапка - категории
	if( $(window).width() < 1024 ) {
		$('header nav .menu_item > .sub_link').click(function(e){
			e.preventDefault()

			if( $(this).hasClass('active') ) {
				$(this).removeClass('active').next().fadeOut(200)
			} else {
				$('header nav .menu_item > .sub_link').removeClass('active')
				$('header nav .sub_cats').fadeOut(200)

				$(this).addClass('active').next().fadeIn(300)
			}
		})
	}


	// Скроллинг к якорю
	$('.scroll_link').click(function(e){
		e.preventDefault()

		$('html, body').stop().animate({
			scrollTop: $( $(this).attr('href') ).offset().top - 40
		}, 1000)
	})


	// Моб. меню
	$('header .mob_menu_link').click(function(e){
		e.preventDefault()

		if( $(this).hasClass('active') ){
			$(this).removeClass('active')
			$('header .info .col.right, header .gender').fadeOut(200)
		} else{
			$('header .search .search_link').removeClass('active')
			$('header .search form').fadeOut(200)

			$(this).addClass('active')
			$('header .info .col.right, header .gender').fadeIn(300)
		}
	})


	// Конфигуратор
	/*$('.configurator .steps .step').click(function(e){
    e.preventDefault()

    var content = $(this).attr('href')

    $('.configurator .steps .step').removeClass('active')
    $(this).addClass('active')

    $('.configurator .step_info').hide()
    $('.configurator ' + content).fadeIn(300)
  })

  $('.configurator .next_link').click(function(e){
    e.preventDefault()

    var content = $(this).attr('href')
    var index = $(this).data('index')

    $('.configurator .steps .step').removeClass('active')
    $('.configurator .steps .step:eq(' + index + ')').addClass('active')

    $('.configurator .step_info').hide()
    $('.configurator ' + content).fadeIn(300)
  })*/


	$('.configurator .type label').click(function(){
		var content = $(this).data('content')

		$('.configurator .type_content').hide()
		$('.configurator ' + content).fadeIn(300)
	})


	$('.configurator .file input[type=file]').change(function(){
		var image = $(this).val()

		$(this).next().find('.name').text( image )
	})


	$('.configurator .model .slider').owlCarousel({
		loop: false,
		dots: false,
		nav: true,
		navSpeed: 500,
		smartSpeed: 500,
		autoplaySpeed: 500,
		margin: 0,
		responsive : {
			480 : {
				items: 2,
			},
			0 : {
				items: 1
			}
		}
	})

	 
	// Блок в карточке товара
	// $('.product_info .related_cats .more').click(function(e){
	////////////////////////////////////////////////////////////
	$(document).on('click', '.product_info .related_cats .more', function(e){
		////////////////////////////////////////////////////////////
		e.preventDefault()

		$(this).parents('.grid').find('.item.hide').slideDown(300)
		$(this).hide()
	})

})
