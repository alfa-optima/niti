var fileInputName = 'persPhotoInput';

// Загрузка фото
function onResponse(d) {
	eval('var obj = ' + d + ';');
	if (obj.status == 'ok'){
		
		process(true);
		$.post("/ajax/ajax.php", {action:"getPersPhotoBlock", fid:obj.fid}, function(data) {
			if (data.status == 'ok'){
				// вставляем фото
				$('.persPhotoBlock').html(data.html);
				// Очищаем файловый инпут
				$('input[name='+fileInputName+']').val('');
			}
		}, 'json')
		.fail(function(data, status, xhr){
			show_error( $('form.'+form_class) , 'no___field', 'Ошибка запроса');
		})
		.always(function(data, status, xhr){
			process(false);
		})
	} else{
		// Вывод ошибки
		//var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: obj.status});
		
		errorWindow( obj.status );
	}
}


// Загрузка фото
$(document).on('click', 'a.persPhotoSelect', function(){
	$('input[name='+fileInputName+']').val('');
	$('input[name='+fileInputName+']').trigger('click');
})
$(document).on('change', 'input[name='+fileInputName+']', function(){
	$(".persUpload___form").submit();
})


// Удаление фото
$(document).on('click', '.persPhotoDeleteButton', function(){
	if (!is_process(this)){
		$.noty.closeAll(); 
		
		var n = noty({
			text        : 'Удалить фото?',
			type        : 'alert',
			dismissQueue: true,
			layout      : 'center',
			theme       : 'defaultTheme',
			buttons     : [
				{addClass: 'btn btn-primary', text: 'Да', onClick: function ($noty) {
					$noty.close();
					$.post("/ajax/ajax.php", {action:"persPhotoDelete"}, function(data){
						if (data.status == 'ok'){
							$('.persPhotoBlock').html(data.html);
						} else if (data.status == 'error'){
							//var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: data.error_text});
							
							errorWindow( data.error_text );
						}
					}, 'json')
					.fail(function(data, status, xhr){
						//var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: "Ошибка запроса..."});
						
						errorWindow( "Ошибка запроса..." );
					})
					.always(function(data, status, xhr){
						process(false);
					})
				}},
				{addClass: 'btn btn-danger', text: 'Нет', onClick: function ($noty) {
					$noty.close();
				}}
			]
		});

	}
})





// Редактирование личных данных №1
$(document).on('click', '.personal_edit_button_1', function(){
	if (!is_process(this)){
		// Форма
		var this_form = $(this).parents('form');
		// Подготовка
		$(this_form).find("input[type=text], input[type=tel], input[type=email], input[type=password], textarea").removeClass("is___error");
		$(this_form).find('p.error___p').html('');
		// Задаём переменные
		var NAME = $(this_form).find("input[name=NAME]").val();
		var EMAIL = $(this_form).find("input[name=EMAIL]").val();
		var PERSONAL_PHONE = $(this_form).find("input[name=PERSONAL_PHONE]").val();
		var error = false;
		
		// Проверки
		if (NAME.length == 0){
			error = ['NAME',   'Введите, пожалуйста, Ваше имя!'];
		} else if (EMAIL.length == 0){
			error = ['EMAIL',   'Введите, пожалуйста, Ваш Email!'];
		} else if (EMAIL.length > 0 && !(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(EMAIL))){
			error = ['EMAIL',   'Email содержит ошибки!'];
		} else if (PERSONAL_PHONE.length == 0){
			error = ['PERSONAL_PHONE',   'Введите, пожалуйста, Ваш телефон!'];
		}
		
		// Если нет ошибок
		if (!error){
			process(true);
			//$.noty.closeAll(); 
			var form_data = $(this_form).serialize();
			$.post("/ajax/ajax.php", {action:"personal_edit", form_data:form_data}, function(data){
				if (data.status == 'ok'){
					//var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'topRight', type: 'success', text: "Успешное сохранение"});
					
					successWindow( "Успешное сохранение" );
				} else if (data.status == 'error'){
					show_error(this_form, false, data.error_text);
				}
			}, 'json')
			.fail(function(data, status, xhr){
				//var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: "Ошибка запроса..."});
				
				errorWindow( "Ошибка запроса..." );
			})
			.always(function(data, status, xhr){
				process(false);
			})
		// Ошибка
		} else {
			show_error(this_form, error[0], error[1]);
		}
		
	}
})



// Редактирование личных данных №2
$(document).on('click', '.personal_edit_button_2', function(){
	if (!is_process(this)){
		// Форма
		var this_form = $(this).parents('form');
		// Подготовка
		$(this_form).find("input[type=text], input[type=tel], input[type=email], input[type=password], textarea").removeClass("is___error");
		$(this_form).find('p.error___p').html('');
		// Задаём переменные
		var UF_COUNTRY = $(this_form).find("select[name=UF_COUNTRY]").val();
		var PERSONAL_CITY = $(this_form).find("input[name=PERSONAL_CITY]").val();
		var PERSONAL_ZIP = $(this_form).find("input[name=PERSONAL_ZIP]").val();
		var PERSONAL_STREET = $(this_form).find("input[name=PERSONAL_STREET]").val();
		var UF_HOUSE = $(this_form).find("input[name=UF_HOUSE]").val();
		var error = false;
		
		// Проверки
		if (UF_COUNTRY == 'error'){
			error = ['UF_COUNTRY',   'Выберите, пожалуйста, страну доставки!'];
		} else if (PERSONAL_CITY.length == 0){
			error = ['PERSONAL_CITY',   'Введите, пожалуйста, Ваш город!'];
		} else if (PERSONAL_ZIP.length == 0){
			error = ['PERSONAL_ZIP',   'Введите, пожалуйста, Ваш почтовый индекс!'];
		} else if (PERSONAL_STREET.length == 0){
			error = ['PERSONAL_STREET',   'Введите, пожалуйста, Вашу улицу!'];
		} else if (UF_HOUSE.length == 0){
			error = ['UF_HOUSE',   'Введите, пожалуйста, номер Вашего дома!'];
		}
		
		// Если нет ошибок
		if (!error){
			process(true);
			//$.noty.closeAll(); 
			var form_data = $(this_form).serialize();
			$.post("/ajax/ajax.php", {action:"personal_edit", form_data:form_data}, function(data){
				if (data.status == 'ok'){
					//var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'topRight', type: 'success', text: "Успешное сохранение"});
					
					successWindow( "Успешное сохранение" );
				} else if (data.status == 'error'){
					show_error(this_form, false, data.error_text);
				}
			}, 'json')
			.fail(function(data, status, xhr){
				//var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: "Ошибка запроса..."});
				
				errorWindow( "Ошибка запроса..." );
			})
			.always(function(data, status, xhr){
				process(false);
			})
		// Ошибка
		} else {
			show_error(this_form, error[0], error[1]);
		}
		
	}
})



// Редактирование личных данных №3
$(document).on('click', '.personal_edit_button_3', function(){
	if (!is_process(this)){
		// Форма
		var this_form = $(this).parents('form');
		// Подготовка
		$(this_form).find("input[type=text], input[type=tel], input[type=email], input[type=password], textarea").removeClass("is___error");
		$(this_form).find('p.error___p').html('');
		// Задаём переменные
		var PASSWORD = $(this_form).find("input[name=PASSWORD]").val();
		var CONFIRM_PASSWORD = $(this_form).find("input[name=CONFIRM_PASSWORD]").val();
		var error = false;
		
		// Проверки
		if (PASSWORD.length == 0){
			error = ['PASSWORD',   'Введите, пожалуйста, новый пароль!'];
		} else if (PASSWORD.length > 0 && PASSWORD.length < 8){
			error = ['PASSWORD',   'Длина пароля - не менее 8 символов!'];
		} else if (CONFIRM_PASSWORD.length == 0){
			error = ['CONFIRM_PASSWORD',   'Повторите, пожалуйста, пароль!'];
		} else if (PASSWORD != CONFIRM_PASSWORD){
			error = ['CONFIRM_PASSWORD',   'Пароли отличаются!'];
		}
		
		// Если нет ошибок
		if (!error){
			process(true);
			//$.noty.closeAll(); 
			var form_data = $(this_form).serialize();
			$.post("/ajax/ajax.php", {action:"personal_edit", form_data:form_data}, function(data){
				if (data.status == 'ok'){
					//var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'topRight', type: 'success', text: "Успешное сохранение"});
					$(this_form).find("input[type=password]").val('');
					
					successWindow( "Успешное сохранение" );
				} else if (data.status == 'error'){
					show_error(this_form, false, data.error_text);
				}
			}, 'json')
			.fail(function(data, status, xhr){
				//var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: "Ошибка запроса..."});
				
				errorWindow( "Ошибка запроса..." );
			})
			.always(function(data, status, xhr){
				process(false);
			})
		// Ошибка
		} else {
			show_error(this_form, error[0], error[1]);
		}
		
	}
})


