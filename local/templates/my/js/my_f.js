var error_interval;
var orderMap;


function infoRequest() {
  $.post("/ajax/constructorInfo.php", {}, function (data) {
    console.log(data);
  }, 'json')
}

function noteRequest(action, id, params, page, limit, page_el_cnt) {
  $.post("/ajax/constructorNote.php", {
    action: action,
    id: id,
    params: params,
    page: page,
    limit: limit,
    page_el_cnt: page_el_cnt,
    isJS: 'Y'
  }, function (data) {
    console.log(data);
  }, 'json')
}

function fileRequest(action, id, base64) {
  $.post("/ajax/constructorFile.php", {action: action, id: id, base64: base64, isJS: 'Y'}, function (data) {
    console.log(data);
  }, 'json')
}

function goodsRequest(action, note_id) {
  $.post("/ajax/constructorGoods.php", {action: action, note_id: note_id, isJS: 'Y'}, function (data) {
    console.log(data);
  }, 'json')
}


function process(attr) {
  if (attr == true) {
    $('.to___process').addClass('is___process');
  } else if (attr == false) {
    $('.to___process').removeClass('is___process');
  }
}

function is_process(el) {
  if (el) {
    return $(el).hasClass('is___process');
  } else {
    return false;
  }
}

function show_error(this_form, field_name, error, show_noty, scroll) {
  $(this_form).find("p.error___p").html(error);
  $(this_form).find('.success___p').html('');
  if (this_form && field_name) {
    var field_type = $(this_form).find('[name=' + field_name + ']')[0].tagName;
    $(this_form).find(field_type + '[name=' + field_name + ']').addClass('error').attr((field_type == 'select' ? 'onchange' : 'oninput'), '$(this).removeClass(\'error\'); $(this).attr("title", ""); $(this).parents("form").find("p.error___p").html("")').attr('title', error);
    if (scroll) {
      if ($(this_form).find(field_type + '[name=' + field_name + ']').length > 0) {
        var destination = $(this_form).find(field_type + '[name=' + field_name + ']').offset();
        $('body, html').animate({scrollTop: destination.top - 30}, 400);
      }
    }
  }
  if (show_noty) {
    //$.noty.closeAll();
    //var n = noty({closeWith: ['hover'], timeout: 6000, layout: 'center', type: 'warning', text: error});

    errorWindow(error);
  }
}

function show_success(this_form, message, show_noty) {
  if (this_form) {
    $(this_form).find("p.error___p").html('');
    $(this_form).find('.success___p').html(message);
  }
  if (show_noty) {
    //var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'success', text: message});

    errorWindow(message);
  }
}


function successWindow(message) {
  $.fancybox.close();
  clearInterval(error_interval);
  $('#successWindow div.title').html(message);
  $.fancybox.open({
    src: '#successWindow',
    type: 'inline',
    opts: {
      speed: 300,
      autoFocus: false,
      i18n: {
        'en': {
          CLOSE: 'Закрыть'
        }
      }
    }
  })
}

function errorWindow(message) {
  $.fancybox.close();
  clearInterval(error_interval);
  $('#errorWindow div.title').html(message);
  $.fancybox.open({
    src: '#errorWindow',
    type: 'inline',
    opts: {
      speed: 300,
      autoFocus: false,
      i18n: {
        'en': {
          CLOSE: 'Закрыть'
        }
      }
    }
  })
  if (message.indexOf('Ошибка запроса') != -1) {
    error_interval = setInterval(function () {
      clearInterval(error_interval);
      $.fancybox.close();
    }, 5000);
  }
}

function messageWindow(message) {
  $.fancybox.close();
  $('#messageWindow div.title').html(message);
  $.fancybox.open({
    src: '#messageWindow',
    type: 'inline',
    opts: {
      speed: 300,
      autoFocus: false,
      i18n: {
        'en': {
          CLOSE: 'Закрыть'
        }
      }
    }
  })
}

function modalNewMarkup1(message) {
  $.fancybox.close();
  $('#modalNewMarkup1 div.title').html(message);
  $.fancybox.open({
    src: '#modalNewMarkup1',
    type: 'inline',
    opts: {
      speed: 300,
      autoFocus: false,
      i18n: {
        'en': {
          CLOSE: 'Закрыть'
        }
      }
    }
  })
}

function modalNewMarkup2(message) {
  $.fancybox.close();
  $('#modalNewMarkup2 div.title').html(message);
  $.fancybox.open({
    src: '#modalNewMarkup2',
    type: 'inline',
    opts: {
      speed: 300,
      autoFocus: false,
      i18n: {
        'en': {
          CLOSE: 'Закрыть'
        }
      }
    }
  })
}

function modalNewMarkup3(message) {
  $.fancybox.close();
  $('#modalNewMarkup3 div.title').html(message);
  $.fancybox.open({
    src: '#modalNewMarkup3',
    type: 'inline',
    opts: {
      speed: 300,
      autoFocus: false,
      i18n: {
        'en': {
          CLOSE: 'Закрыть'
        }
      }
    }
  })
}

function create_form(form_class, method, action) {
  var methodAttr = '';
  if (method && method.length > 0) {
    methodAttr = ' method="' + method + '"';
  }
  if (action && action.length > 0) {
    actionAttr = ' action="' + action + '"';
  }
  if ($(form_class).length > 0) {
    $('.' + form_class).html('');
  } else {
    $('body').append('<form class="' + form_class + '" ' + methodAttr + ' ' + actionAttr + '></form>');
  }
  return $('.' + form_class);
}

function get_pureURL(URL) {
  if (!URL) {
    URL = document.URL;
  }
  var arURI = URL.split("?");
  var pureURI = arURI[0];
  return pureURI;
}


function addToRequestURI(URL, pm, val) {
  var arURI = URL.split("?");
  var pureURI = arURI[0];
  if (URL.indexOf("?") != -1) {
    var arRequests = arURI[1].split("&");
    var arNewRequests = [];
    var cnt = -1;
    $.each(arRequests, function (key, request) {
      arTMP = request.split("=");
      if (arTMP[0] != pm && request.length > 0) {
        cnt++;
        arNewRequests[cnt] = request;
      }
    })
    cnt++;
    arNewRequests[cnt] = pm + "=" + val;
    return pureURI + "?" + arNewRequests.join("&");
  } else {
    return pureURI + "?" + pm + "=" + val;
  }
}


// Функция number_format для форматирования чисел
// Формат: number_format(1234.56, 2, ',', ' ');
function number_format(number, decimals, dec_point, thousands_sep) {
  number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
  var n = !isFinite(+number) ? 0 : +number,
      prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
      sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
      dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
      s = '',
      toFixedFix = function (n, prec) {
        var k = Math.pow(10, prec);
        return '' + (Math.round(n * k) / k)
            .toFixed(prec);
      };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
      .split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '')
      .length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1)
        .join('0');
  }
  return s.join(dec);
}


function minus_zero(this_input, not_empty_value) {
  var input_value = $(this_input).val();
  input_value = input_value.toString().replace(/[^\d;]/g, '');
  $(this_input).val(input_value);
  var dlina = input_value.length;
  if (dlina > 0) {
    stop = false;
    for (var n = 1; n <= dlina; n++) {
      if (stop == false) {
        if (input_value.substring((n - 1), n) == 0) {
          var new_str = input_value.substring(n, dlina);
          if (not_empty_value) {
            $(this_input).val((new_str.length == 0) ? 1 : new_str);
          } else {
            $(this_input).val((new_str.length == 0) ? '' : new_str);
          }
        } else {
          stop = true;
        }
      }
    }
  } else if (not_empty_value) {
    $(this_input).val(1);
  }
}


function panel_regulation(wind_top) {
  var panel_html = $('#panel').html();
  if (panel_html.length > 0) {
    $('#bx-panel-expander').attr('onmouseout', 'panel_regulation();');
    $('#bx-panel-hider').attr('onmouseout', 'panel_regulation();');
    var panel_height = $('#panel').outerHeight(true);
    if (panel_height > 0) {
      if (!wind_top || wind_top < 10) {
        $('header').css('margin-top', panel_height + 'px');
      } else {
        $('header').css('margin-top', '0');
      }
    }
  }
}


function in_array(value, array) {
  for (var i = 0; i < array.length; i++) {
    if (array[i] == value) return true;
  }
  return false;
}


function add_to_basket(tov_id, button) {
  process(true);
  //$.noty.closeAll();
  $.post("/ajax/ajax.php", {action: 'add_to_basket', 'tov_id': tov_id}, function (data) {
    if (data.status == 'ok') {

      /*var button_html = $(button).html();
      button_html = button_html.replace("перейти в корзину", "в корзину");
      button_html = button_html.replace("в корзину", "перейти в корзину");
      $(button).html(button_html);*/

      /*$('#cart_success .product').html(data.tov_name);

      $.fancybox.open({
        src  : '#cart_success',
        type : 'inline',
        opts : {
          speed: 300,
          autoFocus : false,
          i18n : { 'en' : { CLOSE : 'Закрыть' } }
        }
      })*/

      $('.basket_small_block').replaceWith(data.top_basket_html);

      $('.modal___error_p').html('');
      $(".modalErrorSizeYellow-empty").addClass('modalErrorSizeYellow');
      $(".modalErrorSizeYellow-empty").removeClass('modalErrorSizeYellow');
      $(".modalErrorBoxYellow-empty").addClass('modalErrorBoxYellow');
      $(".modalErrorBoxYellow-empty").removeClass('modalErrorBoxYellow');

      $.cookie('go_to_basket', 0, {path: '/'});

      $('.mini_modal_link').trigger('click');

    }
  }, 'json')
      .fail(function (data, status, xhr) {
        errorWindow('Ошибка запроса');
      })
      .always(function (data, status, xhr) {
        process(false);
      })

}


function addToFavorites(tov_id) {
  process(true);
  //$.noty.closeAll();
  $.post("/ajax/ajax.php", {action: "changeFavorites", tov_id: tov_id, act: 'add'}, function (data) {
    if (data.status == 'ok') {
      $('.fav___link[tov_id=' + tov_id + ']').addClass('active');
    }
  }, 'json')
      .fail(function (data, status, xhr) {
        //var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'topRight', type: 'warning', text: "Ошибка запроса..."});

        errorWindow('Ошибка запроса...');
      })
      .always(function (data, status, xhr) {
        process(false);
      })
}


function delFromFavorites(tov_id) {
  process(true);
  //$.noty.closeAll();
  $.post("/ajax/ajax.php", {action: "changeFavorites", tov_id: tov_id, act: 'del'}, function (data) {
    if (data.status == 'ok') {
      $('.fav___link[tov_id=' + tov_id + ']').removeClass('active');
    }
  }, 'json')
      .fail(function (data, status, xhr) {
        //var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'topRight', type: 'warning', text: "Ошибка запроса..."});

        errorWindow('Ошибка запроса...');
      })
      .always(function (data, status, xhr) {
        process(false);
      })
}


function setFavorites() {
    if ($('.fav___link').length > 0) {
          $('.fav___link').removeClass('active');
          var favStr = $.cookie('BITRIX_SM_favorites');
          if (favStr != undefinded && favStr.length > 0) {
              var fav = favStr.split('|');
          } else {
              var fav = [];
          }
          $('.fav___link').each(function () {
              var tov_id = $(this).attr('tov_id');
              if (in_array(tov_id, fav)) {
                  $('.fav___link[tov_id=' + tov_id + ']').addClass('active');
              }
          })
    }
}


function open_left_menu() {
  var cur_URL = get_pureURL();
  var domain = location.protocol + '//' + location.hostname;
  if ($('.left_sections_ul').length > 0) {
    $('.left_sections_ul').each(function () {
      var parent_url = domain + $(this).attr('parent_url');
      if (cur_URL.indexOf(parent_url) == 0) {
        $(this).show();
        $(this).prev().addClass('active');

        if ($(this).find('a').length > 0) {
          $(this).find('a').each(function () {
            var href = domain + $(this).attr('href');
            if (href == cur_URL) {
              $(this).addClass('active');
            }
          })
        }

      }
    })
  }
}


function basket_sum() {

  var basket_sum = 0;
  var basket_disc_sum = 0;

  $('.basket___table tbody tr').each(function () {

    var quantity_input = $(this).find('input[name=quantity]');

    var quantity = $(quantity_input).val();
    var price = $(quantity_input).attr('price');
    var disc_price = $(quantity_input).attr('disc_price');

    basket_sum += price * quantity;
    basket_disc_sum += disc_price * quantity;

  })

  var discount = basket_sum - basket_disc_sum;

  $('.total___sum').html(number_format(basket_disc_sum, 0, ',', ' ') + ' <span class="currency">:</span>');
  $('.discount___sum').html(number_format(discount, 0, ',', ' ') + ' <span class="currency">:</span>');

}


function update_basket_quantity(item_id, quantity, pure_url, focus) {
  process(true);
  //$.noty.closeAll();
  $.post("/ajax/ajax.php", {
    action: "recalc_qt",
    item_id: item_id,
    quantity: quantity,
    pure_url: pure_url
  }, function (data) {
    if (data.status == 'ok') {
      $('.basket_small_block').replaceWith(data.top_basket_html);
      $('.basket___table tbody').html(data.basket_itmes_html);
      basket_sum();
      if (focus) {
        var input = document.querySelector('input.quantity_input_' + item_id);
        input.focus();
        input.selectionStart = input.value.length;
      }
    }
  }, 'json')
      .fail(function (data, status, xhr) {
        //var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'topRight', type: 'warning', text: "Ошибка запроса..."});

        errorWindow('Ошибка запроса...');
      })
      .always(function (data, status, xhr) {
        process(false);
      })
}


// calcDeliveryPrice
function calcDeliveryPrice() {
  var loc_id = $('input[name=loc_id]').val();
  var ds = $('input[name=ds]:checked').val();

  var json_basket = $('input[name=json_basket]').val();

  process(true);
  $('.repeat___block').hide();
  $('input[name=calc_error]').val('N');
  $.post("/ajax/calcDeliveryPrice.php", {
    loc_id: loc_id,
    ds: ds,
    json_basket: json_basket
  }, function (data) {
    if (data.status == 'ok') {
      $('aside.right').html(data.right_basket);
      $('input[name=delivery_price]').val(data.delivery_price);
    }
    if (
        data.res_status == 'request_error'
        ||
        data.res_status == 'calc_error'
    ) {
      $('.repeat___block').show();
      $('input[name=calc_error]').val('Y');
    }
  }, 'json')
      .fail(function (data, status, xhr) {
      })
      .always(function (data, status, xhr) {
        process(false);
      })
}


// update_right_basket
function update_right_basket() {

  var deliv_price = $('input[name=delivery_price]').val();
  var delivery_price = deliv_price.length > 0 ? deliv_price : '-';

  process(true);
  $.post("/ajax/ajax.php", {action: "update_right_basket", delivery_price: delivery_price}, function (data) {
    if (data.status == 'ok') {
      $('aside.right').html(data.html);
    }
  }, 'json')
      .fail(function (data, status, xhr) {
      })
      .always(function (data, status, xhr) {
        process(false);
      })
}



function t() {

  var d = [
    {name: "data[new][imya]", value: "Вася1"},
    {name: "data[new][phone]", value: "+7 (234) 234-23-40"},
    {name: "data[new][page_link]", value: "https://haton.ru/kredit-pod-zalog-avtomobilya/1"},
    {name: "data[new][page_name]", value: "Кредит под залог автомобиля1"},
    {name: "system_form_id", value: "2151"},
    {name: "agree", value: "on1"}
  ]

  for (var i in d){

    $('.form.ajax_submit').first().find('*[name="' + d[i].name  +'"]').val(d[i].value)
    console.log($('.form.ajax_submit').first().find('*[name="' + d[i].name + '"]').val());

  }

}

t()


function aaa(){
  $.get("/ajax/productObject.php", {item_id: 625353}, function (data) {

    console.log(data);

  }, 'json')
      .fail(function (data, status, xhr) {
      })
      .always(function (data, status, xhr) {
        process(false);
      })
}

//aaa()

function product_detail_info() {

//alert(1)

  if ($('.product_detail_block').length > 0) {
    var item_id = $('.product_detail_block').attr('item_id');
    //alert(item_id)

    process(true);
    $.post("/ajax/product_detail_info.php", {item_id: item_id}, function (data) {
      if (data.status == 'ok') {
        var related_right_block = data.related_right_block;
        $('.related_right_block').html(related_right_block);
        if (related_right_block.length > 0) {
          $('.related_right_block').attr('style', '');
        }
        $('.tabs_container').each(function () {
          $(this).find('.tab_content.active').show()
        })
      }
    }, 'json')
        .fail(function (data, status, xhr) {
        })
        .always(function (data, status, xhr) {
          process(false);
        })
  }
}


function autocompleteParams() {
  return {
    source: '/ajax/getLocations.php?ds=' + $('input[name=ds]:checked').val(),
    delay: 700,
    minLength: 2,
    search: function () {
      $('.search___status').hide();
    },
    search: function () {
      $('input[name=loc_id]').val('');
      $('.search___status').hide();
    },
    select: function (event, ui) {
      var name = ui.item.label;
      var ar = name.split('[');
      name = $.trim(ar[0]);
      var val = ar[1];
      ar = val.split(']');
      var id = $.trim(ar[0]);
      $('input[name=loc_id]').val(id);
      $('input[name=loc_name]').val(name);
      calcDeliveryPrice();
      $('#loc_name').blur();
      return false;
    }
  };
}


function getPaymentButton(order_id, pay_sum) {
  $.post("/ajax/getPaymentButton.php", {order_id: order_id}, function (data) {
    if (data) {

      $('.pay___block').html(data);

      $('.cloudpay_button').addClass('pay___button');
      $('.cloudpay_button').html('Оплатить' + (pay_sum > 0 ? (' (' + pay_sum + ' руб.)') : ''));

    }
  })
}


function getDeliveryVariants(loc_id, zip) {

  process(true);
  zip_process = true;

  var params = {
    action: "getDeliveryVariants",
    loc_id: loc_id,
    zip: zip
  };

  $('.order_delivery_block').html('<p class="no___count" style="text-align: left; margin: 20px 10px;">Загрузка...</p>');
  $('form.order___form:visible p.error___p').html('');

  $.post("/ajax/ajax.php", params, function (data) {
    if (data.status == 'ok') {

      $('.order_delivery_block').replaceWith(data.delivery_block_html);

      zip_process = false;

      //basketCalc();

    } else if (data.status == 'error') {
      var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: data.text});
    }
  }, 'json')
      .fail(function (data, status, xhr) {
        var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: "Ошибка запроса"});
      })
      .always(function (data, status, xhr) {
        process(false);
        $('.order_delivery_block p.no___count').remove();
      })
}


function calcPVZprice(pvz_code, pvz_address) {
  process(true);
  var params = {
    action: "calcPVZprice",
    pvz_code: pvz_code
  };
  $('form.order___form:visible p.error___p').html('');
  $('.pvz___desc').html('');
  $('.pvz___input').attr('price', '');
  $('input[name=order_pvz_code]').val('');
  $('input[name=order_pvz_address]').val('');
  $('input[name=order_pvz_price]').val('');
  $('input[name=order_pvz_period]').val('');
  $('input[name=DELIVERY_PRICE]').val('');
  $.post("/ajax/ajax.php", params, function (data) {
    if (data.status == 'ok') {
      if (data.price) {
        $('.pvz___input').attr('price', data.price);
        $('input[name=order_pvz_code]').val(pvz_code);
        $('input[name=order_pvz_address]').val(pvz_address);
        $('input[name=order_pvz_price]').val(data.price);
        $('input[name=DELIVERY_PRICE]').val(data.price);
        $('.pvz___desc').append('Выбран пункт выдачи: &nbsp; <strong>' + pvz_address + '</strong>');
        $('.pvz___desc').append('<br>Стоимость доставки: &nbsp; ' + data.price + ' руб.');
        if (data.delivery_period) {
          $('input[name=order_pvz_period]').val(data.delivery_period);
          $('.pvz___desc').append('<br>Сроки доставки, дней: &nbsp; ' + data.delivery_period);
        }
        recalcRightBasket();
      }
    } else if (data.status == 'error') {
      var n = noty({closeWith: ['hover'], timeout: 5000, layout: 'center', type: 'warning', text: data.text});
    }
  }, 'json')
      .fail(function (data, status, xhr) {
        var n = noty({
          closeWith: ['hover'],
          timeout: 5000,
          layout: 'center',
          type: 'warning',
          text: "Ошибка запроса. Попробуйте снова."
        });
      })
      .always(function (data, status, xhr) {
        process(false);
      })
}


function recalcRightBasket() {
  var basket_disc_sum = $('input[name=basket_disc_sum]').val();
  basket_disc_sum = Number(basket_disc_sum.length > 0 ? basket_disc_sum : 0);
  var delivery_price = $('input[name=DELIVERY_PRICE]').val();
  delivery_price = Number(delivery_price.length > 0 ? delivery_price : 0);
  var sum = basket_disc_sum + delivery_price;
  $('.basketRightDeliveryPrice').html(number_format(delivery_price, 0, ",", " ") + " <span class='currency'>:</span>");
  $('.basketRightSum').html(number_format(sum, 0, ",", " ") + " <span class='currency'>:</span>");
}


function map_init(is_submit) {

  var ds_id = $('input[name=ds]:checked').val();

  if (!is_submit) {
    $('.pvz___desc').html('');
    $('.pvz___input').attr('price', '');
    $('input[name=order_pvz_code]').val('');
    $('input[name=order_pvz_address]').val('');
    $('input[name=order_pvz_price]').val('');
    $('input[name=order_pvz_period]').val('');
    $('.order_address_block').show();
  }

  if (ds_id == 2) {
    $('.order_address_block').hide();
  }

  // Boxberry ПВЗ
  if (ds_id == 5) {

    $('.order_address_block').hide();

    var boxberry_loc_id = $('input[name=BOXBERRY_LOC_ID]').val();
    if (boxberry_loc_id.length > 0) {
      process(true);
      $.post("/ajax/ajax.php", {action: "getPVZ", boxberry_loc_id: boxberry_loc_id}, function (data) {
        if (data.status == 'ok') {
          if (data.boxberry_pvz_list) {

            var boxberry_pvz_list = data.boxberry_pvz_list;

            if (boxberry_pvz_list.length > 0) {

              $('#map').show();

              orderMap = new ymaps.Map("map", {
                center: [55.758315, 37.478858],
                zoom: 12,
                controls: ['zoomControl']
              }, {
                searchControlProvider: 'yandex#search'
              });

              orderMap.behaviors.disable('scrollZoom');

              var myObjects = [];

              var clusterer = new ymaps.Clusterer({
                preset: 'islands#yellowClusterIcons',
                clusterHideIconOnBalloonOpen: false,
                geoObjectHideIconOnBalloonOpen: false
              });

              $.each(boxberry_pvz_list, function (key, pvz) {

                var GPS = pvz['GPS'];
                if (GPS && GPS.length > 0) {
                  GPS = GPS.replace(",.", ".");

                  var ar = GPS.split(',');

                  var ar = GPS.split(',');
                  if (ar.length == 2) {
                    var x = Number(ar[0]);
                    var y = Number(ar[1]);
                  } else if (ar.length == 4) {
                    var x = Number(ar[0] + '.' + ar[1]);
                    var y = Number(ar[2] + '.' + ar[3]);
                  }
                  myObjects.push(new ymaps.Placemark(
                      [x, y],
                      {
                        balloonContentBody: '<strong>' + pvz['Address'] + '</strong>' + '<br>' + pvz['Phone'] + '<br>' + pvz['WorkSchedule'] + '<br><button class="pvz___button to___process" type="button" style="padding:5px; float: right;" code="' + pvz['Code'] + '" address="' + pvz['Address'] + '">Выбрать ПВЗ</button>',
                        iconCaption: pvz['Name']
                      }, {
                        iconLayout: 'default#image',
                        iconImageHref: '/local/templates/my/images/map_marker2.png',
                        iconImageSize: [19, 27],
                        iconImageOffset: [-19, -27],
                        balloonShadow: false,
                        balloonOffset: [120, 60]
                      }
                  ));
                }
              });

              clusterer.add(myObjects);
              orderMap.geoObjects.add(clusterer);

              orderMap.setBounds(clusterer.getBounds(), {
                checkZoomRange: true
              });

            }

          }
        }
      }, 'json')
          .fail(function (data, status, xhr) {
          })
          .always(function (data, status, xhr) {
            process(false);
          })

    }

  } else {

    if (orderMap) {
      orderMap.destroy();
      orderMap = null;
    }

    $('#map').hide();

  }
}



