<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');

use AOptima\Project as project; ?>

			<? if( !isMain() ){ ?>


					<? if(
						!isCatalog()
						&&
						!isProduct()
						&&
						!isContacts()
						&&
						!isDeliveryPayment()
						&&
						!isNews()
						&&
						!isIllustrators()
						&&
						!isShops()
						&&
						!isSearch()
						&&
						!isVacancies()
						&&
						!isPrint()
					){ ?>

							<? if(
								isDelivery()
								||
								isPayment()
								||
								isHelp()
								||
								isGuarantee()
							){ ?>
										</div>
									</section>
									<div class="clear"></div>
								</section>
							<? } ?>

						</div>

					<? } ?>

				</section>


			<? } ?>



			<? // seo_block
			$APPLICATION->IncludeComponent("my:seo_block", ""); ?>



			<!-- Подвал -->
			<footer>

				<? // subscribe_block
				$APPLICATION->IncludeComponent("my:subscribe_block", ""); ?>

				<div class="info">
					<div class="cont">

						<? // bottom_1
						$APPLICATION->IncludeComponent(
							"bitrix:menu",
							"footer_menu",
							array(
								"ROOT_MENU_TYPE" => "bottom_1",
								"MENU_CACHE_TYPE" => "A",
								"MENU_CACHE_TIME" => "3600",
								"MENU_CACHE_USE_GROUPS" => "N",
								"CACHE_SELECTED_ITEMS" => "N",
								"MENU_CACHE_GET_VARS" => array(
								),
								"MAX_LEVEL" => "1",
								"CHILD_MENU_TYPE" => "left",
								"USE_EXT" => "N",
								"DELAY" => "N",
								"ALLOW_MULTI_SELECT" => "N",
								"TOP_LINK_TITLE" => "Покупателям",
								//"TOP_LINK_URL" => "/for_customers/",
								"COMPONENT_TEMPLATE" => "footer_menu"
							),
							false
						);

						// bottom_2
						$APPLICATION->IncludeComponent(
							"bitrix:menu",
							"footer_menu",
							array(
								"ROOT_MENU_TYPE" => "bottom_2",
								"MENU_CACHE_TYPE" => "A",
								"MENU_CACHE_TIME" => "3600",
								"MENU_CACHE_USE_GROUPS" => "N",
								"CACHE_SELECTED_ITEMS" => "N",
								"MENU_CACHE_GET_VARS" => array(
								),
								"MAX_LEVEL" => "1",
								"CHILD_MENU_TYPE" => "left",
								"USE_EXT" => "N",
								"DELAY" => "N",
								"ALLOW_MULTI_SELECT" => "N",
								"COMPONENT_TEMPLATE" => "footer_menu",
								"TOP_LINK_TITLE" => "О компании",
								//"TOP_LINK_URL" => "/about/"
							),
							false
						);

						// bottom_3
						$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"footer_menu",
	array(
		"ROOT_MENU_TYPE" => "bottom_3",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"CACHE_SELECTED_ITEMS" => "N",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "N",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"COMPONENT_TEMPLATE" => "footer_menu",
		"TOP_LINK_TITLE" => "Доставка",
		"TOP_LINK_URL" => ""
	),
	false
);?>


						<div class="contacts">


							<? // Соц.сети в подвале
							$APPLICATION->IncludeComponent(
								"bitrix:news.list",
								"footer_socials",
								array(
									"COMPONENT_TEMPLATE" => "footer_socials",
									"IBLOCK_TYPE" => "content",
									"IBLOCK_ID" => "1",
									"NEWS_COUNT" => "20",
									"SORT_BY1" => "SORT",
									"SORT_ORDER1" => "ASC",
									"SORT_BY2" => "NAME",
									"SORT_ORDER2" => "ASC",
									"FILTER_NAME" => "",
									"FIELD_CODE" => array(
										0 => "",
										1 => "",
									),
									"PROPERTY_CODE" => array(
										0 => "LINK",
										1 => "",
									),
									"CHECK_DATES" => "Y",
									"DETAIL_URL" => "",
									"AJAX_MODE" => "N",
									"AJAX_OPTION_JUMP" => "N",
									"AJAX_OPTION_STYLE" => "Y",
									"AJAX_OPTION_HISTORY" => "N",
									"CACHE_TYPE" => "A",
									"CACHE_TIME" => "36000000",
									"CACHE_FILTER" => "N",
									"CACHE_GROUPS" => "Y",
									"PREVIEW_TRUNCATE_LEN" => "",
									"ACTIVE_DATE_FORMAT" => "d.m.Y",
									"SET_TITLE" => "N",
									"SET_BROWSER_TITLE" => "N",
									"SET_META_KEYWORDS" => "N",
									"SET_META_DESCRIPTION" => "N",
									"SET_STATUS_404" => "N",
									"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
									"ADD_SECTIONS_CHAIN" => "N",
									"HIDE_LINK_WHEN_NO_DETAIL" => "N",
									"PARENT_SECTION" => "",
									"PARENT_SECTION_CODE" => "",
									"INCLUDE_SUBSECTIONS" => "Y",
									"DISPLAY_DATE" => "Y",
									"DISPLAY_NAME" => "Y",
									"DISPLAY_PICTURE" => "Y",
									"DISPLAY_PREVIEW_TEXT" => "Y",
									"PAGER_TEMPLATE" => ".default",
									"DISPLAY_TOP_PAGER" => "N",
									"DISPLAY_BOTTOM_PAGER" => "N",
									"PAGER_TITLE" => "Новости",
									"PAGER_SHOW_ALWAYS" => "N",
									"PAGER_DESC_NUMBERING" => "N",
									"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
									"PAGER_SHOW_ALL" => "N",
									"AJAX_OPTION_ADDITIONAL" => "",
									"SET_LAST_MODIFIED" => "N",
									"PAGER_BASE_LINK_ENABLE" => "N",
									"SHOW_404" => "N",
									"MESSAGE_404" => "",
									"STRICT_SECTION_CHECK" => "N",
									"BLOCK_TITLE" => "Мы в социальных сетях"
								),
								false
							); ?>


							<div class="time"><? $APPLICATION->IncludeFile("/inc/footer_rezhim.inc.php", Array(), Array("MODE"=>"html")); ?></div>

							<div class="phone">
								<div class="number"><? $APPLICATION->IncludeFile("/inc/header_phone.inc.php", Array(), Array("MODE"=>"html")); ?></div>
								<div><? $APPLICATION->IncludeFile("/inc/footer_phone_description.inc.php", Array(), Array("MODE"=>"html")); ?></div>
							</div>
						</div>

						<div class="bottom">
							<div class="copyright">&copy; <? if (date('Y')==2017){ echo(date('Y')); } else { echo("2017 - ".date('Y')); } ?> <? $APPLICATION->IncludeFile("/inc/footer_copyright.inc.php", Array(), Array("MODE"=>"html")); ?></div>

							<div class="logo">
								<img src="<?=SITE_TEMPLATE_PATH?>/images/footer_logo.png" alt="">
							</div>

							<div class="payments">
								<? $APPLICATION->IncludeFile("/inc/footer_pay_info.inc.php", Array(), Array("MODE"=>"html")); ?>
							</div>

						</div>

					</div>
				</div>
			</footer>
<!-- End Подвал -->
</div>


<? // site_modals
include $_SERVER["DOCUMENT_ROOT"] . '/include/areas/site_modals.php'; ?>


<script>
Vue.use(VueAwesomeSwiper)
Vue.use(appstore)

new Vue({
  components: {
    appheader: appheader
  }
}).$mount('#app_header')

<?php
if (isMain()) {
?>
  new Vue({
    components: {
      apphome: apphome
    }
  }).$mount('#app_home')
<?php }?>

<?php if (isProduct()) { ?>
  new Vue({
    components: {
      appproduct: appproduct
    }
  }).$mount('#app_product')
<?php }?>

</script>

<!-- Подключение javascript файлов -->
<script src="<?= SITE_TEMPLATE_PATH ?>/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/jquery-migrate-1.4.1.min.js"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/owl.carousel.min.js"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/main_banners.js"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/fancybox.min.js"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/maskedinput.min.js"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/nice-select.min.js"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/scripts.js?v=<?= time() ?>"></script>


<script src="<?= SITE_TEMPLATE_PATH ?>/js/my.js?v=<?= time() ?>"></script>

<script src="<?= SITE_TEMPLATE_PATH ?>/js/i_subscribes.js?v=<?= time() ?>"></script>

<? if (arURI()[1] == 'personal') { ?>
	<script src="<?= SITE_TEMPLATE_PATH ?>/js/personal.js?v=<?= time() ?>"></script>
<? } ?>

<? if (arURI()[1] == 'personal_prints') { ?>
	<script src="<?= SITE_TEMPLATE_PATH ?>/js/prints.js?v=<?= time() ?>"></script>
<? } ?>

<? if (arURI()[1] == 'personal_constructor' || arURI()[1] == 'personal_approval') { ?>
	<!--
			<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.4/TweenMax.min.js"></script>
			<script src="<?= SITE_TEMPLATE_PATH ?>/js/personal_constructor.js?v=<?= time() ?>"></script>
        -->

	<script src=/personal_constructor/js/chunk-vendors.js></script>
	<script src=/personal_constructor/js/app.js></script>

<? } ?>

<? if (isContacts()) {
    include($_SERVER['DOCUMENT_ROOT'] . '/include/areas/contacts_scripts.php'); ?>
<? } ?>

<script src="<?= SITE_TEMPLATE_PATH ?>/js/slimscroll.min.js"></script>

<? if (isShops()) {
    // footer_shops
    $APPLICATION->IncludeComponent("my:footer_shops", "");
} ?>


<input type="hidden" name="pureURL" value="<?= pureURL() ?>">
<input type="hidden" name="isMain" value="<?= isMain() ? 'Y' : 'N' ?>">


<? project\site::deleteOldImages();
constructorSuccess();
passwordSuccess(); ?>

</body>
</html>
