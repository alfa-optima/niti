<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$activeGenderType = project\gender_type::getActive(); ?>

<!DOCTYPE html>
<html lang="ru-RU">
<head>

	<link rel="canonical" href="<?= $_SERVER['REQUEST_SCHEME'] ?>://<?= $_SERVER['SERVER_NAME'] ?><?= pureURL() ?>"/>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<!-- Переключение IE в последнию версию, на случай если в настройках пользователя стоит меньшая -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<!-- Адаптирование страницы для мобильных устройств -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Запрет распознования номера телефона -->
	<meta name="format-detection" content="telephone=no">
	<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">

	<title><? $APPLICATION->ShowTitle() ?></title> <? $APPLICATION->ShowHead() ?>

	<!-- Традиционная иконка сайта, размер 16x16, прозрачность поддерживается. Рекомендуемый формат: .ico или .png -->
	<link rel="shortcut icon" href="/favicon.ico">

	<!-- Подключение файлов стилей -->
	<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700&amp;subset=cyrillic,cyrillic-ext" rel="stylesheet">

	<link href="<?= SITE_TEMPLATE_PATH ?>/jquery-ui-1.12.1.custom/jquery-ui.css" rel="stylesheet">

	<link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/css/buttons.css">

	<link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/css/owl.carousel.css">
	<link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/css/fancybox.css?v=6">
	<link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/css/jquery.formstyler.css">
	<link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/css/styles.css?v=<?= time() ?>">
	<link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/css/response_1199.css?v=<?= time() ?>" media="(max-width: 1199px)">
	<link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/css/response_1023.css?v=<?= time() ?>" media="(max-width: 1023px)">
	<link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/css/response_767.css?v=<?= time() ?>" media="(max-width: 767px)">
	<link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/css/response_479.css?v=<?= time() ?>" media="(max-width: 479px)">

	<link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/css/main_banners.css">
	<link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/css/main_banners_media.css">

	<link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/css/my.css?v=<?= time() ?>">

	<link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/js/vue-modules/header/appheader.css?v=<?= time() ?>">
	<link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/js/vue-modules/modaldialog/appmodaldialog.css?v=<?= time() ?>">


	<script src="<?= SITE_TEMPLATE_PATH ?>/js/jquery-3.1.1.min.js"></script>
	<script src="<?= SITE_TEMPLATE_PATH ?>/js/jquery.formstyler.min.js"></script>

	<script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/noty/packaged/jquery.noty.packaged.min.js"></script>
	<script src="<?= SITE_TEMPLATE_PATH ?>/js/jquery.cookie.js"></script>
	<script src="<?= SITE_TEMPLATE_PATH ?>/js/my_f.js?v=<?= time() ?>"></script>

	<link href="https://cdn.jsdelivr.net/npm/swiper@5.3.6/css/swiper.min.css" rel="stylesheet">
	<script src="https://cdn.jsdelivr.net/npm/swiper@5.3.6/js/swiper.min.js"></script>
	<script src="https://unpkg.com/vue"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/vue-awesome-swiper"></script>

<!--	<link rel="stylesheet" href="https://unpkg.com/purecss@2.0.3/build/pure-min.css" integrity="sha384-cg6SkqEOCV1NbJoCu11+bm0NvBRc8IYLRGXkmNrqUBfTjmMYwNKPWBTIKyw9mHNJ" crossorigin="anonymous">-->
    <?
    //echo '<script src="' . SITE_TEMPLATE_PATH . '/js/vue-modules/header/app-header.min.js?t=' . time() . '"></script>';
    echo '<script src="' . SITE_TEMPLATE_PATH . '/js/vue-modules/store/appstore.umd.js?t=' . time() . '"></script>';
    echo '<script src="' . SITE_TEMPLATE_PATH . '/js/vue-modules/header/appheader.umd.js?t=' . time() . '"></script>';
    echo '<script src="' . SITE_TEMPLATE_PATH . '/js/vue-modules/modaldialog/appmodaldialog.umd.js?t=' . time() . '"></script>';
    echo '<script src="' . SITE_TEMPLATE_PATH . '/js/vue-modules/dependences/dependences.umd.js?t=' . time() . '"></script>';
    //echo '<script src="' . SITE_TEMPLATE_PATH . '/js/vue-modules/dep-vmodal/depvmodal.umd.js?t=' . time() . '"></script>';

    if ( isMain() ){ ?>
        <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/js/vue-modules/home/apphome.css?v=<?= time() ?>">
        <script src="<?= SITE_TEMPLATE_PATH ?>/js/vue-modules/home/apphome.umd.js?t=<?= time() ?>"></script>
    <? }

    if (isProduct()) { ?>
        <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH ?>/js/vue-modules/product/appproduct.css?v=<?= time() ?>">
        <script src="<?= SITE_TEMPLATE_PATH ?>/js/vue-modules/product/appproduct.umd.js?t=<?= time() ?>"></script>
    <?php }

    if (arURI()[1] == 'personal_constructor' || arURI()[1] == 'personal_approval') { ?>

        <link href=/personal_constructor/css/chunk-60ac4d30.css rel=prefetch>
        <link href=/personal_constructor/js/chunk-3159568c.js rel=prefetch>
        <link href=/personal_constructor/js/chunk-46d96339.js rel=prefetch>
        <link href=/personal_constructor/js/chunk-60ac4d30.js rel=prefetch>
        <link href=/personal_constructor/js/chunk-7f292b36.js rel=prefetch>
        <link href=/personal_constructor/css/app.css rel=preload as=style>
        <link href=/personal_constructor/js/app.js rel=preload as=script>
        <link href=/personal_constructor/js/chunk-vendors.js rel=preload as=script>
        <link href=/personal_constructor/css/app.css rel=stylesheet>

    <? }

    if (arURI()[1] == 'personal_sales') { ?>

		<link href="<?= SITE_TEMPLATE_PATH ?>/css/personal_sales.css" rel="stylesheet">

    <? } ?>

</head>

<body>

<div id="panel"><? $APPLICATION->ShowPanel(); ?></div>

<div class="wrap">

    <?

    // MENU gender_categories
    $APPLICATION->IncludeComponent("my:gender_categories", "");

    // header_user_block
    $APPLICATION->IncludeComponent("my:header_user_block", "");

    // gender_choice
    $APPLICATION->IncludeComponent("my:gender_choice", "");

    ?>

	<!-- Шапка -->
	<!--	<app-header></app-header>-->
	<div id="app_header">
		<appheader></appheader>
	</div>
	<!-- End Шапка -->

    <? if ( isMain() ){

        // Баннеры на главной (слайдер)
        $GLOBALS['main_slider_banners']['PROPERTY_GENDER_TYPE_VALUE'] = $_SESSION['gender_type']['NAME'];

        $APPLICATION->IncludeComponent(
            "bitrix:news.list", "main_slider_banners",
            array(
                "COMPONENT_TEMPLATE" => "main_slider_banners",
                "IBLOCK_TYPE" => "content",
                "IBLOCK_ID" => 30,
                "NEWS_COUNT" => "100",
                "SORT_BY1" => "SORT",
                "SORT_ORDER1" => "ASC",
                "SORT_BY2" => "NAME",
                "SORT_ORDER2" => "ASC",
                "FILTER_NAME" => "main_slider_banners",
                "FIELD_CODE" => array(),
                "PROPERTY_CODE" => array('LINK', 'GENDER_TYPE', 'FOR_SLIDER', 'LINK_TEXT'),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "600",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "PREVIEW_TRUNCATE_LEN" => "",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "SET_TITLE" => "N",
                "SET_BROWSER_TITLE" => "N",
                "SET_META_KEYWORDS" => "N",
                "SET_META_DESCRIPTION" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "INCLUDE_SUBSECTIONS" => "Y",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "PAGER_TEMPLATE" => ".default",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "AJAX_OPTION_ADDITIONAL" => "undefined",
                "SET_LAST_MODIFIED" => "N",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "SHOW_404" => "N",
                "MESSAGE_404" => ""
            ),
            false
        );

        // Хиты на главной
        $APPLICATION->IncludeComponent("my:main_hits", "");

        // Преимущества на главной
        $APPLICATION->IncludeComponent(
            "bitrix:news.list", "main_adv",
            array(
                "COMPONENT_TEMPLATE" => "main_adv",
                "IBLOCK_TYPE" => "content",
                "IBLOCK_ID" => project\site::MAIN_ADV_IBLOCK_ID,
                "NEWS_COUNT" => "5",
                "SORT_BY1" => "SORT",
                "SORT_ORDER1" => "ASC",
                "SORT_BY2" => "NAME",
                "SORT_ORDER2" => "ASC",
                "FILTER_NAME" => "",
                "FIELD_CODE" => array(),
                "PROPERTY_CODE" => array('LINK_SLIDER', 'TEXT_SLIDER'),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000000",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "PREVIEW_TRUNCATE_LEN" => "",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "SET_TITLE" => "N",
                "SET_BROWSER_TITLE" => "N",
                "SET_META_KEYWORDS" => "N",
                "SET_META_DESCRIPTION" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "INCLUDE_SUBSECTIONS" => "Y",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "PAGER_TEMPLATE" => ".default",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "AJAX_OPTION_ADDITIONAL" => "undefined",
                "SET_LAST_MODIFIED" => "N",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "SHOW_404" => "N",
                "MESSAGE_404" => ""
            ),
            false
        );

        // Коллекции на главной
        $APPLICATION->IncludeComponent("my:main_collections", "");

        // Иллюстраторы на главной
        $APPLICATION->IncludeComponent("my:illustrators_main", "");

        echo '<div id="app_home"><apphome></apphome></div>';

        ?>

			<section class="widgets">
				<div class="cont">
					<div class="grid">
						<div class="item_wrap">
							<div class="item">
								<div class="friends">
									<div class="title">с друзьями<br> дешевле</div>

									<div class="fotos">
										<img src="<?= SITE_TEMPLATE_PATH ?>/images/friends_foto.png" alt="">
										<img src="<?= SITE_TEMPLATE_PATH ?>/images/friends_foto.png" alt="">
										<img src="<?= SITE_TEMPLATE_PATH ?>/images/friends_foto.png" alt="">
									</div>

									<div class="desc">находите друзей в социальных сетях и получайте скуидку до</div>

									<div class="discount">20
										<small>%</small>
									</div>

									<a href="#" class="vk_like_link">
										<img src="<?= SITE_TEMPLATE_PATH ?>/images/ic_vk.png" alt="">мне нравится
									</a>
								</div>
							</div>
						</div>


						<div class="item_wrap">
							<div class="item instagram">
								<div class="title">мы в Instagram</div>

								<div class="info">
									<div class="grid">
										<div class="item_wrap">
											<a href="/" target="_blank" class="item">
												<img src="<?= SITE_TEMPLATE_PATH ?>/images/instagram_thumb.jpg" alt="">
											</a>
										</div>

										<div class="item_wrap">
											<a href="/" target="_blank" class="item">
												<img src="<?= SITE_TEMPLATE_PATH ?>/images/instagram_thumb.jpg" alt="">
											</a>
										</div>

										<div class="item_wrap">
											<a href="/" target="_blank" class="item">
												<img src="<?= SITE_TEMPLATE_PATH ?>/images/instagram_thumb.jpg" alt="">
											</a>
										</div>

										<div class="item_wrap">
											<a href="/" target="_blank" class="item">
												<img src="<?= SITE_TEMPLATE_PATH ?>/images/instagram_thumb.jpg" alt="">
											</a>
										</div>

										<div class="item_wrap">
											<a href="/" target="_blank" class="item">
												<img src="<?= SITE_TEMPLATE_PATH ?>/images/instagram_thumb.jpg" alt="">
											</a>
										</div>

										<div class="item_wrap">
											<a href="/" target="_blank" class="item">
												<img src="<?= SITE_TEMPLATE_PATH ?>/images/instagram_thumb.jpg" alt="">
											</a>
										</div>

										<div class="item_wrap">
											<a href="/" target="_blank" class="item">
												<img src="<?= SITE_TEMPLATE_PATH ?>/images/instagram_thumb.jpg" alt="">
											</a>
										</div>

										<div class="item_wrap">
											<a href="/" target="_blank" class="item">
												<img src="<?= SITE_TEMPLATE_PATH ?>/images/instagram_thumb.jpg" alt="">
											</a>
										</div>

										<div class="item_wrap">
											<a href="/" target="_blank" class="item">
												<img src="<?= SITE_TEMPLATE_PATH ?>/images/instagram_thumb.jpg" alt="">
											</a>
										</div>
									</div>
								</div>

								<a href="#" class="subscribe_link">Подписаться</a>
							</div>
						</div>


						<div class="item_wrap big">
							<a href="/" class="item competition"></a>
						</div>
					</div>
				</div>
			</section>

        <? if (1 == 2) { ?>
				<section class="franchise">
					<div class="cont">

              <? $APPLICATION->IncludeFile("/inc/main_fr_img.inc.php", array(), array("MODE" => "html")); ?>

						<div class="col right">

							<div class="title"><? $APPLICATION->IncludeFile("/inc/main_fr_title.inc.php", array(), array("MODE" => "html")); ?></div>

							<p><? $APPLICATION->IncludeFile("/inc/main_fr_text.inc.php", array(), array("MODE" => "html")); ?></p>

							<a href="/franchise/" class="order_link">купить франшизу</a>

							<div class="shops">
								<a href="/shops/">список всех магазинов</a>
							</div>
						</div>
						<div class="clear"></div>
					</div>
				</section>
        <? } ?>

			<section class="articles">
				<div class="cont">

                    <? // Новости на главной
                    $APPLICATION->IncludeComponent(
                        "bitrix:news.list", "main_news",
                        array(
                            "COMPONENT_TEMPLATE" => "main_news",
                            "IBLOCK_TYPE" => "content",
                            "IBLOCK_ID" => project\site::NEWS_IBLOCK_ID,
                            "NEWS_COUNT" => "4",
                            "SORT_BY1" => "PROPERTY_SORT_DATE",
                            "SORT_ORDER1" => "DESC",
                            "SORT_BY2" => "NAME",
                            "SORT_ORDER2" => "ASC",
                            "FILTER_NAME" => "",
                            "FIELD_CODE" => array(),
                            "PROPERTY_CODE" => array('SORT_DATE'),
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "36000000",
                            "CACHE_FILTER" => "N",
                            "CACHE_GROUPS" => "Y",
                            "PREVIEW_TRUNCATE_LEN" => "",
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",
                            "SET_TITLE" => "N",
                            "SET_BROWSER_TITLE" => "N",
                            "SET_META_KEYWORDS" => "N",
                            "SET_META_DESCRIPTION" => "N",
                            "SET_STATUS_404" => "N",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "PARENT_SECTION" => "",
                            "PARENT_SECTION_CODE" => "",
                            "INCLUDE_SUBSECTIONS" => "Y",
                            "DISPLAY_DATE" => "Y",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "Y",
                            "DISPLAY_PREVIEW_TEXT" => "Y",
                            "PAGER_TEMPLATE" => ".default",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "PAGER_TITLE" => "Новости",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "AJAX_OPTION_ADDITIONAL" => "undefined",
                            "SET_LAST_MODIFIED" => "N",
                            "PAGER_BASE_LINK_ENABLE" => "N",
                            "SHOW_404" => "N",
                            "MESSAGE_404" => ""
                        ),
                        false
                    ); ?>

				</div>
			</section>

			<section class="bottom_text">
				<div class="cont">
					<div class="text_block">
                        <? $APPLICATION->IncludeFile("/inc/main_text.inc.php", array(), array("MODE" => "html")); ?>
					</div>
				</div>
			</section>
			<!-- End Основная часть -->

    <? } else { ?>

        <section class="page_content">

            <? // Хлебные крошки
            $APPLICATION->IncludeComponent(
                "bitrix:breadcrumb", "breadcrumb",
                array("START_FROM" => "0", "PATH" => "", "SITE_ID" => "s1")
            ); ?>

            <? if (
                  !isCatalog()
                  &&
                  !isProduct()
                  &&
                  !isContacts()
                  &&
                  !isDeliveryPayment()
                  &&
                  !isNews()
                  &&
                  !isIllustrators()
                  &&
                  !isShops()
                  &&
                  !isSearch()
                  &&
                  !isVacancies()
                  &&
                  !isPrint()
                  &&
                  !isCollection()
            ){ ?>

            <div class="cont">

                <? if (
                    !isDelivery()
                    &&
                    !isPayment()
                    &&
                    !isHelp()
                    &&
                    !isGuarantee()
                    &&
                    !isCollection()
                    &&
                    !isSearch()
                    &&
                    !isFabricPage()
                ){ ?>
                    <h1 class="page_title"><? $APPLICATION->ShowTitle() ?></h1>
                <? } ?>

            <? if (
                isDelivery()
                ||
                isPayment()
                ||
                isHelp()
                ||
                isGuarantee()
            ){ ?>

                <section class="<? if (isDelivery()) { ?>delivery_page<? } ?> <? if (isPayment()) { ?>payment_page<? } ?>">

                    <h1 class="page_title"><? $APPLICATION->ShowTitle() ?></h1>

                    <aside class="left">
                        <? // left_menu
                        $APPLICATION->IncludeComponent(
                            "bitrix:menu", "left_menu",
                            array(
                                "ROOT_MENU_TYPE" => "left",
                                "MENU_CACHE_TYPE" => "A",
                                "MENU_CACHE_TIME" => "3600",
                                "MENU_CACHE_USE_GROUPS" => "N",
                                "CACHE_SELECTED_ITEMS" => "N",
                                "MENU_CACHE_GET_VARS" => array(),
                                "MAX_LEVEL" => "1",
                                "CHILD_MENU_TYPE" => "left",
                                "USE_EXT" => "N",
                                "DELAY" => "N",
                                "ALLOW_MULTI_SELECT" => "N",
                                "COMPONENT_TEMPLATE" => "left_menu",
                            ),
                            false
                        ); ?>
                    </aside>

                    <section class="content right">
                        <div class="text_block">

            <? } ?>

        <? } ?>

    <? } ?>


