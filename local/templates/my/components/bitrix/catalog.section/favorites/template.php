<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (count($arResult['ITEMS']) > 0){ ?>

	<div class="products">
		<div class="grid">
		
			<? foreach ($arResult['ITEMS'] as $key => $arItem){ ?>

				<div class="item_wrap">
				
					<? // catalog_item_other
					$APPLICATION->IncludeComponent(
						"my:catalog_item", "", array(
							'arItem' => $arItem,
							'component' => $this,
							'is_fovorites' => true
						)
					); ?>
					
				</div>
				
			<? } ?>
		
		</div>
	</div>
	
<? } ?>