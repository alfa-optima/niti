<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use AOptima\Tools as tools;

$arResponse = array(
    'offers' => array(),
    'response_info' => array(
        'products_count' => $arResult['NAV_RESULT']->NavRecordCount,
        'page_product_count' => $arParams['PAGE_ELEMENT_COUNT'],
        'pages_count' => $arResult['NAV_RESULT']->NavPageCount,
    )
);

if (count($arResult['ITEMS']) > 0) {

    foreach ($arResult['ITEMS'] as $offer) {

        $arOffer = array(
            'id' => $offer['ID'],
            'name' => trim($offer['NAME']),
            'price' => $offer['PRICES']['BASE']['VALUE_VAT']
        );

        if (intval($offer['PROPERTIES']['CML2_LINK']['VALUE']) > 0) {
            $product = tools\el::info($offer['PROPERTIES']['CML2_LINK']['VALUE']);
            if (intval($product['ID']) > 0) {
                $arOffer['product']['id'] = $product['ID'];
                $arOffer['product']['name'] = trim($product['NAME']);
                $arOffer['product']['url'] = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . $product['DETAIL_PAGE_URL'];

                $el_sections = el_sections($product['ID']);
                if (is_array($el_sections) && count($el_sections) > 0) {
                    $section = section_info($el_sections[0]['ID']);
                    if (intval($section['ID']) > 0) {
                        $section_chain = section_chain($section['ID']);
                        $f_section = section_info($section_chain[0]['ID']);
                        $arOffer['product']['section'] = array(
                            'id' => $section['ID'],
                            'name' => trim($section['NAME']),
                            'url' => $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . $section['SECTION_PAGE_URL'],
                            'gender' => $f_section['UF_POL'],
                        );
                    }
                }

            }
        }

        if (strlen($offer['PROPERTIES']['SIZE']['VALUE']) > 0) {
            $arOffer['size'] = $offer['PROPERTIES']['SIZE']['VALUE'];
        }

        if (strlen(strip_tags($offer['DISPLAY_PROPERTIES']['COLOR']['DISPLAY_VALUE'])) > 0) {
            $arOffer['color'] = strip_tags($offer['DISPLAY_PROPERTIES']['COLOR']['DISPLAY_VALUE']);
        }

        $arOffer['picture'] = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . $offer['PROPERTIES']['PHTO']['VALUE'];

        $arResponse['offers'][$offer['ID']] = $arOffer;

    }

}

echo json_encode($arResponse);
