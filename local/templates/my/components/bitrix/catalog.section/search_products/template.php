<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use AOptima\Tools as tools;

$arResponse = array(
	'products' => array(),
	'response_info' => array(
		'products_count' => $arResult['NAV_RESULT']->NavRecordCount,
		'page_product_count' => $arParams['PAGE_ELEMENT_COUNT'],
		'pages_count' => $arResult['NAV_RESULT']->NavPageCount,
	)
);

if( count($arResult['ITEMS']) > 0 ){

	foreach ($arResult['ITEMS'] as $arItem){

		$product = array(
			'id' => $arItem['ID'],
			'name' => trim($arItem['NAME']),
			'url' => $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].$arItem['DETAIL_PAGE_URL'],
			'offers' => array()
		);

		if( intval($arItem['PROPERTIES']['PRINT_ID']['VALUE']) > 0 ){
			$print = tools\el::info($arItem['PROPERTIES']['PRINT_ID']['VALUE']);
			if( intval($print['ID']) > 0 ){
				$product['print'] = array(
					'id' => $print['ID'],
					'name' => $print['NAME']
				);
				if( strlen($print['PROPERTY_PHOTO_VALUE']) > 0 ){
					$product['print']['picture'] = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].$print['PROPERTY_PHOTO_VALUE'];
				}
				if( intval($arItem['PROPERTIES']['USER_ID']['VALUE']) > 0 ){
					$user = user_info($arItem['PROPERTIES']['USER_ID']['VALUE']);
					if( intval($user['ID']) > 0 ){
						$product['designer'] = array(
							'id' => $user['ID'],
							'name' => $user['NAME']
						);
					}
				}
			}
		}

		$el_sections = tools\el::sections($arItem['ID']);
		if( is_array($el_sections) && count($el_sections) > 0 ){
			$section = tools\section::info($el_sections[0]['ID']);
			if( intval($section['ID']) > 0 ){
				$section_chain = tools\section::chain($section['ID']);
				$f_section = tools\section::info($section_chain[0]['ID']);
				$product['section'] = array(
					'id' => $section['ID'],
					'name' => $section['NAME'],
					'url' => $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].$section['SECTION_PAGE_URL'],
					'gender' => $f_section['UF_POL'],
				);
			}
		}

		if( count($arItem['OFFERS']) > 0 ){

			foreach ($arItem['OFFERS'] as $offer){

				$product['offers'][$offer['ID']] = array(
					'id' => $offer['ID'],
					'name' => trim($offer['NAME']),
					'price' => $offer['PRICES']['BASE']['VALUE_VAT'],
					'picture' => $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].$offer['PROPERTIES']['PHTO']['VALUE']
				);

				if ( strlen($offer['PROPERTIES']['SIZE']['VALUE']) > 0 ){
					$product['offers'][$offer['ID']]['size'] = $offer['PROPERTIES']['SIZE']['VALUE'];
				}

				if ( strlen(strip_tags($offer['DISPLAY_PROPERTIES']['COLOR']['DISPLAY_VALUE'])) > 0 ){
					$product['offers'][$offer['ID']]['color'] = strip_tags($offer['DISPLAY_PROPERTIES']['COLOR']['DISPLAY_VALUE']);
				}

			}

		}

		$arResponse['products'][$arItem['ID']] = $product;

	}

}



echo json_encode($arResponse);
