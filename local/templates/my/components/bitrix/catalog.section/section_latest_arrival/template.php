<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$max_count = $arParams['PAGE_ELEMENT_COUNT'];

if (count($arResult['ITEMS']) > 0){ ?>

	<section class="products">
	
		<div class="cont">
		
			<div class="block_title"><div>Последнее поступление</div></div>

			<div class="grid latest_arrival_block">
			
				<? $cnt = 0;
				foreach ($arResult['ITEMS'] as $key => $arItem){ $cnt++;
					
					if ( $cnt < $max_count ){
					
						$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
						$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM'))); ?>
						
						<div class="item_wrap">
						
							<? // catalog_item
							$APPLICATION->IncludeComponent(
								"my:catalog_item", "", array('arItem' => $arItem, 'component' => $this)
							); ?>
						
						</div>
						
					<? }

				} ?>
				
			</div>


			<div class="more latest_arrival_more_block" style="display:<? if($cnt==$max_count){ echo 'block'; } else { echo 'none'; } ?>;">
				<a style="cursor:pointer" class="latest_arrival_more_button to___process">
					<span class="icon"></span>показать еще
				</a>
			</div>
			
		</div>
		
	</section>
	
<? } ?>