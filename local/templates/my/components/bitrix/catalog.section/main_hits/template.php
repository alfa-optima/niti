<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$max_count = $arParams['PAGE_ELEMENT_COUNT'];

if (count($arResult['ITEMS']) > 0) {

    $cnt = 0;
    foreach ($arResult['ITEMS'] as $key => $arItem) {
        $cnt++;

        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

        // catalog_item
        /*
        $APPLICATION->IncludeComponent(
            "my:catalog_item",
            "json",
            [
                'arItem' => $arItem,
                'component' => $this
            ]
        );
        */

//        echo 123123;
//        print_r($arItem);
//        exit();

        $item = [
            'ID' => $arItem['ID'],
            'DETAIL_PAGE_URL' => $arItem['DETAIL_PAGE_URL'],
            'NAME' => htmlspecialchars_decode($arItem['NAME']),
            'photo' => $arItem['photo'],
            'min_price' => number_format($arResult['min_price'], 0, ",", " "),
            'max_price' => 0,
            'isFavorite' => project\favorites::isFavorite($arItem['ID'])
        ];

        if ( $arItem['PROPERTIES']['NEW']['VALUE'] == 'Да' ){
            $item['new'] = true;
        } 

        $item['photo'] = $arItem['OFFERS'][0]['PROPERTIES']['PHTO']['VALUE'];

        $result[] = $item;

    }

    echo \Bitrix\Main\Web\Json::encode($result);

}


/*
 *
 * <div class="product product___block" id="<?=$arResult['component']->GetEditAreaId($arItem['ID']);?>" tov_id="<?=$arItem['ID']?>">

    <? if ( $arItem['PROPERTIES']['NEW']['VALUE'] == 'Да' ){ ?>
        <div class="sticker new">NEW</div>
    <? } ?>

    <? if( !$arResult['is_fovorites'] ){ ?>
        <a style="cursor:pointer" class="favorite_link fav___link to___process" tov_id="<?=$arItem['ID']?>"></a>
    <? } else { ?>
        <a style="cursor:pointer" class="favorite_del favorite___remove to___process" item_id="<?=$arItem['ID']?>"></a>
    <? } ?>

    <div class="thumb">
        <a class="detail_page_url" href="<?=$arItem['DETAIL_PAGE_URL']?>">
            <img style="display:none" class="default_photo" src="<?=rIMG($arResult['default_photo'], 4, $is_4?195:270, 280)?>">
            <? foreach( $arResult['color_images'] as $color_id => $img_id ){ ?>
                <img style="display:none" color_id="<?=$color_id?>" src="<?=rIMG($img_id, 4, $is_4?195:270, 280)?>">
            <? } ?>
        </a>
    </div>

    <a href="javascript:;" class="quike_view_link" data-fancybox data-src="/ajax/product.php?id=<?=$arItem['ID']?>" data-type="ajax">быстрый просмотр</a>

    <div class="name">
        <a class="detail_page_url" href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=htmlspecialchars_decode($arItem['PRODUCT_NAME'])?></a>
    </div>

    <div class="price"><? if( $arResult['min_price'] != $arResult['max_price'] ){ ?>от <? } ?><?=number_format($arResult['min_price'], 0, ",", " ")?> <span class="currency">:</span></div>

</div>

 */
