<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

foreach ( $arResult['ITEMS'] as $key => $arItem ){

    $arItem['DEFAULT_OFFER'] = false;
    foreach ( $arItem['OFFERS'] as $offer ){
        if( $offer["PROPERTIES"]['MAIN_TP']['VALUE'] ){
            $arItem['DEFAULT_OFFER'] = $offer;
        }
    }
    if( !$arItem['DEFAULT_OFFER'] ){   $arItem['DEFAULT_OFFER'] = $arItem['OFFERS'][0];   }

    $arItem['price'] = $arItem['DEFAULT_OFFER']['PRICES']['BASE']['DISCOUNT_VALUE_VAT'];

    $arItem['prices'] = false;
    foreach ( $arItem['OFFERS'] as $offer ){
        $arItem['prices'][] = $offer['PRICES']['BASE']['DISCOUNT_VALUE_VAT'];
    }
    $arItem['min_price'] = min($arItem['prices']);
    $arItem['max_price'] = max($arItem['prices']);

    $arItem['photo'] = $arItem['DEFAULT_OFFER']['PROPERTIES']['PHOTO_ID']['VALUE'];

    $arItem['PRODUCT_NAME'] = htmlspecialchars_decode($arItem['~NAME']);
    if( $arItem['DISPLAY_PROPERTIES']['PRINT_ID']['LINK_ELEMENT_VALUE'] ){
        $keys = array_keys($arItem['DISPLAY_PROPERTIES']['PRINT_ID']['LINK_ELEMENT_VALUE']);
        if( count($keys) > 0 ){
            $arItem['PRODUCT_NAME'] = htmlspecialchars_decode($arItem['DISPLAY_PROPERTIES']['PRINT_ID']['LINK_ELEMENT_VALUE'][$keys[0]]['~NAME']);
        }
    }

    $arItem['SECTION_NOMIN_TITLE'] = false;
    if(
        isset( $arParams['els_sections'][ $arItem['IBLOCK_SECTION_ID'] ]['UF_NOMIN_TITLE'] )
        &&
        strlen( $arParams['els_sections'][ $arItem['IBLOCK_SECTION_ID'] ]['UF_NOMIN_TITLE'] ) > 0
    ){
        $arItem['SECTION_NOMIN_TITLE'] = $arParams['els_sections'][ $arItem['IBLOCK_SECTION_ID'] ]['UF_NOMIN_TITLE'];
    }

    $arResult['ITEMS'][$key] = [
        'ID' => $arItem['ID'],
        'DETAIL_PAGE_URL' => $arItem['DETAIL_PAGE_URL'],
        'NAME' => $arItem['NAME'],
        'SECTION_NOMIN_TITLE' => $arItem['SECTION_NOMIN_TITLE'],
        'photo' => $arItem['photo'],
        'price' => $arItem['price'],
        'min_price' => $arItem['min_price'],
        'max_price' => $arItem['max_price'],
        'isFavorite' => project\favorites::isFavorite($arItem['ID'])
    ];

}

echo \Bitrix\Main\Web\Json::encode( $arResult['ITEMS'] );

