<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (count($arResult['ITEMS']) > 0){ ?>

	<div class="products">
		<div class="grid four catalog_load_block">
		
			<? foreach ($arResult['ITEMS'] as $key => $arItem){ ?>
				
				<div class="item_wrap">

					<? // catalog_item
					$APPLICATION->IncludeComponent(
						"my:catalog_item", "", array('arItem' => $arItem, 'component' => $this)
					); ?>
				
				</div>
				
			<? } ?>
		
		</div>
	</div>

	<input type="hidden" name="all_elements_cnt" value="<?=$arResult['NAV_RESULT']->NavRecordCount?> <?=pfCnt($arResult['NAV_RESULT']->NavRecordCount, "товар", "товара", "товаров")?>">
	
	<? if($arParams["DISPLAY_BOTTOM_PAGER"]){ ?>
		<?=$arResult["NAV_STRING"]?>
	<? } ?>
	
	
<? } ?>