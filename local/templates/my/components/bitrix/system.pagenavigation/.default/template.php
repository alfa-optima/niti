<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
 
//pre($arResult);
 
if(!$arResult["NavShowAlways"]){
    if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
    return;
}
 
$countPagesDisplay = 3; // kolichestvo otobrajaemih stranic v paginacii

 
$i = 1;
$left_border = $arResult["NavPageNomer"];
$right_border = $arResult["NavPageNomer"];
$max = $arResult["NavPageCount"];
while($i < $countPagesDisplay){
    if($i % 2 == 0){
        $lb = $left_border;
        $left_border = addLeftBorder($left_border);
        if ($left_border == $lb){
            $rb = $right_border;
            $right_border = addRightBorder($right_border, $max);
            if ($right_border == $rb){   $i = $countPagesDisplay;   }
        }
    } else {
        $rb = $right_border;
        $right_border = addRightBorder($right_border, $max);
        if ($right_border == $rb){
            $lb = $left_border;
            $left_border = addLeftBorder($left_border);
            if ($left_border == $lb){   $i = $countPagesDisplay;   }
        }
    }
    $i++;
}
$arResult["nStartPage"] = $left_border;
$arResult["nEndPage"] = $right_border;

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : ""); ?>



<div class="pagination right">

 
	<? if ($arResult["NavPageNomer"] > 1): ?>
	 
		<?if($arResult["bSavePage"]):?>
		
			<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>" class="prev"></a>

			<?if ($arResult["NavPageNomer"] > 2){?>
			
				<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=1">1</a>

			<? } ?>
			
		<?else:?>
		
			<?if ($arResult["NavPageNomer"] > 2):?>
			
				<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>" class="prev"></a>

			<?else:?>
				
				<a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>" class="prev"></a>

			<?endif?>
			
			<?if ($arResult["nStartPage"] != 1):?>
			
				<a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>">1</a>
				
			<?endif?>
			
		<?endif?>
	 
	<?endif?>
	 


	<?if ($arResult["nStartPage"] > 2):?>
	
		<div class="sep">...</div>
		
	<?endif?>
	 
	<?while($arResult["nStartPage"] <= $arResult["nEndPage"]):?>
	
		<?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
		
			<a class="active"><?=$arResult["nStartPage"]?></a>
		   
		<?elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):?>
		
			<a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><?=$arResult["nStartPage"]?></a>
		
		<?else:?>
		
			<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"><?=$arResult["nStartPage"]?></a>
			
		<?endif?>
		
		<?$arResult["nStartPage"]++?>
		
	<?endwhile?>
	 
	<?if ($arResult["nEndPage"] < ($arResult["NavPageCount"] - 1)):?>
		<div class="sep">...</div>
	<?endif?>



	<?if($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
		<?if($arResult["nEndPage"] < $arResult["NavPageCount"]):?>
		
			<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["NavPageCount"]?>"><?=$arResult["NavPageCount"]?></a>

			
			<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>" class="next"></a>
			
		<?else:?>

			<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>" class="next"></a>
			
		<?endif?>
		
	<?endif?>
 
</div>
<div class="clear"></div>