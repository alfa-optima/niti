<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if( count($arResult['ITEMS']) > 0 ){ 
	foreach ($arResult['ITEMS'] as $shop){ ?>

		var myPlacemark = new ymaps.Placemark([<?=$shop['PROPERTIES']['MAP_POINT']['VALUE']?>], {
			balloonContent: '<?=$shop['PROPERTIES']['ADDRESS']['VALUE']?>'
		}, {
			iconImageHref: '<?=SITE_TEMPLATE_PATH?>/images/map_marker2.png',
			iconImageSize: [19, 27],
			iconImageOffset: [-10, -27],
			hideIconOnBalloonOpen: false,
			balloonShadow: false,
			balloonOffset: [72, 37]
		})
		
		points['<?=$shop['ID']?>'] = [<?=$shop['PROPERTIES']['MAP_POINT']['VALUE']?>];

		myMap.geoObjects.add(myPlacemark)
		//myPlacemark.balloon.open()

	<? }
} ?>
