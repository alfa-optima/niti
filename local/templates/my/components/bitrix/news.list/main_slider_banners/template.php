<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (count($arResult["ITEMS"]) > 0) {

    $slider_items = array();
    $other_items = array();

    foreach ($arResult["ITEMS"] as $arItem) {
        if ($arItem['PROPERTIES']['FOR_SLIDER']['VALUE'] == 'Y') {
            $slider_items[] = $arItem;
        } else {
            $other_items[] = $arItem;
        }
    }
    $other_items = array_slice($other_items, 0, 2);

    $result['main'] = [];
    $result['sub'] = [];
    $result['all'] = [];

    $cnt = 0;
    foreach ($slider_items as $arItem) {
        $cnt++;
        //$pic_src = rIMG($arItem['PREVIEW_PICTURE']['ID'], 5, 755, 545);

        $result['main'][$cnt]['img_big'] = rIMG($arItem['PREVIEW_PICTURE']['ID'], 5, 1000, 770);

        if (strlen(trim($arItem['PROPERTIES']['LINK']['VALUE'])) > 0
            &&
            strlen(trim($arItem['PROPERTIES']['LINK_TEXT']['VALUE'])) > 0) {
            $result['main'][$cnt]['link'] = [
                'title' => $arItem['PROPERTIES']['LINK_TEXT']['VALUE'],
                'link' => $arItem['PROPERTIES']['LINK']['VALUE'],
            ];
        }

        $result['main'][$cnt]['sub_title'] = $arItem['NAME'];

        if ($arItem['PREVIEW_TEXT']) {
            $result['main'][$cnt]['sub_text'] = $arItem['PREVIEW_TEXT'];
        }

    }

    foreach ($other_items as $arItem) {

        $pic_src = rIMGG($arItem['PREVIEW_PICTURE']['ID'], 5, 390, 277);

        $link = false;
        if (strlen(trim($arItem['PROPERTIES']['LINK']['VALUE'])) > 0) {
            $link = $arItem['PROPERTIES']['LINK']['VALUE'];
        }

        $result['sub'][] = [
            'img_big' => rIMGG($arItem['PREVIEW_PICTURE']['ID'], 5, 1000, 770),
            'img' => rIMGG($arItem['PREVIEW_PICTURE']['ID'], 5, 390, 277),
            'link' => $link,
        ];

    }

    $result['main'] = array_values($result['main']);
    $result['all'] = array_merge ($result['main'], $result['sub']);
    echo "<script>var main_banners = " . json_encode($result) . " ;</script>";

}

/*
 * if (count($arResult["ITEMS"]) > 0) {

    $slider_items = array();
    $other_items = array();

    foreach ($arResult["ITEMS"] as $arItem) {
        if ($arItem['PROPERTIES']['FOR_SLIDER']['VALUE'] == 'Y') {
            $slider_items[] = $arItem;
        } else {
            $other_items[] = $arItem;
        }
    }
    $other_items = array_slice($other_items, 0, 2); ?>

  <div class="center">

    <section class="promo_banners">

      <div class="promo_banners_flex">

        <div class="promo_banners_slider owl-carousel">

            <? $cnt = 0;
            foreach ($slider_items as $arItem) {
                $cnt++;

                $pic_src = rIMG($arItem['PREVIEW_PICTURE']['ID'], 5, 755, 545); ?>

              <div class="promo_banners_slider_item" <? if ($cnt > 1){ ?>style="display:none"<? } ?>>
                  <? if (
                      strlen(trim($arItem['PROPERTIES']['LINK']['VALUE'])) > 0
                      &&
                      strlen(trim($arItem['PROPERTIES']['LINK_TEXT']['VALUE'])) > 0
                  ) { ?>
                    <div class="promo_banners_button">
                      <a href="<?= $arItem['PROPERTIES']['LINK']['VALUE'] ?>"><?= $arItem['PROPERTIES']['LINK_TEXT']['VALUE'] ?></a>
                    </div>
                  <? } ?>
                <div class="promo_banners_sub_flex">
                  <div class="promo_banners_sub">
                    <div class="promo_banners_sub_title"><?= $arItem['NAME'] ?></div>
                      <? if ($arItem['PREVIEW_TEXT']) { ?>
                        <div class="promo_banners_sub_text"><?= $arItem['PREVIEW_TEXT'] ?></div>
                      <? } ?>
                  </div>
                </div>
                <div class="promo_banners_image">
                  <a
                      <? if (strlen(trim($arItem['PROPERTIES']['LINK']['VALUE'])) > 0){ ?>href="<?= $arItem['PROPERTIES']['LINK']['VALUE'] ?>"<? } ?>>
                    <span class="promo_banners_overlay"></span>
                    <img src="<?= $pic_src ?>">
                  </a>
                </div>
              </div>

            <? } ?>

        </div>

        <div class="promo_banners_right">

            <? foreach ($other_items as $arItem) {

                $pic_src = rIMGG($arItem['PREVIEW_PICTURE']['ID'], 5, 390, 277); ?>

              <div class="promo_banners_right_item">
                <div class="promo_banners_button">
                  <a
                      <? if (strlen(trim($arItem['PROPERTIES']['LINK']['VALUE'])) > 0){ ?>href="<?= $arItem['PROPERTIES']['LINK']['VALUE'] ?>"<? } ?>>
                    Купить
                  </a>
                </div>
                <a
                    <? if (strlen(trim($arItem['PROPERTIES']['LINK']['VALUE'])) > 0){ ?>href="<?= $arItem['PROPERTIES']['LINK']['VALUE'] ?>"<? } ?>>
                  <span class="promo_banners_right_overlay"></span>
                  <img src="<?= $pic_src ?>">
                </a>
              </div>

            <? } ?>

        </div>

      </div>

    </section>

  </div>

  <script>
  $(document).ready(function () {
   $('.promo_banners_slider').owlCarousel({
    items: 1,
    loop: true,
    nav: false
   })
  })
  </script>


    <? if (1 == 2) { ?>

    <section class="promo_banners">
      <div class="cont">
        <div class="grid">

            <? $cnt = 0;
            foreach ($arResult["ITEMS"] as $arItem) {

                if ($arItem['PROPERTIES']['FOR_SLIDER']['VALUE'] == 'Y') {
                    $cnt++;

                    if ($cnt == 1) {

                        $class = 'big';
                        $pic_src = rIMGG($arItem['PREVIEW_PICTURE']['ID'], 5, 634, 562); ?>

                      <div class="item_wrap <?= $class ?>">
                        <a
                            <? if ($arItem['PROPERTIES']['LINK']['VALUE']){ ?>href="<?= $arItem['PROPERTIES']['LINK']['VALUE'] ?>"<? } ?>
                            class="item">
                          <img src="<?= $pic_src ?>">
                        </a>
                      </div>

                    <? }
                }
            } ?>

            <? foreach ($arResult["ITEMS"] as $arItem) {

                if ($arItem['PROPERTIES']['FOR_SLIDER']['VALUE'] != 'Y') {

                    $class = '';
                    $pic_src = rIMGG($arItem['PREVIEW_PICTURE']['ID'], 5, 501, 266); ?>

                  <div class="item_wrap <?= $class ?>">
                    <a
                        <? if ($arItem['PROPERTIES']['LINK']['VALUE']){ ?>href="<?= $arItem['PROPERTIES']['LINK']['VALUE'] ?>"<? } ?>
                        class="item">
                      <img src="<?= $pic_src ?>">
                    </a>
                  </div>

                <? }
            } ?>

        </div>
      </div>
    </section>

    <? } ?>

<? } ?>
 */
