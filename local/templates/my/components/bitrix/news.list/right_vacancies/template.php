<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (count($arResult["ITEMS"]) > 0){?>

	<div class="articles">
	
		<div class="title">Другие вакансии</div>
		
		<? foreach($arResult["ITEMS"] as $arItem){
		
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM'))); ?>
			
			<div class="item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
				<div class="thumb">
					<a href="<?=$arItem['DETAIL_PAGE_URL']?>">
						<img src="<?=rIMGG($arItem['PREVIEW_PICTURE']['ID'], 5, 270, 200)?>">
					</a>
				</div>

				<div class="name">
					<a href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['NAME']?></a>
				</div>

				<div class="date"><?=editDate(ConvertDateTime($arItem['PROPERTIES']['SORT_DATE']['VALUE'], "DD.MM.YYYY", "ru"))?> <?=ConvertDateTime($arItem['PROPERTIES']['SORT_DATE']['VALUE'], "HH:MI", "ru")?></div>

				<div class="desc"><?=obrezka(strip_tags($arItem['PREVIEW_TEXT']),12, "..." )?></div>

				<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="details">Читать полностью</a>
			</div>
			
		<? } ?>

	</div>

<? } ?>