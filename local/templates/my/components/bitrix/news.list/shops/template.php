<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$cities = array();
if( count($arResult['ITEMS']) > 0 ){
	foreach ($arResult['ITEMS'] as $shop){
		$cities[] = $shop['PROPERTIES']['CITY']['VALUE'];
	}
	$cities = array_unique($cities);
	sort($cities);
} ?>


<div class="cont">

	<section class="shops">
		<h1 class="page_title">наши магазины</h1>

		<div class="map">
		
			<? if( count($arResult['ITEMS']) > 0 ){ ?>
			
				<div class="cities">
					<div class="title">Ваш город</div>

					<div class="city">
						<select name="city">
							<option value="" data-display="Все города"></option>
							<option value="all">Все города</option>
							<? foreach ( $cities as $city ){ ?>
								<option value="<?=$city?>"><?=$city?></option>
							<? } ?>
						</select>
					</div>

					<div class="count"><?=count($arResult['ITEMS'])?> <?=pfCnt(count($arResult['ITEMS']), "магазин", "магазина", "магазинов")?></div>

					<div class="scroll">
					
						<? foreach ( $arResult['ITEMS'] as $shop ){ ?>
						
							<a style="cursor:pointer" class="item shop___link" city="<?=$shop['PROPERTIES']['CITY']['VALUE']?>" item_id="<?=$shop['ID']?>">
								<div class="name"><?=$shop['PROPERTIES']['CITY']['VALUE']?></div>

								<div class="adres"><?=$shop['PROPERTIES']['ADDRESS']['VALUE']?></div>

								<div class="phone">тел: <?=$shop['PROPERTIES']['PHONE']['VALUE']?></div>
							</a>

						<? } ?>
						
					</div>
				</div>
				
			<? } ?>

			<div id="map"></div>
			
		</div>
	</section>

</div>