<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (count($arResult["ITEMS"]) > 0){ ?>

	<div class="title"><?=$arParams['BLOCK_TITLE']?></div>

	<div class="socials">
	
		<? foreach($arResult["ITEMS"] as $arItem){
		
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM'))); ?>
	
			<a href="<?=$arItem['PROPERTIES']['LINK']['VALUE']?>" target="_blank" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
				<img src="<?=rIMGG($arItem['PREVIEW_PICTURE']['ID'], 4, 19, 19)?>">
			</a>

		<? } ?>
		
	</div>
	
<? } ?>