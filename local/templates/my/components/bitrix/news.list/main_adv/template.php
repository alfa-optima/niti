<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (count($arResult["ITEMS"]) > 0) { ?>

    <?
    $result = [];

    foreach ($arResult["ITEMS"] as $arItem) {

        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

        $item = [
            'id' => $this->GetEditAreaId($arItem['ID']),
            'link' => false,
            'img' => rIMGG($arItem['PREVIEW_PICTURE']['ID'], 4, 35, 35),
            'title' => $arItem["~PREVIEW_TEXT"]
        ];

        if (strlen($arItem['PROPERTIES']['LINK']['VALUE']) > 0) {
            $item['link'] = $arItem['PROPERTIES']['LINK']['VALUE'];
        }

        $result[] = $item;

    }

    echo "<script>var main_adv = " . json_encode($result) . ";</script>";


}

/*
<div class="item_wrap" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
          <? if (strlen($arItem['PROPERTIES']['LINK']['VALUE']) > 0){ ?>
        <a href="<?= $arItem['PROPERTIES']['LINK']['VALUE'] ?>" style="text-decoration:none">
            <? } ?>
          <div class="item">
            <div class="icon">
              <img src="<?= rIMGG($arItem['PREVIEW_PICTURE']['ID'], 4, 35, 35) ?>">
            </div>
            <div class="name"><?= $arItem["~PREVIEW_TEXT"] ?></div>
          </div>
            <? if (strlen($arItem['PROPERTIES']['LINK']['VALUE']) > 0){ ?>
        </a>
      <? } ?>
      </div>
