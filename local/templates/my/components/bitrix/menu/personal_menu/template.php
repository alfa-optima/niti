<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($arResult)){ ?>

	<div class="page_menu">

		<? foreach($arResult as $arItem){
			if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
				continue;

			if($arItem["SELECTED"]){ ?>
				<a class="active" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
			<? } else { ?>
				<a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
			<? }

		} ?>
	
	</div>
	
<? } ?>