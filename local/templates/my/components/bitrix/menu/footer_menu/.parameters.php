<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"TOP_LINK_TITLE" => Array(
		"NAME" => 'Заголовок верхнего пункта',
		"TYPE" => "TEXT",
	),
	"TOP_LINK_URL" => Array(
		"NAME" => 'Ссылка верхнего пункта',
		"TYPE" => "TEXT",
	),
);
?>
