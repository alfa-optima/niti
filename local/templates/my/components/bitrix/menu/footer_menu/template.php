<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($arResult)){ ?>

	<div class="links">
	
		<? if( strlen($arParams['TOP_LINK_TITLE']) > 0 ){ ?>
			<div class="title">
				<a <? if( strlen($arParams['TOP_LINK_URL']) > 0 ){ ?>href="<?=$arParams['TOP_LINK_URL']?>"<? } else { ?>style="text-decoration:none"<? } ?>><?=$arParams['TOP_LINK_TITLE']?></a>
			</div>
		<? } ?>
		
		<ul>
		
			<? foreach($arResult as $arItem){
				if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
					continue;

				if($arItem["SELECTED"]){ ?>
				
					<li><a href="<?=$arItem["LINK"]?>" <? if( substr_count($arItem["LINK"], '#') ){ ?>class="modal_link"<? } ?>><?=$arItem["TEXT"]?></a></li>
					
				<? } else { ?>
				
					<li><a href="<?=$arItem["LINK"]?>" <? if( substr_count($arItem["LINK"], '#') ){ ?>class="modal_link"<? } ?>><?=$arItem["TEXT"]?></a></li>
					
				<? }

			} ?>
		
		</ul>
		
	</div>
	
<? } ?>