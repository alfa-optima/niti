<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools; ?>

<div class="images left">
  <div class="big photo___block" style="display:none">

    <div id="photo_block_slider" class="slider owl-carousel">
      <div class="slide">

          <? if ( $arResult['default_photo_id'] ) { ?>

                <a href="<?=rIMG($arResult['default_photo_id'], 1, 801, 700) ?>" class="fancy_img" data-fancybox="gallery">
                    <img class="default_photo" src="<?= rIMG($arResult['default_photo_id'], 4, 402, 478) ?>">
                </a>

          <? } else { ?>

                <img src="<?=SITE_TEMPLATE_PATH?>/images/no_image.png">

          <? } ?>

      </div>
    </div>

  </div>
</div>

<div class="data left product_card__central_block">

  <h1 class="prod_name"><?= htmlspecialchars_decode($arResult['product_name']) ?></h1>

  <div class="articul">Артикул: <span><?= $arResult['PROPERTIES']['CML2_ARTICLE']['VALUE'] ?></span></div>

    <? if (strlen($arResult['PROPERTIES']['CML2_ARTICLE']['VALUE']) > 0) { ?>
      <div class="articul">Артикул2: <span><?= $arResult['PROPERTIES']['CML2_ARTICLE']['VALUE'] ?></span></div>
    <? } ?>

    <? if ($arResult['price']) { ?>

      <div class="price price___block">
        <div class="new">
            <? if ($arResult['defaultPriceIsEq'] == 0) {
                echo 'от ';
            } ?>
            <?= number_format($arResult['price'], 0, ",", " ") ?> <span class="currency">:</span>
        </div>

        <div class="old old___price_block">1290 Р</div>
        <div class="percents discount___block">-20%</div>

      </div>

    <? } ?>

    <? if (
        is_array($arResult['offersInfo']['color_list'])
        &&
        count($arResult['offersInfo']['color_list']) > 0
    ): ?>

      <div class="color">
        <div class="title">Цвет</div>
        <div class="grid">
            <? // Вывод всех цветов
            $k = 0;
            foreach ($arResult['offersInfo']['color_list'] as $key => $arColor) {
                $k++;
                $isEqPrices = isEqualPrices($arResult['offersInfo']['color_prices'], $arColor['color']['ID']); ?>

              <input type="radio" name="color" id="prod_color<?= $k ?>">

              <label for="prod_color<?= $k ?>" class="color___label" equal_prices="<?= $isEqPrices ?>"
                     min_price="<?= $arResult['offersInfo']['color_min_prices'][$arColor['color']['ID']] ?>"
                     color_id="<?= $arColor['color']['ID'] ?>" title="<?= $arColor['color']['NAME'] ?>"
                     photo="<?= rIMG($arColor['PROPERTIES']['PHTO']['VALUE'], 5, 402, 478) ?>"
                     photo_big="<?= rIMG($arColor['PROPERTIES']['PHTO']['VALUE'], 4, 800, 700) ?>">
                  <? if (intval($arColor['color']['PREVIEW_PICTURE']) > 0) { ?>
                    <img src="<?= \CFile::GetPath($arColor['color']['PREVIEW_PICTURE']) ?>">
                  <? } else { ?>
                    <img src="/getColor.php?color_code=<?= str_replace("#", "", $arColor['color']['PROPERTY_COLOR_VALUE'] ? $arColor['color']['PROPERTY_COLOR_VALUE'] : $arColor['color']['PROPERTY_COLOR_CODE_VALUE']) ?>">
                  <? } ?>
              </label>

            <? } ?>

        </div>
      </div>

        <? if (
            is_array($arResult['offersInfo']['color_sizes'])
            &&
            count($arResult['offersInfo']['color_sizes']) > 0
        ): ?>
        <div class="size">
          <div class="title">Размер</div>
          <div class="modalErrorSizeYellow-empty">
            <p class="modal___error_p"></p>
              <? // Выводим блоки с размерами
              $k = 0;
              foreach ($arResult['offersInfo']['color_sizes'] as $color_id => $sizes) { ?>

                <div class="grid colorSizesBlock" color_id="<?= $color_id ?>"
                     style="display:<? if ($color_id == $default_color_id) { ?>flex<? } else { ?>none<? } ?>">

                    <? foreach ($sizes as $size) {
                        $k++; ?>

                      <input type="radio" name="size" id="prod_size<?= $k ?>"
                             value="<?= $arResult['offersInfo']['color_prices'][$color_id][$size]['ID'] ?>">

                      <label for="prod_size<?= $k ?>" class="size___label"
                             price="<?= $arResult['offersInfo']['color_prices'][$color_id][$size]['PRICE'] ?>"
                             old_price="<?= $arResult['offersInfo']['color_prices'][$color_id][$size]['OLD_PRICE'] ?>"><?= $size ?></label>

                    <? } ?>

                </div>

              <? } ?>
          </div>
        </div>
        <?
        endif; //is_array($arResult['offersInfo']['color_sizes'])

    else:

        if (
            is_array($arResult['offersInfo']['sizes'])
            &&
            count($arResult['offersInfo']['sizes']) > 0
        ): ?>
          <div class="size">

            <div class="title">Размер</div>

            <div class="modalErrorSizeYellow-empty">

              <p class="modal___error_p"></p>

              <div class="grid colorSizesBlock" color_id="">

                  <? $k = 0;
                  foreach ($arResult['offersInfo']['sizes'] as $size => $sizeInfo) {
                      $k++; ?>

                    <input type="radio" name="size" id="prod_size<?= $k ?>" value="<?= $sizeInfo['ID'] ?>">

                    <label for="prod_size<?= $k ?>" class="size___label" price="<?= $sizeInfo['PRICE'] ?>"
                           old_price="<?= $sizeInfo['OLD_PRICE'] ?>"><?= $size ?></label>

                  <? } ?>

              </div>

            </div>

          </div>
        <?
        endif; //is_array($arResult['offersInfo']['sizes'])

    endif; //is_array($arResult['offersInfo']['color_list']) <-- main
    ?>

  <div class="product_card__buy_block">

    <div class="buy left">

        <? if ($arResult['price']) { ?>
          <a tov_name="<?= $arResult['NAME'] ?>"
             <? if ($arResult['sku_id']){ ?>sku_id="<?= $arResult['sku_id'] ?>"<? } ?> style="cursor:pointer"
             class="buy_link add_to_basket to___process">
            <img src="<?=SITE_TEMPLATE_PATH?>/images/ic_buy_link.png">в корзину
          </a>
        <? } ?>

      <a href="#buy_1_click_modal" class="modal_link quike_buy_link" tov_id="<?= $arResult['ID'] ?>">купить в 1 клик</a>

    </div>

    <a style="cursor:pointer" class="favorite_link left fav___link to___process" tov_id="<?= $arResult['ID'] ?>">
      <div class="hover add_hover">Добавить в избранное</div>
      <div class="hover del_hover">Убрать из избранного</div>
    </a>
    <div class="clear"></div>

  </div>

</div>

<input type="hidden" name="tovar_id" value="<?= $arResult['ID'] ?>">

<script>
// $('#photo_block_slider').owlCarousel({
//   loop: true,
//   dots: false,
//   nav: false,
//   items: 1,
//   navSpeed: 500,
//   smartSpeed: 500,
//   autoplaySpeed: 500,
//   margin: 0,
// })
</script>
