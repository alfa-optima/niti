<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();


$arResult['product_name'] = $arResult['NAME'];
if( $arResult['DISPLAY_PROPERTIES']['PRINT_ID']['LINK_ELEMENT_VALUE'] ){
	$keys = array_keys($arResult['DISPLAY_PROPERTIES']['PRINT_ID']['LINK_ELEMENT_VALUE']);
	if( count($keys) > 0 ){
		$arResult['product_name'] = $arResult['DISPLAY_PROPERTIES']['PRINT_ID']['LINK_ELEMENT_VALUE'][$keys[0]]['NAME'];
	}
}

 
$arResult['DEFAULT_OFFER'] = false;
foreach ( $arResult['OFFERS'] as $offer ){
    if( $offer["PROPERTIES"]['MAIN_TP']['VALUE'] ){
        $arResult['DEFAULT_OFFER'] = $offer;
    }
}
if( !$arResult['DEFAULT_OFFER'] ){   $arResult['DEFAULT_OFFER'] = $arResult['OFFERS'][0];   }

$arResult['default_photo_id'] = $arResult['DEFAULT_OFFER']['DETAIL_PICTURE']['ID'];
$arResult['default_photo_url'] = $arResult['DEFAULT_OFFER']['DETAIL_PICTURE']['SRC'];


$arResult['price'] = false;
$arResult['sku_id'] = false;
$arResult['picture'] = false;


// Инфо по ТП
$arResult['offersInfo'] = getTovOffers($arResult);

if( count($arResult['offersInfo']['color_list']) > 0 ){

	$keys = array_keys($arResult['offersInfo']['color_list']);
	$default_color_key = $keys[0];

	$default_color_id = $arResult['offersInfo']['color_list'][$default_color_key]['color']['ID'];
	
	$arResult['price'] = $arResult['offersInfo']['color_min_prices'][$arResult['offersInfo']['color_list'][$default_color_key]['color']['ID']];

	$arResult['defaultPriceIsEq'] = isEqualPrices($arResult['offersInfo']['color_prices'], $default_color_id);

	// Фото и видео
	if ( strlen($arResult['offersInfo']['color_list'][$default_color_key]['PROPERTIES']['PHTO']['VALUE']) > 0
	){   
		$arResult['picture'] = $arResult['offersInfo']['color_list'][$default_color_key]['PROPERTIES']['PHTO']['VALUE'];
	}

} else {
	
	if( file_exists($_SERVER['DOCUMENT_ROOT'].$arResult['OFFERS'][0]['PROPERTIES']['PHTO']['SRC']) ){
		$arResult['picture'] = $arResult['OFFERS'][0]['PROPERTIES']['PHTO']['VALUE'];
	}
	
	if ( count($arResult['offersInfo']['sizes']) > 0 ){

		$prices = array();
		foreach ($arResult['offersInfo']['sizes'] as $size => $sizeInfo){
			if( !$arResult['price'] ){   $arResult['price'] = $arResult['offersInfo']['sizes'][$size]['PRICE'];   }
			$prices[] = $sizeInfo['PRICE'];
		}
		$prices = $arResult['offersInfo']['prices'];
		$min_price = min($prices); 
		$max_price = max($prices);
		
		$arResult['defaultPriceIsEq'] = $max_price==$min_price?1:0;
	
	} else {
		
		if( count($arResult['OFFERS']) > 0 ){
			$cnt = 0;
			foreach ( $arResult['OFFERS'] as $offer ){ $cnt++;
				$prices[] = $offer['PRICES']['BASE']['DISCOUNT_VALUE_VAT'];
				if( $cnt == 1 ){     $arResult['sku_id'] = $offer['ID'];     }
			}
			$arResult['price'] = min($prices); 
			$max_price = max($prices); 
			
			$arResult['defaultPriceIsEq'] = $max_price==$arResult['price']?1:0;
		}
	}
	
}
