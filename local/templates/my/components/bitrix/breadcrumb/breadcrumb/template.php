<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
include "lang/ru/template.php";

if(empty($arResult)) 
	return "";


$strReturn = '<div class="breadcrumbs"><div class="cont"><a href="/">Главная</a> <span class="sep"></span>';
$page_title = $GLOBALS['APPLICATION']->GetTitle();

$itemSize = count($arResult)-1;

for ($index = 0; $index <= $itemSize; $index++) {
	$title = /*htmlspecialcharsex(*/ $arResult[$index]["TITLE"] /*)*/;
	if (
		$arResult[($index+1)]["TITLE"] == $arResult[($index)]["TITLE"]
		&&
		$arResult[($index+1)]["LINK"] == $arResult[($index)]["LINK"]
	){} else {
		if(($arResult[$index]["LINK"] <> "") && $arResult[$index+1]["TITLE"])
			$strReturn .= '<a href="'.$arResult[$index]["LINK"].'" title="'.$title.'">'.$title.'</a> <span class="sep"></span>';
		else
			$strReturn .= $title;
	}
}

$strReturn .= '</div></div>';

return $strReturn; ?>
