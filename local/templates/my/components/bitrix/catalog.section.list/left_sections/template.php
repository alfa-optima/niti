<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if( count($arResult["SECTIONS"]) > 0 ){
	
	$sections = array();
	
	foreach ( $arResult["SECTIONS"] as $sect ){
		if( !$min_level || $min_level > $sect['DEPTH_LEVEL'] ){
			$min_level = $sect['DEPTH_LEVEL'];
		}
	}
	
	foreach ( $arResult["SECTIONS"] as $sect ){
		if( $sect['DEPTH_LEVEL'] == $min_level ){
			$sections[$sect['ID']] = $sect;
		}
	}
	
	foreach ( $arResult["SECTIONS"] as $sect ){
		if( 
			$sect['DEPTH_LEVEL'] == ($min_level + 1)
			&&
			$sections[$sect['IBLOCK_SECTION_ID']]
		){
			$sections[$sect['IBLOCK_SECTION_ID']]['subsections'][$sect['ID']] = $sect;
		}
	} 
	
	foreach ( $sections as $sect_id => $sect_1 ){
		if( $sect_1['UF_GENDER_GROUP'] != $arParams['GENDER_TYPE']['ID'] ){
			unset($sections[$sect_id]);
		}
	} ?>

	<div class="cats">
		<ul>
		
			<? foreach ($sections as $section){ ?>
			
				<li>
				
					<a class="left_menu_link" style="cursor:pointer"><?=$section['UF_NAME']?$section['UF_NAME']:$section['NAME']?></a>

					<? // Считаем разделы 3 уровня
					if ( count($section['subsections']) > 0 ){ ?>
					
						<ul class="left_sections_ul" parent_url="<?=$section['SECTION_PAGE_URL']?>" style="display:none">
						
							<? foreach ( $section['subsections'] as $sect ){ ?>
							
								<li>
									<a href="<?=$sect['SECTION_PAGE_URL']?>"><?=$sect['UF_NAME']?$sect['UF_NAME']:$sect['NAME']?></a>
								</li>
								
							<? } ?>
							
						</ul>
						
					<? } ?>
					
				</li>
			
			<? } ?>
			
		</ul>
	</div>

<? } ?>