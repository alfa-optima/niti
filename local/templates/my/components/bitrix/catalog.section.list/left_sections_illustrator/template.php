<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (count($arResult["SECTIONS"]) > 0){ ?>

	<div class="cats">
		<ul>
		
			<? foreach ($arResult["SECTIONS"] as $section_1){
				
				if ( $section_1['DEPTH_LEVEL'] == 1 ){ ?>
			
					<li>
					
						<a href="<?=$section_1['SECTION_PAGE_URL']?>"><?=$section_1['NAME']?></a>
						
					</li>
			
				<? }
			} ?>
			
		</ul>
	</div>

<? } ?>