<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$main_collections = array();

if( count($arResult['ITEMS']) > 0 ){
	$cnt = 0; $key = 0;
	foreach( $arResult['ITEMS'] as $arItem ){ $cnt++;
		$main_collections[$key][] = clean_array($arItem);
		if( $cnt%10 == 0 ){   $key++;   }
	}
}

if( count($main_collections) > 0 ){ ?>


	<section class="collections">
		<div class="cont">

			<div class="block_title"><div>коллекции</div></div>

			<? foreach( $main_collections as $block_collections ){ ?>

				<div class="hor_scroll">
					<div class="grid">

						<div class="item_wrap">

							<? $cnt = 0;
							foreach( $block_collections as $collection ){ $cnt++;

								$height = 180;
								if( in_array($cnt, array(1, 7)) ){    $height = 385;    } ?>

								<a href="<?=$collection['DETAIL_PAGE_URL']?>" class="item">
									<? if( intval($collection['DETAIL_PICTURE']['ID']) > 0 ){ ?>
										<img src="<?=rIMG($collection['DETAIL_PICTURE']['ID'], 5, 270, $height)?>">
									<? } else { ?>
										<div style="display:block; width: 270px; height: <?=$height?>px; background-color: #f4f4f4;"></div>
									<? } ?>
									<div class="name"><?=$collection['NAME']?></div>
								</a>

								<? if(
									in_array($cnt, array(2, 5, 7))
									&&
									$cnt < count($block_collections)
								){    echo '</div><div class="item_wrap">';    }

							} ?>

						</div>

					</div>
				</div>

			<? } ?>

		</div>
	</section>


<? } ?>
