<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

// Инфо об элементе
$collection = tools\el::info_by_code($arResult['VARIABLES']['ELEMENT_CODE'], $arParams['IBLOCK_ID']);

if (
    intval( $collection['ID'] ) > 0
    &&
    $collection['DETAIL_PAGE_URL'] == tools\funcs::pureURL()
){


    $filter_cache_ib_prop = new project\filter_cache_ib_prop();

    $_GET[ $filter_cache_ib_prop->propParams['COLLECTION']['searchParamCode'] ] = $collection['CODE'];
    $APPLICATION->IncludeComponent( "my:search", '' );





} else {

    // 404
    include_once($_SERVER['DOCUMENT_ROOT'] . '/include/areas/404.php');

}
