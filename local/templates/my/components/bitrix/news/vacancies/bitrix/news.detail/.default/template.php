<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<section class="content left">
	<div class="text_block">
		<h1><?=$arResult['NAME']?></h1>

		<p class="date"><?=strtolower(editDATE(ConvertDateTime($arResult["PROPERTIES"]['SORT_DATE']['VALUE'], "DD.MM.YYYY", "ru")))?> <?=ConvertDateTime($arResult["PROPERTIES"]['SORT_DATE']['VALUE'], "HH:MI", "ru")?></p>

		<img src="<?=rIMGG($arResult['DETAIL_PICTURE']['ID'], 5, 870, 460)?>">

		<p>
			<?=$arResult['DETAIL_TEXT']?>
		</p>

	</div>


	<!---<div class="tags">
		<div class="grid">
			<a href="/">Магазин</a>
			<a href="/">Niti-niti</a>
			<a href="/">Готовый набор</a>
			<a href="/">Товар</a>
		</div>
	</div>--->
</section>