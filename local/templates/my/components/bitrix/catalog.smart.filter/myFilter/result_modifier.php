<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if (isset($arParams["TEMPLATE_THEME"]) && !empty($arParams["TEMPLATE_THEME"]))
{
	$arAvailableThemes = array();
	$dir = trim(preg_replace("'[\\\\/]+'", "/", dirname(__FILE__)."/themes/"));
	if (is_dir($dir) && $directory = opendir($dir))
	{
		while (($file = readdir($directory)) !== false)
		{
			if ($file != "." && $file != ".." && is_dir($dir.$file))
				$arAvailableThemes[] = $file;
		}
		closedir($directory);
	}

	if ($arParams["TEMPLATE_THEME"] == "site")
	{
		$solution = COption::GetOptionString("main", "wizard_solution", "", SITE_ID);
		if ($solution == "eshop")
		{
			$theme = COption::GetOptionString("main", "wizard_eshop_adapt_theme_id", "blue", SITE_ID);
			$arParams["TEMPLATE_THEME"] = (in_array($theme, $arAvailableThemes)) ? $theme : "blue";
		}
	}
	else
	{
		$arParams["TEMPLATE_THEME"] = (in_array($arParams["TEMPLATE_THEME"], $arAvailableThemes)) ? $arParams["TEMPLATE_THEME"] : "blue";
	}
} else {
	$arParams["TEMPLATE_THEME"] = "blue";
}



// Коллекции
//$filterCollectionsIDs = [];
//foreach( $arResult["ITEMS"] as $key => $arItem ){
//    if( $arItem["CODE"] == 'COLLECTION' ){
//        foreach($arItem["VALUES"] as $collection_id => $ar){
//            if ( !checkCollection( $collection_id, $arParams['SEARCH_IDS'] ) ){
//                unset($arItem["VALUES"][$collection_id]);
//            } else {
//                $filterCollectionsIDs[] = $collection_id;
//            }
//        }
//        $arResult["ITEMS"][$key] = $arItem;
//    }
//}
//$filterCollections = project\collection::getList( $filterCollectionsIDs );
//global $collectionRecursiveList;
//$collectionRecursiveList = [];
//project\collection::recursiveList( $filterCollections );
//$arResult['COLLECTIONS'] = project\collection::groupListForFilter( $collectionRecursiveList );


