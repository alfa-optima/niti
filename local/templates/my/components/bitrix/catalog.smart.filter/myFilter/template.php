<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

\Bitrix\Main\Loader::includeModule('aoptima.tools');

use AOptima\Tools as tools;

$_SESSION['FILTER_COLOR_ID'] = false;

$values = 0; ?>


<div class="filter">

	<form name="<?=$arResult["FILTER_NAME"]."_form" ?>" action="<?=$arResult["FORM_ACTION"] ?>" method="get" class="smartfilter">

        <? foreach ($arResult["HIDDEN"] as $arItem) { ?>

            <input type="hidden" name="<? echo $arItem["CONTROL_NAME"] ?>" id="<? echo $arItem["CONTROL_ID"] ?>" value="<?=$arItem["HTML_VALUE"] ?>"/>

        <? }

      // с цветами
      foreach ($arResult["ITEMS"] as $key => $arItem) {

          if (empty($arItem["VALUES"]) || isset($arItem["PRICE"]))
              continue;

          if (
              $arItem["DISPLAY_TYPE"] == "A"
              && (
                  $arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0
              )
          )
              continue;

          $arCur = current($arItem["VALUES"]);
          switch ($arItem["DISPLAY_TYPE"]) {
              case "A"://NUMBERS_WITH_SLIDER
                  break;
              case "B"://NUMBERS
                  break;
              case "G"://CHECKBOXES_WITH_PICTURES
                  break;
              case "H"://CHECKBOXES_WITH_PICTURES_AND_LABELS
                  break;
              case "P"://DROPDOWN
                  break;
              case "R"://DROPDOWN_WITH_PICTURES_AND_LABELS
                  break;
              case "K"://RADIO_BUTTONS
                  break;
              case "U"://CALENDAR
                  break;
              default://CHECKBOXES

                  if (empty($arItem["VALUES"]))
                      continue;

                  // Цвета
                  if ($arItem["CODE"] == 'COLORS_AUTO') {

                      foreach ($arItem["VALUES"] as $color_id => $ar) {
                          if (!checkColor($color_id, $arParams['SEARCH_IDS'])) {
                              unset($arItem["VALUES"][$color_id]);
                          }
                      }
                      foreach ($arItem["VALUES"] as $color_id => $ar) {
                          if ($ar["CHECKED"]) {
                              $_SESSION['FILTER_COLOR_ID'] = $color_id;
                          }
                      }

                      if (count($arItem["VALUES"]) > 0) { ?>

                        <div class="item">
                            <div class="name left"><?= $arItem["NAME"] ?></div>
                            <button type="button" class="reset_btn right block_unset_button">×</button>
                            <div class="clear"></div>
                            <div class="data">
                                <div class="color">

<? foreach ($arItem["VALUES"] as $color_id => $ar) {

    $color = tools\el::info($color_id);

    $values++; ?>

    <div class="item_wrap">
        <input
            type="checkbox"
            value="<? echo $ar["HTML_VALUE"] ?>"
            name="<? echo $ar["CONTROL_NAME"] ?>"
            id="<? echo $ar["CONTROL_ID"] ?>"
            <? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?>
            color_id="<?=$color['ID'] ?>"
            prop_code="<?=$arItem['CODE'] ?>"
            prop_value="<?=$ar["VALUE"]; ?>"
        >
        <label for="<? echo $ar["CONTROL_ID"] ?>" title="<?= $ar["VALUE"]; ?>" color_id="<?= $color['ID'] ?>">
            <? if (intval($color['PREVIEW_PICTURE']) > 0) { ?>
                <img src="<?= \CFile::GetPath($color['PREVIEW_PICTURE']) ?>">
            <? } else { ?>
                <img src="/getColor.php?color_code=<?= str_replace("#", "", $color['PROPERTY_COLOR_CODE_VALUE']) ?>">
            <? } ?>
        </label>
    </div>

<? } ?>

                                </div>

                                <div class="form item_filter_submit_block">

                                    <input
                                        class="submit_btn set___button"
                                        type="submit"
                                        id="set_filter"
                                        name="set_filter"
                                        value="<?= GetMessage("CT_BCSF_SET_FILTER") ?>"
                                    />

                                    <input
                                        class="submit_btn <? if (arURI()[1] == 'search') { ?>search_unset___button<? } else { ?>unset___button<? } ?>" type="button"
                                        <? if (arURI()[1] == 'search') { ?>
                                            q="<?= strip_tags(param_get('q')) ?>"
                                        <? } ?>
                                        id="del_filter"
                                        name="del_filter"
                                        value="<?= GetMessage("CT_BCSF_DEL_FILTER") ?>"
                                    />

                                </div>

                            </div>
                        </div>

                      <? }
                  }
          }

      }

      // без цветов
      foreach ($arResult["ITEMS"] as $key => $arItem) {

          if (empty($arItem["VALUES"]) || isset($arItem["PRICE"]))
              continue;

          if (
              $arItem["DISPLAY_TYPE"] == "A"
              && (
                  $arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0
              )
          )
              continue;

          $arCur = current($arItem["VALUES"]);
          switch ($arItem["DISPLAY_TYPE"]) {
              case "A"://NUMBERS_WITH_SLIDER
                  break;
              case "B"://NUMBERS
                  break;
              case "G"://CHECKBOXES_WITH_PICTURES
                  break;
              case "H"://CHECKBOXES_WITH_PICTURES_AND_LABELS
                  break;
              case "P"://DROPDOWN
                  break;
              case "R"://DROPDOWN_WITH_PICTURES_AND_LABELS
                  break;
              case "K"://RADIO_BUTTONS
                  break;
              case "U"://CALENDAR
                  break;
              default://CHECKBOXES

                  if (empty($arItem["VALUES"]))
                      continue;

                  if (
                      // Не цвета
                      $arItem["CODE"] != 'COLORS_AUTO'
                      //&&
                      // Не коллекции
                      //$arItem["CODE"] != 'COLLECTION'
                  ) { ?>

                    <div class="item">

                        <div class="name left"><?= $arItem["NAME"] ?></div>

                        <button type="button" class="reset_btn right block_unset_button">×</button>
                        <div class="clear"></div>

                        <div class="data">

                            <?php
                            /*
                            <div class="search">
                                <input type="text" class="input filter_search_input">
                            </div>
                            <div class="scroll">

        <? foreach ($arItem["VALUES"] as $val => $ar) {
            $values++; ?>

                                        <div class="line">
                                            <input
                                                            type="checkbox"
                                                            value="<? echo $ar["HTML_VALUE"] ?>"
                                                            name="<? echo $ar["CONTROL_NAME"] ?>"
                                                            id="<? echo $ar["CONTROL_ID"] ?>"
                <? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?>
                                                            prop_code="<?= $arItem['CODE'] ?>"
                                                            prop_value="<?= strtolower($ar["VALUE"]); ?>"
                                            >
                                            <label for="<? echo $ar["CONTROL_ID"] ?>"><?= $ar["VALUE"]; ?> <? if ($ar["ELEMENT_COUNT"]) { ?>
                                                    <span>(<?= $ar["ELEMENT_COUNT"] ?>)</span><? } ?></label>
                                        </div>

        <? } ?>

                            </div>

    echo '<pre style="font-size: 17px;background-color: #363636;color: #fff;padding: 20px;line-height: 26px;">';
    print_r($arItem["VALUES"]);
    echo '</pre>'; */ ?>

                            <div class="side_collections">
                                <ul class="side_collections__category_list">
                                    <li class="side_collections__category">
                                        <a class="side_collections__category_link">Животные</a>
                                        <ul class="side_collections__subcategory_list">
                                            <li class="side_collections__subcategory_name">
                                                <a class="side_collections__subcategory_link">Птицы</a>
                                            </li>
                                            <li class="side_collections__subcategory_name">
                                                <a class="side_collections__subcategory_link">Рыбы</a>
                                            </li>
                                            <li class="side_collections__subcategory_name">
                                                <a class="side_collections__subcategory_link">Медведи</a>
                                            </li>
                                            <li class="side_collections__subcategory_name">
                                                <a class="side_collections__subcategory_link">Коты</a>
                                            </li>
                                            <li class="side_collections__subcategory_name">
                                                <a class="side_collections__subcategory_link">Собаки</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="side_collections__category">
                                        <a class="side_collections__category_link">Любовь</a></li>
                                    <li class="side_collections__category">
                                        <a class="side_collections__category_link">Манимализм</a></li>
                                    <li class="side_collections__category">
                                        <a class="side_collections__category_link">Персонажи</a></li>
                                </ul>
                            </div>

                            <div class="form item_filter_submit_block">

                                <input
                                    class="submit_btn set___button"
                                    type="submit"
                                    id="set_filter"
                                    name="set_filter"
                                    value="<?= GetMessage("CT_BCSF_SET_FILTER") ?>"
                                />


                                <input
                                    class="submit_btn <? if (arURI()[1] == 'search') { ?>search_unset___button<? } else { ?>unset___button<? } ?>"
                                    type="button"
                                    <? if (arURI()[1] == 'search') { ?>
                                        q="<?= strip_tags(param_get('q')) ?>"
                                    <? } ?>
                                    id="del_filter"
                                    name="del_filter"
                                    value="<?= GetMessage("CT_BCSF_DEL_FILTER") ?>"
                                />

                            </div>

                        </div>

                    </div>

                  <? }

          }

      }

      // Коллекции
      if (count($arResult['COLLECTIONS']) > 0 && 0) { ?>

				<div class="item">
					<div class="name left">коллекции</div>

					<button type="reset" class="reset_btn right">Сбросить</button>
					<div class="clear"></div>

					<div class="data">
						<div class="search">
							<input type="text" name="" value="" class="input">
						</div>

						<div class="scroll">

    <? foreach ($arResult['COLLECTIONS'] as $group) {
        if ($group['SUB_ITEMS']) {
            foreach ($group['SUB_ITEMS'] as $sub_item) { ?>

                <div class="line">
                    <input type="checkbox" name="filter_collection" id="filter_collection1">
                    <label for="filter_collection1">Bad boy <span>(1)</span></label>
                </div>

            <? }
        } else { ?>

            <div class="line">
                <input type="checkbox" name="filter_collection" id="filter_collection2">
                <label for="filter_collection2">Flowers <span>(10)</span></label>
            </div>

        <? }
    } ?>

						</div>
					</div>
				</div>

          <?php if (0) { ?>

                <div class="item nn_menu_filter_collections">

                    <div class="name left nn_menu_filter_collections__title">Коллекции</div>

                    <button type="button" class="reset_btn right block_unset_button">×</button>
                    <div class="clear"></div>

                    <div class="data">

                        <div class="item_wrap">

                            <ul class="nn_menu_filter_collections__list">

    <? foreach ($arResult['COLLECTIONS'] as $group) {

        if ($group['SUB_ITEMS']) { ?>

            <li class="dropped_menu <? if (in_array(tools\funcs::param_get('collection_id'), array_keys($group['SUB_ITEMS']))) { ?>drop_active<? } ?>">

                <a href="javascript:void(0);"
                     class="filterCollectionGroup nn_menu_filter_collections__link">
                    <span><?= $group['NAME'] ?></span>
                    <span><?= count($group['SUB_ITEMS']) ?></span>
                </a>

                <ul class="dropped_items">

                    <? foreach ($group['SUB_ITEMS'] as $sub_item) { ?>

                        <li>
                            <a class="nn_menu_filter_collections__link" <? if (tools\funcs::param_get('collection_id') == $sub_item['ID']){ ?>style="text-decoration:underline"<? } ?> href="<?= addToRequestURI('collection_id', $sub_item['ID']) ?>"><?= $sub_item['NAME'] ?></a>
                        </li>

                    <? } ?>

                </ul>
            </li>

        <? } else { ?>

            <li>
                <a class="nn_menu_filter_collections__link" href="<?= addToRequestURI('collection_id', $group['ID']) ?>"><span><?= $group['NAME'] ?></span></a>
            </li>

        <? }

    } ?>

                            </ul>

                        </div>

                        <div class="form item_filter_submit_block">

                            <input
                                class="submit_btn set___button"
                                type="submit"
                                id="set_filter"
                                name="set_filter"
                                value="<?= GetMessage("CT_BCSF_SET_FILTER") ?>"
                            />

                            <input
                                class="submit_btn <? if (arURI()[1] == 'search') { ?>search_unset___button<? } else { ?>unset___button<? } ?>"
                                type="button"
                                <? if (arURI()[1] == 'search') { ?>
                                    q="<?= strip_tags(param_get('q')) ?>"
                                <? } ?>
                                id="del_filter"
                                name="del_filter"
                                value="<?= GetMessage("CT_BCSF_DEL_FILTER") ?>"
                            />

                        </div>

                    </div>
                </div>

          <?php } ?>


      <? }

      if ($values > 0) { ?>

            <div class="item submit form filter_submit_block" style="display: none">

                <input
                    class="submit_btn set___button"
                    type="submit"
                    id="set_filter"
                    name="set_filter"
                    value="<?= GetMessage("CT_BCSF_SET_FILTER") ?>"
                />

                <input
                    class="submit_btn <? if (arURI()[1] == 'search') { ?>search_unset___button<? } else { ?>unset___button<? } ?>"
                    type="button"
                    <? if (arURI()[1] == 'search') { ?>
                        q="<?= strip_tags(param_get('q')) ?>"
                    <? } ?>
                    id="del_filter"
                    name="del_filter"
                    value="<?= GetMessage("CT_BCSF_DEL_FILTER") ?>"
                />

            </div>

      <? } ?>

	</form>

</div>
