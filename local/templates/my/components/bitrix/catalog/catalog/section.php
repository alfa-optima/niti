<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

use \Bitrix\Main\Loader;
use \Bitrix\Main\ModuleManager;

$this->setFrameMode(true);

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;


// инфо по разделу
$section = tools\section::info($arResult['VARIABLES']['SECTION_ID']);

if (
    intval( $section['ID'] ) > 0
    &&
    tools\funcs::pureURL() == $section['SECTION_PAGE_URL']
){

    // Проверим наличие у данной категории привязанной страницы из фабрики поиска
    $searchFabricPage = false;
    if( intval($section['UF_FABRIC_PAGE']) > 0 ){
        $searchFabricPage = tools\el::info( $section['UF_FABRIC_PAGE'] );
    }

    if( intval( $searchFabricPage['ID'] ) > 0 ){

        // Получим цепочку разделов
        $section_chain = tools\section::chain($section['ID']);

        // Добавляем разделы цепочки в хлебные крошки
        foreach ($section_chain as $sect) {
            $APPLICATION->AddChainItem($sect["NAME"], $sect["SECTION_PAGE_URL"]);
        }

        // SEO-поля
        $APPLICATION->SetPageProperty("description", $section["UF_DESCRIPTION"] ? $section["UF_DESCRIPTION"] : $section["NAME"]);
        $APPLICATION->SetPageProperty("keywords", $section["UF_KEYWORDS"] ? $section["UF_KEYWORDS"] : $section["NAME"]);
        $APPLICATION->SetPageProperty("title", $section["UF_TITLE"] ? $section["UF_TITLE"] : $section["NAME"]);

        // Устанавливаем GET-параметры из наличии страницы из фабрики поиска
        project\search_fabric::setGetParams( $searchFabricPage );

        // Выводим страницу поиска
        $APPLICATION->IncludeComponent( "my:search", '' );

    } else {

        include( $_SERVER[ "DOCUMENT_ROOT" ] . "/include/areas/404.php" );
    }

} else {

    include_once($_SERVER['DOCUMENT_ROOT'] . '/include/areas/404.php');
}


