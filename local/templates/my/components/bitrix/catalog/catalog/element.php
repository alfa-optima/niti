<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

use \Bitrix\Main\Loader;
use \Bitrix\Main\ModuleManager;

$this->setFrameMode(true);

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;
\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

// Инфо о товаре
$el = tools\el::info_by_code($arResult['VARIABLES']['ELEMENT_CODE'], $arParams['IBLOCK_ID']);

if (
    intval($el['ID']) > 0
    &&
    pureURL() == $el['DETAIL_PAGE_URL']
//    &&
//    $el['PROPERTY_HAS_SKU_VALUE'] == 1
){

    $product_name = $el['NAME'];
    if (intval($el['PROPERTY_PRINT_ID_VALUE']) > 0) {
        $print = tools\el::info($el['PROPERTY_PRINT_ID_VALUE']);
        if (intval($print['ID']) > 0) {
            $product_name = $print['NAME'];
        }
    }

    // Добавляем в просмотренные
    add_to_viewed_session($el['ID']);

    // SEO-поля
    $APPLICATION->SetPageProperty("description", html_entity_decode($el["PROPERTY_DESCRIPTION_VALUE"] ? $el["PROPERTY_DESCRIPTION_VALUE"] : $product_name));
    $APPLICATION->SetPageProperty("keywords", html_entity_decode($el["PROPERTY_KEYWORDS_VALUE"] ? $el["PROPERTY_KEYWORDS_VALUE"] : $product_name));
    $APPLICATION->SetPageProperty("title", html_entity_decode($el["PROPERTY_TITLE_VALUE"] ? $el["PROPERTY_TITLE_VALUE"] : $product_name));


    if( !isset($_GET['_escaped_fragment_']) ){

        $DATA_OBJ = project\product::getObject($el["ID"]); ?>

        <script>
        /*var productObject = <? //CUtil::PhpToJSObject($DATA_OBJ, false, true)?>;*/
        var productObject = <? echo json_encode($DATA_OBJ, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);?>;
        </script>

			<div id="app_product"><appproduct></appproduct></div>

    <? } else {

        // Разделы товара
        $el_sections = tools\el::sections($el['ID']);

        // ID первого раздела
        $section_id = $el_sections[0]['ID'];

        // Получим цепочку разделов
        $section_chain = tools\section::chain($section_id);

        // Добавляем разделы цепочки в хлебные крошки
        foreach ($section_chain as $sect) {
            $APPLICATION->AddChainItem($sect["NAME"], $sect["SECTION_PAGE_URL"]);
        }

        // Добавляем сам товар в хлебные крошки
        $APPLICATION->AddChainItem($product_name, '');

        // Получаем комментарии
        $comment = new project\comment($el['ID']);
        $comments = $comment->GetList(); ?>

        <script type="text/javascript">
        $(document).ready(function () {
            var color_id = $.cookie('BITRIX_SM_color_id');
            setTimeout(function () {
              if (color_id) {
                if ($('.color___label[color_id=' + color_id + ']').length > 0) {
                  $('.color___label[color_id=' + color_id + ']').trigger('click');
                } else {
                  $('.color___label').eq(0).trigger('click');
                }
              } else {
                $('.color___label').eq(0).trigger('click');
              }
              $('.photo___block').show();
            }, 400)
        });
        </script>

        <section class="product_info product_detail_block very_strange_margin" item_id="<?= $el['ID'] ?>">
            <div class="cont">

                <?
                // Шаблон
                $componentElementParams = array(
                    'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
                    'IBLOCK_ID' => $arParams['IBLOCK_ID'],
                    'PROPERTY_CODE' => $arParams['DETAIL_PROPERTY_CODE'],
                    'META_KEYWORDS' => $arParams['DETAIL_META_KEYWORDS'],
                    'META_DESCRIPTION' => $arParams['DETAIL_META_DESCRIPTION'],
                    'BROWSER_TITLE' => $arParams['DETAIL_BROWSER_TITLE'],
                    'SET_CANONICAL_URL' => $arParams['DETAIL_SET_CANONICAL_URL'],
                    'BASKET_URL' => $arParams['BASKET_URL'],
                    'ACTION_VARIABLE' => $arParams['ACTION_VARIABLE'],
                    'PRODUCT_ID_VARIABLE' => $arParams['PRODUCT_ID_VARIABLE'],
                    'SECTION_ID_VARIABLE' => $arParams['SECTION_ID_VARIABLE'],
                    'CHECK_SECTION_ID_VARIABLE' => (isset($arParams['DETAIL_CHECK_SECTION_ID_VARIABLE']) ? $arParams['DETAIL_CHECK_SECTION_ID_VARIABLE'] : ''),
                    'PRODUCT_QUANTITY_VARIABLE' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
                    'PRODUCT_PROPS_VARIABLE' => $arParams['PRODUCT_PROPS_VARIABLE'],
                    'CACHE_TYPE' => $arParams['CACHE_TYPE'],
                    'CACHE_TIME' => $arParams['CACHE_TIME'],
                    'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
                    'SET_TITLE' => $arParams['SET_TITLE'],
                    'SET_LAST_MODIFIED' => $arParams['SET_LAST_MODIFIED'],
                    'MESSAGE_404' => $arParams['~MESSAGE_404'],
                    'SET_STATUS_404' => $arParams['SET_STATUS_404'],
                    'SHOW_404' => $arParams['SHOW_404'],
                    'FILE_404' => $arParams['FILE_404'],
                    'PRICE_CODE' => $arParams['PRICE_CODE'],
                    'USE_PRICE_COUNT' => $arParams['USE_PRICE_COUNT'],
                    'SHOW_PRICE_COUNT' => $arParams['SHOW_PRICE_COUNT'],
                    'PRICE_VAT_INCLUDE' => $arParams['PRICE_VAT_INCLUDE'],
                    'PRICE_VAT_SHOW_VALUE' => $arParams['PRICE_VAT_SHOW_VALUE'],
                    'USE_PRODUCT_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
                    'PRODUCT_PROPERTIES' => $arParams['PRODUCT_PROPERTIES'],
                    'ADD_PROPERTIES_TO_BASKET' => (isset($arParams['ADD_PROPERTIES_TO_BASKET']) ? $arParams['ADD_PROPERTIES_TO_BASKET'] : ''),
                    'PARTIAL_PRODUCT_PROPERTIES' => (isset($arParams['PARTIAL_PRODUCT_PROPERTIES']) ? $arParams['PARTIAL_PRODUCT_PROPERTIES'] : ''),
                    'LINK_IBLOCK_TYPE' => $arParams['LINK_IBLOCK_TYPE'],
                    'LINK_IBLOCK_ID' => $arParams['LINK_IBLOCK_ID'],
                    'LINK_PROPERTY_SID' => $arParams['LINK_PROPERTY_SID'],
                    'LINK_ELEMENTS_URL' => $arParams['LINK_ELEMENTS_URL'],
                    'OFFERS_CART_PROPERTIES' => $arParams['OFFERS_CART_PROPERTIES'],
                    'OFFERS_FIELD_CODE' => $arParams['DETAIL_OFFERS_FIELD_CODE'],
                    'OFFERS_PROPERTY_CODE' => $arParams['DETAIL_OFFERS_PROPERTY_CODE'],
                    'OFFERS_SORT_FIELD' => $arParams['OFFERS_SORT_FIELD'],
                    'OFFERS_SORT_ORDER' => $arParams['OFFERS_SORT_ORDER'],
                    'OFFERS_SORT_FIELD2' => $arParams['OFFERS_SORT_FIELD2'],
                    'OFFERS_SORT_ORDER2' => $arParams['OFFERS_SORT_ORDER2'],
                    'ELEMENT_ID' => $arResult['VARIABLES']['ELEMENT_ID'],
                    'ELEMENT_CODE' => $arResult['VARIABLES']['ELEMENT_CODE'],
                    'SECTION_ID' => $arResult['VARIABLES']['SECTION_ID'],
                    'SECTION_CODE' => $arResult['VARIABLES']['SECTION_CODE'],
                    'SECTION_URL' => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['section'],
                    'DETAIL_URL' => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['element'],
                    'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                    'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                    'HIDE_NOT_AVAILABLE' => $arParams['HIDE_NOT_AVAILABLE'],
                    'HIDE_NOT_AVAILABLE_OFFERS' => $arParams['HIDE_NOT_AVAILABLE_OFFERS'],
                    'USE_ELEMENT_COUNTER' => $arParams['USE_ELEMENT_COUNTER'],
                    'SHOW_DEACTIVATED' => $arParams['SHOW_DEACTIVATED'],
                    'USE_MAIN_ELEMENT_SECTION' => $arParams['USE_MAIN_ELEMENT_SECTION'],
                    'STRICT_SECTION_CHECK' => (isset($arParams['DETAIL_STRICT_SECTION_CHECK']) ? $arParams['DETAIL_STRICT_SECTION_CHECK'] : ''),
                    'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
                    'LABEL_PROP' => $arParams['LABEL_PROP'],
                    'LABEL_PROP_MOBILE' => $arParams['LABEL_PROP_MOBILE'],
                    'LABEL_PROP_POSITION' => $arParams['LABEL_PROP_POSITION'],
                    'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
                    'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
                    'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
                    'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
                    'DISCOUNT_PERCENT_POSITION' => (isset($arParams['DISCOUNT_PERCENT_POSITION']) ? $arParams['DISCOUNT_PERCENT_POSITION'] : ''),
                    'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
                    'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
                    'MESS_SHOW_MAX_QUANTITY' => (isset($arParams['~MESS_SHOW_MAX_QUANTITY']) ? $arParams['~MESS_SHOW_MAX_QUANTITY'] : ''),
                    'RELATIVE_QUANTITY_FACTOR' => (isset($arParams['RELATIVE_QUANTITY_FACTOR']) ? $arParams['RELATIVE_QUANTITY_FACTOR'] : ''),
                    'MESS_RELATIVE_QUANTITY_MANY' => (isset($arParams['~MESS_RELATIVE_QUANTITY_MANY']) ? $arParams['~MESS_RELATIVE_QUANTITY_MANY'] : ''),
                    'MESS_RELATIVE_QUANTITY_FEW' => (isset($arParams['~MESS_RELATIVE_QUANTITY_FEW']) ? $arParams['~MESS_RELATIVE_QUANTITY_FEW'] : ''),
                    'MESS_BTN_BUY' => (isset($arParams['~MESS_BTN_BUY']) ? $arParams['~MESS_BTN_BUY'] : ''),
                    'MESS_BTN_ADD_TO_BASKET' => (isset($arParams['~MESS_BTN_ADD_TO_BASKET']) ? $arParams['~MESS_BTN_ADD_TO_BASKET'] : ''),
                    'MESS_BTN_SUBSCRIBE' => (isset($arParams['~MESS_BTN_SUBSCRIBE']) ? $arParams['~MESS_BTN_SUBSCRIBE'] : ''),
                    'MESS_BTN_DETAIL' => (isset($arParams['~MESS_BTN_DETAIL']) ? $arParams['~MESS_BTN_DETAIL'] : ''),
                    'MESS_NOT_AVAILABLE' => (isset($arParams['~MESS_NOT_AVAILABLE']) ? $arParams['~MESS_NOT_AVAILABLE'] : ''),
                    'MESS_BTN_COMPARE' => (isset($arParams['~MESS_BTN_COMPARE']) ? $arParams['~MESS_BTN_COMPARE'] : ''),
                    'MESS_PRICE_RANGES_TITLE' => (isset($arParams['~MESS_PRICE_RANGES_TITLE']) ? $arParams['~MESS_PRICE_RANGES_TITLE'] : ''),
                    'MESS_DESCRIPTION_TAB' => (isset($arParams['~MESS_DESCRIPTION_TAB']) ? $arParams['~MESS_DESCRIPTION_TAB'] : ''),
                    'MESS_PROPERTIES_TAB' => (isset($arParams['~MESS_PROPERTIES_TAB']) ? $arParams['~MESS_PROPERTIES_TAB'] : ''),
                    'MESS_COMMENTS_TAB' => (isset($arParams['~MESS_COMMENTS_TAB']) ? $arParams['~MESS_COMMENTS_TAB'] : ''),
                    'MAIN_BLOCK_PROPERTY_CODE' => (isset($arParams['DETAIL_MAIN_BLOCK_PROPERTY_CODE']) ? $arParams['DETAIL_MAIN_BLOCK_PROPERTY_CODE'] : ''),
                    'MAIN_BLOCK_OFFERS_PROPERTY_CODE' => (isset($arParams['DETAIL_MAIN_BLOCK_OFFERS_PROPERTY_CODE']) ? $arParams['DETAIL_MAIN_BLOCK_OFFERS_PROPERTY_CODE'] : ''),
                    'USE_VOTE_RATING' => $arParams['DETAIL_USE_VOTE_RATING'],
                    'VOTE_DISPLAY_AS_RATING' => (isset($arParams['DETAIL_VOTE_DISPLAY_AS_RATING']) ? $arParams['DETAIL_VOTE_DISPLAY_AS_RATING'] : ''),
                    'USE_COMMENTS' => $arParams['DETAIL_USE_COMMENTS'],
                    'BLOG_USE' => (isset($arParams['DETAIL_BLOG_USE']) ? $arParams['DETAIL_BLOG_USE'] : ''),
                    'BLOG_URL' => (isset($arParams['DETAIL_BLOG_URL']) ? $arParams['DETAIL_BLOG_URL'] : ''),
                    'BLOG_EMAIL_NOTIFY' => (isset($arParams['DETAIL_BLOG_EMAIL_NOTIFY']) ? $arParams['DETAIL_BLOG_EMAIL_NOTIFY'] : ''),
                    'VK_USE' => (isset($arParams['DETAIL_VK_USE']) ? $arParams['DETAIL_VK_USE'] : ''),
                    'VK_API_ID' => (isset($arParams['DETAIL_VK_API_ID']) ? $arParams['DETAIL_VK_API_ID'] : 'API_ID'),
                    'FB_USE' => (isset($arParams['DETAIL_FB_USE']) ? $arParams['DETAIL_FB_USE'] : ''),
                    'FB_APP_ID' => (isset($arParams['DETAIL_FB_APP_ID']) ? $arParams['DETAIL_FB_APP_ID'] : ''),
                    'BRAND_USE' => (isset($arParams['DETAIL_BRAND_USE']) ? $arParams['DETAIL_BRAND_USE'] : 'N'),
                    'BRAND_PROP_CODE' => (isset($arParams['DETAIL_BRAND_PROP_CODE']) ? $arParams['DETAIL_BRAND_PROP_CODE'] : ''),
                    'DISPLAY_NAME' => (isset($arParams['DETAIL_DISPLAY_NAME']) ? $arParams['DETAIL_DISPLAY_NAME'] : ''),
                    'IMAGE_RESOLUTION' => (isset($arParams['DETAIL_IMAGE_RESOLUTION']) ? $arParams['DETAIL_IMAGE_RESOLUTION'] : ''),
                    'PRODUCT_INFO_BLOCK_ORDER' => (isset($arParams['DETAIL_PRODUCT_INFO_BLOCK_ORDER']) ? $arParams['DETAIL_PRODUCT_INFO_BLOCK_ORDER'] : ''),
                    'PRODUCT_PAY_BLOCK_ORDER' => (isset($arParams['DETAIL_PRODUCT_PAY_BLOCK_ORDER']) ? $arParams['DETAIL_PRODUCT_PAY_BLOCK_ORDER'] : ''),
                    'ADD_DETAIL_TO_SLIDER' => (isset($arParams['DETAIL_ADD_DETAIL_TO_SLIDER']) ? $arParams['DETAIL_ADD_DETAIL_TO_SLIDER'] : ''),
                    'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
                    'ADD_SECTIONS_CHAIN' => (isset($arParams['ADD_SECTIONS_CHAIN']) ? $arParams['ADD_SECTIONS_CHAIN'] : ''),
                    'ADD_ELEMENT_CHAIN' => (isset($arParams['ADD_ELEMENT_CHAIN']) ? $arParams['ADD_ELEMENT_CHAIN'] : ''),
                    'DISPLAY_PREVIEW_TEXT_MODE' => (isset($arParams['DETAIL_DISPLAY_PREVIEW_TEXT_MODE']) ? $arParams['DETAIL_DISPLAY_PREVIEW_TEXT_MODE'] : ''),
                    'DETAIL_PICTURE_MODE' => (isset($arParams['DETAIL_DETAIL_PICTURE_MODE']) ? $arParams['DETAIL_DETAIL_PICTURE_MODE'] : array()),
                    'ADD_TO_BASKET_ACTION' => $basketAction,
                    'ADD_TO_BASKET_ACTION_PRIMARY' => (isset($arParams['DETAIL_ADD_TO_BASKET_ACTION_PRIMARY']) ? $arParams['DETAIL_ADD_TO_BASKET_ACTION_PRIMARY'] : null),
                    'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
                    'DISPLAY_COMPARE' => (isset($arParams['USE_COMPARE']) ? $arParams['USE_COMPARE'] : ''),
                    'COMPARE_PATH' => $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['compare'],
                    'BACKGROUND_IMAGE' => (isset($arParams['DETAIL_BACKGROUND_IMAGE']) ? $arParams['DETAIL_BACKGROUND_IMAGE'] : ''),
                    'COMPATIBLE_MODE' => (isset($arParams['COMPATIBLE_MODE']) ? $arParams['COMPATIBLE_MODE'] : ''),
                    'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : ''),
                    'SET_VIEWED_IN_COMPONENT' => (isset($arParams['DETAIL_SET_VIEWED_IN_COMPONENT']) ? $arParams['DETAIL_SET_VIEWED_IN_COMPONENT'] : ''),
                    'SHOW_SLIDER' => (isset($arParams['DETAIL_SHOW_SLIDER']) ? $arParams['DETAIL_SHOW_SLIDER'] : ''),
                    'SLIDER_INTERVAL' => (isset($arParams['DETAIL_SLIDER_INTERVAL']) ? $arParams['DETAIL_SLIDER_INTERVAL'] : ''),
                    'SLIDER_PROGRESS' => (isset($arParams['DETAIL_SLIDER_PROGRESS']) ? $arParams['DETAIL_SLIDER_PROGRESS'] : ''),
                    'USE_ENHANCED_ECOMMERCE' => (isset($arParams['USE_ENHANCED_ECOMMERCE']) ? $arParams['USE_ENHANCED_ECOMMERCE'] : ''),
                    'DATA_LAYER_NAME' => (isset($arParams['DATA_LAYER_NAME']) ? $arParams['DATA_LAYER_NAME'] : ''),
                    'BRAND_PROPERTY' => (isset($arParams['BRAND_PROPERTY']) ? $arParams['BRAND_PROPERTY'] : ''),
                    'USE_GIFTS_DETAIL' => $arParams['USE_GIFTS_DETAIL'] ?: 'Y',
                    'USE_GIFTS_MAIN_PR_SECTION_LIST' => $arParams['USE_GIFTS_MAIN_PR_SECTION_LIST'] ?: 'Y',
                    'GIFTS_SHOW_DISCOUNT_PERCENT' => $arParams['GIFTS_SHOW_DISCOUNT_PERCENT'],
                    'GIFTS_SHOW_OLD_PRICE' => $arParams['GIFTS_SHOW_OLD_PRICE'],
                    'GIFTS_DETAIL_PAGE_ELEMENT_COUNT' => $arParams['GIFTS_DETAIL_PAGE_ELEMENT_COUNT'],
                    'GIFTS_DETAIL_HIDE_BLOCK_TITLE' => $arParams['GIFTS_DETAIL_HIDE_BLOCK_TITLE'],
                    'GIFTS_DETAIL_TEXT_LABEL_GIFT' => $arParams['GIFTS_DETAIL_TEXT_LABEL_GIFT'],
                    'GIFTS_DETAIL_BLOCK_TITLE' => $arParams['GIFTS_DETAIL_BLOCK_TITLE'],
                    'GIFTS_SHOW_NAME' => $arParams['GIFTS_SHOW_NAME'],
                    'GIFTS_SHOW_IMAGE' => $arParams['GIFTS_SHOW_IMAGE'],
                    'GIFTS_MESS_BTN_BUY' => $arParams['~GIFTS_MESS_BTN_BUY'],
                    'GIFTS_PRODUCT_BLOCKS_ORDER' => $arParams['LIST_PRODUCT_BLOCKS_ORDER'],
                    'GIFTS_SHOW_SLIDER' => $arParams['LIST_SHOW_SLIDER'],
                    'GIFTS_SLIDER_INTERVAL' => isset($arParams['LIST_SLIDER_INTERVAL']) ? $arParams['LIST_SLIDER_INTERVAL'] : '',
                    'GIFTS_SLIDER_PROGRESS' => isset($arParams['LIST_SLIDER_PROGRESS']) ? $arParams['LIST_SLIDER_PROGRESS'] : '',
                    'GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT' => $arParams['GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT'],
                    'GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE' => $arParams['GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE'],
                    'GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE' => $arParams['GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE'],
                );
                if (isset($arParams['USER_CONSENT'])) {
                    $componentElementParams['USER_CONSENT'] = $arParams['USER_CONSENT'];
                }
                if (isset($arParams['USER_CONSENT_ID'])) {
                    $componentElementParams['USER_CONSENT_ID'] = $arParams['USER_CONSENT_ID'];
                }
                if (isset($arParams['USER_CONSENT_IS_CHECKED'])) {
                    $componentElementParams['USER_CONSENT_IS_CHECKED'] = $arParams['USER_CONSENT_IS_CHECKED'];
                }
                if (isset($arParams['USER_CONSENT_IS_LOADED'])) {
                    $componentElementParams['USER_CONSENT_IS_LOADED'] = $arParams['USER_CONSENT_IS_LOADED'];
                }

                $template = '';
                if( isset($_GET['_escaped_fragment_']) ){
                    $template = 'searchBot';
                }

                $elementId = $APPLICATION->IncludeComponent(
                    'bitrix:catalog.element', $template, $componentElementParams, $component
                ); ?>

              <script type="text/javascript">
              $(document).ready(function () {

                // Карточка товара
                $prouctCarousel = $('.product_info .images .big .slider').owlCarousel({
                  loop: false,
                  nav: false,
                  navSpeed: 500,
                  smartSpeed: 500,
                  autoplaySpeed: 500,
                  items: 1,
                  margin: 30,
                  onTranslate: function (event) {
                    $('.product_info .images .thumbs a').removeClass('active')
                    $('.product_info .images .thumbs a:eq(' + event.item.index + ')').addClass('active')
                  },
                  responsive: {
                    768: {
                      dots: false,
                      autoHeight: false
                    },
                    0: {
                      dots: true,
                      autoHeight: true
                    }
                  }
                })

                // Смена цвета в карточке товара
                $(document).on('click', '.color___label', function () {

                  var attr_for = $(this).attr('for');
                  if (!$('input[id=' + attr_for + ']').prop('checked')) {
                    $.noty.closeAll();
                    var photo = $(this).attr('photo');
                    var photo_big = $(this).attr('photo_big');
                    var color_id = $(this).attr('color_id');
                    var min_price = $(this).attr('min_price');
                    var equalPrices = $(this).attr('equal_prices');
                    $('.colorSizesBlock').hide();
                    $('.colorSizesBlock[color_id=' + color_id + ']').show();
                    $('input[name=size]').prop('checked', false);
                    $('.price___block div.new').html(((equalPrices == 0) ? 'от ' : '') + number_format(min_price, 0, ",", " ") + ' <span class="currency">:</span>');
                    $('.old___price_block').hide();
                    $('.discount___block').hide();
                    if (photo.length > 0) {
                      $prouctCarousel.trigger('replace.owl.carousel', '<div class="slider owl-carousel"><div class="slide"><a href="' + photo_big + '" class="fancy_img" data-fancybox="gallery"><img src="' + photo + '"></a></div></div>');
                      $('.fancy_img').fancybox({
                        transitionEffect: 'slide',
                        animationEffect: 'fade',
                        i18n: {
                          'en': {
                            CLOSE: 'Закрыть'
                          }
                        }
                      })
                    }
                  }
                })

              });
              </script>


              <div class="data right product_card__right_block">


                  <?
                  // tovar_author_block
                  //echo '<pre>';
                  //print_r($arResult);
                  //print_r($arParams);
                  //print_r($el);
                  //echo '</pre>';
                  // exit;
                  //e/cho '<pre>';
                  $elll = $APPLICATION->IncludeComponent("my:tovar_author_block", "", array('el' => $el));
                  //print_r($el);

                  //echo '</pre>';
                //exit;
                ?>
                <!--<div class="also">
                            <div class="title">Также с этим товаром</div>

                            <div class="grid">
                                <div class="item">
                                    <img src="<?=SITE_TEMPLATE_PATH?>/images/ic_prod_also1.png" alt="">
                                </div>

                                <div class="item">
                                    <img src="<?=SITE_TEMPLATE_PATH?>/images/ic_prod_also1.png" alt="">
                                </div>

                                <div class="item">
                                    <img src="<?=SITE_TEMPLATE_PATH?>/images/ic_prod_also1.png" alt="">
                                </div>

                                <div class="item">
                                    <img src="<?=SITE_TEMPLATE_PATH?>/images/ic_prod_also1.png" alt="">
                                </div>
                            </div>

                            <div class="grid">
                                <div class="item big">
                                    <img src="<?=SITE_TEMPLATE_PATH?>/images/ic_prod_also2.png" alt="">
                                </div>

                                <div class="item big">
                                    <img src="<?=SITE_TEMPLATE_PATH?>/images/ic_prod_also2.png" alt="">
                                </div>

                                <div class="item big">
                                    <img src="<?=SITE_TEMPLATE_PATH?>/images/ic_prod_also2.png" alt="">
                                </div>

                                <div class="item big">
                                    <img src="<?=SITE_TEMPLATE_PATH?>/images/ic_prod_also2.png" alt="">
                                </div>

                                <div class="item big">
                                    <img src="<?=SITE_TEMPLATE_PATH?>/images/ic_prod_also2.png" alt="">
                                </div>

                                <div class="item big">
                                    <img src="<?=SITE_TEMPLATE_PATH?>/images/ic_prod_also2.png" alt="">
                                </div>
                            </div>

                            <a href="#" class="hide_link"></a>
                        </div>-->

                <div class="product_card__also_with_product">Также с этим принтом</div>

                <div class="related_cats related_right_block" style="margin:0"></div>


                  <? if (strlen($el['DETAIL_TEXT']) > 0) { ?>

                    <!--					<div class="accordion">-->
                    <!--						<div class="item">-->
                    <!--							<div class="name active">описание</div>-->
                    <!--							<div class="val" style="display:block">--><? //=$el['DETAIL_TEXT']?><!--</div>-->
                    <!--						</div>-->
                    <!--					</div>-->

                  <? } ?>


                <div class="share share42init" data-title="<?= $el['NAME'] ?>" data-description="<?= $el['DETAIL_TEXT'] ?>"
                     data-image="http://<?= $_SERVER['HTTP_HOST'] ?><?= \CFile::GetPath($el['DETAIL_PICTURE']) ?>"
                     data-url="http://<?= $_SERVER['HTTP_HOST'] ?><?= $el['DETAIL_PAGE_URL'] ?>"></div>
                <script type="text/javascript" src="/share42/share42.js"></script>


                <a href="#comments" class="comments_link scroll_link">Читать комментарии
                  (<?= ((count($comments) > project\comment::CNT) ? (project\comment::CNT . '+') : count($comments)) ?>)</a>

              </div>
              <div class="clear"></div>

            </div>
        </section>

        <? // Также с этим принтом
        $APPLICATION->IncludeComponent(
            "my:related_goods", "",
            array('product_id' => $el['ID'])
        ); ?>

        <? // Другие иллюстрации автора
        if (intval($el['PROPERTY_PRINT_ID_VALUE']) > 0) {
            $ids = project\author_goods::list($el['ID'], true);
            if (count($ids) > 0) {
                $GLOBALS['author_goods_Filter']['ID'] = $ids;
                $GLOBALS['author_goods_Filter']['!ID'] = $el['ID'];
                $APPLICATION->IncludeComponent(
                    "bitrix:catalog.section", "author_goods",
                    Array(
                        "IBLOCK_TYPE" => "content",
                        "IBLOCK_ID" => $el['IBLOCK_ID'],
                        "SECTION_USER_FIELDS" => array(),
                        "ELEMENT_SORT_FIELD" => "NAME",
                        "ELEMENT_SORT_ORDER" => "ASC",
                        "ELEMENT_SORT_FIELD2" => "NAME",
                        "ELEMENT_SORT_ORDER2" => "ASC",
                        "FILTER_NAME" => "author_goods_Filter",
                        "HIDE_NOT_AVAILABLE" => "N",
                        "PAGE_ELEMENT_COUNT" => "6",
                        "LINE_ELEMENT_COUNT" => "3",
                        "PROPERTY_CODE" => array('PRINT_ID'),
                        "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
                        "OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
                        "OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
                        "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
                        "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
                        "OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
                        "OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
                        "OFFERS_LIMIT" => 1 /*$arParams["LIST_OFFERS_LIMIT"]*/,
                        "TEMPLATE_THEME" => "",
                        "PRODUCT_SUBSCRIPTION" => "N",
                        "SHOW_DISCOUNT_PERCENT" => "N",
                        "SHOW_OLD_PRICE" => "N",
                        "MESS_BTN_BUY" => "Купить",
                        "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                        "MESS_BTN_SUBSCRIBE" => "Подписаться",
                        "MESS_BTN_DETAIL" => "Подробнее",
                        "MESS_NOT_AVAILABLE" => "Нет в наличии",
                        "SECTION_URL" => "",
                        "DETAIL_URL" => "",
                        "SECTION_ID_VARIABLE" => "SECTION_ID",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "CACHE_GROUPS" => "Y",
                        "SET_META_KEYWORDS" => "N",
                        "META_KEYWORDS" => "",
                        "SET_META_DESCRIPTION" => "N",
                        "META_DESCRIPTION" => "",
                        "BROWSER_TITLE" => "-",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "DISPLAY_COMPARE" => "N",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "CACHE_FILTER" => "Y",
                        "PRICE_CODE" => array('BASE'),
                        "USE_PRICE_COUNT" => "N",
                        "SHOW_PRICE_COUNT" => "1",
                        "PRICE_VAT_INCLUDE" => "Y",
                        "CONVERT_CURRENCY" => "N",
                        "BASKET_URL" => "/personal/basket.php",
                        "ACTION_VARIABLE" => "action",
                        "PRODUCT_ID_VARIABLE" => "id",
                        "USE_PRODUCT_QUANTITY" => "N",
                        "ADD_PROPERTIES_TO_BASKET" => "Y",
                        "PRODUCT_PROPS_VARIABLE" => "prop",
                        "PARTIAL_PRODUCT_PROPERTIES" => "N",
                        "PRODUCT_PROPERTIES" => "",
                        "PAGER_TEMPLATE" => "",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "PAGER_TITLE" => "Товары",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "Y",
                        "ADD_PICT_PROP" => "-",
                        "LABEL_PROP" => "-",
                        'INCLUDE_SUBSECTIONS' => "Y",
                        'SHOW_ALL_WO_SECTION' => "Y"
                    )
                );
            }
        }

        // Просмотренные товары
        if (count($_SESSION['viewed_products']) > 0) {
            $GLOBALS["viewed_products"]['ID'] = $_SESSION['viewed_products'];
            $GLOBALS["viewed_products"]['!ID'] = $el['ID'];
            $APPLICATION->IncludeComponent(
                "bitrix:catalog.section",
                "viewed_products",
                Array(
                    "IBLOCK_TYPE" => "content",
                    "IBLOCK_ID" => $el['IBLOCK_ID'],
                    "SECTION_USER_FIELDS" => array(),
                    "ELEMENT_SORT_FIELD" => "NAME",
                    "ELEMENT_SORT_ORDER" => "ASC",
                    "ELEMENT_SORT_FIELD2" => "NAME",
                    "ELEMENT_SORT_ORDER2" => "ASC",
                    "FILTER_NAME" => "viewed_products",
                    "HIDE_NOT_AVAILABLE" => "N",
                    "PAGE_ELEMENT_COUNT" => "6",
                    "LINE_ELEMENT_COUNT" => "3",
                    "PROPERTY_CODE" => array('PRINT_ID'),
                    "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
                    "OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
                    "OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
                    "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
                    "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
                    "OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
                    "OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
                    "OFFERS_LIMIT" => 1 /*$arParams["LIST_OFFERS_LIMIT"]*/,
                    "TEMPLATE_THEME" => "",
                    "PRODUCT_SUBSCRIPTION" => "N",
                    "SHOW_DISCOUNT_PERCENT" => "N",
                    "SHOW_OLD_PRICE" => "N",
                    "MESS_BTN_BUY" => "Купить",
                    "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                    "MESS_BTN_SUBSCRIBE" => "Подписаться",
                    "MESS_BTN_DETAIL" => "Подробнее",
                    "MESS_NOT_AVAILABLE" => "Нет в наличии",
                    "SECTION_URL" => "",
                    "DETAIL_URL" => "",
                    "SECTION_ID_VARIABLE" => "SECTION_ID",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_GROUPS" => "Y",
                    "SET_META_KEYWORDS" => "N",
                    "META_KEYWORDS" => "",
                    "SET_META_DESCRIPTION" => "N",
                    "META_DESCRIPTION" => "",
                    "BROWSER_TITLE" => "-",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "DISPLAY_COMPARE" => "N",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "CACHE_FILTER" => "Y",
                    "PRICE_CODE" => array('BASE'),
                    "USE_PRICE_COUNT" => "N",
                    "SHOW_PRICE_COUNT" => "1",
                    "PRICE_VAT_INCLUDE" => "Y",
                    "CONVERT_CURRENCY" => "N",
                    "BASKET_URL" => "/personal/basket.php",
                    "ACTION_VARIABLE" => "action",
                    "PRODUCT_ID_VARIABLE" => "id",
                    "USE_PRODUCT_QUANTITY" => "N",
                    "ADD_PROPERTIES_TO_BASKET" => "Y",
                    "PRODUCT_PROPS_VARIABLE" => "prop",
                    "PARTIAL_PRODUCT_PROPERTIES" => "N",
                    "PRODUCT_PROPERTIES" => "",
                    "PAGER_TEMPLATE" => "",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "Товары",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "Y",
                    "ADD_PICT_PROP" => "-",
                    "LABEL_PROP" => "-",
                    'INCLUDE_SUBSECTIONS' => "Y",
                    'SHOW_ALL_WO_SECTION' => "Y"
                )
            );
        }

        // comments
        $APPLICATION->IncludeComponent(
            "my:comments", "",
            array(
                'tov_id' => $el['ID'],
                'comments' => $comments
            )
        );

    }

} else {

    // 404
    include_once($_SERVER['DOCUMENT_ROOT'] . '/include/areas/404.php');

}
