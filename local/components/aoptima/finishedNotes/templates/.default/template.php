<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools; ?>


<? if( count($arResult['finishedNotes']) > 0 ){ ?>

    <table class="constructor_approval_table">
        <tr>
            <th>ID</th>
            <th>Название</th>
            <th>Иллюстратор</th>
            <th>Дата завершения</th>
        </tr>

        <? while ($arItem = $arResult['r_elements']->Fetch()){ ?>

            <tr>
                <td><?=$arItem['ID']?></td>
                <td><?=$arItem['NAME']?></td>
                <td><?=$arItem['USER']['NAME']?><? if( strlen($arItem['USER']['LAST_NAME']) > 0 ){ echo ' '.$arItem['USER']['LAST_NAME']; } ?> [<?=$arItem['USER']['ID']?>]</td>
                <td><?=$arItem['PROPERTY_FINISH_DATE_VALUE']?></td>
            </tr>

        <? } ?>

    </table>

    <? echo $arResult['NAV_STRING']; ?>


<? } else { ?>

    <p class="no___count">Нет новых заявок на согласование</p>

<? } ?>