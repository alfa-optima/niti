<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$APPLICATION->AddChainItem('Личный кабинет', '/personal/');
$APPLICATION->AddChainItem('Завершенные заявки', tools\funcs::pureURL());


$arResult['finishedNotes'] = project\constructor::getFinishedNotes();

$arResult['r_elements'] = new \CDBResult;
$arResult['r_elements']->InitFromArray( $arResult['finishedNotes'] );
$arResult['r_elements']->NavStart( 15 );
$arResult['NAV_STRING'] = $arResult['r_elements']->GetPageNavStringEx(
    $navComponentObject, false, false, false, false
);



$this->IncludeComponentTemplate();