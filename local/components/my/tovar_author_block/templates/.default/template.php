<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if( $arResult ){ ?>
	
	<div class="author">
	
		<div class="avatar left">
			<a href="/illustrators/<?=$arResult['ID']?>/">
				<? if( intval($arResult['PERSONAL_PHOTO']) > 0 ){ ?>
					<img src="<?=rIMGG($arResult['PERSONAL_PHOTO'], 5, 45, 45)?>">
				<? } else { ?>
					<img src="<?=SITE_TEMPLATE_PATH?>/images/user_no_photo.jpg">
				<? } ?>
			</a>
		</div>

		<div class="info right">
			<div class="title">Автор</div>
			<div class="name">
				<a href="/illustrators/<?=$arResult['ID']?>/"><?=$arResult['NAME']?> <?=$arResult['LAST_NAME']?></a>
			</div>
		</div>
		<div class="clear"></div>

		<a style="cursor:pointer" class="likes user_like___button to___process" user_id="<?=$arResult['ID']?>"><span><?=$arResult['UF_LIKES']?></span> понравилось</a>
		
	</div>

<? } ?>