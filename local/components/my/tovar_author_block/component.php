<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$el = $arParams['el'];

$arResult = false;

if( intval($el['PROPERTY_PRINT_ID_VALUE']) > 0 ){
	
	// Инфо о принте
	$print = tools\el::info($el['PROPERTY_PRINT_ID_VALUE']);
	
	if(
		intval($print['ID']) > 0
		&&
		intval($print['PROPERTY_USER_ID_VALUE']) > 0
	){

		// Автор
		$arUser = tools\user::info( $print['PROPERTY_USER_ID_VALUE'] );
		if (
			$arUser
			&&
			$arUser['UF_USER_TYPE'] == 2
		){

			$arResult = $arUser;
		
			// Получим тип пользователя
			/*$user_type_name = false;
			$enums = \CUserFieldEnum::GetList(array(), array(
				"ID" => $arUser['UF_USER_TYPE']
			));
			if( $enum = $enums->GetNext() ){
				$arResult['USER_TYPE_NAME'] = $enum['VALUE'];
			}*/

		}
	}
}



$this->IncludeComponentTemplate();