<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<div class="cart_info cart_page">

	<!-- Основная часть -->
	<section class="content left basket___section nn_basket_section">
	
		<? if( $arResult['basket_cnt'] > 0 ){ ?>
		
		
			<?=$arResult['ORDER_STEPS']?>

			
			<table class="basket___table">
			
				<thead>
					<tr>
						<th colspan="2">Товар</th>
						<th>Цвет / Размер</th>
						<th>Количество</th>
						<th>Цена</th>
						<th>Удалить</th>
					</tr>
				</thead>

				<tbody>
				
					<? // basket_items
					$APPLICATION->IncludeComponent(
                        "my:basket_items", "",
                        array(
                            'arBasket' => $arResult['arBasket']
                        )
                    ); ?>

				</tbody>
				
			</table>

			<div class="promo_code left">
				<div class="title">Промокод:</div>

				<input type="text" name="promokod" value="<?=$arResult['coupon']?>" class="input <?=$arResult['coupon']?'success':''?>">

				<div class="discount">
					Скидка <div class="price discount___sum"><?=number_format($arResult['discount'], 0, ",", " ")?> <span class="currency">:</span></div>
				</div>
			</div>


			<div class="cart_total right">
				Итого: <div class="price total___sum"><?=number_format($arResult['basket_disc_sum'], 0, ",", " ")?> <span class="currency">:</span></div>
			</div>
			<div class="clear"></div>


			<div class="checkout_links">
			
				<a href="/" class="back_link left">
					<span class="icon"></span>В магазин
				</a>

				<form action="/order_step_2/" method="post" class="order___form">
					<? foreach ( $_POST as $name => $value ){ ?>
						<input type="hidden" name="<?=$name?>" value="<?=$value?>">
					<? } ?>
				</form>
				
				<a style="cursor:pointer" class="next_link right back_to_step_2_button">
					Продолжить<span class="icon"></span>
				</a>
				
			</div>
			
		<? } else { ?>
		
			<p class="no___count">Ваша корзина пуста</p>
		
		<? } ?>
		
	</section>
	<!-- End Основная часть -->
	<div class="clear"></div>

</div>