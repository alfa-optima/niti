<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

// Инфо по корзине
$cupon = project\my_coupon::get_active_coupon();
$basketInfo = basketInfo( $cupon );

$arResult['basket_cnt'] = $basketInfo['basket_cnt'];
$arResult['arBasket'] = $basketInfo['arBasket'];
$arResult['basket_disc_sum'] = $basketInfo['basket_disc_sum']; 
$arResult['discount'] = $basketInfo['discount']; 

$arResult['coupon'] = project\my_coupon::get_active_coupon();


ob_start(); 
	$APPLICATION->IncludeComponent(
		"my:order_steps", "",
		array('step' => 1)
	);
	$arResult['ORDER_STEPS'] = ob_get_contents();
ob_end_clean();


$this->IncludeComponentTemplate();

