<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');

use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;


$arResult['TABS'] = project\home_page_hits::getCategoriesWithProducts(
    [ 'man', 'woman', 'kids', 'all' ]
);





$this->IncludeComponentTemplate();
