<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;


$arResult['TYPES'] = project\gender_type::list();


$activeGenderType = project\gender_type::getActive();
$arResult['activeGenderType'] = $activeGenderType;


foreach( $arResult['TYPES'] as $key => $type ){
	$arResult['TYPES'][$key]['ACTIVE'] = ($activeGenderType['CODE'] == $type['CODE'])?'Y':'N';
	if( $type['CODE'] == 'all' ){
		unset($arResult['TYPES'][$key]);
	}
}









$this->IncludeComponentTemplate();