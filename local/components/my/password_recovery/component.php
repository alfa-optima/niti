<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


if ( $USER->IsAuthorized() ) {  LocalRedirect('/personal/');  }

$arResult['code'] = param_get("code"); 

// Ищем пользователя с таким кодом
$filter = Array( "=UF_RECOVERY_CODE" => $arResult['code'] );
$rsUsers = CUser::GetList(($by="id"), ($order="desc"), $filter, 
	array(
		'FIELDS' => array( 'ID' ),
		'SELECT' => array('UF_*')
	)
);
while ($arUser = $rsUsers->GetNext()){
	$arResult['user_id'] = $arUser['ID'];
}





$this->IncludeComponentTemplate();