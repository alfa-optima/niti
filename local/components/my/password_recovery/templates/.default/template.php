<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if ( 
	intval($arResult['user_id']) > 0
	&&
	strlen($arResult['code']) > 0
){ ?>

	<div class="lk_personal">

		<aside class="right"></aside>

		<!-- Основная часть -->
		<section class="content left">
		
			<form class="form">
			
				<input type="hidden" name="code" value="<?=$arResult['code']?>">
			
				<div class="lk_title">Установка нового пароля:</div>

				<div class="lines">
				
					<div class="line">
						<div class="name">Новый пароль</div>

						<div class="field">
							<input type="password" name="PASSWORD" placeholder="Новый пароль" class="input">
						</div>
					</div>


					<div class="line">
						<div class="name">Пароль</div>

						<div class="field">
							<input type="password" name="CONFIRM_PASSWORD" placeholder="Подтверждение пароля" class="input">
						</div>
					</div>

				</div>
				
				<p class="error___p"></p>

				<div class="submit">
					<button type="button" class="submit_btn new_password_button to___process">Сменить пароль</button>
				</div>
				
			</form>

		</section>
		<div class="clear"></div>

	</div>

<? } else { ?>

	<p style="color: red">Ошибочная ссылка</p>
	
<? } ?>