<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$arResult['arItem'] = $arParams['arItem'];
$arResult['component'] = $arParams['component'];
$arResult['is_4'] = $arParams['is_4'];
$arResult['is_fovorites'] = $arParams['is_fovorites'];

$arResult['DEFAULT_OFFER'] = false;
if( intval($arParams['FILTER_COLOR_ID']) > 0 ){
    foreach ( $arResult['arItem']['OFFERS'] as $offer ){
        if( $offer["PROPERTIES"]['COLOR']['VALUE'] == $arParams['FILTER_COLOR_ID'] ){
            $arResult['DEFAULT_OFFER'] = $offer;
        }
    }
}
if( !$arResult['DEFAULT_OFFER'] ){
    foreach ( $arResult['arItem']['OFFERS'] as $offer ){
        if( $offer["PROPERTIES"]['MAIN_TP']['VALUE'] ){
            $arResult['DEFAULT_OFFER'] = $offer;
        }
    }
}
if( !$arResult['DEFAULT_OFFER'] ){   $arResult['DEFAULT_OFFER'] = $arResult['arItem']['OFFERS'][0];   }

$arResult['default_photo'] = false;
if( intval($arResult['DEFAULT_OFFER']['PROPERTIES']['PHOTO_ID']['VALUE']) > 0 ){
    $arResult['default_photo'] = $arResult['DEFAULT_OFFER']['PROPERTIES']['PHOTO_ID']['VALUE'];
} else if( intval($arResult['arItem']['PROPERTIES']['PHOTO_ID']['VALUE']) > 0 ){
    $arResult['default_photo'] = $arResult['arItem']['PROPERTIES']['PHOTO_ID']['VALUE'];
}


$arResult['color_images'] = array();
foreach ( $arResult['arItem']['OFFERS'] as $key => $offer ){
	$color_id = $offer['PROPERTIES']['COLOR']['VALUE'];
	if( !$arResult['color_images'][$color_id] ){
		$arResult['color_images'][$color_id] = \CFile::GetPath($offer['PROPERTIES']['PHOTO_ID']['VALUE']);
	}
}

$arResult['arItem']['PRODUCT_NAME'] = htmlspecialchars_decode($arResult['arItem']['~NAME']);
if( $arResult['arItem']['DISPLAY_PROPERTIES']['PRINT_ID']['LINK_ELEMENT_VALUE'] ){
	$keys = array_keys($arResult['arItem']['DISPLAY_PROPERTIES']['PRINT_ID']['LINK_ELEMENT_VALUE']);
	if( count($keys) > 0 ){
		$arResult['arItem']['PRODUCT_NAME'] = htmlspecialchars_decode($arResult['arItem']['DISPLAY_PROPERTIES']['PRINT_ID']['LINK_ELEMENT_VALUE'][$keys[0]]['~NAME']);
	}
}

$arResult['arItem']['SECTION_NOMIN_TITLE'] = false;
if(
    isset( $arParams['els_sections'][ $arResult['arItem']['IBLOCK_SECTION_ID'] ]['UF_NOMIN_TITLE'] )
    &&
    strlen( $arParams['els_sections'][ $arResult['arItem']['IBLOCK_SECTION_ID'] ]['UF_NOMIN_TITLE'] ) > 0
){
    $arResult['arItem']['SECTION_NOMIN_TITLE'] = $arParams['els_sections'][ $arResult['arItem']['IBLOCK_SECTION_ID'] ]['UF_NOMIN_TITLE'];
}


$this->IncludeComponentTemplate();
