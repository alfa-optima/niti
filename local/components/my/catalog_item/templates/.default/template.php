<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
\Bitrix\Main\Loader::includeModule('aoptima.tools');

use AOptima\Tools as tools;

$arItem = $arResult['arItem']; ?>

<div class="product product___block" id="<?= $arResult['component']->GetEditAreaId($arItem['ID']); ?>"
		 tov_id="<?= $arItem['ID'] ?>">

    <? if ($arItem['PROPERTIES']['NEW']['VALUE'] == 'Да') { ?>
			<div class="sticker new">NEW</div>
    <? } ?>

    <? if (!$arResult['is_fovorites']) { ?>
			<a style="cursor:pointer" class="favorite_link fav___link to___process" tov_id="<?= $arItem['ID'] ?>">

			</a>
    <? } else { ?>
			<a style="cursor:pointer" class="favorite_del favorite___remove to___process" item_id="<?= $arItem['ID'] ?>">

			</a>
    <? } ?>

	<div class="thumb">
		<a class="detail_page_url nn_preloader" href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
			<img class="default_photo"
					 src="http://new.niti-niti.ru/imgen/a:650,b:739,f:100,k:f8f8f8,c:<?php echo $arResult['default_photo'] ?>.jpg">
        <? /*<img class="default_photo" src="<?=tools\funcs::rIMGG($arResult['default_photo'], 4, $arResult['is_4']?195:270, 280)?>">*/ ?>
		</a>
	</div>

	<a href="javascript:;" class="quike_view_link" data-fancybox data-src="/ajax/product.php?id=<?= $arItem['ID'] ?>"
		 data-type="ajax">быстрый просмотр</a>

	<div class="name">

      <?php
      if ($arResult['arItem']['SECTION_NOMIN_TITLE']) { ?>
				<a class="detail_page_url"
					 href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arResult['arItem']['SECTION_NOMIN_TITLE'] ?></a>
          <?
      } else {
          ?>
				<a class="detail_page_url"
					 href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= htmlspecialchars_decode($arItem['PRODUCT_NAME']) ?></a>
          <?php
      }
      ?>

	</div>

	<div class="nn_price_block">

      <? if ($arResult['DEFAULT_OFFER']) { ?>

				<div class="price"><?= number_format($arResult['DEFAULT_OFFER']['PRICES']['BASE']['DISCOUNT_VALUE_VAT'], 0, ",", " ") ?>
					<span class="currency">:</span></div>

          <? if (
              $arResult['DEFAULT_OFFER']['PRICES']['BASE']['DISCOUNT_VALUE_VAT']
              <
              $arResult['DEFAULT_OFFER']['PRICES']['BASE']['VALUE_VAT']
          ) { ?>
					<div class="price old___price"><?= number_format($arResult['DEFAULT_OFFER']['PRICES']['BASE']['VALUE_VAT'], 0, ",", " ") ?>
						<span class="currency">:</span></div>
          <? } ?>

      <? } ?>

		<div class="nn-quick_view">
			<button class="quick_view__btn" data-id="<?= $arItem['ID'] ?>">
				<span class="quick_view__icon"></span>
			</button>
		</div>

	</div>
</div>
