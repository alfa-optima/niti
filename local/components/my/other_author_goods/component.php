<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); 

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$el = $arParams['el'];

if( intval($el['PROPERTY_PRINT_ID_VALUE']) > 0 ){
	
	// инфо по принту
	$print = tools\el::info($el['PROPERTY_PRINT_ID_VALUE']);
	
	if( intval($print['ID']) > 0 && intval($print['PROPERTY_USER_ID_VALUE']) > 0 ){
		
		// Автор
		$arUser = tools\user::info( $print['PROPERTY_USER_ID_VALUE'] );
		if ( intval($arUser['ID']) > 0 ){
			
			$goods = array();
			$filter = Array(
				"IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
				"ACTIVE" => "Y",
				"!ID" => $el['ID'],
				"PROPERTY_USER_ID" => $arUser['ID'],
				//"PROPERTY_HAS_SKU" => 1
			);
			$dbElements = \CIBlockElement::GetList(Array("ID"=>"DESC"), $filter, false, Array("nTopCount"=>6), Array("ID", "PROPERTY_USER_ID", "PROPERTY_HAS_SKU"));
			while ($element = $dbElements->GetNext()){
				$goods[] = $element['ID'];
			}
			shuffle($goods);
			
			// если есть связ. товары
			if( count($goods) > 0 ){
				// переберём их
				foreach ( $goods as $key => $tov_id ){

					// Инфо о товаре
					$good = tools\el::info($tov_id);
					$goods[$key] = array(
						'ID' => $good['ID'],
						'NAME' => $good['NAME'],
						'~NAME' => $good['~NAME'],
						'DETAIL_PAGE_URL' => $good['DETAIL_PAGE_URL']
					);
					// 1 ТП товара
					$obCache = new \CPHPCache();
					$cache_time = 30*24*60*60;
					$cache_id = 'one_good_sku_'.$tov_id;
					$cache_path = '/one_good_sku/'.$tov_id.'/';
					if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
						$vars = $obCache->GetVars();   extract($vars);
					} elseif($obCache->StartDataCache()){
						$filter = Array(
							"IBLOCK_ID" => project\site::SKU_IBLOCK_ID,
							"ACTIVE" => "Y",
							"PROPERTY_CML2_LINK" => $tov_id
						);
						$fields = Array(
							"ID", "PROPERTY_CML2_LINK",
							"PROPERTY_PHTO", "CATALOG_GROUP_1"
						);
						$skus = \CIBlockElement::GetList(
							array("CATALOG_PRICE_1" => "ASC"), $filter, false,
							array("nTopCount"=>1), $fields
						);
						while ($sku = $skus->GetNext()){
							$one_sku = $sku;
						}
					$obCache->EndDataCache(array('one_sku' => $one_sku));
					}

					$goods[$key]['OFFERS'][0]['PRICES']['BASE']['DISCOUNT_VALUE_VAT'] = $one_sku['CATALOG_PRICE_1'];
					$goods[$key]['OFFERS'][0]['PROPERTIES']['PHTO']['VALUE'] = $one_sku['PROPERTY_PHTO_VALUE'];

				}
			}

		}
	}
}



$arResult['ITEMS'] = $goods;



$this->IncludeComponentTemplate();