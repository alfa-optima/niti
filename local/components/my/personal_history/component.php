<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); 
use \Bitrix\Main,    
    \Bitrix\Main\Localization\Loc as Loc,    
    \Bitrix\Main\Loader,    
    \Bitrix\Main\Config\Option,    
    \Bitrix\Sale\Delivery,    
    \Bitrix\Sale\PaySystem,    
    \Bitrix\Sale,    
    \Bitrix\Sale\Order,    
    \Bitrix\Sale\DiscountCouponsManager,    
    \Bitrix\Main\Context;
\Bitrix\Main\Loader::includeModule("main");   \Bitrix\Main\Loader::includeModule("iblock");
\Bitrix\Main\Loader::includeModule("catalog");   \Bitrix\Main\Loader::includeModule("sale");

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;


$APPLICATION->SetPageProperty("title", 'История заказов');
$APPLICATION->AddChainItem('История заказов', pureURL());



ob_start(); 
	$APPLICATION->IncludeComponent(
		"bitrix:menu", "personal_menu", 
		array(
			"ROOT_MENU_TYPE" => "personal_menu",
			"MENU_CACHE_TYPE" => "N",
			"MENU_CACHE_TIME" => "3600",
			"MENU_CACHE_USE_GROUPS" => "N",
			"CACHE_SELECTED_ITEMS" => "N",
			"MENU_CACHE_GET_VARS" => array(
			),
			"MAX_LEVEL" => "1",
			"CHILD_MENU_TYPE" => "left",
			"USE_EXT" => "N",
			"DELAY" => "N",
			"ALLOW_MULTI_SELECT" => "N",
			"COMPONENT_TEMPLATE" => "footer_menu",
		),
		false
	);
	$arResult['TOP_MENU_HTML'] = ob_get_contents();
ob_end_clean();



$arResult['ORDERS'] = array();
$arFilter = Array( "USER_ID" => $USER->GetID() );
$dbOrders = \CSaleOrder::GetList(array("DATE_INSERT" => "DESC"), $arFilter);
while ($arOrder = $dbOrders->Fetch()){

	$arOrder['status'] = 'В обработке';
	if( $arOrder['STATUS_ID'] == 'F' ){
		$arOrder['status'] = 'Выполнен';
	} else if ( $arOrder['ALLOW_DELIVERY'] == 'Y' ){
		$arOrder['status'] = 'В пути';
	}

	
	// Объект заказа
	$order = Sale\Order::load($arOrder['ID']);
	
	// Массив со скидками
	$arOrder['discountData'] = $order->getDiscount()->getApplyResult();

	// Сумма заказа
	$arOrder['order_sum'] = $order->getPrice();
	
	// Корзина заказа
	$basket = $order->getBasket();
	
	$arOrder['quantity'] = 0;
	$arOrder['arBasket'] = array();
	// перебираем корзину
	foreach($basket as $basketItem){

		$arOrder['quantity'] += $basketItem->getField('QUANTITY');
		
		$item_id = $basketItem->getField('ID');
	
		$el_id = $basketItem->getField('PRODUCT_ID'); 
		
		// Определяем товар или ТП
		$result = \CCatalogSku::GetProductInfo( $el_id );
		$is_SKU = intval($result['ID'])>0;
		// Если ТП
		if ( $is_SKU ){
			// ID товара
			$tov_id = $result['ID'];
		} else {
			// ID товара
			$tov_id = $el_id;
		}
		
		$basketItem->product = tools\el::info($tov_id);
		$basketItem->el = tools\el::info($el_id);

        if ( $is_SKU ){

            $size_id = $basketItem->el['PROPERTY_SIZE_VALUE'];
            $basketItem->size = tools\el::info($size_id);

            $color_id = $basketItem->el['PROPERTY_COLOR_VALUE'];
            $basketItem->color = tools\el::info($color_id);
        }

		
		$basketItem->q = $basketItem->getField('QUANTITY');
		
		// цены
		$price = $arOrder['discountData']['PRICES']['BASKET'][$item_id]['BASE_PRICE'];
		$disc_price = $arOrder['discountData']['PRICES']['BASKET'][$item_id]['PRICE'];
		
		$basketItem->itemSum = $price * $basketItem->q;
		$basketItem->discItemSum = $disc_price * $basketItem->q;
		
		$arOrder['arBasket'][] = $basketItem;
	}
	
	// способы доставки
	$deliveryIds = $order->getDeliverySystemId();
	$deliveryId = $deliveryIds[0];
	$arOrder['arDS'] = \CSaleDelivery::GetByID($deliveryId);
	
	// способы оплаты
	$paymentIds = $order->getPaymentSystemId();
	$paymentId = $paymentIds[0];
	$arOrder['arPS'] = \CSalePaySystem::GetByID($paymentId); 
	
	
	$arOrder['IS_PAID'] = $order->isPaid()?'Y':'N';
	
	if( 
		$arOrder['IS_PAID'] == 'N'
		&&
		in_array( $paymentId, array( 5 ) )
		&&
		$arOrder['STATUS_ID'] != 'F'
		&&
		!$order->isCanceled()
	){
		$arOrder['PAY_BUTTON'] = 'Y';
	} else {
		$arOrder['PAY_BUTTON'] = 'N';
	}

	$arResult['ORDERS'][] = $arOrder;
} 





$this->IncludeComponentTemplate();