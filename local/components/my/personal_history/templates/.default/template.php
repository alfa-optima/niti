<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

// Если авторизован
if( $USER->IsAuthorized() ){ ?>


	<?=$arResult['TOP_MENU_HTML']?>

	
	<div class="lk_orders">
	
		<div class="lk_title">Ваши заказы</div>
		
		<? if( count($arResult['ORDERS']) > 0 ){ ?>

			<div class="table-responsive">
				<table>
					<thead>
						<tr>
							<th colspan="2">Номер заказа</th>
							<th>Количество товаров</th>
							<th>Общая сумма</th>
							<th>Способ доставки</th>
							<th>Способ оплаты</th>
							<th>Бонусных балов</th>
							<th>Статус заказа</th>
						</tr>
					</thead>

					<tbody>
					
					<? foreach( $arResult['ORDERS'] as $arOrder ){ ?>
					
						<tr class="order_link" order_id="<?=$arOrder['ID']?>">
							<td class="icon"></td>
							<td data-column="Номер заказа">№<?=$arOrder['ID']?></td>
							<td data-column="Количество товаров"><?=$arOrder['quantity']?></td>
							<td data-column="Общая сумма"><?=number_format($arOrder['order_sum'], 0, ",", " ")?> <span class="currency">:</span></td>
							<td data-column="Способ доставки"><?=$arOrder['arDS']['NAME']?></td>
							<td data-column="Способ оплаты"><?=$arOrder['arPS']['NAME']?></td>
							<td data-column="Бонусных балов"><!--536 <span class="currency">:</span>--></td>
							<td data-column="Статус заказа"><?=$arOrder['status']?></td>
						</tr>
						
						<tr class="order_data">
							<td colspan="8">
							
								<? if( $arOrder['PAY_BUTTON'] == 'Y' ){ ?>
									<div class="pay___block lk_pay___block" order_id="<?=$arOrder['ID']?>"></div>
								<? } ?>
							
								<div class="cart_info">
									<table <? if( $arOrder['PAY_BUTTON'] == 'Y' ){ ?>style="border-top: 1px solid #e1e1e1;"<? } ?>>
									
										<thead>
											<tr>
												<th colspan="2">Товар</th>
												<th>Количество</th>
												<th>Сумма</th>
												<th>Бонусы</th>
											</tr>
										</thead>

										<tbody>
										
										<? foreach( $arOrder['arBasket'] as $basketItem ){ ?>
										
											<tr>
												<td class="image">

                                                    <? if( intval($basketItem->el['DETAIL_PICTURE']) > 0  ){ ?>
                                                        <a href="<?=$basketItem->product['DETAIL_PAGE_URL']?>">
                                                            <img src="<?=tools\funcs::rIMGG($basketItem->el['DETAIL_PICTURE'], 4, 95, 95)?>">
                                                        </a>
                                                    <? } ?>

												</td>

												<td class="info">
													<div class="name">
														<a href="<?=$basketItem->product['DETAIL_PAGE_URL']?>"><?=$basketItem->product['NAME']?></a>
													</div>

													<div class="features">
														<? if( intval($basketItem->size['ID']) > 0 ){ ?>
															<div>Размер: <?=$basketItem->size['NAME']?></div>
														<? } ?>
														<? if( intval($basketItem->color['ID']) > 0 ){ ?>
															<div>Цвет: <?=$basketItem->color['NAME']?></div>
														<? } ?>
													</div>
												</td>

												<td class="amount" data-column="Количество"><?=round($basketItem->q, 0)?></td>

												<td class="price" data-column="Сумма">
												
													<? if( $basketItem->itemSum != $basketItem->discItemSum ){ ?>
														<div class="old"><?=number_format($basketItem->itemSum, 0, ",", " ")?> <span class="currency">:</span></div>
													<? } ?>
													
													<?=number_format($basketItem->discItemSum, 0, ",", " ")?> <span class="currency">:</span>
												</td>

												<td class="price" data-column="Бонусы"><!--200 <span class="currency">:</span>--></td>
											</tr>
											
										<? } ?>

										</tbody>
									</table>
								</div>
							</td>
						</tr>

					<? } ?>	
						
					</tbody>
				</table>
			</div>
			
		<? } else { ?>
		
			<p class="no___count" style="text-align:left">Список заказов пуст</p>
		
		<? } ?>
			
	</div>
	
	
<? // НЕ авторизован
} else { 


	// personal_auth_block
	include $_SERVER["DOCUMENT_ROOT"].'/include/areas/personal_auth_block.php';

	
} ?>