<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;


$arResult['ITEMS'] = project\illustrator::weekList();

//if( count($arResult['ITEMS']) < 10 ){
//	$arResult['ITEMS'] = array_merge(
//        $arResult['ITEMS'],
//        project\illustrator::getRandList( 1000 - count($arResult['ITEMS']), $arResult['ITEMS'] )
//    );
//}


foreach ( $arResult['ITEMS'] as $key => $id ){

    $illustrator = tools\user::info($id);

    if (intval($illustrator['ID']) > 0) {

        $PRINTS = project\illustrator::getPrints($illustrator['ID']);

        $illustratorItem['PRINTS'] = array_slice($PRINTS, 0, 2);

        $illustratorItem['PRINTS_CNT'] = count($PRINTS);
        $illustratorItem['PRINTS_CNT_WORD'] = pfCnt($illustratorItem['PRINTS_CNT'], "принт", "принта", "принтов");

        if( $illustratorItem['PRINTS_CNT'] > 0){

            $illustratorItem['ID'] = $illustrator['ID'];

            $illustratorItem['NAME'] = $illustrator['NAME'];

            $illustratorItem['LINK'] = '/print/' . $illustratorItem['ID'] . '/';

            if( intval($illustrator['PERSONAL_PHOTO']) > 0 ){
                $illustratorItem['AVATAR'] = '/imgen/a:100,b:100,c:' . $illustrator['PERSONAL_PHOTO'] . ',f:100.doc';
            }

            $illustratorItem['SUBSCRIBES_CNT'] = count(project\i_subscribe::getListForIllustrator($illustratorItem['ID']));

            $illustratorItem['SUBSCRIBES_CNT_WORD'] = pfCnt($illustratorItem['SUBSCRIBES_CNT'], "подписка", "подписки", "подписок");

            $arResult['ITEMS'][$key] = $illustratorItem;

        } else {
            unset($arResult['ITEMS'][$key]);
        }

    } else {
        unset($arResult['ITEMS'][$key]);
    }

}

//echo "<pre>"; print_r( $arResult ); echo "</pre>";

$this->IncludeComponentTemplate();
