<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;



// НОВАЯ ВЕРСИЯ

$arResult['TOP_MENU_CATEGORIES'] = project\top_menu::get();





// TODO: СТАРАЯ ВЕРСИЯ (временно сохраняем)

$genderTypes = project\gender_type::list();

$all_categories = array();
$arResult['CATEGORIES'] = [];

$sections_1_level = project\catalog_niti::sections_1_level();

$activeGenderType = project\gender_type::getActive();

// Кешируем
$obCache = new \CPHPCache();
$cache_time = 60*60;
$cache_id = 'topCategoriesMenu';
$cache_path = '/topCategoriesMenu/';
if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
	$vars = $obCache->GetVars();   extract($vars);
} elseif($obCache->StartDataCache()){

	foreach( $sections_1_level as $section_1 ){

		$cnt_2 = 0;
		$sections_2_level = \CIBlockSection::GetList(
			Array("SORT"=>"ASC"),
			Array(
				"ACTIVE" => "Y",
				"DEPTH_LEVEL" => 2,
				"SECTION_ID" => $section_1['ID'],
				"IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID
			),
			false, Array("UF_GENDER_GROUP", "UF_NAME")
		);

		while ($section_2 = $sections_2_level->GetNext()){

            // Ищём подразделы 3 уровня в разделе $section_2
            $sections_3 = \CIBlockSection::GetList( Array("SORT"=>"ASC"), Array(
                "ACTIVE"=>"Y",
                "GLOBAL_ACTIVE" => "Y",
                "DEPTH_LEVEL" => 3,
                "IBLOCK_ID"=> project\site::CATALOG_IBLOCK_ID,
                "SECTION_ID" => $section_2['ID']
            ), false, Array("UF_GENDER_GROUP", "UF_NAME") );
            while ($section_3 = $sections_3->GetNext()){

                $gender_type = project\gender_type::getByID($section_3['UF_GENDER_GROUP']);

                $section_2['SUBSECTIONS'][] = [
                    'ID' => $section_3['ID'],
                    'NAME' => $section_3['NAME'],
                    'SITE_NAME' => $section_3['UF_NAME'],
                    'SECTION_PAGE_URL' => $section_3['SECTION_PAGE_URL'],
                    'DEPTH_LEVEL' => $section_3['DEPTH_LEVEL'],
                    'GENDER_TYPE' => [
                        'ID' => $gender_type['ID'],
                        'NAME' => $gender_type['NAME'],
                        'CODE' => $gender_type['CODE'],
                    ],
                ];

            }

            $gender_type = project\gender_type::getByID($section_2['UF_GENDER_GROUP']);

            $section_1['SUBSECTIONS'][] = [
                'ID' => $section_2['ID'],
                'NAME' => $section_2['NAME'],
                'SITE_NAME' => $section_2['UF_NAME'],
                'SECTION_PAGE_URL' => $section_2['SECTION_PAGE_URL'],
                'DEPTH_LEVEL' => $section_2['DEPTH_LEVEL'],
                'SUBSECTIONS' => $section_2['SUBSECTIONS'],
                'GENDER_TYPE' => [
                    'ID' => $gender_type['ID'],
                    'NAME' => $gender_type['NAME'],
                    'CODE' => $gender_type['CODE'],
                ],
            ];

		}

        $gender_type = project\gender_type::getByID($section_1['UF_GENDER_GROUP']);

        $all_categories[] = [
            'ID' => $section_1['ID'],
            'NAME' => $section_1['NAME'],
            'SITE_NAME' => $section_1['UF_NAME'],
            'SECTION_PAGE_URL' => $section_1['SECTION_PAGE_URL'],
            'DEPTH_LEVEL' => $section_1['DEPTH_LEVEL'],
            'SUBSECTIONS' => $section_1['SUBSECTIONS'],
            'GENDER_TYPE' => [
                'ID' => $gender_type['ID'],
                'NAME' => $gender_type['NAME'],
                'CODE' => $gender_type['CODE'],
            ],
        ];

	}

$obCache->EndDataCache(array('all_categories' => $all_categories));
}

$arResult['ALL_CATEGORIES'] = $all_categories;



foreach ( $arResult['ALL_CATEGORIES'] as $key1 => $section_1 ){
    foreach ( $section_1['SUBSECTIONS'] as $key2 => $section_2 ){
        if(
            $section_2['GENDER_TYPE']['ID'] == $activeGenderType['ID']
            ||
            $section_2['GENDER_TYPE']['ID'] == $genderTypes['all']['ID']
            ||
            !$section_2['GENDER_TYPE']['ID']
        ){} else {     unset($section_1['SUBSECTIONS'][$key2] );     }
    }
    $arResult['CATEGORIES'][] = $section_1;
}



$this->IncludeComponentTemplate();
