<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (count($arResult['ITEMS']) > 0){ ?>
	
	<section class="products related">
		<div class="cont">
			<div class="block_title"><div>недавно просмотренные</div></div>

			<div class="slider owl-carousel carousel2">

				<? foreach ($arResult['ITEMS'] as $key => $arItem){ ?>

					<div class="slide">
						<? // catalog_item_other
						$APPLICATION->IncludeComponent(
							"my:catalog_item_other", "", array('arItem' => $arItem)
						); ?>
					</div>
					
				<? } ?>
				
			</div>		
		</div>
	</section>
	
<? } ?>