<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;
$arItem = $arResult['arItem']; ?>

<div class="product">

	<a style="cursor:pointer" class="favorite_link fav___link to___process" tov_id="<?=$arItem['ID']?>">

  </a>

	<div class="thumb">
		<a href="<?=$arItem['DETAIL_PAGE_URL']?>">
			<img src="<?=tools\funcs::rIMGG($arResult['default_photo'], 4, 170, 215)?>">
		</a>
	</div>

	<a href="javascript:;" class="quike_view_link" data-fancybox data-src="/ajax/product.php?id=<?=$arItem['ID']?>" data-type="ajax">быстрый просмотр</a>

	<div class="name">
		<a href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['PRODUCT_NAME']?></a>
	</div>

    <? if( $arResult['DEFAULT_OFFER'] ){ ?>
	    <div class="price"><?=number_format($arResult['DEFAULT_OFFER']['PRICES']['BASE']['DISCOUNT_VALUE_VAT'], 0, ",", " ")?> <span class="currency">:</span></div>
    <? } ?>

</div>