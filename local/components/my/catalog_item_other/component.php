<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); 

$arResult['arItem'] = $arParams['arItem']; 

$arResult['prices'] = false;
foreach ( $arResult['arItem']['OFFERS'] as $offer ){
	$arResult['prices'][] = $offer['PRICES']['BASE']['DISCOUNT_VALUE_VAT'];
}
$arResult['min_price'] = min($arResult['prices']); 
$arResult['max_price'] = max($arResult['prices']);

$arResult['DEFAULT_OFFER'] = false;
foreach ( $arResult['arItem']['OFFERS'] as $offer ){
    if( $offer["PROPERTIES"]['MAIN_TP']['VALUE'] ){
        $arResult['DEFAULT_OFFER'] = $offer;
    }
}
if( !$arResult['DEFAULT_OFFER'] ){   $arResult['DEFAULT_OFFER'] = $arResult['arItem']['OFFERS'][0];   }

$arResult['default_photo'] = false;
if( intval($arResult['DEFAULT_OFFER']['PROPERTIES']['PHOTO_ID']['VALUE']) > 0 ){
    $arResult['default_photo'] = $arResult['DEFAULT_OFFER']['PROPERTIES']['PHOTO_ID']['VALUE'];
}

$arResult['arItem']['PRODUCT_NAME'] = htmlspecialchars_decode($arResult['arItem']['~NAME']);
if( $arResult['arItem']['DISPLAY_PROPERTIES']['PRINT_ID']['LINK_ELEMENT_VALUE'] ){
	$keys = array_keys($arResult['arItem']['DISPLAY_PROPERTIES']['PRINT_ID']['LINK_ELEMENT_VALUE']);
	if( count($keys) > 0 ){
		$arResult['arItem']['PRODUCT_NAME'] = htmlspecialchars_decode($arResult['arItem']['DISPLAY_PROPERTIES']['PRINT_ID']['LINK_ELEMENT_VALUE'][$keys[0]]['~NAME']);
	}
}





$this->IncludeComponentTemplate();

