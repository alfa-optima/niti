<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$cur_step = $arParams['step']; ?>


<div class="checkout_steps">

	<a style="cursor:pointer" class="step back_to_basket_button <? if( $cur_step == 1 ){ ?>active<? } ?>">Корзина</a>

	<? if( $cur_step <= 2 ){ ?>
		<div class="step <? if( $cur_step == 2 ){ ?>active<? } ?>">Оформление заказа</div>
	<? } else { ?>
		<a style="cursor:pointer" class="step back_to_step_2_button">Оформление заказа</a>
	<? } ?>

	<? if( $cur_step <= 3 ){ ?>
		<div class="step <? if( $cur_step == 3 ){ ?>active<? } ?>">Оплата</div>
	<? } else { ?>
		<a style="cursor:pointer" class="step back_to_step_3_button">Оплата</a>
	<? } ?>
	
</div>