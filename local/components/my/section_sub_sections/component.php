<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); 
$arResult['sub_sects'] = $arParams['sub_sects']; 

$arResult['classes'] = array(
	1 => 'left big',
	2 => 'left',
	3 => 'right big_height',
	4 => 'left',
	5 => 'left big'
);

$arResult['pic_sizes'] = array(
	1 => array('width' => 570, 'height' => 270),
	2 => array('width' => 270, 'height' => 270),
	3 => array('width' => 270, 'height' => 570),
	4 => array('width' => 270, 'height' => 270),
	5 => array('width' => 570, 'height' => 270)
);





$this->IncludeComponentTemplate();
