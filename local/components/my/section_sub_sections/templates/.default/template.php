<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<div class="sub_cats_block">
	<div class="hor_scroll">
		<div class="grid">

			<? $cnt = 0;
			foreach ( $arResult['sub_sects'] as $sect ){  $cnt++; 
				if ( $cnt > 5 ){   $cnt = 1;   } ?>
			
				<div class="item_wrap <?=$arResult['classes'][$cnt]?>">
					<a href="<?=$sect['SECTION_PAGE_URL']?>" class="item" title="<?=$sect['NAME']?>">
						<img src="<?=rIMGG($sect['PICTURE'], 5, $arResult['pic_sizes'][$cnt]['width'], $arResult['pic_sizes'][$cnt]['height'])?>">
					</a>
				</div>

			<? } ?>
			
		</div>
	</div>
</div>