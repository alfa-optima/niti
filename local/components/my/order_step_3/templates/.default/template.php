<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<div class="checkout_info">


	<aside class="right">

	
		<?=$arResult['BASKET_RIGHT']?>

		
	</aside>


	<section class="content left">
	
	
		<?=$arResult['ORDER_STEPS']?>

		
		<form action="" method="post" class="form order___form">
		
		
			<? foreach ( $_POST as $name => $value ){ 
				if ( !in_array($name,  $arResult['stop_names']) ){ ?>
					<input type="hidden" name="<?=$name?>" value="<?=$value?>">
				<? }
			} ?>
		
		
			<div class="title">Выберите способ оплаты</div>

			<div class="payment_methods">
			
				<? // Варианты оплаты
				foreach( $arResult['arPS'] as $ps ){ ?>
				
					<div class="line">
						<div class="field">
						
							<input type="radio" name="ps" id="payment_method<?=$ps['ID']?>" <? if( $ps['checked'] ){ ?>checked<? } ?> value="<?=$ps['ID']?>">
							
							<label for="payment_method<?=$ps['ID']?>">
								<div class="icon">
									<img src="<?=rIMGG($ps['PSA_LOGOTIP'], 4, 20, 20)?>">
								</div>
								
								<?=$ps['NAME']?>
								
							</label>
						</div>
					</div>
					
				<? } ?>
			
			</div>
			
		</form>


		<div class="checkout_links">
		
			<a style="cursor:pointer" class="back_link left back_to_step_2_button">
				<span class="icon"></span>Вернуться
			</a>

			<a style="cursor:pointer" class="next_link order_link right order___button to___process">
				Oформить заказ<span class="icon"></span>
			</a>
			
		</div>
		
	</section>
	<div class="clear"></div>

</div>