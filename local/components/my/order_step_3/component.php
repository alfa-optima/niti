<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if( count($_POST) > 0 ){} else {    LocalRedirect('/basket/');   }

// Инфо по корзине
$cupon = project\my_coupon::get_active_coupon();
$arResult['basketInfo'] = basketInfo( $cupon );
$arResult['basket_cnt'] = $arResult['basketInfo']['basket_cnt'];
$arResult['arBasket'] = $arResult['basketInfo']['arBasket'];


if ( $arResult['basket_cnt'] == 0 ){    LocalRedirect('/basket/');   }


// Инфо об авторизованном пользователе
if ($USER->IsAuthorized()){
	
	$q = CUser::GetByID($USER->GetID());
	$arResult['arUser'] = $q->GetNext();
	
}


$arResult['stop_names'] = array('ps');


ob_start(); 
	$params = array('basketInfo' => $arResult['basketInfo']);
	if ( strlen(param_post('DELIVERY_PRICE')) > 0 ){
		$params['delivery_price'] = (int)param_post('DELIVERY_PRICE');
	}
	$APPLICATION->IncludeComponent("my:basket_right", "", $params);
	$arResult['BASKET_RIGHT'] = ob_get_contents();
ob_end_clean();


ob_start(); 
	$APPLICATION->IncludeComponent(
		"my:order_steps", "",
		array('step' => 3)
	);
	$arResult['ORDER_STEPS'] = ob_get_contents();
ob_end_clean();


$pss = array();
$res = \CSaleDelivery::GetDelivery2PaySystem();
while ($var = $res->GetNext()){
	if ( $var['DELIVERY_ID'] == param_post('ds') ){ 
		$pss[] = $var["PAYSYSTEM_ID"];
	}
}

$arResult['arPS'] = array();   $cnt = 0;
$stop_pss = [6];
$db_ptype = \CSalePaySystem::GetList(
	Array(
		"SORT"=>"ASC", "PSA_NAME"=>"ASC"
	),
	Array(
		"ACTIVE"=>"Y",
        "ID" => $pss,
        "!ID" => $stop_pss,
		'PSA_PERSON_TYPE_ID' => param_post('user_pay_type_id')
	),
	false, false,
	array('ID', 'NAME', 'LOGOTIP', 'DESCRIPTION')
);
while ($ps = $db_ptype->GetNext()){ $cnt++;
	$ps['checked'] = false; 
	if ( param_post('ps') && param_post('ps') == $ps['ID'] ){
		$ps['checked'] = true; 
	} else if( $cnt == 1 ){
		$ps['checked'] = true;
	}
	$arResult['arPS'][] = $ps;
}












$this->IncludeComponentTemplate();