<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<script src="http://api-maps.yandex.ru/2.0/?load=package.standard,package.geoObjects&lang=ru-RU"></script>

<script>
	var myMap;
	var points = {};
	$(window).load(function(){
		ymaps.ready(init)

		function init(){
			myMap = new ymaps.Map("map", {
				center: [<?=$arResult['settings']['MAP_CENTER']['VALUE']?>],
				zoom: <?=$arResult['settings']['MAP_ZOOM']['VALUE']?>
			})
			
			myMap.controls.add(
			   new ymaps.control.ZoomControl()
			);
			
			<?=$arResult['SHOPS']?>
			
			return myMap;
		}
		
		
		// Клик по магазину
		$(document).on('click', '.shop___link', function(){
			var item_id = $(this).attr('item_id');
			myMap.setCenter(points[item_id], 17);
		})
		
		
		// Выбор города
		$(document).on('click', 'div.cities ul.list li.option', function(){
			var city = $(this).attr('data-value');
			if( city == 'all' ){
				$('.shop___link').show();
				$('.shop___link').eq(0).trigger('click');
			} else {
				$('.shop___link').hide();
				$('.shop___link[city='+city+']').show();
				$('.shop___link:visible').eq(0).trigger('click');
			}
			$('div.cities span.current').trigger('click');
		})
		
		
	})
</script>