<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$menuSubHeader = [];
$menuSubHeader[] = ['name' => 'Партнерская программа', 'url' => '/partnership/', 'num' => 1];
$menuSubHeader[] = ['name' => 'Доставка', 'url' => '/delivery/', 'num' => 2];

if ($arResult['IS_AUTH'] == 'Y') {

    $menuSubHeader[] = ['name' => 'Личный кабинет', 'url' => '/personal/', 'num' => 2];
    $menuSubHeader[] = ['name' => 'Выход', 'url' => '/out.php?to=' . $_SERVER['REQUEST_URI'], 'num' => 2];

} else {
    $menuSubHeader[] = ['name' => 'Войти в аккаунт', 'url' => '#login_modal', 'num' => 3];
}

echo "<script>
var menu_sub_header = " . json_encode($menuSubHeader) . ";
</script>";




