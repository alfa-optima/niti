<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); 

// Получим список всех seo-блоков
$arResult['allSeoBlocks'] = allSeoBlocks();

// Текущий URL
$arResult['cur_url'] = $_SERVER["REQUEST_URI"];

$arResult['seoBlock'] = false;

foreach ($arResult['allSeoBlocks'] as $key => $seo_block){
	
	// Если URL текущей страницы равен URL блока
	if ($seo_block['PROPERTY_URL_VALUE'] == $cur_url){
		
		$arResult['seoBlock'] = $seo_block;
		
		break;
	}

}




$this->IncludeComponentTemplate();