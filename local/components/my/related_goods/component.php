<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$el = tools\el::info( $arParams['product_id'] );
$params = $arParams['params'];

$related_goods = project\related_goods::list($el['ID']);

$arResult['el'] = $el;
$arResult['tabs'] = project\related_goods::tabs($related_goods);



$this->IncludeComponentTemplate();
