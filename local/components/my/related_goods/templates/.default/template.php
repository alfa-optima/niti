<?

$tabs = $arResult['tabs'];
$el = $arResult['el'];

if( count($tabs) > 0 ){ ?>

	<section class="products related">
		<div class="cont">
			<div class="block_title"><div>также с этим принтом</div></div>

			<div class="tabs_container">

				<ul class="tabs">

					<? $cnt = 0;
					foreach ( $tabs as $tab_name => $ids ){ $cnt++; ?>

						<li <? if( $cnt == 1 ){ ?>class="active"<? } ?>>
							<a href="#tab<?=$cnt?>"><?=$tab_name?></a>
						</li>

					<? } ?>

				</ul>

				<? $cnt = 0;
				foreach ( $tabs as $tab_name => $ids ){ $cnt++; ?>

					<div class="tab_content" id="tab<?=$cnt?>">

						<? // Выводим товары $ids
						$GLOBALS['tab_goods_Filter']['ID'] = $ids;
						$GLOBALS['tab_goods_Filter']['!ID'] = $el['ID'];
						$APPLICATION->IncludeComponent(
							"bitrix:catalog.section", "tab_goods",
							Array(
								"IBLOCK_TYPE" => "content",
								"IBLOCK_ID" => $el['IBLOCK_ID'],
								"SECTION_USER_FIELDS" => array(),
								"ELEMENT_SORT_FIELD" => "NAME",
								"ELEMENT_SORT_ORDER" => "ASC",
								"ELEMENT_SORT_FIELD2" => "NAME",
								"ELEMENT_SORT_ORDER2" => "ASC",
								"FILTER_NAME" => "tab_goods_Filter",
								"HIDE_NOT_AVAILABLE" => "N",
								"PAGE_ELEMENT_COUNT" => "6",
								"LINE_ELEMENT_COUNT" => "3",
								"PROPERTY_CODE" => array('PRINT_ID'),
								"OFFERS_CART_PROPERTIES" => $arResult['params']["OFFERS_CART_PROPERTIES"],
								"OFFERS_FIELD_CODE" => $arResult['params']["LIST_OFFERS_FIELD_CODE"],
								"OFFERS_PROPERTY_CODE" => ['PHTO'],
								"OFFERS_SORT_FIELD" => $arResult['params']["OFFERS_SORT_FIELD"],
								"OFFERS_SORT_ORDER" => $arResult['params']["OFFERS_SORT_ORDER"],
								"OFFERS_SORT_FIELD2" => $arResult['params']["OFFERS_SORT_FIELD2"],
								"OFFERS_SORT_ORDER2" => $arResult['params']["OFFERS_SORT_ORDER2"],
								"OFFERS_LIMIT" => 1 /*$arResult['params']["LIST_OFFERS_LIMIT"]*/,
								"TEMPLATE_THEME" => "",
								"PRODUCT_SUBSCRIPTION" => "N",
								"SHOW_DISCOUNT_PERCENT" => "N",
								"SHOW_OLD_PRICE" => "N",
								"MESS_BTN_BUY" => "Купить",
								"MESS_BTN_ADD_TO_BASKET" => "В корзину",
								"MESS_BTN_SUBSCRIBE" => "Подписаться",
								"MESS_BTN_DETAIL" => "Подробнее",
								"MESS_NOT_AVAILABLE" => "Нет в наличии",
								"SECTION_URL" => "",
								"DETAIL_URL" => "",
								"SECTION_ID_VARIABLE" => "SECTION_ID",
								"AJAX_MODE" => "N",
								"AJAX_OPTION_JUMP" => "N",
								"AJAX_OPTION_STYLE" => "Y",
								"AJAX_OPTION_HISTORY" => "N",
								"CACHE_TYPE" => "A",
								"CACHE_TIME" => "36000000",
								"CACHE_GROUPS" => "Y",
								"SET_META_KEYWORDS" => "N",
								"META_KEYWORDS" => "",
								"SET_META_DESCRIPTION" => "N",
								"META_DESCRIPTION" => "",
								"BROWSER_TITLE" => "-",
								"ADD_SECTIONS_CHAIN" => "N",
								"DISPLAY_COMPARE" => "N",
								"SET_TITLE" => "N",
								"SET_STATUS_404" => "N",
								"CACHE_FILTER" => "Y",
								"PRICE_CODE" => array('BASE'),
								"USE_PRICE_COUNT" => "N",
								"SHOW_PRICE_COUNT" => "1",
								"PRICE_VAT_INCLUDE" => "Y",
								"CONVERT_CURRENCY" => "N",
								"BASKET_URL" => "/personal/basket.php",
								"ACTION_VARIABLE" => "action",
								"PRODUCT_ID_VARIABLE" => "id",
								"USE_PRODUCT_QUANTITY" => "N",
								"ADD_PROPERTIES_TO_BASKET" => "Y",
								"PRODUCT_PROPS_VARIABLE" => "prop",
								"PARTIAL_PRODUCT_PROPERTIES" => "N",
								"PRODUCT_PROPERTIES" => "",
								"PAGER_TEMPLATE" => "",
								"DISPLAY_TOP_PAGER" => "N",
								"DISPLAY_BOTTOM_PAGER" => "Y",
								"PAGER_TITLE" => "Товары",
								"PAGER_SHOW_ALWAYS" => "N",
								"PAGER_DESC_NUMBERING" => "N",
								"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
								"PAGER_SHOW_ALL" => "Y",
								"ADD_PICT_PROP" => "-",
								"LABEL_PROP" => "-",
								'INCLUDE_SUBSECTIONS' => "Y",
								'SHOW_ALL_WO_SECTION' => "Y"
							)
						); ?>

					</div>

				<? } ?>


			</div>
		</div>
	</section>

<? } ?>