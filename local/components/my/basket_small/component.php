<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$pureURL = $arParams['pureURL'];



// Инфо по корзине
$cupon = project\my_coupon::get_active_coupon();
$basketInfo = basketInfo( $cupon );

$arResult['basket_cnt'] = $basketInfo['basket_cnt'];
$arResult['arBasket'] = $basketInfo['arBasket'];
$arResult['basket_disc_sum'] = $basketInfo['basket_disc_sum']; 

$arResult['standard_link'] = false;
$arResult['open'] = true;
if ( $arResult['basket_cnt'] == 0 ){
	$arResult['open'] = false;
}

$stop_URLs = array('/basket/');
$dop_stop_URLs = array('/order_step_2/', '/order_step_3/', '/order_step_4/');
if(
	$pureURL
	&&
	(
		in_array($pureURL, $stop_URLs)
		||
		in_array($pureURL, $dop_stop_URLs)
	)
){
	$arResult['open'] = false;
}

if ( $arResult['basket_cnt'] == 0 || in_array($pureURL, $dop_stop_URLs) ){
	$arResult['standard_link'] = true;
}



$this->IncludeComponentTemplate();
