<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

?>

<div class="modal_cont cart basket_small_block">

	<a href="/basket/" class="<? if( !$arResult['standard_link'] ){ ?>mini_modal_link<? } ?> cart_link" <? if( $arResult['open'] ){ ?>data-modal-id="#cart_modal"<? } ?>>
		<span <?if($arResult['basket_cnt']==0){?>class="empty___basket"<? } ?>><?=$arResult['basket_cnt']?></span>
	</a>

	<? if( $arResult['basket_cnt'] > 0 ){ ?>

		<div class="mini_modal" id="cart_modal">

			<? foreach ( $arResult['arBasket'] as $basketItem ){

				$el = $basketItem->el;
				$product = $basketItem->product; ?>

				<div class="product">

					<div class="thumb">
						<a href="<?=$product['DETAIL_PAGE_URL']?>">
							<img style="width:auto" src="<?=rIMG($el['PROPERTY_PHTO_VALUE'], 4, 72, 59)?>">
						</a>
					</div>

					<div class="info">
						<div class="name">
							<a href="<?=$product['DETAIL_PAGE_URL']?>">
								<?=htmlspecialchars_decode($basketItem->product_name)?>
								<? $color_id = $el['PROPERTY_COLOR_VALUE'];
								$size = $el['PROPERTY_SIZE_VALUE'];
								if ( intval($color_id) > 0 || strlen($size) > 0 ){
									echo ' (';
										if ( intval($color_id) > 0 ){
											$color = tools\el::info($color_id);
											echo $color['NAME'];
										}
										if ( strlen($size) > 0 ){
											if ( intval($color_id) > 0 ){
												echo ', ';
											}
											echo $size;
										}
									echo ')';
								} ?>
							</a>
						</div>

						<div><?=round($basketItem->getField('QUANTITY'), 0)?> x <?=number_format($basketItem->discPrice, 0, ",", " ")?> <span class="currency">:</span></div>
					</div>

					<a style="cursor:pointer" class="delete basket_small___remove to___process" item_id="<?=$basketItem->getField('ID')?>"></a>

				</div>

			<? } ?>

			<div class="bottom_info">

				<div class="total_ptice">
					Итого без учета доставки:
					<div class="price right"><?=number_format($arResult['basket_disc_sum'], 0, ",", " ")?> <span class="currency">:</span></div>
					<div class="clear"></div>
				</div>

				<a href="/basket/" class="checkout_link">Оформить заказ</a>

			</div>

		</div>

	<? } ?>

</div>
