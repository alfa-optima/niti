<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('iblock');

if( $arResult ){ ?>

	<div class="cont">

		<section class="category_info">
			<!-- Боковая колонка -->
			<aside class="left mobile_hdden">

				<? // left_sections
				$APPLICATION->IncludeComponent(
					"bitrix:catalog.section.list",   "left_sections_illustrator",
					Array(
						"IBLOCK_TYPE" => "catalog",
						"IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
						"COUNT_ELEMENTS" => "N",
						"TOP_DEPTH" => "1",
						"SECTION_FIELDS" => array(),
						"SECTION_USER_FIELDS" => array(),
						"VIEW_MODE" => "LIST",
						"SHOW_PARENT_NAME" => "Y",
						"SECTION_URL" => "",
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => "36000000",
						"CACHE_GROUPS" => "Y",
						"ADD_SECTIONS_CHAIN" => "N"
					)
				); ?>
				
				<? // Умный фильтр
				$APPLICATION->IncludeComponent(
					"bitrix:catalog.smart.filter", "myFilter",
					Array(
						"IBLOCK_TYPE" => 'catalog',
						"IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
						"FILTER_NAME" => 'filter',
						"PRICE_CODE" => Array('BASE'),
						"CACHE_TYPE" => 'A',
						"CACHE_TIME" => 36000000,
						"CACHE_GROUPS" => 'Y',
						"SAVE_IN_SESSION" => 'N',
						"FILTER_VIEW_MODE" => 'VERTICAL',
						"XML_EXPORT" => 'Y',
						"SECTION_TITLE" => 'NAME',
						"SECTION_DESCRIPTION" => 'DESCRIPTION',
						"HIDE_NOT_AVAILABLE" => 'N',
						"TEMPLATE_THEME" => 'blue',
						"CONVERT_CURRENCY" => 'N',
						"SEF_MODE" => 'Y',
						"INSTANT_RELOAD" => 'N'
					)
				); ?>

			</aside>
			<!-- End Боковая колонка -->


			<!-- Основная часть -->
			<section class="content right">
			
				<div class="cat_head">
				
					<h1 class="page_title left">Товары с принтом "<?=$arResult["NAME"]?>"</h1>

          <aside class="left mobile_visible">

              <? // left_sections
              $APPLICATION->IncludeComponent(
                  "bitrix:catalog.section.list",   "left_sections_illustrator",
                  Array(
                      "IBLOCK_TYPE" => "catalog",
                      "IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
                      "COUNT_ELEMENTS" => "N",
                      "TOP_DEPTH" => "1",
                      "SECTION_FIELDS" => array(),
                      "SECTION_USER_FIELDS" => array(),
                      "VIEW_MODE" => "LIST",
                      "SHOW_PARENT_NAME" => "Y",
                      "SECTION_URL" => "",
                      "CACHE_TYPE" => "A",
                      "CACHE_TIME" => "36000000",
                      "CACHE_GROUPS" => "Y",
                      "ADD_SECTIONS_CHAIN" => "N"
                  )
              ); ?>

              <? // Умный фильтр
              $APPLICATION->IncludeComponent(
                  "bitrix:catalog.smart.filter", "myFilter",
                  Array(
                      "IBLOCK_TYPE" => 'catalog',
                      "IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
                      "FILTER_NAME" => 'filter',
                      "PRICE_CODE" => Array('BASE'),
                      "CACHE_TYPE" => 'A',
                      "CACHE_TIME" => 36000000,
                      "CACHE_GROUPS" => 'Y',
                      "SAVE_IN_SESSION" => 'N',
                      "FILTER_VIEW_MODE" => 'VERTICAL',
                      "XML_EXPORT" => 'Y',
                      "SECTION_TITLE" => 'NAME',
                      "SECTION_DESCRIPTION" => 'DESCRIPTION',
                      "HIDE_NOT_AVAILABLE" => 'N',
                      "TEMPLATE_THEME" => 'blue',
                      "CONVERT_CURRENCY" => 'N',
                      "SEF_MODE" => 'Y',
                      "INSTANT_RELOAD" => 'N'
                  )
              ); ?>

          </aside>

					<div class="count left all_elements_block">0 товаров</div>
					
					<script type="text/javascript">
					$(document).ready(function(){
						var cnt = Number($('input[name=all_elements_cnt]').val());
						$('.all_elements_block').html(cnt);
						cnt = Number(parseInt(cnt.replace(/\D+/g,"")));
						if( cnt > 0 ){    $('.row_cnt_block').show();    }
					});
					</script>

					<? // Устанавливаем количество товаров в строке
					global $APPLICATION;
					$row_cnt = $APPLICATION->get_cookie('row_cnt');
					if ( !$row_cnt ){   $row_cnt = 3;   } ?>
					
					<div class="limit right row_cnt_block" style="display:none">
						<div class="left">Показывать по:</div>
						<a style="cursor:pointer" <? if( $row_cnt == 3 ){ ?>class="active"<? } ?>>3</a>
						<a style="cursor:pointer" <? if( $row_cnt == 4 ){ ?>class="active"<? } ?>>4</a>
						<div class="clear"></div>
					</div>
					<div class="clear"></div>
					
				</div>
				
				
				<? // Количество товаров на странице каталога
				$page_cnt = 12;
				$template_name = "catalog_products_3";
				
				if ( $row_cnt == 4 ){
					$page_cnt = 16;
					$template_name = "catalog_products_4";
				}

				// Товары с принтом
				//$GLOBALS['filter']['PROPERTY_HAS_SKU'] = 1;
				$GLOBALS['filter']['PROPERTY_PRINT_ID'] = $arResult['ID'];
				$GLOBALS['filter']['INCLUDE_SUBSECTIONS'] = "Y";

				// Категории товаров
				$els_sections = project\product::getElsSections([
                    "IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
                    "ACTIVE" => "Y",
                    "PROPERTY_PRINT_ID" => $arResult['ID'],
                ]);
				
				$APPLICATION->IncludeComponent( "bitrix:catalog.section", $template_name,
					Array(
						"els_sections" => $els_sections,
						"SHOW_ALL_WO_SECTION" => "Y",
						"INCLUDE_SUBSECTIONS" => "Y",
						"IBLOCK_TYPE" => "content",
						"IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
						"SECTION_USER_FIELDS" => array(),
						"ELEMENT_SORT_FIELD" => "NAME",
						"ELEMENT_SORT_ORDER" => "ASC",
						"ELEMENT_SORT_FIELD2" => "NAME",
						"ELEMENT_SORT_ORDER2" => "ASC",
						"FILTER_NAME" => "filter",
						"HIDE_NOT_AVAILABLE" => "N",
						//"PAGE_ELEMENT_COUNT" => $page_cnt,
						"LINE_ELEMENT_COUNT" => "3",
						"PROPERTY_CODE" => array('HAS_SKU', 'PRINT_ID'),
						"OFFERS_LIMIT" => "0",
						"OFFERS_PROPERTY_CODE" => array('SIZE', 'COLOR'),
						array(
							0 => 'ID',
						),
						"TEMPLATE_THEME" => "",
						"PRODUCT_SUBSCRIPTION" => "N",
						"SHOW_DISCOUNT_PERCENT" => "N",
						"SHOW_OLD_PRICE" => "N",
						"MESS_BTN_BUY" => "Купить",
						"MESS_BTN_ADD_TO_BASKET" => "В корзину",
						"MESS_BTN_SUBSCRIBE" => "Подписаться",
						"MESS_BTN_DETAIL" => "Подробнее",
						"MESS_NOT_AVAILABLE" => "Нет в наличии",
						"SECTION_URL" => "",
						"DETAIL_URL" => "",
						"SECTION_ID_VARIABLE" => "SECTION_ID",
						"AJAX_MODE" => "N",
						"AJAX_OPTION_JUMP" => "N",
						"AJAX_OPTION_STYLE" => "Y",
						"AJAX_OPTION_HISTORY" => "N",
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => "36000000",
						"CACHE_GROUPS" => "Y",
						"SET_META_KEYWORDS" => "N",
						"META_KEYWORDS" => "",
						"SET_META_DESCRIPTION" => "N",
						"META_DESCRIPTION" => "",
						"BROWSER_TITLE" => "-",
						"ADD_SECTIONS_CHAIN" => "N",
						"DISPLAY_COMPARE" => "N",
						"SET_TITLE" => "N",
						"SET_STATUS_404" => "N",
						"CACHE_FILTER" => "Y",
						"PRICE_CODE" => array('BASE'),
						"USE_PRICE_COUNT" => "N",
						"SHOW_PRICE_COUNT" => "1",
						"PRICE_VAT_INCLUDE" => "Y",
						"CONVERT_CURRENCY" => "N",
						"BASKET_URL" => "/personal/basket.php",
						"ACTION_VARIABLE" => "action",
						"PRODUCT_ID_VARIABLE" => "id",
						"USE_PRODUCT_QUANTITY" => "N",
						"ADD_PROPERTIES_TO_BASKET" => "Y",
						"PRODUCT_PROPS_VARIABLE" => "prop",
						"PARTIAL_PRODUCT_PROPERTIES" => "N",
						"PRODUCT_PROPERTIES" => "",
						"PAGER_TEMPLATE" => "",
						"DISPLAY_TOP_PAGER" => "N",
						"DISPLAY_BOTTOM_PAGER" => "Y",
						"PAGER_TITLE" => "Товары",
						"PAGER_SHOW_ALWAYS" => "N",
						"PAGER_DESC_NUMBERING" => "N",
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
						"PAGER_SHOW_ALL" => "Y",
						"ADD_PICT_PROP" => "-",
						"LABEL_PROP" => "-",
					)
				); ?>
				
			</section>
			<div class="clear"></div>
			
		</section>

	</div>





<? } ?>