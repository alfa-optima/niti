<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); 

$url = $_SERVER['REQUEST_URI'];

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$ar = array('.html', '.htm', '.php');
$get_string = explode('?', $url)[1];
$pureURL = explode('?', $url)[0];
$is_page = false;
foreach( $ar as $item ){
	if( substr_count(pureURL(), $item) ){    $is_page = true;    }
}
if(  !$is_page   &&   $pureURL[strlen($pureURL)-1] != '/'  ){
	$pureURL .= '/';
	if( strlen($get_string) > 0 ){
		$url = $pureURL.'?'.$get_string;
	} else {
		$url = $pureURL;
	}
	LocalRedirect($url , false, '301 Moved permanently');
} else if($is_page){
	CHTTP::SetStatus("404 Not Found");
	@define("ERROR_404","Y");
	$APPLICATION->SetTitle("404 Not Found");
}

$arURI = arURI($url);
$print_id = $arURI[2];

if( !$print_id || $arURI[3] ){
	CHTTP::SetStatus("404 Not Found");
	@define("ERROR_404","Y");
	$APPLICATION->SetTitle("404 Not Found");
}

// Инфо о принте
$print = tools\el::info($print_id);

if( intval($print['ID']) > 0 ){
	
	$APPLICATION->AddChainItem('Иллюстраторы', '/illustrators/');
	$APPLICATION->AddChainItem('Товары с принтом "'.$print["NAME"].'"', $url);
	$APPLICATION->SetPageProperty("title", 'Товары с принтом "'.$print["NAME"].'"');
	
	$arResult = $print;
	
	
	
	
} else {
	CHTTP::SetStatus("404 Not Found");
	@define("ERROR_404","Y");
	$APPLICATION->SetTitle("404 Not Found");
}










$this->IncludeComponentTemplate();