<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (count($arResult['collections']) > 0){ ?>

	<div class="cats">
		<ul>
		
			<? foreach ($arResult['collections'] as $collection){ ?>
				<li>
					<a <? if( $arParams['cur_id'] == $collection['ID'] ){ ?>class="active"<? } ?> href="<?=$collection['DETAIL_PAGE_URL']?>"><?=$collection['NAME']?></a>
				</li>
			<? } ?>
			
		</ul>
	</div>

<? } ?>