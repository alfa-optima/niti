<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<div class="cart">

	<input type="hidden" name="basket_disc_sum" value="<?=$arResult['basket_disc_sum']?>">

	<div class="title">Ваша корзина</div>

	<div class="data">
	
		<? // перебираем корзину
		foreach ( $arResult['arBasket'] as $basketItem ){ ?>
	
			<div class="product">
				<div class="name">
					<a href="<?=$basketItem->product['DETAIL_PAGE_URL']?>"><?=htmlspecialchars_decode($basketItem->product_name)?></a>
				</div>

				<div class="cat">
					<? if( $basketItem->color_name || $basketItem->size_name ){ ?>
						<a style="text-decoration:none"><?=$basketItem->color_name?><? if( $basketItem->color_name && $basketItem->size_name ){ ?> / <? } ?><?=$basketItem->size_name?></a>
					<? } ?>
				</div>

				<div><?=$basketItem->quantity?> x <?=number_format($basketItem->discPrice, 0, ",", " ")?> <span class="currency">:</span></div>
			</div>

		<? } ?>
			
	</div>

	<div class="totals">
	
		<div class="item right_delivery_block">
			<div class="name">Доставка</div>
			<div class="val basketRightDeliveryPrice"><?=$arResult['delivery_price']?></div>
		</div>

		<div class="item">
			<div class="name">Скидка</div>
			<div class="val"><?=number_format($arResult['discount'], 0, ",", " ")?> <span class="currency">:</span></div>
		</div>

		<div class="item">
			<div class="name">Итого</div>
			<div class="val total_price basketRightSum"><?=number_format($arResult['total_sum'], 0, ",", " ")?> <span class="currency">:</span></div>
		</div>
		
	</div>
	
	<input type="hidden" name="json_basket" value="<?=$arResult['json_basket']?>">
	
</div>