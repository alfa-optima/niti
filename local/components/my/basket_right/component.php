<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

// Инфо по корзине
if($arParams['basketInfo']){
	$basketInfo = $arParams['basketInfo'];
} else {
    $cupon = project\my_coupon::get_active_coupon();
	$basketInfo = basketInfo( $cupon );
}



$arResult['basket_cnt'] = $basketInfo['basket_cnt'];
$arResult['arBasket'] = $basketInfo['arBasket']; 
$arResult['discount'] = $basketInfo['discount']; 
$arResult['basket_disc_sum'] = $basketInfo['basket_disc_sum']; 
$arResult['total_sum'] = $basketInfo['basket_disc_sum']; 


$basket_for_json = array();

foreach ( $arResult['arBasket'] as $key => $basketItem ){ 
	$sku = $basketItem->el;
	$product = $basketItem->product; 
	
	$basketItem->color_name = tools\el::info($sku['PROPERTY_COLOR_VALUE'])['NAME'];
	$basketItem->size_name = tools\el::info($sku['PROPERTY_SIZE_VALUE'])['NAME'];

	$basketItem->quantity = round($basketItem->getField('QUANTITY'), 0); 
	
	$basket_for_json[$sku['ID']] = $basketItem->quantity;
	
	$arResult['arBasket'][$key] = $basketItem;
}

$arResult['json_basket'] = htmlspecialchars(json_encode($basket_for_json));



if( 
	strlen($arParams['delivery_price']) > 0
	&&
	$arParams['delivery_price'] != '-'
){
	$arResult['delivery_price'] = $arParams['delivery_price'];
} else {
	$arResult['delivery_price'] = project\site::DEFAULT_DELIVERY_PRICE;
}



if( is_int($arResult['delivery_price']) || is_double($arResult['delivery_price']) ){
	$arResult['total_sum'] += $arResult['delivery_price'];
}



if( 
	is_int($arResult['delivery_price'])
	||
	is_double($arResult['delivery_price'])
){
	$arResult['delivery_price'] = $arResult['delivery_price'].' <span class="currency">:</span>';
} 



$this->IncludeComponentTemplate();