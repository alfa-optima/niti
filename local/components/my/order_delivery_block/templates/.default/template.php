<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>


<div class="order_delivery_block">

	<input type="hidden" name="BOXBERRY_LOC_ID" value="<?=$arResult['BOXBERRY_LOC_ID']?>">
	<input type="hidden" name="SDEK_LOC_ID" value="<?=$arResult['SDEK_LOC_ID']?>">
	<input type="hidden" name="DELIVERY_PRICE" value="<?=$arResult['DELIVERY_PRICE']?>">
	

	<? if( $arResult['DS_CNT'] > 0 ){ ?>

		<div class="title">Выберите способ доставки</div>

		<div class="delivery">
		
		<? foreach( $arResult['DELIVERY_VARIANTS'] as $variant => $price ){
		
			// Самовывоз
			if( $variant == 'SAMO' ){
		
				if( $arResult['SAMO_CHECK'] == 'Y' ){ ?>
				
					<div class="line">
						<div class="field">
							<input type="radio" <? if( param_post('ds') == $arResult['SAMO_DS_ID'] ){ ?>checked<? } ?> name="ds" id="ds_<?=$arResult['SAMO_DS_ID'] ?>" price="<?=$arResult['SAMO_DELIVERY_PRICE']?$arResult['SAMO_DELIVERY_PRICE']:''?>" value="<?=$arResult['SAMO_DS_ID']?>">
							<label for="ds_<?=$arResult['SAMO_DS_ID'] ?>" >
								<div class="name">Самовывоз</div>
								<div class="desc">
									Стоимость доставки: <?=$arResult['SAMO_DELIVERY_PRICE']==0?'бесплатно':($arResult['SAMO_DELIVERY_PRICE'].' рублей')?>
								</div>
							</label>
						</div>
					</div>
				
				<? }
				
			// Доставка курьером до метро
			} else if( $variant == 'CMETRO' ){
		
				if( $arResult['CMETRO_CHECK'] == 'Y' ){ ?>
				
					<div class="line">
						<div class="field">
							<input type="radio" <? if( param_post('ds') == $arResult['CMETRO_DS_ID'] ){ ?>checked<? } ?> name="ds" id="ds_<?=$arResult['CMETRO_DS_ID'] ?>" price="<?=$arResult['CMETRO_DELIVERY_PRICE']?$arResult['CMETRO_DELIVERY_PRICE']:''?>" value="<?=$arResult['CMETRO_DS_ID']?>">
							<label for="ds_<?=$arResult['CMETRO_DS_ID'] ?>" >
								<div class="name"><?=$arResult['CMETRO_DS_NAME']?></div>
								<div class="desc">
									Стоимость доставки: <?=$arResult['CMETRO_DELIVERY_PRICE']==0?'бесплатно':($arResult['CMETRO_DELIVERY_PRICE'].' рублей')?>
								</div>
							</label>
						</div>
					</div>
				
				<? }
				
			// Доставка курьером до адреса
			} else if( $variant == 'CADDRESS' ){
		
				if( $arResult['CADDRESS_CHECK'] == 'Y' ){ ?>
				
					<div class="line">
						<div class="field">
							<input type="radio" <? if( param_post('ds') == $arResult['CADDRESS_DS_ID'] ){ ?>checked<? } ?> name="ds" id="ds_<?=$arResult['CADDRESS_DS_ID'] ?>" price="<?=$arResult['CADDRESS_DELIVERY_PRICE']?$arResult['CADDRESS_DELIVERY_PRICE']:''?>" value="<?=$arResult['CADDRESS_DS_ID']?>">
							<label for="ds_<?=$arResult['CADDRESS_DS_ID'] ?>" >
								<div class="name"><?=$arResult['CADDRESS_DS_NAME']?></div>
								<div class="desc">
									Стоимость доставки: <?=$arResult['CADDRESS_DELIVERY_PRICE']==0?'бесплатно':($arResult['CADDRESS_DELIVERY_PRICE'].' рублей')?>
								</div>
							</label>
						</div>
					</div>
				
				<? }
				
			// Доставка курьером до адреса за МКАД
			} else if( $variant == 'CMKAD' ){
		
				if( $arResult['CMKAD_CHECK'] == 'Y' ){ ?>
				
					<div class="line">
						<div class="field">
							<input type="radio" <? if( param_post('ds') == $arResult['CMKAD_DS_ID'] ){ ?>checked<? } ?> name="ds" id="ds_<?=$arResult['CMKAD_DS_ID'] ?>" price="<?=$arResult['CMKAD_DELIVERY_PRICE']?$arResult['CMKAD_DELIVERY_PRICE']:''?>" value="<?=$arResult['CMKAD_DS_ID']?>">
							<label for="ds_<?=$arResult['CMKAD_DS_ID'] ?>" >
								<div class="name"><?=$arResult['CMKAD_DS_NAME']?></div>
								<div class="desc">
									Стоимость доставки: <?=$arResult['CMKAD_DELIVERY_PRICE']==0?'бесплатно':($arResult['CMKAD_DELIVERY_PRICE'].' рублей')?>
								</div>
							</label>
						</div>
					</div>
				
				<? }
		
		
			// Почта России
			} else if( $variant == 'POCHTA' ){
				
				if( $arResult['POCHTA_CHECK'] == 'Y' ){ ?>
				
					<div class="line">
						<div class="field">
							<input type="radio" <? if( param_post('ds') == $arResult['POCHTA_DS_ID'] ){ ?>checked<? } ?> name="ds" id="ds_<?=$arResult['POCHTA_DS_ID'] ?>" price="<?=$arResult['POCHTA_DELIVERY_PRICE']?$arResult['POCHTA_DELIVERY_PRICE']:''?>" value="<?=$arResult['POCHTA_DS_ID']?>">
							<label for="ds_<?=$arResult['POCHTA_DS_ID'] ?>" >
								<div class="name">Почта России</div>
								<div class="desc">
									Стоимость доставки: <?=$arResult['POCHTA_DELIVERY_PRICE']?> рублей
									<!--<br><i>(рассчитывается исходя из <u>почтового индекса</u>)</i>-->
								</div>
							</label>
						</div>
					</div>
				
				<? }
		
			// СДЭК
			} else if( $variant == 'SDEK' ){

				if( $arResult['SDEK_CHECK'] == 'Y' ){ ?>
			
					<div class="line">
						<div class="field">
							<input type="radio" <? if( param_post('ds') == $arResult['SDEK_DS_ID'] ){ ?>checked<? } ?> name="ds" id="ds_<?=$arResult['SDEK_DS_ID']?>" value="<?=$arResult['SDEK_DS_ID']?>" price="<?=$arResult['SDEK_DELIVERY_PRICE']['price']?$arResult['SDEK_DELIVERY_PRICE']['price']:''?>">
							<label for="ds_<?=$arResult['SDEK_DS_ID']?>">
								<div class="name">СДЭК</div>
								<? if( $arResult['SDEK_DELIVERY_PRICE']['price'] ){ ?>
									<div class="desc">
										Стоимость доставки: &nbsp; <?=$arResult['SDEK_DELIVERY_PRICE']['price']?> рублей
										<!--<br><i>(рассчитывается исходя из <u>населённого пункта</u>)</i>-->
										<?=$arResult['SDEK_DELIVERY_PERIOD']?> <?=$arResult['made_terms']?>
									</div>
								<? } ?>
							</label>
						</div>
					</div>
			
				<? }

			// Boxberry курьер
			} else if( $variant == 'BOXBERRY_COURIER' ){
				
				if( $arResult['BOXBERRY_CHECK_COURIER'] == 'Y' ){ ?>
				
					<div class="line">
					
						<div class="field">
						
							<input <? if( param_post('ds') == $arResult['BOXBERRY_COURIER_DS_ID'] ){ ?>checked<? } ?> type="radio" name="ds" id="ds_<?=$arResult['BOXBERRY_COURIER_DS_ID']?>" value="<?=$arResult['BOXBERRY_COURIER_DS_ID']?>" price="<?=$arResult['BOXBERRY_COURIER_DELIVERY_PRICE']['price']?$arResult['BOXBERRY_COURIER_DELIVERY_PRICE']['price']:''?>">
							
							<label for="ds_<?=$arResult['BOXBERRY_COURIER_DS_ID']?>">
								<div class="name">Cлужба доставки Boxberry до двери </div>
								<? if( $arResult['BOXBERRY_COURIER_DELIVERY_PRICE']['price'] ){ ?>
									<div class="desc">
										Стоимость доставки: &nbsp; <?=$arResult['BOXBERRY_COURIER_DELIVERY_PRICE']['price']?> рублей
										<!--<br><i>(рассчитывается исходя из <u>почтового индекса</u>)</i>-->
										<? if( $arResult['BOXBERRY_COURIER_DELIVERY_PRICE']['delivery_period'] ){ ?>
											<br> Сроки доставки, дней: &nbsp; <?=$arResult['BOXBERRY_COURIER_DELIVERY_PRICE']['delivery_period']?> <?=$arResult['made_terms']?>
										<? } ?>
									</div>
								<? } ?>
							</label>
							
						</div>
					</div>
					
				<? }

			// Boxberry ПВЗ
			} else if( $variant == 'BOXBERRY_PVZ' ){
				
				if( $arResult['BOXBERRY_CHECK_PVZ'] == 'Y' ){ ?>
				
					<div class="line">
					
						<input type="hidden" name="order_pvz_code" value="<?=param_post('order_pvz_code')?>">
						<input type="hidden" name="order_pvz_address" value="<?=param_post('order_pvz_address')?>">
						<input type="hidden" name="order_pvz_price" value="<?=param_post('order_pvz_price')?>">
						<input type="hidden" name="order_pvz_period" value="<?=param_post('order_pvz_period')?>">
					
						<div class="field">
						
							<input type="radio" class="pvz___input" <? if( param_post('ds') == $arResult['BOXBERRY_PVZ_DS_ID'] ){ ?>checked<? } ?> name="ds" id="ds_<?=$arResult['BOXBERRY_PVZ_DS_ID']?>" value="<?=$arResult['BOXBERRY_PVZ_DS_ID']?>" price="<?=$arResult['BOXBERRY_PVZ_DELIVERY_PRICE']['price']?$arResult['BOXBERRY_PVZ_DELIVERY_PRICE']['price']:''?>">
							
							<label for="ds_<?=$arResult['BOXBERRY_PVZ_DS_ID']?>">
								<div class="name">
									Cлужба доставки Boxberry до пункта выдачи
									<div class="tooltip">
										<div class="icon">?</div>
										<div class="text">
											Выберите ПВЗ на карте
										</div>
									</div>
								</div>
								<div class="desc pvz___desc"><?
									if( strlen(param_post('order_pvz_address')) > 0 ){
										echo 'Выбран пункт выдачи: &nbsp; <strong>'.param_post('order_pvz_address').'</strong>';
									}
									if( strlen(param_post('order_pvz_price')) > 0 ){
										echo '<br>Стоимость доставки: &nbsp; '.param_post('order_pvz_price').' руб.';
									}
									if( strlen(param_post('order_pvz_period')) > 0 ){
										echo '<br>Сроки доставки, дней: &nbsp; '.param_post('order_pvz_period').$arResult['made_terms'];
									}
								?></div>
							</label>
							
						</div>
						
					</div>
					
					
					<div id="map" style="display:none"></div>
					
					
					<script>
					if( $('input[name=ds]').length == 1 ){
						$('input[name=ds]').eq(0).prop('checked', true);
					}
					ymaps.ready(map_init);
					</script>

				<? }

			}
			
		} ?>
			
		</div>
		
	<? } else { ?>
	
		<div class="title" style="margin: 20px 0 30px 0; color: #ff8100;"><i>Нет подходящих вариантов доставки в указанный населённый пункт</i></div>
	
	<? } ?>

</div>

