<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); 

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if( intval( $arParams['loc_id'] ) > 0 ){
	
	$arResult['DS_CNT'] = 0;
	$arResult['DELIVERY_VARIANTS'] = array();
	

	$arResult['DELIVERY_PRICE'] = false;

	$arResult['LOCATION_ID'] = $arParams['loc_id'];
	
	$res = \Bitrix\Sale\Location\LocationTable::getList(array(
		'filter' => array(
			'ID' => $arResult['LOCATION_ID'],
			'=NAME.LANGUAGE_ID' => 'ru',
			'=PARENT.NAME.LANGUAGE_ID' => 'ru'
		),
		'select' => array(
			'*', 'NAME_RU' => 'NAME.NAME', 'TYPE_CODE' => 'TYPE.CODE', 'PARENT_NAME_RU' => 'PARENT.NAME.NAME', 'ZIP' => 'EXTERNAL.XML_ID'
		),
		'limit' => 1
	));
	
	if( $loc = $res->fetch() ){
		
		
		$arResult['made_terms'] = ' (плюс 1-3 дня на изготовление)';
		
		
		$basketParams = basketParams();
		

		$arResult['arUser'] = false;
		if( $USER->IsAuthorized() ){
			$arResult['arUser'] = tools\user::info( $USER->GetID() );
		}


		$arResult['LOCATION_ZIP'] = $arParams['zip']?$arParams['zip']:$loc['ZIP'];
		
		
		
		// Самовывоз
		$db_dtype = \CSaleDelivery::GetList(
			Array("SORT"=>"ASC", "NAME"=>"ASC"),
			Array(
				"ACTIVE"=>"Y",
				"LID" => SITE_ID,
				"ID" => 2,
				"LOCATION" => $loc['ID']
			), false, false, array()
		);
		while ($ds = $db_dtype->GetNext()){
			
			$arResult['SAMO_DS_ID'] = $ds['ID'];
			
			$arResult['SAMO_CHECK'] = 'Y';
			$arResult['DS_CNT']++;
			
			$arResult['SAMO_DELIVERY_PRICE'] = $ds['PRICE'];
			$arResult['DELIVERY_VARIANTS']['SAMO'] = $ds['PRICE'];
			
			if( param_post('ds') == $arResult['SAMO_DS_ID'] ){
				$arResult['DELIVERY_PRICE'] = $ds['PRICE'];
			}
			
		}
		
		
		
		// Доставка курьером до метро
		$db_dtype = \CSaleDelivery::GetList(
			Array("SORT"=>"ASC", "NAME"=>"ASC"),
			Array(
				"ACTIVE"=>"Y",
				"LID" => SITE_ID,
				"ID" => 7,
				"LOCATION" => $loc['ID']
			), false, false, array()
		);
		while ($ds = $db_dtype->GetNext()){
			$arResult['CMETRO_DS_ID'] = $ds['ID'];
			$arResult['CMETRO_DS_NAME'] = $ds['NAME'];
			$arResult['CMETRO_CHECK'] = 'Y';
			$arResult['DS_CNT']++;
			$arResult['CMETRO_DELIVERY_PRICE'] = $ds['PRICE'];
			$arResult['DELIVERY_VARIANTS']['CMETRO'] = $ds['PRICE'];
			if( param_post('ds') == $arResult['CMETRO_DS_ID'] ){
				$arResult['DELIVERY_PRICE'] = $ds['PRICE'];
			}
		}
		
		
		
		// Доставка курьером до адреса
		$db_dtype = \CSaleDelivery::GetList(
			Array("SORT"=>"ASC", "NAME"=>"ASC"),
			Array(
				"ACTIVE"=>"Y",
				"LID" => SITE_ID,
				"ID" => 8,
				"LOCATION" => $loc['ID']
			), false, false, array()
		);
		while ($ds = $db_dtype->GetNext()){
			$arResult['CADDRESS_DS_ID'] = $ds['ID'];
			$arResult['CADDRESS_DS_NAME'] = $ds['NAME'];
			$arResult['CADDRESS_CHECK'] = 'Y';
			$arResult['DS_CNT']++;
			$arResult['CADDRESS_DELIVERY_PRICE'] = $ds['PRICE'];
			$arResult['DELIVERY_VARIANTS']['CADDRESS'] = $ds['PRICE'];
			if( param_post('ds') == $arResult['CADDRESS_DS_ID'] ){
				$arResult['DELIVERY_PRICE'] = $ds['PRICE'];
			}
		}
		
		
		
		// Доставка курьером до адреса за МКАД
		$db_dtype = \CSaleDelivery::GetList(
			Array("SORT"=>"ASC", "NAME"=>"ASC"),
			Array(
				"ACTIVE"=>"Y",
				"LID" => SITE_ID,
				"ID" => 9,
				"LOCATION" => $loc['ID']
			), false, false, array()
		);
		while ($ds = $db_dtype->GetNext()){
			$arResult['CMKAD_DS_ID'] = $ds['ID'];
			$arResult['CMKAD_DS_NAME'] = $ds['NAME'];
			$arResult['CMKAD_CHECK'] = 'Y';
			$arResult['DS_CNT']++;
			$arResult['CMKAD_DELIVERY_PRICE'] = $ds['PRICE'];
			$arResult['DELIVERY_VARIANTS']['CMKAD'] = $ds['PRICE'];
			if( param_post('ds') == $arResult['CMKAD_DS_ID'] ){
				$arResult['DELIVERY_PRICE'] = $ds['PRICE'];
			}
		}
		
		
		
		$start_time = time();
		
		
		
		// Почта России
		//echo '<p>Почта России старт - 0</p><br>';
		$arResult['POCHTA_CHECK'] = 'N';
		if( 
			strlen($arResult['LOCATION_ZIP']) > 0
			&&
			// Если в корзине нет кружки
			$basketParams['hasCup'] != 'Y'
		){
			
			$db_dtype = \CSaleDelivery::GetList(
				Array("SORT"=>"ASC", "NAME"=>"ASC"),
				Array(
					"ACTIVE"=>"Y",
					"LID" => SITE_ID,
					"ID" => 6
				), false, false, array()
			);
			while ($ds = $db_dtype->GetNext()){
				
				$arResult['POCHTA_DS_ID'] = $ds['ID'];
				
				// Расчёт стоимости доставки
				$arResult['POCHTA_DELIVERY_PRICE'] = project\deliv_pochta::calcDeliveryPrice( $arResult['LOCATION_ZIP'], $basketParams );
				
				if( $arResult['POCHTA_DELIVERY_PRICE'] ){
					$arResult['POCHTA_CHECK'] = 'Y';
					$arResult['DS_CNT']++;
					
					$arResult['DELIVERY_VARIANTS']['POCHTA'] = $arResult['POCHTA_DELIVERY_PRICE'];
				}
				
			}
			
		}

		
		
		// СДЭК
		//echo '<p>СДЭК старт - '.(time()-$start_time).'</p><br>';
		$arResult['SDEK_CHECK'] = 'N';
		$sdekLocations = project\deliv_sdek::search( $loc['NAME_RU'], false, true );
		if( count($sdekLocations) > 0 ){
			
			$sdekLoc = $sdekLocations[0];
			
			$arResult['SDEK_LOC_ID'] = $sdekLoc['sdek_loc_id'];
		
			// sdek
			$db_dtype = \CSaleDelivery::GetList(
				Array("SORT"=>"ASC", "NAME"=>"ASC"),
				Array(
					"ACTIVE"=>"Y",
					"LID" => SITE_ID,
					"ID" => 3
				), false, false, array()
			);
			while ($ds = $db_dtype->GetNext()){
				
				$arResult['SDEK_DS_ID'] = $ds['ID'];
				
				// Расчёт стоимости доставки
				$arResult['SDEK_DELIVERY_PRICE'] = project\deliv_sdek::calcDeliveryPrice( $arResult['SDEK_LOC_ID'], $basketParams );
				
				$arResult['SDEK_DELIVERY_PERIOD'] = '';
				
				if( $arResult['SDEK_DELIVERY_PRICE']['price'] ){
					$arResult['SDEK_CHECK'] = 'Y';
					$arResult['DS_CNT']++;
					
					$arResult['DELIVERY_VARIANTS']['SDEK'] = $arResult['SDEK_DELIVERY_PRICE']['price'];
					
					if( param_post('ds') == $arResult['SDEK_DS_ID'] ){
						$arResult['DELIVERY_PRICE'] = $arResult['SDEK_DELIVERY_PRICE']['price'];
					}
					if( 
						$arResult['SDEK_DELIVERY_PRICE']['deliveryPeriodMin']
						||
						$arResult['SDEK_DELIVERY_PRICE']['deliveryPeriodMax']
					){
						$ar = array();
						if( $arResult['SDEK_DELIVERY_PRICE']['deliveryPeriodMin'] ){
							$ar[] = $arResult['SDEK_DELIVERY_PRICE']['deliveryPeriodMin'];
						}
						if( $arResult['SDEK_DELIVERY_PRICE']['deliveryPeriodMax'] ){
							$ar[] = $arResult['SDEK_DELIVERY_PRICE']['deliveryPeriodMax'];
						}
						if( $ar[1] && $ar[1] == $ar[0] ){
							$arResult['SDEK_DELIVERY_PERIOD'] = '<br> Сроки доставки, дней: &nbsp; '.$ar[0];
						} else {
							$arResult['SDEK_DELIVERY_PERIOD'] = '<br> Сроки доставки, дней: &nbsp; '.implode(' - ', $ar);
						}
					}
				}
			}
		}
		
		
		
		// BOXBERRY
		//echo '<p>BOXBERRY старт - '.(time()-$start_time).'</p><br>';
		$arResult['BOXBERRY_CHECK_COURIER'] = 'N';
		$arResult['BOXBERRY_CHECK_PVZ'] = 'N';
		$boxberryLocations = project\deliv_boxberry::search( $loc['NAME_RU'], true );
		if( 
			count($boxberryLocations) > 0
			&&
			strlen($arResult['LOCATION_ZIP']) > 0
		){
			$boxberryLoc = $boxberryLocations[0];
			if(
				strtolower( trim( $boxberryLoc['name_ru'] ) )
				==
				strtolower( trim( $loc['NAME_RU'] ) )
			){
				
				$arResult['BOXBERRY_LOC_ID'] = $boxberryLoc['boxberry_loc_id'];
				
				// boxberry курьер
				$db_dtype = \CSaleDelivery::GetList(
					Array("SORT"=>"ASC", "NAME"=>"ASC"),
					Array(
						"ACTIVE"=>"Y",
						"LID" => SITE_ID,
						"ID" => 4
					), false, false, array()
				);
				while ($ds = $db_dtype->GetNext()){
					$arResult['BOXBERRY_COURIER_DS_ID'] = $ds['ID'];
					// Проверка возможности курьерской доставки
					$arResult['BOXBERRY_CHECK_COURIER'] = project\deliv_boxberry::checkCourier( $arResult['LOCATION_ZIP'] );
					if( $arResult['BOXBERRY_CHECK_COURIER'] == 'Y' ){
						// Расчёт стоимости доставки
						$arResult['BOXBERRY_COURIER_DELIVERY_PRICE'] = project\deliv_boxberry::calcCourierPrice( $arResult['LOCATION_ZIP'], $basketParams );
						$arResult['DS_CNT']++;
						
						$arResult['DELIVERY_VARIANTS']['BOXBERRY_COURIER'] = $arResult['BOXBERRY_COURIER_DELIVERY_PRICE']['price'];
						
						if( param_post('ds') == $arResult['BOXBERRY_COURIER_DS_ID'] ){
							$arResult['DELIVERY_PRICE'] = $arResult['BOXBERRY_COURIER_DELIVERY_PRICE']['price'];
						}
					}
				}
			
				// boxberry ПВЗ
				$db_dtype = \CSaleDelivery::GetList(
					Array("SORT"=>"ASC", "NAME"=>"ASC"),
					Array(
						"ACTIVE"=>"Y",
						"LID" => SITE_ID,
						"ID" => 5
					), false, false, array()
				);
				while ($ds = $db_dtype->GetNext()){
					
					$arResult['BOXBERRY_PVZ_DS_ID'] = $ds['ID'];
					
					// Проверка наличие точек ПВЗ
					$arResult['BOXBERRY_PVZ_LIST'] = project\deliv_boxberry::checkPVZ( $boxberryLoc['boxberry_loc_id'] );
					
					if( 
						$arResult['BOXBERRY_PVZ_LIST']
						&&
						is_array($arResult['BOXBERRY_PVZ_LIST'])
						&&							
						count($arResult['BOXBERRY_PVZ_LIST']) > 0
						&&
						!$arResult['BOXBERRY_PVZ_LIST'][0]['err']
					){
						foreach($arResult['BOXBERRY_PVZ_LIST'] as $key => $pvz){
							if( strlen($pvz['GPS']) > 0 ){} else {
								unset($arResult['BOXBERRY_PVZ_LIST'][$key]);
							}
						}
						if( count($arResult['BOXBERRY_PVZ_LIST']) > 0 ){
							$arResult['BOXBERRY_CHECK_PVZ'] = 'Y';
							$arResult['DS_CNT']++;
							
							$arResult['DELIVERY_VARIANTS']['BOXBERRY_PVZ'] = 1000000000;
							
							if( param_post('ds') == 5 && strlen(param_post('order_pvz_price')) > 0 ){
								$arResult['DELIVERY_PRICE'] = param_post('order_pvz_price');
								$arResult['DELIVERY_VARIANTS']['BOXBERRY_PVZ'] = $arResult['DELIVERY_PRICE'];
							}
						}
					}
				}
			}
		}
		
		
		
		//echo '<p>финиш - '.(time()-$start_time).'</p><br>';

		
		
	}
	
	asort($arResult['DELIVERY_VARIANTS']);
	
}


$this->IncludeComponentTemplate();