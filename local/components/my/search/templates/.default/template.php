<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php"); ?>

<div id="app_test">

	<section style="display: none" :class="{nn_illustrator:true, active: info.metadata && info.metadata.illustrator}" v-if="info.metadata && info.metadata.illustrator">
		<div class="cont">
			<div class="nn_illustrator__wrap">
			<figure>
				<img :src="`http://new.niti-niti.ru/imgen/a:76,b:76,f:100,k:f8f8f8,c:${info.metadata.illustrator.picture_id}.jpg`" :alt="info.metadata.illustrator.name">
				<figcaption>{{ info.metadata.illustrator.name }}</figcaption>
			</figure>
			<div class="nn_illustrator__info">
				<div class="nn_illustrator__info_item">Россия, Москва</div>
				<div class="nn_illustrator__dot"></div>
				<div class="nn_illustrator__info_item">{{ info.metadata.illustrator.prints_count }} иллюстраций</div>
<!--				<div class="nn_illustrator__dot"></div>-->
<!--				<div class="nn_illustrator__info_item active">Посмотреть профиль</div>-->
			</div>
		</div>
			<button class="nn_illustrator__share">Поделиться</button>
		</div>
	</section>

	<section :class="{nn_illustration: true, active: info.metadata && info.metadata.print}" style="background-color: #fef8ec; display: none" v-if="info.metadata && info.metadata.print">
		<div class="cont">
			<div class="nn_illustration__author">
			<a class="nn_illustration__author_back_btn" :href="`/illustrators/${info.metadata.print.illustrator.id}/`">
				<div class="nn_illustration__author_back">
					<svg xmlns="http://www.w3.org/2000/svg" height="300px" width="300px" version="1.1" baseProfile="tiny" x="0px" y="0px" viewBox="0 0 100 100" xml:space="preserve"><g><polygon fill-rule="evenodd" points="27,84.641 62.219,50 27,15.359 32.439,10 73,50.02 32.439,90  "></polygon></g></svg>
				</div>
				<div class="nn_illustration__author_icon">
					<img :src="`http://new.niti-niti.ru/imgen/a:76,b:76,f:100,k:f8f8f8,c:${info.metadata.print.illustrator.picture_id}.jpg`" :alt="info.metadata.print.illustrator.name">
				</div>
				<div class="nn_illustration__author_text">
					<h3>{{ info.metadata.print.illustrator.name }}</h3>
					<p>{{ info.metadata.print.illustrator.prints_count }} иллюстраций</p>
				</div>
			</a>
		</div>
			<div class="nn_illustration__wrap">
			<div class="nn_illustration__picture">
				<img :src="`http://new.niti-niti.ru/imgen/a:600,b:600,f:100,k:f8f8f8,c:${info.metadata.print.picture_id}.jpg`" :alt="info.metadata.print.illustrator.name">
			</div>
			<div class="nn_illustration__content">
				<div class="nn_illustration__text">
					<h1>{{ info.metadata.print.name }}</h1>
					<p class="nn_illustration__author_name">{{ info.metadata.print.illustrator.name }}</p>
					<p>Описание принта.<br>
						Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский набор слов, но это не совсем так. Его корни уходят в один фрагмент классической латыни 45 года н.э., то есть более двух тысячелетий назад.
					</p>
				</div>
				<div class="nn_illustration__buttons">
					<button class="nn_illustration__like">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="-3 -3 30 30">
							<path data-v-76108a6c="" d="M12 4.96l-.975-.975A6.359 6.359 0 0 0 6.5 2.1C2.965 2.1.1 4.983.1 8.535c0 1.758.607 2.95 2.238 4.59l8.05 8.104c.433.435 1.012.671 1.612.671.6 0 1.179-.236 1.611-.67l8.03-8.082c1.627-1.638 2.259-2.865 2.259-4.613 0-3.552-2.865-6.435-6.4-6.435a6.361 6.361 0 0 0-4.527 1.888l.002-.003L12 4.96zm5.5-1.06c2.539 0 4.6 2.074 4.6 4.635 0 1.208-.415 2.014-1.737 3.344l-8.029 8.082A.466.466 0 0 1 12 20.1a.466.466 0 0 1-.334-.14l-8.051-8.103C2.292 10.525 1.9 9.757 1.9 8.535 1.9 5.975 3.961 3.9 6.5 3.9c1.236 0 2.393.493 3.25 1.356l1.614 1.613.636.636 2.248-2.247A4.564 4.564 0 0 1 17.5 3.9z"></path>
						</svg>
					</button>
					<button class="nn_illustration__share">Поделиться</button>
				</div>
			</div>
		</div>
		</div>
	</section>

	<div class="cont">

		<modal name='size-title' height="auto" :scrollable="true" :width="437" :adaptive="true">

			sdf

		</modal>

		<appmodaldialog :is-open="isOpenFiltersModal"
										type="headerAction"
										name="Фильтры"
										name-button-action="Сбросить"
										@action-button="resetAllFilters"
										@modalclose="closeFiltersModal">

			<template v-slot:body>
				<app-filters
								v-if="info.filters"
								:filters="info.filters"
								:loading="loading"
								@search="search"
								@close="closeFiltersModal"
								:is-mobile="true"
				></app-filters>
			</template>

		</appmodaldialog>

		<section class="category_info">

			<aside class="left aside_filters">

				<app-filters
								v-if="info.filters"
								:filters="info.filters"
								:loading="loading"
								@search="search"
				></app-filters>

			</aside>



			<section class="content right" v-if="info.metadata && info.filters">

				<section style="display: none" :class="{'nn_collection':true, active: info.metadata && info.metadata.collection }" v-if="info.metadata && info.metadata.collection">
					<div class="nn_collection__wrap" style="background-color: #C49E7A">

						<div class="nn_collection__text" style="color: #fff;">
							<h1>{{ info.metadata.collection.name }}</h1>
							<p>Оригинальные иллюстрации независимых художников</p>
						</div>
						<div class="nn_collection__picture" style="background-image: url('http://new.niti-niti.ru/images/dad-hero.jpg')">

						</div>
					</div>
				</section>

				<div class="cat_head">

					<div class="sort_buttons--mob nn_sort_mobile_search">
						<button class="sort_buttons--mob__btn_filters" @click="openFiltersModal">
							<span class="icon">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor">
									<path d="M3,10h9.18a3,3,0,0,0,5.63,0H21a1,1,0,0,0,0-2H17.8a3,3,0,0,0-5.63,0H3a1,1,0,0,0,0,2ZM15,8a1,1,0,1,1-1,1A1,1,0,0,1,15,8Z" ></path>
									<path d="M21,14H11.8a3,3,0,0,0-5.63,0H3a1,1,0,0,0,0,2H6.17a3,3,0,0,0,5.63,0H21a1,1,0,0,0,0-2ZM9,16a1,1,0,1,1,1-1A1,1,0,0,1,9,16Z"></path>
								</svg>
							</span>
							<span class="name">Фильтры</span>
						</button>
						<div class="sort_buttons--mob__btn_wrap">
							<button class="sort_buttons--mob__btn" data-row_cnt="2">
											<span class="sort_buttons--mob__span">
											<svg class="sort_buttons--mob__svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
												<path d="M13,13v9h7a2,2,0,0,0,2-2V13H13Z"></path>
												<path d="M20,2H13v9h9V4A2,2,0,0,0,20,2Z"></path>
												<path d="M4,2A2,2,0,0,0,2,4v7h9V2H4Z"></path>
												<path d="M2,13v7a2,2,0,0,0,2,2h7V13H2Z"></path>
											</svg>
								</span>
						</button>
							<button class="sort_buttons--mob__btn" data-row_cnt="1">
											<span class="sort_buttons--mob__span">
											<svg class="sort_buttons--mob__svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor">
												<g>
													<path d="M13,22H3a1,1,0,0,1,0-2H13a1,1,0,0,1,0,2Z"></path>
													<path d="M20,18H4a2,2,0,0,1-2-2V4A2,2,0,0,1,4,2H20a2,2,0,0,1,2,2V16A2,2,0,0,1,20,18ZM4,4V16H20V4Z"></path>
												</g>
											</svg>
												</span>
						</button>
						</div>
					</div>

					<div class="nn_tag_buttons nn_tag_buttons--mob" v-if="info.filters.resets.length > 0">
						<button class="nn_tag_btn" v-for="item in info.filters.resets" @click="go(item.link)">
							<span>{{item.label}}</span>
							<span class="nn_tag_btn__svg">
								<svg data-v-2eaa29a2="" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="300px" width="300px" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve"><rect data-v-2eaa29a2="" width="126.4" height="15" transform="translate(10.60) rotate(45)"></rect><rect data-v-2eaa29a2="" width="126.4" height="15" transform="translate(0 89.39) rotate(-45)"></rect></svg>
							</span>
						</button>
						<button class="nn_tag_clear_btn" @click="go(info.filters.resetUrl)">Сбросить все фильтры</button>
					</div>

					<div class="search_result" style="display: none">

						<div class="search_box">
							<div class="search_box__wrap">
								<div class="search_box__form_wrap">
									<form :action="info.withoutQueryURL" class="search_box__form">
										<div class="search_box__input_wrap">
											<input name="query" :value="info.query" class="search_box__input">
										</div>
										<button type="button" class="search_box__close_btn">
								<span class="search_box__close_svg_wrap">
									<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 24 24">
										<path d="M21.71 20.29l-5.4-5.39A7.92 7.92 0 0 0 18 10a8 8 0 1 0-8 8 7.92 7.92 0 0 0 4.9-1.69l5.39 5.4a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42zM4 10a6 6 0 1 1 6 6 6 6 0 0 1-6-6z">
										</path>
									</svg>
								</span>
										</button>
										<button type="submit" class="search_box__search_btn">
								<span class="search_box__search_btn_svg_wrap">
									<svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 24 24">
										<path d="M21.71 20.29l-5.4-5.39A7.92 7.92 0 0 0 18 10a8 8 0 1 0-8 8 7.92 7.92 0 0 0 4.9-1.69l5.39 5.4a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42zM4 10a6 6 0 1 1 6 6 6 6 0 0 1-6-6z">
										</path>
									</svg>
								</span>
										</button>
									</form>
								</div>
							</div>
							<div class="search_modal">
								<ul class="search_modal__list">
									<li class="search_modal__item">
										<div class="search_modal__icon">
											<svg class="svg_search" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M21.71 20.29l-5.4-5.39A7.92 7.92 0 0 0 18 10a8 8 0 1 0-8 8 7.92 7.92 0 0 0 4.9-1.69l5.39 5.4a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42zM4 10a6 6 0 1 1 6 6 6 6 0 0 1-6-6z"></path>
											</svg>
										</div>
										<div class="search_modal__text">собака</div>
									</li>
									<li class="search_modal__item">
										<div class="search_modal__icon">
											<svg class="svg_chart" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M16.85 6.85l1.44 1.44-4.88 4.88-3.29-3.29c-.39-.39-1.02-.39-1.41 0l-6 6.01c-.39.39-.39 1.02 0 1.41.39.39 1.02.39 1.41 0L9.41 12l3.29 3.29c.39.39 1.02.39 1.41 0l5.59-5.58 1.44 1.44c.31.31.85.09.85-.35V6.5c.01-.28-.21-.5-.49-.5h-4.29c-.45 0-.67.54-.36.85z"></path>
											</svg>
										</div>
										<div class="search_modal__text">собака-улыбака</div>
									</li>
									<li class="search_modal__item">
										<div class="search_modal__picture">
											<img src="https://ih0.redbubble.net/avatar.2183682.140x140.jpg">
										</div>
										<div class="search_modal__text">собака цветная</div>
									</li>
									<li class="search_modal__item">
										<button class="search_modal__show_result">
											<div class="search_modal__show_result_text">
												<span>Показать результаты для &nbsp;</span>
												<span class="search_modal__result">собака</span>
											</div>
											<div class="search_modal__show_result_icon">
												<svg xmlns="http://www.w3.org/2000/svg" height="300px" width="300px" version="1.1" baseProfile="tiny" x="0px" y="0px" viewBox="0 0 100 100" xml:space="preserve"><g><polygon fill-rule="evenodd" points="27,84.641 62.219,50 27,15.359 32.439,10 73,50.02 32.439,90  "></polygon></g>
												</svg>
											</div>
										</button>
									</li>
								</ul>
							</div>
						</div>

						<div class="search_result__section" v-if="info.collections.length > 0">

								<button class="search_result__slide_btn left" disabled>
									<svg xmlns="http://www.w3.org/2000/svg" height="300px" width="300px" version="1.1" baseProfile="tiny" x="0px" y="0px" viewBox="0 0 100 100" xml:space="preserve" class="collapsible_btn"><g><polygon fill-rule="evenodd" points="27,84.641 62.219,50 27,15.359 32.439,10 73,50.02 32.439,90  "></polygon></g></svg>
								</button>
								<button class="search_result__slide_btn right">
									<svg xmlns="http://www.w3.org/2000/svg" height="300px" width="300px" version="1.1" baseProfile="tiny" x="0px" y="0px" viewBox="0 0 100 100" xml:space="preserve" class="collapsible_btn"><g><polygon fill-rule="evenodd" points="27,84.641 62.219,50 27,15.359 32.439,10 73,50.02 32.439,90  "></polygon></g></svg>
								</button>

								<div class="search_result__row ">

									<div class="search_result__slider">

										<a class="search_result__button" v-for="item in info.collections" :href="item.link">
											<div class="search_result__button_name">{{item.name}}</div>
										</a>

										<a class="search_result__button" v-for="item in info.illustrators" :href="item.link">
											<div class="search_result__button_name">{{item.name}}</div>
										</a>

										<a class="search_result__button" v-for="item in info.prints" :href="item.link">
											<div class="search_result__button_name">{{item.name}}</div>
										</a>

									</div>

								</div>

							</div>

					</div>

					<div class="search_result_info">
						<h1 class="search_result_text__h1">Вы искали {{ info.metadata.title }}</h1>

						<div class="search_result_text__count">
							<span style="margin-left: 15px">{{ info.metadata.resultCount }} результатов</span>
						</div>

						<div class="limit right row_cnt_block">
							<!--					<div class="left">Показывать по:</div>-->
							<!--					<a style="cursor:pointer">3</a>-->
							<!--					<a style="cursor:pointer" class="active">4</a>-->
							<!--					<div class="clear"></div>-->
							<div class="nn_filter_select">
								<select @change="onChangeGo($event)" v-model="sortLink">
									<option
										v-for="item in info.sorts"
										:value="item.link">{{ item.label }}</option>
								</select>
								<div class="nn_filter_select__icon">
									<svg xmlns="http://www.w3.org/2000/svg" height="300px" width="300px" version="1.1" baseProfile="tiny" x="0px" y="0px" viewBox="0 0 100 100" xml:space="preserve"><g><polygon fill-rule="evenodd" points="27,84.641 62.219,50 27,15.359 32.439,10 73,50.02 32.439,90  "></polygon></g></svg>
								</div>
							</div>
						</div>
					</div>

					<div class="nn_tag_buttons nn_tag_buttons--desk" v-if="info.filters.resets.length > 0">
						<button class="nn_tag_btn" v-for="item in info.filters.resets" @click="go(item.link)">
							<span>{{item.label}}</span>
							<span class="nn_tag_btn__svg">
								<svg data-v-2eaa29a2="" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="300px" width="300px" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve"><rect data-v-2eaa29a2="" width="126.4" height="15" transform="translate(10.60) rotate(45)"></rect><rect data-v-2eaa29a2="" width="126.4" height="15" transform="translate(0 89.39) rotate(-45)"></rect></svg>
							</span>
						</button>
						<button class="nn_tag_clear_btn" @click="go(info.filters.resetUrl)">Сбросить все фильтры</button>
					</div>

				</div>

				<div class="products">
					<div class="grid catalog_load_block">

						<div class="item_wrap"
								 v-for="item in info.products">

							<div class="product product___block" id="bx_3966226736_750291" :tov_id="item.id">
								<a style="cursor:pointer" class="favorite_link fav___link to___process" :tov_id="item.id"></a>

								<div class="thumb">
									<a class="detail_page_url nn_preloader" :href="item.url">
										<img class="default_photo" :src="`http://new.niti-niti.ru/imgen/a:270,b:307,f:95,k:f8f8f8,c:${item.picture_id}.jpg`">
									</a>
								</div>

								<a href="javascript:;"
									 class="quike_view_link"
									 data-fancybox=""
									 data-src="/ajax/product.php?id=750291"
									 data-type="ajax">быстрый просмотр</a>

								<div class="name">

									<a class="detail_page_url" :href="item.url">{{item.name}}</a>

								</div>

								<div class="nn_price_block">

									<div class="price">{{item.price}} <span class="currency">:</span></div>

									<div class="price old___price">1 190 <span class="currency">:</span></div>

									<div class="nn-quick_view">
										<button class="quick_view__btn" :data-id="item.id">
											<span class="quick_view__icon"></span>
										</button>
									</div>

								</div>
							</div>

						</div>

					</div>
				</div>

				<div v-if="info.pagination" class="nn_pagination">

					<button :class="classesBtnPrevPage">
						<span>Назад</span>
						<span class="nn_pagination__button_icon">
							<svg xmlns="http://www.w3.org/2000/svg" height="300px" width="300px" version="1.1" baseProfile="tiny"
									 x="0px" y="0px" viewBox="0 0 100 100" xml:space="preserve">
								<g><polygon fill-rule="evenodd"
														points="27,84.641 62.219,50 27,15.359 32.439,10 73,50.02 32.439,90  "/></g>
							</svg>
						</span>
					</button>
					<div class="nn_pagination__info">
						Показано с {{ info.pagination.fromNumber }} по {{ info.pagination.toNumber }} из {{
						info.pagination.total }} строк
					</div>
					<button :class="classesBtnNextPage" @click="go(info.pagination.paginationLinks.nextPage, $event)">
						<span>Вперед</span>
						<span class="nn_pagination__button_icon">
							<svg xmlns="http://www.w3.org/2000/svg" height="300px" width="300px" version="1.1" baseProfile="tiny"
									 x="0px" y="0px" viewBox="0 0 100 100" xml:space="preserve">
								<g><polygon fill-rule="evenodd" points="27,84.641 62.219,50 27,15.359 32.439,10 73,50.02 32.439,90  "/></g>
							</svg>
						</span>
					</button>

				</div>

			</section>

		</section>

		<pre style="display: block;float: left; width: 100%">{{ info.filters.filters[5] }}</pre>

	</div>

</div>

<? include('filter.php'); ?>

<script>
var searchResults = <?=$arResult['response_json']?>;
new Vue({
	data: function () {
		return {
			isOpenFiltersModal: false,
			loading: false,
			info: {
				query: '',
				appliedCount: 0,
				filters: {
					appliedCount: 0
				},
				categories: [],
				products: [],
				pagination: null
			},
			sortLink: ''
		}
	},
	components: {
		appmodaldialog: appmodaldialog
	},
	computed: {
		classesBtnPrevPage:function(){

			let disabled = false;

			if(this.info.pagination.currentPage === 1){
				disabled = true
			}

			return {
				'nn_pagination__button': true,
				'back': true,
				'disabled': disabled
			}
		},
		classesBtnNextPage:function(){
			return {
				'nn_pagination__button': true,
				'forward': true,
				'disabled': this.info.pagination.pagesCount === this.info.pagination.currentPage
			}
		},
	},
	mounted: function () {
		if (typeof searchResults === 'object') {
			this.info = searchResults
		}
		console.log(this.info)

		this.init();

		// this.$modal.show("size-title")
		// axios
		// 	//.get('https://21cf5605-d81f-4fac-9f2c-0ed29cf5ce7d.mock.pstmn.io' + '/search')
		// 	.get('http://new.niti-niti.ru/test/?query=%D1%81%D0%B5%D1%80%D0%B4%D1%86%D0%B5')
		// 	//.then(response => (this.info = response.data))
		// 	.then(response => {
		// 	console.log(response)
		// 	this.info = response.data
		// }
	//)
	},
	methods: {
		init: function(){

			for(let i in this.info.sorts){

					if(this.info.sorts[i].applied === true){
						this.sortLink = this.info.sorts[i].link
					}

			}

		},
		setLocation: function (curLoc) {

			window.history.pushState('page2', 'Title', curLoc.replace(/[\?&]schema=[^&]+/, '').replace(/^&/, '?'));
		},
		search: function (url) {
			this.loading = true
			let vm = this
			console.log('search-url', url)

			this.info.products = []

			axios
				.get(url)
				.then(response => {
					console.log(response)
					this.info = response.data
					vm.loading = false
					vm.setLocation(url);
					this.init();
				})
		},
		go: function (url, event) {
			if (event) {
				event.preventDefault()
			}
			url = 'http://new.niti-niti.ru' + url
			this.search(url)
		},
		openFiltersModal: function () {
			document.querySelector('html').classList.add('fixed_html')
			this.isOpenFiltersModal = true

		},
		onChangeGo: function(){
			console.log('onChangeGo', this.sortLink)
			this.go(this.sortLink);
		},
		closeFiltersModal: function(){
			document.querySelector('html').classList.remove('fixed_html')
			this.isOpenFiltersModal = false
		},
		resetAllFilters: function(){
			this.closeFiltersModal();
			//console.log(this.info)
			//console.log(this.info.filters.staticFiltersReset)
			//alert(this.info.filters.staticFiltersReset)
			this.go(this.info.filters.staticFiltersReset);
		}
	}
}).$mount('#app_test')

</script>

<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>
