<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$arResult = project\search::request();

$arResult['response_json'] = json_encode($arResult['response'], JSON_HEX_TAG | JSON_NUMERIC_CHECK | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);

if( $_GET['schema'] == 'json' ){

    header('Content-Type: application/json');

    echo $arResult['response_json'];

} else {

    $this->IncludeComponentTemplate();

}
