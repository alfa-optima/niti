<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if( $arResult ){ ?>

	<div class="item">
	
		<div class="prints left"><b><?=$arResult['PRINTS_CNT']?></b> принтов</div>
		
		<div class="comments right"><b class="subscribes___cnt" illustrator="<?=$arResult['ID']?>"><?=$arResult['SUBSCRIBES_CNT']?></b> подписок</div>
		
		<div class="clear"></div>

		<div class="author">
		
			<div class="foto">
				<a href="/illustrators/<?=$arResult['ID']?>/">
					<? if( intval($arResult['PERSONAL_PHOTO']) > 0 ){ ?>
						<img src="<?=rIMGG($arResult['PERSONAL_PHOTO'], 5, 93, 97)?>">
					<? } else { ?>
						<div style="display:block; width:89px; height:97px; background-color:#f4f4f4; border-radius: 50%;"></div>
					<? } ?>
				</a>
				<div class="star illustr_subscribe_button to___process" illustrator="<?=$arResult['ID']?>">Подписаться</div>
			</div>

			<div class="name"><?=$arResult['NAME']?></div>
			
		</div>
		
		<div class="examples">
			
			<? if( count($arResult['PRINTS']) > 0 ){ 
				foreach ( $arResult['PRINTS'] as $print ){ ?>
			
					<div class="item_wrap">
						<a href="/print/<?=$print['ID']?>/" class="item">
							<? if( intval($print['PROPERTY_PHOTO_ID_VALUE']) > 0 ){ ?>
								<img style="width:auto; margin: 0 auto;" src="<?=rIMG($print['PROPERTY_PHOTO_ID_VALUE'], 5, 157, 187)?>">
							<? } else { ?>
								<img src="<?=SITE_TEMPLATE_PATH?>/images/no_print.jpg">
							<? } ?>
						</a>
					</div>
					
				<? }
			} ?>

		</div>
		
	</div>
	
<? } ?>