<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$arResult = false;

if( intval($arParams['id']) > 0 ){ 

	$illustrator = tools\user::info($arParams['id']);

	if( intval($illustrator['ID']) > 0 ){

		$arResult = $illustrator;
		
		$prints = project\illustrator::getPrints($arResult['ID']);
		
		$arResult['PRINTS'] = $prints;
		//shuffle($arResult['PRINTS']);
		$arResult['PRINTS'] = array_slice($arResult['PRINTS'], 0, 2);
		
		$arResult['PRINTS_CNT'] = count($prints);
		$arResult['PRINTS_CNT_WORD'] = pfCnt($arResult['PRINTS_CNT'], "принт", "принта", "принтов");
		
		$subscribes = project\i_subscribe::getListForIllustrator($arResult['ID']);
		$arResult['SUBSCRIBES_CNT'] = count($subscribes);
		$arResult['SUBSCRIBES_CNT_WORD'] = pfCnt($arResult['SUBSCRIBES_CNT'], "подписка", "подписки", "подписок");
		
	}

}





$this->IncludeComponentTemplate();