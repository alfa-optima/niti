<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (count($arResult["SECTIONS"]) > 0){ ?>

	<div class="cats">
		<ul>
		
			<? foreach ($arResult["SECTIONS"] as $section){ ?>
			
				<li>
					<a <? if( $section['ID'] == param_get('section') ){ ?>class="active"<? } ?> href="<?=addToRequestURI('section', $section['ID'])?>"><?=$section['NAME']?></a>
				</li>
			
			<? } ?>
			
		</ul>
	</div>

<? } ?>