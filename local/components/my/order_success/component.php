<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('sale');
use \Bitrix\Sale;

if( 
	count($_POST) > 0
	&&
	intval( param_post('order_id') ) > 0
){} else {  LocalRedirect('/');  }


$arResult['order_id'] = strip_tags( param_post('order_id') );
$arResult['ps'] = strip_tags( param_post('ps') );


$APPLICATION->SetPageProperty("title", 'Заказ №'.$arResult['order_id'].' успешно оформлен!');
$APPLICATION->AddChainItem('Заказ №'.$arResult['order_id'].' успешно оформлен!', '');



$arResult['SHOW_PAY_BUTTON'] = 'N';
if( in_array( $arResult['ps'], array(5) ) ){
	$arResult['SHOW_PAY_BUTTON'] = 'Y';

    $order = Sale\Order::load($arResult['order_id']);
    $paymentCollection = $order->getPaymentCollection();
    foreach ($paymentCollection as $payment) {
        $pay_sum = $payment->getSum();
    }
    if( !$pay_sum ){
        $pay_sum = 0;
    }

}







$this->IncludeComponentTemplate();