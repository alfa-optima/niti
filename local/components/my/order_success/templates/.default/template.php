<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); 
use \Bitrix\Main,
\Bitrix\Main\Localization\Loc as Loc,
\Bitrix\Main\Loader,
\Bitrix\Main\Config\Option,
\Bitrix\Main\Application,
\Bitrix\Sale\Delivery,
\Bitrix\Sale\PaySystem,
\Bitrix\Sale,
\Bitrix\Sale\Basket,
\Bitrix\Sale\Order,
\Bitrix\Sale\DiscountCouponsManager,
\Bitrix\Main\Context; ?>


<div style="margin: 50px 0;">

	<p class="success___p">Заказ №<?=$arResult['order_id']?> успешно оформлен!</p>
	
	
	<? if( $arResult['SHOW_PAY_BUTTON'] == 'Y' ){ ?>
	
		<br>
		<p class="success___p">Теперь заказ необходимо оплатить:</p>
	
		<div class="pay___block"></div>

		<script type="text/javascript">
		$(document).ready(function(){
			getPaymentButton(<?=$arResult['order_id']?>, <?=$pay_sum?>);
		});
		</script>

	<? } else { ?>
	
		<br>
		<p class="success___p">Мы свяжемся с Вами в ближайшее время.</p>
	
	<? } ?>
	
	
</div>


