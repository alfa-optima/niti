<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if ( $arResult['BANNER'] ){ ?>
	
	<a <? if( strlen($arResult['BANNER']['PROPERTY_BUTTON_URL_VALUE']) > 0 ){ ?>href="<?=$arResult['BANNER']['PROPERTY_BUTTON_URL_VALUE']?>"<? } ?> class="catalog_banner" style="background-image: url('<?=rIMGG($arResult['BANNER']['PREVIEW_PICTURE'], 5, 1170, 300)?>');">
		<div class="info">
		
			<div class="title"><?=$arResult['BANNER']['NAME']?></div>

			<div class="desc"><?=$arResult['BANNER']['PREVIEW_TEXT']?></div>

			<? if( 
				strlen($arResult['BANNER']['PROPERTY_BUTTON_URL_VALUE']) > 0
				&&
				strlen($arResult['BANNER']['PROPERTY_BUTTON_TITLE_VALUE']) > 0
			){ ?>
				<div class="details"><?=$arResult['BANNER']['PROPERTY_BUTTON_TITLE_VALUE']?></div>
			<? } ?>
			
		</div>
	</a>
	
<? } ?>