<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
 
// Текущий URL
$pureURL = $arParams['pureURL'];

// Все баннеры
$all_banners = all_banners();

if ( count($all_banners) > 0 ){

	$arResult['BANNER'] = false;

	// Сначала ищем точное совпадение URL-ов
	foreach ( $all_banners as $ban ){
		if ( 
			$ban['PROPERTY_URL_VALUE'] == $pureURL
			&&
			!$arResult['BANNER']
		){
			$arResult['BANNER'] = $ban;
		}
	}
	
	// Если точное совпадение не удалось найти
	if ( !$arResult['BANNER'] ){
		// Повторно перебираем баннеры
		foreach ( $all_banners as $ban ){
			if ( 
				string_begins_with($ban['PROPERTY_URL_VALUE'], $pureURL)
				&&
				$ban['PROPERTY_SUB_VALUE'] == 'Да'
				&&
				!$arResult['BANNER']
			){
				$arResult['BANNER'] = $ban;
			}
		}
	} 
	
}

$this->IncludeComponentTemplate();