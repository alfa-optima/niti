<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

// Инфо по корзине
$cupon = project\my_coupon::get_active_coupon();
$arResult['basketInfo'] = basketInfo( $cupon );

$arResult['basket_cnt'] = $arResult['basketInfo']['basket_cnt'];
$arResult['arBasket'] = $arResult['basketInfo']['arBasket'];

if ( $arResult['basket_cnt'] == 0 ){
	LocalRedirect('/basket/');
}

$arResult['all_countries'] = all_countries();

// Инфо об авторизованном пользователе
if ($USER->IsAuthorized()){
	
	$q = \CUser::GetByID($USER->GetID());
	$arResult['arUser'] = $q->GetNext();
	
}




$arResult['stop_names'] = array(
	'NAME',
	'LAST_NAME',
	'EMAIL',
	'PERSONAL_PHONE',
	'LOCATION_NAME',
	'LOCATION_ID',
	'LOCATION_ZIP',
	'STREET',
	'HOUSE',
	'CORPUS',
	'NUMBER',
	'ds',
	'order_pvz_code',
	'order_pvz_address',
	'order_pvz_price',
	'order_pvz_period',
	'DELIVERY_PRICE',
	'BOXBERRY_LOC_ID',
	'SDEK_LOC_ID',
);




$arResult['SHOW_MAP'] = 'N';
$arResult['SHOW_ADDRESS_BLOCK'] = 'N';
$arResult['DELIVERY_BLOCK_HTML'] = '';

if( intval(param_post('LOCATION_ID')) > 0 ){
	
	ob_start(); 
		$APPLICATION->IncludeComponent(
			"my:order_delivery_block", "",
			array(
				'loc_id' => param_post('LOCATION_ID'),
				'zip' => param_post('LOCATION_ZIP')
			)
		);
		$arResult['DELIVERY_BLOCK_HTML'] = ob_get_contents();
	ob_end_clean();
	
	// Boxberry ПВЗ - показываем карту
	if( param_post('ds') == 5 ){
		$arResult['SHOW_MAP'] = 'Y';
		
	} else {
		$arResult['SHOW_ADDRESS_BLOCK'] = 'Y';
	}
	
}

//echo "<pre>"; print_r( $arResult['arUser'] ); echo "</pre>";

$arResult['STREET'] = strip_tags($_POST['STREET']?$_POST['STREET']:$arResult['arUser']['PERSONAL_STREET']);
$arResult['HOUSE'] = strip_tags($_POST['HOUSE']?$_POST['HOUSE']:$arResult['arUser']['UF_HOUSE']);
$arResult['CORPUS'] = strip_tags($_POST['CORPUS']?$_POST['CORPUS']:$arResult['arUser']['UF_KORPUS']);
$arResult['NUMBER'] = strip_tags($_POST['NUMBER']?$_POST['NUMBER']:$arResult['arUser']['UF_KVARTIRA']);



ob_start(); 
	$APPLICATION->IncludeComponent(
		"my:order_steps", "",
		array('step' => 2)
	);
	$arResult['ORDER_STEPS'] = ob_get_contents();
ob_end_clean();





ob_start(); 
	$params = array('basketInfo' => $arResult['basketInfo']);
	if ( strlen(param_post('DELIVERY_PRICE')) > 0 ){
		$params['delivery_price'] = (int)param_post('DELIVERY_PRICE');
	}
	$APPLICATION->IncludeComponent("my:basket_right", "", $params);
	$arResult['BASKET_RIGHT'] = ob_get_contents();
ob_end_clean();
	
	
	
	
	
	
	
	
	
$this->IncludeComponentTemplate();

