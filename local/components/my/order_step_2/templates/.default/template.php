<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<script src="http://api-maps.yandex.ru/2.1/?lang=ru-RU"></script>


<div class="checkout_info">

	<aside class="right">

		<?=$arResult['BASKET_RIGHT']?>

	</aside>


	<section class="content left">
	
	
		<?=$arResult['ORDER_STEPS']?>


		<form action="/order_step_3/" method="post" class="form order___form">
		
		
			<? foreach ( $_POST as $name => $value ){ 
				if ( !in_array($name,  $arResult['stop_names']) ){ ?>
					<input type="hidden" name="<?=$name?>" value="<?=$value?>">
				<? }
			} ?>
			
		
			<div class="title">Персональные данные</div>

			<div class="lines">
				<div class="line">
					<div class="name">Имя <span class="required">*</span></div>
					<div class="field">
						<input type="text" name="NAME" value="<?=strip_tags(param_post('NAME')?param_post('NAME'):$arResult['arUser']['NAME'])?>" class="input">
					</div>
				</div>
			</div>

			<div class="lines">
				<div class="line">
					<div class="name">Фамилия <span class="required">*</span></div>
					<div class="field">
						<input type="text" name="LAST_NAME" value="<?=strip_tags(param_post('LAST_NAME')?param_post('LAST_NAME'):$arResult['arUser']['LAST_NAME'])?>" class="input">
					</div>
				</div>
			</div>

			<div class="lines">
				<div class="line">
					<div class="name">E-mail <span class="required">*</span></div>
					<div class="field">
						<input type="email" name="EMAIL" value="<?=strip_tags(param_post('EMAIL')?param_post('EMAIL'):$arResult['arUser']['EMAIL'])?>" class="input">
					</div>
				</div>
			</div>

			<div class="lines">
				<div class="line">
					<div class="name">Телефон <span class="required">*</span></div>
					<div class="field">
						<div class="phone_code">+7</div>
						<input type="tel" name="PERSONAL_PHONE" value="<?=strip_tags(param_post('PERSONAL_PHONE')?param_post('PERSONAL_PHONE'):$arResult['arUser']['PERSONAL_PHONE'])?>" class="input" placeholder="___ ___ ____">
					</div>
				</div>

				<div class="line">
					<div class="field">
						<div class="exp"><span class="required">*</span> Поля обязательные для заполнения</div>
					</div>
				</div>
			</div>


			<div class="title">Местоположение</div>

			<div class="lines">
				<div class="line">
					<div class="name">Населённый пункт <span class="required">*</span></div>
					<div class="field">
						<input type="text" name="LOCATION_NAME" class="input" value="<?=param_post('LOCATION_NAME')?>">
						<input name="LOCATION_ID" type="hidden" value="<?=param_post('LOCATION_ID')?>">
					</div>
				</div>
				<div class="line">
					<div class="field">
						<div class="exp">Начните ввод и выберите<br>из предложенных вариантов</div>
					</div>
				</div>
			</div>
			
			<div class="lines">
				<div class="line">
					<div class="name">Почтовый индекс <span class="required">*</span></div>
					<div class="field">
						<input type="text" name="LOCATION_ZIP" class="input" value="<?=param_post('LOCATION_ZIP')?>">
					</div>
				</div>
			</div>

			
			
			<div class="order_delivery_block"><?=$arResult['DELIVERY_BLOCK_HTML']?></div>
			<? if( $arResult['SHOW_MAP'] == 'Y' ){ ?>
				<script type="text/javascript">
				$(document).ready(function(){
					map_init( true );
				});
				</script>
			<? } ?>
			
			
			<div class="lines order_address_block" <? if( $arResult['SHOW_ADDRESS_BLOCK'] == 'N' ){ ?>style="display:none"<? } ?>>
				<div class="line">
					<div class="name">Улица <span class="required">*</span></div>
					<div class="field">
						<input placeholder="Ваша улица" type="text" name="STREET" value="<?=$arResult['STREET']?>" class="input">
					</div>
				</div>
				<div class="lines line">
					<div class="line">
						<div class="name">Дом <span class="required">*</span></div>
						<div class="field">
							<input placeholder="Дом" type="text" name="HOUSE" value="<?=$arResult['HOUSE']?>" class="input">
						</div>
					</div>
					<div class="line">
						<div class="name">Корпус</div>
						<div class="field">
							<input type="text" placeholder="Корп." name="CORPUS" value="<?=$arResult['CORPUS']?>" class="input">
						</div>
					</div>
					<div class="line">
						<div class="name">Квартира</div>
						<div class="field">
							<input type="text" placeholder="Квартира" name="NUMBER" value="<?=$arResult['NUMBER']?>" class="input">
						</div>
					</div>
				</div>
			</div>

			<p class="error___p" style="margin: 20px 0 10px 0;"></p>
			
		</form>


		<div class="checkout_links">
			<a style="cursor:pointer" class="back_link left back_to_basket_button to___process">
				<span class="icon"></span>В корзину
			</a>
			<a style="cursor:pointer" class="next_link right to_step_3_button to___process">
				Продолжить<span class="icon"></span>
			</a>
		</div>
		
	</section>
	
	<div class="clear"></div>

</div>








<? if( 1==2 ){ ?>

	<div class="checkout_info">


		<aside class="right">
		
			<?=$arResult['BASKET_RIGHT']?>

		</aside>


		<section class="content left">
		
		
			<?=$arResult['ORDER_STEPS']?>
			
			
			<div class="line form" style="margin: 50px 0">
				<div class="gender">
				
					<? // Типы плательщиков
					foreach( $arResult['PAYER_TYPES'] as $ptype ){ ?>

						<div class="item">
							<input type="radio" name="payer_type" id="payer_type<?=$ptype['ID']?>" value="<?=$ptype['ID']?>" form_name="order_form_<?=$ptype['ID']?>" <? if( $ptype['checked'] ){ ?>checked<? } ?>>
							<label for="payer_type<?=$ptype['ID']?>"><?=$ptype['NAME']?></label>
						</div>
					
					<? } ?>
					
				</div>
			</div>

			
			
			<form action="/order_step_3/" method="post" class="form order___form order_form_1" <? if ( !$arResult['fiz_visible'] ){ ?>style="display:none"<? } ?>>
			
			
				<? foreach ( $_POST as $name => $value ){ 
					if ( !in_array($name,  $arResult['fiz_stop_names']) ){ ?>
						<input type="hidden" name="<?=$name?>" value="<?=$value?>">
					<? }
				} ?>
			
				<input type="hidden" name="user_pay_type_id" value="1">
				<input type="hidden" name="user_pay_type" value="fiz">
			
				<div class="title">Контактные данные</div>

				<div class="lines">
				
					<div class="line">
						<div class="name">Имя</div>

						<div class="field">
							<input type="text" name="NAME" value="<?=strip_tags(param_post('NAME')?param_post('NAME'):$arResult['arUser']['NAME'])?>" class="input">
						</div>
					</div>

					<div class="line">
						<div class="name">Фамилия</div>

						<div class="field">
							<input type="text" name="LAST_NAME" value="<?=strip_tags(param_post('LAST_NAME')?param_post('LAST_NAME'):$arResult['arUser']['LAST_NAME'])?>" class="input">
						</div>
					</div>
					
				</div>

				<div class="lines">
				
					<div class="line">
						<div class="name">E-mail</div>

						<div class="field">
							<input type="email" name="EMAIL" value="<?=strip_tags(param_post('EMAIL')?param_post('EMAIL'):$arResult['arUser']['EMAIL'])?>" class="input">
						</div>
					</div>

					<div class="line">
						<div class="name">Телефон</div>

						<div class="field">
							<div class="phone_code">+7</div>
							
							<input type="tel" name="PERSONAL_PHONE" value="<?=strip_tags(param_post('PERSONAL_PHONE')?param_post('PERSONAL_PHONE'):$arResult['arUser']['PERSONAL_PHONE'])?>" class="input" placeholder="___ ___ ____">
						</div>
					</div>
					
				</div>
				
				<p class="error___p"></p>

			</form>
			
			
			<form action="/order_step_3/" method="post" class="form order___form order_form_2" <? if ( !$arResult['yur_visible'] ){ ?>style="display:none"<? } ?>>
			
			
				<? foreach ( $_POST as $name => $value ){ 
					if ( !in_array($name,  $arResult['yur_stop_names']) ){ ?>
						<input type="hidden" name="<?=$name?>" value="<?=$value?>">
					<? }
				} ?>
			
				<input type="hidden" name="user_pay_type_id" value="2">
				<input type="hidden" name="user_pay_type" value="yur">
		

				<div class="title">Контактные данные</div>

				<div class="lines">
				
					<div class="line">
						<div class="name">Имя контактного лица</div>

						<div class="field">
							<input type="text" name="NAME" value="<?=strip_tags(param_post('NAME')?param_post('NAME'):$arResult['arUser']['NAME'])?>" class="input">
						</div>
					</div>


					<div class="line">
						<div class="name">Фамилия контактного лица</div>

						<div class="field">
							<input type="text" name="LAST_NAME" value="<?=strip_tags(param_post('LAST_NAME')?param_post('LAST_NAME'):$arResult['arUser']['LAST_NAME'])?>" class="input">
						</div>
					</div>
					
				</div>


				<div class="lines">
				
					<div class="line">
						<div class="name">E-mail</div>

						<div class="field">
							<input type="email" name="EMAIL" value="<?=strip_tags(param_post('EMAIL')?param_post('EMAIL'):$arResult['arUser']['EMAIL'])?>" class="input">
						</div>
					</div>

					<div class="line">
						<div class="name">Телефон</div>

						<div class="field">
							<div class="phone_code">+7</div>
							
							<input type="tel" name="PERSONAL_PHONE" value="<?=strip_tags(param_post('PERSONAL_PHONE')?param_post('PERSONAL_PHONE'):$arResult['arUser']['PERSONAL_PHONE'])?>" class="input" placeholder="___ ___ ____">
						</div>
					</div>
					
				</div>


				
				
				<div class="title">Реквизиты компании</div>

				<div class="lines">
				
					<div class="line">
						<div class="name">Название компании</div>

						<div class="field">
							<input type="text" name="UF_COMPANY_NAME" value="<?=strip_tags(param_post('UF_COMPANY_NAME')?param_post('UF_COMPANY_NAME'):$arResult['arUser']['UF_COMPANY_NAME'])?>" class="input">
						</div>
					</div>

					<div class="line">
						<div class="name">ИНН</div>

						<div class="field">
							<input type="text" name="UF_INN" value="<?=strip_tags(param_post('UF_INN')?param_post('UF_INN'):$arResult['arUser']['UF_INN'])?>" class="input">
						</div>
					</div>
					
				</div>
				
				<div class="lines">
				
					<div class="line">
						<div class="name">Юридический адрес</div>

						<div class="field">
							<input type="text" name="UF_YUR_ADDRESS" value="<?=strip_tags(param_post('UF_YUR_ADDRESS')?param_post('UF_YUR_ADDRESS'):$arResult['arUser']['UF_YUR_ADDRESS'])?>" class="input">
						</div>
					</div>

					<div class="line">
						<div class="name">Наименование банка</div>

						<div class="field">
							<input type="text" name="UF_BANK" value="<?=strip_tags(param_post('UF_BANK')?param_post('UF_BANK'):$arResult['arUser']['UF_BANK'])?>" class="input">
						</div>
					</div>
					
				</div>
				
				<div class="lines">
				
					<div class="line">
						<div class="name">БИК</div>

						<div class="field">
							<input type="text" name="UF_BIK" value="<?=strip_tags(param_post('UF_BIK')?param_post('UF_BIK'):$arResult['arUser']['UF_BIK'])?>" class="input">
						</div>
					</div>

					<div class="line">
						<div class="name">Расчётный счёт</div>

						<div class="field">
							<input type="text" name="UF_RS" value="<?=strip_tags(param_post('UF_RS')?param_post('UF_RS'):$arResult['arUser']['UF_RS'])?>" class="input">
						</div>
					</div>
					
				</div>
				
				<div class="lines">
				
					<div class="line">
						<div class="name">Корреспондентский счёт</div>

						<div class="field">
							<input type="text" name="UF_KS" value="<?=strip_tags(param_post('UF_KS')?param_post('UF_KS'):$arResult['arUser']['UF_KS'])?>" class="input">
						</div>
					</div>
					
				</div>
				
				<p class="error___p"></p>

			</form>
			

			
			<div class="checkout_links">
			
				<a style="cursor:pointer" class="back_link left back_to_basket_button">
					<span class="icon"></span>В корзину
				</a>

				<a style="cursor:pointer" class="next_link right to_step_3_button">
					Продолжить<span class="icon"></span>
				</a>
				
			</div>
			
			
		</section>
		<div class="clear"></div>

	</div>
	
<? } ?>