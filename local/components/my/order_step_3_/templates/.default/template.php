<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<div class="checkout_info">


	<aside class="right">

	
		<?=$arResult['BASKET_RIGHT']?>

		
	</aside>

	
	<section class="content left">
	
	
		<?=$arResult['ORDER_STEPS']?>


		<form action="/order_step_4/" method="post" class="form order___form">
		
			<input type="hidden" name="delivery_price" value="<?=(param_post('delivery_price'))?param_post('delivery_price'):'-'?>">
		
		
			<? foreach ( $_POST as $name => $value ){ 
				if ( !in_array($name,  $arResult['stop_names']) ){ ?>
					<input type="hidden" name="<?=$name?>" value="<?=$value?>">
				<? }
			} ?>
			
		
			<div class="title">Выберите способ доставки</div>

			<div class="lines delivery_methods">
			
				<? // Варианты доставки
				foreach( $arResult['arDS'] as $ds ){ ?>
			
					<div class="line">
						<div class="field">
							<input price="<?=$ds['PRICE']?>" type="radio" name="ds" id="delivery_method<?=$ds['ID']?>" <? if( $ds['checked'] ){ ?>checked<? } ?> value="<?=$ds['ID']?>">
							<label for="delivery_method<?=$ds['ID']?>">
								<div class="name"><?=$ds['NAME']?></div>
								<div class="desc"><?=$ds['DESCRIPTION']?></div>
							</label>
						</div>
					</div>

				<? } ?>
	
			</div>

			
			
			<div class="loc___block" style="display:<?=$arResult['SHOW_AUTOCOMPLETE'] == 'Y'?'block':'none'?>">
				
				<div class="lines">
				
					<div class="line" style="width:100%">
						<div class="name">Населённый пункт</div>
						<div class="field">
							<input type="text" id="loc_name" name="loc_name" value="<?=strip_tags(param_post('loc_name')?param_post('loc_name'):'')?>" class="input">
							<input type="hidden" name="loc_id" value="<?=strip_tags(param_post('loc_id')?param_post('loc_id'):'')?>">
							<input type="hidden" name="calc_error" value="<?=strip_tags(param_post('calc_error')?param_post('calc_error'):'N')?>">
						</div>
						<div class="field search___status" style="display:none">
							<span>Поиск...</span>
						</div>
					</div>
					

					
				</div>
				
				<div class="lines repeat___block" <?=$arResult['SHOW_RECALC_BUTTON']=='Y'?'':'style="display:none"'?>>
					<div class="line">
						<div class="field">
							<p style="color:red">Не удалось рассчитать стоимость доставки</p>
						</div>
					</div>
					<div class="line">
						<div class="checkout_links" style="padding-top:0">
							<a style="cursor:pointer; width:auto; color:red" class="back_link right recalcDeliveryPriceButton to___process">Повторный запрос</a>
						</div>
					</div>
				</div>
				
				<div class="title">Адрес доставки</div>

				<!--<div class="lines">

					<div class="line">
						<div class="name">Страна</div>

						<div class="field">
							<select name="UF_COUNTRY">
								<option value="0"></option>
								<? foreach ( $arResult['all_countries'] as $country ){ ?>
									<option <? if( strtolower(trim($country['NAME'])) == strtolower(trim(param_post('UF_COUNTRY')?param_post('UF_COUNTRY'):$arResult['arUser']['UF_COUNTRY'])) ){ ?>selected<? } ?> value="<?=$country['NAME']?>"><?=$country['NAME']?></option>
								<? } ?>
							</select>
						</div>
					</div>

					<div class="line">
						<div class="name">Город</div>

						<div class="field">
							<input type="text" name="PERSONAL_CITY" value="<?=strip_tags(param_post('PERSONAL_CITY')?param_post('PERSONAL_CITY'):$arResult['arUser']['PERSONAL_CITY'])?>" class="input">
						</div>
					</div>
					
				</div>-->

				<div class="lines">
					<div class="line">
						<div class="name">Индекс</div>

						<div class="field">
							<input type="text" name="PERSONAL_ZIP" value="<?=strip_tags(param_post('PERSONAL_ZIP')?param_post('PERSONAL_ZIP'):$arResult['arUser']['PERSONAL_ZIP'])?>" class="input">
						</div>
					</div>


					<div class="line">
						<div class="name">Улица</div>

						<div class="field">
							<input type="text" name="PERSONAL_STREET" value="<?=strip_tags(param_post('PERSONAL_STREET')?param_post('PERSONAL_STREET'):$arResult['arUser']['PERSONAL_STREET'])?>" class="input">
						</div>
					</div>
				</div>

				<div class="lines">
				
					<div class="line">
						<div class="name">Дом</div>

						<div class="field">
							<input type="text" name="UF_HOUSE" value="<?=strip_tags(param_post('UF_HOUSE')?param_post('UF_HOUSE'):$arResult['arUser']['UF_HOUSE'])?>" class="input">
						</div>
					</div>
					
					<div class="line">
						<div class="name">Корпус</div>

						<div class="field">
							<input type="text" name="UF_KORPUS" value="<?=strip_tags(param_post('UF_KORPUS')?param_post('UF_KORPUS'):$arResult['arUser']['UF_KORPUS'])?>" class="input">
						</div>
					</div>
					
					<div class="line">
						<div class="name">Подъезд</div>

						<div class="field">
							<input type="text" name="UF_PODYEZD" value="<?=strip_tags(param_post('UF_PODYEZD')?param_post('UF_PODYEZD'):$arResult['arUser']['UF_PODYEZD'])?>" class="input">
						</div>
					</div>

					<div class="line">
						<div class="name">Квартира</div>

						<div class="field">
							<input type="text" name="UF_KVARTIRA" value="<?=strip_tags(param_post('UF_KVARTIRA')?param_post('UF_KVARTIRA'):$arResult['arUser']['UF_KVARTIRA'])?>" class="input">
						</div>
					</div>
					
				</div>
				
			</div>
			
			
			
			<div class="title">Комментарий к заказу</div>

			<div class="line">
				<div class="field">
					<textarea name="comment"><?=strip_tags(param_post('comment'))?></textarea>
				</div>
			</div>
			
			<p class="error___p"></p>
			
		</form>


		<div class="checkout_links">
		
			<a style="cursor:pointer" class="back_link left back_to_step_2_button">
				<span class="icon"></span>Вернуться
			</a>

			<a style="cursor:pointer" class="next_link right to_step_4_button">
				Продолжить<span class="icon"></span>
			</a>
			
		</div>
		
		
	</section>
	<div class="clear"></div>

</div>