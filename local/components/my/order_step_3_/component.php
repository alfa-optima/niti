<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if( count($_POST) > 0 ){} else {    LocalRedirect('/basket/');   }

// Инфо по корзине
$arResult['basketInfo'] = basketInfo();

$arResult['basket_cnt'] = $arResult['basketInfo']['basket_cnt'];
$arResult['arBasket'] = $arResult['basketInfo']['arBasket'];


// Инфо об авторизованном пользователе
if ($USER->IsAuthorized()){
	
	$q = CUser::GetByID($USER->GetID());
	$arResult['arUser'] = $q->GetNext();
	
}


if ( $arResult['basket_cnt'] == 0 ){    LocalRedirect('/basket/');   }

ob_start(); 
	$params = array('basketInfo' => $arResult['basketInfo']);
	if ( $_POST['delivery_price'] != null ){
		$params['delivery_price'] = (int)param_post('delivery_price');
	}
	
	
	
	$APPLICATION->IncludeComponent("my:basket_right", "", $params);
	$arResult['BASKET_RIGHT'] = ob_get_contents();
ob_end_clean();



ob_start(); 
	$APPLICATION->IncludeComponent(
		"my:order_steps", "",
		array('step' => 3)
	);
	$arResult['ORDER_STEPS'] = ob_get_contents();
ob_end_clean();



if ( !param_post('delivery_price') ){
	echo '<script type="text/javascript"> $(document).ready(function(){ update_right_basket(); }); </script>';
}


$arResult['stop_names'] = array(
	'ds', 'comment', 'delivery_price', 'loc_id', 'loc_name', 'calc_error',
	//'UF_COUNTRY',
	//'PERSONAL_CITY',
	'PERSONAL_ZIP',
	'PERSONAL_STREET',
	'UF_HOUSE',
	'UF_KORPUS',
	'UF_PODYEZD',
	'UF_KVARTIRA'
);




$cur_ds = false;

// Варианты доставки
$arResult['arDS'] = array();
$cnt = 0;
$db_dtype = \CSaleDelivery::GetList(
	Array("SORT"=>"ASC", "NAME"=>"ASC"),
	Array(
		"ACTIVE"=>"Y", "LID" => SITE_ID
	),
	false, false, array()
);
while ($ds = $db_dtype->GetNext()){ $cnt++; 
	$ds['checked'] = false; 
	if ( param_post('ds') ){
		if(param_post('ds') == $ds['ID']){    $ds['checked'] = true;   }
	} else if( $cnt == 1 ){
		$ds['checked'] = true;
	}
	if( $ds['checked'] ){
		$cur_ds = $ds['ID'];
	}
	$arResult['arDS'][] = $ds;
}


$arResult['SHOW_AUTOCOMPLETE'] = 'N';
if( in_array($cur_ds, project\site::$calc_ds_list ) ){
	$arResult['SHOW_AUTOCOMPLETE'] = 'Y';
}


$arResult['SHOW_RECALC_BUTTON'] = 'N';
if( param_post('calc_error') ){
	$arResult['SHOW_RECALC_BUTTON'] = param_post('calc_error');
}



$this->IncludeComponentTemplate();
	
