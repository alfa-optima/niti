<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>


<nav>

	<? // Перебираем 1 уровень
	foreach ( $arResult['sections'] as $section_1 ){ ?>

		<a href="<?=$section_1['SECTION_PAGE_URL']?>"><?=$section_1['NAME']?></a>
		
	<? } ?>
	
</nav>



<? if( 1==2 ){ ?>

	<nav>

		<? // Перебираем 1 уровень
		foreach ( $arResult['sections'] as $section_1 ){ ?>
		
			<div class="menu_item">
			
				<a href="<?=$section_1['SECTION_PAGE_URL']?>" class="sub_link"><?=$section_1['NAME']?></a>

				<? // Если есть подразделы 2 уровня
				if ( $section_1['SUBSECTIONS'] ){ ?>

					<div class="sub_cats">
						<div class="cont">
						
							<div class="cats left">
							
								<div class="title"><?=$section_1['NAME']?></div>

								<div class="grid">
								
									<? // Перебираем подразделы 2 уровня
									foreach ( $section_1['SUBSECTIONS'] as $section_2 ){ ?>
									
										<div class="item_wrap">
											<div class="item">
												<ul>
													<li class="main">
														<a href="<?=$section_2['SECTION_PAGE_URL']?>"><?=$section_2['NAME']?></a>
													</li>
													
													<? // Если есть подразделы 3 уровня
													if ( $section_2['SUBSECTIONS'] ){ 
														foreach ( $section_2['SUBSECTIONS'] as $section_3 ){ ?>
													
															<li><a href="<?=$section_3['SECTION_PAGE_URL']?>"><?=$section_3['NAME']?></a></li>
														<? }
													}?>
													
												</ul>
											</div>
										</div>
										
									<? } ?>

								</div>
							</div>

							<div class="product right">
								<!--
								<div class="sticker">эксклюзив</div>
								<div class="thumb">
									<a href="/">
										<img src="<?=SITE_TEMPLATE_PATH?>/images/product_thumb2.jpg" alt="">
									</a>
								</div>
								<div class="info">
									<div class="cat">
										<a href="/">Хейтер</a>
									</div>

									<div class="name">
										<a href="/">Футболки с принтом женские</a>
									</div>

									<div class="price">1 290 <span class="currency">:</span></div>
								</div>
								-->
							</div>
							<div class="clear"></div>
							
						</div>
					</div>

				<? } ?>
				
			</div>

		<? } ?>
			
	</nav>
	
<? } ?>