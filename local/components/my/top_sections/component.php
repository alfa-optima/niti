<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); 
\Bitrix\Main\Loader::includeModule("iblock");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$sections = array();

// Кешируем
$obCache = new \CPHPCache();
$cache_time = 30*24*60*60;
// ID кеша
$cache_id = 'top_sections';
// путь хранения кеша
$cache_path = '/top_sections/';
if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
	$vars = $obCache->GetVars();   extract($vars);
} elseif($obCache->StartDataCache()){

	// Соберём разделы 1 уровня
	$sections_1 = \CIBlockSection::GetList( Array("SORT"=>"ASC"), Array(
		"ACTIVE"=>"Y",
		"GLOBAL_ACTIVE" => "Y",
		"DEPTH_LEVEL" => 1,
		"IBLOCK_ID"=> project\site::CATALOG_IBLOCK_ID
	), false, array() );
	while ($section_1 = $sections_1->GetNext()){
		// Ищём подразделы 2 уровня в разделе $section_1
		$sections_2 = \CIBlockSection::GetList( Array("SORT"=>"ASC"), Array(
			"ACTIVE"=>"Y",
			"GLOBAL_ACTIVE" => "Y",
			"DEPTH_LEVEL" => 2,
			"IBLOCK_ID"=> project\site::CATALOG_IBLOCK_ID,
			"SECTION_ID" => $section_1['ID']
		), false, array() );
		while ($section_2 = $sections_2->GetNext()){
			// Ищём подразделы 3 уровня в разделе $section_2
			$sections_3 = \CIBlockSection::GetList( Array("SORT"=>"ASC"), Array(
				"ACTIVE"=>"Y",
				"GLOBAL_ACTIVE" => "Y",
				"DEPTH_LEVEL" => 3,
				"IBLOCK_ID"=> project\site::CATALOG_IBLOCK_ID,
				"SECTION_ID" => $section_2['ID']
			), false, array() );
			while ($section_3 = $sections_3->GetNext()){
				$section_2['SUBSECTIONS'][] = $section_3;
			}
			$section_1['SUBSECTIONS'][] = $section_2;
		}
		$sections[] = $section_1;
	} 

$obCache->EndDataCache(array('sections' => $sections));
} 

$arResult['sections'] = $sections; 







$this->IncludeComponentTemplate();
