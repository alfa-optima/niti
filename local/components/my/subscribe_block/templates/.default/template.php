<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<div class="subscribe">
	<div class="cont">
		<div class="title">ПОДПИШИТЕСЬ НА НОВОСТИ</div>

		<form>
			<input type="text" name="name" value="" class="input" placeholder="Имя">

			<input type="email" name="email" class="input" placeholder="E-mail">
			
			<button type="button" class="submit_btn subscribe_button to___process">отправить</button>
		</form>

		<div class="agree">Подписываясь на рассылку, вы соглашаетесь с условиями <a href="/oferta/">оферты</a> и <a href="/politika-konfidentsialnosti/">политики конфиденциальности</a>.</div>
	</div>
</div>