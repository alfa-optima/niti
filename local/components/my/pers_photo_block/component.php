<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$arResult = $arParams['arUser']; 

if( intval($arResult['PERSONAL_PHOTO']) > 0 ){
	
	$arResult['PERSONAL_PHOTO'] = rIMGG($arResult['PERSONAL_PHOTO'], 5, project\pers_photo::MAX_WIDTH, project\pers_photo::MAX_HEIGHT);
	
}



$this->IncludeComponentTemplate();
