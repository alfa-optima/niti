<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<div class="img">

	<? if( $arResult['PERSONAL_PHOTO'] ){ ?>
	
		<img src="<?=$arResult['PERSONAL_PHOTO']?>">
		
		<div class="links">
			<a style="cursor:pointer" class="edit_link persPhotoSelect to___process">Изменить</a>
			<a style="cursor:pointer" class="del_link persPhotoDeleteButton to___process">Удалить</a>
		</div>
		
	<? } else { ?>
	
		<a style="cursor:pointer" class="persPhotoSelect to___process">
			<img src="<?=SITE_TEMPLATE_PATH?>/images/load_image.jpg">
		</a>
		
	<? } ?>
	
</div>

<? if( intval($arResult['PERSONAL_PHOTO']) > 0 ){ ?>
	<div>Личная фотография</div>
<? } ?>