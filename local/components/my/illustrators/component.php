<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); 

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;


$arURI = tools\funcs::arURI( tools\funcs::pureURL() );

if( intval( $arURI[2] ) > 0 ){
	
	$illustrator_id = $arURI[2];
	$arResult = tools\user::info($illustrator_id);

	if( intval($arResult['ID']) > 0 ){

        // Принты иллюстратора
        $arResult['PRINTS'] = project\illustrator::getPrints($arResult['ID']);
        $arResult['PRINTS_CNT'] = count($arResult['PRINTS']);
        $arResult['PRINTS_CNT_WORD'] = pfCnt(count($arResult['PRINTS']), "принт", "принта", "принтов");


        $subscribes = project\i_subscribe::getListForIllustrator($illustrator_id);
        $arResult['SUBSCRIBES_CNT'] = count($subscribes);
        $arResult['SUBSCRIBES_CNT_WORD'] = pfCnt($arResult['SUBSCRIBES_CNT'], "подписка", "подписки", "подписок");

	}
	
	
} else {
	
	
	$arResult['SORT'] = param_get('sort');
	if( !$arResult['SORT'] ){    $arResult['SORT'] = 'new';    }

	// Все иллюстраторы
	$illustrators = project\illustrator::getList();
	
	if( $arResult['SORT'] == 'rec' ){

		if( count($illustrators) > 0 ){
			
			$illustrators_1 = array();    $illustrators_2 = array();
			
			foreach ( $illustrators as $illustrator_id ){
				$illustrator = tools\user::info($illustrator_id);
				if($illustrator['UF_RECOMMEND'] == 1){
					$illustrators_1[] = $illustrator_id;
				} else {
					$illustrators_2[] = $illustrator_id;
				}
			}
			$illustrators = array_merge($illustrators_1, $illustrators_2);
		}
		
	} else if( $arResult['SORT'] == 'pop' ){
		
		$illustrators_1 = array();    $illustrators_2 = array();
		
		$month_illustrators = project\illustrator::monthList();
		if( count($month_illustrators) > 0 ){
			foreach($month_illustrators as $user_id){
				$illustrators_1[] = $user_id;
			}
		}
		
		if( count($illustrators) > 0 ){
			foreach($illustrators as $user_id){
				if( !in_array($user_id, $illustrators_1) ){
					$illustrators_2[] = $user_id;
				}
			}
		}
		$illustrators = array_merge($illustrators_1, $illustrators_2);
		
	}

	$arResult['LIST'] = $illustrators;
	
	
	
	
	
	
}








$this->IncludeComponentTemplate();