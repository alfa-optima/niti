<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if( intval($arResult['ID']) > 0 ){

    $filter_cache_illustrators = new project\filter_cache_illustrators();

	$APPLICATION->AddChainItem($arResult["NAME"], '/illustrators/'.$arResult['ID'].'/');
	$APPLICATION->SetPageProperty("title", 'Иллюстратор '.$arResult["NAME"]);


    $_GET[ $filter_cache_illustrators->searchParamCode ] = $arResult['ID'];
    $APPLICATION->IncludeComponent( "my:search", '' );
	
	
} else {
	
	// 404
	include_once($_SERVER['DOCUMENT_ROOT'].'/include/areas/404.php');
	
} ?>