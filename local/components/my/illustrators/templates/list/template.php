<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project; ?>

<div class="cont">

	<section class="illustrators">
		<h1 class="page_title">Иллюстраторы</h1>
		
		<? if( count($arResult['LIST']) > 0 ){ ?>

			<div class="sort">
				<a href="?sort=new&PAGEN_1=1" <? if( $arResult['SORT'] == 'new' ){ ?>class="active"<? } ?> sort="new">Новые</a> <span class="sep">|</span> 
				<a href="?sort=rec&PAGEN_1=1" <? if( $arResult['SORT'] == 'rec' ){ ?>class="active"<? } ?> sort="rec">Рекомендованные</a> <span class="sep">|</span> 
				<a href="?sort=pop&PAGEN_1=1" <? if( $arResult['SORT'] == 'pop' ){ ?>class="active"<? } ?> sort="pop">Популярные</a>
			</div>

			<div class="grid">
			
				<? // Выводим список иллюстраторов
				$r_elements = new \CDBResult;
				$r_elements->InitFromArray($arResult['LIST']);
				$r_elements->NavStart(project\illustrator::PAGE_EL_CNT);
				$NAV_STRING = $r_elements->GetPageNavStringEx($navComponentObject, false, false, false);
				while ($id = $r_elements->Fetch()){ 
				
					// illustrator_item_block
					$APPLICATION->IncludeComponent(
						"my:illustrator_item_block", "",
						array('id' => $id)
					);
					
				} ?>
				
			</div>


			<?=$NAV_STRING;?>
			
			
		<? } ?>
			
		<div class="clear"></div>
		
	</section>

</div>
