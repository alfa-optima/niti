<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); 

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$arResult['tov_id'] = $arParams['tov_id']; 
$arResult['comments'] = $arParams['comments']; 

$arResult['ANSWER_LOGO'] = project\site::NITI_COMMENTS_LOGO;

$arResult['AUTH'] = $USER->IsAuthorized();


$arResult['max_cnt'] = project\comment::CNT;


if ( count($arResult['comments']) > 0 ){ 
			
	$cnt = 0;
	foreach ( $arResult['comments'] as $key => $arItem ){ $cnt++;
	
		if ( $cnt <= $arResult['max_cnt'] ){
		
			// Инфо по пользователю
			$user_id = $arItem['PROPERTY_USER_ID_VALUE']; 
			$q = CUser::GetByID($user_id);   $arUser = $q->GetNext();
			$arResult['comments'][$key]['arUser'] = $arUser;
			
			// Получим тип пользователя
			$arResult['comments'][$key]['user_type_name'] = false;
			if ( $arUser['UF_USER_TYPE'] != 1 ){
				$enums = \CUserFieldEnum::GetList(array(), array(
					"ID" => $arUser['UF_USER_TYPE']
				));
				if($enum = $enums->GetNext()){
					$arResult['comments'][$key]['user_type_name'] = $enum['VALUE'];
				}
			}
			
		}
		
	}
	
}




$this->IncludeComponentTemplate();