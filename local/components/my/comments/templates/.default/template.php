<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<section class="comments" id="comments">
	<div class="cont">
	
		<div class="block_title"><div>комментарии</div></div>

		<div class="items">
		
			<? if ( count($arResult['comments']) > 0 ){ 
			
				$cnt = 0;
				foreach ( $arResult['comments'] as $arItem ){ $cnt++;
				
					if ( $cnt <= $arResult['max_cnt'] ){ ?>
				
						<div class="item comment___item" item_id="<?=$arItem['ID']?>">
						
							<div class="avatar left">
							<? if( intval($arItem['arUser']['PERSONAL_PHOTO']) > 0 ){ ?>
								<img src="<?=rIMGG($arItem['arUser']['PERSONAL_PHOTO'], 5, 70, 70)?>">
							<? } else { ?>
								<img src="<?=SITE_TEMPLATE_PATH?>/images/avatar_img.jpg">
							<? } ?>
							</div>

							<div class="info right">
								<div class="author left"><?=$arItem['arUser']['NAME']?$arItem['arUser']['NAME']:$arItem['arUser']['LOGIN']?> <? if( $arResult['user_type_name'] ){ ?><span>(<?=$arResult['user_type_name']?>)</span><? } ?></div>

								<a style="cursor:pointer" class="like___button to___process" comment_id="<?=$arItem['ID']?>" title="Лайк">
									<div class="likes left">
										<?=$arItem['PROPERTY_LIKES_VALUE']?>
									</div>
								</a>
								
								<!--<div class="count left">358</div>-->
								<div class="clear"></div>

								<div class="date"><?=ConvertDateTime($arItem['DATE_CREATE'], "DD.MM.YYYY", "ru")?> в <?=ConvertDateTime($arItem['DATE_CREATE'], "HH:MI", "ru")?></div>

								<div class="text"><?=$arItem['PREVIEW_TEXT']?></div>
							</div>
							<div class="clear"></div>
							
							<? if( strlen($arItem['DETAIL_TEXT']) > 0 ){ ?>
							
								<div class="item children admin">
								
									<div class="avatar left">
										<img src="<?=$arResult['ANSWER_LOGO']?>">
									</div>

									<div class="info right">
										<div class="author">Администратор</div>
										<div class="text"><?=$arItem['DETAIL_TEXT']?></div>
									</div>
									
									<div class="clear"></div>
									
								</div>
								
							<? } ?>
							
						</div>
					
					<? }
				}
				
			} else { ?>
			
				<p class="no___count">Комментариев пока нет</p>
				
			<? } ?>

		</div>


		<? if ( count($arResult['comments']) > $arResult['max_cnt'] ){ ?>
		
			<div class="more">
				<a style="cursor:pointer" tov_id="<?=$arResult['tov_id']?>" class="more___comments to___process"><span class="icon"></span>показать еще</a>
			</div>
			
		<? } ?>

		<? if( $arResult['AUTH'] ){ ?>

			<div class="add_comment">
			
				<div class="title">Ваш комментарий</div>

				<form class="form">
				
					<div class="line">
						<div class="name">Текст комментария</div>

						<div class="field">
							<textarea name="message"></textarea>
						</div>
					</div>
					
					<p class="error___p"></p>

					<div class="submit">
						<button type="button" class="submit_btn comment___button to___process" tov_id="<?=$arResult['tov_id']?>">написать</button>
					</div>
					
				</form>
				
			</div>
			
		<? } ?>
		
	</div>
</section>