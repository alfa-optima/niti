<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

if ( $arParams['arBasket'] ){
	$arResult['arBasket'] = $arParams['arBasket'];
} else {
	
	// Инфо по корзине
    if( isset( $arParams['new_coupon'] ) ){
        $cupon = $arParams['new_coupon'];
    } else {
        $cupon = project\my_coupon::get_active_coupon();
    }

	$basketInfo = basketInfo( $cupon );
	$arResult['arBasket'] = $basketInfo['arBasket'];
}



$this->IncludeComponentTemplate();