<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

foreach ( $arResult['arBasket'] as $basketItem ){
	
	$el = $basketItem->el;
	$product = $basketItem->product; 
	$price = $basketItem->price;
	$discPrice = $basketItem->discPrice;
	$quantity = $basketItem->getField('QUANTITY');  ?>

	<tr>
		<td class="image">
			<a href="<?=$product['DETAIL_PAGE_URL']?>" style="max-width:120px">
				<img src="<?=rIMG(intval($el['PROPERTY_PHOTO_ID_VALUE'])>0?$el['PROPERTY_PHOTO_ID_VALUE']:$el['DETAIL_PICTURE'], 4, 120, 99)?>" style="max-width:120px">
			</a>
		</td>

		<td class="info">
		
			<div class="name">
				<a href="<?=$product['DETAIL_PAGE_URL']?>"><?=htmlspecialchars_decode($basketItem->product_name)?></a>
			</div>

			<!--<div class="cat">
				<a href="/">Футболка с 3D принтом</a>
			</div>-->
			
		</td>

		<td class="size" data-column="Размер"><?
			if($el['PROPERTY_COLOR_VALUE']){
				echo tools\el::info($el['PROPERTY_COLOR_VALUE'])['NAME'];
			}
			if($el['PROPERTY_SIZE_VALUE']){
				if($el['PROPERTY_COLOR_VALUE']){    echo ' / ';    }
				echo tools\el::info($el['PROPERTY_SIZE_VALUE'])['NAME'];
			}
		?></td>

		<td class="amount" data-column="Количество">
			<div class="box">
			
				<a style="cursor:pointer" class="minus to___process"></a>
				
				<input item_id="<?=$basketItem->getField('ID')?>" price="<?=$price?>" disc_price="<?=$discPrice?>" name="quantity" type="text" value="<?=round($quantity, 0)?>" data-minimum="1" data-maximum="999" maxlength="3" class="input quantity_input_<?=$basketItem->getField('ID')?>">
				
				<a style="cursor:pointer" class="plus to___process"></a>
				
			</div>
		</td>

		<td class="price" data-column="Цена"><?=number_format($discPrice*$quantity, 0, ",", " ")?> <span class="currency">:</span></td>
		<td class="basket__delete_td">

			<a style="cursor:pointer" class="basket___delete basket___remove to___process" item_id="<?=$basketItem->getField('ID')?>" title="Удалить"><span class="basket__delete_title">Удалить</span></a>
		</td>
	</tr>

<? } ?>