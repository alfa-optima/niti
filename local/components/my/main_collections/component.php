<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

\Bitrix\Main\Loader::includeModule('aoptima.project');

use AOptima\Project as project;

// Список коллекций на главной
$main_collections = project\collection::getMainList();

if (count($main_collections) > 0) {






    $result = [];

    foreach ($main_collections as $block) {

        foreach ($block as $collection) {

            //echo '<pre style="font-size: 17px;background-color: #363636;color: #fff;padding: 20px;line-height: 26px;">';
            //print_r($collection);
            //echo '</pre>';

            $result[] = [
                'name' => $collection['NAME'],
                'link' => $collection['DETAIL_PAGE_URL'],
                'img_v' => $collection['PROPERTY_VERTICAL_PHOTO_VALUE'],
                'img_h' => $collection['PROPERTY_HORIZONTAL_PHOTO_VALUE'],

                //'img_v' => 'http://new.niti-niti.ru/upload/imager/91da432607b2f32652dc49c60166611f.jpg',
                //'img_h' => 'http://new.niti-niti.ru/upload/imager/91da432607b2f32652dc49c60166611f.jpg',



            ];
        }

    }



    echo "<script>var main_collections = " . json_encode($result) . " ;</script>";
}

/*
 *
if (count($main_collections) > 0) { ?>

  <section class="collections">
    <div class="cont">

      <div class="block_title">
        <div>коллекции</div>
      </div>

        <?
        echo '<pre>';
        print_r($main_collections);
        echo '</pre>';

        foreach ($main_collections as $block_collections) {
            shuffle($block_collections); ?>

          <div class="hor_scroll">
            <div class="grid">

              <div class="item_wrap">

                  <? $cnt = 0;
                  foreach ($block_collections as $collection) {
                      $cnt++;

                      $height = 180;
                      if (in_array($cnt, array(1, 7))) {
                          $height = 385;
                      } ?>

                    <a href="<?= $collection['DETAIL_PAGE_URL'] ?>" class="item">
                        <? if (intval($collection['DETAIL_PICTURE']) > 0) { ?>
                          <img src="<?= rIMG($collection['DETAIL_PICTURE'], 5, 270, $height) ?>">
                        <? } else { ?>
                          <div style="display:block; width: 270px; height: <?= $height ?>px;"></div>
                        <? } ?>
                      <div class="name"><?= $collection['NAME'] ?></div>
                    </a>

                      <? if (
                          in_array($cnt, array(2, 5, 7))
                          &&
                          $cnt < count($block_collections)
                      ) {
                          echo '</div><div class="item_wrap">';
                      }

                  } ?>

              </div>

            </div>
          </div>

        <? } ?>

    </div>
  </section>


<? } ?>
