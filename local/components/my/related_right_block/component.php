<? if( !defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true )die();

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$aksessuary_code = 'aksessuary';

$arResult = false;

$el = $arParams['el'];

$sect_id = tools\el::sections($el['ID'])[0]['ID'];
$chain = tools\section::chain($sect_id);
$el_type_section_id = $chain[1]['ID'];

$el_sect = tools\section::info($el_type_section_id);


if( intval($el['PROPERTY_PRINT_ID_VALUE']) > 0 ){

	$genderTypes = project\gender_type::list();
	unset($genderTypes['all']);
	
	foreach ( $genderTypes as $code => $genderType ){
		
		// Запрашиваем разделы c данным типом
		$dbSections = \CIBlockSection::GetList(
			Array("SORT"=>"ASC"),
			Array(
				"ACTIVE" => "Y",
				"DEPTH_LEVEL" => 2,
				"UF_GENDER_GROUP" => $genderType['ID'],
				"IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
			), false, array('UF_TYPE_PIC', 'UF_NAME')
		);
		while ( $section = $dbSections->GetNext() ){
			
			// Ищем 1 элемент в этом разделе
			$filter = Array(
				"IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
				"ACTIVE" => "Y",
				"SECTION_ID" => $section['ID'],
				"INCLUDE_SUBSECTIONS" => "Y",
				"PROPERTY_PRINT_ID" => $el['PROPERTY_PRINT_ID_VALUE']
			);
			$fields = Array( "ID", "DETAIL_PAGE_URL", "PROPERTY_PRINT_ID" );
			$dbElements = \CIBlockElement::GetList(
				array("SORT"=>"ASC"), $filter, false, array("nTopCount"=>1), $fields
			);
			while ($element = $dbElements->GetNext()){
				
				// собираем в массив
				$fields = array(
					'NAME' => $section['UF_NAME']?$section['UF_NAME']:$section['NAME'],
					'URL' => $element['DETAIL_PAGE_URL'],
					'ACTIVE' => $section['ID']==$el_type_section_id?'Y':'N'
				);
				if( intval($section['UF_TYPE_PIC']) > 0 ){
					$fields['PIC_SRC'] = \CFile::GetPath($section['UF_TYPE_PIC']);
					$finfo = \CFile::GetFileArray($section['UF_TYPE_PIC']);
					$fields['WIDTH'] = $finfo['WIDTH'];
					$fields['HEIGHT'] = $finfo['HEIGHT']/2;
				}
				
				$type_name = $genderType['ALT_VALUE']?$genderType['ALT_VALUE']:$genderType['VALUE'];
				
				$arResult[$type_name]['ITEMS'][$section['ID']] = $fields;
				
			}
			
		}


	}

	
	
	// Запрашиваем разделы аксессуаров
	$aksessuary_sections = \CIBlockSection::GetList(
		Array("SORT"=>"ASC"),
		Array(
			"ACTIVE" => "Y",
			"DEPTH_LEVEL" => 1,
			"CODE" => $aksessuary_code,
			"IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
		), false, array('UF_TYPE_PIC', 'UF_NAME')
	);
	if ( $aksessuary_section = $aksessuary_sections->GetNext() ){
		
		$type_name = 'Аксессуары';
		
		$dbSections = \CIBlockSection::GetList(
			Array("SORT"=>"ASC"),
			Array(
				"ACTIVE" => "Y",
				"DEPTH_LEVEL" => 2,
				"SECTION_ID" => $aksessuary_section['ID'],
				"IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
			), false, array('UF_TYPE_PIC', 'UF_NAME')
		);
		while ( $section = $dbSections->GetNext() ){
			
			// Ищем 1 элемент в этом разделе
			$filter = Array(
				"IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
				"ACTIVE" => "Y",
				"SECTION_ID" => $section['ID'],
				"INCLUDE_SUBSECTIONS" => "Y",
				"PROPERTY_PRINT_ID" => $el['PROPERTY_PRINT_ID_VALUE']
			);
			$fields = Array( "ID", "DETAIL_PAGE_URL", "PROPERTY_PRINT_ID" );
			$dbElements = \CIBlockElement::GetList(
				array("SORT"=>"ASC"), $filter, false, array("nTopCount"=>1), $fields
			);
			if ($element = $dbElements->GetNext()){
				
				// собираем в массив
				$fields = array(
					'NAME' => $section['UF_NAME']?$section['UF_NAME']:$section['NAME'],
					'URL' => $element['DETAIL_PAGE_URL'],
					'ACTIVE' => $section['ID']==$el_type_section_id?'Y':'N'
				);
				if( intval($section['UF_TYPE_PIC']) > 0 ){
					$fields['PIC_SRC'] = \CFile::GetPath($section['UF_TYPE_PIC']);
					$finfo = \CFile::GetFileArray($section['UF_TYPE_PIC']);
					$fields['WIDTH'] = $finfo['WIDTH'];
					$fields['HEIGHT'] = $finfo['HEIGHT']/2;
				}
				
				$arResult[$type_name]['ITEMS'][$section['ID']] = $fields;
				
			}
			
		}
		
	}
	
	
	foreach( $arResult as $typeName => $arType ){
		$hasActiveElement = false;
		foreach( $arType['ITEMS'] as $item ){
			if( $item['ACTIVE']=='Y' ){    $hasActiveElement = true;    }
		}
		$arResult[$typeName]['ACTIVE'] = $hasActiveElement?'Y':'N';
	}

	
	
}





$this->IncludeComponentTemplate();