<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); 

if( $arResult ){ ?>

	<div class="tabs_container">

		<ul class="tabs">
			<? $cnt = 0;
			foreach ( $arResult as $cat_name => $ar ){ $cnt++; ?>
				<li <? if( $ar['ACTIVE'] == 'Y' ){ ?>class="active"<? } ?>>
					<a href="#<?=md5($cat_name)?>"><?=$cat_name?></a>
				</li>
			<? } ?>
		</ul>

		<? foreach ( $arResult as $cat_name => $ar ){ ?>
		
			<div class="tab_content <? if( $ar['ACTIVE'] == 'Y' ){ ?>active<? } ?>" id="<?=md5($cat_name)?>">
				<div class="grid">
				
					<? $k = 0;
					foreach ( $ar['ITEMS'] as $item ){ $k++; ?>
					
						<a href="<?=$item['URL']?>" class="right___related_item item <?=$k>8?'hide':''?> <?=$item['ACTIVE']=='Y'?'active':''?>">
							<div class="icon">
								<? if( $item['PIC_SRC'] ){ ?>
									<div style="width: <?=$item['WIDTH']?>px; height: <?=$item['HEIGHT']?>px; background: url(<?=$item['PIC_SRC']?>) 0 0 no-repeat;"></div>
								<? } ?>
							</div>
							<div class="name"><?=$item['NAME']?></div>
						</a>
						
					<? } ?>

					<? if( $k > 8 ){ ?>
					
						<a style="cursor:pointer" class="more"></a>
						
					<? } ?>
					
				</div>
			</div>

		<? } ?>
	
	</div>
	
<? } ?>