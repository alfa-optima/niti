<? require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use AOptima\Project as project;
use AOptima\Tools as tools;

if(
    \Bitrix\Main\Loader::includeModule('aoptima.project')
    &&
    \Bitrix\Main\Loader::includeModule('aoptima.tools')
){

    $arURI = tools\funcs::arURI( tools\funcs::pureURL() );
    
    $catalog1LevelCategories = project\catalog_niti::sections_1_level();
    
    // Проверка наличия SEO-страницы (из фабрики поиска) с символьным кодом $arURI[1]
    $searchFabricPage = tools\el::info_by_code( $arURI[ 1 ], project\site::SEARCH_FABRIC_IBLOCK_ID);


    // Каталог
    if(
        $catalog1LevelCategories[ $arURI[ 1 ] ]
        ||
        $arURI[ 1 ] == 'products'
    ){


        include( $_SERVER[ "DOCUMENT_ROOT" ] . "/catalog/catalog.php" );


    // стандартная страница поиска
    } else if(  $arURI[ 1 ] == 'search'  &&  !$arURI[2]  ){


        $APPLICATION->IncludeComponent( "my:search", '' );


    // страница иллюстраторов
    } else if(  $arURI[ 1 ] == 'illustrators'  &&  intval($arURI[2]) > 0  ){


        if( $_GET['schema'] != 'json' ){
            require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
        }

        $filter_cache_illustrators = new project\filter_cache_illustrators();

        $_GET[ $filter_cache_illustrators->searchParamCode ] = intval($arURI[2]);
        $APPLICATION->IncludeComponent( "my:search", '' );

        if( $_GET['schema'] != 'json' ){
            require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
        }


    // страница принтов
    } else if(  $arURI[ 1 ] == 'print'  &&  intval($arURI[2]) > 0  ){


        if( $_GET['schema'] != 'json' ){
            require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
        }

        $filter_cache_ib_prop = new project\filter_cache_ib_prop();
        $_GET[ $filter_cache_ib_prop->propParams['PRINT_ID']['searchParamCode'] ] = intval($arURI[2]);
        $APPLICATION->IncludeComponent( "my:search", '' );

        if( $_GET['schema'] != 'json' ){
            require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
        }

    // страница коллекций
    } else if(  $arURI[ 1 ] == 'collections'  ){

        if( strlen( $arURI[ 2 ] ) > 0 ){
            
            if( $_GET['schema'] != 'json' ){
                require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
            }

            $filter_cache_ib_prop = new project\filter_cache_ib_prop();
            $_GET[ $filter_cache_ib_prop->propParams['COLLECTION']['searchParamCode'] ] = $arURI[ 2 ];
            $APPLICATION->IncludeComponent( "my:search", '' );

            if( $_GET['schema'] != 'json' ){
                require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
            }

        } else {
            include( $_SERVER[ "DOCUMENT_ROOT" ] . "/collections/index.php" );
        }

    // СЕО-страница (фабрика поиска)
    } else if( intval( $searchFabricPage['ID'] ) > 0  &&  !$arURI[2] ){


        project\search_fabric::setGetParams( $searchFabricPage );
        $APPLICATION->IncludeComponent( "my:search", '' );


    } else {


        include( $_SERVER[ "DOCUMENT_ROOT" ] . "/include/areas/404.php" );

    }

}