<? namespace AOptima\Project;
use AOptima\Project as project;


class site {
	
	const API_ACCESS_KEY = 'db6fea603cb04d02a3b5fb2ed514b750';
	
	const CATALOG_PRICE_ID = 1;
	const CATALOG_PRICE_CODE = 'BASE';

	const CATALOG_IBLOCK_ID = 3;
	const CATALOG_IBLOCK_TYPE = 'catalog';

	const SEARCH_CATEGORIES_IBLOCK_ID = 35;
	const GENDERS_IBLOCK_ID = 38;
	const SEARCH_FABRIC_IBLOCK_ID = 37;
	const COUNTRIES_IBLOCK_ID = 4;
	const SKU_IBLOCK_ID = 5;
	const COLOR_IBLOCK_ID = 6;
	const SIZES_IBLOCK_ID = 33;
	const COMMENTS_IBLOCK_ID = 7;
	const BUY_1_CLICK_IBLOCK_ID = 8;
	const BANNERS_IBLOCK_ID = 9;
	const COLLECTIONS_IBLOCK_ID = 10;
	const SKU_PROCESSING_IBLOCK_ID = 11;
	const MAPS_IBLOCK_ID = 12;
	const CONTACTS_IBLOCK_ID = 13;
	const FEEDBACK_IBLOCK_ID = 14;
	const DELIVERY_IBLOCK_ID = 15;
	const NEWS_IBLOCK_ID = 16;
	const MAIN_ADV_IBLOCK_ID = 17;
	const MAIN_LINKS_IBLOCK_ID = 18;
	const TEMP_FID_IBLOCK = 19;
	const PRINTS_IBLOCK_ID = 20;
	const SEO_IBLOCK_ID = 21;
	const VACANCIES_IBLOCK_ID = 23;
	const CONSTR_GROUPS_IBLOCK_ID = 24;
	const CONSTR_COLORS_IBLOCK_ID = 25;
	const CONSTR_TAGS_IBLOCK_ID = 26;
	const CONSTR_ITEMS_IBLOCK_ID = 27;
    const CONSTR_SKU_IBLOCK_ID = 28;
    const ISUBSCRIBES_IBLOCK_ID = 29;
	const PRODUCT_TYPES_IBLOCK_ID = 36;

	const DOCUMENT_ROOT = "/home/bitrix/www";

	const SERVICE_USER_ID = 153;

	const CONSTRUCTOR_NOTES = 32;

	
	const PHONE_PREFIX = '+7 ';
	
	const NITI_COMMENTS_LOGO = SITE_TEMPLATE_PATH."/images/admin_avatar_img.png";
	
	const LATEST_ARRIVAL_CNT = 8;
	
	const USER_LIKES_SESSION = 'userLikes';

	static $user_pay_types = array(
		3 => 1,
		4 => 2
	);
	
	
	const DEFAULT_DELIVERY_PRICE = '-';
	static $calc_ds_list = array( 
		'delivSDEK' => 3,
		'delivBoxberry' => 4
	);
	

	
	
	
	protected $settingsPath = '/local/php_interface/site_settings/';
	
	// настройки по умолчанию
	protected $settings = array(
		'MAP_CENTER' => array(
			'NAME' => 'Координаты центра карты (магазины)',
			'VALUE' => '55.734435043607, 37.629476418501'
		),
		'MAP_ZOOM' => array(
			'NAME' => 'Масштаб карты (магазины)',
			'VALUE' => 16
		)
	);

	
	
	

	// Получение настроек по-умолчанию
	public function defaultSettings(){
		return $this->settings;
	}
	
	// Получение текущих настроек
	public function getSettings(){
		$settings = $this->defaultSettings();
		$settingsKeys = array_keys($settings);
		foreach ($settingsKeys as $key){
			$value = false;
			$file = $_SERVER['DOCUMENT_ROOT'].$this->settingsPath.$key.'.txt';
			if( file_exists($file) ){
				$value = file_get_contents($file);
				if(strlen($value) > 0){    $settings[$key]['VALUE'] = $value;    }
			}
		}
		return $settings;
	}
	
	// Редактирование настроек
	public function updateSettings($settings){
		$settingsPath = $_SERVER['DOCUMENT_ROOT'].$this->settingsPath;
		if( !file_exists($settingsPath) ){     mkdir($settingsPath, 0700);     }
		if(
			$settings
			&&
			is_array($settings)
			&&
			count($settings) > 0
		){
			$defaultSettings = $this->defaultSettings();
			$defaultSettingsKeys = array_keys($defaultSettings);
			foreach ( $settings as $key => $value ){
				if( in_array($key, $defaultSettingsKeys) ){
					$file_path = $settingsPath.$key.".txt";
					$file = fopen($file_path, "w"); 
					$res = fwrite($file,  $value);
					fclose($file);
				}
			}
		}
	}


	


    static function isSearchFabricPage(){
        $pureURL = \AOptima\Tools\funcs::pureURL();
        $arURL = \AOptima\Tools\funcs::arURI( $pureURL );
        if( strlen($arURL[1]) > 0 ){
            $fabricPage = \AOptima\Tools\el::info_by_code( $arURL[1], project\site::SEARCH_FABRIC_IBLOCK_ID );
            if( intval($fabricPage['ID']) > 0 ){
                return $fabricPage['CODE'];
            }
        }
        return false;
    }
    static function isIllustratorPage(){
        $pureURL = \AOptima\Tools\funcs::pureURL();
        $arURL = \AOptima\Tools\funcs::arURI( $pureURL );
        if(
            \AOptima\Tools\funcs::string_begins_with(project\search::$illustratorsUrl, $pureURL)
            &&
            intval($arURL[2]) > 0
        ){
            return intval($arURL[2]);
        } else {
            return false;
        }
    }
    static function isPrintPage(){
        $pureURL = \AOptima\Tools\funcs::pureURL();
        $arURL = \AOptima\Tools\funcs::arURI( $pureURL );
        if(
            \AOptima\Tools\funcs::string_begins_with(project\search::$printUrl, $pureURL)
            &&
            intval($arURL[2]) > 0
        ){
            return intval($arURL[2]);
        } else {
            return false;
        }
    }
    static function isCollectionPage(){
        $pureURL = \AOptima\Tools\funcs::pureURL();
        $arURL = \AOptima\Tools\funcs::arURI( $pureURL );
        if(
            \AOptima\Tools\funcs::string_begins_with(project\search::$collectionsUrl, $pureURL)
            &&
            strlen($arURL[2]) > 0
        ){
            return trim(strip_tags($arURL[2]));
        } else {
            return false;
        }
    }
    static function isSpecificPage(){
        return static::isIllustratorPage()||static::isPrintPage()||static::isCollectionPage();
    }
	
	
	
	
	
	// Удаление старых файлов
	static function deleteOldImages(){
	    \Bitrix\Main\Loader::includeModule('iblock');
		// Кешируем
		$obCache = new \CPHPCache();
		$cache_time = 24*60*60; // сутки
		$cache_id = 'deleteOldImages';
		$cache_path = '/deleteOldImages/';
		if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){   $vars = $obCache->GetVars();   extract($vars);   } elseif($obCache->StartDataCache()){      $date = date("d.m.Y", time());      $obCache->EndDataCache(array('date' => $date));    }
		$cur_date = date("d.m.Y", time());
		// Если даты отличаются, запускаем удаление старых временных файлов
		if ($cur_date != $date){
			// Выбираем элементы старше 1 суток
			global $DB;
			$elements = \CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=>static::TEMP_FID_IBLOCK, '<DATE_ACTIVE_FROM' => date($DB->DateFormatToPHP(FORMAT_DATETIME), time()-86400)), false, false, Array("ID", "NAME", "ACTIVE_FROM"));
			while ($element = $elements->GetNext()){
				\CIBlockElement::Delete($element['ID']);
				\CFile::Delete(intval($element['NAME']));
			}
		}
	}
	
	
	
	// добавляем в спец.инфоблок id временного файла
	static function saveTempEl($fid){
		$el = new CIBlockElement;
		global $DB;
		$fields = Array(
		  "IBLOCK_ID"      => static::TEMP_FID_IBLOCK,
		  "NAME"           => $fid,
		  "ACTIVE_FROM"    => date($DB->DateFormatToPHP(FORMAT_DATETIME), time()),
		  "ACTIVE"         => "Y"
		);
		$el->Add($fields);
	}
	
	
	
	// удаление временного элемента
	static function deleteTempEl($fid){
	    \Bitrix\Main\Loader::includeModule('iblock');
		$dbElements = \CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>static::TEMP_FID_IBLOCK, "=NAME"=>$fid), false, Array("nTopCount"=>1), Array("ID", "NAME"));
		while ($element = $dbElements->GetNext()){
			\CIBlockElement::Delete($element['ID']);
		}
	}
	
	
	
	
	
	
	
	

	
	
	
	
	
	
	
}