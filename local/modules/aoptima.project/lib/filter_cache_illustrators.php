<? namespace AOptima\Project;
use AOptima\Project as project;



class filter_cache_illustrators extends filter_cache {


    const CACHE_PATH = '/local/php_interface/filter_caches/illustrators/';


    public $searchParamCode = 'illustrator';


    function __construct(){

        $this->cache_path = $_SERVER['DOCUMENT_ROOT'].static::CACHE_PATH;

    }



    // Создание кэшей
    public function create(){

        // Проверка наличия рабочей папки
        if( !file_exists( $this->cache_path ) ){
            mkdir( $this->cache_path, 0700 );
        }

        \Bitrix\Main\Loader::includeModule('iblock');

        // Массив для новых файлов кэша
        $newFiles = [];

        // Текущий набор файлов кэша
        $currentFiles = $this->getCurrentFiles( $this->cache_path );

        // Получим полный набор иллюстраторов
        $allIllustrators = project\illustrator::getList();

        // Переберём категории
        foreach ( $allIllustrators as $illustrator_id ){

            $product_ids = [];

            // Получим товары иллюстратора
            $filter = [
                "IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
                "ACTIVE" => "Y",
                "INCLUDE_SUBSECTIONS" => "Y",
                "PROPERTY_USER_ID" => $illustrator_id
            ];
            $fields = [ "ID", 'PROPERTY_USER_ID' ];
            $products = \CIBlockElement::GetList(
                [ "SORT" => "ASC" ], $filter, false, false, $fields
            );
            while ($product = $products->GetNext()){
                $product_ids[] = $product['ID'];
            }
            $product_ids = array_unique($product_ids);

            // Сохраняем файл кэша
            $fileName = $illustrator_id.".json";
            $filePath = $this->cache_path.$fileName;
            $file = fopen($filePath, "w");
            $res = fwrite( $file,  json_encode( $product_ids ) );
            fclose($file);

            $newFiles[] = $fileName;
        }

        // Удаляем устаревшие файлы (те, которыйх нет в $newFiles)
        if( count($currentFiles) > 0 ){
            $filesToDelete = array_diff($currentFiles, $newFiles);
            foreach ( $filesToDelete as $fileName ){
                unlink( $this->cache_path.$fileName );
            }
        }
    }







}

