<? namespace AOptima\Project;
use AOptima\Project as project;



class search {


    const BASE_URL = '/search/';



    static $illustratorsUrl = '/illustrators/';
    static $printUrl = '/print/';
    static $collectionsUrl = '/collections/';


    public $params = [];
    public $paramClasses = [];


    function __construct(){

        $filter_cache_gender = new project\filter_cache_gender();
        $filter_cache_illustrators = new project\filter_cache_illustrators();
        $filter_cache_product_types = new project\filter_cache_product_types();
        $filter_cache_ib_prop = new project\filter_cache_ib_prop();
        $filter_cache_ib_prop_tp = new project\filter_cache_ib_prop_tp();

        $this->paramClasses = [
            $filter_cache_gender->searchParamCode => 'filter_cache_gender',
            $filter_cache_ib_prop->propParams['COLLECTION']['searchParamCode'] => 'filter_cache_ib_prop',
            $filter_cache_ib_prop->propParams['TAGS']['searchParamCode'] => 'filter_cache_ib_prop',
            $filter_cache_ib_prop->propParams['PRINT_ID']['searchParamCode'] => 'filter_cache_ib_prop',
            $filter_cache_ib_prop_tp->propParams['COLOR']['searchParamCode'] => 'filter_cache_ib_prop_tp',
            $filter_cache_ib_prop_tp->propParams['SIZE']['searchParamCode'] => 'filter_cache_ib_prop_tp',
            $filter_cache_product_types->searchParamCode => 'filter_cache_product_types',
            $filter_cache_illustrators->searchParamCode => 'filter_cache_illustrators',
            'sort' => 'filter_cache_sorts',
        ];

        $this->params = [
            'query',
            project\search_category::$searchParamCode,
            $filter_cache_gender->searchParamCode,
            $filter_cache_ib_prop->propParams['COLLECTION']['searchParamCode'],
            $filter_cache_ib_prop->propParams['TAGS']['searchParamCode'],
            $filter_cache_ib_prop_tp->propParams['COLOR']['searchParamCode'],
            $filter_cache_ib_prop_tp->propParams['SIZE']['searchParamCode'],
            $filter_cache_ib_prop->propParams['PRINT_ID']['searchParamCode'],
            $filter_cache_illustrators->searchParamCode,
            $filter_cache_product_types->searchParamCode,
            'page',
            'limit',
            'sort',
            'schema',
        ];
    }






    static function getBaseURL( $arResult ){
        $baseURL = static::BASE_URL;
        if( project\site::isSpecificPage() ){
            $baseURL = \AOptima\Tools\funcs::pureURL();
        }
        return $baseURL;
    }



    static function request(){

        \Bitrix\Main\Loader::includeModule('aoptima.tools');

        $start_time = microtime(true);

        $arResult = [
            'get' => [],
            'params' => [],
            'getUrl' => null,
            'catsCategories' => [],
            'groupCategories' => [],
            'intersectCatalogCategories' => [],
            'products_ids' => [],
            'response' => [
                'query' => null,
                'withoutQueryURL' => null,
                'sorts' => [],
                'filters' => [
                    'resetUrl' => null,
                    'appliedCount' => 0,
                    'appliedPath' => [],
                    'resets' => [],
                    'staticFiltersReset' => null,
                    'staticFilters' => [],
                    'filters' => [],
                ],
                'products' => [],
                'illustrators' => [],
                'prints' => [],
                'collections' => [],
                'pagination' => null,
                'metadata' => [
                    'title' => null,
                    'resultCount' => 0,
                    'seoMetadata' => [
                        'searchTitle' => null,
                        'pageDescription' => null,
                        'pageKeywords' => null,
                        'canonicalURL' => null,
                    ],
                    'illustrator' => null,
                    'print' => null,
                    'collection' => null,
                ],
            ],
            'caches' => []
        ];

        if( count( $_GET ) > 0 ){

            $search = new project\search();

            // Обработка массива $arResult['get']
            $arResult['response']['withoutQueryURL'] = \AOptima\Tools\funcs::pureURL( $_SERVER['REQUEST_URI'] );

            foreach ( $_GET as $key => $value ){
                if(
                    in_array($key, $search->params)
                    &&
                    ( is_string($value) || is_int($value) )
                ){
                    $arResult['get'][$key] = trim(strip_tags($value));
                    if( $key != 'query' ){
                        $arResult['response']['withoutQueryURL'] = \AOptima\Tools\funcs::addGetParamToURL($arResult['response']['withoutQueryURL'], $key, trim(strip_tags($value)));
                    }
                }
            }

        }

        // Сбор параметров
        $arResult = static::createParams( $arResult );

        // Получение инфы по фильтру
        $arResult = project\search_filter::getItems( $arResult );

        $arResult['response']['filters']['staticFiltersReset'] = \AOptima\Tools\funcs::removeGetParamFromURL( $arResult['getUrl'], project\search_category::$searchParamCode );
        $arResult['response']['filters']['staticFiltersReset'] = \AOptima\Tools\funcs::addGetParamToURL( $arResult['response']['filters']['staticFiltersReset'], 'schema', 'json' );

        // Получим категории каталога - для последующей фильтрации товаров
        $arResult = static::catCatalogCategories( $arResult );
        $arResult = static::groupCatalogCategories( $arResult );
        $arResult = static::intersectCatalogCategories( $arResult );

        // ПОИСК
        if( strlen( $arResult['params']['query'] ) > 0 ){

            $arResult['response']['query'] = $arResult['params']['query'];

            // Поиск ТОВАРОВ по поисковому индексу
            $arResult['products_ids'] = project\search_products::searchByQ( $arResult['params']['query'] );

            // $arResult['products_ids'] = \AOptima\Project\elastic::search($arResult['params']['query'], [
            //     'fields' => ['name', 'article'],
            //     'index' => 'model_index',
            //     'return' => '_id'
            // ]);

            // Поиск ИЛЛЮСТРАТОРОВ
            $illustrators = project\search_illustrators::searchByName( $arResult['params']['query'] );
            foreach ( $illustrators as $illustrator ){
                $arResult['response']['illustrators'][] = [
                    'id' => $illustrator['ID'],
                    'name' => \AOptima\Tools\user::getFullName( $illustrator['ID'] ),
                    'link' => $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].'/illustrators/'.$illustrator['ID'].'/',
                ];
            }

            // Поиск ПРИНТОВ
            $prints = project\search_prints::searchByQ( $arResult['params']['query'] );
            foreach ( $prints as $print ){
                $arResult['response']['prints'][] = [
                    'id' => $print['ID'],
                    'name' => $print['NAME'],
                    'link' => $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].'/print/'.$print['ID'].'/',
                ];
            }

            // Поиск КОЛЛЕКЦИЙ
            $collections = project\search_collections::searchByQ( $arResult['params']['query'] );
            foreach ( $collections as $collection ){
                $arResult['response']['collections'][] = [
                    'id' => $collection['ID'],
                    'name' => $collection['NAME'],
                    'link' => $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].$collection["DETAIL_PAGE_URL"],
                ];
            }

        // Если поискового запроса нет
        } else {

            // Если выбрана сортировка по релевантности, то она не актуальна, т.к. нет поискового запроса
            // или если указана несуществующая сортировка
            // То сменим сортировку, возьмём первую в списке
            if(
                $arResult['params']['sort'] == 'relevant'
                ||
                !isset( project\search_sorts::$items[ $arResult['params']['sort'] ] )
            ){
                $arResult['params']['sort'] = project\search_sorts::$items[ array_keys(project\search_sorts::$items)[0] ]['CODE'];
            }

            // Получим все товары каталога, используя кэш по текущей сортировке
            $filter_cache_sorts = new project\filter_cache_sorts();
            $arResult['products_ids'] = $filter_cache_sorts->getCacheProductsIDs( $arResult['params']['sort'] );

        }

        // Деактивация значений пунктов фильтра (до учёта применённых фильтров)
        $arResult = project\search_filter::deactivateFilterValues_1( $arResult );

        // Применение фильтров
        $arResult = static::doFilterProducts( $arResult );

        // Деактивация значений пунктов фильтра (с учётом применённых фильтров)
        $arResult = project\search_filter::deactivateFilterValues_2( $arResult );

        // Применение лимита (при наличии)
        $arResult = static::applyLimit( $arResult );

        // Добавление инфы по сортировке
        $arResult = static::getSortsInfo( $arResult );

        $end_time = microtime(true);

        // Запрос товаров по ID
        $arResult = static::getProducts( $arResult );

        // Обработка подмассива metadata
        $arResult = static::updateMetaData( $arResult );
        
        // Обработка массива перед выводом
        $arResult = static::updateArResult( $arResult );

        $arResult['response']['do_time'] = ($end_time - $start_time);

        return $arResult;
    }



    // Добавление инфы по сортировке
    static function getSortsInfo( $arResult ){
        $sorts = project\search_sorts::getList();
        foreach ( $sorts as $sort ){

            $applied = $sort['CODE']==$arResult['params']['sort'];

            if( project\site::isSearchFabricPage() ){
                $link = \AOptima\Tools\funcs::pureURL();
            } else {
                $link = $arResult['getUrl'];
            }
            $link = \AOptima\Tools\funcs::addGetParamToURL( $link, 'sort', $sort['CODE'] );
            $link = \AOptima\Tools\funcs::addGetParamToURL( $link, 'schema', 'json' );

            $arResult['response']['sorts'][] = [
                'name' => $sort['CODE'],
                'label' => $sort['NAME'],
                'applied' => $applied,
                'link' => $link,
                'experiences' => [
                    [
                        'name' => 'control',
                        'value' => 'radio',
                    ]
                ]
            ];
        }
        return $arResult;
    }



    // Сбор параметров
    static function createParams( $arResult ){

        $filter_cache_gender = new project\filter_cache_gender();
        $filter_cache_illustrators = new project\filter_cache_illustrators();
        $filter_cache_product_types = new project\filter_cache_product_types();
        $filter_cache_ib_prop = new project\filter_cache_ib_prop();
        $filter_cache_ib_prop_tp = new project\filter_cache_ib_prop_tp();
        
        // query
        $arResult['response']['filters']['resetUrl'] = static::getBaseURL( $arResult );
        if( strlen($arResult['get']['query']) > 0 ){
            $arResult['params']['query'] = $arResult['get']['query'];
            $arResult['response']['filters']['resetUrl'] = \AOptima\Tools\funcs::addGetParamToURL( $arResult['response']['filters']['resetUrl'], 'query', $arResult['params']['query'] );
        }
        $params = [];
        // cats
        if( strlen($arResult['get'][ project\search_category::$searchParamCode ]) > 0 ){
            $params[] = project\search_category::$searchParamCode;
        }
        // group
        if( strlen($arResult['get'][ $filter_cache_gender->searchParamCode ]) > 0 ){
            $params[] = $filter_cache_gender->searchParamCode;
        }
        // product_type
        if( strlen($arResult['get'][ $filter_cache_product_types->searchParamCode ]) > 0 ){
            $params[] = $filter_cache_product_types->searchParamCode;
        }
        // collection
        if( strlen($arResult['get'][ $filter_cache_ib_prop->propParams['COLLECTION']['searchParamCode'] ]) > 0 ){
            $params[] = $filter_cache_ib_prop->propParams['COLLECTION']['searchParamCode'];
        }
        // tags
        if( strlen($arResult['get'][ $filter_cache_ib_prop->propParams['TAGS']['searchParamCode'] ]) > 0 ){
            $params[] = $filter_cache_ib_prop->propParams['TAGS']['searchParamCode'];
        }
        // color
        if( strlen($arResult['get'][ $filter_cache_ib_prop_tp->propParams['COLOR']['searchParamCode'] ]) > 0 ){
            $params[] = $filter_cache_ib_prop_tp->propParams['COLOR']['searchParamCode'];
        }
        // size
        if( strlen($arResult['get'][ $filter_cache_ib_prop_tp->propParams['SIZE']['searchParamCode'] ]) > 0 ){
            $params[] = $filter_cache_ib_prop_tp->propParams['SIZE']['searchParamCode'];
        }
        // illustrator
        if( strlen($arResult['get'][ $filter_cache_illustrators->searchParamCode ]) > 0 ){
            $params[] = $filter_cache_illustrators->searchParamCode;
        }
        // print
        if( strlen($arResult['get'][ $filter_cache_ib_prop->propParams['PRINT_ID']['searchParamCode'] ]) > 0 ){
            $params[] = $filter_cache_ib_prop->propParams['PRINT_ID']['searchParamCode'];
        }

        foreach ( $params as $param ){
            if( $param == $filter_cache_ib_prop->propParams['COLLECTION']['searchParamCode'] ){
                $arResult['params'][ $param ] = \AOptima\Tools\el::getIDsByCodes( $arResult['get'][ $param ], project\site::COLLECTIONS_IBLOCK_ID );
            } else if( $param == $filter_cache_ib_prop->propParams['TAGS']['searchParamCode'] ){
                $arResult['params'][ $param ] = \AOptima\Tools\el::getIDsByCodes( $arResult['get'][ $param ], project\site::CONSTR_TAGS_IBLOCK_ID );
            } else if( $param == $filter_cache_ib_prop_tp->propParams['COLOR']['searchParamCode'] ){
                $arResult['params'][ $param ] = \AOptima\Tools\el::getIDsByCodes( $arResult['get'][ $param ], project\site::COLOR_IBLOCK_ID );
            } else if( $param == $filter_cache_ib_prop_tp->propParams['SIZE']['searchParamCode'] ){
                $arResult['params'][ $param ] = \AOptima\Tools\el::getIDsByCodes( $arResult['get'][ $param ], project\site::SIZES_IBLOCK_ID );
            } else {
                if( substr_count($arResult['get'][ $param ], ',') ){
                    $arResult['params'][ $param ] = explode(',', $arResult['get'][ $param ]);
                    $arResult['params'][ $param ] = array_unique($arResult['params'][ $param ]);
                } else {
                    $arResult['params'][ $param ] = [ $arResult['get'][ $param ] ];
                }
            }
        }

        // page
        $arResult['params']['page'] = intval($arResult['get']['page'])>1?$arResult['get']['page']:1;
        // limit
        if( intval($arResult['get']['limit']) > 0 ){
            $arResult['params']['limit'] = intval($arResult['get']['limit']);
        }
        // sort
        if( strlen($arResult['get']['sort']) > 0 ){
            $arResult['params']['sort'] = $arResult['get']['sort'];
        } else {
            $arResult['params']['sort'] = project\search_sorts::DEFAULT_SORT;
        }
        $arResult['response']['filters']['resetUrl'] = \AOptima\Tools\funcs::addGetParamToURL( $arResult['response']['filters']['resetUrl'], 'schema', 'json' );
        return $arResult;
    }



    // Применение фильтров к товарам
    static function doFilterProducts( $arResult ){

        if( count( $arResult['products_ids'] ) > 0 ){

            $filteredProducts = [];

            $ib_prop_cache = new project\filter_cache_ib_prop();
            $ib_prop_cache_tp = new project\filter_cache_ib_prop_tp();
            $categories_cache = new project\filter_cache_categories();
            $filter_cache_gender = new project\filter_cache_gender();
            $filter_cache_illustrators = new project\filter_cache_illustrators();
            $filter_cache_product_types = new project\filter_cache_product_types();

            // Фильтрация по пунктам фильтра (СВОЙСТВА - КАТАЛОГ)
            foreach ( $arResult['response']['filters']['filters'] as $fkey => $filterItem ){
                if( in_array($filterItem['type'], $ib_prop_cache->propSearchParams) ){
                    $param_name = $filterItem['type'];
                    $prop_code = $ib_prop_cache->getPropCodeByParamName( $param_name );
                    // Если пункт сейчас учавствует в фильтрации
                    if( isset( $arResult['params'][ $param_name ] ) ){
                        // массив для сбора ID товаров
                        $itemProducts = [];
                        // Перебираем значения пункта фильтра
                        foreach ( $arResult['params'][ $param_name ] as $item_id ){
                            // Получим из кэша товары по выбранному в фильтре значению
                            $cacheProductsIDs = $arResult['caches']['props'][$prop_code][$item_id];
                            // мёржим с уже собранными значениями
                            $itemProducts = array_merge($itemProducts, $cacheProductsIDs);
                            unset($cacheProductsIDs);
                        }
                        $itemProducts = array_unique($itemProducts);
                        $filteredProducts[] = $itemProducts;
                    }
                }
            }

            // Фильтрация по пунктам фильтра (СВОЙСТВА - ТП)
            foreach ( $arResult['response']['filters']['filters'] as $fkey => $filterItem ){
                if( in_array($filterItem['type'], $ib_prop_cache_tp->propSearchParams) ){
                    $param_name = $filterItem['type'];
                    $prop_code = $ib_prop_cache_tp->getPropCodeByParamName( $param_name );

                    // Если пункт сейчас учавствует в фильтрации
                    if( isset( $arResult['params'][ $param_name ] ) ){
                        // массив для сбора ID товаров
                        $itemProducts = [];
                        // Перебираем значения пункта фильтра
                        foreach ( $arResult['params'][ $param_name ] as $item_id ){
                            // Получим из кэша товары по выбранному в фильтре значению
                            $cacheProductsIDs = $arResult['caches']['props_tp'][$prop_code][$item_id];
                            // мёржим с уже собранными значениями
                            $itemProducts = array_merge($itemProducts, $cacheProductsIDs);
                            unset($cacheProductsIDs);
                        }
                        $itemProducts = array_unique($itemProducts);
                        $filteredProducts[] = $itemProducts;
                    }
                }
            }

            // Фильтрация по пунктам фильтра (иллюстраторы)
            foreach ( $arResult['response']['filters']['filters'] as $fkey => $filterItem ){
                if(
                    $filterItem['type'] == $filter_cache_illustrators->searchParamCode
                    &&
                    // Если пункт сейчас учавствует в фильтрации
                    isset( $arResult['params'][ $filter_cache_illustrators->searchParamCode ] )
                ){
                    // массив для сбора ID товаров
                    $itemProducts = [];
                    // Перебираем значения пункта фильтра
                    foreach ( $arResult['params'][ $filter_cache_illustrators->searchParamCode ] as $illustrator_id ){
                        // Получим из кэша товары по выбранному в фильтре значению
                        $cacheProductsIDs = $arResult['caches'][ $filter_cache_illustrators->searchParamCode ][ $illustrator_id ];
                        if( is_array($cacheProductsIDs) ){
                            // мёржим с уже собранными значениями
                            $itemProducts = array_merge($itemProducts, $cacheProductsIDs);
                            unset($cacheProductsIDs);
                        }
                    }
                    $itemProducts = array_unique($itemProducts);
                    $filteredProducts[] = $itemProducts;
                }
            }

            // Фильтрация по категориям
            if( count($arResult['intersectCatalogCategories']) > 0 ){
                $itemProducts = [];
                foreach ( $arResult['intersectCatalogCategories'] as $section_id ){
                    // Получение товаров из кэша по ID категории
                    $cacheProductsIDs = $categories_cache->getCacheProductsIDs( $section_id );
                    // мёржим с уже собранными значениями
                    $itemProducts = array_merge($itemProducts, $cacheProductsIDs);
                }
                $itemProducts = array_unique($itemProducts);
                $filteredProducts[] = $itemProducts;
            } else if( count($arResult['params'][ project\search_category::$searchParamCode ]) > 0 ){
                $filteredProducts[] = [ 0 ];
            }

            // Ищём пересечения $arResult['products_ids'] с отфильтрованными
            foreach ( $filteredProducts as $arProducts ){
                $arResult['products_ids'] = array_intersect($arResult['products_ids'], $arProducts);
            }

        }

        return $arResult;
    }



    // Применение лимита (при наличии)
    static function applyLimit( $arResult ){
        if(
            count( $arResult['products_ids'] ) > 0
            &&
            intval( $arResult['params']['limit'] ) > 0
        ){
            $arResult['products_ids'] = array_slice($arResult['products_ids'], 0, $arResult['params']['limit']);
        }
        return $arResult;
    }



    // Получение ID категорий каталога (из категорий поиска)
    static function catCatalogCategories( $arResult ){
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        $arResult['catsCategories'] = [];
        if( is_array( $arResult['params'][ project\search_category::$searchParamCode ] ) ){
            foreach ( $arResult['params'][ project\search_category::$searchParamCode ] as $key => $cat_code ){
                $cat = \AOptima\Tools\el::info_by_code($cat_code, project\site::SEARCH_CATEGORIES_IBLOCK_ID);
                if(
                    intval($cat['ID']) > 0
                    &&
                    is_array($cat['PROPERTY_CAT_SECTIONS_VALUE'])
                    &&
                    count($cat['PROPERTY_CAT_SECTIONS_VALUE']) > 0
                ){
                    $arResult['catsCategories'] = array_merge($arResult['catsCategories'], $cat['PROPERTY_CAT_SECTIONS_VALUE']);
                }
            }
        }
        $arResult['catsCategories'] = array_unique($arResult['catsCategories']);
        return $arResult;
    }



    // Получение ID категорий каталога (из группы/пола)
    static function groupCatalogCategories( $arResult ){
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        $filter_cache_gender = new project\filter_cache_gender();
        $arResult['groupCategories'] = [];
        $genderTypeID = false;
        $genderTypes = project\gender_type::list();
        $genderTypeIDs = [];
        if( is_array( $arResult['params'][ $filter_cache_gender->searchParamCode ] ) ){
            foreach ( $arResult['params'][ $filter_cache_gender->searchParamCode ] as $key => $group_code ){
                if( $genderTypes[ $group_code ] ){
                    $genderTypeIDs[] = $genderTypes[ $group_code ]['ID'];
                }
            }
        }
        \Bitrix\Main\Loader::includeModule('iblock');
        if( count( $genderTypeIDs ) > 0 ){
            $filter = [
            	"ACTIVE" => "Y",
            	"UF_GENDER_GROUP" => $genderTypeIDs,
            	"IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
            ];
            $categories = \CIBlockSection::GetList(
            	[ "SORT"=>"ASC" ], $filter,
                false, [ "UF_GENDER_GROUP" ]
            );
            while ( $category = $categories->GetNext() ){
                $arResult['groupCategories'][] = $category['ID'];
            }
        }
        $arResult['groupCategories'] = array_unique($arResult['groupCategories']);
        return $arResult;
    }


    // Пересечение категорий
    static function intersectCatalogCategories( $arResult ){
        if(
            count($arResult['catsCategories']) > 0
            ||
            count($arResult['groupCategories']) > 0
        ){
            if(
                count($arResult['catsCategories']) > 0
                &&
                count($arResult['groupCategories']) > 0
            ){
                $arResult['intersectCatalogCategories'] = array_intersect( $arResult['catsCategories'], $arResult['groupCategories'] );
            } else if( count( $arResult['catsCategories'] ) > 0 ){
                $arResult['intersectCatalogCategories'] = $arResult['catsCategories'];
            } else if( count( $arResult['groupCategories'] ) > 0 ){
                $arResult['intersectCatalogCategories'] = $arResult['groupCategories'];
            }
        }
        return $arResult;
    }



    // Запрос товаров
    static function getProducts( $arResult ){

        global $APPLICATION;

        // Указание номера страницы пагинации
        global $PAGEN_1;
        $PAGEN_1 = $arResult['params']['page'];
        $productsCount = project\search_products::PAGE_ELEMENT_COUNT;

        $productsFilter = [ 0 ];

        $arResult['response']['pagination']['total'] = 0;
        $arResult['response']['pagination']['perPage'] = $productsCount;
        $arResult['response']['pagination']['pagesCount'] = 0;
        $arResult['response']['pagination']['currentPage'] = $PAGEN_1;

        // Разобъём ID-шники товаров по страницам
        $chunks = array_chunk( $arResult['products_ids'], $productsCount );
        // параметры пагинации
        $arResult['response']['pagination']['total'] = count( $arResult['products_ids'] );
        $arResult['response']['pagination']['pagesCount'] = count( $chunks );

        if (
            is_array($arResult['products_ids'])
            &&
            count($arResult['products_ids']) > 0
        ){
            // Если применена сортировка по релевантности
            if( $arResult['params']['sort'] == 'relevant' ){
                // Если для страницы $PAGEN_1 есть ID-шники товаров
                if( isset( $chunks[ $PAGEN_1 - 1 ] ) ){
                    // Возьмём ID товаров для страницы $PAGEN_1
                    $productsFilter = $chunks[ $PAGEN_1 - 1 ];
                }
            // Если применена другая сортировка
            } else {
                // Берём для фильтра сразу все ID
                $productsFilter = $arResult['products_ids'];
            }
        }

        if(
            $arResult['response']['pagination']['currentPage']
            >
            $arResult['response']['pagination']['pagesCount']
        ){
            $arResult['response']['pagination']['currentPage'] = 1;
        }
        if( $arResult['response']['pagination']['currentPage'] > 1 ){
            $previousPage = $arResult['response']['pagination']['currentPage'] - 1;
        } else {
            $previousPage = null;
        }
        if( intval($previousPage) > 0 ){
            $previousPageLink = \AOptima\Tools\funcs::addGetParamToURL($arResult['getUrl'], 'page', $previousPage);
            $previousPageLink = \AOptima\Tools\funcs::addGetParamToURL( $previousPageLink, 'schema', 'json' );
        } else {
            $previousPageLink = null;
        }
        if( $previousPage == 1 ){
            $previousPageLink = \AOptima\Tools\funcs::removeGetParamFromURL($previousPageLink, 'page');
        }

        if(
            $arResult['response']['pagination']['currentPage']
            <
            $arResult['response']['pagination']['pagesCount']
        ){
            $nextPage = $arResult['response']['pagination']['currentPage'] + 1;
        } else {
            $nextPage = null;
        }
        if( intval($nextPage)>0 ){
            $nextPageLink = \AOptima\Tools\funcs::addGetParamToURL($arResult['getUrl'], 'page', $nextPage);
            $nextPageLink = \AOptima\Tools\funcs::addGetParamToURL( $nextPageLink, 'schema', 'json' );
        } else {
            $nextPageLink = null;
        }

        $arResult['response']['pagination']['paginationLinks']['previousPage'] = $previousPageLink;
        $arResult['response']['pagination']['paginationLinks']['nextPage'] = $nextPageLink;

        $arResult['response']['metadata']['resultCount'] = $arResult['response']['pagination']['total'];

        $arResult['response']['pagination']['fromNumber'] = $arResult['response']['pagination']['perPage'] * ( $arResult['response']['pagination']['currentPage'] - 1 ) +1;

        if( $arResult['response']['pagination']['perPage'] * $arResult['response']['pagination']['currentPage'] > $arResult['response']['pagination']['total'] ){
            $arResult['response']['pagination']['toNumber'] = $arResult['response']['pagination']['total'];
        } else {
            $arResult['response']['pagination']['toNumber'] = $arResult['response']['pagination']['perPage'] * $arResult['response']['pagination']['currentPage'];
        }

        if( $arResult['params']['sort'] == 'relevant' ){
            $sort_field_1 = 'NAME';   $sort_order_1 = 'ASC';
            $sort_field_2 = 'NAME';   $sort_order_2 = 'ASC';
        } else {
            $sort_field_1 = project\search_sorts::$items[ $arResult['params']['sort'] ]['FIELD_1'];
            $sort_order_1 = project\search_sorts::$items[ $arResult['params']['sort'] ]['ORDER_1'];
            $sort_field_2 = project\search_sorts::$items[ $arResult['params']['sort'] ]['FIELD_2'];
            $sort_order_2 = project\search_sorts::$items[ $arResult['params']['sort'] ]['ORDER_2'];
        }

        $GLOBALS["searchProductsFilter"]['ID'] = $productsFilter;
        $GLOBALS["searchProductsFilter"]['INCLUDE_SUBSECTIONS'] = 'Y';


        $db_products_time_start = microtime(true);

        $offersFilter = [ 'PROPERTY_MAIN_TP' => 1 ];
        if(
            is_array($arResult['params']['color'])
            &&
            count($arResult['params']['color']) > 0
        ){
            $color_id = $arResult['params']['color'][0];
            $offersFilter = [ 'PROPERTY_COLOR' => $color_id ];
        }

        ob_start();
            $APPLICATION->IncludeComponent(
                "bitrix:catalog.section", "api_search_products",
                Array(
                    "filter_params" => $arResult['params'],
                    "offersFilter" => $offersFilter,
                    "IBLOCK_TYPE" => "catalog",
                    "IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
                    "SECTION_USER_FIELDS" => array(),
                    "ELEMENT_SORT_FIELD" => $sort_field_1,
                    "ELEMENT_SORT_ORDER" => $sort_order_1,
                    "ELEMENT_SORT_FIELD2" => $sort_field_2,
                    "ELEMENT_SORT_ORDER2" => $sort_order_2,
                    "FILTER_NAME" => "searchProductsFilter",
                    "HIDE_NOT_AVAILABLE" => "N",
                    "PAGE_ELEMENT_COUNT" => $productsCount,
                    "LINE_ELEMENT_COUNT" => "3",
                    "PROPERTY_CODE" => [ 'COLOR', 'PRINT_ID', 'TAGS', 'PHOTO_ID' ],
                    "OFFERS_CART_PROPERTIES" => array(),
                    "OFFERS_FIELD_CODE" => [ 'ID' ],
                    "OFFERS_PROPERTY_CODE" => [ 'SIZE', 'COLOR', 'MAIN_TP' ],
                    "OFFERS_SORT_FIELD" => 'ID',
                    "OFFERS_SORT_ORDER" => 'DESC',
                    "OFFERS_SORT_FIELD2" => 'ID',
                    "OFFERS_SORT_ORDER2" => 'DESC',
                    "OFFERS_LIMIT" => 1,
                    "TEMPLATE_THEME" => "",
                    "PRODUCT_SUBSCRIPTION" => "N",
                    "SHOW_DISCOUNT_PERCENT" => "N",
                    "SHOW_OLD_PRICE" => "N",
                    "MESS_BTN_BUY" => "Купить",
                    "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                    "MESS_BTN_SUBSCRIBE" => "Подписаться",
                    "MESS_BTN_DETAIL" => "Подробнее",
                    "MESS_NOT_AVAILABLE" => "Нет в наличии",
                    "SECTION_URL" => "",
                    "DETAIL_URL" => "",
                    "SECTION_ID_VARIABLE" => "SECTION_ID",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "300",
                    "CACHE_GROUPS" => "Y",
                    "SET_META_KEYWORDS" => "N",
                    "META_KEYWORDS" => "",
                    "SET_META_DESCRIPTION" => "N",
                    "META_DESCRIPTION" => "",
                    "BROWSER_TITLE" => "-",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "DISPLAY_COMPARE" => "N",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "CACHE_FILTER" => "Y",
                    "PRICE_CODE" => [ project\site::CATALOG_PRICE_CODE ],
                    "USE_PRICE_COUNT" => "N",
                    "SHOW_PRICE_COUNT" => "1",
                    "PRICE_VAT_INCLUDE" => "Y",
                    "CONVERT_CURRENCY" => "N",
                    "BASKET_URL" => "/personal/basket.php",
                    "ACTION_VARIABLE" => "action",
                    "PRODUCT_ID_VARIABLE" => "id",
                    "USE_PRODUCT_QUANTITY" => "N",
                    "ADD_PROPERTIES_TO_BASKET" => "Y",
                    "PRODUCT_PROPS_VARIABLE" => "prop",
                    "PARTIAL_PRODUCT_PROPERTIES" => "N",
                    "PRODUCT_PROPERTIES" => "",
                    "PAGER_TEMPLATE" => "",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "Товары",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "Y",
                    "ADD_PICT_PROP" => "-",
                    "LABEL_PROP" => "-",
                    'INCLUDE_SUBSECTIONS' => "Y",
                    'SHOW_ALL_WO_SECTION' => "Y"
                )
            );
            $products_json = ob_get_contents();
        ob_end_clean();

        $db_products_time_end = microtime(true);

        $productsRes = \AOptima\Tools\funcs::json_to_array($products_json);

        // Если применена сортировка по релевантности
        if(
            $arResult['params']['sort'] == 'relevant'
            &&
            count($productsRes['products']) > 0
        ){
            foreach ( $productsFilter as $product_id ){
                if( isset($productsRes['products'][$product_id]) ){
                    $arResult['response']['products'][$product_id] = $productsRes['products'][$product_id];
                }
            }
        } else {
            $arResult['response']['products'] = $productsRes['products'];
        }

        $arResult['response']['db_products_time'] = $db_products_time_end - $db_products_time_start;
        $arResult['response']['db_products_time'] = number_format((float)$arResult['response']['db_products_time'], 3, '.', '');

        return $arResult;
    }


    // Обработка подмассива metadata
    static function updateMetaData( $arResult ){

        global $APPLICATION;
        $pureURL = \AOptima\Tools\funcs::pureURL();

        $title = null;
        $description = null;
        $keywords = null;
        $h1 = null;
        $prefix = null;

        // Инфа по иллюстратору
        $illustratorPageID = project\site::isIllustratorPage();
        // Инфа по принту
        $printPageID = project\site::isPrintPage();
        // Инфа по коллекции
        $collectionPageCode = project\site::isCollectionPage();
        // Инфа по фабрике поиска
        $fabricPageCode = project\site::isSearchFabricPage();

        // Если это страница иллюстратора
        if( intval($illustratorPageID) > 0 ){
            $illustrator = \AOptima\Tools\user::info( $illustratorPageID );
            if( intval($illustrator['ID']) > 0 ){

                $title = 'Иллюстратор '.\AOptima\Tools\user::getFullName($illustrator['ID']);
                $description = 'Иллюстратор '.\AOptima\Tools\user::getFullName($illustrator['ID']);
                $h1 = 'Иллюстратор '.\AOptima\Tools\user::getFullName($illustrator['ID']);

                // Подсчёт количества принтов
                \Bitrix\Main\Loader::includeModule('iblock');
                $filter = [
                	"IBLOCK_ID" => project\site::PRINTS_IBLOCK_ID,
                	"ACTIVE" => "Y",
                	"PROPERTY_USER_ID" => $illustrator['ID']
                ];
                $fields = [ "ID", "NAME", "PROPERTY_USER_ID" ];
                $dbElements = \CIBlockElement::GetList(
                	[ "SORT" => "ASC" ], $filter, false, false, $fields
                );
                $prints_count = $dbElements->SelectedRowsCount();
                // Подсчёт количества подписок
                $filter = [
                    "IBLOCK_ID" => project\site::ISUBSCRIBES_IBLOCK_ID,
                    "ACTIVE" => "Y",
                    "PROPERTY_ILLUSTRATOR" => $illustrator['ID']
                ];
                $fields = [ "ID", "NAME", "PROPERTY_ILLUSTRATOR" ];
                $dbElements = \CIBlockElement::GetList(
                    [ "SORT" => "ASC" ], $filter, false, false, $fields
                );
                $isubscribe_count = $dbElements->SelectedRowsCount();
                /////
                $arResult['response']['metadata']['illustrator'] = [
                    'id' => $illustrator['ID'],
                    'name' => \AOptima\Tools\user::getFullName( $illustrator['ID'] ),
                    'picture_id' => $illustrator['PERSONAL_PHOTO'],
                    'date_register' => $illustrator['DATE_REGISTER'],
                    'prints_count' => $prints_count,
                    'isubscribe_count' => $isubscribe_count,
                ];
            }
        }

        // Если это страница принта
        if( intval($printPageID) > 0 ){
            $print = \AOptima\Tools\el::info( $printPageID );
            if( intval($print['ID']) > 0 ){

                $title = 'Принт "'.$print['NAME'].'"';
                $description = 'Принт "'.$print['NAME'].'"';
                $h1 = 'Принт "'.$print['NAME'].'"';

                $arIllustrator = null;
                $illustrator = \AOptima\Tools\user::info( $print['PROPERTY_USER_ID_VALUE'] );
                if( intval($illustrator['ID']) > 0 ){
                    // Подсчёт количества принтов
                    \Bitrix\Main\Loader::includeModule('iblock');
                    $filter = [
                        "IBLOCK_ID" => project\site::PRINTS_IBLOCK_ID,
                        "ACTIVE" => "Y",
                        "PROPERTY_USER_ID" => $illustrator['ID']
                    ];
                    $fields = [ "ID", "NAME", "PROPERTY_USER_ID" ];
                    $dbElements = \CIBlockElement::GetList(
                        [ "SORT" => "ASC" ], $filter, false, false, $fields
                    );
                    $prints_count = $dbElements->SelectedRowsCount();
                    // Подсчёт количества подписок
                    $filter = [
                        "IBLOCK_ID" => project\site::ISUBSCRIBES_IBLOCK_ID,
                        "ACTIVE" => "Y",
                        "PROPERTY_ILLUSTRATOR" => $illustrator['ID']
                    ];
                    $fields = [ "ID", "NAME", "PROPERTY_ILLUSTRATOR" ];
                    $dbElements = \CIBlockElement::GetList(
                        [ "SORT" => "ASC" ], $filter, false, false, $fields
                    );
                    $isubscribe_count = $dbElements->SelectedRowsCount();
                    /////
                    $arIllustrator = [
                        'id' => $illustrator['ID'],
                        'name' => \AOptima\Tools\user::getFullName( $illustrator['ID'] ),
                        'picture_id' => $illustrator['PERSONAL_PHOTO'],
                        'date_register' => $illustrator['DATE_REGISTER'],
                        'prints_count' => $prints_count,
                        'isubscribe_count' => $isubscribe_count,
                    ];
                }
                $arResult['response']['metadata']['print'] = [
                    'name' => $print['NAME'],
                    'picture_id' => $print['PROPERTY_PHOTO_ID_VALUE'],
                    'illustrator' => $arIllustrator
                ];
            }  
        }

        // Если это страница коллекции
        if( strlen($collectionPageCode) > 0 ){
            $collection = \AOptima\Tools\el::info_by_code($collectionPageCode, project\site::COLLECTIONS_IBLOCK_ID);
            if( intval($collection['ID']) > 0 ){

                $title = 'Коллекция "'.$collection['NAME'].'"';
                $description = 'Коллекция "'.$collection['NAME'].'"';
                $h1 = 'Коллекция "'.$collection['NAME'].'"';

                $arResult['response']['metadata']['collection'] = [
                    'name' => $collection['NAME'],
                    'picture_id' => $collection['DETAIL_PICTURE'],
                ];
            }
        }

        // Если это страница фабрики поиска
        if( strlen($fabricPageCode) > 0 ){
            $fabricPage = \AOptima\Tools\el::info_by_code($fabricPageCode, project\site::SEARCH_FABRIC_IBLOCK_ID);
            if( intval($fabricPage['ID']) > 0 ){
                if( strlen($fabricPage['PROPERTY_TITLE_VALUE']) > 0 ){
                    $title = $fabricPage['PROPERTY_TITLE_VALUE'];
                } else {
                    $title = $fabricPage['NAME'];
                }
                if( strlen($fabricPage['PROPERTY_DESCRIPTION_VALUE']) > 0 ){
                    $description = $fabricPage['PROPERTY_DESCRIPTION_VALUE'];
                } else {
                    $description = $fabricPage['NAME'];
                }
                if( strlen($fabricPage['PROPERTY_KEYWORDS_VALUE']) > 0 ){
                    $keywords = $fabricPage['PROPERTY_KEYWORDS_VALUE'];
                } else {
                    $keywords = $fabricPage['NAME'];
                }
                if( strlen($fabricPage['PROPERTY_H1_VALUE']) > 0 ){
                    $h1 = $fabricPage['PROPERTY_H1_VALUE'];
                } else {
                    $h1 = $fabricPage['NAME'];
                }
                $prefix = $fabricPage['PROPERTY_PREFIX_VALUE'];
            }
        }

        if( strlen($arResult['response']['query']) > 0 ){
            $title = 'Вы искали "'.$arResult['response']['query'].'"';
            $description = '';
            $h1 = 'Вы искали "'.$arResult['response']['query'].'"';
        }

        if( !isset( $title ) ){
            $title = 'Поиск по сайту';
        }
        if( !isset( $description ) ){
            $description = 'Поиск по сайту';
        }
        if( !isset( $h1 ) ){
            $h1 = 'Поиск по сайту';
        }

        $arResult['response']['metadata']['seoMetadata']['searchTitle'] = $title;
        $APPLICATION->SetPageProperty("title", $title);

        $arResult['response']['metadata']['seoMetadata']['pageDescription'] = $description;
        $APPLICATION->SetPageProperty("description", $title);

        if( isset( $keywords ) ){
            $arResult['response']['metadata']['seoMetadata']['pageKeywords'] = $keywords;
            $APPLICATION->SetPageProperty("keywords", $keywords);
        }

        if( isset( $keywords ) ){
            $arResult['response']['metadata']['seoMetadata']['prefix'] = $prefix;
        }

        return $arResult;
    }
    

    // Обработка массива перед выводом
    static function updateArResult( $arResult ){
        foreach ( $arResult['response']['filters']['filters'] as $key => $filterItem ){
            if( isset($filterItem['hidden']) && $filterItem['hidden'] ){
                unset($arResult['response']['filters']['filters'][$key]);
            }
        }
        return $arResult;
    }



}
