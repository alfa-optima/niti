<? namespace AOptima\Project;
use AOptima\Project as project;



class filter_cache_gender extends filter_cache {


    const CACHE_PATH = '/local/php_interface/filter_caches/genders/';


    public $isGender = true;


    public $searchParamCode = 'group';


    function __construct(){

        $this->cache_path = $_SERVER['DOCUMENT_ROOT'].static::CACHE_PATH;

    }



    // Создание кэшей
    public function create(){

        // Проверка наличия рабочей папки
        if( !file_exists( $this->cache_path ) ){
            mkdir( $this->cache_path, 0700 );
        }

        \Bitrix\Main\Loader::includeModule('iblock');
        \Bitrix\Main\Loader::includeModule('aoptima.tools');

        // Массив для новых файлов кэша
        $newFiles = [];

        // Текущий набор файлов кэша
        $currentFiles = $this->getCurrentFiles( $this->cache_path );

        // Пользовательское поле UF_GENDER_GROUP
        $gender_types = project\gender_type::list();

        // Получим полный набор категорий каталога
        $categories_cache = new project\filter_cache_categories();
        $allCatalogCategories = $categories_cache->allCatalogCategories();

        // Полы
        foreach ( $gender_types as $gender_type ){

            $product_ids = [];

            // Получим категории товаров данного пола
            $genderCategories = [];
            foreach ( $allCatalogCategories as $section ){
                if( $section['UF_GENDER_GROUP'] == $gender_type['ID'] ){
                    $genderCategories[] = $section['ID'];
                }
            }

            // Получим товары данных категорий
            if( count($genderCategories) > 0 ){
                $filter = [
                    "IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
                    "ACTIVE" => "Y",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "SECTION_ID" => $genderCategories
                ];
                $fields = [ "ID" ];
                $products = \CIBlockElement::GetList(
                    [ "SORT" => "ASC" ], $filter, false, false, $fields
                );
                while ($product = $products->GetNext()){
                    $product_ids[] = $product['ID'];
                }
            }
            $product_ids = array_unique($product_ids);

            // Сохраняем файл кэша
            $fileName = $gender_type['CODE'].".json";
            $filePath = $this->cache_path.$fileName;
            $file = fopen($filePath, "w");
            $res = fwrite( $file,  json_encode( $product_ids ) );
            fclose($file);

            $newFiles[] = $fileName;

            // Удаляем устаревшие файлы (те, которыйх нет в $newFiles)
            if( count($currentFiles) > 0 ){
                $filesToDelete = array_diff($currentFiles, $newFiles);
                foreach ( $filesToDelete as $fileName ){
                    unlink( $this->cache_path.$fileName );
                }
            }
        }
    }



    // Все значения
    public function getAllValues(){
        $list = [];
        $gender_types = project\gender_type::list();
        foreach ( $gender_types as $gender_type ){
            $list[] = $gender_type['CODE'];
        }
        return $list;
    }


    

}

