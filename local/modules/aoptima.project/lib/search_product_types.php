<? namespace AOptima\Project;
use AOptima\Project as project;



class search_product_types {



    static function getList(){
        $list = [];
        \Bitrix\Main\Loader::includeModule('iblock');
        $filter = [
        	"IBLOCK_ID" => project\site::PRODUCT_TYPES_IBLOCK_ID,
        	"ACTIVE" => "Y"
        ];
        $fields = [ "ID", "NAME", "CODE" ];
        $dbElements = \CIBlockElement::GetList(
        	[ "SORT" => "ASC" ], $filter, false, false, $fields
        );
        while ($element = $dbElements->GetNext()){
            $list[] = [
                'ID' => $element['ID'],
                'NAME' => $element['NAME'],
                'CODE' => $element['CODE'],
            ];
        }
        return $list;
    }



}