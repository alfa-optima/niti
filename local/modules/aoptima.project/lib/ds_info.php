<? namespace AOptima\Project;
use AOptima\Project as project;


class ds_info {
	
	
	
	static function getList(){
	    \Bitrix\Main\Loader::includeModule('sale');
		$list = array();
		$db_dtype = \CSaleDelivery::GetList(
			Array("SORT"=>"ASC", "NAME"=>"ASC"),
			Array(
				"ACTIVE"=>"Y",
				"LID" => SITE_ID,
			),
			false,
			false,
			array()
		);
		while ($ds = $db_dtype->GetNext()){
			$list[] = array(
				'id' => $ds['ID'],
				'name' => $ds['NAME'],
				'price' => $ds['PRICE'],
			);
		}
		return json_encode($list);
	}

	
	
}