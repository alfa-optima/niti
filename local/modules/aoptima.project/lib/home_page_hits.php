<? namespace AOptima\Project;
use AOptima\Project as project;



class home_page_hits {

    const MAX_LIST_CNT = 10;
    const LOG_HIBLOCK_ID = 6;
    const LOG_LIST_DAYS = 7;
    const LOG_DAYS_TO_DELETE = 30;



	static function getCategoriesWithProducts( $gender_codes = false ){

        $arSearchCategories = [];

        // Получим категории для поиска, отмеченные для использования в хитах
        $searchCategories = project\search_category::getSections1Level( true );

        if( count($searchCategories) > 0 ){

            $products = [];
            // Получим товары-хиты, отмеченные галочкой Хит
            $hitsByCheckBox = static::getProductsByHitCheckBox();
            if( count($hitsByCheckBox) > 0 ){
                $products = array_merge( $products, $hitsByCheckBox );
            }
            // Получим товары, из лога добавлений в корзину
            $hitsFromLog = static::getProductsFromLog();
            if( count($hitsFromLog) > 0 ){
                $products = array_merge( $products, $hitsFromLog );
            }
            $products = array_unique($products);

            // Если передан XML_ID пола
            $gender_type_ids = [];
            if( is_array($gender_codes) ){
                $gender_types = project\gender_type::list();
                foreach ( $gender_codes as $gender_code ){
                    foreach ( $gender_types as $gender_type ){
                        if( $gender_type['CODE'] == $gender_code ){
                            $gender_type_ids[] = $gender_type['ID'];
                        }
                    }
                }
            }

            foreach ( $searchCategories as $searchCategoryID => $searchCategory ){

                // Соберём категории каталога, настроенные в элементах данной категории для поиска
                $searchCatalogSections = [];
                // получим элементы данной категории для поиска
                $searchCategoryEls = project\search_category::getElList( false, $searchCategory['ID'] );
                // переберём элементы данной категории для поиска
                foreach ( $searchCategoryEls as $searchCategoryEl ){
                    if(
                        is_array($searchCategoryEl['PROPERTY_CAT_SECTIONS_VALUE'])
                        &&
                        count($searchCategoryEl['PROPERTY_CAT_SECTIONS_VALUE']) > 0
                    ){
                        $searchCatalogSections = array_merge($searchCatalogSections, $searchCategoryEl['PROPERTY_CAT_SECTIONS_VALUE']);
                    }
                }
                $searchCatalogSections = array_unique( $searchCatalogSections );

                // Если передан XML_ID пола
                if( is_array($gender_type_ids) ){
                    // Отфильтровываем неподходящие (по полу) категории каталога
                    foreach ( $searchCatalogSections as $scs_key => $searchCatalogSectionID ){
                        $searchCatalogSection = \AOptima\Tools\section::info($searchCatalogSectionID);
                        if( !in_array($searchCatalogSection['UF_GENDER_GROUP'], $gender_type_ids) ){
                            unset($searchCatalogSections[$scs_key]);
                        }
                    }
                }

                // Наполняем поисковые категории товарами
                if( count($searchCatalogSections) > 0 ){
                    foreach ( $products as $key => $product_id ){
                        $el_sect_id = \AOptima\Tools\el::sections($product_id)[0]['ID'];
                        $el_sect_chain = \AOptima\Tools\section::chain($el_sect_id);
                        foreach ( $el_sect_chain as $el_sect_chain_item ){
                            if(
                                in_array( $el_sect_chain_item['ID'], $searchCatalogSections )
                                &&
                                (
                                    count($searchCategories[$searchCategoryID]['PRODUCTS'])
                                    <=
                                    static::MAX_LIST_CNT
                                )
                            ){
                                $searchCategories[$searchCategoryID]['PRODUCTS'][] = $product_id;
                                $searchCategories[$searchCategoryID]['PRODUCTS'] = array_unique($searchCategories[$searchCategoryID]['PRODUCTS']);
                            }
                        }
                    }
                }

            }

            foreach ( $searchCategories as $searchCategoryID => $searchCategory ){
                if( isset($searchCategory['PRODUCTS']) ){
                    $arSearchCategories[$searchCategory['NAME']] = $searchCategory['PRODUCTS'];
                }
            }

        }
        
        return $arSearchCategories;
    }



    // Добавление товара в лог для хитов на главной
    static function addToLog( $el_id ){
        $el = \AOptima\Tools\el::info($el_id);
        if( intval($el['ID']) > 0 ){
            if( intval($el['PROPERTY_CML2_LINK_VALUE']) > 0 ){
                $product_id = $el['PROPERTY_CML2_LINK_VALUE'];
            } else {
                $product_id = $el['ID'];
            }
            $hiblock = new \AOptima\Tools\hiblock(static::LOG_HIBLOCK_ID);
            $arData = Array(
                'UF_PRODUCT_ID' => $product_id,
                'UF_DATE' => date('d.m.Y H:i:s')
            );
            $result = $hiblock->entity_data_class::add($arData);
            if ( $result->isSuccess() ) {} else {
                \AOptima\Tools\logger::addError('home_page_hits::addToLog - Ошибка запись добавленного в корзину товара в лог статистики');
            }
        }
    }


    // Получим записи нужного временного диапазона
    static function getProductsFromLog(){
	    $products = [];
	    // Кеширование
	    $obCache = new \CPHPCache();
	    $cache_time = 5*60;
	    $cache_id = 'home_page_hits_from_log';
	    $cache_path = '/home_page_hits_from_log/';
	    if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
	    	$vars = $obCache->GetVars();   extract($vars);
	    } elseif($obCache->StartDataCache()){
	        // Получим записи за N последних дней
            $hiblock = new \AOptima\Tools\hiblock(static::LOG_HIBLOCK_ID);
            $filterDate = date( "d.m.Y H:i:s", time()-static::LOG_LIST_DAYS*24*60*60 );
            $arFilter = [ ">=UF_DATE" => $filterDate ];
            $arSelect = [ '*' ];
            $arOrder = [ "ID" => "DESC" ];
            $rsData = $hiblock->entity_data_class::getList(array(
                "select" => $arSelect,
                "filter" => $arFilter,
                "order" => $arOrder
            ));
            $rsData = new \CDBResult($rsData, $sTableID);
            while($item = $rsData->Fetch()){
                // инфо по товару
                $el = \AOptima\Tools\el::info( $item['UF_PRODUCT_ID'] );
                // если такой товар действительно есть
                if( intval($el['ID']) > 0 ){
                    $products[$item['UF_PRODUCT_ID']]++;
                // иначе удалим запись
                } else {
                    $hiblock->entity_data_class::delete($item["ID"]);
                }
            }
            krsort($products);
            static::deleteOldProductsFromLog();
	        $obCache->EndDataCache(array('products' => $products));
	    }
        $products = array_keys($products);
        return $products;
    }


    // Удаление устаревших записей
    static function deleteOldProductsFromLog(){
        $hiblock = new \AOptima\Tools\hiblock(static::LOG_HIBLOCK_ID);
        $arFilter = array( "<UF_DATE" => date( "d.m.Y H:i:s", time()-static::LOG_DAYS_TO_DELETE*24*60*60 ));
        $arSelect = array('*');
        $arOrder = array("ID" => "DESC");
        $rsData = $hiblock->entity_data_class::getList(array(
            "select" => $arSelect,
            "filter" => $arFilter,
            "order" => $arOrder
        ));
        $rsData = new \CDBResult($rsData, $sTableID);
        while($item = $rsData->Fetch()){
            $hiblock->entity_data_class::delete($item["ID"]);
        }
    }



    static function getProductsByHitCheckBox(){
	    $list = [];
	    \Bitrix\Main\Loader::includeModule('iblock');
	    // Кеширование
	    $obCache = new \CPHPCache();
	    $cache_time = 10*60;
	    $cache_id = 'homePageCheckboxHits';
	    $cache_path = '/homePageCheckboxHits/';
	    if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
	    	$vars = $obCache->GetVars();   extract($vars);
	    } elseif($obCache->StartDataCache()){
            $filter = [
                "IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
                "ACTIVE" => "Y",
                "PROPERTY_HIT" => true
            ];
            $fields = Array( "ID", "PROPERTY_HIT" );
            $dbElements = \CIBlockElement::GetList( ["RAND"=>"ASC"], $filter, false, false, $fields );
            while ($element = $dbElements->GetNext()){
                $list[] = $element['ID'];
            }
	    $obCache->EndDataCache(array('list' => $list));
	    }
        return $list;
    }

	
	
}