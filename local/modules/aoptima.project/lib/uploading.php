<? namespace AOptima\Project;
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;


class uploading {
	
	
	const MODELS_PATH = '/upload/files/model';
	const MAX_TIME = 290;
	const IMAGES_DIR = '/upload/catalog_images/';
	
	
	
// РАЗДЕЛЫ


	static function getSectionByXMLID($xml_id){
	    \Bitrix\Main\Loader::includeModule('iblock');
		if( strlen($xml_id) > 0 ){
			$dbSections = \CIBlockSection::GetList(Array("SORT"=>"ASC"), Array("=XML_ID" => $xml_id, "IBLOCK_ID"=>project\site::CATALOG_IBLOCK_ID), false);
			if ($section = $dbSections->GetNext()){   return $section;   }
		}
		return false;
	}
	
	
	
	static function create_section($sect, $sort, $section_id){
	    \Bitrix\Main\Loader::includeModule('iblock');
		$sectByXMLID = static::getSectionByXMLID($sect['category_id']);
		if ( !$sectByXMLID ){
			$section = new \CIBlockSection;
			$fields = Array(
				"IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
				"XML_ID" => $sect['category_id'],
				"NAME" => $sect['cat_name'],
				"SORT" => $sort,
				"ACTIVE" => 'Y',
				"CODE" => \CUtil::Translit($sect['cat_name'], "ru")
			);
			if( strlen($section_id) > 0 ){    $fields['IBLOCK_SECTION_ID'] = $section_id;    }
			$sect_id = $section->Add($fields);
			return $sect_id?$sect_id:false;
		} else {
			$section = new \CIBlockSection;
			$fields = Array(
				"XML_ID" => $sect['category_id'],
				"NAME" => $sect['cat_name'],
				"CODE" => \CUtil::Translit($sect['cat_name'], "ru")
			);
			if( strlen($section_id) > 0 ){    $fields['IBLOCK_SECTION_ID'] = $section_id;    }
			$res = $section->Update($sectByXMLID['ID'], $fields);
			return $res?$sectByXMLID['ID']:false;
		}
	}
	
	
	
	
	
	// загрузка разделов
	function sectionsUpload(){

		$path = $_SERVER["DOCUMENT_ROOT"].'/upload/files/categorise.json';
		$content = file_get_contents($path);
		$json_array = json_to_array($content);
		$sections = array();
		
		// ПРОСТАВЛЯЕМ УРОВНИ
		// Найдем 1 уровень
		foreach ( $json_array as $key => $sect ){
			if ( intval($sect['parent_id']) == 0 ){
				// проставим глубину
				$json_array[$key]['depth_level'] = 1;
			}
		}
		// Переберём 1 уровень
		foreach ( $json_array as $key => $sect ){
			if ( $sect['depth_level'] == 1 ){
				// Найдем 2 уровень
				foreach ( $json_array as $key2 => $sect2 ){
					if (
						!isset($sect2['depth_level'])
						&&
						$sect2['parent_id'] == $sect['category_id']
					){
						// проставим глубину
						$json_array[$key2]['depth_level'] = 2;
					}
				}
			}
		}
		// Переберём 2 уровень
		foreach ( $json_array as $key => $sect ){
			if ( $sect['depth_level'] == 2 ){
				// Найдем 3 уровень
				foreach ( $json_array as $key2 => $sect2 ){
					if (
						!isset($sect2['depth_level'])
						&&
						$sect2['parent_id'] == $sect['category_id']
					){
						// проставим глубину
						$json_array[$key2]['depth_level'] = 3;
					}
				}
			}
		}
		
		// СОЗДАЁМ РАЗДЕЛЫ
		// 1 уровень
		$sort_1 = 0;
		foreach ( $json_array as $key_1 => $sect_1 ){
			if ( $sect_1['depth_level'] == 1 ){
				
				$sort_1 = $sort_1 + 100;
				$sect_id_1 = static::create_section($sect_1, $sort_1);
				
				if ( $sect_id_1 ){
					
					// 2 уровень
					$sort_2 = 0;
					foreach ( $json_array as $key_2 => $sect_2 ){
						if (
							$sect_2['depth_level'] == 2
							&&
							$sect_2['parent_id'] == $sect_1['category_id']
						){
							
							$sort_2 = $sort_2 + 100;
							$sect_id_2 = static::create_section($sect_2, $sort_2, $sect_id_1);
							
							if ( $sect_id_2 ){
							
								// 3 уровень
								$sort_3 = 0;
								foreach ( $json_array as $key_3 => $sect_3 ){
									if (
										$sect_3['depth_level'] == 3
										&&
										$sect_3['parent_id'] == $sect_2['category_id']
									){
										$sort_3 = $sort_3 + 100;
										$sect_id_3 = static::create_section($sect_3, $sort_3, $sect_id_2);
									}
								}
								
							}
							
						}
					}
					
				}

			}
		}
		
	}
	
	
	
	
	
	
	
	

	
	
	
	
	// Проверим наличие такого автора
	static function getAuthorByXML_ID($xml_id){
		if( strlen($xml_id) > 0 ){
			$exist = false;
			$filter = Array( "XML_ID" => $xml_id );
			$fields = Array( 'FIELDS' => array( 'ID', 'XML_ID' ) );
			$rsUsers = \CUser::GetList(($by="id"), ($order="desc"), $filter, $fields);
			while ($arUser = $rsUsers->GetNext()){    return $arUser['ID'];    }
		}
		return false;
	}
	
	// Загрузка авторов
	static function authorsUpload(){
		
		$path = $_SERVER["DOCUMENT_ROOT"].'/upload/files/picture_author.json';
		$content = file_get_contents($path);
		$json_array = json_to_array($content);
		
		foreach ( $json_array['author'] as $author ){
			
			// Если такого автора ещё нет
			if( !static::getAuthorByXML_ID($author['author_id']) ){
				
				$password = 'p'.randString(15, array( "abcdefghijklnmopqrstuvwxyz", "ABCDEFGHIJKLNMOPQRSTUVWX­YZ", "0123456789", "!@#\$%^&*()" ));
				
				$email = 'mail_'.md5(time().$password).'@mail.ru';
				
				// Регистрируем пользователя
				$user = new \CUser;
				$arUserFields = Array(
					"ACTIVE" => "Y",
					"XML_ID" => $author['author_id'],
					"NAME" => $author['author_name'],
					"LOGIN" => $email,
					"EMAIL" => $email,
					"PASSWORD" => $password,
					"CONFIRM_PASSWORD" => $password,
					"GROUP_ID" => Array(5),
					"UF_USER_TYPE" => 2,
					"UF_PAYER_TYPE" => 3,
				);
				
				$userID = $user->Add($arUserFields);
				if( !$userID ){
					echo $author['author_name'].' - ERROR'."\n";
				} else {
					echo $author['author_name'].' - OK'."\n";
				}
				
			} else {
				echo $author['author_name'].' - уже загружен'."\n";
			}
			
		}
		
	}
	
	
	
	
	
	
	
	// Проверим наличие такого принта
	static function getPrintByXML_ID($xml_id){
	    \Bitrix\Main\Loader::includeModule('iblock');
		if( strlen($xml_id) > 0 ){
			$dbElements = \CIBlockElement::GetList(
				Array("SORT"=>"ASC"),
				Array("IBLOCK_ID"=>project\site::PRINTS_IBLOCK_ID, "=XML_ID"=>$xml_id),
				false,
				Array("nTopCount"=>1),
				Array("ID", "XML_ID")
			);
			while ($element = $dbElements->GetNext()){   return $element['ID'];   }
		}
		return false;
	}
	
	// Загрузка принтов
	static function printsUpload(){
		\Bitrix\Main\Loader::includeModule('iblock');
		$path = $_SERVER["DOCUMENT_ROOT"].'/upload/files/picture_author.json';
		$content = file_get_contents($path);
		$json_array = json_to_array($content);
		
		foreach ( $json_array['picture'] as $picture ){
			
			$author_xml_id = $picture['author_id'];
			$user_id = static::getAuthorByXML_ID($author_xml_id);
			
			// Если такой пользователь есть
			if( $user_id ){
				
				$print_id = static::getPrintByXML_ID($picture['picture_id']);
				
				// если такого принта нет
				if( !$print_id ){
				
					// Ищем такого автора в системе
					$PROP = array();
					if( intval($user_id) > 0 ){    $PROP[43] = $user_id;    }
					
					$element = new \CIBlockElement;
					$fields = Array(
						"IBLOCK_ID"				=> project\site::PRINTS_IBLOCK_ID,
						"XML_ID"				=> $picture['picture_id'],
						"PROPERTY_VALUES"		=> $PROP,
						"NAME"					=> $picture['picture_name'],
						"ACTIVE"				=> "Y"
					);
					if( $el_id = $element->Add($fields) ){
						echo 'Принт "'.$picture['picture_name'].' ['.$picture['picture_id'].']" - add OK'."\n";
					} else {
						echo 'Принт "'.$picture['picture_name'].' ['.$picture['picture_id'].']" - add ERROR'."\n";
					}
					
				// если есть, обновим инфу
				} else {
					
					$el = new \CIBlockElement;
					$fields = Array( "NAME" => $picture['picture_name'] );
					$res = $el->Update($print_id, $fields);
					if($res){
						if( intval($user_id) > 0 ){
							$set_prop = array("USER_ID" => $user_id);
							\CIBlockElement::SetPropertyValuesEx($print_id, project\site::PRINTS_IBLOCK_ID, $set_prop);
						}
						echo 'Принт "'.$picture['picture_name'].' ['.$picture['picture_id'].']" - update OK'."\n";
					} else {
						echo 'Принт "'.$picture['picture_name'].' ['.$picture['picture_id'].']" - update ERROR'."\n";
					}
					
				}
				
			} else {
				
				echo 'Пользователя id="'.$author_xml_id.'" нет'."\n";
				
			}
			
		}
		
	}
	
	
	
	
	
	
	
	
	
	
// МОДЕЛИ
	
	
	// Проверка наличия товара по его XML_ID
	static function getModelByXMLID($xml_id){
	    \Bitrix\Main\Loader::includeModule('iblock');
		if( strlen($xml_id) > 0 ){
			$filter = Array(
				"IBLOCK_ID"=>project\site::CATALOG_IBLOCK_ID,
				"=XML_ID"=>$xml_id
			);
			$dbElements = \CIBlockElement::GetList(Array("SORT"=>"ASC"), $filter, false, Array("nTopCount"=>1), Array("ID", "XML_ID"));
			while ($element = $dbElements->GetNext()){   return $element;   }
		}
		return false;
	}
	
	
	// Создание товара
	static function create_model($model){
	    \Bitrix\Main\Loader::includeModule('iblock');
		if ( $parent_section = static::getSectionByXMLID($model['category_id']) ){
			$element = new \CIBlockElement;
			// Если такого товара ещё нет
			$elByXMLID = static::getModelByXMLID($model['model_id']);
			if ( !$elByXMLID ){
				// Создаём его
				$PROP = array();
				$PROP[9] = $model['title_meta'];
				$PROP[10] = $model['descr'];
				$PROP[11] = $model['keywords'];
				if( strlen($model['picture']) > 0 ){
					$print_id = static::getPrintByXML_ID($model['picture']);
					if( intval($print_id) > 0 ){    
						$PROP[15] = $print_id;
						$print = tools\el::info($print_id);
						if( intval($print['PROPERTY_USER_ID_VALUE']) > 0 ){
							$PROP[48] = $print['PROPERTY_USER_ID_VALUE'];
						}
					}
				}
				$code = strlen($model['code'])>0?$model['code']:\CUtil::Translit($model['name'], "ru");
				$fields = Array(
					"IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
					"PROPERTY_VALUES" => $PROP,
					"XML_ID" => $model['model_id'],
					"NAME" => $model['name'],
					"SORT" => '500',
					"ACTIVE" => 'Y',
					"DETAIL_TEXT" => $model['text'],
					"CODE" => $code.'_'.$model['model_id']
				);
				if( strlen($model['category_id']) > 0 ){
					$fields['IBLOCK_SECTION_ID'] = $parent_section['ID'];
				}
				$el_id = $element->Add($fields);
			// Если такой товар уже есть
			} else {
				// Внесём изменения в него
				$code = strlen($model['code'])>0?$model['code']:\CUtil::Translit($model['name'], "ru");
				$fields = Array(
					"XML_ID" => $model['model_id'],
					"NAME" => htmlspecialchars_decode($model['name']),
					"DETAIL_TEXT" => $model['text'],
					"CODE" => $code.'_'.$model['model_id']
				);
				if( intval($model['category_id']) > 0 ){
					$fields['IBLOCK_SECTION_ID'] = $parent_section['ID'];
				}
				
				$res = $element->Update($elByXMLID['ID'], $fields);
				if ( $res ){
					// В случае успеха - вносим изменения в свойства
					$set_prop = array(
						"TITLE" => $model['title_meta'],
						"DESCRIPTION" => $model['descr'],
						"KEYWORDS" => $model['keywords']
					);
					if( strlen($model['picture']) > 0 ){
						$print_id = static::getPrintByXML_ID($model['picture']);
					}
					if( intval($print_id) > 0 ){    
						$print = tools\el::info($print_id);
						if( intval($print['PROPERTY_USER_ID_VALUE']) > 0 ){
							$set_prop['USER_ID'] = intval($print['PROPERTY_USER_ID_VALUE']) > 0?$print['PROPERTY_USER_ID_VALUE']:false;
						}
					}
					$set_prop['PRINT_ID'] = intval($print_id) > 0?$print_id:false;
					\CIBlockElement::SetPropertyValuesEx($elByXMLID['ID'], project\site::CATALOG_IBLOCK_ID, $set_prop);
					$el_id = $elByXMLID['ID'];
				}
			}
			return intval($el_id)>0?$el_id:false;
		} else {
			tools\logger::addError('В модели id='.$model['model_id'].' нет привязки к категории');
		}
	}
	
	
	
	// Проверка наличия ТП по его XML_ID
	static function getSKUByXMLID($xml_id){
	    \Bitrix\Main\Loader::includeModule('iblock');
		if( strlen($xml_id) > 0 ){
			$filter = Array(
				"IBLOCK_ID"=>project\site::SKU_IBLOCK_ID,
				"=XML_ID"=>$xml_id
			);
			$dbElements = \CIBlockElement::GetList(Array("SORT"=>"ASC"), $filter, false, Array("nTopCount"=>1), Array("ID", "XML_ID"));
			while ($sku = $dbElements->GetNext()){   return $sku;   }
		}
		return false;
	}
	
	

	function fsize($path){
		$fp = fopen($path,"r");
		$inf = stream_get_meta_data($fp);
		fclose($fp);
		foreach($inf["wrapper_data"] as $v)
		if ( stristr($v,"content-length") ){
			$v = explode(":",$v);
			return trim($v[1]);
		}
	}
	
	
	
	// Выкачивание отдельного файла
	static function downloadFile($url) {
		if( strlen($url) > 0 ){
			// получим расширение
			$pure_url = pureURL($url);
			$arURI = explode('/', $pure_url);
			$ind_section = $arURI[count($arURI) - 2];
			$ind_section = ($ind_section != 'full_hd')?$ind_section:false;
			$file_name = $arURI[count($arURI) - 1];
			// путь для сохранения файла
			$server_file_path = static::IMAGES_DIR.'sku/';
			if( $ind_section ){   $server_file_path .= $ind_section.'/';   }
			$server_file_path .= $file_name;
			
			$local_file_size = filesize($_SERVER['DOCUMENT_ROOT'].$server_file_path);
			$remote_file_size = static::fsize($url);
			
			if( 
				!file_exists($_SERVER['DOCUMENT_ROOT'].$server_file_path)
				||
				$remote_file_size != $local_file_size
			){
				
				// Создаём папки, если их нет
				if ( !file_exists($_SERVER['DOCUMENT_ROOT'].static::IMAGES_DIR) ){
					mkdir($_SERVER['DOCUMENT_ROOT'].static::IMAGES_DIR, 0700);
				}
				if ( !file_exists($_SERVER['DOCUMENT_ROOT'].static::IMAGES_DIR.'sku/') ){
					mkdir($_SERVER['DOCUMENT_ROOT'].static::IMAGES_DIR.'sku/', 0700);
				}
				if(
					$ind_section
					&&
					!file_exists($_SERVER['DOCUMENT_ROOT'].static::IMAGES_DIR.'sku/'.$ind_section.'/')
				){
					mkdir($_SERVER['DOCUMENT_ROOT'].static::IMAGES_DIR.'sku/'.$ind_section.'/', 0700);
				}
				
				$ReadFile = fopen($url, "rb");
				if ( $ReadFile ){
					$WriteFile = fopen( $_SERVER['DOCUMENT_ROOT'].$server_file_path, "wb" );
					if ( $WriteFile ){
						while( !feof($ReadFile) ){
							fwrite($WriteFile, fread($ReadFile, 4096));
						}
						fclose($WriteFile);
					}
					fclose($ReadFile);
				}
			}
			return file_exists($_SERVER['DOCUMENT_ROOT'].$server_file_path)?$server_file_path:false;
		}
		return false;
	}
	
	// Создание ТП
	static function create_SKU($sku, $el_id, $model){
		\Bitrix\Main\Loader::includeModule('iblock');
		$element = new \CIBlockElement;
		// Если такого ТП ещё нет
		$elByXMLID = static::getSKUByXMLID($sku['product_id']);
		if ( !$elByXMLID ){
			
			// Создаём его
			$PROP = array();
			$PROP[5] = $el_id;
			$PROP[45] = array_keys($sku['param']);
			$PROP[46] = $sku['param'];
			// Загрузка фото
			if( strlen($sku['images']['full_hd']) > 0 ){
				$file_path = static::downloadFile($sku['images']['full_hd']);
				if( $file_path ){    $PROP[47] = $file_path;    }
			}
			$fields = Array(
				"IBLOCK_ID" => project\site::SKU_IBLOCK_ID,
				"PROPERTY_VALUES" => $PROP,
				"XML_ID" => $sku['product_id'],
				"NAME" => $model['name'],
				"SORT" => '500',
				"ACTIVE" => 'Y'
			);
			$sku_id = $element->Add($fields);
			if( intval($sku_id) > 0 ){
				// Создаём товар
				$fields = array( "ID" => $sku_id, "VAT_ID" => 1, "VAT_INCLUDED" => "Y" );
				$product = \CCatalogProduct::Add($fields);
			}
			
		// Если такое ТП уже есть
		} else {
			
			// Внесём изменения в него
			$fields = Array(
				"XML_ID" => $sku['product_id'],
				"NAME" => $model['name']
			);
			$res = $element->Update($elByXMLID['ID'], $fields);
			if ( $res ){
				$sku_id = $elByXMLID['ID'];
				// Обновляем товар
				//$fields = array( "VAT_ID" => 1, "VAT_INCLUDED" => "Y" );
				//$product = \CCatalogProduct::Update($sku_id, $fields);
				// В случае успеха - вносим изменения в свойства
				$set_prop = array(
					"CML2_LINK" => $el_id,
					"PARAMS" => array_keys($sku['param']),
					"PARAM_VALUES" => $sku['param']
				);
				if( strlen($sku['images']['full_hd']) > 0 ){
					$file_path = static::downloadFile($sku['images']['full_hd']);
					if( $file_path ){    $set_prop['PHTO'] = $file_path;    }
				}
				\CIBlockElement::SetPropertyValuesEx($sku_id, project\site::SKU_IBLOCK_ID, $set_prop);
			}
			
		}
		if ( intval( $sku_id ) > 0 && $sku['price'] ){
			static::UpdateSKUPrice ($sku_id, $sku['price']);
		}
		return intval($sku_id)>0?$sku_id:false;
	}
	
	
	// Установка/обновление цены у товара
	static function UpdateSKUPrice ($sku_id, $price){
		\Bitrix\Main\Loader::includeModule('catalog');
		$arFields = array(
			"ID" => $sku_id, 
			"VAT_ID" => 1,  
			"VAT_INCLUDED" => "Y"
		);
		\CCatalogProduct::Add($arFields);
		
		BXClearCache(true, "/tools\el::info/".$sku_id."/");
		$sku = tools\el::info($sku_id);
		if ( intval($sku['ID']) ){
			$price_id = $sku['CATALOG_PRICE_ID_1'];
			$fields = Array(
				"PRODUCT_ID" => $sku_id,
				"CATALOG_GROUP_ID" => 1,
				"PRICE" => $price,
				"CURRENCY" => "RUB"
			);
			if ( intval($price_id) > 0 ){
				// Обновляем ценовое предложение
				CPrice::Update($price_id, $fields);
			} else {
				// Создаём ценовое предложение
				CPrice::Add($fields);
			}
		}
	}
	
	
	// Загрузка моделей (товаров)
	static function modelsUpload(){
		
		if( static::notProcess() ){
			
			$path = $_SERVER["DOCUMENT_ROOT"].static::MODELS_PATH;
			$files = new DirectoryIterator($path);
			
			// Макс. время работы скрипта
			$start_time = microtime(true);
			
			foreach ($files as $file){
				
				$url = $file->getPathName();

				if (
					$file->isFile()
					&& 
					file_exists($url)
					&&
					is_readable($url)
				){
					
					static::writeToFile( '_process.php', $file->GetFileName() );
					
					$modelsToUpload = static::getModelsToUpload();
					if( !$modelsToUpload ){
						$content = file_get_contents( $url );
						$modelsToUpload = json_to_array($content);
						static::writeToFile('_models_to_upload.json', $content);
					}
					
					if ( is_array($modelsToUpload) && count($modelsToUpload) > 0 ){
						
						foreach ($modelsToUpload as $model){
							
							// Создаём товар
							$model_id = static::create_model($model);
							
							if ( intval($model_id) > 0 ){
								
								// В случае успеха - создаём ТП
								if(
									is_array($model['products'])
									&&
									count($model['products']) > 0
								){
									
									$uploadedSKU = static::getUploadedSKU();
									
									foreach ( $model['products'] as $sku ){
										
										if( !in_array($sku['product_id'], $uploadedSKU) ){
										
											$sku_id = static::create_SKU($sku, $model_id, $model);
											if ( intval($sku_id) > 0 ){
												$uploadedSKU[] = $sku['product_id'];
												static::writeToFile("_models_uploaded.json", json_encode($uploadedSKU));
											}
											
											if (  microtime(true) - $start_time > static::MAX_TIME ){
												static::removeFile('_process.php');
												die();
											}
											
										}
										
									}
								}
								
								unset($modelsToUpload[$model['model_id']]);
								static::writeToFile('_models_to_upload.json', json_encode($modelsToUpload));
								static::removeFile('_models_uploaded.json');

							}
							
						}
						
					}
					
					if( copy($file->getPath().'/'.$file->GetFileName(), $file->getPath().'/old/'.$file->GetFileName()) ){
						unlink($file->getPath().'/'.$file->GetFileName());
						static::removeFile('_models_to_upload.json');
					}
					
				}

			}
			
			static::removeFile('_process.php');
			
		}

	}
	
	
	static function notProcess(){
		$file_path = $_SERVER['DOCUMENT_ROOT']."/test/_process.php";
		if ( !file_exists($file_path) ){   return true;   }
		return false;
	}
	
	
	static function getModelsToUpload(){
		$modelsToUpload = false;
		$file_path = $_SERVER['DOCUMENT_ROOT']."/test/_models_to_upload.json";
		if( file_exists($file_path) ){
			$content = file_get_contents( $file_path );
			if( strlen($content) > 0 ){
				$modelsToUpload = json_to_array($content);
			}
		}
		return $modelsToUpload;
	}
	
	
	static function getUploadedSKU(){
		$uploadedSKU = array();
		$file_path = $_SERVER['DOCUMENT_ROOT']."/test/_models_uploaded.json";
		if( file_exists($file_path) ){
			$content = file_get_contents( $file_path );
			if( strlen($content) > 0 ){
				$uploadedSKU = json_to_array($content);
			}
		}
		return $uploadedSKU;
	}
	
	
	static function writeToFile($file_name, $content){
		$file_path = $_SERVER['DOCUMENT_ROOT']."/test/".$file_name;
		$file = fopen($file_path, "w"); 
		$res = fwrite($file,  $content);
		fclose($file);
	}
	
	static function removeFile($file_name){
		unlink($_SERVER['DOCUMENT_ROOT']."/test/".$file_name);
	}
	
	
	
	
	
	
	
	// getColorByName
	static function getColorByName($color_name){
	    \Bitrix\Main\Loader::includeModule('iblock');
		if( strlen($color_name) > 0 ){
			$dbElements = \CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>project\site::COLOR_IBLOCK_ID, "=NAME" => trim($color_name)), false, Array("nTopCount"=>1), Array("ID", "NAME"));
			while ($element = $dbElements->GetNext()){  return $element;  }
		}
		return false;
	}
	
	
	// Создание справочника цветов
	static function makeColorsCatalog($all_colors){
	    \Bitrix\Main\Loader::includeModule('iblock');
		$color_ids = array();
		if ( is_array($all_colors) && count($all_colors) > 0 ){
			foreach ($all_colors as $key => $color_name){
				$color = static::getColorByName($color_name);
				if( !$color ){
					$element = new \CIBlockElement;
					$fields = Array(
						"IBLOCK_ID"				=> project\site::COLOR_IBLOCK_ID,
						"NAME"					=> trim($color_name),
						"CODE"					=> \CUtil::Translit($color_name, "ru"),
						"ACTIVE"				=> "Y"
					);
					$color_id = $element->Add($fields);
					$color_ids[$color_name] = $color_id;
				} else {
					$color_ids[$color_name] = $color['ID'];
				}
			}
		}
		return $color_ids;
	}
	
	static function getParamValues(){
		$obCache = new \CPHPCache();
		$cache_time = 30*24*60*60;
		$cache_id = 'paramsInfo';
		$cache_path = '/paramsInfo/';
		if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
			$vars = $obCache->GetVars();   extract($vars);
		} elseif($obCache->StartDataCache()){
			$path = $_SERVER["DOCUMENT_ROOT"].'/upload/files/params.json';
			$content = file_get_contents($path);
			$json_array = json_to_array($content);
			// Собираем значения
			$values = array();
			foreach ( $json_array as $param ){
				$param_id = $param['id'];
				if ( is_array($param['value']) && count($param['value']) > 0 ){
					foreach ($param['value'] as $key => $value){
						if ( strtolower(trim($param['name'])) == 'размер'){
							$values['sizes'][$param_id][$value['value']] = $value['name'];
						} else if ( strtolower(trim($param['name'])) == 'цвет'){
							$all_colors[] = $value['name'];
							$values['colors'][$param_id][$value['value']] = $value['name'];
						}
					}
				}
			}
			// Создаём справочник цветов
			$all_colors = array_unique($all_colors);
			$color_ids = static::makeColorsCatalog($all_colors);
			// Проставляем ID цветов
			foreach ( $values['colors'] as $param_id => $colors ){
				foreach ($colors as $value_id => $color_name){
					if( $color_ids[$color_name] ){
						$values['colors'][$param_id][$value_id] = $color_ids[$color_name];
					}
				}
			}
		$obCache->EndDataCache(array('values' => $values));
		}
		return $values;
	}
	
	
	// Загрузка характеристик
	static function paramsUpload(){
	    \Bitrix\Main\Loader::includeModule('iblock');
		
		$values = static::getParamValues();
		
		// Макс. время работы скрипта
		$start_time = microtime(true);
		
		$changed_cnt = 0;

		// Перебираем ТП
		$filter = Array(
			"IBLOCK_ID" => project\site::SKU_IBLOCK_ID,
			"!PROPERTY_PARAMS" => false,
			"!PROPERTY_PARAM_VALUES" => false
		);
		if ( $_SESSION['LAST_SKU'] ){
			$filter['>ID'] = $_SESSION['LAST_SKU'];
		}
		$fields = Array("ID", "NAME", "PROPERTY_PARAMS", "PROPERTY_PARAM_VALUES");
		$skus = \CIBlockElement::GetList(Array("ID"=>"ASC"), $filter, false, Array("nTopCount" => 8000), $fields);
		while ($sku = $skus->GetNext()){
			
			$_SESSION['SKU_CNT']++;
			$_SESSION['LAST_SKU'] = $sku['ID'];
			
			$params = $sku['PROPERTY_PARAMS_VALUE'];
			$param_values = $sku['PROPERTY_PARAM_VALUES_VALUE'];
			
			// Получаем значения свойств
			$set_prop = array();
			foreach($params as $key => $param_id){
				if( $values['sizes'][$param_id] ){
					$set_prop["SIZE"] = $values['sizes'][$param_id][$param_values[$key]];
				} else if( $values['colors'][$param_id] ){
					$set_prop["COLOR"] = $values['colors'][$param_id][$param_values[$key]];
				}
			}
			if( count($set_prop) > 0 ){
				
				// Вносим изменения в элемент
				\CIBlockElement::SetPropertyValuesEx($sku['ID'], project\site::SKU_IBLOCK_ID, $set_prop);
				
				$changed_cnt++;
				
				if (  microtime(true) - $start_time > static::MAX_TIME ){
					
					pre($_SESSION['SKU_CNT'].' ...');
					echo '<script type="text/javascript">
					$(document).ready(function(){
						window.location.href = "/test/";
					});</script>';
					die();
					
				}
				
			}
			
		}
		
		if ( $changed_cnt > 0 ){
			
			pre($_SESSION['SKU_CNT'].' ...');
			echo '<script type="text/javascript">
			$(document).ready(function(){
				window.location.href = "/test/";
			});</script>';
			die();
			
		} else {
			echo date('d.m.Y H:i:s');
		}
		
	}
	
	
	
	
	// Привязка основных цветов к товарам
	static function setDefaultColors(){
	    \Bitrix\Main\Loader::includeModule('iblock');
		
		require_once ($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/csv_data.php");
		
		$file = $_SERVER['DOCUMENT_ROOT'].'/upload/files/default_colors.csv';
		if (file_exists($file)){
			
			$switch = [
				'Белый' => 'white',
				'Серый-меланж' => 'gray',
				'Светло-желтый' => 'svetlo-zheltyj',
				'Серый с черными рукавами' => 'seryj_s_chernymi_rukavami',
				'Мятный' => 'mjatnyj',
				'Розовый' => 'rozovyj',
				'Голубой' => 'goluboj',
				'Черный' => 'chernyj',
				'Лаванда' => 'lavandovyj',
				'Персик' => 'persikovyj',
				'Белый с мятными рукавами' => 'belyjmjatnyj',
				'Серый/Мятный' => 'seryjmjatnyj',
				'Зеленый' => 'green',
				'Бордовый' => 'red',
				'Темно-серый' => 'temno-seryj',
				'Индиго' => 'indigo',
				'Темно-розовый' => 'temno_rozovyj',
				'Желтый меланж' => 'zheltyj_melanzh',
				'Синий меланж' => 'sinij_melanzh',
				'Коричневый меланж' => 'korichnevyj_melanzh',
				'Красный меланж' => 'krasnyj_melanzh',
				'Голубой меланж' => 'goluboj_melanzh',
				'Темно-серый меланж' => 'temno_seryj_melanzh',
				'Серый/Голубой' => 'seryjgoluboj'
			];
			
			
			$csvFile = new CCSVData('R', false);
			$csvFile->LoadFile($file);
			$csvFile->SetDelimiter(';');
			while ($row = $csvFile->Fetch()){
				
				$xml_id = $row[0];
				$product = static::getModelByXMLID($xml_id);
				
				if( intval($product['ID']) > 0 ){
					
					$color_code = $row[1];
					
					$color_name = array_search($color_code, $switch);
					
					if( $color_name ){
						
						$color = static::getColorByName($color_name);
						
						if( intval($color['ID']) > 0 ){
							
							$set_prop = array("COLOR_DEFAULT" => $color['ID']);
							\CIBlockElement::SetPropertyValuesEx($product['ID'], project\site::CATALOG_IBLOCK_ID, $set_prop);
							
						} else {
							
							echo 'Цвет "'.$color_name.'" не найден '."\n";
						}
						
					} else {
						
						echo 'Имя цвета "'.$color_code.'" не определено '."\n";
					}

				} else {
					
					echo 'Товара ID "'.$xml_id.'" на сайте нет'."\n";
				}
				
			}
			
		}
	
	}
	
	
	
	
	
	
}

