<? namespace AOptima\Project;
use AOptima\Project as project;



class filter_cache_product_types extends filter_cache {


    const CACHE_PATH = '/local/php_interface/filter_caches/product_types/';

    public $isProductTypes = true;

    public $searchParamCode = 'product_type';



    function __construct(){

        $this->cache_path = $_SERVER['DOCUMENT_ROOT'].static::CACHE_PATH;

    }



    // Создание кэшей
    public function create(){

        // Проверка наличия рабочей папки
        if( !file_exists( $this->cache_path ) ){
            mkdir( $this->cache_path, 0700 );
        }

        \Bitrix\Main\Loader::includeModule('iblock');

        // Массив для новых файлов кэша
        $newFiles = [];

        // Текущий набор файлов кэша
        $currentFiles = $this->getCurrentFiles( $this->cache_path );

        // Получим полный набор категорий каталога
        $categories_cache = new project\filter_cache_categories();
        $allCatalogCategories = $categories_cache->allCatalogCategories();

        // Получим полный набор типов товаров
        $product_types = project\search_product_types::getList();

        // Переберём категории
        foreach ( $product_types as $product_type ){

            $product_ids = [];

            // Получим категории товаров данного типа
            $productTypeCategories = [];
            foreach ( $allCatalogCategories as $section ){
                if( $section['UF_PRODUCT_TYPE'] == $product_type['ID'] ){
                    $productTypeCategories[] = $section['ID'];
                }
            }

            // Получим товары данных категорий
            if( count($productTypeCategories) > 0 ){
                $filter = [
                    "IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
                    "ACTIVE" => "Y",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "SECTION_ID" => $productTypeCategories
                ];
                $fields = [ "ID" ];
                $products = \CIBlockElement::GetList(
                    [ "SORT" => "ASC" ], $filter, false, false, $fields
                );
                while ($product = $products->GetNext()){
                    $product_ids[] = $product['ID'];
                }
            }
            $product_ids = array_unique($product_ids);

            // Сохраняем файл кэша
            $fileName = $product_type['CODE'].".json";
            $filePath = $this->cache_path.$fileName;
            $file = fopen($filePath, "w");
            $res = fwrite( $file,  json_encode( $product_ids ) );
            fclose($file);

            $newFiles[] = $fileName;
        }

        // Удаляем устаревшие файлы (те, которыйх нет в $newFiles)
        if( count($currentFiles) > 0 ){
            $filesToDelete = array_diff($currentFiles, $newFiles);
            foreach ( $filesToDelete as $fileName ){
                unlink( $this->cache_path.$fileName );
            }
        }
    }







}

