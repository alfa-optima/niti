<? namespace AOptima\Project;

use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');

use AOptima\Tools as tools;

class product
{

    static function getObject($product_id = false)
    {

        \Bitrix\Main\Loader::includeModule('iblock');

        $DATA_OBJ = [];

        if (intval($product_id) > 0) {

            // Инфо о товаре
            $el = tools\el::info($product_id);

            if (intval($el['ID']) > 0) {

                // Категория товара
                $el_section = tools\section::info(tools\el::sections($el['ID'])[0]['ID']);

                $fabricPage = tools\el::info($el_section['UF_FABRIC_PAGE']);

                // Добавляем в просмотренные
                add_to_viewed_session($el['ID']);

                $picture = false;
                $price = false;
                $sku_id = false;

                // Инфо по ТП
                $offersInfo = getTovOffers($el['ID']);

                // Инфо по доп.фоткам для категории $el_section['ID']
                $morePhotosInfo = project\dop_photo::getList($el_section['ID']);

                // ID ТП по умолчанию
                $defaultOfferID = false;
                if (count($offersInfo['all_sku']) > 0) {
                    foreach ($offersInfo['all_sku'] as $key => $sku) {
                        if ($sku['PROPERTY_MAIN_TP_VALUE']) {
                            $defaultOfferID = $sku['ID'];
                        }
                    }
                }
                if (!$defaultOfferID) {
                    $defaultOfferID = array_keys($offersInfo['all_sku'])[0];
                }

                if (count($offersInfo['color_list']) > 0) {

                    $keys = array_keys($offersInfo['color_list']);
                    $default_color_key = $keys[0];

                    $default_color_id = $offersInfo['color_list'][$default_color_key]['color']['ID'];

                    $price = $offersInfo['color_min_prices'][$offersInfo['color_list'][$default_color_key]['color']['ID']];

                    $defaultPriceIsEq = isEqualPrices($offersInfo['color_prices'], $default_color_id);

                } else {

                    if (count($offersInfo['sizes']) > 0) {
                        $prices = array();
                        foreach ($offersInfo['sizes'] as $size => $sizeInfo) {
                            if (!$price) {
                                $price = $offersInfo['sizes'][$size]['PRICE'];
                            }
                            $prices[] = $sizeInfo['PRICE'];
                        }
                        $prices = $offersInfo['prices'];
                        $min_price = min($prices);
                        $max_price = max($prices);
                        $defaultPriceIsEq = $max_price == $min_price ? 1 : 0;
                    } else {
                        if (count($offersInfo['all_sku']) > 0) {
                            $cnt = 0;
                            foreach ($offersInfo['all_sku'] as $offer) {
                                $cnt++;
                                $prices[] = $offer['DISC_PRICE'];
                                if ($cnt == 1) {
                                    $sku_id = $offer['ID'];
                                }
                            }
                            $price = min($prices);
                            $max_price = max($prices);
                            $defaultPriceIsEq = $max_price == $price ? 1 : 0;
                        }
                    }

                }

                global $APPLICATION;
                $color_id = $APPLICATION->get_cookie('color_id');

                $DATA_OBJ['id'] = $el['ID'];
                $DATA_OBJ['productURL'] = $el['DETAIL_PAGE_URL'];
                $DATA_OBJ['nameModel'] = $el['~NAME'];
                $DATA_OBJ['nameType'] = $el_section['UF_NOMIN_TITLE'];
                $DATA_OBJ['offers'] = [];
                $DATA_OBJ['offersInfo'] = $offersInfo;
                $DATA_OBJ['defaultOfferID'] = $defaultOfferID;
                $DATA_OBJ['isFavorited'] = project\favorites::isFavorite($el['ID']);
                $DATA_OBJ['tags'] = [
                    'category' => [],
                    'all' => [],
                ];
                $DATA_OBJ['categoryProductDescription'] = $el_section['UF_PRODUCT_DESCR'];
                $DATA_OBJ['comments'] = [];

                if (!empty($el_section['UF_SIZE_TABLE'])) {
                    $DATA_OBJ['sizeTable'] = rIMGG($el_section['UF_SIZE_TABLE'], 4, 800, 800);
                }

                if (
                    is_array($offersInfo['all_sku'])
                    &&
                    count($offersInfo['all_sku']) > 0
                ) {} else {
                    $offersInfo['all_sku'][$el['ID']] = $el;
                    $DATA_OBJ['defaultOfferID'] = $el['ID'];
                }

                if (
                    is_array($offersInfo['all_sku'])
                    &&
                    count($offersInfo['all_sku']) > 0
                ) {
                    foreach ($offersInfo['all_sku'] as $offer_id => $offer) {
                        $DATA_OBJ['offers'][$offer_id] = [
                            'id' => $offer_id,
                            'name' => "",
                            'price' => $offer['CATALOG_PRICE_1'],
                            'pictures' => [
                                'default_picture' => null,
                                'more_pictures' => [],
                                'textures' => [],
                            ]
                        ];
                        if (intval($offer['PROPERTY_PHOTO_ID_VALUE']) > 0) {
                            $DATA_OBJ['offers'][$offer_id]['pictures']['default_picture'] = $offer['PROPERTY_PHOTO_ID_VALUE'];
                        }
                        if (is_array($offer['PROPERTY_PHOTOS_VALUE'])) {
                            foreach ($offer['PROPERTY_PHOTOS_VALUE'] as $photo_id) {
                                $DATA_OBJ['offers'][$offer_id]['pictures']['more_pictures'][] = $photo_id;
                            }
                        }
                        if (
                            intval($offer['PROPERTY_COLOR_VALUE']) > 0
                            &&
                            !$el['PROPERTY_DONT_ADD_DOP_PHOTOS_VALUE']
                        ) {
                            $dopPhotosInfo = $morePhotosInfo[$offer['PROPERTY_COLOR_VALUE']];
                            if (is_array($dopPhotosInfo)) {
                                if (is_array($dopPhotosInfo['photos'])) {
                                    $DATA_OBJ['offers'][$offer_id]['pictures']['more_pictures'] = array_merge($DATA_OBJ['offers'][$offer_id]['pictures']['more_pictures'], $dopPhotosInfo['photos']);
                                }
                                if (is_array($dopPhotosInfo['textures'])) {
                                    $DATA_OBJ['offers'][$offer_id]['pictures']['textures'] = array_merge($DATA_OBJ['offers'][$offer_id]['pictures']['textures'], $dopPhotosInfo['textures']);
                                }
                            }
                        }
                        if (!empty($offer['PROPERTY_COLOR_VALUE'])) {
                            $DATA_OBJ['offers'][$offer_id]['color'] = intval($offer['PROPERTY_COLOR_VALUE']);
                        }
                        if (!empty($offer['PROPERTY_SIZE_VALUE'])) {
                            $DATA_OBJ['offers'][$offer_id]['size'] = $offer['PROPERTY_SIZE_VALUE'];
                        }
                    }

                }

                $author = tools\user::info($el['PROPERTY_USER_ID_VALUE']);
                if (!empty($author)) {
                    $DATA_OBJ['author'] = [
                        'name' => $author['NAME'],
                        'link' => 'http://new.niti-niti.ru/illustrators/' . $author['ID'] . '/'
                    ];
                }

                $print = tools\el::info($el['PROPERTY_PRINT_ID_VALUE']);
                if (intval($print['ID']) > 0) {
                    $DATA_OBJ['print'] = [
                        'id' => $print['ID'],
                        'name' => $print['NAME'],
                        'picture_id' => $print['PROPERTY_PHOTO_ID_VALUE'],
                        'zayavka_id' => $print['PROPERTY_ZAYAVKA_ID_VALUE'],
                        'illustrator_id' => $print['PROPERTY_USER_ID_VALUE'],
                        'print_page' => '/print/' . $print['ID'] . '/',
                        'productsCNT' => project\catalog_print::productsCNT($print['ID']),
                    ];
                }

                if ($picture) {
                    $DATA_OBJ['picture'] = $picture;
                    $DATA_OBJ['images'][] = [
                        'thumbnail' => 'http://new.niti-niti.ru' . rIMG($picture, 5, 428, 490),
                        'full' => 'http://new.niti-niti.ru' . rIMG($picture, 1, 800, 700),
                        'main' => true
                    ];
                    $DATA_OBJ['images'][] = [
                        'thumbnail' => 'http://new.niti-niti.ru' . rIMG($picture, 1, 284, 230),
                        'full' => 'https://niti-niti.ru/images/jsnew/test2_full.jpg'
                    ];
                    $DATA_OBJ['images'][] = [
                        'thumbnail' => 'http://new.niti-niti.ru' . rIMG($picture, 1, 284, 230),
                        'full' => 'https://niti-niti.ru/images/jsnew/test4_full.jpg'
                    ];
                    $DATA_OBJ['images'][] = [
                        'thumbnail' => 'http://new.niti-niti.ru' . rIMG($picture, 1, 284, 230),
                        'full' => 'https://niti-niti.ru/images/jsnew/test5_full.jpg'
                    ];
                }

                if (strlen($el['PROPERTY_CML2_ARTICLE_VALUE']) > 0) {
                    $DATA_OBJ['article'] = $el['PROPERTY_CML2_ARTICLE_VALUE'];
                }

                if ($price) {
                    $DATA_OBJ['prices'] = [
                        'actual' => $price,
                        //'old' => 1390 //TODO тут нужна старая цена
                    ];
                }

//                $DATA_OBJ['discount'] = [
//                    'percent' => 10//TODO Процент скидки
//                ];

                $DATA_OBJ['params'] = [];

                if (
                    is_array($offersInfo['color_list'])
                    &&
                    count($offersInfo['color_list']) > 0
                ) {
                    // Вывод всех цветов
                    $k = 0;
                    foreach ($offersInfo['color_list'] as $key => $arColor) {
                        $k++;

                        $isEqPrices = isEqualPrices($offersInfo['color_prices'], $arColor['color']['ID']);
                        $colorItem = [
                            "id" => intval($arColor['color']['ID']),
                            "title" => $arColor['color']['NAME'],
                            "code" => $arColor['color']['CODE'],
                            "hex" => $arColor['color']['PROPERTY_COLOR_CODE_VALUE']
                        ];

                        if (intval($arColor['color']['PREVIEW_PICTURE']) > 0) {
                            $colorItem['image'] = \CFile::GetPath($arColor['color']['PREVIEW_PICTURE']);
                        } else {
                            // TODO Так не должно быть, но проверяем
                            if (empty($arColor['color']['PROPERTY_COLOR_CODE_VALUE'])) {
                                $arColor['color']['PROPERTY_COLOR_CODE_VALUE'] = '#000000';
                            }
                            $colorItem['hex'] = $arColor['color']['PROPERTY_COLOR_CODE_VALUE'];
                        }
                        $DATA_OBJ['params']['colors'][$colorItem['id']] = $colorItem;
                    }

                    // Вывод всех размеров
                    if (
                        is_array($offersInfo['color_sizes'])
                        &&
                        count($offersInfo['color_sizes']) > 0
                    ) {
                        // Выводим блоки с размерами
                        $k = 0;
                        foreach ($offersInfo['color_sizes'] as $color_id => $sizes) {
                            if ($color_id == $default_color_id) {
                                foreach ($sizes as $size_id) {

                                    $arSize = tools\el::info($size_id);

                                    $k++;
                                    $DATA_OBJ['params']['sizes'][intval($size_id)] = [
                                        "id" => intval($size_id),
                                        "title" => $arSize['NAME'],
                                        "sort" => $arSize['SORT']
                                    ];

                                }
                            }
                        }
                    }

                } else {

                    if (
                        is_array($offersInfo['sizes'])
                        &&
                        count($offersInfo['sizes']) > 0
                    ) {
                        $k = 0;
                        foreach ($offersInfo['sizes'] as $size_id => $sizeInfo) {

                            $arSize = tools\el::info($size_id);

                            $k++;
                            $DATA_OBJ['params']['sizes'][intval($size_id)] = [
                                "id" => $size_id,
                                "title" => $arSize['NAME'],
                                "sort" => $arSize['SORT']
                            ];
                        }
                    }

                }

                if(
                    is_array($el['PROPERTY_TAGS_VALUE'])
                    &&
                    count($el['PROPERTY_TAGS_VALUE']) > 0
                ){
                    $filter_cache_ib_prop = new project\filter_cache_ib_prop();
                    foreach ( $el['PROPERTY_TAGS_VALUE'] as $tag_id ){
                        $tag = tools\el::info($tag_id);
                        if( intval($fabricPage) > 0 ){
                            $search_fabric = new project\search_fabric();
                            $fabricSearchParams = $search_fabric->getSearchParams( $fabricPage['ID'] );
                            $link = '/search/?tags='.$tag['CODE'];
                            foreach ( $fabricSearchParams as $paramName => $value ){
                                if( $paramName != $filter_cache_ib_prop->propParams['TAGS']['searchParamCode'] ){
                                    if( is_array($value) ){
                                        $value = implode(',', $value);
                                    }
                                    $link .= '&'.$paramName.'='.$value;
                                }
                            }
                            $title = ''.$tag['NAME'].' + '.$fabricPage['NAME'].'';
                        } else {
                            $link = '/search/?tags='.$tag['CODE'];
                            $title = ''.$tag['NAME'];
                        }
                        if( intval($tag['ID']) > 0 ){
                            $DATA_OBJ['tags']['category'][] = [
                                'id' => $tag['ID'],
                                'title' => $title,
                                'link' => $link,
                            ];
                            $DATA_OBJ['tags']['all'][] = [
                                'id' => $tag['ID'],
                                'title' => ''.$tag['NAME'].'',
                                'link' =>'/search/?tags='.$tag['CODE']
                            ];
                        }
                    }
                }


                $DATA_OBJ['meta'] = [

                    'title' => strlen($el['PROPERTY_TITLE_VALUE']) > 0 ? $el['PROPERTY_TITLE_VALUE'] : $el['NAME'],

                    'description' => strlen($el['PROPERTY_DESCRIPTION_VALUE']) > 0 ? $el['PROPERTY_DESCRIPTION_VALUE'] : $el['NAME'],

                    'keywords' => strlen($el['PROPERTY_KEYWORDS_VALUE']) > 0 ? $el['PROPERTY_KEYWORDS_VALUE'] : ''

                ];

                // Комментарии
                $comment = new project\comment($el['ID']);
                $DATA_OBJ['comments'] = $comment->GetList(strip_tags($_GET['max_comment_id']));

            }

        }

        return $DATA_OBJ;
    }

    static function getDefaultOffer($product_id)
    {
        \Bitrix\Main\Loader::includeModule('iblock');
        $filter = array(
            "IBLOCK_ID" => project\site::SKU_IBLOCK_ID,
            "ACTIVE" => "Y",
            "PROPERTY_CML2_LINK" => $product_id,
            "PROPERTY_MAIN_TP" => 1
        );
        $fields = array("ID", "NAME", "DETAIL_PICTURE", "PROPERTY_CML2_LINK", "PROPERTY_MAIN_TP", "PROPERTY_PHOTO_ID");
        $dbElements = \CIBlockElement::GetList(
            array("SORT" => "ASC"), $filter, false, array("nTopCount" => 1), $fields
        );
        if ($element = $dbElements->GetNext()) {
            return $element;
        }
        return false;
    }





    static function getElsSections( $filter ){
        \Bitrix\Main\Loader::includeModule('iblock');
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        $els_sections = [];
        $fields = Array( "ID", "PROPERTY_PRINT_ID" );
        $dbElements = \CIBlockElement::GetList(
            array("SORT"=>"ASC"), $filter, false, false, $fields
        );
        while ($element = $dbElements->GetNext()){
            $el_sect = \AOptima\Tools\el::sections($element['ID'])[0];
            $el_sect = \AOptima\Tools\section::info($el_sect['ID']);
            $els_sections[$el_sect['ID']] = $el_sect;
        }
        return $els_sections;
    }





}
