<? namespace AOptima\Project;
use AOptima\Project as project;



class filter_cache_ib_prop_tp extends filter_cache_ib_prop {


    const CACHE_PATH = '/local/php_interface/filter_caches/props_tp/';


    public $iblock_id = project\site::SKU_IBLOCK_ID;

    public $productIdField = 'PROPERTY_CML2_LINK';

    public $propParams = [
        'COLOR'=> [
            'CODE' => 'COLOR',
            'typeInFilter' => 'checkbox',
            'experiencesValue' => 'color',
            'searchParamCode' => 'color',
        ],
        'SIZE' => [
            'CODE' => 'SIZE',
            'typeInFilter' => 'checkbox',
            'searchParamCode' => 'size',
            'hidden' => true,
        ]
    ];



    

}

