<? namespace AOptima\Project;

use Elasticsearch\ClientBuilder;

class elastic
{

    static function connection()
    {
        return ClientBuilder::create()
            ->setHosts([['host' => 'search-niti-niti-gga2ud7dgodcg3brb2tytxqwgy.eu-central-1.es.amazonaws.com', 'port' => '', 'scheme' => 'https']])
            ->build();
    }


    static function search($string, $config)
    {

        $client = self::connection();

        $params = [
            'index' => $config['index'],
            'body' => [
                'query' => []
            ]
        ];

        if (count($config['fields']) > 1) {

            // $params['body']['query']['multi_match']['query'] = $string;
            $params['body']['query']['match_phrase_prefix'] = [];
            //$params['body']['_source'] = [];

            //foreach ($config['fields'] as $field) {
            $params['body']['query']['match_phrase_prefix'][$config['fields'][0]] = [
                "query" => $string,
                "slop" => 3,
                // "max_expansions" => 10
            ];
            //$params['body']['_source'][] = $config['fields'][0];

            //}

            /*


    "query": {
        "match_phrase_prefix" : {
            "summary": {
                "query": "search en",
                "slop": 3,
                "max_expansions": 10
            }
        }
    },
    "_source": [ "title", "summary", "publish_date" ]

             */


            //$params['body']['query']['multi_match']['operator'] = 'or';
            //foreach ($config['fields'] as $field) {
            //    $params['body']['query']['multi_match']['fields'][] = $field;
            //}

            /*

             */

        } else {
            $params['body']['query']['match'][$config['fields'][0]] = [
                "query" => $string,
                "operator" => "and"
            ];
        }

        echo '<pre style="font-size: 17px;background-color: #363636;color: #fff;padding: 20px;line-height: 26px;">';
        print_r($params);
        echo '</pre>';
        //exit;


        $response = $client->search($params);

        //echo '<pre style="font-size: 17px;background-color: #363636;color: #fff;padding: 20px;line-height: 26px;">';
        //print_r($params);
        //echo '</pre>';
        // exit;

        // echo '<pre style="font-size: 17px;background-color: #363636;color: #fff;padding: 20px;line-height: 26px;">';
        // print_r($response);
        // echo '</pre>';

        $result = [];

        switch ($config['return']) {
            case '_id' :

                foreach ($response['hits']['hits'] as $item) {
                    $result[] = $item['_id'];
                }

                break;

            default:
                $result = $response['hits'];

        }


        return $result;

    }

    static function setItem($id, $data, $index)
    {

        $client = self::connection();

        $params = [
            'index' => $index,
            'id' => $id,
            'body' => $data
        ];

        $response = $client->index($params);

        echo '<pre style="font-size: 17px;background-color: #363636;color: #fff;padding: 20px;line-height: 26px;">';
        print_r($response);
        echo '</pre>';

    }

    static function geleteIndex($index)
    {

        $client = self::connection();

        $deleteParams = [
            'index' => $index
        ];
        $response = $client->indices()->delete($deleteParams);

        echo '<pre style="font-size: 17px;background-color: #363636;color: #fff;padding: 20px;line-height: 26px;">';
        print_r($response);
        echo '</pre>';

    }

    static function setSettings()
    {

        $client = self::connection();

        $params = [
            'index' => 'model_index',
            'body' => [
                'settings' => [
                    'analysis' => [
                        'tokenizer' => [
                            "autocomplete" => [
                                "type" => "edge_ngram",
                                "min_gram" => 2,
                                "max_gram" => 10,
                                "token_chars" => [
                                    "letter"
                                ]
                            ]
                        ],
                        'analyzer' => [
                            "autocomplete" => [
                                "type" => "custom",
                                "tokenizer" => "autocomplete",
                                "filter" => [
                                    "lowercase"
                                ]
                            ],
                            "autocomplete_search" => [
                                "tokenizer" => "lowercase"
                            ]
                        ]
                    ]
                ],
                'mappings' => [
                    'properties' => [
                        "name" => [
                            "type" => "text",
                            "analyzer" => "autocomplete",
                            "search_analyzer" => "autocomplete_search"
                        ]
                    ],
                ]
            ]
        ];

        $response = $client->indices()->create($params);

        echo '<pre style="font-size: 17px;background-color: #363636;color: #fff;padding: 20px;line-height: 26px;">';
        print_r($response);
        echo '</pre>';


    }

    static function refreshFullIndex($index)
    {
        $client = self::connection();
        //$client

    }

}
