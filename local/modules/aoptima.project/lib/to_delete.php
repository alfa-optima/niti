<? namespace AOptima\Project;
use AOptima\Project as project;
use AOptima\Tools as tools;



abstract class to_delete {



    function __construct(){
        if( intval($this->hiblock_id) > 0 ){
            \Bitrix\Main\Loader::includeModule('highloadblock');
            $this->hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById( $this->hiblock_id )->fetch();
            $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity( $this->hlblock );
            $this->entity = $entity;
            $this->entity_data_class = $entity->getDataClass();
            $this->entity_table_name = $this->hlblock['TABLE_NAME'];
            $this->sTableID = 'tbl_'.$this->entity_table_name;
        }
    }



    // Добавление ID в очередь на удаление
    public function add( $id ){
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        \Bitrix\Main\Loader::includeModule('iblock');
        $list = $this->list( $id );
        if( count($list) > 0 ){
            // Деактивируем элемент инфоблока //////
            $element = new \CIBlockElement;
            $sku_id = $element->Update( $id, [ "ACTIVE" => "N" ] );
            ////////////////////////////////////////
            return $list[0]['ID'];
        } else {
            $result = $this->entity_data_class::add([ 'UF_ID' => $id ]);
            if ($result->isSuccess()) {
                // Деактивируем элемент инфоблока ////////
                $element = new \CIBlockElement;
                $sku_id = $element->Update( $id, [ "ACTIVE" => "N" ] );
                //////////////////////////////////////////
                return $result->getId();
            } else {
                tools\logger::addError('Ошибка добавления в очередь на удаление: ' . $result->getErrors());
            }
        }
        return false;
    }



    // Перечень записей на удаление
    public function list( $id = false, $limit = false ){
        $list = [];
        $arFilter = [];
        if( intval( $id ) > 0 ){     $arFilter['UF_ID'] = $id;     }
        $arSelect = [ 'ID', 'UF_ID' ];
        $arOrder = [ 'ID' => 'ASC' ];
        $params = [
            "select" => $arSelect,
            "filter" => $arFilter,
            "order" => $arOrder
        ];
        if( intval($limit) > 0 ){     $params['limit'] = $limit;     }
        $rsData = $this->entity_data_class::getList($params);
        $rsData = new \CDBResult( $rsData, $this->sTableID );
        while($arRes = $rsData->Fetch()){
            $list[] = $arRes;
        }
        return $list;
    }



    // Удаление записи
    public function delete( $note_id ){
        $this->entity_data_class::delete( $note_id );
    }



}