<? namespace AOptima\Project;
use AOptima\Project as project;


class news {
    
	const allItemsCacheID = 'all_news';


	
	// Все новости
	static function all_items(){
	    \Bitrix\Main\Loader::includeModule('iblock');
		$all_items = array();
		$obCache = new \CPHPCache();
		$cache_time = 30*24*60*60;
		$cache_id = static::allItemsCacheID;
		$cache_path = '/'.static::allItemsCacheID.'/';
		if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
			$vars = $obCache->GetVars();   extract($vars);
		} elseif($obCache->StartDataCache()){
			$dbElements = \CIBlockElement::GetList(
				Array("SORT"=>"ASC"),
				Array("IBLOCK_ID"=>project\site::NEWS_IBLOCK_ID, "ACTIVE"=>"Y"),
				false, false, Array("ID", "PROPERTY_SORT_DATE"));
			while ($element = $dbElements->GetNext()){
				$all_items[] = clean_array($element);
			}
		$obCache->EndDataCache(array('all_items' => $all_items));
		}
		return $all_items;
	}
	
	
	
	// Все годы новостей
	static function years(){
		$years = array();
		$all_news = static::all_items();
		if( count($all_news) > 0 ){
			foreach ($all_news as $news_item){
				$year = ConvertDateTime($news_item['PROPERTY_SORT_DATE_VALUE'], "YYYY", "ru");
				$years[] = $year;
			}
		}
		$years = array_unique($years);
		sort($years);
		$years = array_reverse($years);
		return $years;
	}
	
	
	
}