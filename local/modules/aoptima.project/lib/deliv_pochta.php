<? namespace AOptima\Project;
use AOptima\Project as project;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;


class deliv_pochta {
	
	const BASE_URL = 'https://otpravka-api.pochta.ru';
	const TOKEN = 'YdEGCaoh3PBtZeLynEltBd4H2cI0apKh';
	const KEY = 'YS5tb3JhckBuaXRpLW5pdGkucnU6cG9jaHRhcG9jaHRhMjM=';
	const ZIP_FROM = 127220;
	const INCLUDE_VAT = 'Y';
	const TIMEOUT = 3;
	
	
	
	
	static function calcDeliveryPrice( $zip_to, $basketParams ){
		

		$weight = $basketParams['weight']; // грамм
		$height  = $basketParams['height']; // высота (см)
		$width = $basketParams['width']; // ширина (см)
		$length = $basketParams['length']; // длина (см)
		$orderSum = $basketParams['orderSum']; // cтоимость товаров без учета стоимости доставки,
		
		$send_body = array(
			"courier" => true,
			"declared-value" => $orderSum,
			"mass" => $weight,
			"fragile" => false,
			"index-from" => static::ZIP_FROM,
			"index-to" => $zip_to,
			"mail-category" => "ORDINARY",
			"mail-type" => "POSTAL_PARCEL",
			"with-simple-notice" => false,
			"rcp-pays-shipping" => true
		);
		$send_body = json_encode($send_body);
		
		$client = new Client(array( 'timeout' => static::TIMEOUT ));
		
		try {
		
			$response = $client->request(
				'POST',
				static::BASE_URL.'/1.0/tariff',  
				array(
					'headers' => array(
						'Authorization' => 'AccessToken '.static::TOKEN,
						'X-User-Authorization' => 'Basic '.static::KEY,
						'Content-Type' => 'application/json;charset=UTF-8'
					),
					'body' => $send_body
				)
			);
			
			if( $response->getStatusCode() == 200 ){
			
				$body = $response->getBody();
				$res = json_to_array($body->getContents());
				
				if( $res['total-rate'] ){
					if( static::INCLUDE_VAT == 'Y' ){
						$delivery_price = ($res['total-rate'] + $res['total-vat'])/ 100 ;
					} else {
						$delivery_price = $res['total-rate'] / 100;
					}
					return $delivery_price;
				}
				
			} else {
				tools\logger::addError('Ошибка запроса Почта России calcDeliveryPrice - ответ сервера '.$response->getStatusCode());
			}
			
		} catch(RequestException $ex){
			tools\logger::addError('Ошибка запроса Почта России calcDeliveryPrice - '.$ex->getMessage());
		}

		return false;
	}
	
	

	
	
	
	
	
}