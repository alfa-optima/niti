<? namespace AOptima\Project;
use AOptima\Project as project;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;



class dop_photo {


    const IBLOCK_ID = 34;




    static function getList ( $category_id /*, $color_id*/ ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $items = [];
        $filter = Array(
        	"IBLOCK_ID" => static::IBLOCK_ID,
        	"ACTIVE" => "Y",
            "PROPERTY_CATEGORY_ID" => $category_id,
            //"PROPERTY_COLOR_ID" => $color_id,
        );
        $fields = [
            "ID", "NAME", "PROPERTY_CATEGORY_ID",
            "PROPERTY_COLOR_ID", "PROPERTY_PHOTOS", "PROPERTY_TYPE"
        ];
        $dbElements = \CIBlockElement::GetList(
        	["SORT"=>"ASC"], $filter, false, false, $fields
        );
        while ($element = $dbElements->GetNext()){
            if( is_array( $element['PROPERTY_PHOTOS_VALUE'] ) ){
                foreach ( $element['PROPERTY_PHOTOS_VALUE'] as $photo_id ){
                    foreach ( $element['PROPERTY_COLOR_ID_VALUE'] as $color_id ){
                        // Готовые фото
                        if( $element['PROPERTY_TYPE_ENUM_ID'] == 15 ){
                            $items[$color_id]['photos'][] = $photo_id;
                        // Фактура материала
                        } else if( $element['PROPERTY_TYPE_ENUM_ID'] == 16 ){
                            $items[$color_id]['textures'][] = $photo_id;
                        }
                    }
                }
            }
        }
        return $items;
    }





}