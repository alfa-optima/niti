<?

namespace AOptima\Project;

use Intervention\Image\ImageManagerStatic as Image;

class image_generation
{

    const shotKey = [

        "a" => "width_artboard",
        "b" => "height_artboard",

        "c" => "key_image",

        "d" => "width_image",
        "e" => "height_image",

        "f" => "width_p_image",
        "g" => "height_p_image",

        "h" => "x_image",
        "i" => "y_image",

        "k" => "hex_artboard",
        "l" => "type_position",

        "m" => "file_exception"

    ];

    protected $widthArtboard = NULL;
    protected $heightArtboard = NULL;

    protected $keyImage = '';

    protected $widthImage = NULL;
    protected $heightImage = NULL;

    protected $widthPercentImage = NULL;
    protected $heightPercentImage = NULL;

    protected $xImage = NULL;
    protected $yImage = NULL;

    protected $hexArtboard;
    protected $typePosition;
    protected $mimeType = 'jpg'; //default JPG

    protected $listTypePosition = [
        "top-left", "top", "top-right", "left", "right", "bottom-left", "bottom", "bottom-right", "center"
    ];

    protected $listFileException = [
        "jpg", "png", "webp"
    ];

    function __construct($config)
    {

        $config = $this->decodeConfig($config);

        //echo '<pre style="font-size: 17px;background-color: #363636;color: #fff;padding: 20px;line-height: 26px;">';
        //print_r($config);
        //echo '</pre>';
        //exit;

        try {
            /*
             * Холст
             */
            if (!empty($config['width_artboard']) && !empty($config['height_artboard'])) {
                $this->widthArtboard = $config['width_artboard'];
                $this->heightArtboard = $config['height_artboard'];
            } else {
                throw new \Exception("Missing required parameter: width_artboard or height_artboard");
            }

            $this->hexArtboard = (!empty($config['hex_artboard']) ? $config['hex_artboard'] : "#ffffff");
            //$this->hexArtboard = (!empty($config['hex_artboard']) ? $config['hex_artboard'] : "#00f937");

            if (!empty($config['type_position']) && in_array($config['type_position'], $this->listTypePosition)) {
                $this->typePosition = $config['type_position'];
            } else {
                $this->typePosition = 'center';
                //throw new \Exception("Missing required parameter: type_position");
            }


            /*
             * Изображение
             */
            if (!empty($config['key_image'])) {
                //$this->keyImage = $_SERVER['DOCUMENT_ROOT'] . '/ajax/image-creator/' . $config['key_image'];
                //$this->keyImage = $_SERVER['DOCUMENT_ROOT'] . '/ajax/image-creator/' . $config['key_image'];
                $this->keyImage = $_SERVER['DOCUMENT_ROOT'] . \CFile::GetPath($config['key_image']);


            } else {
                throw new \Exception("Missing required parameter: key_image");
            }

            $this->widthImage = (!empty($config['width_image']) ? $config['width_image'] : $this->widthArtboard * 0.8);

            if (!empty($config['height_image'])) {
                $this->heightImage = $config['height_image'];
                $this->widthImage = NULL;
            }

            if (!empty($config['width_p_image'])) {
                $this->widthPercentImage = intval($config['width_p_image']);
                $this->calculationWidthImageByWidthPercent();
            }

            if (!empty($config['height_p_image'])) {
                $this->heightPercentImage = intval($config['height_p_image']);
                $this->calculationHeightImageByHeightPercent();
            }

            $this->xImage = (!empty($config['x_image'] && is_null($this->xImage)) ? $config['x_image'] : 0);
            $this->yImage = (!empty($config['y_image'] && is_null($this->xImage)) ? $config['y_image'] : 0);

            if (!empty($config['type_position'])) {
                $this->typePosition = $config['type_position'];
            }

            if (!empty($config['file_exception']) && in_array($config['file_exception'], $this->listFileException)) {
                $this->mimeType = $config['file_exception'];
            }

        } catch (\Exception $e) {

            header('HTTP/1.1 404 Not Found');
            header('Content-Type: image/png');
            header('Content-ErrorMsg: ' . $e->getMessage());

            echo base64_decode('iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAAApJREFUCNdjYAAAAAIAAeIhvDMAAAAASUVORK5CYII=');
            die();

        }


        //$this->showConfig();
        //exit();


    }

    /**
     * @param $config_string
     * @return array
     */
    private function decodeConfig($config_string)
    {

        $config_array = [];

        /*
        http://nb.devs/ic/a:1000,b:1000,c:bayka-tolstovka.png,d:300,e:300,k:#cccccc,l:center.jpga
        */

        $beans = explode(",", $config_string);

        //echo '<pre>';
        //print_r($config_string);
        //print_r($beans);

        foreach ($beans as $bean) {

            $param = explode(":", $bean);

            if (!empty(self::shotKey[$param[0]])) {
                $config_array[self::shotKey[$param[0]]] = $param[1];
            }

        }

        //print_r($config_array);
        //exit();


        return $config_array;

    }

    public function showConfig()
    {
        echo '<pre style="font-size: 13px;background-color: #363636;color: #fff;padding: 20px;line-height: 16px;">';

        $data = get_object_vars($this);
        unset($data['listTypePosition']);
        unset($data['listFileException']);
        print_r($data);
        echo '</pre>';
        exit;
    }

    private function calculationWidthImageByWidthPercent()
    {

        if (filter_var(
                $this->widthPercentImage,
                FILTER_VALIDATE_INT,
                ['options' => ['min_range' => 1, 'max_range' => 200]]
            ) == true) {

            $this->widthImage = $this->widthArtboard * ($this->widthPercentImage * 0.01);
        }


    }

    private function calculationHeightImageByHeightPercent()
    {

        if (filter_var(
                $this->heightPercentImage,
                FILTER_VALIDATE_INT,
                ['options' => ['min_range' => 1, 'max_range' => 200]]
            ) == true) {

            $this->heightImage = $this->heightArtboard * ($this->heightPercentImage * 0.01);
            $this->widthImage = NULL;
        }

    }

    public function make()
    {

        $img = Image::cache(function ($image) {

            $image->canvas($this->widthArtboard, $this->heightArtboard, $this->hexArtboard);

            $photo = Image::make($this->keyImage)->resize($this->widthImage, $this->heightImage, function ($constraint) {
                $constraint->aspectRatio();
            });

            $image->insert($photo, $this->typePosition, $this->xImage, $this->yImage);

            //sleep(3);
            return $image->encode($this->mimeType);

        }, 0);
        //print_r($img);
        //exit;

        header('Content-Type: image/' . $this->mimeType);
        //header('Content-ErrorMsg: ' . $e->getMessage());

        echo $img;

    }
}
