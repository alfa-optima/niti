<? namespace AOptima\Project;
use AOptima\Project as project;

trait files_trait {
	
	
	// filesSession
	protected function filesSession(){
		if ( !$_SESSION['filesSession'] ){
			$_SESSION['filesSession'] = md5(time().$_SERVER['HTTP_USER_AGENT'].$_SERVER['REMOTE_ADDR']);
		}
		return $_SESSION['filesSession'];
	}
	
	
	// moveFile
	protected function moveFile($files){
		if ( $files ){
			if ( !file_exists($_SERVER['DOCUMENT_ROOT'].static::TEMP_UPLOAD_PATH) ){
				mkdir($_SERVER['DOCUMENT_ROOT'].static::TEMP_UPLOAD_PATH, 0700);
			}
			$this->FILES = $files;
			$this->input_name = array_keys($this->FILES)[0];
			// сразу получим инфо по фото
			$this->file_name = basename($this->FILES[$this->input_name]['name']);
			$this->new_file_name = $this->filesSession() . '.' . explode('.', $this->file_name)[1];
			$this->file_path = ($this->doc_root) . (static::TEMP_UPLOAD_PATH) . $this->new_file_name;
			// Перемещение файла во временную папку
			return move_uploaded_file($this->FILES[$this->input_name]['tmp_name'], $this->file_path);
		}
		return false;	
	}

	
	// Получение типов файла
	public function fileTypes(){
		return $this->fileTypes;
	}
	
	
	// Получение типа файла
	static function getFileType($image_path){
		$finfo = finfo_open(FILEINFO_MIME_TYPE);
		$file_type = finfo_file($finfo, $image_path);
		finfo_close($finfo);
		return $file_type;
	}

	
	// Проверка файла
	public function checkFile(){
		if ( $this->file_moved ){
			$size = $this->FILES[$this->input_name]['size'];
			// Объём
			if ( $size <= static::MAX_FILE_SIZE ){
				// Проверим является ли файл изображением (при необходимости)
				if( $f_info = getimagesize( $this->file_path ) ){
					$this->width = $f_info[0];
					$this->height = $f_info[1];
					if (
						$this->width >= static::MIN_WIDTH
						&&
						$this->height >= static::MIN_HEIGHT
					){
						// Расширение файла
						$file_type = explode('/',  static::getFileType($this->file_path) )[1];
						if ( in_array($file_type, $this->fileTypes) ){
							return 'ok';
						} else {
							return 'Тип файла не соответствует<br>требованиям ('.implode(', ', $this->fileTypes).')';
						}
					} else {
						return 'Мин. размеры фото:<br>ширина '.static::MIN_WIDTH.'px; высота - '.static::MIN_HEIGHT.'px';
					}
				} else {
					unlink($this->file_path);
					return 'Загруженный файл не является изображением';
				}
			} else {
				return 'Превышен размер загружаемого файла ('.round((static::MAX_FILE_SIZE/1024/1024), 0).' МБ)';
			}
		} else {
			return 'Файл \"'.$this->file_name.'\" не загружен';
		}
	}
	
	
}




