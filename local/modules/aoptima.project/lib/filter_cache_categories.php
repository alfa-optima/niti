<? namespace AOptima\Project;
use AOptima\Project as project;



class filter_cache_categories extends filter_cache {


    const CACHE_PATH = '/local/php_interface/filter_caches/categories/';



    function __construct(){

        $this->cache_path = $_SERVER['DOCUMENT_ROOT'].static::CACHE_PATH;

    }



    // Создание кэшей
    public function create(){

        // Проверка наличия рабочей папки
        if( !file_exists( $this->cache_path ) ){
            mkdir( $this->cache_path, 0700 );
        }

        \Bitrix\Main\Loader::includeModule('iblock');

        // Массив для новых файлов кэша
        $newFiles = [];

        // Текущий набор файлов кэша
        $currentFiles = $this->getCurrentFiles( $this->cache_path );

        // Получим полный набор категорий каталога
        $allCatalogCategories = $this->allCatalogCategories();

        // Переберём категории
        foreach ( $allCatalogCategories as $section ){
            $product_ids = [];
            $filter = [
                "IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
                "ACTIVE" => "Y",
                "INCLUDE_SUBSECTIONS" => "Y",
                "SECTION_ID" => $section['ID']
            ];
            $fields = [ "ID" ];
            $products = \CIBlockElement::GetList(
                ["SORT" => "ASC"], $filter, false, false, $fields
            );
            while ($product = $products->GetNext()){
                $product_ids[] = $product['ID'];
            }
            $product_ids = array_unique($product_ids);

            // Сохраняем файл кэша
            $fileName = $section['ID'].".json";
            $filePath = $this->cache_path.$fileName;
            $file = fopen($filePath, "w");
            $res = fwrite( $file,  json_encode( $product_ids ) );
            fclose($file);

            $newFiles[] = $fileName;
        }

        // Удаляем устаревшие файлы (те, которыйх нет в $newFiles)
        if( count($currentFiles) > 0 ){
            $filesToDelete = array_diff($currentFiles, $newFiles);
            foreach ( $filesToDelete as $fileName ){
                unlink( $this->cache_path.$fileName );
            }
        }
    }



    // Все категории каталога
    public function allCatalogCategories(){
        \Bitrix\Main\Loader::includeModule('iblock');
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 5*60;
        $cache_id = 'allCatalogCategories';
        $cache_path = '/allCatalogCategories/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
        	$vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $list = [];
            $filter = [ "IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID ];
            $dbSections = \CIBlockSection::GetList(
                [ "SORT" => "ASC" ], $filter, false, [ "UF_GENDER_GROUP", "UF_PRODUCT_TYPE" ]
            );
            while ($section = $dbSections->GetNext()){
                $list[] = $section;
            }
        $obCache->EndDataCache([ 'list' => $list ]);
        }
        return $list;
    }





}

