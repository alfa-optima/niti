<? namespace AOptima\Project;
use AOptima\Project as project;



class catalog_search {
	

	
	// Проверка наличия коллекции
	static function checkCollection($collection_id, $search_ids){
	    \Bitrix\Main\Loader::includeModule('iblock');
		if(
			intval($collection_id) > 0
			&&
			$search_ids && is_array($search_ids) && count($search_ids) > 0
		){
			$filter = Array(
				"IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
				"ACTIVE" => "Y",
				"ID" => $search_ids,
				"PROPERTY_COLLECTION" => $collection_id,
			);
			$fields = Array( "ID", "PROPERTY_COLLECTION" );
			$dbElements = \CIBlockElement::GetList(
				array("SORT"=>"ASC"), $filter, false, array("nTopCount"=>1), $fields
			);
			while ($element = $dbElements->GetNext()){   return true;   }
		}
		return false;
	}
	
	
	
	// Проверка наличия цвета
	static function checkColor($color_id, $search_ids){
	    \Bitrix\Main\Loader::includeModule('iblock');
		if(
			intval($color_id) > 0
			&&
			$search_ids && is_array($search_ids) && count($search_ids) > 0
		){
			$filter = Array(
				"IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
				"ACTIVE" => "Y",
				"ID" => $search_ids,
				"PROPERTY_COLORS_AUTO" => $color_id,
			);
			$fields = Array( "ID", "PROPERTY_COLORS_AUTO" );
			$dbElements = \CIBlockElement::GetList(
				array("SORT"=>"ASC"), $filter, false, array("nTopCount"=>1), $fields
			);
			while ($element = $dbElements->GetNext()){   return true;   }
		}
		return false;
	}
	
	
	
}