<? namespace AOptima\Project;
use AOptima\Project as project;



class filter_cache_ib_prop extends filter_cache {


    const CACHE_PATH = '/local/php_interface/filter_caches/props/';


    public $isIbProp = true;

    public $iblock_id = project\site::CATALOG_IBLOCK_ID;

    public $productIdField = 'ID';

    public $propParams = [
        'COLLECTION' => [
            'CODE' => 'COLLECTION',
            'typeInFilter' => 'radio',
            'searchParamCode' => 'collection',
            'hasEGroups' => true,
        ],
        'PRINT_ID' => [
            'CODE' => 'PRINT_ID',
            'typeInFilter' => 'radio',
            'searchParamCode' => 'print',
            'hidden' => true,
        ],
        'TAGS' => [
            'CODE' => 'TAGS',
            'typeInFilter' => 'checkbox',
            'searchParamCode' => 'tags',
            'hidden' => true,
        ]
    ];



    function __construct(){
        $this->cache_path = $_SERVER['DOCUMENT_ROOT'].static::CACHE_PATH;
        // Соберём массив $this->props
        $this->props = [];
        $this->propCodes = [];
        $this->propSearchParams = [];
        foreach ( $this->propParams as $key => $propParam ){
            // Инфа по свойству $propCode
            $prop = \AOptima\Tools\prop::getByCode( $propParam['CODE'], $this->iblock_id );
            // Если свойство СУЩЕСТВУЕТ
            if( intval($prop['ID']) > 0 ){
                $prop['typeInFilter'] = $propParam['typeInFilter'];
                $prop['hasEGroups'] = $propParam['hasEGroups']?true:false;
                $prop['experiencesValue'] = $propParam['experiencesValue']?$propParam['experiencesValue']:$propParam['typeInFilter'];
                $searchParam = null;
                if( $prop['CODE'] == 'COLLECTION' ){
                    $prop['searchParam'] = $this->propParams['COLLECTION']['searchParamCode'];
                } else if( $prop['CODE'] == 'PRINT_ID' ){
                    $prop['searchParam'] = $this->propParams['PRINT_ID']['searchParamCode'];
                } else if( $prop['CODE'] == 'TAGS' ){
                    $prop['searchParam'] = $this->propParams['TAGS']['searchParamCode'];
                }
                $this->props[$key] = $prop;
                $this->propCodes[$key] = $prop['CODE'];
                $this->propSearchParams[$key] = $propParam['searchParamCode'];
            }
        }
    }


    public function getPropCodeByParamName( $paramName ){
        $propCode = null;
        foreach ( $this->propParams as $pCode => $prop ){
            if( $paramName == $prop['searchParamCode'] ){
                $propCode = $prop['CODE'];
            }
        }
        return $propCode;
    }

    public function getParamNameByPropCode( $propCode ){
        $paramName = null;
        foreach ( $this->propParams as $pCode => $prop ){
            if( $propCode == $prop['CODE'] ){
                $paramName = $prop['searchParamCode'];
            }
        }
        return $paramName;
    }


    public function getEGroups( $prop ){
        \Bitrix\Main\Loader::includeModule('iblock');
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 30*60;
        $cache_id = 'filter_e_groups_'.$prop['LINK_IBLOCK_ID'].'_'.$prop['CODE'];
        $cache_path = '/filter_e_groups/'.$prop['LINK_IBLOCK_ID'].'/'.$prop['CODE'].'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $list = [];
            $filter = [
                "ACTIVE" => "Y",
                "DEPTH_LEVEL" => 1,
                "IBLOCK_ID" => $prop['LINK_IBLOCK_ID']
            ];
            $dbSections = \CIBlockSection::GetList( [ "SORT" => "ASC" ], $filter );
            while ($section = $dbSections->GetNext()){
                $list[] = [
                    'id' => $section['ID'],
                    'sort' => $section['SORT'],
                    'label' => $section['NAME'],
                ];
            }
            $obCache->EndDataCache([ 'list' => $list ]);
        }
        return $list;
    }


    // Создание кэшей
    public function create(){
        // Проверка наличия рабочей папки
        if( !file_exists( $this->cache_path ) ){
            mkdir( $this->cache_path, 0700 );
        }
        // Перебираем свойства
        foreach ( $this->props as $prop ){
            // Если тип свойства E (привязка к элементам инфоблока)
            if( $prop['PROPERTY_TYPE'] == 'E' ){
                $this->createForPropE( $prop );
            }
        }
    }


    // Создание кэшей длс свойств типа E
    public function createForPropE( $prop ){

        \Bitrix\Main\Loader::includeModule('iblock');

        // Автосоздание папки для свойства
        $path = $this->cache_path.$prop['CODE'].'/';
        if( !file_exists( $path ) ){    mkdir($path, 0700);    }

        // Массив для новых файлов кэша
        $newFiles = [];

        // Текущий набор файлов кэша
        $currentFiles = $this->getCurrentFiles( $path );

        // Получим полный набор значений свойства $prop
        $all_values = $this->getAllPropValuesE( $prop );

        // Будем запрашивать товары, по каждому элементу
        foreach ( $all_values as $value_el_id ){
            $product_ids = [];
            $filter = [
                "IBLOCK_ID" => $this->iblock_id,
                "ACTIVE" => "Y",
                "PROPERTY_".$prop['CODE'] => $value_el_id
            ];
            $fields = [ "ID", "PROPERTY_".$prop['CODE'] ];
            if( $this->productIdField != 'ID' ){   $fields[] = $this->productIdField;   }
            $products = \CIBlockElement::GetList(
                ["SORT" => "ASC"], $filter, false, false, $fields
            );
            while ( $product = $products->GetNext() ){
                if( $this->productIdField == 'ID' ){
                    $product_ids[] = $product['ID'];
                } else {
                    $product_ids[] = $product[$this->productIdField.'_VALUE'];
                }
            }
            $product_ids = array_unique($product_ids);

            // Сохраняем файл кэша
            $fileName = $value_el_id.".json";
            $filePath = $path.$fileName;
            $file = fopen($filePath, "w");
            $res = fwrite( $file,  json_encode( $product_ids ) );
            fclose($file);

            $newFiles[] = $fileName;
        }

        // Удаляем устаревшие файлы (те, которыйх нет в $newFiles)
        if( count($currentFiles) > 0 ){
            $filesToDelete = array_diff($currentFiles, $newFiles);
            foreach ( $filesToDelete as $fileName ){
                unlink( $path.$fileName );
            }
        }
    }



    // Все элементы соответствующего инфоблока/справочника
    public function getAllPropValuesE( $prop ){
        $list = [];
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 30*60;
        $cache_id = 'filter_cache_ib_prop_all_e_items_'.$prop['LINK_IBLOCK_ID'].'_'.$prop['CODE'];
        $cache_path = '/filter_cache_ib_prop_all_e_items/'.$prop['LINK_IBLOCK_ID'].'/'.$prop['CODE'].'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
        	$vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $filter = [
                "IBLOCK_ID" => $prop['LINK_IBLOCK_ID'],
                "ACTIVE" => "Y",
            ];
            $fields = [ "ID" ];
            $items = \CIBlockElement::GetList(
                ["SORT"=>"ASC"], $filter, false, false, $fields
            );
            while ($item = $items->GetNext()){
                $list[] = $item['ID'];
            }
        $obCache->EndDataCache([ 'list' => $list ]);
        }
        return $list;
    }




    // Все значения
    public function getAllValues( $propCode ){
        $list = [];
        // Инфа по свойству $propCode
        $prop = \AOptima\Tools\prop::getByCode( $propCode, $this->iblock_id );
        // Если тип свойства E (привязка к элементам инфоблока)
        if( $prop['PROPERTY_TYPE'] == 'E' ){
            // Получим все возможные значения фильтра
            $list = $this->getAllPropValuesE( $prop );

        }
        return $list;
    }


    

}

