<? namespace AOptima\Project;
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;



AddEventHandler("iblock", "OnBeforeIBlockElementDelete", array('\AOptimaproject\catalog_print', 'OnBeforeIBlockElementDeleteHandler'));




class catalog_print {
	
	
	
	static function OnBeforeIBlockElementDeleteHandler($ID){
	    \Bitrix\Main\Loader::includeModule('iblock');
		$iblock_id = tools\el::getIblock($ID);
		if( $iblock_id == project\site::PRINTS_IBLOCK_ID ){
			
			$filter = Array(
			    "ACTIVE" => "Y",
				"IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
				"PROPERTY_PRINT_ID" => $ID,
			);
			$fields = Array( "PROPERTY_PRINT_ID" );
			$dbElements = \CIBlockElement::GetList(
				array("SORT"=>"ASC"), $filter, false, array("nTopCount"=>1), $fields
			);
			while ($element = $dbElements->GetNext()){
				global $APPLICATION;
				$APPLICATION->throwException('В каталоге есть активные товары, привязанные к этому принту');
				return false;
			}
			
		}

	}
	
	
	
	
	
	protected $iblock_id = project\site::PRINTS_IBLOCK_ID;
	protected $user_id;
	protected $photo;
	protected $fields;
	
	
	
	function __construct ($id = false){
		if( intval($id) > 0 ){
			$print = tools\el::info($id);
			if( intval($print['ID']) > 0 ){
				$this->fields = $print;
			}
			if( intval($print['PROPERTY_USER_ID_VALUE']) > 0 ){
				$this->user_id = $print['PROPERTY_USER_ID_VALUE'];
			}
			if( strlen($print['PROPERTY_PHOTO_VALUE']) > 0 ){
				$this->photo = $print['PROPERTY_PHOTO_VALUE'];
			}
            if( strlen($print['PROPERTY_ZAYAVKA_ID_VALUE']) > 0 ){
                $this->zayavka_id = $print['PROPERTY_ZAYAVKA_ID_VALUE'];
            }
		}
	}
	
	
	
	// Список принтов пользователя
	public function getList($user_id){
	    \Bitrix\Main\Loader::includeModule('iblock');
		$list = array();
		$dbElements = \CIBlockElement::GetList(
			Array("SORT"=>"ASC"),
			Array(
				"IBLOCK_ID"=>$this->iblock_id,
				"ACTIVE"=>"Y",
				"PROPERTY_USER_ID"=>$user_id,
				"PROPERTY_STATUS_VALUE" => 'Активно',
				"!PROPERTY_PHOTO_ID" => false
			), false, false,
			Array("ID", "PROPERTY_USER_ID", "PROPERTY_STATUS", "PROPERTY_PHOTO_ID"));
		while ($element = $dbElements->GetNext()){
			$list['ACTIVE'][] = $element['ID'];
		}
		$dbElements = \CIBlockElement::GetList(
			Array("SORT"=>"ASC"),
			Array(
				"IBLOCK_ID"=>$this->iblock_id,
				"ACTIVE"=>"Y",
				"PROPERTY_USER_ID"=>$user_id,
				"PROPERTY_STATUS_VALUE" => false,
				"!PROPERTY_PHOTO_ID" => false
			), false, false,
			Array("ID", "PROPERTY_USER_ID", "PROPERTY_STATUS", "PROPERTY_PHOTO_ID"));
		while ($element = $dbElements->GetNext()){
			$list['NOT_ACTIVE'][] = $element['ID'];
		}
		return $list;
	}



    // Список всех принтов
    static function getListForSearchCache(){
        $prints = [];
        // Кешируем
        $obCache = new \CPHPCache();
        $cache_time = 30*24*60*60;
        $cache_id = 'all_prints';
        $cache_path = '/all_prints/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            \Bitrix\Main\Loader::includeModule('iblock');
            $filter = [
                "IBLOCK_ID" => project\site::PRINTS_IBLOCK_ID,
                "ACTIVE" => "Y",
            ];
            $fields = [ "ID" ];
            $dbElements = \CIBlockElement::GetList(
                [ "SORT" => "ASC" ], $filter, false, false, $fields
            );
            while ($element = $dbElements->GetNext()){
                $prints[] = $element['ID'];
			}
            $obCache->EndDataCache(['prints' => $prints]);
        }
        return $prints;
    }



	public function productsCNT( $id ){
        \Bitrix\Main\Loader::includeModule('iblock');
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 5*60;
        $cache_id = 'printProductsCNT_'.$id;
        $cache_path = '/printProductsCNT/'.$id.'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
        	$vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $dbElements = \CIBlockElement::GetList(
                Array("SORT"=>"ASC"),
                Array(
                    "IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
                    "ACTIVE" => "Y",
                    "PROPERTY_PRINT_ID" => $id
                ), false, false,
                Array("ID", "PROPERTY_PRINT_ID")
            );
            $totalCNT = $dbElements->SelectedRowsCount();
        $obCache->EndDataCache(array('totalCNT' => $totalCNT));
        }
        return $totalCNT;
    }
	
	
	// Создание принта
	public function add($name, $photo_id, $zayavka){
	    \Bitrix\Main\Loader::includeModule('iblock');
		if(
		    strlen($name) > 0
            &&
            intval($photo_id) > 0
            &&
            intval($zayavka['ID']) > 0
        ){
			$file = project\site::DOCUMENT_ROOT.\CFile::GetPath($photo_id);
			if( file_exists($file) ){

				$PROP = array();
				$PROP[43] = $zayavka['PROPERTY_USER_VALUE'];
				$PROP[100] = $zayavka['ID'];

                $print_photo_id = \CFile::CopyFile($photo_id);
                if( intval($print_photo_id) > 0 ){
                    $PROP[56] = $print_photo_id;
                }

				////////////////////////
				$element = new \CIBlockElement;
				$fields = Array(
					"IBLOCK_ID"				=> $this->iblock_id,
					"PROPERTY_VALUES"		=> $PROP,
					"NAME"					=> $name,
					"ACTIVE"				=> "Y"
				);

				if( $el_id = $element->Add($fields) ){
					project\site::deleteTempEl($photo_id);
					return $el_id;
				}
			}
		}
		return false;
	}
	
	
	
	
	
	
	
	// Проверка наличия цвета у иллюстратора
	static function checkColor($color_id, $print_id){
	    \Bitrix\Main\Loader::includeModule('iblock');
		$check = false;
		// Кешируем 
		$obCache = new \CPHPCache();
		$cache_time = 60*60;
		$cache_id = 'printCheckColor_'.$print_id.'_'.$color_id;
		$cache_path = '/printCheckColor/'.$print_id.'/'.$color_id.'/';
		if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
			$vars = $obCache->GetVars();   extract($vars);
		} elseif($obCache->StartDataCache()){
			$filter = Array(
				"IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
				"ACTIVE" => "Y",
				"PROPERTY_PRINT_ID" => $print_id,
				"PROPERTY_COLORS_AUTO" => $color_id,
			);
			$fields = Array( "PROPERTY_PRINT_ID", "PROPERTY_COLORS_AUTO" );
			$dbElements = \CIBlockElement::GetList(
				array("SORT"=>"ASC"), $filter, false, array("nTopCount"=>1), $fields
			);
			while ($element = $dbElements->GetNext()){  $check = true;  }
		$obCache->EndDataCache(array('check' => $check));
		}
		return $check;
	}
	
	
	
	
	// Проверка наличия коллекции у иллюстратора
	static function checkCollection($collection_id, $print_id){
	    \Bitrix\Main\Loader::includeModule('iblock');
		$check = false;
		// Кешируем 
		$obCache = new \CPHPCache();
		$cache_time = 60*60;
		$cache_id = 'printCheckCollection_'.$print_id.'_'.$collection_id;
		$cache_path = '/printCheckCollection/'.$print_id.'/'.$collection_id.'/';
		if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
			$vars = $obCache->GetVars();   extract($vars);
		} elseif($obCache->StartDataCache()){
			$filter = Array(
				"IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
				"ACTIVE" => "Y",
				"PROPERTY_PRINT_ID" => $print_id,
				"PROPERTY_COLLECTION" => $collection_id,
			);
			$fields = Array( "PROPERTY_PRINT_ID", "PROPERTY_COLLECTION" );
			$dbElements = \CIBlockElement::GetList(
				array("SORT"=>"ASC"), $filter, false, array("nTopCount"=>1), $fields
			);
			while ($element = $dbElements->GetNext()){  $check = true;  }
		$obCache->EndDataCache(array('check' => $check));
		}
		return $check;
	}
	
	
	
	
	
}