<? namespace AOptima\Project;
use AOptima\Project as project;



class search_sections {



    static function searchByTitle( $q ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $q = strip_tags(trim($q));
        $list = [];
        $filter = array(
        	"ACTIVE" => "Y",
        	"IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
            "NAME" => "%".$q."%",
            "UF_FOR_SEARCH" => 1
        );
        $dbSections = \CIBlockSection::GetList(
        	array("NAME" => "ASC"),
        	$filter, false, [ "UF_FOR_SEARCH", "UF_ICON" ]
        );
        while ($section = $dbSections->GetNext()){
            $list[$section['ID']] = $section;
        }
        return $list;
    }

	

	static function searchByQ( $q ){

		\Bitrix\Main\Loader::includeModule("search");
        \Bitrix\Main\Loader::includeModule('aoptima.tools');

        $q = strip_tags($q);
        $ids = array();

		$obSearch = new \CSearch;
		$obSearch->Search(
            [
                "QUERY" => $q,
                "SITE_ID" => LANG,
                [
                    [
                        "=MODULE_ID" => "iblock", 
                        "ITEM_ID" => "S%",
                        "PARAM2" => project\site::CATALOG_IBLOCK_ID
                    ]
                ]
            ],
            ["TITLE_RANK"=>"DESC"], ['STEMMING' => true]
		);
		if ( $obSearch->errorno != 0 ){} else {
			while($item = $obSearch->GetNext()){
			    $sect_id = preg_replace('/S/', '', $item['ITEM_ID']);
                $sect = \AOptima\Tools\section::info( $sect_id );
                if (
                    intval($sect['ID']) > 0
                    &&
                    $sect['ACTIVE'] == 'Y'
                ){     $ids[$sect['ID']] = $sect;     }
			}
		}
		if ( count($ids) == 0 ){
			$obSearch = new \CSearch;
			$obSearch->Search(
				[
					"QUERY" => $q,
					"SITE_ID" => LANG,
					[
						[
							"=MODULE_ID" => "iblock", "ITEM_ID" => "S%",
							"PARAM2" => project\site::CATALOG_IBLOCK_ID
						]
					]
				],
				["TITLE_RANK"=>"DESC"], ['STEMMING' => false]
			);
			if ($obSearch->errorno != 0){} else {
				while($item = $obSearch->GetNext()){
                    $sect_id = preg_replace('/S/', '', $item['ITEM_ID']);
                    $sect = \AOptima\Tools\section::info( $sect_id );
                    if (
                        intval($sect['ID']) > 0
                        &&
                        $sect['ACTIVE'] == 'Y'
                    ){     $ids[$sect['ID']] = $sect;     }
				}
			}	
		}
		return $ids;
	}




	static function request($get){
        
        \Bitrix\Main\Loader::includeModule('iblock');
		
		$_GET = $get;
		
		// Проверка ключа
		if(
			!$_GET['key']
			||
			$_GET['key'] != project\site::API_ACCESS_KEY
		){
			
			$arResponse['error'] = 'Ошибка авторизации';
			$json = json_encode($arResponse);
			
		} else {
			
			
			$arResponse = array();
			
			// Формируем фильтр
			$filter = array(
				'IBLOCK_ID' => project\site::CATALOG_IBLOCK_ID,
				'ACTIVE' => 'Y'
			);
			
			if( intval($_GET['id']) > 0 ){
				$filter['ID'] = $_GET['id'];
			}
			
			if( intval($_GET['depth']) > 0 ){
				$filter['DEPTH_LEVEL'] = $_GET['depth'];
			}
			
			if( strlen($_GET['q']) > 0 ){
				$ids = static::searchByQ($_GET['q']);
				if( count($ids) > 0 ){
					$filter['ID'] = $ids;
				} else {
					$filter['ID'] = 0;
				}
			}
			
			if( intval($_GET['section_id']) > 0 ){
				$filter['SECTION_ID'] = $_GET['section_id'];
			}

			// Запрос
			$dbSections = \CIBlockSection::GetList(Array("NAME"=>"ASC"), $filter, false);
			$totalCNT = $dbSections->SelectedRowsCount();
			while ($section = $dbSections->GetNext()){
				$section_chain = section_chain($section['ID']);
				$f_section = section_info($section_chain[0]['ID']);
				$arSect = array(
					'id' => $section['ID'],
					'name' => trim($section['NAME']),
					'url' => $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].$section['SECTION_PAGE_URL'],
					'section_id' => $section['IBLOCK_SECTION_ID'],
					'gender' => $f_section['UF_POL'],
				);
				$arResponse['sections'][$section['ID']] = $arSect;
			}
				
			$arResponse['response_info']['sections_count'] = $totalCNT;

			
			$json = json_encode($arResponse);
			
		}
		
		return $json;
		
	}





}