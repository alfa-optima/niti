<? namespace AOptima\Project;
use AOptima\Project as project;


class search_prints {



    static function searchByQ( $q ){

        \Bitrix\Main\Loader::includeModule("search");
        \Bitrix\Main\Loader::includeModule('aoptima.tools');

        $q = strip_tags($q);

        $ids = [];

        $obSearch = new \CSearch;
        $obSearch->Search(
            [
                "QUERY" => $q, "SITE_ID" => LANG,
                [
                    [
                        "=MODULE_ID" => "iblock", "!ITEM_ID" => "S%",
                        "PARAM2" => project\site::PRINTS_IBLOCK_ID
                    ]
                ]
            ],
            ["TITLE_RANK" => "DESC"], ['STEMMING' => true]
        );
        if ($obSearch->errorno != 0) {} else {
            while ($item = $obSearch->GetNext()) {
                $el = \AOptima\Tools\el::info($item['ITEM_ID']);
                if (
                    intval($el['ID']) > 0
                    &&
                    $el['ACTIVE'] == 'Y'
                ){     $ids[$el['ID']] = $el;     }
            }
        }
        if (count($ids) == 0) {
            $obSearch = new \CSearch;
            $obSearch->Search(
                [
                    "QUERY" => $q, "SITE_ID" => LANG,
                    [
                        [
                            "=MODULE_ID" => "iblock", "!ITEM_ID" => "S%",
                            "PARAM2" => project\site::PRINTS_IBLOCK_ID
                        ]
                    ]
                ],
                [ "TITLE_RANK" => "DESC" ], [ 'STEMMING' => false ]
            );
            if ($obSearch->errorno != 0) {} else {
                while ($item = $obSearch->GetNext()) {
                    $el = \AOptima\Tools\el::info($item['ITEM_ID']);
                    if (
                        intval($el['ID']) > 0
                        &&
                        $el['ACTIVE'] == 'Y'
                    ){     $ids[$el['ID']] = $el;     }
                }
            }
        }
        return $ids;
    }




}
