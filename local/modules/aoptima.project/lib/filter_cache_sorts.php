<? namespace AOptima\Project;
use AOptima\Project as project;



class filter_cache_sorts extends filter_cache {


    const CACHE_PATH = '/local/php_interface/filter_caches/sorts/';



    function __construct(){

        $this->cache_path = $_SERVER['DOCUMENT_ROOT'].static::CACHE_PATH;

    }



    // Создание кэшей
    public function create(){

        // Проверка наличия рабочей папки
        if( !file_exists( $this->cache_path ) ){
            mkdir( $this->cache_path, 0700 );
        }

        \Bitrix\Main\Loader::includeModule('iblock');

        // Массив для новых файлов кэша
        $newFiles = [];

        // Текущий набор файлов кэша
        $currentFiles = $this->getCurrentFiles( $this->cache_path );

        // Получим полный набор категорий каталога
        $sorts = project\search_sorts::getList();

        // Переберём категории
        foreach ( $sorts as $sort ){
            if( !in_array($sort['CODE'], [ 'relevant' ]) ){
                $product_ids = [];
                $filter = [
                    "IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
                    "ACTIVE" => "Y",
                    "INCLUDE_SUBSECTIONS" => "Y"
                ];
                $fields = [ "ID" ];
                /// ELASTIC
                if( $sort['ELASTIC'] ){   $fields[] = 'NAME';   }
                ///
                $arSort = [];
                if( isset($sort['FIELD_1']) && isset($sort['ORDER_1']) ){
                    $arSort[$sort['FIELD_1']] = $sort['ORDER_1'];
                    if( isset($sort['FIELD_2']) && isset($sort['ORDER_2']) ){
                        $arSort[$sort['FIELD_2']] = $sort['ORDER_2'];
                    }
                }
                $products = \CIBlockElement::GetList( $arSort, $filter, false, false, $fields );
                while ($product = $products->GetNext()){
                    $product_ids[] = $product['ID'];
                    /// ELASTIC
                    if( $sort['ELASTIC'] ){
                        project\elastic::setItem(
                            $product['ID'],
                            ['article' => $product['ID'], 'name' => $product['NAME']],
                            'model_index'
                        );
                    }
                    ///
                }
                $product_ids = array_unique($product_ids);

                // Сохраняем файл кэша
                $fileName = $sort['CODE'].".json";
                $filePath = $this->cache_path.$fileName;
                $file = fopen($filePath, "w");
                $res = fwrite( $file,  json_encode( $product_ids ) );
                fclose($file);

                $newFiles[] = $fileName;
            }
        }

        // Удаляем устаревшие файлы (те, которыйх нет в $newFiles)
        if( count($currentFiles) > 0 ){
            $filesToDelete = array_diff($currentFiles, $newFiles);
            foreach ( $filesToDelete as $fileName ){
                unlink( $this->cache_path.$fileName );
            }
        }
    }







}

