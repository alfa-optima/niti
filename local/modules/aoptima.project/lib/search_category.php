<? namespace AOptima\Project;
use AOptima\Project as project;



class search_category {


    static $searchParamCode = 'cats';


    static function getElList(
        $only_for_home_page_hits = false,
        $search_sect_id = false
    ){
        $list = [];

        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 30*60;
        if( intval($search_sect_id) > 0 ){
            $cache_id = 'search_categories_'.$search_sect_id;
            $cache_path = '/search_categories/'.$search_sect_id.'/';
        } else {
            $cache_id = 'search_categories';
            $cache_path = '/search_categories/';
        }
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif( $obCache->StartDataCache() ){
            \Bitrix\Main\Loader::includeModule('iblock');
            $filter = [
                "IBLOCK_ID" => project\site::SEARCH_CATEGORIES_IBLOCK_ID,
                "ACTIVE" => "Y"
            ];
            if( intval($search_sect_id) > 0 ){
                $filter['SECTION_ID'] = $search_sect_id;
            }
            $fields = [ "ID", "NAME", "CODE", "PROPERTY_CAT_SECTIONS", "PROPERTY_USE_IN_HITS" ];
            $search_categories = \CIBlockElement::GetList(
                ["SORT"=>"ASC"], $filter, false, false, $fields
            );
            while ($search_category = $search_categories->GetNext()){
                $list[$search_category['ID']] = $search_category;
            }
            $obCache->EndDataCache(array('list' => $list));
        }
        if( $only_for_home_page_hits ){
            foreach ( $list as $search_category_id => $search_category ){
                if( !$search_category['PROPERTY_USE_IN_HITS_VALUE'] ){
                    unset($list[$search_category_id]);
                }
            }
        }
        return $list;
    }



    static function getSections1Level( $only_for_home_page_hits = false ){
        $list = [];
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 30*60;
        $cache_id = 'search_categories_sections';
        $cache_path = '/search_categories_sections/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            \Bitrix\Main\Loader::includeModule('iblock');
            $filter = [
            	"ACTIVE" => "Y",
            	"DEPTH_LEVEL" => 1,
            	"IBLOCK_ID" => project\site::SEARCH_CATEGORIES_IBLOCK_ID
            ];
            $dbSections = \CIBlockSection::GetList( [ "SORT" => "ASC" ], $filter, false, [ "UF_USE_IN_HITS" ] );
            while ($section = $dbSections->GetNext()){
                $list[ $section['ID'] ] = $section;
            }
            $obCache->EndDataCache(array('list' => $list));
        }
        if( $only_for_home_page_hits ){
            foreach ( $list as $key => $section ){
                if( !$section['UF_USE_IN_HITS'] ){
                    unset($list[$key]);
                }
            }
        }
        return $list;
    }


	
}