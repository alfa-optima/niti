<? namespace AOptima\Project;
use AOptima\Project as project;


class favorites {


    const COOKIE_NAME = 'favorites';



    static function isFavorite( $product_id ){
        $favorites = project\favorites::list();
        return in_array($product_id, $favorites);
    }



    static function list(){
        global $APPLICATION;
        $str = $APPLICATION->get_cookie(static::COOKIE_NAME);
        if( strlen($str) > 0 ){
            $list = explode('|', $str);
        } else {
            $list = array();
        }
        return $list;
    }





}