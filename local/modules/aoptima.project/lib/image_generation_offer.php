<? namespace AOptima\Project;

use AOptima\Project as project;

use Intervention\Image\ImageManagerStatic as Image;
use PhpParser\Node\Expr\Cast\Object_;

class image_generation_offer
{
    const DOCUMENT_ROOT = "/home/bitrix/www";
    static $type = 'prod';
    static $return = 'save';

    static protected $widthArtboard = 220;
    static protected $heightArtboard = 220;

    static protected $widthImage = NULL;
    static protected $heightImage = NULL;

    static protected $xImage = NULL;
    static protected $yImage = NULL;

    static protected $mimeType = 'png';

    static private $productPhoto = NULL;
    static private $printPhoto = NULL;

    static private $position;

    static private $quality = 100;

    function __construct($config)
    {

        try {
            /*
             * Холст
             */
            if (!empty($config['width_artboard']) && !empty($config['height_artboard'])) {
                $this->widthArtboard = $config['width_artboard'];
                $this->heightArtboard = $config['height_artboard'];
            } else {
                throw new Exception("Missing required parameter: width_artboard or height_artboard");
            }

            $this->hexArtboard = (!empty($config['hex_artboard']) ? $config['hex_artboard'] : "#ffffff");

            if (!empty($config['type_position']) && in_array($config['type_position'], $this->listTypePosition)) {
                $this->typePosition = $config['type_position'];
            } else {
                throw new Exception("Missing required parameter: type_position");
            }


            /*
             * Изображение
             */
            if (!empty($config['key_image'])) {
                $this->keyImage = self::DOCUMENT_ROOT . '/ajax/image-creator/' . $config['key_image'];
            } else {
                throw new Exception("Missing required parameter: key_image");
            }

            $this->widthImage = (!empty($config['width_image']) ? $config['width_image'] : $this->widthArtboard * 0.8);

            if (!empty($config['width_p_image'])) {
                $this->widthPercentImage = intval($config['width_p_image']);
                $this->calculationWidthImageByWidthPercent();
            }

            if (!empty($config['height_p_image'])) {
                $this->heightPercentImage = intval($config['height_p_image']);
                $this->calculationHeightImageByHeightPercent();
            }

            if (!empty($config['height_image'])) {
                $this->heightImage = $config['height_image'];
            }

            $this->xImage = (!empty($config['x_image'] && is_null($this->xImage)) ? $config['x_image'] : 0);
            $this->yImage = (!empty($config['y_image'] && is_null($this->xImage)) ? $config['y_image'] : 0);

            if (!empty($config['type_position'])) {
                $this->typePosition = $config['type_position'];
            }

            if (!empty($config['file_exception']) && in_array($config['file_exception'], $this->listFileException)) {
                $this->mimeType = $config['file_exception'];
            }

        } catch (\Exception $e) {

            header('HTTP/1.1 404 Not Found');
            header('Content-Type: image/png');
            header('Content-ErrorMsg: ' . $e->getMessage());

            echo base64_decode('iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAAApJREFUCNdjYAAAAAIAAeIhvDMAAAAASUVORK5CYII=');
            die();

        }

    }

    static private function setSettings($params)
    {

        self::$productPhoto = self::DOCUMENT_ROOT . \CFile::GetPath($params['product_photo_id']);
        self::$printPhoto = self::DOCUMENT_ROOT . \CFile::GetPath($params['print_photo_id']);


        //$print_photo = 'https://i-love-png.com/images/kraken_body01_final_01_1766.png';
        //$print_photo = 'https://craft.atlassian.design/uploads/guidelines/marketing/overview/marketing-overview.png';
        //self::$printPhoto = 'https://lcidev.bobbentleydesign.com/wp-content/uploads/2019/11/workshop-illustration.png';


        self::$position->w = $params['coordinate']['w'];
        self::$position->h = $params['coordinate']['h'];

        self::$position->x = $params['coordinate']['x'];
        self::$position->y = $params['coordinate']['y'];


        if(!empty($params['coordinate']['custom'])){

            /*
    [l] => 38.6
    [t] => 311.4
    [w] => 179.1
    [h] => 321
             */
            // echo '<pre>';
            // print_r($params['coordinate']['custom']);
            // echo '</pre>';

            self::$position->w = intval($params['coordinate']['custom']['w']);
            self::$position->h = intval($params['coordinate']['custom']['h']);

            self::$position->x = intval(self::$position->x + $params['coordinate']['custom']['l']);
            self::$position->y = intval(self::$position->y + $params['coordinate']['custom']['t']);

        }



        //TODO
        if (empty(self::$position->w)) {
            self::$position->w = 450;
        }

        if (empty(self::$position->h)) {
            self::$position->h = 700;
        }

        if (empty(self::$position->x)) {
            self::$position->x = 400;
        }

        if (empty(self::$position->y)) {
            self::$position->y = 400;
        }

    }

    static public function make($params)
    {

        //self::$type = 'dev';

        self::setSettings($params);

        if (self::$type === 'dev') {
            echo '<pre style="font-size: 17px;background-color: #363636;color: #fff;padding: 20px;line-height: 26px;">';
            print_r($params);
            print_r(self::$productPhoto);
            echo '<br>';
            print_r(self::$printPhoto);
            echo '<br>';
            print_r(self::$position);
            echo '</pre>';
            exit();
        }

        $product_photo_object = Image::make(self::$productPhoto);
        self::$widthArtboard = $product_photo_object->width();
        self::$heightArtboard = $product_photo_object->height();

        $img = Image::canvas(self::$widthArtboard, self::$heightArtboard);
        $img->insert(self::$productPhoto);

        // $background = Image::canvas(self::$position->w, self::$position->h, '#cccccc');
        $background = Image::canvas(self::$position->w, self::$position->h);

        $image = Image::make(self::$printPhoto)->resize(self::$position->w, self::$position->h, function ($c) {
            $c->aspectRatio();
            // $c->upsize();
        });
        $background->insert($image, 'top-left', 0, 0);
        $img->insert($background, 'top-left', self::$position->x, self::$position->y);
        $img->encode(self::$mimeType);

        if (self::$return === 'view') {

            header('Content-Type: image/' . self::$mimeType);
            echo $img;

        } elseif (self::$return === 'save') {

            $pathTemp = self::DOCUMENT_ROOT . '/upload/offer_photos_tmp/' . time() . '.png';
            $img->save($pathTemp, self::$quality, self::$mimeType);
            $arFile = \CFile::MakeFileArray($pathTemp);
            $fid = \CFile::SaveFile($arFile, 'offer_photos');
            unlink($pathTemp);
            return $fid;

        }

    }
}
