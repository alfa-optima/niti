<? namespace AOptima\Project;
use AOptima\Project as project;



class search_sorts {


    const DEFAULT_SORT = 'new';



    static $items = [

        'new' => [
            'CODE' => 'new',
            'NAME' => 'Сначала новые',
            'ELASTIC' => true,
            'FIELD_1' => 'ID',
            'ORDER_1' => 'DESC',
            'FIELD_2' => 'NAME',
            'ORDER_2' => 'ASC',
        ],

        'old' => [
            'CODE' => 'old',
            'NAME' => 'Сначала старые',
            'FIELD_1' => 'ID',
            'ORDER_1' => 'ASC',
            'FIELD_2' => 'NAME',
            'ORDER_2' => 'ASC',
        ],

        'relevant' => [
            'CODE' => 'relevant',
            'NAME' => 'По релевантности',
        ],

    ];



    static function getList (){
        $list = [];
        foreach ( static::$items as $item_code => $item ){
            $list[$item_code] = $item;
        }
        return $list;
    }







}