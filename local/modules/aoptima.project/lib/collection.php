<? namespace AOptima\Project;
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;



class collection {



	static function OnBeforeIBlockElementDeleteHandler($ID){
	    \Bitrix\Main\Loader::includeModule('iblock');
		$iblock_id = tools\el::getIblock($ID);
		if( $iblock_id == project\site::PRINTS_IBLOCK_ID ){
			$filter = Array(
				"IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
				"PROPERTY_COLLECTION" => $ID,
			);
			$fields = Array( "PROPERTY_COLLECTION" );
			$dbElements = \CIBlockElement::GetList(
				array("SORT"=>"ASC"), $filter, false, array("nTopCount"=>1), $fields
			);
			while ($element = $dbElements->GetNext()){
				global $APPLICATION;
				$APPLICATION->throwException('В каталоге есть товары, привязанные к этой коллекции');
				return false;
			}
		}
	}



	// Наличие товаров в коллекции
	static function hasGoods($collection_id){
	    \Bitrix\Main\Loader::includeModule('iblock');
		if( intval($collection_id) > 0 ){
			$filter = Array(
				"IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
				"ACTIVE" => "Y",
				"PROPERTY_COLLECTION" => $collection_id,
				//"PROPERTY_HAS_SKU" => 1
			);
			$fields = Array("ID", "PROPERTY_HAS_SKU", "PROPERTY_COLLECTION");
			$collections = \CIBlockElement::GetList(Array("SORT"=>"ASC"), $filter, false, Array("nTopCount"=>1), $fields);
			while ($collection = $collections->GetNext()){ $cnt++;
				return true;
			}
		}
		return false;
	}



	// Коллекции для главной
	static function getMainList(){
	    \Bitrix\Main\Loader::includeModule('iblock');
		$mainCollections = array();
		// Кешируем
		$obCache = new \CPHPCache();
		$cache_time = 30*24*60*60;
		$cache_id = 'mainCollections';
		$cache_path = '/mainCollections/';
		if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
			$vars = $obCache->GetVars();   extract($vars);
		} elseif($obCache->StartDataCache()){
			$filter = Array(
				"IBLOCK_ID" => project\site::COLLECTIONS_IBLOCK_ID,
				"ACTIVE" => "Y",
				"PROPERTY_MAIN_PAGE_VALUE" => "Да"
			);
			$fields = Array("ID", "NAME", "DETAIL_PAGE_URL", "DETAIL_PICTURE", "PROPERTY_MAIN_PAGE", "PROPERTY_HORIZONTAL_PHOTO", "PROPERTY_VERTICAL_PHOTO");
			// Запрос
			$cnt = 0; $key = 0;
			$collections = \CIBlockElement::GetList(Array("SORT"=>"ASC"), $filter, false, false, $fields);
			while ($collection = $collections->GetNext()){ $cnt++;
				$mainCollections[$key][] = clean_array($collection);
				if( $cnt%10 == 0 ){   $key++;   }
			}
		$obCache->EndDataCache(array('mainCollections' => $mainCollections));
		}
		return $mainCollections;
	}



	// Все коллекции
	static function getList( $ids = false ){
	    \Bitrix\Main\Loader::includeModule('iblock');
		$all_collections = array();
		// Кешируем
		$obCache = new \CPHPCache();
		$cache_time = 30*24*60*60;
		$cache_id = 'allCollections';
		$cache_path = '/allCollections/';
		if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
			$vars = $obCache->GetVars();   extract($vars);
		} elseif($obCache->StartDataCache()){
			$filter = [
				"IBLOCK_ID" => project\site::COLLECTIONS_IBLOCK_ID,
				"ACTIVE" => "Y"
			];
			$fields = [ "ID", "NAME", "CODE", "DETAIL_PAGE_URL", "DETAIL_PICTURE", "PROPERTY_PARENT_ID" ];
			// Запрос
			$collections = \CIBlockElement::GetList(Array("SORT"=>"ASC", "NAME"=>"ASC"), $filter, false, false, $fields);
			while ($collection = $collections->GetNext()){ $cnt++;
				$all_collections[$collection['ID']] = clean_array($collection);
			}
		$obCache->EndDataCache(array('all_collections' => $all_collections));
		}
		if( is_array($ids) ){
		    foreach ( $all_collections as $collection_id => $collection ){
		        if( !in_array($collection_id, $ids) ){
		            unset($all_collections[$collection_id]);
		        }
		    }
		}
		return $all_collections;
	}



	static function recursiveList($all_collections, $parent_id = 0, $level = 0){
		global $collectionRecursiveList;
		foreach ( $all_collections as $collection ){
			if ( $collection['PROPERTY_PARENT_ID_VALUE'] == $parent_id ){
				$collection['LEVEL'] = $level+1;
				$collectionRecursiveList[] = array(
					'ID' => $collection['ID'],
					'NAME' => $collection['NAME'],
					'PARENT_ID' => $collection['PROPERTY_PARENT_ID_VALUE'],
					'LEVEL' => $collection['LEVEL'],
				);
				static::recursiveList($all_collections, $collection['ID'], $level + 1);
			}
		}
	}



	static function groupListForFilter($collectionRecursiveList){
		$groupList = array();
		foreach( $collectionRecursiveList as $key => $item ){
			if( $item['LEVEL'] == 1 ){
				$groupList[$key] = $item;
				$parent_key = $key;
			} else {
				$groupList[$parent_key]['SUB_ITEMS'][$item['ID']] = $item;
			}
		}
		return $groupList;
	}



}

