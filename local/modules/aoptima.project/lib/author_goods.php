<? namespace AOptima\Project;
use AOptima\Project as project;
use AOptima\Tools as tools;



class author_goods {
    


    static function list( $product_id, $other = false ){
        \Bitrix\Main\Loader::includeModule('iblock');
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        $ids = array();
        $el = tools\el::info($product_id);
        if( intval( $el['PROPERTY_PRINT_ID_VALUE'] ) > 0 ){
            
            $el_sections = tools\el::sections($el['ID']);
            $section_id = $el_sections[0]['ID'];
            $section_chain = tools\section::chain($section_id);
            $section_id = $section_chain[1]['ID'];
            
            // инфо по принту
            $print = tools\el::info($el['PROPERTY_PRINT_ID_VALUE']);
            if (
                intval($print['ID']) > 0
                &&
                intval($print['PROPERTY_USER_ID_VALUE']) > 0
            ) {
                $user_id = $print['PROPERTY_USER_ID_VALUE'];
                if (
                    !$_SESSION['el_other_prints'][$el['ID']]
                    ||
                    (time() - $_SESSION['el_other_prints'][$el['ID']]['time']) > 60
                ) {
                    // все принты автора
                    $prints = project\illustrator::getPrints($user_id);
                    foreach ($prints as $key => $id) {
                        if ($id == $print['ID']) {    unset($prints[$key]);    }
                    }
                    shuffle($prints);
                    $_SESSION['el_other_prints'][$el['ID']] = array(
                        'prints' => $prints,
                        'time' => time()
                    );
                } else {
                    $prints = $_SESSION['el_other_prints'][$el['ID']]['prints'];
                }
                $prints = array_slice($prints, 0, 6);
                foreach ($prints as $key => $id) {
                    $filter = Array(
                        "IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
                        "ACTIVE" => "Y",
                        "PROPERTY_PRINT_ID" => $id,
                        //"PROPERTY_HAS_SKU" => 1,
                        "SECTION_ID" => $section_id,
                        "INCLUDE_SUBSECTIONS" => "Y"
                    );
                    if( $other ){
                        $filter['!ID'] = $product_id;
                    }
                    $fields = Array("ID", "PROPERTY_PRINT_ID", "PROPERTY_HAS_SKU");
                    $goods = \CIBlockElement::GetList(
                        array("SORT" => "ASC"), $filter, false, array("nTopCount" => 1), $fields
                    );
                    while ($good = $goods->GetNext()) {
                        $ids[] = $good['ID'];
                    }
                }
            }
        }
        return $ids;
    }

    
    


}