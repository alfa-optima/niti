<? namespace AOptima\Project;
use AOptima\Project as project;



class search_filter {


    static function getSorts(){
        
        $ib_prop_cache = new project\filter_cache_ib_prop();
        $ib_prop_cache_tp = new project\filter_cache_ib_prop_tp();
        $gender_cache = new project\filter_cache_gender();
        $product_types_cache = new project\filter_cache_product_types();
        $illustrators_cache = new project\filter_cache_illustrators();

        $sorts = [];
        if( isset($ib_prop_cache_tp->propParams['COLOR']['searchParamCode']) ){
            $sorts[$ib_prop_cache_tp->propParams['COLOR']['searchParamCode']] = 10;
        }
        if( isset($ib_prop_cache->propParams['COLLECTION']['searchParamCode']) ){
            $sorts[$ib_prop_cache->propParams['COLLECTION']['searchParamCode']] = 20;
        }
        if( isset($gender_cache->searchParamCode) ){
            $sorts[$gender_cache->searchParamCode] = 30;
        }
        if( isset($product_types_cache->searchParamCode) ){
            $sorts[$product_types_cache->searchParamCode] = 40;
        }
        if( isset($illustrators_cache->searchParamCode) ){
            $sorts[$illustrators_cache->searchParamCode] = 50;
        }
        if( isset($ib_prop_cache->propParams['PRINT_ID']['searchParamCode']) ){
            $sorts[$ib_prop_cache->propParams['PRINT_ID']['searchParamCode']] = 60;
        }
        if( isset($ib_prop_cache->propParams['TAGS']['searchParamCode']) ){
            $sorts[$ib_prop_cache->propParams['TAGS']['searchParamCode']] = 70;
        }
        if( isset($ib_prop_cache_tp->propParams['SIZE']['searchParamCode']) ){
            $sorts[$ib_prop_cache_tp->propParams['SIZE']['searchParamCode']] = 80;
        }

        asort($sorts);

        return $sorts;
    }


    static function getItems ( $arResult ){

        // Получим URL фильтра
        $arResult = static::getFilterURL( $arResult );

        // Категории фильтра
        $arResult = static::getFilterCats( $arResult );

        // Пункты фильтра
        $arResult = static::getFilterItems( $arResult );

        // Сортировка пунктов фильтра
        $arResult = static::sortFilterItems( $arResult );

        return $arResult;
    }




    // Получение URL фильтра
    static function getFilterURL( $arResult ){

        $filter_cache_gender = new project\filter_cache_gender();
        $filter_cache_illustrators = new project\filter_cache_illustrators();
        $filter_cache_product_types = new project\filter_cache_product_types();
        $filter_cache_ib_prop = new project\filter_cache_ib_prop();
        $filter_cache_ib_prop_tp = new project\filter_cache_ib_prop_tp();

        $arResult['getUrl'] = project\search::getBaseURL( $arResult );
        foreach ( $arResult['get'] as $key => $value ){
            if(
                !(
                    (
                        $key == $filter_cache_illustrators->searchParamCode
                        &&
                        project\site::isIllustratorPage()
                    )
                    ||
                    (
                        $key == $filter_cache_ib_prop->propParams['PRINT_ID']['searchParamCode']
                        &&
                        project\site::isPrintPage()
                    )
                    ||
                    (
                        $key == $filter_cache_ib_prop->propParams['COLLECTION']['searchParamCode']
                        &&
                        project\site::isCollectionPage()
                    )
                )
            ){
                $arResult['getUrl'] = \AOptima\Tools\funcs::addGetParamToURL( $arResult['getUrl'], $key, $value );
            }
        }
        //if( $arResult['params']['page'] == 1 ){
            $arResult['getUrl'] = \AOptima\Tools\funcs::removeGetParamFromURL( $arResult['getUrl'], 'page' );
        //}
        return $arResult;
    }




    // Категории для поиска
    static function getFilterCats ( $arResult ){

        \Bitrix\Main\Loader::includeModule('iblock');
        \Bitrix\Main\Loader::includeModule('aoptima.tools');

        $filter_cache_gender = new project\filter_cache_gender();
        $filter_cache_illustrators = new project\filter_cache_illustrators();
        $filter_cache_product_types = new project\filter_cache_product_types();
        $filter_cache_ib_prop = new project\filter_cache_ib_prop();
        $filter_cache_ib_prop_tp = new project\filter_cache_ib_prop_tp();

        // Получим все поисковые категории
        $catsSections1Level = project\search_category::getSections1Level();

        // Получим все поисковые элементы-категории
        $allCatEls = project\search_category::getElList();

        foreach ( $catsSections1Level as $section_1_level ){

            $cats = [];   $arCatEls = [];

            // Отфильтруем по принадлежности категории 1 уровня
            foreach ( $allCatEls as $arCatEl ){
                $catSectID = \AOptima\Tools\el::sections( $arCatEl['ID'] )[0]['ID'];
                if( $catSectID == $section_1_level['ID'] ){    $arCatEls[] = $arCatEl;    }
            }

            // Переберём элементы-категории
            foreach ( $arCatEls as $cat ){

                $applied = is_array($arResult['params'][ project\search_category::$searchParamCode ]) && in_array($cat['CODE'], $arResult['params'][ project\search_category::$searchParamCode ]);

                // Договорились, что категории не учавствуют в appliedCount, appliedPath и resets

                $link = project\search::getBaseURL( $arResult );
                if( strlen($arResult['params']['query']) > 0 ){
                    $link = \AOptima\Tools\funcs::addGetParamToURL( $link, 'query', $arResult['params']['query'] );
                }
                $link = \AOptima\Tools\funcs::addGetParamToURL( $link, project\search_category::$searchParamCode, $cat['CODE'] );
                $link = \AOptima\Tools\funcs::addGetParamToURL( $link, 'schema', 'json' );

                $cats[] = [
                    'name' => $cat['CODE'],
                    'label' => $cat['NAME'],
                    'applied' => $applied,
                    'link' => $link,
                ];
            }

            $arResult['response']['filters']['staticFilters'][] = [
                'type' => $section_1_level['CODE'],
                'label' => $section_1_level['NAME'],
                'options' => $cats,
            ];
        }

        // Получим категории-элементы верхнего уровня (без род. категорий)
        $topLevelEls = [];
        foreach ( $allCatEls as $arCatEl ){
            $catSectID = \AOptima\Tools\el::sections($arCatEl['ID'])[0]['ID'];
            // Если нет привязки к категории
            if( !( intval($catSectID) > 0 ) ){

                $applied = is_array($arResult['params'][ project\search_category::$searchParamCode ]) && in_array($arCatEl['CODE'], $arResult['params'][ project\search_category::$searchParamCode ]);

                $link = project\search::getBaseURL( $arResult );
                if( strlen($arResult['params']['query']) > 0 ){
                    $link = \AOptima\Tools\funcs::addGetParamToURL( $link, 'query', $arResult['params']['query'] );
                }
                $link = \AOptima\Tools\funcs::addGetParamToURL( $link, project\search_category::$searchParamCode, $arCatEl['CODE'] );
                $link = \AOptima\Tools\funcs::addGetParamToURL( $link, 'schema', 'json' );

                $arResult['response']['filters']['staticFilters'][] = [
                    'name' => $arCatEl['CODE'],
                    'label' => $arCatEl['NAME'],
                    'applied' => $applied,
                    'link' => $link,
                ];
            }
        }

        return $arResult;
    }




    // Пункты фильтра
    static function getFilterItems( $arResult ){

        \Bitrix\Main\Loader::includeModule('aoptima.tools');

        $ib_prop_cache = new project\filter_cache_ib_prop();
        $ib_prop_cache_tp = new project\filter_cache_ib_prop_tp();
        $gender_cache = new project\filter_cache_gender();
        $product_types_cache = new project\filter_cache_product_types();
        $illustrators_cache = new project\filter_cache_illustrators();

        $illustratorPageID = project\site::isIllustratorPage();
        $printPageID = project\site::isPrintPage();
        $collectionPageCode = project\site::isCollectionPage();

    // ПОЛ
        $gender_items = [];   $hasApplied = false;
        $gender_types = project\gender_type::list();
        $selectedGroups = isset($arResult['params'][$searchParam])?$arResult['params'][$searchParam]:null;
        if( !isset($selectedGroups) ){    $selectedGroups = [];    }

        $hidden = false;
        $searchParam = $gender_cache->searchParamCode;
        $inputType = 'checkbox';
        
        foreach ( $gender_types as $gender_type ){
            if(
                !$hidden
                ||
                ( $hidden && in_array($gender_type['CODE'], $selectedGroups) )
            ){

                // Получим из кэша ID товаров по данному фильтру
                $cacheProductsIDs = $gender_cache->getCacheProductsIDs( $gender_type['CODE'] );

                // Если есть товары по данному фильтру
                if( count($cacheProductsIDs) > 0 ){

                    $applied = is_array( $arResult['params'][ $searchParam ] ) && in_array( $gender_type['CODE'], $arResult['params'][ $searchParam ] );

                    if( $applied ){

                        $hasApplied = true;

                        $arResult['response']['filters']['appliedPath'][] = $searchParam;

                        $resetLink = $arResult['getUrl'];
                        if( $inputType == 'radio' ){
                            $resetLink = \AOptima\Tools\funcs::removeGetParamFromURL($resetLink, $gender_type['CODE']);
                        } else if( $inputType == 'checkbox' ){
                            $resetLink = \AOptima\Tools\funcs::removeSubValueFromGetParamInURL($resetLink, $searchParam, $gender_type['CODE']);
                        }
                        $resetLink = \AOptima\Tools\funcs::addGetParamToURL( $resetLink, 'schema', 'json' );

                        $arResult['response']['filters']['resets'][] = [
                            'label' => $gender_type['NAME'],
                            'link' => $resetLink,
                        ];
                    }

                    $disabled = false;

                    $link = $arResult['getUrl'];
                    if( $inputType == 'radio' ){
                        $link = \AOptima\Tools\funcs::addGetParamToURL( $link, $searchParam, $gender_type['CODE'] );
                    } else if( $inputType == 'checkbox' ){
                        if( $applied ){
                            $link = \AOptima\Tools\funcs::removeSubValueFromGetParamInURL($link, $searchParam, $gender_type['CODE']);
                        } else {
                            $link = \AOptima\Tools\funcs::addSubValueToGetParamInURL($link, $searchParam, $gender_type['CODE']);
                        }
                    }
                    $link = \AOptima\Tools\funcs::addGetParamToURL( $link, 'schema', 'json' );

                    $gender_items[] = [
                        'id' => $gender_type['ID'],
                        'name' => $gender_type['CODE'],
                        'label' => $gender_type['NAME'],
                        'applied' => $applied,
                        'disabled' => $disabled,
                        'link' => $link,
                        'products_count' => 0,
                    ];
                }

                $arResult['caches'][ $searchParam ][$gender_type['CODE']] = $cacheProductsIDs;
                unset($cacheProductsIDs);
            }
        }

        // Если есть пукты с товарами
        if( $gender_items > 0 ){

            // Пункт "Все"
            $applied = !$hasApplied;

            $disabled = false;

            // Добавим пункт в фильтр
            $arResult['response']['filters']['filters'][] = [
                'type' => $searchParam,
                'label' => 'Пол/Группа',
                'hasApplied' => $hasApplied,
                'hidden' => $hidden,
                'experiences' => [
                    [
                        'name' => 'control',
                        'value' => 'checkbox',
                    ]
                ],
                'items' => $gender_items
            ];
        }


    // ТИП ТОВАРА
        $product_type_items = [];   $hasApplied = false;
        // Получим полный набор типов товаров
        $product_types = project\search_product_types::getList();
        $selectedProductTypes = isset($arResult['params'][$searchParam])?$arResult['params'][$searchParam]:null;
        if( !isset($selectedProductTypes) ){    $selectedProductTypes = [];    }

        $searchParam = $product_types_cache->searchParamCode;
        $hidden = true;
        $inputType = 'radio';

        // Переберём типы товаров
        foreach ( $product_types as $product_type ){
            if(
                !$hidden
                ||
                ( $hidden && in_array($product_type['CODE'], $selectedProductTypes) )
            ){

                // Получим из кэша ID товаров по данному фильтру
                $cacheProductsIDs = $product_types_cache->getCacheProductsIDs( $product_type['CODE'] );

                // Если есть товары по данному фильтру
                if( count($cacheProductsIDs) > 0 ){

                    $applied = is_array( $arResult['params'][ $searchParam ] ) && in_array( $product_type['CODE'], $arResult['params'][ $searchParam ] );

                    if( $applied ){

                        $hasApplied = true;

                        $arResult['response']['filters']['appliedPath'][] = $searchParam;

                        $resetLink = $arResult['getUrl'];
                        if( $inputType == 'radio' ){
                            $resetLink = \AOptima\Tools\funcs::removeGetParamFromURL($resetLink, $product_type['CODE']);
                        } else if( $inputType == 'checkbox' ){
                            $resetLink = \AOptima\Tools\funcs::removeSubValueFromGetParamInURL($resetLink, $searchParam, $product_type['CODE']);
                        }
                        $resetLink = \AOptima\Tools\funcs::addGetParamToURL( $resetLink, 'schema', 'json' );

                        $arResult['response']['filters']['resets'][] = [
                            'label' => $product_type['NAME'],
                            'link' => $resetLink,
                        ];
                    }

                    $disabled = false;

                    $link = $arResult['getUrl'];
                    if( $inputType == 'radio' ){
                        $link = \AOptima\Tools\funcs::addGetParamToURL( $link, $searchParam, $product_type['CODE'] );
                    } else if( $inputType == 'checkbox' ){
                        if( $applied ){
                            $link = \AOptima\Tools\funcs::removeSubValueFromGetParamInURL($link, $searchParam, $product_type['CODE']);
                        } else {
                            $link = \AOptima\Tools\funcs::addSubValueToGetParamInURL($link, $searchParam, $product_type['CODE']);
                        }
                    }
                    $link = \AOptima\Tools\funcs::addGetParamToURL( $link, 'schema', 'json' );

                    $product_type_items[] = [
                        'id' => $product_type['ID'],
                        'name' => $product_type['CODE'],
                        'label' => $product_type['NAME'],
                        'applied' => $applied,
                        'disabled' => $disabled,
                        'link' => $link,
                        'products_count' => 0,
                    ];
                }
    
                $arResult['caches'][ $searchParam ][ $product_type['CODE'] ] = $cacheProductsIDs;
                unset($cacheProductsIDs);
            }
        }
        
        // Если есть пукты с товарами
        if( $product_type_items > 0 ){

            // Пункт "Все"
            $applied = !$hasApplied;

            $disabled = false;

            $link = $arResult['getUrl'];
            $link = \AOptima\Tools\funcs::removeGetParamFromURL($link,  $searchParam );
            $link = \AOptima\Tools\funcs::addGetParamToURL( $link, 'schema', 'json' );

            array_unshift($product_type_items, [
                'name' => 'reset',
                'label' => 'Все',
                'applied' => $applied,
                'disabled' => $disabled,
                'link' => $link,
            ]);

            // Добавим пункт в фильтр
            $arResult['response']['filters']['filters'][] = [
                'type' => $searchParam,
                'label' => 'Тип товара',
                'hasApplied' => $hasApplied,
                'hidden' => $hidden,
                'experiences' => [
                    [
                        'name' => 'control',
                        'value' => 'radio',
                    ]
                ],
                'items' => $product_type_items
            ];

        }


    // ИЛЛЮСТРАТОР
        $illustrator_items = [];   $hasApplied = false;
        // Получим полный набор типов товаров
        $illustrators = project\illustrator::getList();
        $searchParam = $illustrators_cache->searchParamCode;
        // Проверим находимся ли мы на странице иллюстратора
        $hidden = true;
        //$hidden = intval($illustratorPageID)>0||intval($printPageID)>0;
        $inputType = 'radio';
        // Переберём типы товаров
        foreach ( $illustrators as $illustrator_id ){
            if(
                !$hidden
                ||
                ( $hidden && $illustrator_id == $illustratorPageID )
            ){

                $illustrator = \AOptima\Tools\user::info( $illustrator_id );

                // Получим из кэша ID товаров по данному фильтру
                $cacheProductsIDs = $illustrators_cache->getCacheProductsIDs( $illustrator_id );

                // Если есть товары по данному фильтру
                if( count($cacheProductsIDs) > 0 ){

                    $applied = is_array( $arResult['params'][$searchParam] ) && in_array( $illustrator['ID'], $arResult['params'][$searchParam] );

                    if( $applied ){

                        $hasApplied = true;

                        $arResult['response']['filters']['appliedPath'][] = $searchParam;

                        $resetLink = $arResult['getUrl'];
                        if( $inputType == 'radio' ){
                            $resetLink = \AOptima\Tools\funcs::removeGetParamFromURL($resetLink, $illustrator_id);
                        } else if( $inputType == 'checkbox' ){
                            $resetLink = \AOptima\Tools\funcs::removeSubValueFromGetParamInURL($resetLink, $searchParam, $illustrator_id);
                        }
                        $resetLink = \AOptima\Tools\funcs::addGetParamToURL( $resetLink, 'schema', 'json' );

                        $arResult['response']['filters']['resets'][] = [
                            'label' => \AOptima\Tools\user::getFullName( $illustrator_id ),
                            'link' => $resetLink,
                        ];
                    }

                    $disabled = false;

                    $link = $arResult['getUrl'];
                    if( $inputType == 'radio' ){
                        $link = \AOptima\Tools\funcs::addGetParamToURL( $link, $searchParam, $illustrator_id );
                    } else if( $inputType == 'checkbox' ){
                        if( $applied ){
                            $link = \AOptima\Tools\funcs::removeSubValueFromGetParamInURL($link, $searchParam, $illustrator_id);
                        } else {
                            $link = \AOptima\Tools\funcs::addSubValueToGetParamInURL($link, $searchParam, $illustrator_id);
                        }
                    }
                    $link = \AOptima\Tools\funcs::addGetParamToURL( $link, 'schema', 'json' );

                    $illustrator_items[] = [
                        'id' => $illustrator['ID'],
                        'name' => $illustrator['ID'],
                        'label' => \AOptima\Tools\user::getFullName( $illustrator_id ),
                        'applied' => $applied,
                        'disabled' => $disabled,
                        'link' => $link,
                        'products_count' => 0,
                    ];
                }

                $arResult['caches'][$searchParam][ $illustrator_id ] = $cacheProductsIDs;
                unset($cacheProductsIDs);
            }
        }

        // Если есть пукты с товарами
        if( $illustrator_items > 0 ){

            // Пункт "Все"
            $applied = !$hasApplied;

            $disabled = false;

            $link = $arResult['getUrl'];
            $link = \AOptima\Tools\funcs::removeGetParamFromURL($link, $searchParam);
            $link = \AOptima\Tools\funcs::addGetParamToURL( $link, 'schema', 'json' );

            array_unshift($illustrator_items, [
                'name' => 'reset',
                'label' => 'Все',
                'applied' => $applied,
                'disabled' => $disabled,
                'link' => $link,
            ]);

            // Добавим пункт в фильтр
            $arResult['response']['filters']['filters'][] = [
                'type' => $searchParam,
                'label' => 'Иллюстратор',
                'hidden' => $hidden,
                'hasApplied' => $hasApplied,
                'experiences' => [
                    [
                        'name' => 'control',
                        'value' => 'radio',
                    ]
                ],
                'items' => $illustrator_items
            ];

        }

    // СВОЙСТВА ИНФОБЛОКОВ (КАТАЛОГ)
        foreach ( $ib_prop_cache->props as $prop ){

            $searchParam = $ib_prop_cache->getParamNameByPropCode( $prop['CODE'] );

            // Если тип свойства E (привязка к элементам инфоблока)
            if( $prop['PROPERTY_TYPE'] == 'E' ){

                $hidden = (
                    (
                        $prop['searchParam'] == $ib_prop_cache->propParams['COLLECTION']['searchParamCode']
                        &&
                        project\site::isCollectionPage()
                    )
                    ||
                    (
                        $prop['searchParam'] == $ib_prop_cache->propParams['PRINT_ID']['searchParamCode']
                        &&
                        project\site::isPrintPage()
                    )
                    ||
                    $ib_prop_cache->propParams[$prop['CODE']]['hidden']
                )?true:false;

                $selectedValues = isset( $arResult['params'][ $searchParam ] )?$arResult['params'][$searchParam]:null;
                if( !isset($selectedValues) ){    $selectedValues = [];    }

                $collectionPageID = null;
                if( strlen($collectionPageCode) > 0 ){
                    $collection = \AOptima\Tools\el::info_by_code($collectionPageCode, project\site::COLLECTIONS_IBLOCK_ID);
                    if( intval($collection['ID']) > 0 ){
                        $collectionPageID = $collection['ID'];
                    }
                }

                $items = [];
                $hasApplied = false;

                // Получим все возможные значения фильтра
                $all_values = $ib_prop_cache->getAllPropValuesE( $prop );

                foreach ( $all_values as $value_el_id ){

                    if(
                        !$hidden
                        ||
                        (
                            $hidden
                            &&
                            (
                                $printPageID == $value_el_id
                                ||
                                $collectionPageID == $value_el_id
                                ||
                                in_array($value_el_id, $selectedValues)
                            )
                        )
                    ){

                        // Получим из кэша ID товаров по данному фильтру
                        $cacheProductsIDs = $ib_prop_cache->getCacheProductsIDs( $value_el_id, $prop['CODE'] );

                        // Если есть товары по данному фильтру
                        if( count($cacheProductsIDs) > 0 ){

                            // инфо по элементу
                            $el = \AOptima\Tools\el::info( $value_el_id );
                            if( intval($el['ID']) > 0 ){

                                $search_value = (isset($el['CODE'])&&strlen($el['CODE'])>0)?$el['CODE']:$el['ID'];

                                // параметры значения
                                $applied = is_array( $arResult['params'][ $searchParam ] ) && in_array( $value_el_id, $arResult['params'][ $searchParam ] );

                                if( $applied ){

                                    $hasApplied = true;

                                    $arResult['response']['filters']['appliedPath'][] = $searchParam;

                                    $resetLink = $arResult['getUrl'];
                                    if( $prop['typeInFilter'] == 'radio' ){
                                        $resetLink = \AOptima\Tools\funcs::removeGetParamFromURL($resetLink, $searchParam);
                                    } else if( in_array($prop['typeInFilter'], ['checkbox', 'color']) ){
                                        $resetLink = \AOptima\Tools\funcs::removeSubValueFromGetParamInURL($resetLink, $searchParam, $search_value);
                                    }
                                    $resetLink = \AOptima\Tools\funcs::addGetParamToURL( $resetLink, 'schema', 'json' );

                                    $arResult['response']['filters']['resets'][] = [
                                        'label' => $el['NAME'],
                                        'link' => $resetLink,
                                    ];
                                }

                                $disabled = false;

                                $link = $arResult['getUrl'];
                                if( $prop['typeInFilter'] == 'radio' ){
                                    $link = \AOptima\Tools\funcs::addGetParamToURL($link, $searchParam, $search_value);
                                } else if( in_array($prop['typeInFilter'], ['checkbox', 'color']) ){
                                    if( $applied ){
                                        $link = \AOptima\Tools\funcs::removeSubValueFromGetParamInURL($link, $searchParam, $search_value);
                                    } else {
                                        $link = \AOptima\Tools\funcs::addSubValueToGetParamInURL($link, $searchParam, $search_value);
                                    }
                                }
                                $link = \AOptima\Tools\funcs::addGetParamToURL( $link, 'schema', 'json' );

                                $item = [
                                    'id' => $el['ID'],
                                    'name' => $el['ID'],
                                    'label' => $el['NAME'],
                                    'applied' => $applied,
                                    'disabled' => $disabled,
                                    'link' => $link,
                                    'products_count' => 0,
                                ];

                                if( $prop['hasEGroups'] ){
                                    $el_sect_id = \AOptima\Tools\el::sections($el['ID'])[0]['ID'];
                                    $item['group_id'] = $el_sect_id;
                                    $item['sort'] = $el['SORT'];
                                }

                                $items[] = $item;
                            }
                        }

                        $arResult['caches']['props'][$prop['CODE']][$value_el_id] = $cacheProductsIDs;
                        unset($cacheProductsIDs);
                    }
                }

                // Пункт "Все"
                if( $prop['typeInFilter'] == 'radio' ){

                    $applied = !$hasApplied;
                    $disabled = false;

                    $link = $arResult['getUrl'];
                    $link = \AOptima\Tools\funcs::removeGetParamFromURL($link, $searchParam);
                    $link = \AOptima\Tools\funcs::addGetParamToURL( $link, 'schema', 'json' );

                    array_unshift($items, [
                        'name' => 'reset',
                        'label' => 'Все',
                        'applied' => $applied,
                        'disabled' => $disabled,
                        'link' => $link,
                    ]);

                }

                $filterItem = [
                    'type' => $searchParam,
                    'label' => $prop['NAME'],
                    'hidden' => $hidden,
                    'hasApplied' => $hasApplied,
                    'experiences' => [
                        [
                            'name' => 'control',
                            'value' => $prop['experiencesValue'],
                        ]
                    ],
                    'items' => $items
                ];

                if( $prop['hasEGroups'] ){
                    $filterItem['groups'] = $ib_prop_cache->getEGroups( $prop );
                }

                $arResult['response']['filters']['filters'][] = $filterItem;
            }
        }


    // СВОЙСТВА ИНФОБЛОКОВ (ТП)
        foreach ( $ib_prop_cache_tp->props as $prop ){

            $searchParam = $ib_prop_cache_tp->getParamNameByPropCode( $prop['CODE'] );
            
            // Если тип свойства E (привязка к элементам инфоблока)
            if( $prop['PROPERTY_TYPE'] == 'E' ){

                $hidden = $ib_prop_cache_tp->propParams[ $prop['CODE'] ]['hidden']?true:false;
                
                $selectedValues = isset( $arResult['params'][ $searchParam ] )?$arResult['params'][$searchParam]:null;
                if( !isset($selectedValues) ){    $selectedValues = [];    }
                
                $items = [];
                $hasApplied = false;

                // Получим все возможные значения фильтра
                $all_values = $ib_prop_cache_tp->getAllPropValuesE( $prop );

                foreach ( $all_values as $value_el_id ){
                    if(
                        !$hidden
                        ||
                        ( $hidden && in_array($value_el_id, $selectedValues) )
                    ){
                    
                        // Получим из кэша ID товаров по данному фильтру
                        $cacheProductsIDs = $ib_prop_cache_tp->getCacheProductsIDs( $value_el_id, $prop['CODE'] );
    
                        // Если есть товары по данному фильтру
                        if( count($cacheProductsIDs) > 0 ){
    
                            // инфо по элементу
                            $el = \AOptima\Tools\el::info( $value_el_id );
                            if( intval($el['ID']) > 0 ){
    
                                $search_value = (isset($el['CODE'])&&strlen($el['CODE'])>0)?$el['CODE']:$el['ID'];
    
                                // параметры значения
                                $applied = is_array( $arResult['params'][ $searchParam ] ) && in_array( $value_el_id, $arResult['params'][ $searchParam ] );
    
                                if( $applied ){
    
                                    $hasApplied = true;
    
                                    $arResult['response']['filters']['appliedPath'][] = $searchParam;
    
                                    $resetLink = $arResult['getUrl'];
                                    if( $prop['typeInFilter'] == 'radio' ){
                                        $resetLink = \AOptima\Tools\funcs::removeGetParamFromURL($resetLink, $searchParam);
                                    } else if( in_array($prop['typeInFilter'], ['checkbox', 'color']) ){
                                        $resetLink = \AOptima\Tools\funcs::removeSubValueFromGetParamInURL($resetLink, $searchParam, $search_value);
                                    }
                                    $resetLink = \AOptima\Tools\funcs::addGetParamToURL( $resetLink, 'schema', 'json' );
    
                                    $arResult['response']['filters']['resets'][] = [
                                        'label' => $el['NAME'],
                                        'link' => $resetLink,
                                    ];
                                }
    
                                $disabled = false;
    
                                $link = $arResult['getUrl'];
                                if( $prop['typeInFilter'] == 'radio' ){
                                    $link = \AOptima\Tools\funcs::addGetParamToURL($link, $searchParam, $search_value);
                                } else if( in_array($prop['typeInFilter'], ['checkbox', 'color']) ){
                                    if( $applied ){
                                        $link = \AOptima\Tools\funcs::removeSubValueFromGetParamInURL($link, $searchParam, $search_value);
                                    } else {
                                        $link = \AOptima\Tools\funcs::addSubValueToGetParamInURL($link, $searchParam, $search_value);
                                    }
                                }
                                $link = \AOptima\Tools\funcs::addGetParamToURL( $link, 'schema', 'json' );
                                
                                $item = [
                                    'id' => $el['ID'],
                                    'name' => $el_value,
                                    'label' => $el['NAME'],
                                    'applied' => $applied,
                                    'disabled' => $disabled,
                                    'link' => $link,
                                    'products_count' => 0,
                                ];
    
                                if(
                                    isset($el['PROPERTY_COLOR_CODE_VALUE'])
                                    &&
                                    strlen($el['PROPERTY_COLOR_CODE_VALUE']) > 0
                                ){
                                    $item['color_hex'] = $el['PROPERTY_COLOR_CODE_VALUE'];
                                }

                                if( $prop['hasEGroups'] ){
                                    $el_sect_id = \AOptima\Tools\el::sections($el['ID'])[0]['ID'];
                                    $item['group_id'] = $el_sect_id;
                                    $item['sort'] = $el['SORT'];
                                }
    
                                $items[] = $item;
                            }
                        }
    
                        $arResult['caches']['props_tp'][$prop['CODE']][$value_el_id] = $cacheProductsIDs;
                        unset($cacheProductsIDs);
                    }
                }

                // Пункт "Все"
                if( $prop['typeInFilter'] == 'radio' ){

                    $applied = !$hasApplied;
                    $disabled = false;

                    $link = $arResult['getUrl'];
                    $link = \AOptima\Tools\funcs::removeGetParamFromURL($link, $searchParam);
                    $link = \AOptima\Tools\funcs::addGetParamToURL( $link, 'schema', 'json' );

                    array_unshift($items, [
                        'name' => 'reset',
                        'label' => 'Все',
                        'applied' => $applied,
                        'disabled' => $disabled,
                        'link' => $link,
                    ]);

                }

                $filterItem = [
                    'type' => $searchParam,
                    'label' => $prop['NAME'],
                    'hasApplied' => $hasApplied,
                    'hidden' => $hidden,
                    'experiences' => [
                        [
                            'name' => 'control',
                            'value' => $prop['experiencesValue'],
                        ]
                    ],
                    'items' => $items
                ];
                if( $prop['hasEGroups'] ){
                    $filterItem['groups'] = $ib_prop_cache_tp->getEGroups( $prop );
                }
                $arResult['response']['filters']['filters'][] = $filterItem;
            }
        }

        $arResult['response']['filters']['appliedPath'] = array_unique( $arResult['response']['filters']['appliedPath'] );
        $arResult['response']['filters']['appliedCount'] = count($arResult['response']['filters']['appliedPath']);

        return $arResult;
    }


    // Сортировка пунктов фильтра
    static function sortFilterItems( $arResult ){
        // Получим назначенные позиции в сортировке
        $sorts = static::getSorts();
        // Соберём новый массив $arResult['response']['filters']['filters']
        $newFilterItems = [];
        $allItems = [];
        $usedItems = [];
        $notUsedItems = [];
        foreach ( $sorts as $sortCode => $sortPosition ){
            foreach ( $arResult['response']['filters']['filters'] as $item ){
                $allItems[] = $item['type'];
                if( $sortCode == $item['type'] ){
                    $usedItems[] = $item['type'];
                    $newFilterItems[] = $item;
                }
            }
        }
        $allItems = array_unique($allItems);
        $notUsedItems = array_diff($allItems, $usedItems);
        if( count($notUsedItems) > 0 ){
            foreach ( $notUsedItems as $sortCode ){
                foreach ( $arResult['response']['filters']['filters'] as $item ){
                    if( $sortCode == $item['type'] ){
                        $newFilterItems[] = $item;
                    }
                }
            }
        }

        $arResult['response']['filters']['filters'] = $newFilterItems;
        unset($newFilterItems);

        return $arResult;
    }



    // Деактивация значений пунктов фильтра (первая)
    static function deactivateFilterValues_1( $arResult ){

        $filter_cache_gender = new project\filter_cache_gender();
        $filter_cache_illustrators = new project\filter_cache_illustrators();
        $filter_cache_product_types = new project\filter_cache_product_types();
        $ib_prop_cache = new project\filter_cache_ib_prop();
        $ib_prop_cache_tp = new project\filter_cache_ib_prop_tp();
        
        if( count( $arResult['products_ids'] ) > 0 ){

            // Перебираем пункты фильтра (ГРУППА/ПОЛ)
            foreach ( $arResult['response']['filters']['filters'] as $fkey => $filterItem ){
                if( $filterItem['type'] == $filter_cache_gender->searchParamCode ){
                    // Переберём все значения пункта
                    foreach ( $filterItem['items'] as $fvkey => $arValue ){
                        if(
                            intval( $arValue['id'] ) > 0
                            &&
                            is_array( $arResult['caches'][ $filter_cache_gender->searchParamCode ][ $arValue['name']  ] )
                        ){
                            // Определим, будут ли товары по данному значению свойства
                            // если его выбрать
                            $propProducts = array_intersect($arResult['products_ids'], $arResult['caches'][ $filter_cache_gender->searchParamCode ][ $arValue['name'] ]);
                            if( count($propProducts) == 0 && !$arValue['applied'] ){
                                $arResult['response']['filters']['filters'][$fkey]['items'][$fvkey]['disabled'] = true;
                            }
                            $arResult['response']['filters']['filters'][$fkey]['items'][$fvkey]['products_count'] = count($propProducts);
                        }
                    }
                }
            }

            // Перебираем пункты фильтра (ТИПЫ ТОВАРОВ)
            foreach ( $arResult['response']['filters']['filters'] as $fkey => $filterItem ){
                if( $filterItem['type'] == $filter_cache_product_types->searchParamCode ){
                    // Переберём все значения пункта
                    foreach ( $filterItem['items'] as $fvkey => $arValue ){
                        if(
                            intval( $arValue['id'] ) > 0
                            &&
                            is_array( $arResult['caches'][ $filter_cache_product_types->searchParamCode ][ $arValue['name']  ] )
                        ){
                            // Определим, будут ли товары по данному значению свойства
                            // если его выбрать
                            $propProducts = array_intersect($arResult['products_ids'], $arResult['caches'][ $filter_cache_product_types->searchParamCode ][ $arValue['name'] ]);
                            if( count($propProducts) == 0 && !$arValue['applied'] ){
                                $arResult['response']['filters']['filters'][$fkey]['items'][$fvkey]['disabled'] = true;
                            }
                            $arResult['response']['filters']['filters'][$fkey]['items'][$fvkey]['products_count'] = count($propProducts);
                        }
                    }
                }
            }

            // Перебираем пункты фильтра (ИЛЛЮСТРАТОРЫ)
            foreach ( $arResult['response']['filters']['filters'] as $fkey => $filterItem ){
                if( $filterItem['type'] == $filter_cache_illustrators->searchParamCode ){
                    // Переберём все значения пункта
                    foreach ( $filterItem['items'] as $fvkey => $arValue ){
                        if(
                            intval( $arValue['id'] ) > 0
                            &&
                            is_array( $arResult['caches'][ $filter_cache_illustrators->searchParamCode ][ $arValue['name']  ] )
                        ){
                            // Определим, будут ли товары по данному значению свойства
                            // если его выбрать
                            $propProducts = array_intersect($arResult['products_ids'], $arResult['caches']['illustrator'][ $arValue['name'] ]);
                            if( count($propProducts) == 0 && !$arValue['applied'] ){
                                $arResult['response']['filters']['filters'][$fkey]['items'][$fvkey]['disabled'] = true;
                            }
                            $arResult['response']['filters']['filters'][$fkey]['items'][$fvkey]['products_count'] = count($propProducts);
                        }
                    }
                }
            }

            // Перебираем пункты фильтра (свойства каталога)
            foreach ( $arResult['response']['filters']['filters'] as $fkey => $filterItem ){
                if( in_array($filterItem['type'], $ib_prop_cache->propSearchParams) ){
                    $prop_code = $ib_prop_cache->getPropCodeByParamName($filterItem['type']);
                    // Переберём все значения пункта
                    foreach ( $filterItem['items'] as $fvkey => $arValue ){
                        if(
                            intval( $arValue['id'] ) > 0
                            &&
                            is_array( $arResult['caches']['props'][ $prop_code ][ $arValue['id'] ] )
                        ){
                            // Определим, будут ли товары по данному значению свойства
                            // если его выбрать
                            $propProducts = array_intersect($arResult['products_ids'], $arResult['caches']['props'][ $prop_code ][ $arValue['id'] ]);
                            if( count($propProducts) == 0 && !$arValue['applied'] ){
                                $arResult['response']['filters']['filters'][$fkey]['items'][$fvkey]['disabled'] = true;
                            }
                            $arResult['response']['filters']['filters'][$fkey]['items'][$fvkey]['products_count'] = count($propProducts);
                        }
                    }
                }
            }

            // Перебираем пункты фильтра (свойства ТП)
            foreach ( $arResult['response']['filters']['filters'] as $fkey => $filterItem ){
                if( in_array($filterItem['type'], $ib_prop_cache_tp->propSearchParams) ){
                    $prop_code = $prop_code = $ib_prop_cache_tp->getPropCodeByParamName($filterItem['type']);
                    // Переберём все значения пункта
                    foreach ( $filterItem['items'] as $fvkey => $arValue ){
                        if(
                            intval( $arValue['id'] ) > 0
                            &&
                            is_array( $arResult['caches']['props_tp'][ $prop_code ][ $arValue['id'] ] )
                        ){
                            // Определим, будут ли товары по данному значению свойства
                            // если его выбрать
                            $propProducts = array_intersect($arResult['products_ids'], $arResult['caches']['props_tp'][ $prop_code ][ $arValue['id'] ]);
                            if( count($propProducts) == 0 && !$arValue['applied'] ){
                                $arResult['response']['filters']['filters'][$fkey]['items'][$fvkey]['disabled'] = true;
                            }
                            $arResult['response']['filters']['filters'][$fkey]['items'][$fvkey]['products_count'] = count($propProducts);
                        }
                    }
                }
            }
        }
        
        return $arResult;
    }



    static function getFilterItemInputType( $arResult, $checkParam ){
        foreach ( $arResult['response']['filters']['filters'] as $item ){
            if( $item['type'] == $checkParam ){
                return $item['experiences'][0]['value'];
            }
        }
        return false;
    }



    // Деактивация значений пунктов фильтра (вторая)
    static function deactivateFilterValues_2( $arResult ){
        
        $search = new project\search();

        $paramClasses = $search->paramClasses;
        
        // Все параметры фильтра
        $allParams = $search->params;
        // Стоп-параметры
        $stopParams = [
            'query', 'limit', 'page', project\search_category::$searchParamCode, 'schema', 'sort'
        ];
        // Все параметры, которые учавствуют в проверках
        $allParamsToCheck = array_diff( $allParams, $stopParams );
        // Применённые параметры
        $appliedParams = array_intersect( array_keys($arResult['params']), $allParamsToCheck );
        // Значения применённых параметров
        $appliedParamsValues = [];
        foreach ( $appliedParams as $appliedParam ){
            $appliedParamsValues[$appliedParam] = $arResult['params'][$appliedParam];
        }

        // Переберём все параметры, которые учавствуют в проверках
        foreach ( $allParamsToCheck as $checkParam ){
            if( isset( $paramClasses[ $checkParam ] ) ){

                // Получим сначала применённые параметры (кроме текущего)
                // Нам сначала надо найти набор товаров по всем параметрам - кроме текущего
                $otherAppliedParams = array_diff( $appliedParams, [ $checkParam ] );
                
                // Если есть применённые пункты, кроме текущего в цикле
                if( count($otherAppliedParams) > 0 ){
                    
                    // Будем перебирать применённые параметры - кроме текущего
                    $otherFilteredProducts = [];
                    foreach ( $otherAppliedParams as $otherParam ){
                        // Класс текущего параметра
                        $otherParamClass = '\\AOptima\\Project\\'.$paramClasses[ $otherParam ];
                        if( class_exists( $otherParamClass ) ){
                            $otherOb = new $otherParamClass();
                            // Получим (именно применённые) значения данного параметра
                            $otherParamValues = $appliedParamsValues[ $otherParam ];
                            // Будем перебирать применённые значения данного параметра
                            foreach ( $otherParamValues as $value ){
                                // Получим набор товаров отдельно с учётом (только) данного значения данного пункта
                                if( $otherOb->isIbProp ){
                                    $propCode = $otherOb->getPropCodeByParamName( $otherParam );
                                    $cachedProducts = $otherOb->getCacheProductsIDs( $value, $propCode );
                                } else {
                                    $cachedProducts = $otherOb->getCacheProductsIDs( $value );
                                }
                                $otherFilteredProducts[$otherParam][$value] = $cachedProducts;
                            }
                        }
                    }

                    // Ищём пересечения найденных выше товаров между собой
                    $otherAppliedParamsProducts = [];
                    if( count($otherFilteredProducts) > 0 ){

                        // начальный набор массива
                        $otherAppliedParamsProducts = $otherFilteredProducts[ array_keys($otherFilteredProducts)[0] ][ array_keys($otherFilteredProducts[ array_keys($otherFilteredProducts)[0] ])[0] ];

                        foreach ( $otherFilteredProducts as $paramName => $ars ){
                            $inputType = static::getFilterItemInputType( $arResult, $paramName );
                            if(
                                $inputType == 'checkbox'
                                ||
                                $inputType == 'color'
                            ){
                                foreach ( $ars as $value => $arProducts ){
                                    $otherAppliedParamsProducts = array_merge($otherAppliedParamsProducts, $arProducts);
                                }
                                $otherAppliedParamsProducts = array_unique($otherAppliedParamsProducts);
                            } else if( $inputType == 'radio' ){
                                foreach ( $ars as $value => $arProducts ){
                                    $otherAppliedParamsProducts = array_intersect($otherAppliedParamsProducts, $arProducts);
                                }
                             }
                        }

                        unset($otherFilteredProducts);
                    }

                    // Теперь возьмём текущий параметр
                    // Класс текущего параметра
                    $paramClass = '\\AOptima\\Project\\'.$paramClasses[ $checkParam ];
                    // Объект текущего параметра
                    $currentOb = new $paramClass();
                    // Получим все значения текущего параметра (из фактического фильтра)
                    $allValues = [];
                    // Переберём все пункты фильтра
                    foreach ( $arResult['response']['filters']['filters'] as $fkey => $fItem ){
                        $type = $checkParam;
                        // Если это текущий параметр
                        if( $fItem['type'] == $type ){
                            foreach ( $fItem['items'] as $fvkey => $arVal ){
                                if( $arVal['name'] != 'reset' ){
                                    $allValues[] = $arVal;
                                }
                            }
                        }
                    }

                    // Перебираем все значения текущего параметра
                    foreach ( $allValues as $arValue ){

                        $valueField = 'id';
                        if( $currentOb->isGender ){   $valueField = 'name';   }
                        if( $currentOb->isProductTypes ){   $valueField = 'name';   }

                        // Получим товары, которые можно получить применением данного значения в отдельности
                        $value = $arValue[$valueField];

                        if( $currentOb->isIbProp ){
                            $propCode = $currentOb->getPropCodeByParamName( $checkParam );
                            $cachedProducts = $currentOb->getCacheProductsIDs( $value, $propCode );
                        } else {
                            $cachedProducts = $currentOb->getCacheProductsIDs( $value );
                        }

                        // Скрестим данные товары с товарами по другим применённым параметрам
                        $valueProducts = array_intersect( $otherAppliedParamsProducts, $cachedProducts );

                        // Переберём все фильтры
                        foreach ( $arResult['response']['filters']['filters'] as $fkey => $fItem ){
                            $type = $checkParam;
                            // Если это текущий параметр
                            if( $fItem['type'] == $type ){
                                
                                // Переберём его значения
                                foreach ( $fItem['items'] as $fvkey => $arVal ){

// Если в итоге товаров по данному пересечению нет,
// то устанавливаем данному значению disabled
if(
    $arVal[$valueField] == $value
    &&
    $arVal['name'] != 'reset'
){
    if( count($valueProducts) == 0 && !$arVal['applied'] ){
        $arResult['response']['filters']['filters'][$fkey]['items'][$fvkey]['disabled'] = true;
    }
    $arResult['response']['filters']['filters'][$fkey]['items'][$fvkey]['products_count'] = count($valueProducts);
}

                                }
                            }
                        }
                    }
                }
             }
        }

        return $arResult;
    }




}