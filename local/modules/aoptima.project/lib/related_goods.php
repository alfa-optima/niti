<? namespace AOptima\Project;
use AOptima\Project as project;
use AOptima\Tools as tools;


// Также с этим принтом
class related_goods {


    const CACHE_NAME = 'related_goods';
    const DEFAULT_CNT = 50;




    static function list( $product_id, $max_cnt = false ){
        \Bitrix\Main\Loader::includeModule('iblock');
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        $related_goods = array();
        if( !$max_cnt ){    $max_cnt = static::DEFAULT_CNT;    }
        $el = tools\el::info($product_id);
        if( intval( $el['PROPERTY_PRINT_ID_VALUE'] ) > 0 ){
            $filter = Array(
                "IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,  "ACTIVE"=>"Y",
                "PROPERTY_PRINT_ID" => $el['PROPERTY_PRINT_ID_VALUE'],
                "!ID" => $product_id
            );
            $obCache = new \CPHPCache();
            $cache_time = 24*60*60;
            $cache_id = static::CACHE_NAME.'_'.$product_id.'_'.$el['PROPERTY_PRINT_ID_VALUE'].'_'.$max_cnt;
            $cache_path = '/'.static::CACHE_NAME.'/'.$product_id.'/'.$el['PROPERTY_PRINT_ID_VALUE'].'/'.$max_cnt.'/';
            if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
                $vars = $obCache->GetVars();   extract($vars);
            } elseif($obCache->StartDataCache()){
                $dbElements = \CIBlockElement::GetList(Array("RAND"=>"ASC"), $filter, false, Array("nTopCount" => $max_cnt), Array("ID", "PROPERTY_PRINT_ID"));
                while ($element = $dbElements->GetNext()){
                    $related_goods[] = $element['ID'];
                }
                $obCache->EndDataCache(array('related_goods' => $related_goods));
            }
        }
        shuffle($related_goods);
        return $related_goods;
    }




    // Рапределение товаров по табам
    static function tabs ( $related_goods ){
        $tabs = array();   $sorts = array();
        // если есть связ. товары
        if( count($related_goods) > 0 ){
            // переберём их
            foreach ( $related_goods as $tov_id ){
                
                $genderTypeID = false;
                
                // получим непосредственные разделы товара $tov_id
                $sect_id = tools\el::sections($tov_id)[0]['ID'];

                // получим массив цепочки
                $section_chain = tools\section::chain($sect_id);
                foreach ( $section_chain as $sect ){

                    $sect = tools\section::info($sect['ID']);
                    if( intval($sect['UF_GENDER_GROUP']) > 0 ){
                        $genderTypeID = $sect['UF_GENDER_GROUP'];
                    }
                }
                
                if( $genderTypeID ){

                    $genderType = project\gender_type::getByID($genderTypeID);

                    if( intval($genderType['ID']) > 0 ){
                        $tab_name = $genderType['NAME'];
                        $sorts[$tab_name] = $genderType['SORT'];
                        if( !in_array($tov_id, $tabs[$tab_name]) ){
                            $tabs[$tab_name][] = $tov_id;
                        }
                    }

                }

            }
        }
        array_multisort($sorts, SORT_ASC, SORT_NUMERIC, $tabs);
        return $tabs;
    }





}