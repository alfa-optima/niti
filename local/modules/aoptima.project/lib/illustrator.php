<? namespace AOptima\Project;
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;





class illustrator extends user {


	const PAGE_EL_CNT = 9;







	// Список всех иллюстраторов
	static function getList(){
		$illustrators = array();
		// Кешируем
		$obCache = new \CPHPCache();
		$cache_time = 30*24*60*60;
		$cache_id = 'illustrators';
		$cache_path = '/illustrators/';
		if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
			$vars = $obCache->GetVars();   extract($vars);
		} elseif($obCache->StartDataCache()){
			$filter = Array(
				"ACTIVE" => "Y",
				//"GROUPS_ID" => array(5),
				"UF_USER_TYPE" => array(2)
			);
			$fields = Array(
				'FIELDS' => array( 'ID' )
			);
			$rsUsers = \CUser::GetList(($by="id"), ($order="desc"), $filter, $fields);
			while ($arUser = $rsUsers->GetNext()){
				$illustrators[] = $arUser['ID'];
			}
		$obCache->EndDataCache(array('illustrators' => $illustrators));
		}
		return $illustrators;
	}




	// Случайный список N иллюстраторов
	static function getRandList( $cnt = 10, $stop_ids = [] ){
		$illustrators = array();
		$obCache = new \CPHPCache();
		$cache_time = 120;
		$cache_id = 'illustratorsRandList_'.md5(json_encode($stop_ids));
		$cache_path = '/illustratorsRandList/'.md5(json_encode($stop_ids)).'/';
		if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
			$vars = $obCache->GetVars();   extract($vars);
		} elseif($obCache->StartDataCache()){
			$filter = Array(
				"ACTIVE" => "Y",
				//"GROUPS_ID" => array(5),
				"UF_USER_TYPE" => array(2),
				"!PERSONAL_PHOTO" => false
			);
			if( count($stop_ids) > 0 ){
                $filter['ID'] = "~".implode("&~", $stop_ids);
			}
			$rsUsers = \CUser::GetList(
				($by="id"),
                ($order="desc"),
				$filter,
                [
                    'FIELDS' => [ 'ID' ],
                    'NAV_PARAMS' => ['nTopCount' => $cnt]
                ]
			);
			while ($arUser = $rsUsers->GetNext()){
				$illustrators[] = $arUser['ID'];
			}
			shuffle($illustrators);
		$obCache->EndDataCache(array('illustrators' => $illustrators));
		}
		return $illustrators;
	}




	// Заказы предыдущей недели
	static function prevWeekOrders(){
        \Bitrix\Main\Loader::includeModule('sale');
		$list = array();
		$obCache = new \CPHPCache();
		$cache_time = 12*60*60;
		$cache_id = 'prevWeekOrders';
		$cache_path = '/prevWeekOrders/';
		if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
			$vars = $obCache->GetVars();   extract($vars);
		} elseif($obCache->StartDataCache()){
			// Текущий день недели
			$w_day = week_day();
			// Берём прошлую неделю
			//$last_pn = date("d.m.Y", strtotime("-".($w_day + 7 - 1)." day"));
			$last_pn = date("d.m.Y", strtotime("-60 day"));
			$last_vs = date("d.m.Y", strtotime("-".($w_day)." day"));
			$filter_array = array(
				'>=DATE_INSERT' => $last_pn.' 00:00:00',
				'<=DATE_INSERT' => $last_vs.' 23:59:59',
			);
			$orders = \Bitrix\Sale\OrderTable::getList(array(
				'filter' => $filter_array,
				'select' => array( 'ID', 'DATE_INSERT', 'USER_ID' ),
				'order' => array('ID' => 'DESC')
			));
			while ($order = $orders->fetch()){
				$list[] = $order;
			}
		$obCache->EndDataCache(array('list' => $list));
		}
		return $list;
	}





	// Заказы за последние 30 дней
	static function monthOrders(){
        \Bitrix\Main\Loader::includeModule('sale');
		$list = array();
		$obCache = new \CPHPCache();
		$cache_time = 12*60*60;
		$cache_id = 'monthOrders';
		$cache_path = '/monthOrders/';
		if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
			$vars = $obCache->GetVars();   extract($vars);
		} elseif($obCache->StartDataCache()){
			// Берём прошлую неделю
			$date = date("d.m.Y", strtotime("-".($w_day + 30 - 1)." day"));
			$filter_array = array(
				'>=DATE_INSERT' => $date.' 00:00:00'
			);
			$orders = \Bitrix\Sale\OrderTable::getList(array(
				'filter' => $filter_array,
				'select' => array( 'ID', 'DATE_INSERT', 'USER_ID' ),
				'order' => array('ID' => 'DESC')
			));
			while ($order = $orders->fetch()){
				$list[] = $order;
			}
		$obCache->EndDataCache(array('list' => $list));
		}
		return $list;
	}






	// Самые продаваемые иллюстраторы за пред. неделю
	static function weekList(){
		$places = array();
		// Заказы предыдущей недели
		$orders = static::prevWeekOrders();
		foreach ($orders as $order){
			$user = tools\user::info($order['USER_ID']);
			if( $user['UF_USER_TYPE'] == 2 ){
				$places[$user['ID']]++;
			}
		}
		arsort($places);
		$illustrators = array_keys($places);
		$illustrators = array_slice($illustrators, 0, 10);
		return $illustrators;
	}




	// Самые продаваемые иллюстраторы за последние 30 дней
	static function monthList(){
		$places = array();
		// Заказы предыдущей недели
		$orders = static::monthOrders();
		foreach ($orders as $order){
			$user = tools\user::info($order['USER_ID']);
			if( $user['UF_USER_TYPE'] == 2 ){
				$places[$user['ID']]++;
			}
		}
		arsort($places);
		$illustrators = array_keys($places);
		array_splice($illustrators, 10);
		return $illustrators;
	}













	// Список всех принтов иллюстратора $user_id
	static function getPrints($user_id){
	    \Bitrix\Main\Loader::includeModule('iblock');
		$prints = array();
		if( intval($user_id) > 0 ){
			// Кешируем
			$obCache = new \CPHPCache();
			$cache_time = 24*60*60;
			$cache_id = 'illustratorPrints_'.$user_id;
			$cache_path = '/illustratorPrints/'.$user_id.'/';
			if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
				$vars = $obCache->GetVars();   extract($vars);
			} elseif($obCache->StartDataCache()){
				$dbElements = \CIBlockElement::GetList(
					Array("SORT"=>"ASC"),
					Array(
						"IBLOCK_ID" => project\site::PRINTS_IBLOCK_ID,
						"ACTIVE" => "Y",
						"PROPERTY_USER_ID" => $user_id
					),
					false, false,
					Array("ID", "NAME", "PROPERTY_USER_ID", "PROPERTY_PHOTO_ID")
				);
				while ($print = $dbElements->GetNext()){
					$prints[] = $print;
				}
			$obCache->EndDataCache(array('prints' => $prints));
			}
		}
		return $prints;
	}






	// Проверка наличия цвета у ил
	static function checkColor($color_id, $user_id){
	    \Bitrix\Main\Loader::includeModule('iblock');
		$check = false;
		// Кешируем
		$obCache = new \CPHPCache();
		$cache_time = 60*60;
		$cache_id = 'illustratorCheckColor_'.$user_id.'_'.$color_id;
		$cache_path = '/illustratorCheckColor/'.$user_id.'/'.$color_id.'/';
		if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
			$vars = $obCache->GetVars();   extract($vars);
		} elseif($obCache->StartDataCache()){
			$filter = Array(
				"IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
				"ACTIVE" => "Y",
				"PROPERTY_USER_ID" => $user_id,
				"PROPERTY_COLORS_AUTO" => $color_id,
			);
			$fields = Array( "PROPERTY_USER_ID", "PROPERTY_COLORS_AUTO" );
			$dbElements = \CIBlockElement::GetList(
				array("SORT"=>"ASC"), $filter, false, array("nTopCount"=>1), $fields
			);
			while ($element = $dbElements->GetNext()){  $check = true;  }
		$obCache->EndDataCache(array('check' => $check));
		}
		return $check;
	}




	// Проверка наличия коллекции у иллюстратора
	static function checkCollection($collection_id, $user_id){
	    \Bitrix\Main\Loader::includeModule('iblock');
		$check = false;
		// Кешируем
		$obCache = new \CPHPCache();
		$cache_time = 60*60;
		$cache_id = 'illustratorCheckCollection_'.$user_id.'_'.$collection_id;
		$cache_path = '/illustratorCheckCollection/'.$user_id.'/'.$collection_id.'/';
		if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
			$vars = $obCache->GetVars();   extract($vars);
		} elseif($obCache->StartDataCache()){
			$filter = Array(
				"IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
				"ACTIVE" => "Y",
				"PROPERTY_USER_ID" => $user_id,
				"PROPERTY_COLLECTION" => $collection_id,
			);
			$fields = Array( "PROPERTY_USER_ID", "PROPERTY_COLLECTION" );
			$dbElements = \CIBlockElement::GetList(
				array("SORT"=>"ASC"), $filter, false, array("nTopCount"=>1), $fields
			);
			while ($element = $dbElements->GetNext()){  $check = true;  }
		$obCache->EndDataCache(array('check' => $check));
		}
		return $check;
	}




}


