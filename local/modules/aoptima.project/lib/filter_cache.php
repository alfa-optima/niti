<? namespace AOptima\Project;
use AOptima\Project as project;



abstract class filter_cache {


    public $isIbProp = false;
    public $isGender = false;
    public $isProductTypes = false;




    // Создание кэшей
    static function creation(){

        BXClearCache(true, "/allCatalogCategories/");

        // Создание кэшей по категориям
        $categories_cache = new project\filter_cache_categories();
        $categories_cache->create();

        // Создание кэшей по полам
        $gender_cache = new project\filter_cache_gender();
        $gender_cache->create();

        // Создание кэшей по свойствам инфоблоков (Каталог)
        $ib_prop_cache = new project\filter_cache_ib_prop();
        $ib_prop_cache->create();

        // Создание кэшей по свойствам инфоблоков (ТП)
        $ib_prop_cache_tp = new project\filter_cache_ib_prop_tp();
        $ib_prop_cache_tp->create();

        // Создание кэшей по сортировкам
        $sorts_cache = new project\filter_cache_sorts();
        $sorts_cache->create();

        // Создание кэшей по типам товаров
        $product_types_cache = new project\filter_cache_product_types();
        $product_types_cache->create();

        // Создание кэшей по иллюстраторам
        $illustrators_types_cache = new project\filter_cache_illustrators();
        $illustrators_types_cache->create();

    }



    // файлы папки
    public function getCurrentFiles( $path ){
        $currentFiles = [];
        if( file_exists( $path ) ){
            $files = new \DirectoryIterator( $path );
            foreach( $files as $file ){
                if( $file->isFile() ){   $currentFiles[] = $file->GetFileName();   }
            }
        }
        return $currentFiles;
    }



    // Получение товаров из кэша
    public function getCacheProductsIDs( $fileName, $parentDir = false ){
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        $product_ids = [];
        $fileDir = $this->cache_path;
        if( strlen($parentDir) > 0 ){    $fileDir .= $parentDir.'/';    }
        $filePath = $fileDir.$fileName.'.json';
        if( file_exists( $filePath ) ){
            $json = file_get_contents( $filePath );
            if( strlen( $json ) > 0 ){
                $product_ids = \AOptima\Tools\funcs::json_to_array( $json );
            }
        }
        $product_ids = array_unique($product_ids);
        return $product_ids;
    }



}


