<? namespace AOptima\Project;
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;



class catalog_niti {
	
    const PRICE_ROUND = 2;

    const PRODUCT_COLLECTIONS_PROP_ID = 29;
    const PRODUCT_COLLECTIONS_NAMES_PROP_ID = 113;

    const CLOTHES_SECTION_CODE = 'clothes';
    const ACCESSOIRES_SECTION_CODE = 'accessories';



    static function clothesCatalogSectionID(){
        $all_catalog_categories = project\catalog_niti::all_catalog_categories();
        foreach ( $all_catalog_categories as $cat ){
            if( $cat['CODE'] == static::CLOTHES_SECTION_CODE ){
                return $cat['ID'];
            }
        }
        return false;
    }

    static function accessoriesCatalogSectionID(){
        $all_catalog_categories = project\catalog_niti::all_catalog_categories();
        foreach ( $all_catalog_categories as $cat ){
            if( $cat['CODE'] == static::ACCESSOIRES_SECTION_CODE ){
                return $cat['ID'];
            }
        }
        return false;
    }



    static function all_catalog_categories(){
        \Bitrix\Main\Loader::includeModule('iblock');
        $sections = array();
        $obCache = new \CPHPCache();
        $cache_time = 24*60*60;
        $cache_id = 'all_catalog_categories';
        $cache_path = '/all_catalog_categories/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $dbSections = \CIBlockSection::GetList(
                Array("SORT"=>"ASC"),
                Array(
                    "ACTIVE"=>"Y",
                    "IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID
                ), false,
                [ 'UF_GENDER_GROUP', 'UF_FABRIC_PAGE', 'UF_NAME' ]
            );
            while ($section = $dbSections->GetNext()){
                $sections[$section['ID']] = [
                    'ID' => $section['ID'],
                    'CODE' => $section['CODE'],
                    'DEPTH_LEVEL' => $section['DEPTH_LEVEL'],
                    'GENDER_TYPE' => $section['UF_GENDER_GROUP'],
                    'SECTION_ID' => $section['IBLOCK_SECTION_ID'],
                    'FABRIC_PAGE_ID' => $section['UF_FABRIC_PAGE'],
                    'URL' => $section['SECTION_PAGE_URL'],
                    'NAME' => $section['NAME'],
                    'SITE_NAME' => $section['UF_NAME'],
                ];
            }
            $obCache->EndDataCache( array('sections' => $sections) );
        }
        return $sections;
    }



	// Разделы каталога 1 уровня
	static function sections_1_level(){
	    
	    \Bitrix\Main\Loader::includeModule('iblock');
	    
		$sections = array();
		$obCache = new \CPHPCache();
		$cache_time = 600;
		$cache_id = 'catalog_sections_1_level';
		$cache_path = '/catalog_sections_1_level/';
		if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
			$vars = $obCache->GetVars();   extract($vars);
		} elseif($obCache->StartDataCache()){
			$dbSections = \CIBlockSection::GetList(
				Array("SORT"=>"ASC"),
				Array(
					"ACTIVE"=>"Y",
					"DEPTH_LEVEL" => 1,
					"IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID
				), false
			);
			while ($section = $dbSections->GetNext()){
				$sections[$section['CODE']] = $section;
			}
		$obCache->EndDataCache( array('sections' => $sections) );
		}
		return $sections;
	}


		
	// Хиты на главной
	function getMainHits(){

        \Bitrix\Main\Loader::includeModule('iblock');
		
		$genderTypes = project\gender_type::list();
		
		$activeGenderSects = array();
		$dbSections = \CIBlockSection::GetList(
			Array("SORT"=>"ASC"),
			Array(
				"ACTIVE"=>"Y",
				"DEPTH_LEVEL" => 2,
				"IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
				"UF_GENDER_GROUP" => $_SESSION['gender_type']['ID']
			), false, array("UF_GENDER_GROUP")
		);
		while ($section = $dbSections->GetNext()){
			$activeGenderSects[] = $section['ID'];
		}
		sort($activeGenderSects);
		
		$allGenderSects = array();
		$dbSections = \CIBlockSection::GetList(
			Array("SORT"=>"ASC"),
			Array(
				"ACTIVE"=>"Y",
				"DEPTH_LEVEL" => 2,
				"IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
				"UF_GENDER_GROUP" => $genderTypes['all']['ID']
			), false, array("UF_GENDER_GROUP")
		);
		while ($section = $dbSections->GetNext()){
			$allGenderSects[] = $section['ID'];
		}
		sort($allGenderSects);
		
		$filter_sects = array_merge($activeGenderSects, $allGenderSects);
		$filter_sects = array_unique($filter_sects);
		
		// Кешируем
		$obCache = new \CPHPCache();
		$cache_time = 3600;
		$cache_id = 'main_hits_'.implode('_', $filter_sects);
		$cache_path = '/main_hits/'.implode('_', $filter_sects).'/';
		if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
			$vars = $obCache->GetVars();   extract($vars);
		} elseif($obCache->StartDataCache()){
			$main_hits = array();
			$filter = Array(
				"IBLOCK_ID"=>project\site::CATALOG_IBLOCK_ID,
				"ACTIVE"=>"Y",
				"PROPERTY_HIT_VALUE" => 'Да',
				//"PROPERTY_HAS_SKU" => 1,
				"INCLUDE_SUBSECTIONS" => "Y",
			);
			if( count($activeGenderSects) > 0 ){
				$filter['SECTION_ID'] = $filter_sects;
			}
			$fields = Array("ID", "PROPERTY_HIT", "PROPERTY_HAS_SKU");
			$dbElements = \CIBlockElement::GetList(Array("SORT"=>"ASC"), $filter, false, false, $fields);
			while ($element = $dbElements->GetNext()){
				$main_hits[] = $element['ID'];
			}
		$obCache->EndDataCache(array('main_hits' => $main_hits));
		}
		return $main_hits;
	}



    // Цепочка разделов в строковом формате
    static function getCategoryChainStr( $id ){
        $chainStr = [];
        $chain = tools\section::chain($id);
        foreach ( $chain as $sect ){  $chainStr[] = $sect['NAME'];  }
        $chainStr = html_entity_decode(implode(' -> ', $chainStr));
        $chainStr = str_replace(["\n", "\r"], "", $chainStr);
        return $chainStr;
    }



	// Получение настроек категории каталога
	static function getCategorySettings( $id ){
        $settings = false;
        if( intval($id) > 0 ){
            $section = tools\section::info($id);
            if( intval($section['ID']) > 0 ){
                $settings = [];
                if( strlen($section['UF_SETTINGS']) > 0 ){
                    $settings = tools\funcs::json_to_array($section['UF_SETTINGS']);
                }
            }
        }
        return $settings;
    }



    // Сохранение цены
	static function saveCategoryPrice( $id, float $price ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $ids = [];
        if( is_array($id) ){
            $ids = $id;
        } else if( intval($id) > 0 ){
            $ids = [ $id ];
        }
	    if( count($ids) > 0 ){
            $price = number_format($price, 2, ".", "");
            foreach ( $ids as $sect_id ){
                $settings = static::getCategorySettings($sect_id);
                $settings['price'] = $price;
                $sectionOb = new \CIBlockSection;
                $fields = [ "UF_SETTINGS" => json_encode($settings) ];
                $res = $sectionOb->Update($sect_id, $fields);
                if( !$res ){
                    // Ошибку в лог
                    $error_text = '[Каталог] Ошибка сохранения JSON настроек для категории - ' . $sectionOb->LAST_ERROR;
                    tools\logger::addError($error_text);
                    return false;
                }
            }
            return true;
        }
        return false;
    }


	
}