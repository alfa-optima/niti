<? namespace AOptima\Project;
use AOptima\Project as project;


class pers_photo {
	
	use project\files_trait;
	
	const MIN_WIDTH = 65;
	const MIN_HEIGHT = 65;
	const MAX_WIDTH = 270;
	const MAX_HEIGHT = 270;
	const MAX_FILE_SIZE = 5242880;
	
	const TEMP_UPLOAD_DIR = "persPhotoTMP";
	const TEMP_UPLOAD_PATH = "/upload/persPhotoTMP/";

	
	protected $FILES = false;
	protected $doc_root = false;
	protected $fileTypes = array('jpeg', 'png');
	protected $file_name = false;
	protected $new_file_name = false;
	protected $file_path = false;
	protected $width = false;
	protected $height = false;
	protected $file_moved = false;
	protected $file_extension = false;
	
	
	
	// конструктор объекта
	function __construct($files){
		$this->doc_root = $_SERVER["DOCUMENT_ROOT"];
		// Перемещение файла во временную папку
		if ( $this->moveFile($files) ){
			$this->file_moved = true;
		}	
	}
	
		

	protected function jsOnResponse($obj){
		echo '<script type="text/javascript">
		window.parent.onResponse("'.$obj.'");
		</script>';
	}

	
	
	// Загрузка фото
	public function upload(){
		$check_status = $this->checkFile();
		if ( $check_status == 'ok' ){
			// МАСШТАБИРУЕМ ФОТО
			$big_file_path = (static::TEMP_UPLOAD_PATH).($this->new_file_name);
			// Инфо о файле $big_file_path
			$arFile = \CFile::MakeFileArray(($this->doc_root).($big_file_path));
			// Сохраняем файл в битриксе
			$big_fid = \CFile::SaveFile($arFile, static::TEMP_UPLOAD_DIR);
			// отмасштабированное фото
			$file_src = rIMGG( $big_fid , 5, static::MAX_WIDTH,  static::MAX_HEIGHT);
			global $USER;
			$user_id = $USER->getID();
			$user = new \CUser;
			$fields = array('PERSONAL_PHOTO' => \CFile::MakeFileArray($file_src));
			$res = $user->Update($user_id, $fields);
			if( $res ){
				// Удаляем временные файлы
				unlink( $this->file_path );
				unlink( ($this->doc_root).($file_src) );
				\CFile::Delete($big_fid);
				// Ответ
				$this->jsOnResponse("{'status':'ok', 'fname':'" . $this->file_name . "'}");
			} else {
				// Ответ
				$this->jsOnResponse("{'status':'Ошибка сохранения профиля'}");
			}
		} else {
			// Ответ
			$this->jsOnResponse("{'status':'".$check_status."'}");
		}
	}

	
	
	
	
	
	
}