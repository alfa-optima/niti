<? namespace AOptima\Project;
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;



class user {
	
	static function OnAfterUserHandler($arFields){
		BXClearCache(true, "/user_info/".$arFields['ID']."/");
		BXClearCache(true, "/illustrators/");
		BXClearCache(true, "/illustratorPrints/".$arFields['ID']."/");
	}
	static function OnBeforeUserDeleteHandler($user_id){
	    \Bitrix\Main\Loader::includeModule('iblock');
		BXClearCache(true, "/user_info/".$user_id."/");
		BXClearCache(true, "/illustrators/");
		BXClearCache(true, "/illustratorPrints/".$user_id."/");
		if( count(project\illustrator::getPrints($user_id)) > 0 ){
			global $APPLICATION;
			$APPLICATION->throwException('У данного пользователя есть принты');
			return false;
		} else {
			$filter = Array(
				"IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
				"PROPERTY_USER_ID" => $user_id,
			);
			$fields = Array( "PROPERTY_USER_ID" );
			$dbElements = \CIBlockElement::GetList(
				array("SORT"=>"ASC"), $filter, false, array("nTopCount"=>1), $fields
			);
			if ($element = $dbElements->GetNext()){
				global $APPLICATION;
				$APPLICATION->throwException('В каталоге есть товары, привязанные к данному пользователю');
				return false;
			}
		}
	}
	

	
	
	
}

