<? namespace AOptima\Project;
use AOptima\Project as project;
use AOptima\Tools as tools;

class gender_type {



    static function getActive(){
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        $arURI = tools\funcs::arURI(tools\funcs::pureURL());
        $genderTypes = static::list();
        $genderUrlKey = trim(strip_tags($arURI[1]));
        $genderUrlKey = str_replace('_home', '', $genderUrlKey);
        if( strlen($genderUrlKey) > 0 && $genderTypes[$genderUrlKey] ){
            $_SESSION['gender_type'] = $genderTypes[$genderUrlKey];
        }
        if( !$_SESSION['gender_type'] ){
            $_SESSION['gender_type'] = $genderTypes['woman'];
        }
        return $_SESSION['gender_type'];
    }



    static function getByID( $genderTypeID ){
        $genderType = false;
        // Кеширование
        $obCache = new \CPHPCache();
        $cache_time = 30*60;
        $cache_id = 'genderTypeByID_'.$genderTypeID;
        $cache_path = '/genderTypeByID/'.$genderTypeID.'/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
        	$vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            \Bitrix\Main\Loader::includeModule('iblock');
            $filter = [
                "IBLOCK_ID" => project\site::GENDERS_IBLOCK_ID,
                "ACTIVE" => "Y",
                "ID" => $genderTypeID
            ];
            $fields = [ "ID", "NAME", "CODE", 'PROPERTY_SHOW_IN_TOP_MENU' ];
            $types = \CIBlockElement::GetList(
                [ "SORT" => "ASC" ], $filter, false, false, $fields
            );
            while ($type = $types->GetNext()){
                if( $type['CODE'] == 'woman' ){
                    $type['ALT_VALUE'] = 'Для неё';
                } else if( $type['CODE'] == 'man' ){
                    $type['ALT_VALUE'] = 'Для него';
                } else if( $type['CODE'] == 'kids' ){
                    $type['ALT_VALUE'] = 'Детям';
                } else if( $type['CODE'] == 'all' ){
                    $type['ALT_VALUE'] = 'Для всех';
                } else {
                    $type['ALT_VALUE'] = $type['NAME'];
                }
                $genderType = $type;
            }
        $obCache->EndDataCache(array('genderType' => $genderType));
        }
        return $genderType;
    }



    static function list(){
        $arTypes = array();
        $obCache = new \CPHPCache();
        $cache_time = 600;
        $cache_id = 'genderTypes';
        $cache_path = '/genderTypes/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            \Bitrix\Main\Loader::includeModule('iblock');
            $filter = [
            	"IBLOCK_ID" => project\site::GENDERS_IBLOCK_ID,
            	"ACTIVE" => "Y"
            ];
            $fields = [ "ID", "NAME", "CODE", 'PROPERTY_SHOW_IN_TOP_MENU' ];
            $types = \CIBlockElement::GetList(
            	[ "SORT" => "ASC" ], $filter, false, false, $fields
            );
            while ($type = $types->GetNext()){
                if( $type['CODE'] == 'woman' ){
                    $type['ALT_VALUE'] = 'Для неё';
                } else if( $type['CODE'] == 'man' ){
                    $type['ALT_VALUE'] = 'Для него';
                } else if( $type['CODE'] == 'kids' ){
                    $type['ALT_VALUE'] = 'Детям';
                } else if( $type['CODE'] == 'all' ){
                    $type['ALT_VALUE'] = 'Для всех';
                } else {
                    $type['ALT_VALUE'] = $type['NAME'];
                }
                $arTypes[$type['CODE']] = $type;
            }
            $obCache->EndDataCache( array('arTypes' => $arTypes) );
        }
        return $arTypes;
    }



}