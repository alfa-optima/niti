<? namespace AOptima\Project;
use AOptima\Project as project;
use AOptima\Tools as tools;


class tag {


    const IBLOCK_ID = 26;



    static function add ( $tag ){

        \Bitrix\Main\Loader::includeModule('iblock');
        \Bitrix\Main\Loader::includeModule('aoptima.tools');

        $tags_ids = [];
        if( is_array($tag) ){
            $tags = $tag;
        } else if( strlen($tag) > 0 ){
            $tags = [ $tag ];
        }
        // Приведём теги к нижнему регистру, удалим пробелы по бокам + отсеем повторы
        if( is_array($tags) ){
            foreach ( $tags as $key => $tag ){
                $tag = trim(strtolower($tag));
                if( strlen($tag) > 0 ){} else {
                    unset($tags[$key]);
                }
            }
            $tags = array_unique($tags);
        }
        if( is_array($tags) && count($tags) > 0 ){

            echo "<pre>"; print_r( $tags ); echo "</pre>";

            foreach ( $tags as $key => $tag ){
                $filter = [
                	"IBLOCK_ID" => static::IBLOCK_ID,
                	"=NAME" => $tag
                ];
                $fields = [ "ID", "NAME" ];
                $dbElements = \CIBlockElement::GetList(
                	["SORT" => "ASC"], $filter, false, ["nTopCount" => 1], $fields
                );
                if ( $arItem = $dbElements->GetNext() ){
                    $tags_ids[] = $arItem['ID'];
                } else {
                    $element = new \CIBlockElement;
                    $fields = [
                        "IBLOCK_ID" => static::IBLOCK_ID,
                        "NAME" => $tag,
                        "CODE" => \AOptima\Tools\el::getUniqueCode( $tag, static::IBLOCK_ID ),
                        "ACTIVE" => "Y"
                    ];
                    $id = $element->Add( $fields );
                    if( intval($id) > 0 ){
                        $tags_ids[] = $id;
                    }
                }
            }
        }
        return $tags_ids;
    }



}