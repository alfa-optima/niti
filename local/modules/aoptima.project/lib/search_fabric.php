<? namespace AOptima\Project;
use AOptima\Project as project;



class search_fabric {


    public $propCorellation = [];

    
    function __construct() {

        $filter_cache_gender = new project\filter_cache_gender();
        $filter_cache_product_types = new project\filter_cache_product_types();
        $filter_cache_ib_prop = new project\filter_cache_ib_prop();
        $filter_cache_ib_prop_tp = new project\filter_cache_ib_prop_tp();

$this->propCorellation['CAT'] = project\search_category::$searchParamCode;
$this->propCorellation['GROUPS'] = $filter_cache_gender->searchParamCode;
$this->propCorellation['PRODUCT_TYPES'] = $filter_cache_product_types->searchParamCode;
$this->propCorellation['COLLECTIONS'] = $filter_cache_ib_prop->propParams['COLLECTION']['searchParamCode'];
$this->propCorellation['TAGS'] = $filter_cache_ib_prop->propParams['TAGS']['searchParamCode'];
$this->propCorellation['COLORS'] = $filter_cache_ib_prop_tp->propParams['COLOR']['searchParamCode'];
$this->propCorellation['SIZES'] = $filter_cache_ib_prop_tp->propParams['SIZE']['searchParamCode'];
    
    }


    static function setGetParams( $fabricPage ){
        if( intval($fabricPage['ID']) > 0 ){

            $filter_cache_gender = new project\filter_cache_gender();
            $filter_cache_illustrators = new project\filter_cache_illustrators();
            $filter_cache_product_types = new project\filter_cache_product_types();
            $filter_cache_ib_prop = new project\filter_cache_ib_prop();
            $filter_cache_ib_prop_tp = new project\filter_cache_ib_prop_tp();
            $filter_cache_ib_prop_tp = new project\filter_cache_ib_prop_tp();

            // GROUPS
            if(
                is_array($fabricPage['PROPERTY_GROUPS_VALUE'])
                &&
                count($fabricPage['PROPERTY_GROUPS_VALUE']) > 0
            ){
                $codes = \AOptima\Tools\el::getCodesByIDs( $fabricPage['PROPERTY_GROUPS_VALUE'] );
                if( count($codes) > 0 ){
                    $_GET[ $filter_cache_gender->searchParamCode ] = implode(',', $codes);
                }
            }
            // CATS
            if( intval($fabricPage['PROPERTY_CAT_VALUE']) ){
                $cat = \AOptima\Tools\el::info( $fabricPage['PROPERTY_CAT_VALUE'] );
                if( intval($cat['ID']) > 0 ){
                    $_GET[ project\search_category::$searchParamCode ] = $cat['CODE'];
                }
            }
            // COLORS
            if(
                is_array($fabricPage['PROPERTY_COLORS_VALUE'])
                &&
                count($fabricPage['PROPERTY_COLORS_VALUE']) > 0
            ){
                $codes = \AOptima\Tools\el::getCodesByIDs( $fabricPage['PROPERTY_COLORS_VALUE'] );
                if( count($codes) > 0 ){
                    $_GET[ $filter_cache_ib_prop_tp->propParams['COLOR']['searchParamCode'] ] = implode(',', $codes);
                }
            }
            // SIZES
            if(
                is_array($fabricPage['PROPERTY_SIZES_VALUE'])
                &&
                count($fabricPage['PROPERTY_SIZES_VALUE']) > 0
            ){
                $codes = \AOptima\Tools\el::getCodesByIDs( $fabricPage['PROPERTY_SIZES_VALUE'] );
                if( count($codes) > 0 ){
                    $_GET[ $filter_cache_ib_prop_tp->propParams['SIZE']['searchParamCode'] ] = implode(',', $codes);
                }
            }
            // COLLECTIONS
            if(
                is_array($fabricPage['PROPERTY_COLLECTIONS_VALUE'])
                &&
                count($fabricPage['PROPERTY_COLLECTIONS_VALUE']) > 0
            ){
                $codes = \AOptima\Tools\el::getCodesByIDs( $fabricPage['PROPERTY_COLLECTIONS_VALUE'] );
                if( count($codes) > 0 ){
                    $_GET[ $filter_cache_ib_prop->propParams['COLLECTION']['searchParamCode'] ] = implode(',', $codes);
                }
            }
            // TAGS
            if(
                is_array($fabricPage['PROPERTY_TAGS_VALUE'])
                &&
                count($fabricPage['PROPERTY_TAGS_VALUE']) > 0
            ){
                $codes = \AOptima\Tools\el::getCodesByIDs( $fabricPage['PROPERTY_TAGS_VALUE'] );
                if( count($codes) > 0 ){
                    $_GET[ $filter_cache_ib_prop->propParams['TAGS']['searchParamCode'] ] = implode(',', $codes);
                }
            }
            // PRODUCT_TYPES
            if(
                is_array($fabricPage['PROPERTY_PRODUCT_TYPES_VALUE'])
                &&
                count($fabricPage['PROPERTY_PRODUCT_TYPES_VALUE']) > 0
            ){
                $codes = \AOptima\Tools\el::getCodesByIDs( $fabricPage['PROPERTY_PRODUCT_TYPES_VALUE'] );
                if( count($codes) > 0 ){
                    $_GET[ $filter_cache_product_types->searchParamCode ] = implode(',', $codes);
                }
            }
        }
    }




    public function getSearchParams( $fabric_id ){
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        $fabricPage = \Aoptima\Tools\el::info($fabric_id);
        if( intval($fabricPage['ID']) > 0 ){

            $params = [];
            
            // Предполагаем, что все свойства - типа E (привязка к элементам инфоблоков)
            // Перебираем коды свойств
            foreach ( $this->propCorellation as $prop_code => $paramName ){
                $prop_value = $fabricPage['PROPERTY_'.$prop_code.'_VALUE'];
                $values = [];
                if( is_array($prop_value) ){
                    foreach ( $prop_value as $key => $el_id ){
                        $el = \Aoptima\Tools\el::info($el_id);
                        if( intval($el['ID']) > 0 ){
                            if( isset($el['CODE']) && strlen($el['CODE']) > 0 ){
                                $values[] = $el['CODE'];
                            } else {
                                $values[] = $el['ID'];
                            }
                        }
                    }
                } else if( intval($prop_value) > 0 ){
                    $el = \Aoptima\Tools\el::info($prop_value);
                    if( intval($el['ID']) > 0 ){
                        if( isset($el['CODE']) && strlen($el['CODE']) > 0 ){
                            $values[] = $el['CODE'];
                        } else {
                            $values[] = $el['ID'];
                        }
                    }
                }
                if( count( $values ) > 0 ){
                    $params[ $paramName ] = $values;
                }
            }
            return $params;
        }
        return false;
    }



    static function getAllList(){
        \Bitrix\Main\Loader::includeModule('iblock');
        $list = [];
        $obCache = new \CPHPCache();
        $cache_time = 24*60*60;
        $cache_id = 'all_fabric_pages';
        $cache_path = '/all_fabric_pages/';
        if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
            $vars = $obCache->GetVars();   extract($vars);
        } elseif($obCache->StartDataCache()){
            $filter = [
            	"IBLOCK_ID" => project\site::SEARCH_FABRIC_IBLOCK_ID,
            	"ACTIVE" => "Y",
            ];
            $fields = [ "ID", "NAME", "CODE" ];
            $dbElements = \CIBlockElement::GetList(
            	[ "SORT" => "ASC" ], $filter, false, false, $fields
            );
            while ($element = $dbElements->GetNext()){
                $list[$element['ID']] = $element;
            }
            $obCache->EndDataCache( array('list' => $list) );
        }
        return $list;
    }




}