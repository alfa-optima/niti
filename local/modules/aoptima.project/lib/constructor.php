<? namespace AOptima\Project;

use AOptima\Project as project;
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.tools');

class constructor
{

    const UPDATE_FILE_PATH = '/constructor_api/tmp/';
    const UPDATE_FILE_NAME = 'file';
    const TEMP_PATH = '/upload/constructor_tmp/';
    const FILES_DIR_NAME = 'constructor_files';
    const IMAGES_PATH = '/upload/constructor_images/';

    static $imageTypes = array('jpeg' => 'image/jpeg', 'png' => 'image/png');


    static function checkUser()
    {
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        global $USER;
        if ($USER->IsAuthorized()) {
            $user = tools\user::info($USER->GetID());
            if ($user['UF_USER_TYPE'] == 2) {
                return $user['ID'];
            }
        }
        //// TODO СДЕЛАТЬ В ИТОГЕ - false ////
        //return false;
        return 211;
    }


    static function isUserNote($user_id = false, $note_id = false)
    {
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        if (
            intval($user_id) > 0
            &&
            intval($note_id) > 0
        ) {
            \Bitrix\Main\Loader::includeModule('iblock');
            $filter = array(
                "IBLOCK_ID" => project\site::CONSTRUCTOR_NOTES,
                "ID" => $note_id,
                "PROPERTY_USER" => $user_id
            );
            $fields = array("ID", "PROPERTY_USER");
            $notes = \CIBlockElement::GetList(
                array("NAME" => "ASC"), $filter, false, false, $fields
            );
            if ($note = $notes->GetNext()) {
                return true;
            }
        }
        return false;
    }


    // Создание заявки
    static function addNote($params = false)
    {
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        \Bitrix\Main\Loader::includeModule('iblock');
        $element = new \CIBlockElement;
        $user_id = static::checkUser();
        if (intval($user_id) > 0) {
            $PROP[93] = $user_id;
            $fields = array(
                "IBLOCK_ID" => project\site::CONSTRUCTOR_NOTES,
                "PROPERTY_VALUES" => $PROP,
                "NAME" => 'Заявка',
                "ACTIVE" => "Y",
                "DETAIL_TEXT" => strip_tags($params)
            );
            if ($el_id = $element->Add($fields)) {
                return $el_id;
            } else {
                tools\logger::addError($element->LAST_ERROR);
            }
        }
        return false;
    }


    // Редактирование заявки
    static function updateNote($id = false, $params = false)
    {
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        $user_id = static::checkUser();
        if (
            intval($id) > 0
            &&
            intval($user_id) > 0
            &&
            static::isUserNote($user_id, $id)
        ) {
            \Bitrix\Main\Loader::includeModule('iblock');
            $element = new \CIBlockElement;
            $fields = array(
                "DETAIL_TEXT" => strip_tags($params)
            );
            if ($res = $element->Update($id, $fields)) {
                return true;
            } else {
                tools\logger::addError($element->LAST_ERROR);
            }
        }
        return false;
    }


    // Инфо по заявке
    static function getNote($id = false)
    {
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        $user_id = static::checkUser();
        if (
            intval($id) > 0
            &&
            intval($user_id) > 0
        ) {
            \Bitrix\Main\Loader::includeModule('iblock');
            $filter = array(
                "IBLOCK_ID" => project\site::CONSTRUCTOR_NOTES,
                "ID" => $id,
                //"PROPERTY_USER" => $user_id
            );
            $fields = array("ID", "DATE_CREATE", "ACTIVE", "TIMESTAMP_X", "PROPERTY_USER", "DETAIL_TEXT");
            $notes = \CIBlockElement::GetList(
                array("NAME" => "ASC"), $filter, false, array("nTopCount" => 1), $fields
            );
            if ($note = $notes->GetNext()) {

                //if($note['~DETAIL_TEXT'])

                return array(
                    'ID' => $note['ID'],
                    'ACTIVE' => $note['ACTIVE'],
                    'DATE_CREATE' => $note['DATE_CREATE'],
                    'DATE_UPDATE' => $note['TIMESTAMP_X'],
                    'PARAMS' => $note['~DETAIL_TEXT'],
                );
            }
        }
        return false;
    }


    static function getBasicInfo()
    {

        \Bitrix\Main\Loader::includeModule('iblock');

        // ЦВЕТА
        $result['COLORS'] = [];

        $filter = [
            "IBLOCK_ID" => project\site::COLOR_IBLOCK_ID,
            "ACTIVE" => "Y",
            "PROPERTY_IN_CONSTRUCTOR_VALUE" => 'Y'
        ];
        $fields = [
            "ID", "NAME", "PREVIEW_PICTURE",
            "PROPERTY_IN_CONSTRUCTOR", "PROPERTY_COLOR_CODE"
        ];
        $colors = \CIBlockElement::GetList(
            ["NAME" => "ASC"], $filter, false, false, $fields
        );
        while ($color = $colors->GetNext()) {
            $result['COLORS'][$color['ID']] = [
                'ID' => $color['ID'],
                'NAME' => $color['NAME'],
                'HEX_CODE' => $color['PROPERTY_COLOR_CODE_VALUE'],
                'PICTURE' => $color['PREVIEW_PICTURE'] ? \CFile::GetPath($color['PREVIEW_PICTURE']) : false,
            ];
        }

        // ГРУППЫ ТОВАРОВ
        $result['GROUPS'] = [
            'LIST' => [],
            'ITEMS' => []
        ];

        $groups = \CIBlockSection::GetList(
            ["SORT" => "ASC"],
            ["ACTIVE" => "Y", "IBLOCK_ID" => project\site::CONSTR_GROUPS_IBLOCK_ID]
        );

        while ($group = $groups->GetNext()) {

            $arGroup = [
                'ID' => $group['ID'],
                'SORT' => $group['SORT'],
                'NAME' => $group['NAME'],
                'ITEMS' => []
            ];

            // Элементы группы
            $filter = [
                "IBLOCK_ID" => project\site::CONSTR_GROUPS_IBLOCK_ID,
                "ACTIVE" => "Y",
                "SECTION_ID" => $group['ID']
            ];
            $fields = [
                "ID", "NAME", "PREVIEW_PICTURE", "DETAIL_PICTURE",
                "PROPERTY_X", "PROPERTY_Y", "PROPERTY_W", "PROPERTY_H",
                "PROPERTY_NEED_FINAL_PICTURE"
            ];
            $elements = \CIBlockElement::GetList(
                ["SORT" => "ASC"], $filter, false, false, $fields
            );
            while ($el = $elements->GetNext()) {

                $arElement = array(
                    'ID' => $el['ID'],
                    'NAME' => $el['NAME'],
                    'NEED_FINAL_PICTURE' => $el['PROPERTY_NEED_FINAL_PICTURE_VALUE'] ? 'Y' : 'N',
                    'BASE_PICTURE' => $el['PREVIEW_PICTURE'] ? \CFile::GetPath($el['PREVIEW_PICTURE']) : false,
                    'ICON_PICTURE' => $el['DETAIL_PICTURE'] ? \CFile::GetPath($el['DETAIL_PICTURE']) : false,
                    'X' => $el['PROPERTY_X_VALUE'],
                    'Y' => $el['PROPERTY_Y_VALUE'],
                    'W' => $el['PROPERTY_W_VALUE'],
                    'H' => $el['PROPERTY_H_VALUE'],
                    'COLOR_ITEMS' => array()
                );

                // SKU элемента
                $skuFilter = array(
                    "IBLOCK_ID" => project\site::CONSTR_SKU_IBLOCK_ID,
                    "ACTIVE" => "Y",
                    "PROPERTY_CML2_LINK" => $el['ID']
                );
                $skuFields = array(
                    "ID", "NAME", "PREVIEW_PICTURE",
                    "PROPERTY_CML2_LINK", "PROPERTY_COLOR_ID"
                );
                $skus = \CIBlockElement::GetList(
                    ["SORT" => "ASC"], $skuFilter, false, false, $skuFields
                );
                while ($sku = $skus->GetNext()) {

                    $color = tools\el::info($sku['PROPERTY_COLOR_ID_VALUE']);

                    //$arElement['COLOR_ITEMS'][$sku['ID']] = [
                    $arElement['COLOR_ITEMS'][] = [
                        'id' => intval($sku['ID']),
                        'name' => $sku['NAME'],
                        'picture' => $sku['PREVIEW_PICTURE'] ? \CFile::GetPath($sku['PREVIEW_PICTURE']) : false,
                        'color_id' => $color['ID']
                    ];
                }

                $arGroup['ITEMS'][] = $el['ID'];
                $result['GROUPS']['ITEMS'][$el['ID']] = $arElement;
            }

            $result['GROUPS']['LIST'][$group['ID']] = $arGroup;
        }

        // КОЛЛЕКЦИИ
        $result['COLLECTIONS'] = [];
        foreach (project\collection::getList() as $collection) {
            $result['COLLECTIONS'][$collection['ID']] = [
                'ID' => $collection['ID'],
                'NAME' => $collection['NAME'],
                'CODE' => $collection['CODE'],
                'PARENT_ID' => $collection['PROPERTY_PARENT_ID_VALUE'],
                'PICTURE' => $collection['DETAIL_PICTURE'] ? \CFile::GetPath($collection['DETAIL_PICTURE']) : false
            ];
        }

        return $result;

    }


    // Список заявок
    static function getNotesList(
        $user_id = false,
        $page = false,
        $limit = 1000000000,
        $page_el_cnt = 10
    )
    {
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        if (!$user_id) {
            $user_id = static::checkUser();
        }

        $limit = intval($limit) > 0 ? $limit : 1000000000;
        $page_el_cnt = intval($page_el_cnt) > 0 ? $page_el_cnt : 10;
        $page = intval($page) > 0 ? $page : 1;

        if (intval($user_id) > 0) {
            $all_list = array();
            \Bitrix\Main\Loader::includeModule('iblock');
            $filter = array(
                "IBLOCK_ID" => project\site::CONSTRUCTOR_NOTES,
                "ACTIVE" => "Y",
                "PROPERTY_USER" => $user_id
            );
            $fields = array("ID", "DATE_CREATE", 'ACTIVE', "TIMESTAMP_X", "PROPERTY_USER", "DETAIL_TEXT");

            $dop_params = array();
            $dop_params['nTopCount'] = $limit;

            $notes = \CIBlockElement::GetList(
                array("ID" => "DESC"),
                $filter, false,
                $dop_params,
                $fields
            );
            //$notes->NavStart($limit);
            while ($note = $notes->GetNext()) {
                $all_list[] = array(
                    'ID' => $note['ID'],
                    'ACTIVE' => $note['ACTIVE'],
                    'DATE_CREATE' => $note['DATE_CREATE'],
                    'DATE_UPDATE' => $note['TIMESTAMP_X'],
                    'PARAMS' => $note['~DETAIL_TEXT'],
                );
            }


            $listRes['list'] = array_slice($all_list, ($page_el_cnt * ($page - 1)), $page_el_cnt);
            $listRes['pages'] = ceil(count($all_list) / $page_el_cnt);
            $listRes['all_list'] = $all_list;

            //echo "<pre>"; print_r($all_list); echo "</pre>";

            return $listRes;
        }
        return false;
    }


    // Удаление заявки
    static function deleteNote($id = false)
    {
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        $user_id = static::checkUser();
        if (
            intval($id) > 0
            &&
            intval($user_id) > 0
            &&
            static::isUserNote($user_id, $id)
        ) {
            \Bitrix\Main\Loader::includeModule('iblock');
            $res = \CIBlockElement::Delete($id);
            return $res;
        }
        return false;
    }


    // Инфо по файлу по ID
    static function getFile($id)
    {
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        if (intval($id) > 0) {
            $arFile = \CFile::GetFileArray($id);
            if (intval($arFile['ID']) > 0) {
                return array(
                    'file_id' => $arFile['ID'],
                    'src' => $arFile['SRC'],
                    'file_size' => $arFile['FILE_SIZE']
                );
            }
        }
        return false;
    }


    // base64 -> в файл системы
    static function base64_to_file($base64)
    {
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        $result = array('status' => 'error');
        if (strlen($base64) > 0) {
            $ar1 = explode(';', $base64);
            $ar2 = explode(':', $ar1[0]);
            $fileType = trim($ar2[1]);
            if (!in_array($fileType, static::$imageTypes)) {
                $result['text'] = 'Допустимые форматы файла - ' . implode(', ', array_keys(static::$imageTypes));
            } else {

                $dir = $_SERVER['DOCUMENT_ROOT'] . static::TEMP_PATH;
                if (!file_exists($images_path)) {
                    mkdir($dir, 0700);
                }

                $ex = array_search($fileType, static::$imageTypes);
                $file_name = md5($base64) . '.' . $ex;
                $file_path = $dir . $file_name;

                $file = fopen($file_path, "wb");
                if (substr_count($base64, ',')) {
                    $base64 = trim(explode(',', $base64)[1]);
                }
                fwrite($file, base64_decode($base64));
                fclose($file);

                if (file_exists($file_path)) {

                    $arFile = \CFile::MakeFileArray($file_path);
                    $file_id = \CFile::SaveFile($arFile, static::FILES_DIR_NAME);

                    if (intval($file_id) > 0) {

                        unlink($file_path);

                        $arFile = \CFile::GetFileArray($file_id);
                        $result = array(
                            'status' => 'ok',
                            'file_id' => $arFile['ID'],
                            'src' => $arFile['SRC'],
                            'file_size' => $arFile['FILE_SIZE']
                        );

                    } else {
                        $result['text'] = 'Ошибка создания файла';
                    }
                } else {
                    $result['text'] = 'Ошибка записи файла';
                }
            }
        }
        return $result;
    }


    // Сохранение файла
    static function save_file($file)
    {
        \Bitrix\Main\Loader::includeModule('aoptima.tools');

        $result = array('status' => 'error');

        $fileType = $file['type'];
        $file_path = $file['tmp_name'];

        if (!in_array($fileType, static::$imageTypes)) {
            $result['text'] = 'Допустимые форматы файла - ' . implode(', ', array_keys(static::$imageTypes));
        } else {

            if (file_exists($file_path)) {

                $arFile = \CFile::MakeFileArray($file_path);
                $file_id = \CFile::SaveFile($arFile, static::FILES_DIR_NAME);

                if (intval($file_id) > 0) {

                    unlink($file_path);

                    $arFile = \CFile::GetFileArray($file_id);
                    $result = array(
                        'status' => 'ok',
                        'file_id' => $arFile['ID'],
                        'src' => $arFile['SRC'],
                        'file_size' => $arFile['FILE_SIZE']
                    );

                } else {
                    $result['text'] = 'Ошибка создания файла';
                }
            } else {
                $result['text'] = 'Ошибка записи файла';
            }
        }

        return $result;
    }


    // Согласованные заявки для создание товаров
    static function getAcceptedNotes()
    {
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        \Bitrix\Main\Loader::includeModule('iblock');

        $list = [];
        $dbElements = \CIBlockElement::GetList(
            array("ID" => "ASC"),
            array(
                "IBLOCK_ID" => project\site::CONSTRUCTOR_NOTES,
                //"ACTIVE" => "Y",
                // согласованные
                "PROPERTY_ACCEPTED" => 1,
                // не завершённые
                "!PROPERTY_FINISHED" => 1,
            ),
            false, false, array("ID", "PROPERTY_ACCEPTED", "PROPERTY_FINISHED"));
        while ($element = $dbElements->GetNext()) {
            $list[] = $element['ID'];
        }
        return $list;
    }

    // Завершённые заявки
    static function getFinishedNotes()
    {
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        \Bitrix\Main\Loader::includeModule('iblock');
        $list = [];
        $dbElements = \CIBlockElement::GetList(
            array("ID" => "ASC"),
            array(
                "IBLOCK_ID" => project\site::CONSTRUCTOR_NOTES,
                "PROPERTY_FINISHED" => 1,
            ),
            false, false, [
                "ID", "NAME", "PROPERTY_FINISHED", "PROPERTY_FINISH_DATE", "PROPERTY_USER"
            ]
        );
        while ($element = $dbElements->GetNext()) {

            $element['USER'] = tools\user::info($element['PROPERTY_USER_VALUE']);

            $list[] = $element;
        }
        return $list;
    }


    // Запуск всех согласованных заявок
    static function executeAllAcceptedNotes()
    {
        $list = static::getAcceptedNotes();
        foreach ($list as $zayavka_id) {
            static::executeNote($zayavka_id, true);
        }
    }



    // Получение фото для разных размеров (при необходимости)
    static function getSizesOffersPhotos(
        $categoryPictureID,
        $categoryElement,
        $print,
        $categorySizesParams,
        $sizes,
        $arJSON
    ){
        $sizesOffersPhotos = [];
        // если переданы настройки по размерам
        if (
            is_array($categorySizesParams)
            &&
            count($categorySizesParams) > 0
        ) {
            // Получим (временные) наложения принта для всех размеров товара
            if (count($sizes) > 0) {
                foreach ($sizes as $size_id) {
                    $sizesOffersPhotos[$size_id] = false;

                    // Если для данного размера передано готовое фото наложения
                    if (intval($categorySizesParams[$size_id]['picture_id']) > 0) {

                        // берём для данного размера это фото
                        $sizesOffersPhotos[$size_id] = $categorySizesParams[$size_id]['picture_id'];

                    } else {

                        // Если для категории передано готовое фото
                        if (intval($categoryPictureID) > 0) {

                            $sizesOffersPhotos[$size_id] = $categoryPictureID;

                        // Если для категории НЕ передано готовое фото
                        } else {

                            // определяем id файла принта для наложения
                            $print_picture_id = $print['PROPERTY_PHOTO_ID_VALUE'];
                            // возможность передачи фото уникального ПРИНТА для данного цвета
                            if (intval($categorySizesParams[$size_id]['print_picture_id']) > 0) {
                                $print_picture_id = $categorySizesParams[$size_id]['print_picture_id'];
                            }
                            // id фото-фона для наложения (пока взят базовый фон)
                            // TODO решить что в итоге нужно в качестве фона
                            $background_picture_id = $categoryElement['PREVIEW_PICTURE'];
                            //
                            $arBackgroundPhoto = \CFile::GetFileArray($background_picture_id);
                            $arPrintPhoto = \CFile::GetFileArray($print_picture_id);
                            if (
                                is_array($arBackgroundPhoto)
                                &&
                                is_array($arPrintPhoto)
                            ) {

                                $new_pic_id = project\image_generation_offer::make([
                                    'product_photo_id' => $background_picture_id,
                                    'print_photo_id' => $print_picture_id,
                                    'coordinate' => [
                                        'x' => $categoryElement['PROPERTY_X_VALUE'],
                                        'y' => $categoryElement['PROPERTY_Y_VALUE'],
                                        'w' => $categoryElement['PROPERTY_W_VALUE'],
                                        'h' => $categoryElement['PROPERTY_H_VALUE'],
                                        'custom' => $arJSON['categoriesParams'][$categoryElement['ID']]['position']
                                    ]
                                ]);

                                $sizesOffersPhotos[$size_id] = $new_pic_id;
                            }

                        }
                    }
                }
            }
        }
        return [
            'sizesOffersPhotos' => $sizesOffersPhotos
        ];
    }



    static function getColorsOffersPhotos(
        $categoryPictureID,
        $categoryElement,
        $print,
        $categoryColorsParams,
        $arJSON
    ){
        \Bitrix\Main\Loader::includeModule('iblock');

        $colorOffersPhotos = [];
        $colors = [];

        // если переданы настройки по цветам
        if (
            is_array($categoryColorsParams)
            &&
            count($categoryColorsParams) > 0
        ) {

            $colors = static::getColors( $categoryElement['ID'], $categoryColorsParams );

            // Получим (временные) наложения принта для всех цветов товара
            // (при наличии цветов у товара)
            if (count($colors) > 0) {
                foreach ($colors as $color) {
                    $colorOffersPhotos[$color['id']] = false;

                    // Если для данного цвета передано готовое фото
                    if (intval($categoryColorsParams[$color['id']]['picture_id']) > 0) {

                        // берём для данного цвета это фото
                        $colorOffersPhotos[$color['id']] = $categoryColorsParams[$color['id']]['picture_id'];

                    } else {

                        // Если для категории передано готовое фото
                        if ( intval($categoryPictureID) > 0 ){

                            $colorOffersPhotos[$color['id']] = $categoryPictureID;

                        // Если для категории НЕ передано готовое фото
                        } else {

                            // определяем id файла принта для наложения
                            $print_picture_id = $print['PROPERTY_PHOTO_ID_VALUE'];
                            // возможность передачи фото уникального ПРИНТА для данного цвета
                            if (intval($categoryColorsParams[$color['id']]['print_picture_id']) > 0) {
                                $print_picture_id = $categoryColorsParams[$color['id']]['print_picture_id'];
                            }
                            // id фото-фона для наложения
                            $background_picture_id = $color['background_picture_id'];
                            //
                            $arBackgroundPhoto = \CFile::GetFileArray($background_picture_id);
                            $arPrintPhoto = \CFile::GetFileArray($print_picture_id);
                            if (
                                is_array($arBackgroundPhoto)
                                &&
                                is_array($arPrintPhoto)
                            ) {
                                $new_pic_id = project\image_generation_offer::make([
                                    'product_photo_id' => $background_picture_id,
                                    'print_photo_id' => $print_picture_id,
                                    'coordinate' => [
                                        'x' => $categoryElement['PROPERTY_X_VALUE'],
                                        'y' => $categoryElement['PROPERTY_Y_VALUE'],
                                        'w' => $categoryElement['PROPERTY_W_VALUE'],
                                        'h' => $categoryElement['PROPERTY_H_VALUE'],
                                        'custom' => $arJSON['categoriesParams'][$categoryElement['ID']]['position']
                                    ]
                                ]);

                                \AOptima\Tools\logger::addError('$background_picture_id - ' . $background_picture_id);

                                $colorOffersPhotos[$color['id']] = $new_pic_id;
                            }

                        }
                    }
                }
            }
        }
        return [
            'colorOffersPhotos' => $colorOffersPhotos,
            'colors' => $colors
        ];
    }



    static function getColors(
        $categoryElementID,
        $categoryColorsParams
    ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $colors = [];
        // если переданы настройки по цветам
        if (
            is_array($categoryColorsParams)
            &&
            count($categoryColorsParams) > 0
        ) {
            // запрос цветовых ТП одежды
            $dbElements = \CIBlockElement::GetList(
                ["SORT" => "ASC"],
                [
                    "IBLOCK_ID" => project\site::CONSTR_SKU_IBLOCK_ID,
                    "PROPERTY_COLOR_ID" => array_keys($categoryColorsParams),
                    "ACTIVE" => "Y",
                    "PROPERTY_CML2_LINK" => $categoryElementID
                ], false, false,
                ["ID", "NAME", "PREVIEW_PICTURE", "PROPERTY_COLOR_ID", "PROPERTY_CML2_LINK"]
            );
            while ($ell = $dbElements->GetNext()) {
                $colors[] = [
                    'id' => $ell['PROPERTY_COLOR_ID_VALUE'],
                    'name' => tools\el::info($ell['PROPERTY_COLOR_ID_VALUE'])['NAME'],
                    'background_picture_id' => $ell['PREVIEW_PICTURE'],
                ];
            }
        }
        return $colors;
    }




    // Создание товаров по заявке
    static function executeNote($zayavka_id, $dont_stop = false)
    {

        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        \Bitrix\Main\Loader::includeModule('iblock');
        \Bitrix\Main\Loader::includeModule('catalog');
        global $USER;
        $error = false;
        $hasErrors = false;

        // Инфо о заявке
        $zayavka = tools\el::info($zayavka_id);

        // Проверки
        if (intval($zayavka['ID']) > 0) {
            // Заявка согласована
            if ($zayavka['PROPERTY_ACCEPTED_VALUE']) {
                // Заявка ещё не завершена
                if (!$zayavka['PROPERTY_FINISHED_VALUE']) {
                    if (strlen($zayavka['DETAIL_TEXT']) > 0) {
                        $arJSON = tools\funcs::json_to_array($zayavka['~DETAIL_TEXT']);
                        if (is_array($arJSON) && count($arJSON) > 0) {
                            $tovarCategories = $arJSON['categories'];
                            if (is_array($tovarCategories) && count($tovarCategories) > 0) {
                            } else {
                                $error = 'в заявку (ID=' . $zayavka_id . ') не переданы категории';
                            }
                        } else {
                            $error = 'заявка (ID=' . $zayavka_id . ') - JSON пустой';
                        }
                    } else {
                        $error = 'заявка (ID=' . $zayavka_id . ') - JSON пустой';
                    }
                } else {
                    $error = 'заявка (ID=' . $zayavka_id . ') уже завершена';
                }
            } else {
                $error = 'заявка (ID=' . $zayavka_id . ') ещё не согласована';
            }
        } else {
            $error = 'заявка (ID=' . $zayavka_id . ') не найдена';
        }

        if ($error) {
            $hasErrors = true;
            tools\logger::addError('Конструктор: ошибка создания товаров - ' . $error);
            // ответ
            if (!$dont_stop) {
                return array('status' => 'error', 'text' => $error);
            }
        }

        // Удалим товары, созданные по данной заявке ранее
        $products = \CIBlockElement::GetList(
            ["SORT" => "ASC"],
            [
                "IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
                "PROPERTY_ZAYAVKA" => $zayavka_id
            ], false, false, ["ID", "NAME", "PROPERTY_ZAYAVKA"]
        );
        while ($product = $products->GetNext()) {
            // Добавим товар в очередь на удаление
            $to_delete_product = new project\to_delete_product();
            $to_delete_product->add($product['ID']);
        }

        // Удалим принты, привязанные к данной заявке ранее (при наличии)
        $prints = \CIBlockElement::GetList(
            ["SORT" => "ASC"],
            [
                "IBLOCK_ID" => project\site::PRINTS_IBLOCK_ID,
                "PROPERTY_ZAYAVKA_ID" => $zayavka_id
            ], false, false, ["ID", "PROPERTY_ZAYAVKA_ID"]
        );
        while ($print = $prints->GetNext()) {
            \CIBlockElement::Delete($print['ID']);
        }

        // Создаём новый принт
        $catalogPrint = new project\catalog_print();
        $print_id = $catalogPrint->add(
            $arJSON['name'],
            $arJSON['print_picture_id'],
            $zayavka
        );
        if (intval($print_id) > 0) {
            $print = tools\el::info($print_id);
        } else {
            $error = 'заявка (ID=' . $zayavka_id . ') - ошибка создания нового принта';
        }

        if ($error) {
            $hasErrors = true;
            tools\logger::addError('Конструктор: ошибка создания товара - ' . $error);
            // ответ
            if (!$dont_stop) {
                return array('status' => 'error', 'text' => $error);
            }
        }

        $collections = [];
        if( is_array($arJSON['collections'])  ){
            foreach ( $arJSON['collections'] as $collection ){
                if( intval($collection['id']) >0 ){
                    $collections[] = intval($collection['id']);
                }
            }
        }

        $user_id = $zayavka['PROPERTY_USER_VALUE'];

        $to_delete = [
            'elements' => []
        ];

        // Перебираем выбранные элементы-категории товаров
        // (сначала проверяем на ошибки)
        $categoriesErrors = [];
        foreach ($tovarCategories as $categoryElementID) {
            // Инфо по элементу-категории товаров
            $categoryElement = tools\el::info($categoryElementID);
            $catalogCategoryID = $categoryElement['PROPERTY_CATALOG_SECTION_VALUE'];
            if (intval($catalogCategoryID) > 0) {
                // Инфо о категории каталога
                $catalogCategory = tools\section::info($catalogCategoryID);
                if (intval($catalogCategory['ID']) > 0) {
                    // Цепочка разделов в строковом формате
                    $sectChainStr = project\catalog_niti::getCategoryChainStr($catalogCategory['ID']);
                    // Получим настройки категории
                    $categorySettings = project\catalog_niti::getCategorySettings($catalogCategory['ID']);
                    if (is_null($categorySettings['price'])) {
                        $categoriesErrors[] = 'У категории каталога "' . $sectChainStr . '" (ID = ' . $catalogCategory['ID'] . ') не настроена цена для товаров;';
                    }
                } else {
                    $categoriesErrors[] = 'Тип/категория товара (для конструктора) "' . html_entity_decode($categoryElement['NAME']) . '" привязана к несуществующей категории каталога;';
                }
            } else {
                $categoriesErrors[] = 'Тип/категория товара (для конструктора) "' . html_entity_decode($categoryElement['NAME']) . '" не имеет привязки к целевой категории каталога;';
            }
        }
        if (count($categoriesErrors) > 0) {
            $error_text = "[Конструктор] Заявка ID=" . $zayavka_id . " - ошибки по настройкам категорий:" . "\n";
            $error_text .= "- " . implode("\n" . "- ", $categoriesErrors);
            tools\logger::addError($error_text);
            // ответ
            return array('status' => 'error', 'text' => $error_text);
        }

        // Теги
        $tags_ids = [];
        if( is_array($arJSON['tags']) && count($arJSON['tags']) > 0 ){
            $tags = [];
            foreach ( $arJSON['tags'] as $key => $tagItem ){
                if( strlen(trim($tagItem['text'])) > 0 ){
                    $tags[] = $tagItem['text'];
                }
            }
            if( count($tags) > 0 ){
                $tags_ids = project\tag::add($tags);
            }
        }

        // Перебираем выбранные элементы-категории товаров
        foreach ($tovarCategories as $categoryElementID) {

            // Инфо по элементу-категории товаров
            $categoryElement = tools\el::info($categoryElementID);
            $catalogCategoryID = $categoryElement['PROPERTY_CATALOG_SECTION_VALUE'];

            // Инфо о категории каталога
            $catalogCategory = tools\section::info($catalogCategoryID);

            $categoryPictureID = false;
            if ( intval($arJSON['categoriesParams'][$categoryElementID]['picture_id'] ) > 0) {
                $categoryPictureID = $arJSON['categoriesParams'][$categoryElementID]['picture_id'];
            }

            // Получим настройки категории
            $categorySettings = project\catalog_niti::getCategorySettings($catalogCategory['ID']);

            // Все размеры (ID)
            $sizes = $categoryElement['PROPERTY_SIZES_VALUE'];
            $first_size_id = $sizes[0];
            // настройки категории по размерам
            $categorySizesParams = $arJSON['categoriesParams'][$categoryElementID]['sizes'];
            // настройки категории по цветам
            $categoryColorsParams = $arJSON['categoriesParams'][$categoryElementID]['colors'];

            // Получение фото (наложений) для разных размеров (при наличии)
            // наложение произойдёт только если передан $categorySizesParams
            $res = static::getSizesOffersPhotos(
                $categoryPictureID,
                $categoryElement,
                $print,
                $categorySizesParams,
                $sizes,
                $arJSON
            );
            extract($res);

            // Получение фото (наложений) для разных цветов (при наличии)
            // наложение произойдёт только если передан $categoryColorsParams
            $res = static::getColorsOffersPhotos(
                $categoryPictureID,
                $categoryElement,
                $print,
                $categoryColorsParams,
                $arJSON
            );
            extract($res);

            // Принятие решения о создании товара
            $makeProduct = count($sizes) > 0 || count($colors) > 0 || intval($categoryPictureID) > 0;

            if( $makeProduct ){

                // Создаём товар
                $PROP[15] = $print_id;
                $PROP[48] = $user_id;
                $PROP[29] = $collections;
                $PROP[85] = $zayavka_id;
                if( count($tags_ids) > 0 ){    $PROP[81] = $tags_ids;    }
                $PROP[114] = json_encode([
                    'colorOffersPhotos' => $colorOffersPhotos,
                    'sizesOffersPhotos' => $sizesOffersPhotos
                ]);

                // Устанавливаем фото к товару
                if(  count($sizes) == 0  &&  count($colors) == 0  ){
                    if( intval($categoryPictureID) > 0 ){
                        $PROP[116] = $categoryPictureID;
                    }
                }

                echo "<pre>"; print_r( $PROP ); echo "</pre>";

                $element = new \CIBlockElement;
                $fields = array(
                    "IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
                    "IBLOCK_SECTION_ID" => $categoryElement['PROPERTY_CATALOG_SECTION_VALUE'],
                    "PROPERTY_VALUES" => $PROP,
                    "NAME" => strip_tags($arJSON['name']),
                    "ACTIVE" => "Y",
                    "CODE" => strip_tags(\CUtil::Translit($arJSON['name'] . time(), "ru"))
                );

                if ($tov_id = $element->Add($fields)) {

                    $to_delete[] = $tov_id;

                    if (intval($arJSON['default_color_id']) > 0) {
                        $default_color_id = $arJSON['default_color_id'];
                    } else {
                        $default_color_id = $colors[0]['id'];
                    }

                    // Если есть и цвета и размеры
                    if (
                        count($sizes) > 0
                        &&
                        count($colors) > 0
                    ){


                        foreach ($colors as $color) {
                            foreach ($sizes as $size_id) {

                                // Создаём ТП
                                $PROP = [];
                                $PROP[5] = $tov_id;
                                $PROP[6] = $color['id'];
                                $PROP[7] = $size_id;

                                // ТП по умолчанию
                                if (
                                    $color['id'] == $default_color_id
                                    &&
                                    $size_id == $first_size_id
                                ) {
                                    $PROP[98] = 1;
                                }

                                $element = new \CIBlockElement;
                                $fields = array(
                                    "IBLOCK_ID" => project\site::SKU_IBLOCK_ID,
                                    "PROPERTY_VALUES" => $PROP,
                                    "NAME" => $arJSON['name'] . ' (' . $color['name'] . ', ' . tools\el::info($size_id)['NAME'] . ')',
                                    "ACTIVE" => "Y"
                                );

                                $photo_id = false;
                                if ( intval($colorOffersPhotos[$color['id']]) > 0 ){
                                    $photo_id = $colorOffersPhotos[$color['id']];
                                } else if ( intval($sizesOffersPhotos[$size_id]) > 0 ){
                                    $photo_id = $sizesOffersPhotos[$size_id];
                                }
                                if ( intval($photo_id) > 0 ){
                                    $fields['PROPERTY_VALUES'][47] = $photo_id;
                                    //$fields['DETAIL_PICTURE'] = \CFile::MakeFileArray($photo_id);
                                    //$new_offer_photo_id = \CFile::CopyFile($photo_id);
                                    //if ( intval($new_offer_photo_id) > 0 ) {
                                    //    $fields['DETAIL_PICTURE'] = \CFile::MakeFileArray($new_offer_photo_id);
                                    //}
                                }


                                $sku_id = $element->Add($fields);

                                if (intval($sku_id) > 0) {

                                    $to_delete[] = $sku_id;

                                    // Создаём товар
                                    $productFields = array(
                                        "ID" => $sku_id,
                                        "VAT_ID" => 1,
                                        "VAT_INCLUDED" => "Y"
                                    );
                                    $res = \CCatalogProduct::Add($productFields);
                                    if ($res) {

                                        // Ценовое предложение
                                        $arFields = array(
                                            "PRODUCT_ID" => $sku_id,
                                            "CATALOG_GROUP_ID" => project\site::CATALOG_PRICE_ID,
                                            "PRICE" => $categorySettings['price'],
                                            "CURRENCY" => "RUB"
                                        );
                                        \CPrice::Add($arFields);

                                    }

                                } else {

                                    // Удалим ранее созданные
                                    static::deleteItems($to_delete);
                                    // Удаляем временные фото наложений
                                    static::removeTempPhotos( $colorOffersPhotos, $sizesOffersPhotos );
                                    $hasErrors = true;
                                    // Ошибку в лог
                                    $error_text = '[Конструктор] элемент инфоблока ТП не создан - ' . $element->LAST_ERROR;
                                    tools\logger::addError($error_text);
                                    // ответ
                                    if (!$dont_stop) {
                                        return array('status' => 'error', 'text' => $error_text);
                                    }
                                }
                            }
                        }


                        // Если есть только размеры
                    } else if (count($sizes) > 0) {


                        $first_size_id = $sizes[0];

                        foreach ($sizes as $size_id) {

                            // Создаём ТП
                            $PROP = [];
                            $PROP[5] = $tov_id;
                            $PROP[7] = $size_id;
                            // ТП по умолчанию
                            if ($size_id == $first_size_id) {
                                $PROP[98] = 1;
                            }

                            $element = new \CIBlockElement;
                            $fields = array(
                                "IBLOCK_ID" => project\site::SKU_IBLOCK_ID,
                                "PROPERTY_VALUES" => $PROP,
                                "NAME" => $arJSON['name'] . ' (' . tools\el::info($size_id)['NAME'] . ')',
                                "ACTIVE" => "Y"
                            );

                            $photo_id = false;
                            if (intval($sizesOffersPhotos[$size_id]) > 0) {
                                $photo_id = $sizesOffersPhotos[$size_id];
                            }
                            if ( intval($photo_id) > 0 ){
                                $fields['PROPERTY_VALUES'][47] = $photo_id;
                                //$fields['DETAIL_PICTURE'] = \CFile::MakeFileArray($photo_id);
                                //$new_offer_photo_id = \CFile::CopyFile($photo_id);
                                //if ( intval($new_offer_photo_id) > 0 ) {
                                //    $fields['DETAIL_PICTURE'] = \CFile::MakeFileArray($new_offer_photo_id);
                                //}
                            }

                            $sku_id = $element->Add($fields);

                            if (intval($sku_id) > 0) {

                                $to_delete[] = $sku_id;

                                // Создаём товар
                                $productFields = array(
                                    "ID" => $sku_id,
                                    "VAT_ID" => 1,
                                    "VAT_INCLUDED" => "Y"
                                );
                                $res = \CCatalogProduct::Add($productFields);

                                if ( $res ){

                                    // Ценовое предложение
                                    $arFields = array(
                                        "PRODUCT_ID" => $sku_id,
                                        "CATALOG_GROUP_ID" => project\site::CATALOG_PRICE_ID,
                                        "PRICE" => $categorySettings['price'],
                                        "CURRENCY" => "RUB"
                                    );
                                    \CPrice::Add($arFields);

                                }

                            } else {

                                // Удалим ранее созданные
                                static::deleteItems($to_delete);
                                // Удаляем временные фото наложений
                                static::removeTempPhotos( $colorOffersPhotos, $sizesOffersPhotos );
                                $hasErrors = true;
                                // Ошибку в лог
                                $error_text = '[Конструктор] элемент инфоблока ТП не создан - ' . $element->LAST_ERROR;
                                tools\logger::addError($error_text);
                                // ответ
                                if (!$dont_stop) {
                                    return array('status' => 'error', 'text' => $error_text);
                                }
                            }
                        }


                        // Если есть только цвета
                    } else if (count($colors) > 0) {


                        $first_color_id = $colors[0]['id'];

                        foreach ($colors as $color) {

                            // Создаём ТП
                            $PROP = [];
                            $PROP[5] = $tov_id;
                            $PROP[6] = $color['id'];
                            // ТП по умолчанию
                            if ($color['id'] == $first_color_id) {
                                $PROP[98] = 1;
                            }

                            $element = new \CIBlockElement;
                            $fields = array(
                                "IBLOCK_ID" => project\site::SKU_IBLOCK_ID,
                                "PROPERTY_VALUES" => $PROP,
                                "NAME" => $arJSON['name'] . ' (' . $color['name'] . ')',
                                "ACTIVE" => "Y"
                            );

                            $photo_id = false;
                            if (intval($colorOffersPhotos[$color['id']]) > 0) {
                                $photo_id = $colorOffersPhotos[$color['id']];
                            }
                            if ( intval($photo_id) > 0 ){
                                $fields['PROPERTY_VALUES'][47] = $photo_id;
                                //$fields['DETAIL_PICTURE'] = \CFile::MakeFileArray($photo_id);
                                //$new_offer_photo_id = \CFile::CopyFile($photo_id);
                                //if ( intval($new_offer_photo_id) > 0 ) {
                                //    $fields['DETAIL_PICTURE'] = \CFile::MakeFileArray($new_offer_photo_id);
                                //}
                            }
                            $sku_id = $element->Add($fields);

                            if (intval($sku_id) > 0) {

                                $to_delete[] = $sku_id;

                                // Создаём товар
                                $productFields = array(
                                    "ID" => $sku_id,
                                    "VAT_ID" => 1,
                                    "VAT_INCLUDED" => "Y"
                                );
                                $res = \CCatalogProduct::Add($productFields);

                                if ($res) {

                                    // Ценовое предложение
                                    $arFields = array(
                                        "PRODUCT_ID" => $sku_id,
                                        "CATALOG_GROUP_ID" => project\site::CATALOG_PRICE_ID,
                                        "PRICE" => $categorySettings['price'],
                                        "CURRENCY" => "RUB"
                                    );
                                    \CPrice::Add($arFields);

                                }

                            } else {

                                // Удалим ранее созданные
                                static::deleteItems($to_delete);
                                // Удаляем временные фото наложений
                                static::removeTempPhotos( $colorOffersPhotos, $sizesOffersPhotos );

                                $hasErrors = true;
                                // Ошибку в лог
                                $error_text = '[Конструктор] элемент инфоблока ТП не создан - ' . $element->LAST_ERROR;
                                tools\logger::addError($error_text);
                                // ответ
                                if (!$dont_stop) {
                                    return array('status' => 'error', 'text' => $error_text);
                                }
                            }
                        }

                    // Если нет ни цветов, ни размеров
                    } else {

                        // Создаём просто основной товар
                        $productFields = array(
                            "ID" => $tov_id,
                            "VAT_ID" => 1,
                            "VAT_INCLUDED" => "Y"
                        );
                        $res = \CCatalogProduct::Add($productFields);

                        if ($res) {

                            // И установим ему цену $categorySettings['price']
                            $arFields = array(
                                "PRODUCT_ID" => $tov_id,
                                "CATALOG_GROUP_ID" => project\site::CATALOG_PRICE_ID,
                                "PRICE" => $categorySettings['price'],
                                "CURRENCY" => "RUB"
                            );
                            \CPrice::Add($arFields);

                        }

                    }

                    setSKUInfo($tov_id);

                    // Удаляем временные фото наложений
                    //static::removeTempPhotos( $colorOffersPhotos, $sizesOffersPhotos );

                    // обновим CODE
                    $element = new \CIBlockElement;
                    $fields = array(
                        "XML_ID" => 'constructor_' . $tov_id,
                        "CODE" => \CUtil::Translit($print['NAME'] . '_' . $tov_id, "ru")
                    );
                    $res = $element->Update($tov_id, $fields);

                    // Рассылки подписавшимся пользователям
                    //project\i_subscribe::send($print['PROPERTY_USER_ID_VALUE'], $tov_id);

                    BXClearCache(true, '/illustratorCheckColor/' . $user_id . '/');


                } else {

                    // Удалим ранее созданные
                    static::deleteItems($to_delete);
                    // Удаляем временные фото наложений
                    static::removeTempPhotos( $colorOffersPhotos, $sizesOffersPhotos );
                    $hasErrors = true;
                    // Ошибку в лог
                    $error_text = '[Конструктор] элемент инфоблока товаров не создан - ' . $element->LAST_ERROR;
                    tools\logger::addError($error_text);
                    // ответ
                    if (!$dont_stop) {
                        return array('status' => 'error', 'text' => $error_text);
                    }
                }

            }

        }

        // установка галочки Завершена
        if (!$hasErrors) {
            $set_prop = ["FINISHED" => 1];
            $constr_iblock_id = project\site::CONSTRUCTOR_NOTES;
            //\CIBlockElement::SetPropertyValuesEx($zayavka['ID'], $constr_iblock_id, $set_prop);
            BXClearCache(true, "/".tools\el::EL_CACHE_NAME."/".$zayavka['ID']."/");
            BXClearCache(true, "/".tools\el::EL_CACHE_NAME."_by_code/".$zayavka['IBLOCK_ID']."/".$zayavka['CODE']."/");
        }

        return array('status' => 'ok');
    }



    static function removeTempPhotos(
        $colorOffersPhotos,
        $sizesOffersPhotos
    ){
        // Удаляем временные фото наложений
        if ( is_array($colorOffersPhotos) && count($colorOffersPhotos) > 0) {
            foreach ($colorOffersPhotos as $photo_id) {  \CFile::Delete($photo_id);  }
        }
        if ( is_array($sizesOffersPhotos) && count($sizesOffersPhotos) > 0) {
            foreach ($sizesOffersPhotos as $photo_id) {  \CFile::Delete($photo_id);  }
        }
    }



    // Удаление после ошибки
    static function deleteItems($to_delete)
    {
        \Bitrix\Main\Loader::includeModule('iblock');
        if (is_array($to_delete['elements'])) {
            foreach ($to_delete['elements'] as $element_id) {
                \CIBlockElement::Delete($element_id);
            }
        }
    }



}
