<? namespace AOptima\Project;
use AOptima\Project as project;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;


class handlers {



    static function OnAfterIBlockAdd( $arFields ){
        BXClearCache(true, "/catalog_iblocks/");
    }
    static function OnAfterIBlockUpdate( $arFields ){
        BXClearCache(true, "/catalog_iblocks/");
    }
    static function OnIBlockDelete( $ID ){
        BXClearCache(true, "/catalog_iblocks/");
    }


    static function OnBeforeIBlockAdd( &$arFields ){}
    static function OnBeforeIBlockUpdate( &$arFields ){}





    static function OnAfterIBlockElementAdd( $arFields ){
        // Каталог
        if( $arFields['IBLOCK_ID'] == project\site::CATALOG_IBLOCK_ID ){
            BXClearCache(true, "/homePageCheckboxHits/");
            BXClearCache(true, "/home_page_hits_from_log/");
        }
        // Категории для поиска
        if( $arFields['IBLOCK_ID'] == project\site::SEARCH_CATEGORIES_IBLOCK_ID ){
            BXClearCache(true, "/search_categories/");
        }
        // Коллекции
        if( $arFields['IBLOCK_ID'] == project\site::COLLECTIONS_IBLOCK_ID ){
            BXClearCache(true, "/filter_cache_ib_prop_all_e_items/".$arFields['IBLOCK_ID']."/");
        }
        // Теги
        if( $arFields['IBLOCK_ID'] == project\site::CONSTR_TAGS_IBLOCK_ID ){
            BXClearCache(true, "/filter_cache_ib_prop_all_e_items/".$arFields['IBLOCK_ID']."/");
        }
        // Принты
        if( $arFields['IBLOCK_ID'] == project\site::PRINTS_IBLOCK_ID ){
            BXClearCache(true, "/all_prints/");
        }
        // Фабрика поиска
        if( $arFields['IBLOCK_ID'] == project\site::SEARCH_FABRIC_IBLOCK_ID ){
            BXClearCache(true, "/all_fabric_pages/");
        }
    }
    static function OnAfterIBlockElementUpdate( $arFields ){
        // Каталог
        if( $arFields['IBLOCK_ID'] == project\site::CATALOG_IBLOCK_ID ){
            BXClearCache(true, "/homePageCheckboxHits/");
            BXClearCache(true, "/home_page_hits_from_log/");
        }
        // Категории для поиска
        if( $arFields['IBLOCK_ID'] == project\site::SEARCH_CATEGORIES_IBLOCK_ID ){
            BXClearCache(true, "/search_categories/");
        }
        // Коллекции
        if( $arFields['IBLOCK_ID'] == project\site::COLLECTIONS_IBLOCK_ID ){
            BXClearCache(true, "/filter_cache_ib_prop_all_e_items/".$arFields['IBLOCK_ID']."/");
        }
        // Теги
        if( $arFields['IBLOCK_ID'] == project\site::CONSTR_TAGS_IBLOCK_ID ){
            BXClearCache(true, "/filter_cache_ib_prop_all_e_items/".$arFields['IBLOCK_ID']."/");
        }
        // Принты
        if( $arFields['IBLOCK_ID'] == project\site::PRINTS_IBLOCK_ID ){
            BXClearCache(true, "/all_prints/");
        }
        // Фабрика поиска
        if( $arFields['IBLOCK_ID'] == project\site::SEARCH_FABRIC_IBLOCK_ID ){
            BXClearCache(true, "/all_fabric_pages/");
        }
    }
    static function OnAfterIBlockElementDelete(){}


    static function OnBeforeIBlockElementAdd( &$arFields ){
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        // Каталог
        if( $arFields['IBLOCK_ID'] == project\site::CATALOG_IBLOCK_ID ){
            $collection_names = [];
            $prop_id = project\catalog_niti::PRODUCT_COLLECTIONS_PROP_ID;
            $arProp = $arFields['PROPERTY_VALUES'][$prop_id];
            if( is_array($arProp) ){
                if( intval($arProp[array_keys($arProp)[0]]['VALUE']) > 0 ){
                    $collection = \AOptima\Tools\el::info($arProp[array_keys($arProp)[0]]['VALUE']);
                    if( intval($collection['ID']) > 0 ){
                        $collection_names[] = $collection['NAME'];
                    }
                }
                $collection_names = array_unique($collection_names);
            }
            if( count($collection_names) > 0 ){
                $arFields['PROPERTY_VALUES'][project\catalog_niti::PRODUCT_COLLECTIONS_NAMES_PROP_ID] = [];
                $cnt = -1;
                foreach ( $collection_names as $collection_name ){
                    $cnt++;
                    $arFields['PROPERTY_VALUES'][project\catalog_niti::PRODUCT_COLLECTIONS_NAMES_PROP_ID]['n'.$cnt]['VALUE'] = $collection_name;
                }
            }
        }

        // Пол
        if( $arFields['IBLOCK_ID'] == project\site::GENDERS_IBLOCK_ID ){
            // Отмена сохранения
            global $APPLICATION;
            $APPLICATION->throwException("Изменение состава групп/полов заблокировано");
            return false;
        }

    }
    static function OnBeforeIBlockElementUpdate( &$arFields ){
        \Bitrix\Main\Loader::includeModule('aoptima.tools');
        // Каталог
        if( $arFields['IBLOCK_ID'] == project\site::CATALOG_IBLOCK_ID ){
            $collection_names = [];
            $prop_id = project\catalog_niti::PRODUCT_COLLECTIONS_PROP_ID;
            $arProp = $arFields['PROPERTY_VALUES'][$prop_id];
            if( is_array($arProp) ){
                if( intval($arProp[array_keys($arProp)[0]]['VALUE']) > 0 ){
                    $collection = \AOptima\Tools\el::info($arProp[array_keys($arProp)[0]]['VALUE']);
                    if( intval($collection['ID']) > 0 ){
                        $collection_names[] = $collection['NAME'];
                    }
                }
                $collection_names = array_unique($collection_names);
            }
            if( count($collection_names) > 0 ){
                $arFields['PROPERTY_VALUES'][project\catalog_niti::PRODUCT_COLLECTIONS_NAMES_PROP_ID] = [];
                $cnt = -1;
                foreach ( $collection_names as $collection_name ){
                    $cnt++;
                    $arFields['PROPERTY_VALUES'][project\catalog_niti::PRODUCT_COLLECTIONS_NAMES_PROP_ID]['n'.$cnt]['VALUE'] = $collection_name;
                }
            }
        }
        // Пол
        if( $arFields['IBLOCK_ID'] == project\site::GENDERS_IBLOCK_ID ){
            // Отмена сохранения
            global $APPLICATION;
            $APPLICATION->throwException("Изменение состава групп/полов заблокировано");
            return false;
        }
    }
    static function OnBeforeIBlockElementDelete( $ID ){
        $iblock_id = tools\el::getIblock($ID);
        // Каталог
        if( $iblock_id == project\site::CATALOG_IBLOCK_ID ){
            BXClearCache(true, "/homePageCheckboxHits/");
            BXClearCache(true, "/home_page_hits_from_log/");
            // инфо об элементе
            $el = tools\el::info($ID);
            if( intval($el['ID']) > 0 ){
                BXClearCache(true, "/related_goods/".$ID."/");
                if( strlen($el['PROPERTY_PHOTOS_INFO_VALUE']) > 0 ){
                    $photosInfo = tools\funcs::json_to_array($el['PROPERTY_PHOTOS_INFO_VALUE']);
                    // Удаляем фото наложений, которые делались для товара
                    if (
                        is_array($photosInfo['colorOffersPhotos'])
                        &&
                        count($photosInfo['colorOffersPhotos']) > 0
                    ){
                        foreach ($photosInfo['colorOffersPhotos'] as $photo_id) {
                            \CFile::Delete($photo_id);
                        }
                    }
                    if (
                        is_array($photosInfo['sizesOffersPhotos'])
                        &&
                        count($photosInfo['sizesOffersPhotos']) > 0
                    ){
                        foreach ($photosInfo['sizesOffersPhotos'] as $photo_id) {
                            \CFile::Delete($photo_id);
                        }
                    }
                }
            }
        }
        // Категории для поиска
        if( $iblock_id == project\site::SEARCH_CATEGORIES_IBLOCK_ID ){
            BXClearCache(true, "/search_categories/");
        }
        // Коллекции
        if( $iblock_id == project\site::COLLECTIONS_IBLOCK_ID ){
            BXClearCache(true, "/filter_cache_ib_prop_all_e_items/".$iblock_id."/");
        }
        // Теги
        if( $iblock_id == project\site::CONSTR_TAGS_IBLOCK_ID ){
            BXClearCache(true, "/filter_cache_ib_prop_all_e_items/".$iblock_id."/");
        }
        // Пол
        if( $iblock_id == project\site::GENDERS_IBLOCK_ID ){
            // Отмена сохранения
            global $APPLICATION;
            $APPLICATION->throwException("Изменение состава групп/полов заблокировано");
            return false;
        }
        // Принты
        if( $iblock_id == project\site::PRINTS_IBLOCK_ID ){
            BXClearCache(true, "/all_prints/");
        }
        // Фабрика поиска
        if( $iblock_id == project\site::SEARCH_FABRIC_IBLOCK_ID ){
            BXClearCache(true, "/all_fabric_pages/");
        }
    }






    static function OnAfterIBlockSectionAdd( $arFields ){
        BXClearCache(true, "/filter_e_groups/".$arFields['IBLOCK_ID'].'/');
        // Категории для поиска
        if( $arFields['IBLOCK_ID'] == project\site::SEARCH_CATEGORIES_IBLOCK_ID ){
            BXClearCache(true, "/search_categories/");
            BXClearCache(true, "/search_categories_sections/");
        }
        // Каталог
        if( $arFields['IBLOCK_ID'] == project\site::CATALOG_IBLOCK_ID ){
            BXClearCache(true, "/all_catalog_categories/");
            BXClearCache(true, "/catalog_sections_1_level/");
        }
    }
    static function OnAfterIBlockSectionUpdate( $arFields ){
        BXClearCache(true, "/filter_e_groups/".$arFields['IBLOCK_ID'].'/');
        // Категории для поиска
        if( $arFields['IBLOCK_ID'] == project\site::SEARCH_CATEGORIES_IBLOCK_ID ){
            BXClearCache(true, "/search_categories/");
            BXClearCache(true, "/search_categories_sections/");
        }
        // Каталог
        if( $arFields['IBLOCK_ID'] == project\site::CATALOG_IBLOCK_ID ){
            BXClearCache(true, "/all_catalog_categories/");
            BXClearCache(true, "/catalog_sections_1_level/");
        }
    }
    static function OnAfterIBlockSectionDelete(){}


    static function OnBeforeIBlockSectionAdd( &$arFields ){}
    static function OnBeforeIBlockSectionDelete( $ID ){
        $iblock_id = tools\el::getIblock($ID);
        BXClearCache(true, "/filter_e_groups/".$iblock_id.'/');
        // Категории для поиска
        if( $iblock_id == project\site::SEARCH_CATEGORIES_IBLOCK_ID ){
            BXClearCache(true, "/search_categories/");
            BXClearCache(true, "/search_categories_sections/");
        }
        // Каталог
        if( $iblock_id == project\site::CATALOG_IBLOCK_ID ){
            BXClearCache(true, "/all_catalog_categories/");
            BXClearCache(true, "/catalog_sections_1_level/");
        }
    }




    static function OnSuccessCatalogImport1C( $arParams, $filePath ){}





    static function OnAfterUserAdd(&$arFields){}
    static function OnAfterUserUpdate(&$arFields){}
    static function OnBeforeUserDelete($ID){}






}