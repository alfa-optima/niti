<? namespace AOptima\Project;
use AOptima\Project as project;



class search_illustrators {



	const PAGE_ELEMENT_COUNT = 30;
	
	

	static function searchByName( $q ){
	    
	    $list = [];

	    $filter = [
	        "ACTIVE" => "Y",
            "UF_USER_TYPE" => 2,
            'NAME' => '%'.$q.'%'
        ];
	    $fields = Array(
	    	'FIELDS' => [ 'ID', 'NAME' ],
	    	'SELECT' => [ 'UF_USER_TYPE' ]
	    );
	    $rsUsers = \CUser::GetList(($by="id"), ($order="desc"), $filter, $fields);
	    while ($arUser = $rsUsers->GetNext()){
            $list[$arUser['ID']] = $arUser;
	    }

        $filter = [
            "ACTIVE" => "Y",
            "UF_USER_TYPE" => 2,
            'LAST_NAME' => '%'.$q.'%'
        ];
        $fields = Array(
            'FIELDS' => [ 'ID', 'LAST_NAME' ],
            'SELECT' => [ 'UF_USER_TYPE' ]
        );
        $rsUsers = \CUser::GetList(($by="id"), ($order="desc"), $filter, $fields);
        while ($arUser = $rsUsers->GetNext()){
            $list[$arUser['ID']] = $arUser;
        }
	    return $list;
	}




}