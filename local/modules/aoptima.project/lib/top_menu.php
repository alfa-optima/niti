<? namespace AOptima\Project;
use AOptima\Project as project;



class top_menu {


    const DOP_PUNKTY_IBLOCK_ID = 39;


    
    
    static function get(){

        $genderTypes = project\gender_type::list();

        // Все категории каталога
        $all_catalog_categories = project\catalog_niti::all_catalog_categories();

        // Все категории каталога
        $all_fabric_pages = project\search_fabric::getAllList();
        
        // Добавим на 1 уровень гендерные пункты - для неё, для него, детям
        $menu = [];
        foreach( $genderTypes as $genderType ){
            if( $genderType['PROPERTY_SHOW_IN_TOP_MENU_VALUE'] ){
                // Соберём пункты для текущего пола
                $items = [];
                foreach ( $all_catalog_categories as $cat_2 ){
                    if(
                        $cat_2['DEPTH_LEVEL'] == 2
                        &&
                        $cat_2['GENDER_TYPE'] == $genderType['ID']
                        &&
                        $cat_2['SECTION_ID'] == project\catalog_niti::clothesCatalogSectionID()
                    ){
                        // Соберём подкатегории
                        $sub_items = [];
                        foreach ( $all_catalog_categories as $cat_3 ){
                            if(
                                $cat_3['DEPTH_LEVEL'] == 3
                                &&
                                $cat_3['SECTION_ID'] == $cat_2['ID']
                            ){
                                $sub_items[] = [
                                    'name' => strlen($cat_3['SITE_NAME'])>0?$cat_3['SITE_NAME']:$cat_3['NAME'],
                                    'url' => project\top_menu::getCategoryURL( $cat_3, $all_fabric_pages )
                                ];
                            }
                        }
                        $items[] = [
                            'name' => strlen($cat_2['SITE_NAME'])>0?$cat_2['SITE_NAME']:$cat_2['NAME'],
                            'url' => project\top_menu::getCategoryURL( $cat_2, $all_fabric_pages ),
                            'sub_items' => $sub_items
                        ];
                    }
                }
                $menu[] = [
                    'type' => 'gender',
                    'name' => $genderType['NAME'],
                    'items' => $items
                ];
            }
        }

        // Добавим на 1 уровень - аксессуары
        // Соберём подпункты для аксессуаров
        $items = [];
        foreach ( $all_catalog_categories as $cat_2 ){
            if(
                $cat_2['DEPTH_LEVEL'] == 2
                &&
                $cat_2['SECTION_ID'] == project\catalog_niti::accessoriesCatalogSectionID()
            ){
                // Соберём подпункты для текущего пола
                $sub_items = [];
                foreach ( $all_catalog_categories as $cat_3 ){
                    if(
                        $cat_3['DEPTH_LEVEL'] == 3
                        &&
                        $cat_3['SECTION_ID'] == $cat_2['ID']
                    ){
                        $sub_items[] = [
                            'name' => strlen($cat_3['UF_NAME'])>0?$cat_3['UF_NAME']:$cat_3['NAME'],
                            'url' => project\top_menu::getCategoryURL( $cat_3, $all_fabric_pages )
                        ];
                    }
                }
                $items[] = [
                    'name' => strlen($cat_2['SITE_NAME'])>0?$cat_2['SITE_NAME']:$cat_2['NAME'],
                    'url' => project\top_menu::getCategoryURL( $cat_2, $all_fabric_pages ),
                    'sub_items' => $sub_items
                ];
            }
        }
        $menu[] = [
            'type' => project\catalog_niti::ACCESSOIRES_SECTION_CODE,
            'name' => 'Аксессуары',
            'items' => $items
        ];

        // Добавим на 1 уровень - доп. пункты
        $dop_items = static::dop_items();
        foreach ( $dop_items as $dop_item ){
            $menu[] = [
                'type' => 'dop_item',
                'name' => $dop_item['title'],
                'link' => $dop_item['link'],
                'open_in_new_tab' => $dop_item['open_in_new_tab'],
                'color' => $dop_item['color'],
            ];
        }
        
        return $menu;
    }
    


    static function getCategoryURL( $cat, $all_fabric_pages ){
        $fabric_page = null;
        if(
            intval( $cat['FABRIC_PAGE_ID'] ) > 0
            &&
            isset( $all_fabric_pages[ $cat['FABRIC_PAGE_ID'] ] )
        ){    $fabric_page = $all_fabric_pages[ $cat['FABRIC_PAGE_ID'] ];    }
        if( isset($fabric_page) ){
            $url = '/'.$fabric_page['CODE'].'/';
        } else {
            $url = '/search/';
            //$url = $cat['URL'];
        }
        return $url;
    }
    
    

    // Верх. меню
    static function dop_items(){
        \Bitrix\Main\Loader::includeModule('iblock');
        $list = [];
        $filter = [
        	"IBLOCK_ID" => static::DOP_PUNKTY_IBLOCK_ID,
        	"ACTIVE" => "Y"
        ];
        $fields = [ "ID", "NAME", "PROPERTY_LINK", "PROPERTY_OPEN_IN_NEW_TAB", "PROPERTY_COLOR" ];
        $dbElements = \CIBlockElement::GetList(
        	[ "SORT" => "ASC" ], $filter, false, false, $fields
        );
        while ($element = $dbElements->GetNext()){
            $list[] = [
                'title' => $element['NAME'],
                'link' => $element['PROPERTY_LINK_VALUE'],
                'open_in_new_tab' => $element['PROPERTY_OPEN_IN_NEW_TAB_VALUE']?true:false,
                'color' => $element['PROPERTY_COLOR_VALUE']?true:false
            ];
        }
        return $list;
    }


    
    
    
    
    



}

