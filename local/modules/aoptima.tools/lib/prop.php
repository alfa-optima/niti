<? namespace AOptima\Tools;
use AOptima\Tools as tools;



class prop {



    // Свойство по ID
    static function getByID( $prop_id ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $props = \CIBlockProperty::GetByID( $prop_id );
        if($prop = $props->GetNext()){
            return $prop;
        }
        return false;
    }



    // Свойство по коду
    static function getByCode( $prop_code, $iblock_id ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $props = \CIBlockProperty::GetList(
            [ "sort"=>"asc", "name"=>"asc" ],
            [ "IBLOCK_ID" => $iblock_id, "CODE" => $prop_code ]
        );
        if ($prop = $props->GetNext()){
            return $prop;
        }
        return false;
    }


	
	// Код свойства по ID
    static function getCode( $prop_id ){
		\Bitrix\Main\Loader::includeModule('iblock');
		$prop_res = \CIBlockProperty::GetByID( $prop_id );
		if($el_prop = $prop_res->GetNext()){
			$prop_code = $el_prop['CODE'];
			return $prop_code;
		}
		return false;
	}



    // Перечень свойств инфоблока
    static function getList( $iblock_id ){
        \Bitrix\Main\Loader::includeModule('iblock');
        $list = [];
        $properties = \CIBlockProperty::GetList(
            [ "sort"=>"asc", "name"=>"asc" ],
            [ "IBLOCK_ID" => $iblock_id ]
        );
        while ($prop = $properties->GetNext()){
            $list[$prop['ID']] = $prop;
        }
        return $list;
    }


	
}