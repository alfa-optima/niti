<? namespace AOptima\Tools;
use AOptima\Tools as tools;



class user {
	

	
	const USER_CACHE_NAME = 'user';
	
	
	
	
	// Инфо о пользователе
	static function info($user_id){
		$arUser = false;
		if( intval($user_id) > 0 ){
			// Кешируем 
			$obCache = new \CPHPCache();
			$cache_time = 30*24*60*60;
			$cache_id = static::USER_CACHE_NAME.'_'.$user_id;
			$cache_path = '/'.static::USER_CACHE_NAME.'/'.$user_id.'/';
			if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
				$vars = $obCache->GetVars();   extract($vars);
			} elseif($obCache->StartDataCache()){
				// Инфо о пользователе
				$dbUser = \CUser::GetByID($user_id);   $arUser = $dbUser->GetNext();
				if ( intval($arUser['ID']) > 0 ){    
					$arUser = tools\funcs::clean_array($arUser);
				}
			$obCache->EndDataCache(array('arUser' => $arUser));
			}
		}
		return $arUser;
	}
	


	static function getFullName( $user_id ){
	    $user = static::info($user_id);
        if( intval($user['ID']) > 0 ){
            $fullName = '';
            if( strlen($user['NAME']) > 0 ){
                $fullName .= $user['NAME'];
            }
            if( strlen($user['LAST_NAME']) > 0 ){
                if( strlen($user['LAST_NAME']) > 0 ){
                    $fullName .= ' ';
                }
                $fullName .= $user['LAST_NAME'];
            }
            if( strlen($fullName) == 0 ){
                $fullName = 'Пользователь [ID = '.$user['ID'].']';
            }
            return $fullName;
        }
        return false;
    }
	
	
	
	// Авторизация
	static function auth(){
		if(
			!empty($_SERVER['HTTP_X_REQUESTED_WITH'])
			&&
			strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
		){
			global $USER;
			if( !$USER->IsAuthorized() ){
				$result = $USER->Login(strip_tags(tools\funcs::param_post("login")), strip_tags(tools\funcs::param_post("password")), "Y");
				if (!is_array($result)){
					$USER->Authorize($USER->GetID());
					// Ответ
					echo json_encode( Array( "status" => "ok" ) );
				} else {
					// Ответ
					echo json_encode( Array( "status" => "error", "text" => "Неверный логин или пароль" ) );
				}
			} else {
				// Ответ
				echo json_encode( Array( "status" => "auth", "text" => "Вы уже авторизованы" ) );
			}
		}
	}
	
	
	
	
	// Регистрация
	/*static function register(){
		if(
			!empty($_SERVER['HTTP_X_REQUESTED_WITH'])
			&&
			strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
		){
			global $USER;
			if( !$USER->IsAuthorized() ){
				// данные в массив
				$arFields = Array();
				parse_str(param_post("form_data"), $arFields);
				// проверяем EMAIL на логин
				$filter = Array( "LOGIN" => strip_tags($arFields["EMAIL"]) );
				$rsUsers = \CUser::GetList(($by="id"), ($order="desc"), $filter); 
				while ($user = $rsUsers->GetNext()){
					// Если Email уже есть
					echo json_encode( Array( "status" => "error", "text" => "Данный логин уже зарегистрирован на&nbsp;сайте!" ) );  return;
				}
				// проверяем EMAIL на наличие у других пользователей
				$filter = Array( "EMAIL" => strip_tags($arFields["EMAIL"]) );
				$rsUsers = \CUser::GetList(($by="id"), ($order="desc"), $filter);
				while ($user = $rsUsers->GetNext()){
					// Если Email уже есть
					echo json_encode( Array( "status" => "error", "text" => "Данный Email уже зарегистрирован на&nbsp;сайте!" ) );  return;
				}
				// Регистрируем пользователя
				$user = new \CUser;
				$arUserFields = Array(
					"ACTIVE" => "Y",
					"LOGIN" => strip_tags($arFields["EMAIL"]),
					"EMAIL" => strip_tags($arFields["EMAIL"]),
					"NAME" => strip_tags($arFields["NAME"]),
					"PASSWORD" => strip_tags($arFields["PASSWORD"]),
					"CONFIRM_PASSWORD" => strip_tags($arFields["CONFIRM_PASSWORD"]),
					"GROUP_ID" => Array(6)
				);
				$userID = $user->Add($arUserFields);
				if ($userID){
					static::sendRegInfo($userID);
					// Ответ
					echo json_encode( array("status" => 'ok') );
				} else {
					// Ответ
					echo json_encode( array("status" => 'error', "text" => $user->LAST_ERROR) );
				}
			} else {
				// Ответ
				echo json_encode( Array( "status" => "auth", "text" => "Вы уже авторизованы" ) );
			}
		}
	}*/
	
	
	
	
	// Регистрационная информация на почту
	static function sendRegInfo($user_id, $password = false){
		if( intval($user_id) > 0 ){
			$user = static::info($user_id);
			if( intval($user['ID']) > 0 ){
				$html .= '<p>Ваша регистрационная информация для сайта "'.$_SERVER['HTTP_HOST'].'":</p>';
				$html .= '<p>ID: '.$user["ID"].'</p>';
				$html .= '<p>Имя: '.$user["NAME"].'</p>';
				$html .= '<p>E-Mail: '.$user["EMAIL"].'</p>';
				if( $password ){
					$html .= '<p>Пароль: '.$password.'</p>';
				}
				// отправка инфы на почту
				$arEventFields = Array(
					"EMAIL" => $user["EMAIL"],
					"TITLE" => 'Регистрационная информация (сайт "'.$_SERVER['SERVER_NAME'].'")',
					"HTML" => $html
				);
				\CEvent::SendImmediate("SEND_USER_INFO", "s1", $arEventFields);
			}
		}
	}
	
	
	
	
	// Сверка пароля с пользовательским
	static function comparePassword($userId, $password){
		if( intval($userId) > 0 && strlen($password) > 0 ){
			$userData = static::info($userId);
			$salt = substr($userData['PASSWORD'], 0, (strlen($userData['PASSWORD']) - 32));
			$realPassword = substr($userData['PASSWORD'], -32);
			$password = md5($salt.$password);
			return ($password == $realPassword);
		}
		return false;
	}
	
	
	
	
}



