<?

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;



// Ловим изменения элементов инфоблока (After)
AddEventHandler("iblock", "OnAfterIBlockElementAdd", "OnAfterIBlockElementHandler");
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", "OnAfterIBlockElementHandler");
function OnAfterIBlockElementHandler($arFields){
	// Проставка свойства SORT_DATE
	setSortDate($arFields);
	// Чистим кеши
	BXClearCache(true, "/el_info/".$arFields['ID']."/");
	$el = tools\el::info($arFields['ID']);
	BXClearCache(true, "/el_info_by_code/".$arFields['IBLOCK_ID']."/".$el['CODE']."/");
	BXClearCache(true, "/el_sections/".$arFields['ID']."/");
	
	// Каталог
	if ( $arFields['IBLOCK_ID'] == project\site::CATALOG_IBLOCK_ID ){
		BXClearCache(true, "/main_hits/");
		BXClearCache(true, "/".project\related_goods::CACHE_NAME."/".$el['PROPERTY_PRINT_ID_VALUE']."/");
		BXClearCache(true, '/illustratorCheckColor/');
	}
	
	// SKU
	if ( $arFields['IBLOCK_ID'] == project\site::SKU_IBLOCK_ID ){
		$result = \CCatalogSku::GetProductInfo( $arFields['ID'] );
		if ( intval($result['ID'])>0 ){
			BXClearCache(true, "/one_good_sku/".$result['ID']."/");
			BXClearCache(true, '/illustratorCheckColor/');
		}
	}
	
	// Страны
	if ( $arFields['IBLOCK_ID'] == project\site::COUNTRIES_IBLOCK_ID ){
		BXClearCache(true, "/all_countries/");
	}
	
	// Баннеры
	if ( $arFields['IBLOCK_ID'] == project\site::BANNERS_IBLOCK_ID ){
		BXClearCache(true, "/all_banners/");
	}
	
	// Карты
	if ( $arFields['IBLOCK_ID'] == project\site::MAPS_IBLOCK_ID ){
		BXClearCache(true, "/maps/");
		BXClearCache(true, "/map_points/");
	}
	
	// Контакты
	if ( $arFields['IBLOCK_ID'] == project\site::CONTACTS_IBLOCK_ID ){
		BXClearCache(true, "/contact_groups/");
		BXClearCache(true, "/contact_group_elements/");
	}
	
	// Коллекции
	if ( $arFields['IBLOCK_ID'] == project\site::COLLECTIONS_IBLOCK_ID ){
		BXClearCache(true, "/mainCollections/");
		BXClearCache(true, "/allCollections/");
	}
	
	// SEO блоки
	if ( $arFields['IBLOCK_ID'] == project\site::SEO_IBLOCK_ID ){
		BXClearCache(true, "/allSeoBlocks/");
	}
	
	// Новости
	if ( $arFields['IBLOCK_ID'] == project\site::NEWS_IBLOCK_ID ){
		BXClearCache(true, "/".project\news::allItemsCacheID."/");
	}
	
	// Принты
	if ( $arFields['IBLOCK_ID'] == project\site::PRINTS_IBLOCK_ID ){
		BXClearCache(true, "/illustratorPrints/");
		
		/*$dbProps = \CIBlockProperty::GetByID( 'STATUS', $arFields['IBLOCK_ID'] );
		if($prop = $dbProps->GetNext()){
			if( $arFields['PROPERTY_VALUES'][$prop['ID']][array_keys($arFields['PROPERTY_VALUES'][$prop['ID']])[0]]['VALUE'] == 6 ){
				\CIBlockElement::Delete($arFields['ID']);
			}
		}*/
		
	}
	
}






// Ловим удаление элементов инфоблока (Before)
AddEventHandler("iblock", "OnBeforeIBlockElementDelete", "OnBeforeIBlockElementDeleteHandler");
function OnBeforeIBlockElementDeleteHandler($ID){
	$iblock_id = tools\el::getIblock($ID);
	$el = tools\el::info($ID);
	// Чистим кеши
	BXClearCache(true, "/el_info/".$ID."/");
	BXClearCache(true, "/el_info_by_code/".$el['IBLOCK_ID']."/".$el['CODE']."/");
	BXClearCache(true, "/el_sections/".$el['ID']."/");
	
	// Каталог
	if ( $iblock_id == project\site::CATALOG_IBLOCK_ID ){
		BXClearCache(true, "/main_hits/");
		
		BXClearCache(true, '/illustratorCheckColor/');
		
		BXClearCache(true, "/".project\related_goods::CACHE_NAME."/".$el['PROPERTY_PRINT_ID_VALUE']."/");
		
		clear_dir($_SERVER['DOCUMENT_ROOT'].project\constructor::IMAGES_PATH.$ID.'/');
		rmdir($_SERVER['DOCUMENT_ROOT'].project\constructor::IMAGES_PATH.$ID.'/');
	}
	
	// ТП
	if ( $iblock_id == project\site::SKU_IBLOCK_ID ){
		BXClearCache(true, '/illustratorCheckColor/');
	}
	
	// Страны
	if ( $iblock_id == project\site::COUNTRIES_IBLOCK_ID ){
		BXClearCache(true, "/all_countries/");
	}
	
	// Баннеры
	if ( $iblock_id == project\site::BANNERS_IBLOCK_ID ){
		BXClearCache(true, "/all_banners/");
	}
	
	// Карты
	if ( $iblock_id == project\site::MAPS_IBLOCK_ID ){
		BXClearCache(true, "/maps/");
		BXClearCache(true, "/map_points/");
	}
	
	// Контакты
	if ( $iblock_id == project\site::CONTACTS_IBLOCK_ID ){
		BXClearCache(true, "/contact_groups/");
		BXClearCache(true, "/contact_group_elements/");
	}

	// Коллекции
	if ( $iblock_id == project\site::COLLECTIONS_IBLOCK_ID ){
		BXClearCache(true, "/mainCollections/");
		BXClearCache(true, "/allCollections/");
	}
	
	// SEO блоки
	if ( $iblock_id == project\site::SEO_IBLOCK_ID ){
		BXClearCache(true, "/allSeoBlocks/");
	}
	
	// Новости
	if ( $iblock_id == project\site::NEWS_IBLOCK_ID ){
		BXClearCache(true, "/".project\news::allItemsCacheID."/");
	}
	
	// Принты
	if ( $iblock_id == project\site::PRINTS_IBLOCK_ID ){
		BXClearCache(true, "/illustratorPrints/");
	}
	
	// Заявки из конструктора
	if ( $iblock_id == project\site::CONSTR_ITEMS_IBLOCK_ID ){
		global $APPLICATION;
		$APPLICATION->throwException('Заявки не подлежат удалению');
		return false;
	}
	
}



// Ловим изменения элементов инфоблока (Before)
AddEventHandler("iblock", "OnBeforeIBlockElementAdd", "OnBeforeIBlockElementHandler");
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", "OnBeforeIBlockElementHandler");
function OnBeforeIBlockElementHandler(&$arFields){
	// Цвета
	if( $arFields['IBLOCK_ID'] == project\site::CONSTR_COLORS_IBLOCK_ID ){
		$arFields['NAME'] = strtolower(trim($arFields['NAME']));
		$filter = Array(
			"IBLOCK_ID" => project\site::CONSTR_COLORS_IBLOCK_ID,
			"ACTIVE" => "Y",
			"=NAME" => $arFields['NAME'],
		);
		if( intval($arFields['ID']) > 0 ){
			$filter['!ID'] = $arFields['ID'];
		}
		$dbElements = \CIBlockElement::GetList(
			Array("SORT"=>"ASC"), $filter, false, Array("nTopCount"=>1), Array("ID", "NAME"));
		while ($element = $dbElements->GetNext()){
			// Отмена сохранения
			global $APPLICATION;
			$APPLICATION->throwException('Цвет "'.$arFields['NAME'].'" уже создан');
			return false;
		}
	
	}
	// Теги
	if( $arFields['IBLOCK_ID'] == project\site::CONSTR_TAGS_IBLOCK_ID ){
		$arFields['NAME'] = strtolower(trim($arFields['NAME']));
		$filter = Array(
			"IBLOCK_ID" => project\site::CONSTR_TAGS_IBLOCK_ID,
			"ACTIVE" => "Y",
			"=NAME" => $arFields['NAME'],
		);
		if( intval($arFields['ID']) > 0 ){
			$filter['!ID'] = $arFields['ID'];
		}
		$dbElements = \CIBlockElement::GetList(
			Array("SORT"=>"ASC"), $filter, false, Array("nTopCount"=>1), Array("ID", "NAME"));
		while ($element = $dbElements->GetNext()){
			// Отмена сохранения
			global $APPLICATION;
			$APPLICATION->throwException('Тег "'.$arFields['NAME'].'" уже создан');
			return false;
		}
	
	}
}




// Ловим изменения разделов
AddEventHandler("iblock", "OnAfterIBlockSectionAdd", "OnAfterIBlockSectionHandler");
AddEventHandler("iblock", "OnAfterIBlockSectionUpdate", "OnAfterIBlockSectionHandler");
function OnAfterIBlockSectionHandler($arFields) {
	// Чистим кеши
	BXClearCache(true, "/section/".$arFields['ID']."/");
	$section = tools\section::info($arFields['ID']);
	BXClearCache(true, "/section_by_code/".$arFields['IBLOCK_ID'].'/'.$section['CODE']."/");
	// подразделы
	$sub_sects = tools\section::sub_sects($arFields['ID']);
	if ( count($sub_sects) > 0 ){
		foreach ($sub_sects as $sub_sect){
			BXClearCache(true, "/section/".$chainSection['ID']."/");
			BXClearCache(true, "/section_by_code/".$arFields['IBLOCK_ID']."/".$chainSection['CODE']."/");
			BXClearCache(true, "/section_chain/".$chainSection['ID']."/");
			BXClearCache(true, "/sub_sects_cnt/".$chainSection['ID']."/");
			BXClearCache(true, "/sub_sects/".$chainSection['ID']."/");	
		}
	}
	//цепочка
	$dbChain = \CIBlockSection::GetNavChain($arFields['IBLOCK_ID'], $arFields['ID']);
	while ($chainSection = $dbChain->GetNext()){
		BXClearCache(true, "/section/".$chainSection['ID']."/");
		BXClearCache(true, "/section_by_code/".$arFields['IBLOCK_ID']."/".$chainSection['CODE']."/");
		BXClearCache(true, "/section_chain/".$chainSection['ID']."/");
		BXClearCache(true, "/sub_sects_cnt/".$chainSection['ID']."/");
		BXClearCache(true, "/sub_sects/".$chainSection['ID']."/");
	}
	BXClearCache(true, "/el_sections/");
	
	// Каталог
	if ( $arFields['IBLOCK_ID'] == project\site::CATALOG_IBLOCK_ID ){
		BXClearCache(true, "/top_sections/");
		BXClearCache(true, "/catalog_1_level_sections/");
		BXClearCache(true, "/catalog_sections_1_level/");
		BXClearCache(true, "/topCategoriesMenu/");
	}
	
	// Карты
	if ( $arFields['IBLOCK_ID'] == project\site::MAPS_IBLOCK_ID ){
		BXClearCache(true, "/maps/");
		BXClearCache(true, "/map_points/");
	}
	
	// Контакты
	if ( $arFields['IBLOCK_ID'] == project\site::CONTACTS_IBLOCK_ID ){
		BXClearCache(true, "/contact_groups/");
		BXClearCache(true, "/contact_group_elements/");
	}
	
}



// Перед сохранением раздела
AddEventHandler("iblock", "OnBeforeIBlockSectionAdd", "OnBeforeSectionHandler");
AddEventHandler("iblock", "OnBeforeIBlockSectionUpdate", "OnBeforeSectionHandler");
function OnBeforeSectionHandler(&$arFields){
	
	// Каталог
	if ( $arFields['IBLOCK_ID'] == project\site::CATALOG_IBLOCK_ID ){
		
		// Если родитель - раздел 1 уровня (значит текущий раздел 2 уровня), то
		$parent_sect = tools\section::info($arFields['IBLOCK_SECTION_ID']);
		if( $parent_sect['DEPTH_LEVEL'] == 1 ){
			
			// Если не задан вес
			if( !$arFields['UF_VES'] || strlen($arFields['UF_VES']) == 0 ){
				
				global $APPLICATION;
				$APPLICATION->throwException('Укажите, пожалуйста, вес для товаров данной категории');
				return false;
				
			}
		}

	}
	
}



AddEventHandler("iblock", "OnBeforeIBlockSectionDelete", "OnBeforeSectionDeleteHandler");
function OnBeforeSectionDeleteHandler($ID) {

	$iblock_id = tools\section::getIblock($ID);
	
	// Чистим кеши
	BXClearCache(true, "/section/".$ID."/");
	$section = tools\section::info($ID);
	BXClearCache(true, "/section_by_code/".$iblock_id.'/'.$section['CODE']."/");
	BXClearCache(true, "/section/".$ID."/");
	// подразделы
	$sub_sects = tools\section::sub_sects($arFields['ID']);
	if ( count($sub_sects) > 0 ){
		foreach ($sub_sects as $sub_sect){
			BXClearCache(true, "/section/".$chainSection['ID']."/");
			BXClearCache(true, "/section_by_code/".$iblock_id."/".$chainSection['CODE']."/");
			BXClearCache(true, "/section_chain/".$chainSection['ID']."/");
			BXClearCache(true, "/sub_sects_cnt/".$chainSection['ID']."/");
			BXClearCache(true, "/sub_sects/".$chainSection['ID']."/");	
		}
	}
	// цепочка
	$dbChain = \CIBlockSection::GetNavChain($iblock_id, $ID);
	while ($chainSection = $dbChain->GetNext()){
		BXClearCache(true, "/section/".$chainSection['ID']."/");
		BXClearCache(true, "/section_by_code/".$iblock_id."/".$chainSection['CODE']."/");
		BXClearCache(true, "/section_chain/".$chainSection['ID']."/");
		BXClearCache(true, "/sub_sects_cnt/".$chainSection['ID']."/");
		BXClearCache(true, "/sub_sects/".$chainSection['ID']."/");
	}
	BXClearCache(true, "/el_sections/");
	
	// Каталог
	if ( $iblock_id == project\site::CATALOG_IBLOCK_ID ){
		BXClearCache(true, "/top_sections/");
		BXClearCache(true, "/catalog_1_level_sections/");
		BXClearCache(true, "/catalog_sections_1_level/");
		BXClearCache(true, "/topCategoriesMenu/");
	}
	
	// Карты
	if ( $iblock_id == project\site::MAPS_IBLOCK_ID ){
		BXClearCache(true, "/maps/");
		BXClearCache(true, "/map_points/");
	}
	
	// Контакты
	if ( $iblock_id == project\site::CONTACTS_IBLOCK_ID ){
		BXClearCache(true, "/contact_groups/");
		BXClearCache(true, "/contact_group_elements/");
	}
	
}




// Ловим изменения по свойствам инфоблока
AddEventHandler("iblock", "OnAfterIBlockPropertyAdd", "OnAfterIBlockPropertyHandler");
AddEventHandler("iblock", "OnAfterIBlockPropertyUpdate", "OnAfterIBlockPropertyHandler");
function OnAfterIBlockPropertyHandler($arFields){
	BXClearCache(true, "/all_iblock_props/".$arFields['IBLOCK_ID']."/");
}
AddEventHandler("iblock", "OnBeforeIBlockDelete", "OnBeforeIBlockDeleteHandler");
function OnBeforeIBlockDeleteHandler($iblock_id){
	BXClearCache(true, "/all_iblock_props/".$iblock_id."/");
}



// Ловим Add и Update ценового предложения (After)
AddEventHandler("catalog", "OnPriceAdd", "OnAfterPriceHandler");
AddEventHandler("catalog", "OnPriceUpdate", "OnAfterPriceHandler");
function OnAfterPriceHandler($ID, $arFields){
	// Чистим кеши
	BXClearCache(true, "/el_info/".$arFields['PRODUCT_ID']."/");
	$el = tools\el::info($arFields['PRODUCT_ID']);
	BXClearCache(true, "/el_info_by_code/".$el['IBLOCK_ID']."/".$el['CODE']."/");
}
// Ловим Delete ценового предложения (OnBeforePriceDelete)
AddEventHandler("catalog", "OnBeforePriceDelete", "OnBeforeHandler");
function OnBeforeHandler($ID){
	$arFields = \CPrice::GetByID($ID);
	// Чистим кеши
	BXClearCache(true, "/el_info/".$arFields['PRODUCT_ID']."/");
	$el = tools\el::info($arFields['PRODUCT_ID']);
	BXClearCache(true, "/el_info_by_code/".$el['IBLOCK_ID']."/".$el['CODE']."/");
}




// Ловим Add и Update пользователя (After)
AddEventHandler("main", "OnAfterUserAdd", "OnAfterUserHandler");
AddEventHandler("main", "OnAfterUserUpdate", "OnAfterUserHandler");
function OnAfterUserHandler($arFields){
	BXClearCache(true, "/user_info/".$arFields['ID']."/");
}
// Ловим Delete пользователя (Before)
AddEventHandler("main", "OnBeforeUserDelete", "OnBeforeUserDeleteHandler");
function OnBeforeUserDeleteHandler($user_id){
	BXClearCache(true, "/user_info/".$user_id."/");
}




// СОБЫТИЕ ИМПОРТА ИЗ 1С, СОЗДАНИЯ НОВОГО И ИЗМЕНЕНИЯ КУРСА
/*AddEventHandler("catalog", "OnSuccessCatalogImport1C", "CatalogImport1C");
function CatalogImport1C($ID, $arFields){
	BXClearCache(true, "/");
}*/



// ОБРАБОТЧИКИ ДЛЯ SKU
	// Ловим добавление товара
	
	AddEventHandler("catalog", "OnProductAdd", "OnProductAddHandler_SKU");
	function OnProductAddHandler_SKU($ID, $arFields){
		$iblock_id = tools\el::getIblock($ID);
		// ТП
		if ($iblock_id == project\site::SKU_IBLOCK_ID){
			
			addSKUtoProcessing($ID);
			
			$result = \CCatalogSku::GetProductInfo( $ID );
			if ( intval($result['ID'])>0 ){
				BXClearCache(true, "/one_good_sku/".$result['ID']."/");
			}
		}
	}
	// Ловим изменения элементов инфоблока (After)
	AddEventHandler("iblock", "OnAfterIBlockElementAdd", "OnAfterIBlockElementHandler_SKU");
	AddEventHandler("iblock", "OnAfterIBlockElementUpdate", "OnAfterIBlockElementHandler_SKU");
	function OnAfterIBlockElementHandler_SKU($arFields){
		// Каталог
		if ($arFields['IBLOCK_ID'] == project\site::CATALOG_IBLOCK_ID){
			setSKUinfo($arFields['ID']);
		}
		// ТП
		if ($arFields['IBLOCK_ID'] == project\site::SKU_IBLOCK_ID){
			setSKUinfo($arFields['ID']);
			
			$result = \CCatalogSku::GetProductInfo( $arFields['ID'] );
			if ( intval($result['ID'])>0 ){
				BXClearCache(true, "/one_good_sku/".$result['ID']."/");
			}
			
		}
		if ($arFields['IBLOCK_ID'] == project\site::SKU_PROCESSING_IBLOCK_ID){
			BXClearCache(true, "/sku_to_processing/");
		}
	}
	// Ловим событие после удаления элемента инфоблока
	AddEventHandler(
		"iblock",
		"OnAfterIBlockElementDelete",
		"OnAfterIBlockElementDeleteHandler_SKU"
	);
	function OnAfterIBlockElementDeleteHandler_SKU($arFields){
		// Если определена сессия sku_deletes
		if ( intval( $_SESSION['sku_deletes'][$arFields['ID']] ) > 0 ){
			setSKUinfo( $_SESSION['sku_deletes'][$arFields['ID']] );
		}
		unset($_SESSION['sku_deletes']);
	}
	// Ловим удаление элементов инфоблока (Before)
	AddEventHandler(
		"iblock",
		"OnBeforeIBlockElementDelete",
		"OnBeforeIBlockElementDeleteHandler_SKU"
	);
	function OnBeforeIBlockElementDeleteHandler_SKU($ID){
		$iblock_id = tools\el::getIblock($ID);
		// ТП
		if ( $iblock_id == project\site::SKU_IBLOCK_ID ){
			// Инфа об основном товаре
			$result = \CCatalogSku::GetProductInfo( $ID );
			// ID товара
			$tov_id = $result['ID'];
			// запишем ID основного товара в сессию - для OnAfterIBlockElementDeleteHandler
			$_SESSION['sku_deletes'][$ID] = $tov_id;
			
			$result = \CCatalogSku::GetProductInfo( $ID );
			if ( intval($result['ID'])>0 ){
				BXClearCache(true, "/one_good_sku/".$result['ID']."/");
			}
			
		}
		if ($iblock_id == project\site::SKU_PROCESSING_IBLOCK_ID){
			BXClearCache(true, "/sku_to_processing/");
		}
	}



// Обновляем urlrewrite.php после добавление нового раздела
AddEventHandler("iblock", "OnAfterIBlockSectionAdd", "OnAfterIBlockSectionAddHandler");
function OnAfterIBlockSectionAddHandler($arFields) {
	UpdateUrlRewrite(project\site::CATALOG_IBLOCK_ID, 'myCatalog', "/catalog/catalog.php");
}
// Обновляем urlrewrite.php после редактирования раздела
AddEventHandler("iblock", "OnAfterIBlockSectionUpdate", "OnAfterIBlockSectionUpdateHandler");
function OnAfterIBlockSectionUpdateHandler($arFields) {
	UpdateUrlRewrite(project\site::CATALOG_IBLOCK_ID, 'myCatalog', "/catalog/catalog.php");
}
// Обновляем urlrewrite.php перед удалением раздела
AddEventHandler("iblock", "OnBeforeIBlockSectionDelete", "OnBeforeIBlockSectionDeleteHandler");
function OnBeforeIBlockSectionDeleteHandler($ID) {
	UpdateUrlRewrite(project\site::CATALOG_IBLOCK_ID, 'myCatalog', "/catalog/catalog.php");
}
