<?

use \Bitrix\Main,
    \Bitrix\Main\Localization\Loc as Loc,
    \Bitrix\Main\Loader,
    \Bitrix\Main\Config\Option,
    \Bitrix\Sale\Delivery,
    \Bitrix\Sale\PaySystem,
    \Bitrix\Sale,
    \Bitrix\Sale\Order,
    \Bitrix\Sale\DiscountCouponsManager,
    \Bitrix\Main\Application,
    \Bitrix\Main\Context;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;



function getLocations()
{
    if (in_array(param_get('ds'), project\site::$calc_ds_list)) {
        $class_name = array_search(param_get('ds'), project\site::$calc_ds_list);
        $res = $class_name::search(param_get('term'));
    }
    return json_encode($res);
}


function checkColor($color_id, $search_ids)
{
    $check = true;
    if (
        arURI(pureURL())[1] == 'illustrators'
        &&
        arURI(pureURL())[2]
    ) {
        $check = project\illustrator::checkColor(
            $color_id,
            arURI(pureURL())[2]
        );
    }
    if (
        arURI(pureURL())[1] == 'print'
        &&
        arURI(pureURL())[2]
    ) {
        $check = project\catalog_print::checkColor(
            $color_id,
            arURI(pureURL())[2]
        );
    }
    if (arURI(pureURL())[1] == 'search') {
        $check = project\catalog_search::checkColor($color_id, $search_ids);
    }
    return $check;
}


function checkCollection($collection_id, $search_ids)
{
    $check = true;
    if (
        arURI(pureURL())[1] == 'illustrators'
        &&
        arURI(pureURL())[2]
    ) {
        $check = project\illustrator::checkCollection(
            $collection_id,
            arURI(pureURL())[2]
        );
    }
    if (
        arURI(pureURL())[1] == 'print'
        &&
        arURI(pureURL())[2]
    ) {
        $check = project\catalog_print::checkCollection(
            $collection_id,
            arURI(pureURL())[2]
        );
    }
    if (arURI(pureURL())[1] == 'search') {
        $check = project\catalog_search::checkCollection($collection_id, $search_ids);
    }
    return $check;
}



function constructorSuccess()
{
    if ($_SESSION['constructor_ok'] == 1) {
        unset($_SESSION['constructor_ok']);
        echo "<script type='text/javascript'>
		$(document).ready(function(){
			successWindow( 'Заявка на создание товара<br>успешно отправлена!<br><br>Товар будет создан<br>после одобрения<br>администратором сайта.' );
		});
		</script>";
    }
}

function passwordSuccess()
{
    if ($_SESSION['password_ok'] == 1) {
        unset($_SESSION['password_ok']);
        echo "<script type='text/javascript'>
		$(document).ready(function(){
			successWindow( 'Пароль успешно изменён!<br><br>Теперь авторизуйтесь!' );
		});
		</script>";
    }
}


// Получение типа удалённого файла
function file_type($url)
{
    $fp = fopen($url, "r");
    $inf = stream_get_meta_data($fp);
    fclose($fp);
    foreach ($inf["wrapper_data"] as $v) {
        if (stristr($v, "content-type")) {
            $v = explode(":", $v);
            return trim($v[1]);
        }
    }
    return false;
}

// Получение размера удалённого файла
function file_size($url)
{
    $fp = fopen($url, "r");
    $inf = stream_get_meta_data($fp);
    fclose($fp);
    foreach ($inf["wrapper_data"] as $v) {
        if (stristr($v, "content-length")) {
            $v = explode(":", $v);
            return trim($v[1]);
        }
    }
    return false;
}

// base64 картинки
function base64_from_image($img, $echo)
{
    $imageSize = getimagesize($img);
    $imageData = base64_encode(file_get_contents($img));
    $res = "data:{$imageSize['mime']};base64,{$imageData}";
    if ($echo == true) {
        echo $res;
    } else {
        return $res;
    }
}


// Все SEO блоки
function allSeoBlocks()
{

    \Bitrix\Main\Loader::includeModule('iblock');

    $seo_blocks = array();

    // Кешируем
    $obCache = new \CPHPCache();
    $cache_time = 30 * 24 * 60 * 60;
    $cache_id = 'allSeoBlocks';
    $cache_path = '/allSeoBlocks/';
    if ($obCache->InitCache($cache_time, $cache_id, $cache_path)) {
        $vars = $obCache->GetVars();
        extract($vars);
    } elseif ($obCache->StartDataCache()) {
        // Перебираем все видео галереи
        $dbElements = \CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => project\site::SEO_IBLOCK_ID, "ACTIVE" => "Y"), false, false, Array("ID", "PROPERTY_URL", "NAME", "PREVIEW_TEXT"));
        while ( $element = $dbElements->GetNext() ){
            $seo_blocks[] = clean_array($element);
        }
        $obCache->EndDataCache(array('seo_blocks' => $seo_blocks));
    }

    return $seo_blocks;
}


// Получение групп контактов
function getContactGroups()
{

    \Bitrix\Main\Loader::includeModule('iblock');

    // Кешируем
    $obCache = new \CPHPCache();
    $cache_time = 30 * 24 * 60 * 60;
    $cache_id = 'contact_groups';
    $cache_path = '/contact_groups/';
    if ($obCache->InitCache($cache_time, $cache_id, $cache_path)) {
        $vars = $obCache->GetVars();
        extract($vars);
    } elseif ($obCache->StartDataCache()) {
        $contact_groups = array();
        // Перебираем разделы
        $dbSections = \CIBlockSection::GetList(Array("SORT" => "ASC"), Array("ACTIVE" => "Y", "IBLOCK_ID" => project\site::CONTACTS_IBLOCK_ID), false);
        while ($section = $dbSections->GetNext()) {
            $contact_groups[] = $section;
        }
        $obCache->EndDataCache(array('contact_groups' => $contact_groups));
    }
    return $contact_groups;
}

// Получение элементов для группы
function getContactGroupElements($group_id)
{

    \Bitrix\Main\Loader::includeModule('iblock');

    $contact_group_elements = array();
    if (intval($group_id) > 0) {
        // ID инфоблока
        $iblock_id = project\site::CONTACTS_IBLOCK_ID;
        // Кешируем
        $obCache = new \CPHPCache();
        $cache_time = 30 * 24 * 60 * 60;
        $cache_id = 'contact_group_elements_' . $group_id;
        $cache_path = '/contact_group_elements/' . $group_id . '/';
        if ($obCache->InitCache($cache_time, $cache_id, $cache_path)) {
            $vars = $obCache->GetVars();
            extract($vars);
        } elseif ($obCache->StartDataCache()) {
            // Запрашиваем инфо по элементу
            $elements = \CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => $iblock_id, "ACTIVE" => "Y", "SECTION_ID" => $group_id), false, false, array('ID', 'NAME', 'PROPERTY_DESCR', 'PROPERTY_LINK'));
            while ($element = $elements->GetNext()) {
                $contact_group_elements[] = $element;
            }
            $obCache->EndDataCache(array('contact_group_elements' => $contact_group_elements));
        }

    }
    return $contact_group_elements;
}


// Получение карт для контактов
function getMaps()
{

    \Bitrix\Main\Loader::includeModule('iblock');

    // Кешируем
    $obCache = new \CPHPCache();
    $cache_time = 30 * 24 * 60 * 60;
    $cache_id = 'maps';
    $cache_path = '/maps/';
    if ($obCache->InitCache($cache_time, $cache_id, $cache_path)) {
        $vars = $obCache->GetVars();
        extract($vars);
    } elseif ($obCache->StartDataCache()) {
        $maps = array();
        // Перебираем разделы
        $dbSections = \CIBlockSection::GetList(Array("SORT" => "ASC"), Array("ACTIVE" => "Y", "IBLOCK_ID" => project\site::MAPS_IBLOCK_ID), false, Array("UF_DOLGOTA", "UF_SHIROTA", "UF_ZOOM"));
        while ($section = $dbSections->GetNext()) {
            $maps[] = $section;
        }
        $obCache->EndDataCache(array('maps' => $maps));
    }
    return $maps;
}

// Получение точек для карты
function getMapPoints($map_id)
{

    \Bitrix\Main\Loader::includeModule('iblock');

    $map_points = array();
    if (intval($map_id) > 0) {
        // ID инфоблока
        $iblock_id = project\site::MAPS_IBLOCK_ID;
        // Кешируем
        $obCache = new \CPHPCache();
        $cache_time = 30 * 24 * 60 * 60;
        $cache_id = 'map_points_' . $map_id;
        $cache_path = '/map_points/' . $map_id . '/';
        if ($obCache->InitCache($cache_time, $cache_id, $cache_path)) {
            $vars = $obCache->GetVars();
            extract($vars);
        } elseif ($obCache->StartDataCache()) {
            // Запрашиваем инфо по элементу
            $elements = \CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => $iblock_id, "ACTIVE" => "Y", "SECTION_ID" => $map_id), false, false, array('ID', 'NAME', 'PROPERTY_DOLGOTA', 'PROPERTY_SHIROTA'));
            while ($element = $elements->GetNext()) {
                $map_points[] = $element;
            }
            $obCache->EndDataCache(array('map_points' => $map_points));
        }

    }
    return $map_points;
}


// Все товары раздела
function section_elements($section_id)
{

    \Bitrix\Main\Loader::includeModule('iblock');

    // ID инфоблока
    $iblock_id = project\site::CATALOG_IBLOCK_ID;

    $section_elements = array();
    // Запрашиваем инфо по элементу
    $elements = \CIBlockElement::GetList(Array("PROPERTY_MIN_PRICE_AUTO" => "ASC", "NAME" => "ASC"), Array("IBLOCK_ID" => $iblock_id, "ACTIVE" => "Y", "SECTION_ID" => $section_id), false, false, array('ID', 'NAME', 'PROPERTY_MIN_PRICE_AUTO'));
    while ($element = $elements->GetNext()) {
        $section_elements[] = $element['ID'];
    }

    return $section_elements;
}


// Список ставок НДС
function NDS_list()
{

    \Bitrix\Main\Loader::includeModule('catalog');

    $obCache = new \CPHPCache();
    $cache_time = 30 * 24 * 60 * 60;
    $cache_id = 'NDS_list';
    $cache_path = '/NDS_list/';
    if ($obCache->InitCache($cache_time, $cache_id, $cache_path)) {
        $vars = $obCache->GetVars();
        extract($vars);
    } elseif ($obCache->StartDataCache()) {
        $NDS_list = Array();
        $q = \CCatalogVat::GetListEx(Array(), Array());
        while ($a = $q->GetNext()) {
            $NDS_list[$a["ID"]] = intval($a["RATE"]);
        }
        $obCache->EndDataCache(array('NDS_list' => $NDS_list));
    }
    return $NDS_list;
}


// Сохранение инфо по SKU
function setSKUInfo($el_id)
{

    \Bitrix\Main\Loader::includeModule('iblock');

    if (intval($el_id) > 0) {

        // Определяем товар или ТП
        /*$result = CCatalogSku::GetProductInfo( $el_id );
        $is_SKU = intval($result['ID'])>0;
        // Если ТП
        if ( $is_SKU ){
            // ID товара
            $tov_id = $result['ID'];
        } else {*/
        // ID товара
        $tov_id = $el_id;
        //}

        $colors = array();
        $sizes = array();
        $prices = array();
        $has_sku = 0;

        $filter = Array(
            "IBLOCK_ID" => project\site::SKU_IBLOCK_ID,
            "ACTIVE" => "Y",
            "PROPERTY_CML2_LINK" => $tov_id
        );

        $fields = Array(
            "ID", "NAME", "PROPERTY_CML2_LINK",
            "PROPERTY_SIZE", "PROPERTY_COLOR", "CATALOG_GROUP_1"
        );

        // Получим все активные ТП товара
        $dbElements = \CIBlockElement::GetList(Array("SORT" => "ASC"), $filter, false, false, $fields);
        while ($sku = $dbElements->GetNext()) {
            $sizes[] = $sku['PROPERTY_SIZE_VALUE'];
            $colors[] = $sku['PROPERTY_COLOR_VALUE'];
            $has_sku = 1;
            // Инфо по элементу
            $price = $sku['CATALOG_PRICE_1'];
            $prices[] = $price;
        }

        $sizes = array_unique($sizes);
        $colors = array_unique($colors);
        $min_price = min($prices);
        // Запишем данные в товар
        $set_prop = array(
            "SIZES_AUTO" => count($sizes) > 0 ? $sizes : false,
            "COLORS_AUTO" => count($colors) > 0 ? $colors : false,
            "HAS_SKU" => $has_sku,
            "MIN_PRICE_AUTO" => $min_price
        );
        \CIBlockElement::SetPropertyValuesEx($tov_id, project\site::CATALOG_IBLOCK_ID, $set_prop);
    }
}


function addSKUtoProcessing($sku_id)
{
    \Bitrix\Main\Loader::includeModule('highloadblock');
    $hlblock_id = 3;
    $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
    $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
    $entity_data_class = $entity->getDataClass();
    $entity_table_name = $hlblock['TABLE_NAME'];
    $sTableID = 'tbl_' . $entity_table_name;
    $arData = Array('UF_SKU_ID' => $sku_id);
    $result = $entity_data_class::add($arData);
}

function getSKUtoProcessing()
{
    \Bitrix\Main\Loader::includeModule('highloadblock');
    $hlblock_id = 3;
    $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
    $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
    $entity_data_class = $entity->getDataClass();
    $entity_table_name = $hlblock['TABLE_NAME'];
    $sTableID = 'tbl_' . $entity_table_name;
    //////////////////////////////////////
    $arFilter = array();
    $arSelect = array('*');
    $arOrder = array("ID" => "ASC");
    $rsData = $entity_data_class::getList(
        array(
            "select" => $arSelect,
            "filter" => $arFilter,
            "limit" => 50,
            "order" => $arOrder
        )
    );
    $rsData = new \CDBResult($rsData, $sTableID);
    while ($element = $rsData->Fetch()) {
        $ids[] = clean_array($element);
    }
    return $ids;
}

function SKU_processing()
{

    \Bitrix\Main\Loader::includeModule('iblock');

    $ids = getSKUtoProcessing();
    if (count($ids) > 0) {
        foreach ($ids as $id_info) {
            $sku_id = intval($id_info['UF_SKU_ID']);
            $skus = \CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => project\site::SKU_IBLOCK_ID, "ID" => $sku_id), false, Array("nTopCount" => 1), Array("ID", "NAME", "PROPERTY_CML2_LINK"));
            while ($sku = $skus->GetNext()) {
                if (intval($sku['PROPERTY_CML2_LINK_VALUE']) > 0) {
                    setSKUInfo($sku['PROPERTY_CML2_LINK_VALUE']);
                }
            }

            \Bitrix\Main\Loader::includeModule('highloadblock');
            $hlblock_id = 3;
            $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById($hlblock_id)->fetch();
            $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
            $entity_data_class = $entity->getDataClass();
            $entity_table_name = $hlblock['TABLE_NAME'];
            $sTableID = 'tbl_' . $entity_table_name;

            $entity_data_class::delete($id_info["ID"]);

        }
    }
}


// Все баннеры
function all_banners()
{

    \Bitrix\Main\Loader::includeModule('iblock');

    // ID инфоблока
    $iblock_id = project\site::BANNERS_IBLOCK_ID;
    // Кешируем
    $obCache = new \CPHPCache();
    $cache_time = 30 * 24 * 60 * 60;
    $cache_id = 'all_banners';
    $cache_path = '/all_banners/';
    if ($obCache->InitCache($cache_time, $cache_id, $cache_path)) {
        $vars = $obCache->GetVars();
        extract($vars);
    } elseif ($obCache->StartDataCache()) {
        $all_banners = array();
        // Запрашиваем инфо по элементу
        $elements = \CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => $iblock_id, "ACTIVE" => "Y"), false, false, array('NAME', 'ID', "PREVIEW_TEXT", "PREVIEW_PICTURE", "PROPERTY_URL", "PROPERTY_BUTTON_URL", "PROPERTY_BUTTON_TITLE", "PROPERTY_SUB"));
        while ($element = $elements->GetNext()) {
            $all_banners[] = $element;
        }
        $obCache->EndDataCache(array('all_banners' => $all_banners));
    }
    return $all_banners;
}


// Получаем SKU товара $el
function getTovOffers($el)
{

    \Bitrix\Main\Loader::includeModule('iblock');

    $all_sku = array();
    $offers = array();
    $offers_prices = array();
    $offers_old_prices = array();
    $colors = array();
    $color_min_prices = array();
    $color_sizes = array();
    $color_prices = array();
    $sizes = array();

    if (!is_array($el) && intval($el) > 0) {

        $filter = Array(
            "IBLOCK_ID" => project\site::SKU_IBLOCK_ID,
            "PROPERTY_CML2_LINK" => $el,
            "ACTIVE" => "Y"
        );

        $fields = Array(
            "ID",
            "ACTIVE",
            "PROPERTY_CML2_LINK",
            "PROPERTY_COLOR",
            "PROPERTY_SIZE",
            "CATALOG_GROUP_1",
            "PROPERTY_MAIN_TP",
            "PROPERTY_PHOTO_ID",
            "PROPERTY_PHOTOS",
        );

        // Собираем все SKU товара
        $dbSKU = \CIBlockElement::GetList(Array('CATALOG_PRICE_1' => "ASC"), $filter, false, false, $fields);
        while ($arSKU = $dbSKU->GetNext()) {
            // Определяем цену со скидкой
            global $USER;
            $arDiscounts = \CCatalogDiscount::GetDiscountByPrice($arSKU['CATALOG_PRICE_ID_1'], $USER->GetUserGroupArray(), "N", SITE_ID);
            $discountPrice = \CCatalogProduct::CountPriceWithDiscount($arSKU['CATALOG_PRICE_1'], $arSKU['CATALOG_CURRENCY_1'], $arDiscounts);
            $discPrice = round($discountPrice, 0);
            $arSKU['DISC_PRICE'] = $discPrice;
            $all_sku[$arSKU['ID']] = clean_array($arSKU);
        }

        // Перебираем SKU товара
        foreach ($all_sku as $sku) {

            $sku_old_price = round($sku['CATALOG_PRICE_1'], 0);
            $sku_price = $sku['DISC_PRICE'];

            $offers_prices[$sku['ID']] = $sku_price;
            $offers_old_prices[$sku['ID']] = $sku_old_price;

            // Определяем цвет
            $color_id = $sku['PROPERTY_COLOR_VALUE'];
            $color = tools\el::info($color_id);
            if ($color) {
                // Расчёт минимальных цен по цветам
                if (
                    !$color_min_prices[$color_id]
                    ||
                    $color_min_prices[$color_id] > $sku_price
                ) {
                    $color_min_prices[$color_id] = $sku_price;
                }
                // Собираем SKU по цветам
                if (!in_array($color['ID'], $colors)) {
                    $colors[] = $color['ID'];
                    $sku['color'] = $color;
                    $offers[$color['ID']] = $sku;
                }
            }

        }

        $offers_prices_not_sorted = $offers_prices;
        array_multisort($offers_prices, SORT_ASC, SORT_NUMERIC, $offers);

        // Собираем цены и размеры цветов
        // переберём цвета
        foreach ($color_min_prices as $color_id => $value) {
            // Перебираем все SKU товара
            foreach ($all_sku as $sku) {

                $sku_price = $offers_prices_not_sorted[$sku['ID']];
                $sku_old_price = $offers_old_prices[$sku['ID']];

                $size = strtoupper($sku['PROPERTY_SIZE_VALUE']);
                if (
                    $sku['PROPERTY_COLOR_VALUE'] == $color_id
                    &&
                    strlen($size) > 0
                    &&
                    !in_array($size, $color_sizes[$color_id])
                ) {
                    // размеры
                    $color_sizes[$color_id][] = $size;
                    // цены
                    $color_prices[$color_id][$size] = array(
                        'ID' => $sku['ID'],
                        'PRICE' => $sku_price,
                        'OLD_PRICE' => $sku_old_price
                    );
                }
            }
            $color_sizes[$color_id] = sizes_sort($color_sizes[$color_id]);
        }

        if (count($color_sizes) == 0) {
            foreach ($all_sku as $sku) {
                if (strlen($sku['PROPERTY_SIZE_VALUE']) > 0) {

                    $sku_old_price = round($sku['CATALOG_PRICE_1'], 0);
                    // Определяем цену со скидкой
                    global $USER;
                    $arDiscounts = \CCatalogDiscount::GetDiscountByPrice($sku['CATALOG_PRICE_ID_1'], $USER->GetUserGroupArray(), "N", SITE_ID);
                    $discountPrice = \CCatalogProduct::CountPriceWithDiscount($sku['CATALOG_PRICE_1'], $sku['CATALOG_CURRENCY_1'], $arDiscounts);
                    $sku_price = round($discountPrice, 0);

                    $sku_price = $discPrice;
                    $sizes[$sku['PROPERTY_SIZE_VALUE']] = array(
                        'ID' => $sku['ID'],
                        'PRICE' => $sku_price,
                        'OLD_PRICE' => $sku_old_price
                    );
                }
            }
        }


    } else if (is_array($el)) {


        foreach ($el['OFFERS'] as $arSKU) {
            $all_sku[$arSKU['ID']] = $arSKU;
        }

        // Перебираем SKU товара
        foreach ($all_sku as $sku) {

            $sku_old_price = round($sku['PRICES']['BASE']['VALUE'], 0);
            $sku_price = round($sku['PRICES']['BASE']['DISCOUNT_VALUE_VAT'], 0);

            $offers_prices[$sku['ID']] = $sku_price;
            $offers_old_prices[$sku['ID']] = $sku_old_price;

            // Определяем цвет
            $color_id = $sku['PROPERTIES']['COLOR']['VALUE'];
            $color = tools\el::info($color_id);
            if ($color) {
                // Расчёт минимальных цен по цветам
                if (
                    !$color_min_prices[$color_id]
                    ||
                    $color_min_prices[$color_id] > $sku_price
                ) {
                    $color_min_prices[$color_id] = $sku_price;
                }
                // Собираем SKU по цветам
                if (!in_array($color['ID'], $colors)) {
                    $colors[] = $color['ID'];
                    $sku['color'] = $color;
                    $offers[$color['ID']] = $sku;
                }
            }

        }

        $offers_prices_not_sorted = $offers_prices;
        array_multisort($offers_prices, SORT_ASC, SORT_NUMERIC, $offers);

        //pre($offers);

        // Собираем цены и размеры цветов
        // переберём цвета
        foreach ($color_min_prices as $color_id => $value) {
            // Перебираем все SKU товара
            foreach ($all_sku as $sku) {

                $sku_price = $offers_prices_not_sorted[$sku['ID']];
                $sku_old_price = $offers_old_prices[$sku['ID']];

                $size = strtoupper($sku['PROPERTIES']['SIZE']['VALUE']);
                if (
                    $sku['PROPERTIES']['COLOR']['VALUE'] == $color_id
                    &&
                    strlen($size) > 0
                    &&
                    !in_array($size, $color_sizes[$color_id])
                ) {
                    // размеры
                    $color_sizes[$color_id][] = $size;
                    // цены
                    $color_prices[$color_id][$size] = array(
                        'ID' => $sku['ID'],
                        'PRICE' => $sku_price,
                        'OLD_PRICE' => $sku_old_price
                    );
                }
            }
            $color_sizes[$color_id] = sizes_sort($color_sizes[$color_id]);
        }

        if (count($color_sizes) == 0) {
            foreach ($all_sku as $sku) {
                if (strlen($sku['PROPERTIES']['SIZE']['VALUE']) > 0) {
                    $sku_old_price = round($sku['PRICES']['BASE']['VALUE'], 0);
                    $sku_price = round($sku['PRICES']['BASE']['DISCOUNT_VALUE_VAT'], 0);

                    $sizes[$sku['PROPERTIES']['SIZE']['VALUE']] = array(
                        'ID' => $sku['ID'],
                        'PRICE' => $sku_price,
                        'OLD_PRICE' => $sku_old_price
                    );
                }
            }
        }

    }

    ksort($sizes);

    $tovOffers = array(
        'color_list' => $offers,
        "color_min_prices" => $color_min_prices,
        "color_sizes" => $color_sizes,
        "color_prices" => $color_prices,
        "sizes" => $sizes,
        'all_sku' => $all_sku
    );
    return $tovOffers;


}


function sizes_sort($sizes)
{
    $new_sizes = array();
    $new_sizes_2 = array();
    $sizes_line = array('XXS', 'XS', 'S', 'M', 'L', 'XL', 'XXL', 'XXXL');
    foreach ($sizes_line as $line_size) {
        if (in_array($line_size, $sizes)) {
            $new_sizes[] = $line_size;
        }
    }
    foreach ($sizes as $size) {
        if (!in_array($size, $new_sizes)) {
            $new_sizes_2[] = $size;
        }
    }
    sort($new_sizes_2);
    $new_sizes = array_merge($new_sizes, $new_sizes_2);
    return $new_sizes;
}


function isEqualPrices($color_prices, $color_id)
{
    $prices = array();
    foreach ($color_prices[$color_id] as $ar) {
        $prices[] = $ar['PRICE'];
    }
    $min_price = min($prices);
    $max_price = max($prices);
    return ($min_price == $max_price) ? 1 : 0;
}


function all_countries()
{

    \Bitrix\Main\Loader::includeModule('iblock');

    $countries = array();
    // Кешируем
    $obCache = new \CPHPCache();
    $cache_time = 30 * 24 * 60 * 60;
    // ID кеша
    $cache_id = 'all_countries';
    // путь хранения кеша
    $cache_path = '/all_countries/';
    if ($obCache->InitCache($cache_time, $cache_id, $cache_path)) {
        $vars = $obCache->GetVars();
        extract($vars);
    } elseif ($obCache->StartDataCache()) {
        // Соберём страны
        $dbElements = \CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => project\site::COUNTRIES_IBLOCK_ID, "ACTIVE" => "Y"), false, false, Array("ID", "NAME"));
        while ($element = $dbElements->GetNext()) {
            $countries[] = clean_array($element);
        }
        $obCache->EndDataCache(array('countries' => $countries));
    }
    return $countries;
}


function isCatalog()
{
    return in_array(arURI()[1], catalog_1_level_sections());
}

function isProduct()
{
    return arURI()[1] == 'products';
}

function isPrint()
{
    return arURI()[1] == 'print';
}

function isContacts()
{
    return (arURI()[1] == 'about' && arURI()[2] == 'contacts');
}

function isDeliveryPayment()
{
    return (arURI()[1] == 'information' && arURI()[2] == 'delivery_and_payment');
}

function isNews()
{
    return (arURI()[1] == 'news');
}

function isIllustrators()
{
    return (arURI()[1] == 'illustrators');
}

function isShops()
{
    return (arURI()[1] == 'shops');
}

function isFabricPage(){
    $searchFabricPage = tools\el::info_by_code( arURI()[1], project\site::SEARCH_FABRIC_IBLOCK_ID);
    return intval($searchFabricPage['ID'])>0;
}

function isSearch()
{
    return (arURI()[1] == 'search');
}

function isVacancies()
{
    return (arURI()[1] == 'vacancies');
}

function isDelivery()
{
    return (arURI()[1] == 'delivery');
}

function isPayment()
{
    return (arURI()[1] == 'payment');
}

function isHelp()
{
    return (arURI()[1] == 'help');
}

function isGuarantee()
{
    return (arURI()[1] == 'guarantee');
}

function isCollection()
{
    return (arURI()[1] == 'collections');
}


function catalog_1_level_sections()
{

    \Bitrix\Main\Loader::includeModule('iblock');
    
    $sections = array();
    // Кешируем
    $obCache = new \CPHPCache();
    $cache_time = 30 * 24 * 60 * 60;
    // ID кеша
    $cache_id = 'catalog_1_levtools\el::sections';
    // путь хранения кеша
    $cache_path = '/catalog_1_levtools\el::sections/';
    if ($obCache->InitCache($cache_time, $cache_id, $cache_path)) {
        $vars = $obCache->GetVars();
        extract($vars);
    } elseif ($obCache->StartDataCache()) {
        // Соберём разделы 1 уровня
        $sections_1 = \CIBlockSection::GetList(Array("SORT" => "ASC"), Array(
            "ACTIVE" => "Y",
            "GLOBAL_ACTIVE" => "Y",
            "DEPTH_LEVEL" => 1,
            "IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID
        ), false, array());
        while ($section_1 = $sections_1->GetNext()) {
            $sections[] = $section_1['CODE'];
        }
        $obCache->EndDataCache(array('sections' => $sections));
    }
    return $sections;
}


// Инфо о пользователе
function user_info($user_id)
{
    $arUser = false;
    if (intval($user_id) > 0) {
        // Кешируем
        $obCache = new \CPHPCache();
        $cache_time = 30 * 24 * 60 * 60;
        $cache_id = 'user_info_' . $user_id;
        $cache_path = '/user_info/' . $user_id . '/';
        if ($obCache->InitCache($cache_time, $cache_id, $cache_path)) {
            $vars = $obCache->GetVars();
            extract($vars);
        } elseif ($obCache->StartDataCache()) {
            // Инфо о пользователе
            $dbUser = \CUser::GetByID($user_id);
            $arUser = $dbUser->GetNext();
            if (intval($arUser['ID']) > 0) {
                $arUser = clean_array($arUser);
            }
            $obCache->EndDataCache(array('arUser' => $arUser));
        }
    }
    return $arUser;
}


// Проставка свойства SORT_DATE
function setSortDate($arFields)
{

    \Bitrix\Main\Loader::includeModule('iblock');

    $prop = \CIBlockProperty::GetByID('SORT_DATE', $arFields['IBLOCK_ID']);
    if ($prop) {
        $date = $arFields['ACTIVE_FROM'];
        if (!$date) {
            $q = \CIBlockElement::GetList(Array("SORT" => "DESC"), Array("IBLOCK_ID" => $arFields['IBLOCK_ID'], "ID" => $arFields['ID']), false, false, Array("ID", "ACTIVE_FROM", "DATE_CREATE"));
            while ($a = $q->GetNext()) {
                if ($a['ACTIVE_FROM']) {
                    $date = $a['ACTIVE_FROM'];
                } else {
                    $date = $a['DATE_CREATE'];
                }
            }
        }
        $set_prop = array("SORT_DATE" => $date);
        \CIBlockElement::SetPropertyValuesEx($arFields['ID'], $arFields['IBLOCK_ID'], $set_prop);
    }
}


// Получаем все свойства для вывода
function all_iblock_props($iblock_id)
{

    \Bitrix\Main\Loader::includeModule('iblock');

    if (intval($iblock_id) > 0) {
        // Кешируем
        $obCache = new \CPHPCache();
        $cache_time = 30 * 24 * 60 * 60;
        $cache_id = 'all_iblock_props_' . $iblock_id;
        $cache_path = '/all_iblock_props/' . $iblock_id . '/';
        if ($obCache->InitCache($cache_time, $cache_id, $cache_path)) {
            $vars = $obCache->GetVars();
            extract($vars);
        } elseif ($obCache->StartDataCache()) {
            // Все свойства для показа
            $all_iblock_props = array();
            $k = -1;
            $properties = \CIBlockProperty::GetList(Array("sort" => "asc", "name" => "asc"), Array("ACTIVE" => "Y", "IBLOCK_ID" => $iblock_id));
            while ($prop = $properties->GetNext()) {
                if (
                    !in_array($prop['CODE'], $stop_props)
                    &&
                    !in_array($prop['CODE'], $all_iblock_props)
                ) {
                    $all_iblock_props[] = array('CODE' => $prop['CODE'], 'NAME' => $prop['NAME']);
                }
            }
            $obCache->EndDataCache(array('all_iblock_props' => $all_iblock_props));
        }
        return $all_iblock_props;
    } else {
        return false;
    }
}


function basketParams()
{

    $basketInfo = basketInfo();

    $arResult['hasCup'] = 'N';

    $arResult['orderSum'] = 0;
    $arResult['weight'] = 0;

    $max_length = false;
    $max_width = false;
    $total_height = 0;

    foreach ($basketInfo['arBasket'] as $basketItem) {

        $item_weight = false;
        $item_length = false;
        $item_width = false;
        $item_height = false;

        $product = $basketItem->product;

        // Род. раздел
        $product_section_id = tools\el::sections($product['ID'])[0]['ID'];

        // Цепочка разделов
        $section_chain = tools\section::chain($product_section_id);
        // переворачиваем цепочку
        $section_chain = array_reverse($section_chain);

        // Перебираем цепочку разделов
        foreach ($section_chain as $sect) {

            // Проверяем нет ли в корзине кружек
            if (substr_count(strtolower(trim($sect['NAME'])), 'кружки')) {
                $arResult['hasCup'] = 'Y';
            }

            // Инфо о разделе
            $section = tools\section::info($sect['ID']);

            if ($section['UF_VES'] > 0 && !$item_weight) {
                $item_weight = $section['UF_VES'];
            }
            if ($section['UF_DLINA'] > 0 && !$item_length) {
                $item_length = $section['UF_DLINA'];
            }
            if ($section['UF_SHIRINA'] > 0 && !$item_width) {
                $item_width = $section['UF_SHIRINA'];
            }
            if ($section['UF_VYSOTA'] > 0 && !$item_height) {
                $item_height = $section['UF_VYSOTA'];
            }

        }


        $item_weight = $item_weight ? $item_weight : 150;
        $item_length = $item_length ? $item_length : 28;
        $item_width = $item_width ? $item_width : 22;
        $item_height = $item_height ? $item_height : 3;


        /*if( !$max_length || $max_length < $item_length ){    $max_length = $item_length;    }
        if( !$max_width || $max_width < $item_width ){    $max_width = $item_width;    }
        $total_height += $item_height;*/


        $arResult['orderSum'] += $basketItem->discPrice * $basketItem->getQuantity();

        // граммы
        $arResult['weight'] += $item_weight * $basketItem->getQuantity();
    }

    // итоговые габариты
    $arResult['length'] = 28;
    $arResult['width'] = 22;
    $arResult['height'] = 3;

    return $arResult;
}


// Получение текущей корзины
function basketInfo($coupon = false){

    \Bitrix\Main\Loader::includeModule('sale');

    $basket_cnt = 0;
    $basket_sum = 0;
    $basket_disc_sum = 0;
    $arBasket = array();
    global $USER;

    $cupons = array();
    if (strlen($coupon) > 0) {
        $cupons = array($coupon);
    }

    // Получаем корзину
    $basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), \Bitrix\Main\Context::getCurrent()->getSite());
    foreach ($basket as $basketItem) {

        // Инфо об элементе
        $el = tools\el::info($basketItem->getField('PRODUCT_ID'));
        if (intval($el['ID']) > 0) {

            $basketItem->el = $el;

            // Проверка Товар/ТП
            $result = CCatalogSku::GetProductInfo($el['ID']);
            $isSKU = $result['ID'];
            $basketItem->isSKU = $isSKU;
            if ($isSKU) {
                $product = tools\el::info($isSKU);
                $basketItem->product = $product;
            } else {
                $product = $el;
                $basketItem->product = $product;
            }

            $basketItem->product_name = $basketItem->product['NAME'];
            if (intval($basketItem->product['PROPERTY_PRINT_ID_VALUE']) > 0) {
                $print = tools\el::info($basketItem->product['PROPERTY_PRINT_ID_VALUE']);
                if (intval($print['ID']) > 0) {
                    $basketItem->product_name = $print['NAME'];
                }
            }

            // Определяем цену со скидкой
            $price = $el['CATALOG_PRICE_1'];
            $basketItem->price = $price;
            $price_id = $el['CATALOG_PRICE_ID_1'];
            $price_currency = $el['CATALOG_CURRENCY_1'];
            $arDiscounts = \CCatalogDiscount::GetDiscountByPrice($price_id, $USER->GetUserGroupArray(), "N", SITE_ID, $cupons);
            $discountPrice = \CCatalogProduct::CountPriceWithDiscount($price, $price_currency, $arDiscounts);
            $discPrice = round($discountPrice, 0);
            $basketItem->discPrice = $discPrice;

            $basket_sum = $basket_sum + $price * $basketItem->getField('QUANTITY');
            $basket_disc_sum = $basket_disc_sum + $discPrice * $basketItem->getField('QUANTITY');
            $arBasket[] = $basketItem;

            $basket_cnt = $basket_cnt + $basketItem->getField('QUANTITY');

        } else {
            $basketItem->delete();
            $basket->save();
        }

    }
    $basket_cnt = round($basket_cnt, 0);
    $basket_sum = round($basket_sum, 2);
    $basket_disc_sum = round($basket_disc_sum, 2);

    return array(
        'basket' => $basket,
        'basket_cnt' => $basket_cnt,
        'basket_sum' => $basket_sum,
        'basket_disc_sum' => $basket_disc_sum,
        'discount' => $basket_sum - $basket_disc_sum,
        'arBasket' => $arBasket
    );
}


// Автозаполнение инфы по пользователям
function auto_update_user_info($user_id, $arFields)
{
    // Инфо по пользователю
    $arUser = Array();
    $q = \CUser::GetByID($user_id);
    $arUser = $q->GetNext();
    // Поля для заполнения, если не заданы
    $fields_to_change = array('NAME', 'LAST_NAME', 'PERSONAL_PHONE', 'WORK_COMPANY', 'UF_ADDRESS', 'UF_REKVIZITY');
    $fields = array();
    foreach ($fields_to_change as $field_code) {
        if (!$arUser[$field_code]) {
            $fields[$field_code] = $arFields[$field_code];
        }
    }
    // Если какие-то данные не определены
    if (count($fields) > 0) {
        // Заносим в профиль пользователя
        $user = new \CUser;
        $res = $user->Update($user_id, $fields);
    }
}


// Проверка "начинается с ..."
function string_begins_with($needle, $haystack)
{
    return (substr($haystack, 0, strlen($needle)) == $needle);
}

// РАССТАНОВКА ТОВАРОВ В ПОИСКЕ
function elements_placement($term, $elements)
{
    $first_elements = array();
    $last_elements = array();
    foreach ($elements as $el_id) {
        $el = tools\el::info($el_id);
        if (intval($el['ID']) > 0) {
            if (string_begins_with(strtolower(trim($term)), strtolower(trim($el['NAME'])))) {
                $first_elements[] = $el_id;
            } else {
                $last_elements[] = $el_id;
            }
        }
    }
    $new_elements = array_merge($first_elements, $last_elements);
    return $new_elements;
}


function get_object_field_value($field_name, $object)
{
    $array = (array)$object;
    foreach ($array as $key => $value) {
        if (substr_count($key, $field_name) != 0) {
            $clear_key_name = substr($key, strpos($key, $field_name), strlen($key));
            if ($clear_key_name == $field_name) {
                $field_value = $value;
            }
        }
    }
    return $field_value;
}


function subscribe_user($email)
{
    // Подписываем  на рассылку
    \Bitrix\Main\Loader::includeModule("subscribe");
    $dbSubscr = \CSubscription::GetByEmail($email);
    if ($dbSubscr->SelectedRowsCount() == 0) {
        $subscr = new CSubscription;
        $array = Array(
            "USER_ID" => false,
            "FORMAT" => "html",
            "EMAIL" => $email,
            "ACTIVE" => "Y",
            "CONFIRMED" => "Y",
            "RUB_ID" => Array(1),
            "SEND_CONFIRM" => "N"
        );
        $subscr = new \CSubscription;
        $subscr->Add($array);
    }
}


// Формирование сессии по просмотренным товарам
function add_to_viewed_session($el_id)
{
    $session_name = 'viewed_products';
    if (!$_SESSION[$session_name]) {
        $_SESSION[$session_name] = array();
    }
    if (
        intval($el_id) > 0
        &&
        !in_array($el_id, $_SESSION[$session_name])
    ) {
        $_SESSION[$session_name][] = $el_id;
        $ids = array_reverse($_SESSION[$session_name]);
        array_splice($ids, 10);
        $_SESSION[$session_name] = array_reverse($ids);
    }
    return count($_SESSION[$session_name]);
}


// Транслит текста
function translit($s)
{
    $s = (string)$s; // преобразуем в строковое значение
    $s = strip_tags($s); // убираем HTML-теги
    $s = str_replace(array("\n", "\r"), " ", $s); // убираем перевод каретки
    $s = preg_replace("/\s+/", ' ', $s); // удаляем повторяющие пробелы
    $s = trim($s); // убираем пробелы в начале и конце строки
    $s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s); // переводим строку в нижний регистр (иногда надо задать локаль)
    $s = strtr($s, array('а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'e', 'ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shch', 'ы' => 'y', 'э' => 'e', 'ю' => 'yu', 'я' => 'ya', 'ъ' => '', 'ь' => ''));
    $s = preg_replace("/[^0-9a-z-_ ]/i", "", $s); // очищаем строку от недопустимых символов
    $s = str_replace(" ", "_", $s); // заменяем пробелы знаком _
    return $s; // возвращаем результат
}


// Обрезка текста
function obrezka($str, $limit, $end_simvol)
{
    $str = strip_tags($str);
    $words = explode(' ', $str); // создаём из строки массив слов
    if ($limit < 1 || sizeof($words) <= $limit) { // если лимит указан не верно или количество слов меньше лимита, то возвращаем исходную строку
        return $str;
    }
    $words = array_slice($words, 0, $limit); // укорачиваем массив до нужной длины
    $out = implode(' ', $words);
    return $out . $end_simvol; //возвращаем строку + символ/строка завершения
}


// Получение массива из JSON
function json_to_array($json_string)
{
    $json_array = array();
    $error = false;
    if ($json_string && strlen($json_string) > 0) {
        //$json = htmlspecialchars_decode($json_string);
        $json = $json_string;

        if (strlen($json) > 0) {
            $json_array = json_decode($json, true);

            if (is_null($json_array)) {
                switch (json_last_error_msg()) {
                    case JSON_ERROR_DEPTH:
                        $error = 'Достигнута максимальная глубина стека';
                        break;
                    case JSON_ERROR_STATE_MISMATCH:
                        $error = 'Некорректные разряды или не совпадение режимов';
                        break;
                    case JSON_ERROR_CTRL_CHAR:
                        $error = 'Некорректный управляющий символ';
                        break;
                    case JSON_ERROR_SYNTAX:
                        $error = 'Синтаксическая ошибка, не корректный JSON';
                        break;
                    case JSON_ERROR_UTF8:
                        $error = 'Некорректные символы UTF-8, возможно неверная кодировка';
                        break;
                    case JSON_ERROR_RECURSION:
                        $error = 'Одна или несколько зацикленных ссылок в кодируемом значении';
                        break;
                    case JSON_ERROR_INF_OR_NAN:
                        $error = 'Одно или несколько значений NAN или INF в кодируемом значении';
                        break;
                    case JSON_ERROR_UNSUPPORTED_TYPE:
                        $error = 'Передано значение с неподдерживаемым типом';
                        break;
                    default:
                        $error = 'Неизвестная ошибка';
                        break;
                }
            }

        }
    }
    return $error ? $error : $json_array;
}


// Многомерный массив в строку GET-параметров
function array_to_get_string($array)
{
    $get_string = '?';
    $cnt = 0;
    foreach ($array as $key_1 => $value_1) {
        if (is_array($value_1) && count($value_1) > 0) {
            foreach ($value_1 as $key_2 => $value_2) {
                if (is_array($value_2) && count($value_2) > 0) {
                    foreach ($value_2 as $key_3 => $value_3) {
                        if (is_array($value_3) && count($value_3) > 0) {
                            foreach ($value_3 as $key_4 => $value_4) {
                                if (is_array($value_4) && count($value_4) > 0) {
                                } else {
                                    $cnt++;
                                    $get_string .= (($cnt == 1) ? '' : '&') . $key_1 . '[' . $key_2 . '][' . $key_3 . '][' . $key_4 . ']=' . $value_4;
                                }
                            }
                        } else {
                            $cnt++;
                            $get_string .= (($cnt == 1) ? '' : '&') . $key_1 . '[' . $key_2 . '][' . $key_3 . ']=' . $value_3;
                        }
                    }
                } else {
                    $cnt++;
                    $get_string .= (($cnt == 1) ? '' : '&') . $key_1 . '[' . $key_2 . ']=' . $value_2;
                }
            }
        } else {
            $cnt++;
            $get_string .= (($cnt == 1) ? '' : '&') . $key_1 . '=' . $value_1;
        }
    }
    return $get_string;
}


// Удаление пробелов по бокам значений массива
function trim_array_values($array)
{
    $ret_arr = array();
    foreach ($array as $val) {
        if (!empty($val)) {
            $ret_arr[] = trim($val);
        }
    }
    return $ret_arr;
}


// arURI
function arURI($url = false)
{
    if (!$url) {
        $url = $_SERVER["REQUEST_URI"];
    }
    return explode("/", $url);
}


// pureURL
function pureURL($url = false)
{
    if (!$url) {
        $url = $_SERVER["REQUEST_URI"];
    }
    $pureURL = $url;
    if (substr_count($pureURL, "?")) {
        $pos = strpos($pureURL, "?");
        $pureURL = substr($pureURL, 0, $pos);
    }
    return $pureURL;
}


// pre
function pre($varriable)
{
    echo "<pre>";
    print_r($varriable);
    echo "</pre>";
}


// pfCnt
function pfCnt($n, $form1, $form2, $form5)
{ // pfCnt($productCount, "товар", "товара", "товаров")
    $n = abs($n) % 100;
    $n1 = $n % 10;
    if ($n > 10 && $n < 20) return $form5;
    if ($n1 > 1 && $n1 < 5) return $form2;
    if ($n1 == 1) return $form1;
    return $form5;
}


// addToRequestURI
function addToRequestURI($pm, $val)
{
    if (substr_count($_SERVER["REQUEST_URI"], "?")) {
        $arURI = explode("?", $_SERVER["REQUEST_URI"]);
        $pureURI = $arURI[0];
        $arRequests = explode("&", $arURI[1]);
        $arNewRequests = Array();
        foreach ($arRequests as $request) {
            $arTMP = explode("=", $request);
            if ($arTMP[0] != $pm) {
                $arNewRequests[] = $request;
            }
        }
        $arNewRequests[] = $pm . "=" . $val;
        return $pureURI . "?" . implode("&", $arNewRequests);
    } else {
        return $_SERVER["REQUEST_URI"] . "?" . $pm . "=" . $val;
    }
}


// addToRequestURIm
function addToRequestURIm($pm, $val)
{
    if (substr_count($_SERVER["REQUEST_URI"], "?")) {
        $arURI = explode("?", $_SERVER["REQUEST_URI"]);
        $pureURI = $arURI[0];
        $arSORT = Array(
            0 => "sort_code=asc",
            1 => "sort_code=desc",
            2 => "sort_producer=asc",
            3 => "sort_producer=desc",
            4 => "sort_cost=asc",
            5 => "sort_cost=desc"
        );
        $arURI[1] = str_replace($arSORT, "", $arURI[1]);
        $arURI[1] = str_replace("&&", "&", $arURI[1]);
        $arRequests = explode("&", $arURI[1]);
        $arNewRequests = Array();
        foreach ($arRequests as $request) {
            $arTMP = explode("=", $request);
            if ($arTMP[0] != $pm) {
                $arNewRequests[] = $request;
            }
        }
        $arNewRequests[] = $pm . "=" . $val;
        $result = $pureURI . "?" . implode("&", $arNewRequests);
        $result = str_replace("&&", "&", $result);
        return $result;
    } else {
        $result = $_SERVER["REQUEST_URI"] . "?" . $pm . "=" . $val;
        $result = str_replace("&&", "&", $result);
        return $result;
    }
}


// isMAIN
function isMain()
{
    $arURI = explode("/", $_SERVER["REQUEST_URI"]);
    if (substr_count($arURI[1], "?")) {
        $pos = strpos($arURI[1], "?");
        $arURI[1] = substr($arURI[1], 0, $pos);
    }
    if (substr_count($arURI[1], "index.php")) {
        return true;
    } elseif (count($arURI) > 1 && strlen(trim($arURI[1])) == 0) {
        return true;
    } else {

        $genderTypes = project\gender_type::list();
        foreach ($genderTypes as $genderType) {
            if (pureURL() == '/' . $genderType['CODE'] . '_home/') {
                return true;
            }
        }
        return false;
    }
}


// editDATE
function editDATE($DATA)
{
    $DATA = substr($DATA, 0, 10);
    $MES = array(
        "01" => "Января",
        "02" => "Февраля",
        "03" => "Марта",
        "04" => "Апреля",
        "05" => "Мая",
        "06" => "Июня",
        "07" => "Июля",
        "08" => "Августа",
        "09" => "Сентября",
        "10" => "Октября",
        "11" => "Ноября",
        "12" => "Декабря"
    );
    $arData = explode(".", $DATA);
    $d = ($arData[0] < 10) ? substr($arData[0], 1) : $arData[0];
    $newData = $d . " " . $MES[$arData[1]] . " " . $arData[2];
    return $newData;
}


// editDATE2
function editDATE2($DATA)
{
    $DATA = substr($DATA, 0, 10);
    $MES = array(
        "01" => "Января",
        "02" => "Февраля",
        "03" => "Марта",
        "04" => "Апреля",
        "05" => "Мая",
        "06" => "Июня",
        "07" => "Июля",
        "08" => "Августа",
        "09" => "Сентября",
        "10" => "Октября",
        "11" => "Ноября",
        "12" => "Декабря"
    );
    $arData = explode(".", $DATA);
    $d = ($arData[0] < 10) ? substr($arData[0], 1) : $arData[0];
    $newData = $d . " " . $MES[$arData[1]];
    return $newData;
}


// Текущий день недели
function week_day()
{
    $day = date('w', strtotime(date('d.m.Y')));
    if ($day == 1) {
        $week_day = 1;
    }
    if ($day == 2) {
        $week_day = 2;
    }
    if ($day == 3) {
        $week_day = 3;
    }
    if ($day == 4) {
        $week_day = 4;
    }
    if ($day == 5) {
        $week_day = 5;
    }
    if ($day == 6) {
        $week_day = 6;
    }
    if ($day == 0) {
        $week_day = 7;
    }
    return $week_day;
}


// Проверка времени на признак "до $time"
function check_time($time)
{
    $datetime1 = date_create(ConvertDateTime(date('d.m.Y H:i:s'), "YYYY-MM-DD HH:MI:SS", "ru"));
    $datetime2 = date_create(ConvertDateTime(date('d.m.Y') . ' ' . $time, "YYYY-MM-DD HH:MI:SS", "ru"));
    $interval = date_diff($datetime1, $datetime2);
    $invert = $datetime1->diff($datetime2)->invert;
    if ($invert == 0) {
        return true;
    } else {
        return false;
    }
}


function del_quots($name)
{
    $new_name = $name;
    $quot_exist = true;
    while ($quot_exist == true) {
        if (substr_count($new_name, 'quot;') || substr_count($new_name, 'amp;')) {
            $new_name = htmlspecialchars_decode($new_name);
        } else {
            $quot_exist = false;
        }
    }
    return $new_name;
}


// месяц по номеру
function getMonthName($number, $nominative_case)
{
    $months = array(
        '01' => array('января', 'январь'),
        '02' => array('февраля', 'февраль'),
        '03' => array('марта', 'март'),
        '04' => array('апреля', 'апрель'),
        '05' => array('мая', 'май'),
        '06' => array('июня', 'июнь'),
        '07' => array('июля', 'июль'),
        '08' => array('августа', 'август'),
        '09' => array('сентября', 'сентябрь'),
        '10' => array('октября', 'октябрь'),
        '11' => array('ноября', 'ноябрь'),
        '12' => array('декабря', 'декабрь')
    );
    return $months[$number][$nominative_case ? 1 : 0];
}


// Очистка массива от полей с символом "~"
function clean_array($array = [], $not_unset_fields = [])
{
    if (is_array($array) && count($array) > 0) {
        // Стоп-поля
        $not_unset_array = array('NAME', 'PREVIEW_TEXT', 'DETAIL_TEXT', 'IBLOCK_SECTION_ID', 'DESCRIPTION');
        // Добавляем стоп-поля при наличии
        if ($not_unset_fields && is_array($not_unset_fields) && count($not_unset_fields) > 0) {
            foreach ($not_unset_fields as $field) {
                if (!in_array($field, $not_unset_array)) {
                    $not_unset_array[] = $field;
                }
            }
        }
        // Перебор полей
        foreach ($array as $key => $value) {
            if (strlen($key) > 1) {
                $key_first_symbol = substr($key, 0, 1);
                $field = substr($key, 1, strlen($key));
                if ($key_first_symbol == '~' && !in_array($field, $not_unset_array)) {
                    unset($array[$key]);
                }
            }
        }
    }
    return $array;
}


// rIMG
/**
 * @param bool $image
 * @param int $mode
 * @param string $width
 * @param string $height
 * @param $watermark_path
 * @return bool
 */
function rIMG($image = false, $mode = 4, $width = '', $height = '', $watermark_path = '')
{
    if (!$image) {
        $image = false;
    }
    if (!$mode) {
        $mode = "4";
    }
    if (!$width) {
        $width = "";
    }
    if (!$height) {
        $height = "";
    }

    if (\Bitrix\Main\Loader::includeModule("imager") && $image) {
        global $APPLICATION;
        $image = $APPLICATION->IncludeComponent("soulstream:imager", "", Array(
            "MODE" => $mode,
            "RETURN" => "src",
            "RESIZE_SMALL" => "Y",
            "IMAGE" => is_numeric($arParams['IMAGE']) ? \CFile::GetPath($image) : $image,
            "FILE_NAME" => "",
            "WIDTH" => $width,
            "HEIGHT" => $height,
            "SAVE_DIR" => "",
            "BG" => "#FFFFFF",
            "QUALITY" => "100",
            "DEBUG" => "N",
            "FILTERTYPE" => "",
            "ADD_CORNER" => "N",
            "CORNER_PATH" => "",
            "ADD_WATERMARK" => (strlen($watermark_path) > 0) ? "Y" : "N",
            "WATERMARK_PATH" => (strlen($watermark_path) > 0) ? $watermark_path : "",
            "WATERMARK_POSITION" => (strlen($watermark_path) > 0) ? "mc" : "",
            "CACHE_IMAGE" => "Y",
            "ADD_TEXT" => "N",
            "TEXT" => "",
            "TEXT_SIZE" => "",
            "TEXT_COLOR" => "",
            "TEXT_Y" => "",
            "TEXT_X" => "",
            "TEXT_POSITION" => "",
            "TEXT_ANGLE" => "",
            "FONT_PATH" => ""
        ),
            false, array("HIDE_ICONS" => "Y")
        );
        return $image;
    }
}


// rIMGG
function rIMGG($image, $mode, $width, $height)
{
    $max_width = $width;
    $max_height = $height;
    $koef = ($width / $height);
    if ($koef <= 1) {
        $width = $max_height * $koef;
        $height = $max_height;
    }
    if ($koef > 1) {
        $width = $max_width;
        $height = $max_width / $koef;
    }
    if ($mode == 4) {
        $image_array = \CFile::ResizeImageGet($image, array('width' => $width, 'height' => $height), BX_RESIZE_IMAGE_PROPORTIONAL, true);
    }
    if ($mode == 5 || !$mode) {
        $image_array = \CFile::ResizeImageGet($image, array('width' => $width, 'height' => $height), BX_RESIZE_IMAGE_EXACT, true);
    }
    $image = $image_array['src'];
    return $image;
}


// получение значения параметра $_POST
function param_post($param)
{
    if (strlen($param) > 0) {
        $request = Application::getInstance()->getContext()->getRequest();
        $value = $request->getPost($param);
        return $value;
    } else {
        return false;
    }
}

// получение значения параметра $_GET
function param_get($param)
{
    if (strlen($param) > 0) {
        $request = Application::getInstance()->getContext()->getRequest();
        $value = $request->getQuery($param);
        return $value;
    } else {
        return false;
    }
}


// Обновление UrlRewrite
function UpdateUrlRewrite($iblock_id, $urlRewriterID, $path)
{
    \Bitrix\Main\Loader::includeModule("iblock");
    CUrlRewriter::Delete(Array("ID" => $urlRewriterID));
    $sections = \CIBlockSection::GetList(Array(), Array("IBLOCK_ID" => $iblock_id, "ACTIVE" => "Y", "!CODE" => false, "INCLUDE_SUBSECTIONS" => "Y", "DEPTH_LEVEl" => 1), false);
    while ($section = $sections->GetNext()) {
        $fields = Array(
            "CONDITION" => "#^/" . $section["CODE"] . "/#",
            "PATH" => $path,
            "ID" => $urlRewriterID
        );
        \CUrlRewriter::Add($fields);
    }
}


// функция scandir
function myscandir($dir)
{
    $list = scandir($dir);
    unset($list[0], $list[1]);
    return array_values($list);
}

// Очистка папки от файлов и подпапок
function clear_dir($dir)
{
    $list = myscandir($dir);
    foreach ($list as $file) {
        if (is_dir($dir . $file)) {
            clear_dir($dir . $file . '/');
            rmdir($dir . $file);
        } else {
            unlink($dir . $file);
        }
    }
}

// Пример:   clear_dir($_SERVER["DOCUMENT_ROOT"]."/upload/imager/");


function addLeftBorder($lb)
{
    if ($lb > 1) {
        $lb = $lb - 1;
    }
    return $lb;
}

function addRightBorder($rb, $max)
{
    if ($rb < $max) {
        $rb = $rb + 1;
    }
    return $rb;
}


?>
