<? \Bitrix\Main\Loader::includeModule('sale');
use \Bitrix\Main;
use \Bitrix\Sale;
use \Bitrix\Sale\DiscountCouponsManager;


class my_coupon {


    // Проверка существование купона
    static function check($coupon){
        DiscountCouponsManager::init();
        $res = DiscountCouponsManager::getData($coupon);
        return (intval($res['ID']) > 0 && $res['ACTIVE'] != 'N');
    }


    // Установка купона
    static function set($coupon){
        global $APPLICATION;
        $APPLICATION->set_cookie('coupon', $coupon, time()+2592000);
        DiscountCouponsManager::init();
        DiscountCouponsManager::add($coupon);
    }


    // Удаления купона из куки
    static function clear(){
        global $APPLICATION;
        $APPLICATION->set_cookie('coupon', '', time()-2592000);
        DiscountCouponsManager::init();
        $arCoupons = DiscountCouponsManager::get(true, array(), true, true);
        foreach($arCoupons as $coupon => $ar){
            DiscountCouponsManager::delete($coupon);
        }
    }


    // Запрос активного купона
    static function get_active_coupon(){
        global $APPLICATION;
        $coupon = $APPLICATION->get_cookie('coupon');
        if ($coupon){
            $coupon_exist = static::check($coupon);
            if ( !$coupon_exist ){
                $coupon = false;
                global $APPLICATION;
                $APPLICATION->set_cookie('coupon', '', time()-2592000);
                DiscountCouponsManager::init();
                DiscountCouponsManager::delete($coupon);
            } else {
                DiscountCouponsManager::init();
                DiscountCouponsManager::add($coupon);
            }
        }
        return $coupon;
    }


    // Получение ID купона
    static function get_coupon_id($coupon){
        DiscountCouponsManager::init();
        $arCoupons = DiscountCouponsManager::get(true, array(), true, true);
        foreach($arCoupons as $c => $ar){
            if( $c == $coupon ){
                return $ar['ID'];
            }
        }
        return false;
    }


    // Деактивация купона в БД
    static function deactivate($coupon){
        \CCatalogDiscountCoupon::Update(static::get_coupon_id($coupon), array('ACTIVE' => 'N'));
    }


    // Удаление купона в БД
    static function delete($coupon){
        \CCatalogDiscountCoupon::Delete(static::get_coupon_id($coupon));
    }
	
	
} ?>