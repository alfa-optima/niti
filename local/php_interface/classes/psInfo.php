<?

class psInfo {
	
	
	
	static function getList(){
		$list = array();
		$db_ptype = \CSalePaySystem::GetList(
			Array(
				"SORT"=>"ASC",
				"PSA_NAME"=>"ASC"
			),
			Array(
				"ACTIVE"=>"Y"
			),
			false,
			false,
			array('ID', 'NAME', 'LOGOTIP', 'DESCRIPTION')
		);
		while ($ps = $db_ptype->GetNext()){
			$list[] = array(
				'id' => $ps['ID'],
				'name' => $ps['NAME'],
			);
		}
		return json_encode($list);
	}

	
	
}