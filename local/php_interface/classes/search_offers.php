<? 
class search_offers {



	const PAGE_ELEMENT_COUNT = 30;

	
	

	static function searchByQ($q){
		\Bitrix\Main\Loader::includeModule("search");
		$obSearch = new \CSearch;
		$ids = array();
		$obSearch->Search(
			array(
				"QUERY" => $q,
				"SITE_ID" => LANG,
				array(
					array(
						"=MODULE_ID" => "iblock",
						"!ITEM_ID" => "S%",
						"PARAM2" => project\site::SKU_IBLOCK_ID
					)
				)
			),
			array("TITLE_RANK"=>"DESC"),
			array('STEMMING' => true)
		);
		if ($obSearch->errorno != 0){} else {
			while($item = $obSearch->GetNext()){
				$ids[] = $item['ITEM_ID'];
			}
		}
		if ( count($ids) == 0 ){
			$obSearch = new \CSearch;
			$obSearch->Search(
				array(
					"QUERY" => $q,
					"SITE_ID" => LANG,
					array(
						array(
							"=MODULE_ID" => "iblock",
							"!ITEM_ID" => "S%",
							"PARAM2" => project\site::SKU_IBLOCK_ID
						)
					)
				),
				array("TITLE_RANK"=>"DESC"),
				array('STEMMING' => false)
			);
			if ($obSearch->errorno != 0){} else {
				while($item = $obSearch->GetNext()){
					$ids[] = $item['ITEM_ID'];
				}
			}	
		}
		return $ids;
	}


	
	

	static function request($get){
		
		$_GET = $get;
		
		// Проверка ключа
		if(
			!$_GET['key']
			||
			$_GET['key'] != project\site::API_ACCESS_KEY
		){
			
			$arResponse['error'] = 'Ошибка авторизации';
			$json = json_encode($arResponse);
			
		} else {
			
			// Формируем фильтр
			if( intval($_GET['id']) > 0 ){
				$GLOBALS["search_offers_Filter"]['ID'] = $_GET['id'];
			}
			
			if( strlen($_GET['q']) > 0 ){
				$ids = static::searchByQ($_GET['q']);
				if( count($ids) > 0 ){
					$GLOBALS["search_offers_Filter"]['ID'] = $ids;
				} else {
					$GLOBALS["search_offers_Filter"]['ID'] = 0;
				}
			}
			
			// Запрос
			ob_start(); 
				global $APPLICATION;
				$APPLICATION->IncludeComponent( "bitrix:catalog.section", "search_offers",
					Array(
						"IBLOCK_TYPE" => "content",
						"IBLOCK_ID" => project\site::SKU_IBLOCK_ID,
						"SECTION_USER_FIELDS" => array(),
						"ELEMENT_SORT_FIELD" => "NAME",
						"ELEMENT_SORT_ORDER" => "ASC",
						"ELEMENT_SORT_FIELD2" => "NAME",
						"ELEMENT_SORT_ORDER2" => "ASC",
						"FILTER_NAME" => "search_offers_Filter",
						"HIDE_NOT_AVAILABLE" => "N",
						"PAGE_ELEMENT_COUNT" => static::PAGE_ELEMENT_COUNT,
						"LINE_ELEMENT_COUNT" => "3",
						"PROPERTY_CODE" => array('COLOR', 'CML2_LINK'),
						"TEMPLATE_THEME" => "",
						"PRODUCT_SUBSCRIPTION" => "N",
						"SHOW_DISCOUNT_PERCENT" => "N",
						"SHOW_OLD_PRICE" => "N",
						"MESS_BTN_BUY" => "Купить",
						"MESS_BTN_ADD_TO_BASKET" => "В корзину",
						"MESS_BTN_SUBSCRIBE" => "Подписаться",
						"MESS_BTN_DETAIL" => "Подробнее",
						"MESS_NOT_AVAILABLE" => "Нет в наличии",
						"SECTION_URL" => "",
						"DETAIL_URL" => "",
						"SECTION_ID_VARIABLE" => "SECTION_ID",
						"AJAX_MODE" => "N",
						"AJAX_OPTION_JUMP" => "N",
						"AJAX_OPTION_STYLE" => "Y",
						"AJAX_OPTION_HISTORY" => "N",
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => "300",
						"CACHE_GROUPS" => "Y",
						"SET_META_KEYWORDS" => "N",
						"META_KEYWORDS" => "",
						"SET_META_DESCRIPTION" => "N",
						"META_DESCRIPTION" => "",
						"BROWSER_TITLE" => "-",
						"ADD_SECTIONS_CHAIN" => "N",
						"DISPLAY_COMPARE" => "N",
						"SET_TITLE" => "N",
						"SET_STATUS_404" => "N",
						"CACHE_FILTER" => "Y",
						"PRICE_CODE" => array('BASE'),
						"USE_PRICE_COUNT" => "N",
						"SHOW_PRICE_COUNT" => "1",
						"PRICE_VAT_INCLUDE" => "Y",
						"CONVERT_CURRENCY" => "N",
						"BASKET_URL" => "/personal/basket.php",
						"ACTION_VARIABLE" => "action",
						"PRODUCT_ID_VARIABLE" => "id",
						"USE_PRODUCT_QUANTITY" => "N",
						"ADD_PROPERTIES_TO_BASKET" => "Y",
						"PRODUCT_PROPS_VARIABLE" => "prop",
						"PARTIAL_PRODUCT_PROPERTIES" => "N",
						"PRODUCT_PROPERTIES" => "",
						"PAGER_TEMPLATE" => "",
						"DISPLAY_TOP_PAGER" => "N",
						"DISPLAY_BOTTOM_PAGER" => "Y",
						"PAGER_TITLE" => "Товары",
						"PAGER_SHOW_ALWAYS" => "N",
						"PAGER_DESC_NUMBERING" => "N",
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
						"PAGER_SHOW_ALL" => "Y",
						"ADD_PICT_PROP" => "-",
						"LABEL_PROP" => "-",
						'INCLUDE_SUBSECTIONS' => "Y",
						'SHOW_ALL_WO_SECTION' => "Y"
					)
				);
				$json = ob_get_contents();
			ob_end_clean();
			
		}
		
		return $json;
		
	}

	
	


}