<? use GuzzleHttp\Client;
use \Bitrix\Sale;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

class orderExport {
	
	
	const BASE_URL = 'http://api-v2.niti-niti.ru/orders/';
	const LOGIN = 'sf3hs67hdf6jdf3y5h';
	const PASSWORD = 'g5fghjdsj890hdwvld';
	
	
	
	
	
	// Отправка заказа
	static function getByID( $order_id ){
		if( intval($order_id) > 0 ){
			
			$client = new Client(array( 'timeout' => 5 ));
			$fields = array(
				'login' => static::LOGIN,
				'pass' => static::PASSWORD,
				"action" => "get-order-by-id"
			);
			$json_body = json_encode($fields);
			
			try {
			
				// Создаём запрос
				$response = $client->request(
					'POST',  static::BASE_URL.$order_id.'/',  
					array(
						'body' => $json_body
					)
				);
				
				if ( $response->getStatusCode() == '200' ){
					
					$body = $response->getBody();
					$content = $body->getContents();

					$res = json_to_array($content);
					
					return $res;

				}
				
			} catch (\Exception $e) {
				
				tools\logger::addError($e->getMessage());
				
				//echo $e->getMessage();
				
			}
			
		}
	}
	
	
	
		
	
	// Отправка заказа
	static function send( $order_id ){
		
		if( intval($order_id) > 0 ){

			$order = Sale\Order::load($order_id);
			
			$propertyCollection = $order->getPropertyCollection();
			$prop = $propertyCollection->getItemByOrderPropertyId(1);
			$name = $prop->getValue();
			$prop = $propertyCollection->getItemByOrderPropertyId(2);
			$last_name = $prop->getValue();
			$prop = $propertyCollection->getItemByOrderPropertyId(3);
			$email = $prop->getValue();
			$prop = $propertyCollection->getItemByOrderPropertyId(4);
			$phone = $prop->getValue();
			$address = static::getFLaddress($propertyCollection);
			$prop = $propertyCollection->getItemByOrderPropertyId(32);
			$comment = $prop->getValue();
			$crm_prop = $propertyCollection->getItemByOrderPropertyId(38);
			$agent_prop = $propertyCollection->getItemByOrderPropertyId(36);
			
			// Сначала проверим существует ли заказ в CRM
			$res = static::getByID( $order_id );
			
			// Если не существует, отправляем данные
			if( $res['type'] == 'order-not-exist' || !$res ){
				
				$arBasket = array();
				$basket = Sale\Basket::loadItemsForOrder($order);
				foreach ($basket as $basketItem){
					for( $n = 1; $n <= $basketItem->getQuantity(); $n++ ){
						$arBasket[] = array(
							'id' => $basketItem->getProductId(),
							'price' => $basketItem->getFinalPrice()
						);
					}
				}
				
				$paymentIds = $order->getPaymentSystemId();
				$deliveryIds = $order->getDeliverySystemId();
				
				$client = new Client(array( 'timeout' => 5 ));
				$fields = array(
					'login' => static::LOGIN,
					'pass' => static::PASSWORD,
					"action" => "import-order",
					"params" => array(
						"order_id" => $order_id,
						"personal" => array(
						  "fio" => ($last_name." ".$name),
						  "email" => $email,
						  "phone_number" => $phone,
						  "address" => $address,
						  "comment" => $comment,
						  "company_name" => $company_name,
						  "legal_address" => $yur_address,
						  "itn" => $inn,
						  "name_bank" => $bank,
						  "bic" => $bik,
						  "current_account" => $rs,
						  "correspondent_account" => $ks,
						),
						"amount" => $order->getPrice(),
						"date_created" => ConvertDateTime($order->getDateInsert(), "DD-MM-YYYY HH:MI:SS", "ru"),
						"id_stores" => 1,
						"discount_percent" => 0,
						"discount_currency" => 0,
						"products" => $arBasket,
						"delivery" => array(
							"id" => $deliveryIds[0],
							"price" => $order->getDeliveryPrice()
						),
						"payment" => array(
							"id" => $paymentIds[0]
						),
					)
				);
				
				$json_body = json_encode($fields);
				
				try {
				
					// Создаём запрос
					$response = $client->request(
						'POST',  static::BASE_URL.'create/',  
						array(
							'body' => $json_body
						)
					);
					
					if ( $response->getStatusCode() == '200' ){
						
						$body = $response->getBody();
						$content = $body->getContents();
						
						$res = json_to_array($content);
							
						// Успех
						if( $res['status'] == 'ok' && $res['result'] == 1 ){
							
							// Отметим в заказе отправку инфы в CRM
							$crm_prop->setValue("Y");
							if( intval($agent_prop->getValue()) ){
								\CAgent::Delete(intval($agent_prop->getValue()));
								$agent_prop->setValue('');
							}
							$order->save();
							
							return $res;
							
						// НЕ успех
						} else {
							
							if( is_array($res) ){
								tools\logger::addError('Ошибка отправки заказа в CRM '.json_encode($res));
							} else {
								tools\logger::addError('Ошибка отправки заказа в CRM '.$res);
							}
							
							// Создадим агента
							return static::addOrderAgent($order_id, $order, $propertyCollection);
						}

					}
					
				} catch (\Exception $e) {
					
					tools\logger::addError($e->getMessage());
					
					// Создадим агента
					return static::addOrderAgent($order_id, $order, $propertyCollection);
					
				}
			
			} else {
				
				$crm_prop->setValue("Y");
				if( intval($agent_prop->getValue()) ){
					\CAgent::Delete(intval($agent_prop->getValue()));
					$agent_prop->setValue('');
				}
				$order->save();

			}

		}
		
		return false;
		
	}

	
	
	
	// addOrderAgent
	static function addOrderAgent($order_id, $order, $propertyCollection){
		// Проверим создан ли уже агент к заказу
		$agent_prop = $propertyCollection->getItemByOrderPropertyId(36);
		// Если агент ещё не создан
		$agent_id = $agent_prop->getValue();
		if( intval($agent_id) > 0 ){
			\CAgent::Delete($agent_id);
		}
		// создадим агент для повторной попытки отправки инфы
		$agent_id = \CAgent::AddAgent(
			"orderExport::send(".$order_id.");",  // имя функции
			"",			// идентификатор модуля
			"Y",		// агент не критичен к кол-ву запусков
			120,			// интервал запуска
			"",			// дата первой проверки - текущее
			"Y",		// агент активен
			date("d.m.Y H:i:s", time()+120),	// дата первого запуска - текущее
			0			// сортировка
		);
		if( intval($agent_id) > 0 ){
			// отметим в заказе создание агента
			$agent_prop->setValue(intval($agent_id));
			$order->save();
			return "orderExport::send(".$order_id.");";
		}

		return false;
	}
	
	
	
	
	// Адрес ФЛ
	static function getFLaddress($propertyCollection){
		$address = array();
		// Индекс
		$prop = $propertyCollection->getItemByOrderPropertyId(7);
		$value = $prop->getValue();
		if( strlen($value) > 0 ){    $address[] = $value;    }
		// Город
		$prop = $propertyCollection->getItemByOrderPropertyId(6);
		$value = $prop->getValue();
		if( strlen($value) > 0 ){    $address[] = 'г.'.$value;    }
		// Улица
		$prop = $propertyCollection->getItemByOrderPropertyId(8);
		$value = $prop->getValue();
		if( strlen($value) > 0 ){    $address[] = 'ул.'.$value;    }
		// Дом
		$prop = $propertyCollection->getItemByOrderPropertyId(9);
		$value = $prop->getValue();
		if( strlen($value) > 0 ){    $address[] = 'д.'.$value;    }
		// Корпус
		$prop = $propertyCollection->getItemByOrderPropertyId(10);
		$value = $prop->getValue();
		if( strlen($value) > 0 ){    $address[] = 'корп.'.$value;    }
		// Квартира
		$prop = $propertyCollection->getItemByOrderPropertyId(12);
		$value = $prop->getValue();
		if( strlen($value) > 0 ){    $address[] = 'кв.'.$value;    }
		
		return implode(', ', $address);
	}
	
	
	
	
	
	// Адрес ФЛ
	static function getYURaddress($propertyCollection){
		$address = array();
		// Индекс
		$prop = $propertyCollection->getItemByOrderPropertyId(26);
		$value = $prop->getValue();
		if( strlen($value) > 0 ){    $address[] = $value;    }
		// Страна
		$prop = $propertyCollection->getItemByOrderPropertyId(24);
		$value = $prop->getValue();
		if( strlen($value) > 0 ){    $address[] = $value;    }
		// Город
		$prop = $propertyCollection->getItemByOrderPropertyId(25);
		$value = $prop->getValue();
		if( strlen($value) > 0 ){    $address[] = 'г.'.$value;    }
		// Улица
		$prop = $propertyCollection->getItemByOrderPropertyId(27);
		$value = $prop->getValue();
		if( strlen($value) > 0 ){    $address[] = 'ул.'.$value;    }
		// Дом
		$prop = $propertyCollection->getItemByOrderPropertyId(28);
		$value = $prop->getValue();
		if( strlen($value) > 0 ){    $address[] = 'д.'.$value;    }
		// Корпус
		$prop = $propertyCollection->getItemByOrderPropertyId(29);
		$value = $prop->getValue();
		if( strlen($value) > 0 ){    $address[] = 'корп.'.$value;    }
		// Подъезд
		$prop = $propertyCollection->getItemByOrderPropertyId(30);
		$value = $prop->getValue();
		if( strlen($value) > 0 ){    $address[] = 'подъезд '.$value;    }
		// Квартира
		$prop = $propertyCollection->getItemByOrderPropertyId(31);
		$value = $prop->getValue();
		if( strlen($value) > 0 ){    $address[] = 'кв.'.$value;    }
		
		return implode(', ', $address);
	}
	
	
	
	
	
}