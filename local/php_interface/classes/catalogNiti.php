<?



class catalogNiti {
	

	
	
	
	// Разделы каталога 1 уровня
	static function sections_1_level(){
		$sections = array();
		$obCache = new \CPHPCache();
		$cache_time = 600;
		$cache_id = 'catalog_sections_1_level';
		$cache_path = '/catalog_sections_1_level/';
		if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
			$vars = $obCache->GetVars();   extract($vars);
		} elseif($obCache->StartDataCache()){
			$dbSections = \CIBlockSection::GetList(
				Array("SORT"=>"ASC"),
				Array(
					"ACTIVE"=>"Y",
					"DEPTH_LEVEL" => 1,
					"IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID
				), false
			);
			while ($section = $dbSections->GetNext()){
				$sections[] = $section;
			}
		$obCache->EndDataCache( array('sections' => $sections) );
		}
		return $sections;
	}
	
	
	
	
	
	// Группы товаров (пол)
	static function genderTypes(){
		$arTypes = array();
		$obCache = new \CPHPCache();
		$cache_time = 600;
		$cache_id = 'genderTypes';
		$cache_path = '/genderTypes/';
		if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
			$vars = $obCache->GetVars();   extract($vars);
		} elseif($obCache->StartDataCache()){
			$types = \CUserFieldEnum::GetList( array(), array( "USER_FIELD_ID" => 52 ) );
			while( $type = $types->GetNext() ){
				if( $type['XML_ID'] == 'woman' ){
					$type['ALT_VALUE'] = 'Для неё';
				}
				if( $type['XML_ID'] == 'man' ){
					$type['ALT_VALUE'] = 'Для него';
				}
				$arTypes[$type['XML_ID']] = $type;
			}
		$obCache->EndDataCache( array('arTypes' => $arTypes) );
		}
		return $arTypes;
	}
	
	
	
	
	
	// Активная группа каталога
	static function activeGenderType(){
		if( !$_SESSION['gender_type'] ){  
			$genderTypes = static::genderTypes();
			$_SESSION['gender_type'] = $genderTypes['woman'];
		}
		return $_SESSION['gender_type'];
	}


	

		
	// Хиты на главной
	function getMainHits(){
		
		$genderTypes = static::genderTypes();
		
		$activeGenderSects = array();
		$dbSections = \CIBlockSection::GetList(
			Array("SORT"=>"ASC"),
			Array(
				"ACTIVE"=>"Y",
				"DEPTH_LEVEL" => 2,
				"IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
				"UF_GENDER_GROUP" => $_SESSION['gender_type']['ID']
			), false, array("UF_GENDER_GROUP")
		);
		while ($section = $dbSections->GetNext()){
			$activeGenderSects[] = $section['ID'];
		}
		sort($activeGenderSects);
		
		$allGenderSects = array();
		$dbSections = \CIBlockSection::GetList(
			Array("SORT"=>"ASC"),
			Array(
				"ACTIVE"=>"Y",
				"DEPTH_LEVEL" => 2,
				"IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
				"UF_GENDER_GROUP" => $genderTypes['all']['ID']
			), false, array("UF_GENDER_GROUP")
		);
		while ($section = $dbSections->GetNext()){
			$allGenderSects[] = $section['ID'];
		}
		sort($allGenderSects);
		
		$filter_sects = array_merge($activeGenderSects, $allGenderSects);
		$filter_sects = array_unique($filter_sects);
		
		// Кешируем
		$obCache = new \CPHPCache();
		$cache_time = 3600;
		$cache_id = 'main_hits_'.implode('_', $filter_sects);
		$cache_path = '/main_hits/'.implode('_', $filter_sects).'/';
		if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
			$vars = $obCache->GetVars();   extract($vars);
		} elseif($obCache->StartDataCache()){
			$main_hits = array();
			$filter = Array(
				"IBLOCK_ID"=>project\site::CATALOG_IBLOCK_ID,
				"ACTIVE"=>"Y",
				"PROPERTY_HIT_VALUE" => 'Да',
				//"PROPERTY_HAS_SKU" => 1,
				"INCLUDE_SUBSECTIONS" => "Y",
			);
			if( count($activeGenderSects) > 0 ){
				$filter['SECTION_ID'] = $filter_sects;
			}
			$fields = Array("ID", "PROPERTY_HIT", "PROPERTY_HAS_SKU");
			$dbElements = \CIBlockElement::GetList(Array("SORT"=>"ASC"), $filter, false, false, $fields);
			while ($element = $dbElements->GetNext()){
				$main_hits[] = $element['ID'];
			}
		$obCache->EndDataCache(array('main_hits' => $main_hits));
		}
		return $main_hits;
	}
	
	
	
	
	
}