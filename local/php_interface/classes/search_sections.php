<? 
class search_sections {



	

	static function searchByQ($q){
		\Bitrix\Main\Loader::includeModule("search");
		$obSearch = new \CSearch;
		$ids = array();
		$obSearch->Search(
			array(
				"QUERY" => $q,
				"SITE_ID" => LANG,
				array(
					array(
						"=MODULE_ID" => "iblock",
						"ITEM_ID" => "S%",
						"PARAM2" => project\site::CATALOG_IBLOCK_ID
					)
				)
			),
			array("TITLE_RANK"=>"DESC"),
			array('STEMMING' => true)
		);
		if ($obSearch->errorno != 0){} else {
			while($item = $obSearch->GetNext()){
				$ids[] = str_replace("S", "", $item['ITEM_ID']);
			}
		}
		if ( count($ids) == 0 ){
			$obSearch = new \CSearch;
			$obSearch->Search(
				array(
					"QUERY" => $q,
					"SITE_ID" => LANG,
					array(
						array(
							"=MODULE_ID" => "iblock",
							"ITEM_ID" => "S%",
							"PARAM2" => project\site::CATALOG_IBLOCK_ID
						)
					)
				),
				array("TITLE_RANK"=>"DESC"),
				array('STEMMING' => false)
			);
			if ($obSearch->errorno != 0){} else {
				while($item = $obSearch->GetNext()){
					$ids[] = str_replace("S", "", $item['ITEM_ID']);
				}
			}	
		}
		return $ids;
	}





	static function request($get){
		
		$_GET = $get;
		
		// Проверка ключа
		if(
			!$_GET['key']
			||
			$_GET['key'] != project\site::API_ACCESS_KEY
		){
			
			$arResponse['error'] = 'Ошибка авторизации';
			$json = json_encode($arResponse);
			
		} else {
			
			
			$arResponse = array();
			
			// Формируем фильтр
			$filter = array(
				'IBLOCK_ID' => project\site::CATALOG_IBLOCK_ID,
				'ACTIVE' => 'Y'
			);
			
			if( intval($_GET['id']) > 0 ){
				$filter['ID'] = $_GET['id'];
			}
			
			if( intval($_GET['depth']) > 0 ){
				$filter['DEPTH_LEVEL'] = $_GET['depth'];
			}
			
			if( strlen($_GET['q']) > 0 ){
				$ids = static::searchByQ($_GET['q']);
				if( count($ids) > 0 ){
					$filter['ID'] = $ids;
				} else {
					$filter['ID'] = 0;
				}
			}
			
			if( intval($_GET['section_id']) > 0 ){
				$filter['SECTION_ID'] = $_GET['section_id'];
			}

			// Запрос
			$dbSections = \CIBlockSection::GetList(Array("NAME"=>"ASC"), $filter, false);
			$totalCNT = $dbSections->SelectedRowsCount();
			while ($section = $dbSections->GetNext()){
				$section_chain = section_chain($section['ID']);
				$f_section = section_info($section_chain[0]['ID']);
				$arSect = array(
					'id' => $section['ID'],
					'name' => trim($section['NAME']),
					'url' => $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].$section['SECTION_PAGE_URL'],
					'section_id' => $section['IBLOCK_SECTION_ID'],
					'gender' => $f_section['UF_POL'],
				);
				$arResponse['sections'][$section['ID']] = $arSect;
			}
				
			$arResponse['response_info']['sections_count'] = $totalCNT;

			
			$json = json_encode($arResponse);
			
		}
		
		return $json;
		
	}





}