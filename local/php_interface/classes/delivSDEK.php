<?

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;


class delivSDEK {
	
	const HIBLOCK = 1;
	
	const AUTH_LOGIN = '5993c44e937eb92d4080024364358f30';
	const AUTH_PASS = '192761b6dd0f3f5af7f8abf984a5f441';
	
	const CITY_FROM_ID = '44';
	
	const MODE_ID = '3';
	
	//const TARIFF_ID = '137';
	static $tariffs = array( 136 , 137, 233, 234 );
	
	
	
	
	
	
	
	
	
	// Расчёт стоимости доставки
	static function calcDeliveryPrice( $loc_xml_id, $basketParams ){
		
		$res = false;
		
		if( intval($loc_xml_id) > 0 ){
		
			include_once( $_SERVER["DOCUMENT_ROOT"]."/local/php_interface/classes/CalculatePriceDeliveryCdek.php" );
			
			try {

				$calc = new CalculatePriceDeliveryCdek();
				
				$calc->setAuth(static::AUTH_LOGIN, static::AUTH_PASS);
				
				$calc->setSenderCityId(static::CITY_FROM_ID);
				
				$calc->setReceiverCityId($loc_xml_id);
				
				$calc->setDateExecute(date('Y-m-d'));
				
				//$calc->setTariffId(static::TARIFF_ID);
				foreach ( static::$tariffs as $tariff_id ){
					$calc->addTariffPriority( $tariff_id );
				}
				
				$calc->setModeDeliveryId(static::MODE_ID);
				
				// добавление места по размерам
				$calc->addGoodsItemBySize(
					round($basketParams['weight'] / 1000, 3), // weight1 (кг)
					$basketParams['length'], // length (см)
					$basketParams['width'], // width (см)
					$basketParams['height'] // height (см)
				);
				
				// добавление места по объёму
				/*$calc->addGoodsItemByVolume(
					$_REQUEST['weight2'], // weight
					$_REQUEST['volume2'] // volume
				);*/
				
				
				if ($calc->calculate() === true) {
					
					$result = $calc->getResult();
					
					if( $result && is_array($result) ){
						
						if( $result['error'] ){
						
							$res = array( 'status' => 'calc_error', 'error' => $result['error'] );
							
						} else {
							
							$res = array(
								'status' => 'ok',
								'price' => ceil($result['result']['price']),
								'deliveryPeriodMin' => $result['result']['deliveryPeriodMin'],
								'deliveryPeriodMax' => $result['result']['deliveryPeriodMax'],
							);
						}
						
					} else {
						
						$res = array( 'status' => 'request_error' );
					}
					
				} else {
					
					$err = $calc->getError();
					if( isset($err['error']) && !empty($err) ) {
						foreach($err['error'] as $e) {
							
							$res = array( 'status' => 'calc_error', 'error' => $e['text'] );
							
							tools\logger::addError('Ошибка расчёта стоимости СДЭК: ' . $e['text'] . '[код '.$e['code'].']');
						}
					}
					
				}

			} catch(\Exception $e) {
				
				$res = array( 'status' => 'request_error' );
				
				tools\logger::addError('Ошибка расчёта стоимости СДЭК: ' . $e->getMessage());
			}
		
		}
		
		return $res;
	}
	
	

	
	
	// Поиск для автокомплита
	static function search( $q, $equiv, $dop_filter ){
		
		$search_array = array();
		
		if( strlen($q) >= 2 ){
			
			\Bitrix\Main\Loader::includeModule('highloadblock');
			$hlblock_id = static::HIBLOCK;
			$hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById( $hlblock_id )->fetch();
			$entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity( $hlblock );
			$entity_data_class = $entity->getDataClass();
			$entity_table_name = $hlblock['TABLE_NAME'];
			$sTableID = 'tbl_'.$entity_table_name;
			
			if( $equiv ){
				$arFilter = array("=UF_CITY_NAME" => trim(strip_tags($q))); 
			} else {
				$arFilter = array("UF_CITY_NAME" => trim(strip_tags($q))."%"); 
			}
			$arSelect = array(
				"ID", 'UF_ID', 'UF_FULL_NAME', 'UF_CITY_NAME',
				'UF_OBL_NAME', 'UF_COUNTRY', 'UF_POST_CODE_LIST'
			);
			$arOrder = array("UF_CITY_NAME" => "ASC");
			
			$rsData = $entity_data_class::getList(array(
				"select" => $arSelect,
				"filter" => $arFilter,
				//"limit" => 100,
				"order" => $arOrder
			));
			$rsData = new \CDBResult($rsData, $sTableID);
			while($el = $rsData->Fetch()){
				$search_array[] = array(
					'label' => $el['UF_CITY_NAME'].' ('.str_replace('обл.', ' обл.', $el['UF_OBL_NAME']).', '.$el['UF_COUNTRY'].') ['.$el['UF_ID'].']',
					'name_ru' => $el['UF_CITY_NAME'],
					'sdek_loc_id' => $el['UF_ID'],
					'value' => $el['UF_ID']
				);
			}
			
			if( $dop_filter && count($search_array) > 0 ){
				foreach($search_array as $key => $search_item){
					if( substr_count($search_item['name_ru'], ',') ){
						$ar = explode(',', $search_item['name_ru']);
						if( strtolower(trim($ar[0])) != strtolower(trim($q)) ){
							unset($search_array[$key]);
						}
					} else {
						if( strtolower(trim($search_item['name_ru'])) != strtolower(trim($q)) ){
							
							unset($search_array[$key]);
						}
					}
				}
			}
			
		}
		
		return $search_array;
	}
	
	
	
	
	
	// Загрузка местоположений
	static function loadLocations($country){
		require_once ($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/csv_data.php");
		$file = $_SERVER['DOCUMENT_ROOT'].'/upload/SDEK/cities_ukr.csv';
		if (file_exists($file)){
			$hiblock = static::HIBLOCK;
			\Bitrix\Main\Loader::includeModule('highloadblock');
			$hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById( $hiblock )->fetch();
			$entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity( $hlblock );
			$entity_data_class = $entity->getDataClass();
			$rows = array();
			$csvFile = new CCSVData('R', false);   $csvFile->LoadFile($file);
			$csvFile->SetDelimiter(';');   $cnt = 0;
			while ($arRes = $csvFile->Fetch()){ 
				$cnt++;
				if( $cnt > 1 ){
					$arData = Array(
						"UF_ID" => $arRes[0],
						"UF_FULL_NAME" => $arRes[1],
						"UF_CITY_NAME" => $arRes[2],
						"UF_OBL_NAME" => $arRes[3],
						"UF_NAL_SUM_LIMIT" => $arRes[5],
						"UF_ENG_NAME" => $arRes[6],
						"UF_POST_CODE_LIST" => $arRes[7],
						"UF_COUNTRY" => $country?$country:'Россия'
					);
					$result = $entity_data_class::add($arData);
				}
			}
		} 
	}

	
	
}