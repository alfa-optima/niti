<? use \Bitrix\Sale;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;


class orderImport {
	
	
	
	// Создание заказа
	function create($get){
		
		$_GET = $get;
		
		// Проверка ключа
		if(
			!$_GET['key']
			||
			$_GET['key'] != project\site::API_ACCESS_KEY
		){
			
			$arResponse['error'] = 'Ошибка авторизации';
			return json_encode($arResponse);
			
		} else {
			
			$payer_type = intval( $_GET['payer_type'] ) > 0? $_GET['payer_type'] : 1 /* ФЛ */;
			$user_id = intval( $_GET['user_id'] ) > 0 ? $_GET['user_id'] : 153 /* служебный пользователь */;
			$pay_id = intval( $_GET['pay_id'] ) > 0 ? $_GET['pay_id'] : 2 /* наличка */;
			$deliv_id = intval( $_GET['deliv_id'] ) > 0 ? $_GET['deliv_id'] : 2 /* самовывоз */;
			
			$pay_sum = 0;
			
			try {
				
				// СОЗДАЁМ ЗАКАЗ
				$order = Sale\Order::create('s1', $user_id);

				// Тип плательщика
				$order->setPersonTypeId($payer_type);

				// Оплата
				$paymentCollection = $order->getPaymentCollection();
				$payment = $paymentCollection->createItem(Sale\PaySystem\Manager::getObjectById( $pay_id ));
				$payment->setField('SUM', $pay_sum); // Сумма к оплате
				$payment->setField('CURRENCY', 'RUB');

				// Доставка
				$shipmentCollection = $order->getShipmentCollection();
				$shipment = $shipmentCollection->createItem(Sale\Delivery\Services\Manager::getObjectById( $deliv_id ));
				$shipmentItemCollection = $shipment->getShipmentItemCollection();
				$order->doFinalAction(true);

				// Сохраняем новый заказ
				$order->save();
				
				// Определяем ID нового заказа
				$order_id = $order->GetId();

				if (intval($order_id) > 0){
					
					orderExport::send($order_id);
					
					return json_encode(
						array(
							'status' => 'ok',
							'order_id' => $order_id
						)
					);
					
				} else {
					
					tools\logger::addError('Ошибка создания заказа (класс "orderImport")');
					
					return json_encode(array('status' => 'error'));
					
				}
				
			} catch (\Exception $e) {
				
				tools\logger::addError($e->getMessage());
				
				return json_encode(array('status' => 'error'));
				
			}
			
		}
		
	}

	
}

