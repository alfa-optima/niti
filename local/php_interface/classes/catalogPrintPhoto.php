<?  

class catalogPrintPhoto {
	
	use files___trait;
	
	const MIN_WIDTH = 1000;
	const MIN_HEIGHT = 1000;
	const MAX_WIDTH = 2000;
	const MAX_HEIGHT = 2000;
	const MAX_FILE_SIZE = 10485760;
	
	const TEMP_UPLOAD_DIR = "printPhotoTMP";
	const TEMP_UPLOAD_PATH = "/upload/printPhotoTMP/";

	
	protected $FILES = false;
	protected $doc_root = false;
	protected $fileTypes = array('jpeg', 'png');
	protected $file_name = false;
	protected $new_file_name = false;
	protected $file_path = false;
	protected $width = false;
	protected $height = false;
	protected $file_moved = false;
	protected $file_extension = false;
	
	
	
	// конструктор объекта
	function __construct($files){
		$this->doc_root = $_SERVER["DOCUMENT_ROOT"];
		// Перемещение файла во временную папку
		if ( $this->moveFile($files) ){
			$this->file_moved = true;
		}	
	}
	
		

	protected function jsOnResponse($obj){
		echo '<script type="text/javascript">
		window.parent.printOnResponse("'.$obj.'");
		</script>';
	}

	
	
	// Загрузка фото
	public function upload(){
		$check_status = $this->checkFile();
		if ( $check_status == 'ok' ){
			$big_file_path = (static::TEMP_UPLOAD_PATH).($this->new_file_name);
			// Инфо о файле $big_file_path
			$arFile = \CFile::MakeFileArray(($this->doc_root).($big_file_path));
			// Сохраняем файл в битриксе
			$fid = \CFile::SaveFile($arFile, static::TEMP_UPLOAD_DIR);
			// Удаляем временные файлы
			unlink( ($this->doc_root).($big_file_path) );
			project\site::saveTempEl($fid);
			
			if( intval($fid) > 0 ){

				// Ответ
				$this->jsOnResponse("{'status':'ok', 'fid':'" . $fid . "'}");
			} else {
				// Ответ
				$this->jsOnResponse("{'status':'Ошибка Загрузки фото'}");
			}
		} else {
			// Ответ
			$this->jsOnResponse("{'status':'".$check_status."'}");
		}
	}

	
	
	
	
	
	
}