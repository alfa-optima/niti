<?

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;


class delivBoxberry {

	const HIBLOCK = 2;
	const AUTH_TOKEN = '21372.pjpqeafd';
	const CITY_FROM_ID = '68';
	const TIMEOUT = 3;




	// Расчёт стоимости доставки (курьер)
	static function calcCourierPrice( $zip, $basketParams ){
		if( strlen($zip) > 0 ){

			$weight = $basketParams['weight']; // грамм
			$height = $basketParams['height']; // высота коробки (см)
			$width = $basketParams['width']; // ширина  коробки (см)
			$depth = $basketParams['length']; // глубина коробки (см)
			$orderSum = $basketParams['orderSum']; // cтоимость товаров без учета стоимости доставки
			$deliverySum = $orderSum; // заявленная ИМ стоимость доставки

			$url='http://api.boxberry.de/json.php?token='.static::AUTH_TOKEN.'&method=DeliveryCosts&weight='.$weight.'&zip='.$zip.'&ordersum='.$orderSum.'&deliverysum='.$deliverySum.'&targetstart='.static::CITY_FROM_ID.'&height='.$height.'&width='.$width.'&depth='.$depth;

			$client = new Client(array( 'timeout' => static::TIMEOUT ));
			try {
				$response = $client->request( 'GET', $url );
				if( $response->getStatusCode() == '200' ){
					$body = $response->getBody();
					$content = $body->getContents();
					$result = json_to_array($content);
					if( $result['price'] ){
						$result['price'] = ceil($result['price']);
					}
					return $result;
				} else {
					tools\logger::addError('Ошибка запроса Boxberry calcCourierPrice - ответ сервера '.$response->getStatusCode());
				}
			} catch(RequestException $ex){
				tools\logger::addError('Ошибка запроса Boxberry calcCourierPrice - '.$ex->getMessage());
			}
		}
		return false;
	}



	// Расчёт стоимости доставки (ПВЗ)
	static function calcPVZPrice( $pvz_code, $basketParams ){
		if( strlen($pvz_code) > 0 ){

			$weight = $basketParams['weight']; // грамм
			$height = $basketParams['height']; // высота коробки (см)
			$width = $basketParams['width']; // ширина  коробки (см)
			$depth = $basketParams['length']; // глубина коробки (см)
			$orderSum = $basketParams['orderSum']; // cтоимость товаров без учета стоимости доставки
			$deliverySum = $orderSum; // заявленная ИМ стоимость доставки

			$url='http://api.boxberry.de/json.php?token='.static::AUTH_TOKEN.'&method=DeliveryCosts&weight='.$weight.'&target='.$pvz_code.'&ordersum='.$orderSum.'&deliverysum='.$deliverySum.'&targetstart='.static::CITY_FROM_ID.'&height='.$height.'&width='.$width.'&depth='.$depth;

			$client = new Client(array( 'timeout' => static::TIMEOUT ));
			try {
				$response = $client->request( 'GET', $url );
				if( $response->getStatusCode() == '200' ){
					$body = $response->getBody();
					$content = $body->getContents();
					$result = json_to_array($content);
					if( $result['price'] ){
						$result['price'] = ceil($result['price']);
					}
					return $result;
				} else {
					tools\logger::addError('Ошибка запроса Boxberry calcPVZPrice - ответ сервера '.$response->getStatusCode());
				}
			} catch(RequestException $ex){
				tools\logger::addError('Ошибка запроса Boxberry calcPVZPrice - '.$ex->getMessage());
			}
		}
		return false;
	}



	// Проверки возможности доставки на ZIP
	static function checkCourier( $zip ){

		$url='http://api.boxberry.de/json.php?token='.static::AUTH_TOKEN.'&method=ZipCheck&Zip='.$zip;

		$client = new Client(array( 'timeout' => static::TIMEOUT ));
		$response = $client->request( 'GET', $url );
		try {
			if( $response->getStatusCode() == '200' ){
				$body = $response->getBody();
				$content = $body->getContents();
				$result = json_to_array($content);
				return $result[0]['ExpressDelivery']==1?'Y':'N';
			} else {
				tools\logger::addError('Ошибка запроса Boxberry checkCourier - ответ сервера '.$response->getStatusCode());
			}
		} catch(RequestException $ex){
			tools\logger::addError('Ошибка запроса Boxberry checkCourier - '.$ex->getMessage());
		}
		return false;
	}



	// Проверки наличия ПВЗ по коду города
	static function checkPVZ( $boxberry_loc_id ){

		$url='http://api.boxberry.de/json.php?token='.static::AUTH_TOKEN.'&method=ListPoints&CityCode='.$boxberry_loc_id.'&prepaid=1';

		$client = new Client(array( 'timeout' => static::TIMEOUT ));
		$response = $client->request( 'GET', $url );
		try {
			if( $response->getStatusCode() == '200' ){
				$body = $response->getBody();
				$content = $body->getContents();
				$result = json_to_array($content);

				return $result;

			} else {
				tools\logger::addError('Ошибка запроса Boxberry checkPVZ - ответ сервера '.$response->getStatusCode());
			}
		} catch(RequestException $ex){
			tools\logger::addError('Ошибка запроса Boxberry checkPVZ - '.$ex->getMessage());
		}
		return false;
	}






	// Поиск для автокомплита
	static function search( $q, $equiv ){

		$search_array = array();

		if( strlen($q) >= 2 ){

			\Bitrix\Main\Loader::includeModule('highloadblock');
			$hlblock_id = static::HIBLOCK;
			$hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById( $hlblock_id )->fetch();
			$entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity( $hlblock );
			$entity_data_class = $entity->getDataClass();
			$entity_table_name = $hlblock['TABLE_NAME'];
			$sTableID = 'tbl_'.$entity_table_name;

			if( $equiv ){
				$arFilter = array("=UF_CITY_NAME" => trim(strip_tags($q)));
			} else {
				$arFilter = array("UF_CITY_NAME" => trim(strip_tags($q))."%");
			}
			$arSelect = array(
				"ID", 'UF_ID', 'UF_FULL_NAME', 'UF_CITY_NAME',
				'UF_OBL_NAME', 'UF_COUNTRY'
			);
			$arOrder = array("UF_CITY_NAME" => "ASC");

			$rsData = $entity_data_class::getList(array(
				"select" => $arSelect,
				"filter" => $arFilter,
				//"limit" => 100,
				"order" => $arOrder
			));
			$rsData = new \CDBResult($rsData, $sTableID);
			while($el = $rsData->Fetch()){
				$search_array[] = array(
					'label' => $el['UF_CITY_NAME'].' ('.str_replace('обл.', ' обл.', $el['UF_OBL_NAME']).', '.$el['UF_COUNTRY'].') ['.$el['UF_ID'].']',
					'name_ru' => $el['UF_CITY_NAME'],
					'boxberry_loc_id' => $el['UF_ID'],
					'value' => $el['UF_ID']
				);
			}

		}

		return $search_array;
	}



	// Загрузка местоположений
	static function loadLocations($country){

		//$url = 'http://api.boxberry.de/json.php?token='.static::AUTH_TOKEN.'&method=ListCitiesFull';
		$url = 'http://api.boxberry.de/json.php?token='.static::AUTH_TOKEN.'&method=ListPoints';
		$json = file_get_contents($url);
		$items = json_to_array($json);

		pre($items);

		/*$hiblock = static::HIBLOCK;
		\Bitrix\Main\Loader::includeModule('highloadblock');
		$hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById( $hiblock )->fetch();
		$entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity( $hlblock );
		$entity_data_class = $entity->getDataClass();

		foreach ($items as $item){
			$arData = Array(
				"UF_ID" => $item['Code'],
				"UF_FULL_NAME" => $item['UniqName'],
				"UF_CITY_NAME" => $item['Name'],
				"UF_PREFIX" => $item['Prefix'],
				"UF_OBL_NAME" => $item['Region'],
				"UF_COUNTRY" => 'Россия'
			);
			$result = $entity_data_class::add($arData);
		}*/
	}



}
