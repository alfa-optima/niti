<?

class comment {

	const CNT = 20;

	public $tov_id;




	// Конструктор
	function __construct ($tov_id){

		if( intval($tov_id) > 0 ){
			$this->tov_id = $tov_id;
		}

	}




	// Добавление нового комментария
	public function add( $message ){

		if(
			intval($this->tov_id) > 0
			&&
			strlen($message) > 0
		){

			global $USER;
			$PROP = array();
			$PROP[16] = $this->tov_id;
			$PROP[17] = 0;
			$PROP[18] = $USER->GetId();

			$el = new CIBlockElement;

			$fields = Array(
				"IBLOCK_ID"      => project\site::COMMENTS_IBLOCK_ID,
				"PROPERTY_VALUES"=> $PROP,
				"NAME"           => 'Новый комментарий',
				"ACTIVE"         => $USER->IsAdmin()?"Y":"N",
				"PREVIEW_TEXT"   => strip_tags($message),
			);

			if($id = $el->Add($fields)){
				return $id;
			} else {
				return false;
			}

		} else {
			return false;
		}

	}




	// Лайк за коммент
	static function like( $comment_id ){

		if ( !$_SESSION['my_likes'] ){    $_SESSION['my_likes'] = array();    }

		if ( !in_array( $comment_id, $_SESSION['my_likes'] ) ){

			// Получим текущее количество лайков
			$db_props = \CIBlockElement::GetProperty(project\site::COMMENTS_IBLOCK_ID, $comment_id, array("sort" => "asc"), array("CODE" => "LIKES"));
			while ($ar_prop = $db_props->GetNext()){

				$likes = $ar_prop['VALUE'];
				$likes++;

				// Вносим изменение в свойство LIKES
				$set_prop = array("LIKES" => $likes);
				\CIBlockElement::SetPropertyValuesEx($comment_id, project\site::COMMENTS_IBLOCK_ID, $set_prop);

				$_SESSION['my_likes'][] = $comment_id;

				// Ответ
				return Array( "status" => "ok", 'likes' => $likes );

			}

			// Ответ
			return Array( "status" => "error", 'error_text' => 'Ошибка голосования' );

		} else {

			// Ответ
			return Array( "status" => "error", 'error_text' => 'Вы уже голосовали за этот комментарий...' );

		}

	}




	// Получение списка комментариев
	public function GetList( $min_id = false ){

		$comments = array();

		if ( intval($this->tov_id) > 0 ){

			$filter = Array(
				"IBLOCK_ID" => project\site::COMMENTS_IBLOCK_ID,
				"ACTIVE"=>"Y",
				"PROPERTY_TOV_ID" => $this->tov_id
			);

			if ( intval($min_id) > 0 ){
				$filter['<ID'] = intval($min_id);
			}

			$dbComments = \CIBlockElement::GetList(
				Array("DATE_CREATE"=>"DESC"),
				$filter, false, Array("nTopCount" => (static::CNT + 1)),
				Array(
					"ID",
					"NAME",
					"DATE_CREATE",
					"PREVIEW_TEXT",
					"DETAIL_TEXT",
					"PROPERTY_TOV_ID",
					"PROPERTY_USER_ID",
					"PROPERTY_LIKES"
				)
			);
			while ($dbComment = $dbComments->GetNext()){

				$comments[] = $dbComment;

			}

		}

		return $comments;

	}




}
