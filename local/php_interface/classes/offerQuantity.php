<? \Bitrix\Main\Loader::includeModule('catalog');
use \Bitrix\Sale;



class offerQuantity {
	
	
	
	
	static function get( $get ){
		
		$_GET = $get;
		
		// Проверка ключа
		if(
			!$_GET['key']
			||
			$_GET['key'] != project\site::API_ACCESS_KEY
		){
			
			$arResponse['error'] = 'Ошибка авторизации';
			return json_encode($arResponse);
			
		} else {
			
			$id = $_GET['id'];
			
			if( strlen($id) > 0 ){} else {
				$arResponse['error'] = 'Не передан id оффера';
				return json_encode($arResponse);
			}
			
			\Bitrix\Main\Loader::includeModule('iblock');
			$filter = Array(
				"IBLOCK_ID" => project\site::SKU_IBLOCK_ID,
				"ACTIVE" => "Y",
				"XML_ID" => $id
			);
			$fields = Array( "ID", "XML_ID" );
			$dbElements = \CIBlockElement::GetList(
				array("SORT"=>"ASC"), $filter, false, array("nTopCount"=>1), $fields
			);
			if ($element = $dbElements->GetNext()){
				
				$product = \CCatalogProduct::GetByID($element['ID']);
				
				$arResponse['status'] = 'ok';
				$arResponse['id'] = $id;
				$arResponse['quantity'] = $product['QUANTITY'];
				return json_encode($arResponse);
				
			} else {
				
				$arResponse['error'] = 'Такой оффер не найден';
				return json_encode($arResponse);
				
			}
			
		}
		
	}
	
	
	
	
	static function set( $get ){
		
		$_GET = $get;
		
		// Проверка ключа
		if(
			!$_GET['key']
			||
			$_GET['key'] != project\site::API_ACCESS_KEY
		){
			
			$arResponse['error'] = 'Ошибка авторизации';
			return json_encode($arResponse);
			
		} else {
			
			$id = $_GET['id'];
			$quantity = $_GET['quantity'];
			
			if( strlen($id) > 0 ){} else {
				$arResponse['error'] = 'Не передан id оффера';
				return json_encode($arResponse);
			}
			
			if( strlen($quantity) > 0 ){} else {
				$arResponse['error'] = 'Не передано количество';
				return json_encode($arResponse);
			}
			
			\Bitrix\Main\Loader::includeModule('iblock');
			$filter = Array(
				"IBLOCK_ID" => project\site::SKU_IBLOCK_ID,
				"ACTIVE" => "Y",
				"XML_ID" => $id
			);
			$fields = Array( "ID", "XML_ID" );
			$dbElements = \CIBlockElement::GetList(
				array("SORT"=>"ASC"), $filter, false, array("nTopCount"=>1), $fields
			);
			if ($element = $dbElements->GetNext()){
				
				$obProduct = new \CCatalogProduct();
				$res = $obProduct->Update($element['ID'], ['QUANTITY' => $quantity]); 
				
				if( $res ){
					$arResponse['status'] = 'ok';
					return json_encode($arResponse);
					
				} else {
					$arResponse['error'] = 'Такой оффер не найден';
					return json_encode($obProduct->LAST_ERROR);
				}
				
			} else {
				
				$arResponse['error'] = 'Такой оффер не найден';
				return json_encode($arResponse);
				
			}
			
		}
		
	}
	

	
	

	
}

