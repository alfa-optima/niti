<?

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;


AddEventHandler("iblock", "OnBeforeIBlockElementDelete", array('catalogPrint', 'OnBeforeIBlockElementDeleteHandler'));




class catalogPrint {
	
	
	
	static function OnBeforeIBlockElementDeleteHandler($ID){
		$iblock_id = tools\el::getIblock($ID);
		if( $iblock_id == project\site::PRINTS_IBLOCK_ID ){
			
			$filter = Array(
				"IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
				"PROPERTY_PRINT_ID" => $ID,
			);
			$fields = Array( "PROPERTY_PRINT_ID" );
			$dbElements = \CIBlockElement::GetList(
				array("SORT"=>"ASC"), $filter, false, array("nTopCount"=>1), $fields
			);
			while ($element = $dbElements->GetNext()){
				global $APPLICATION;
				$APPLICATION->throwException('В каталоге есть товары, привязанные к этому принту');
				return false;
			}
			
			$filter = Array(
				"IBLOCK_ID" => project\site::CONSTR_ITEMS_IBLOCK_ID,
				"PROPERTY_PRINT" => $ID,
			);
			$fields = Array( "PROPERTY_PRINT" );
			$dbElements = \CIBlockElement::GetList(
				array("SORT"=>"ASC"), $filter, false, array("nTopCount"=>1), $fields
			);
			while ($element = $dbElements->GetNext()){
				global $APPLICATION;
				$APPLICATION->throwException('На согласовании есть товары, привязанные к этому принту');
				return false;
			}
			
		}

	}
	
	
	
	
	
	protected $iblock_id = project\site::PRINTS_IBLOCK_ID;
	protected $user_id;
	protected $photo;
	protected $fields;
	
	
	
	function __construct ($id = false){
		if( intval($id) > 0 ){
			$print = tools\el::info($id);
			if( intval($print['ID']) > 0 ){
				$this->fields = $print;
			}
			if( intval($print['PROPERTY_USER_ID_VALUE']) > 0 ){
				$this->user_id = $print['PROPERTY_USER_ID_VALUE'];
			}
			if( strlen($print['PROPERTY_PHOTO_VALUE']) > 0 ){
				$this->photo = $print['PROPERTY_PHOTO_VALUE'];
			}
		}
	}
	
	
	
	// Список принтов пользователя
	public function getList($user_id){
		$list = array();
		$dbElements = \CIBlockElement::GetList(
			Array("SORT"=>"ASC"),
			Array(
				"IBLOCK_ID"=>$this->iblock_id,
				"ACTIVE"=>"Y",
				"PROPERTY_USER_ID"=>$user_id,
				"PROPERTY_STATUS_VALUE" => 'Активно',
				"!PROPERTY_PHOTO_ID" => false
			), false, false,
			Array("ID", "PROPERTY_USER_ID", "PROPERTY_STATUS", "PROPERTY_PHOTO_ID"));
		while ($element = $dbElements->GetNext()){
			$list['ACTIVE'][] = $element['ID'];
		}
		$dbElements = \CIBlockElement::GetList(
			Array("SORT"=>"ASC"),
			Array(
				"IBLOCK_ID"=>$this->iblock_id,
				"ACTIVE"=>"Y",
				"PROPERTY_USER_ID"=>$user_id,
				"PROPERTY_STATUS_VALUE" => false,
				"!PROPERTY_PHOTO_ID" => false
			), false, false,
			Array("ID", "PROPERTY_USER_ID", "PROPERTY_STATUS", "PROPERTY_PHOTO_ID"));
		while ($element = $dbElements->GetNext()){
			$list['NOT_ACTIVE'][] = $element['ID'];
		}
		return $list;
	}

	
	
	// Создание принта
	public function add($name, $photo_id){
		if( strlen($name) > 0 && intval($photo_id) > 0 ){
			$file = $_SERVER['DOCUMENT_ROOT'].\CFile::GetPath($photo_id);
			if( file_exists($file) ){
				global $USER;
				$PROP = array();
				$PROP[43] = $USER->GetID();
				$PROP[56] = $photo_id;
				// Сразу ставим одобренным (так попросил заказчик)
				$PROP[57] = 5;
				////////////////////////
				$element = new \CIBlockElement;
				$fields = Array(
					"IBLOCK_ID"				=> $this->iblock_id,
					"PROPERTY_VALUES"		=> $PROP,
					"NAME"					=> $name,
					"ACTIVE"				=> "Y"
				);
				if( $el_id = $element->Add($fields) ){
					project\site::deleteTempEl($photo_id);
					return $el_id;
				}
			}
		}
		return false;
	}
	
	
	
	
	
	
	
	// Проверка наличия цвета у иллюстратора
	static function checkColor($color_id, $print_id){
		$check = false;
		// Кешируем 
		$obCache = new \CPHPCache();
		$cache_time = 60*60;
		$cache_id = 'printCheckColor_'.$print_id.'_'.$color_id;
		$cache_path = '/printCheckColor/'.$print_id.'/'.$color_id.'/';
		if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
			$vars = $obCache->GetVars();   extract($vars);
		} elseif($obCache->StartDataCache()){
			$filter = Array(
				"IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
				"ACTIVE" => "Y",
				"PROPERTY_PRINT_ID" => $print_id,
				"PROPERTY_COLORS_AUTO" => $color_id,
			);
			$fields = Array( "PROPERTY_PRINT_ID", "PROPERTY_COLORS_AUTO" );
			$dbElements = \CIBlockElement::GetList(
				array("SORT"=>"ASC"), $filter, false, array("nTopCount"=>1), $fields
			);
			while ($element = $dbElements->GetNext()){  $check = true;  }
		$obCache->EndDataCache(array('check' => $check));
		}
		return $check;
	}
	
	
	
	
	// Проверка наличия коллекции у иллюстратора
	static function checkCollection($collection_id, $print_id){
		$check = false;
		// Кешируем 
		$obCache = new \CPHPCache();
		$cache_time = 60*60;
		$cache_id = 'printCheckCollection_'.$print_id.'_'.$collection_id;
		$cache_path = '/printCheckCollection/'.$print_id.'/'.$collection_id.'/';
		if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
			$vars = $obCache->GetVars();   extract($vars);
		} elseif($obCache->StartDataCache()){
			$filter = Array(
				"IBLOCK_ID" => project\site::CATALOG_IBLOCK_ID,
				"ACTIVE" => "Y",
				"PROPERTY_PRINT_ID" => $print_id,
				"PROPERTY_COLLECTION" => $collection_id,
			);
			$fields = Array( "PROPERTY_PRINT_ID", "PROPERTY_COLLECTION" );
			$dbElements = \CIBlockElement::GetList(
				array("SORT"=>"ASC"), $filter, false, array("nTopCount"=>1), $fields
			);
			while ($element = $dbElements->GetNext()){  $check = true;  }
		$obCache->EndDataCache(array('check' => $check));
		}
		return $check;
	}
	
	
	
	
	
}