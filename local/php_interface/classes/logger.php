<?

class logger {
	
	
	const ERROR_LOG_PATH = '/local/php_interface/logs/';
	const ERROR_LOG_FILE = 'errors.log';
	
	
	static function addError($message){
		if( !file_exists($_SERVER['DOCUMENT_ROOT'].(static::ERROR_LOG_PATH)) ){
			mkdir($_SERVER['DOCUMENT_ROOT'].(static::ERROR_LOG_PATH), 0700);
		}
		$message = '['.date("Y-m-d H:i:s").'] '.$message."\n";
		$file_path = $_SERVER["DOCUMENT_ROOT"].(static::ERROR_LOG_PATH).(static::ERROR_LOG_FILE);

		$f = fopen($file_path, "a");
		fwrite($f, $message);
		fclose($f);
		
		// Если превышает 2 МБ
		if( filesize($file_path)/1024/1024 > 2  ){

			// Архивируем файл
			$time = time();
			$tmpPath = $_SERVER["DOCUMENT_ROOT"].(static::ERROR_LOG_PATH)."logs_".$time."/";
			if( mkdir($tmpPath, 0700) ) {
				copy($file_path, $tmpPath.static::ERROR_LOG_FILE);
				$arPackFiles[] = $tmpPath.static::ERROR_LOG_FILE;
			}
			$packarc = \CBXArchive::GetArchive($_SERVER["DOCUMENT_ROOT"].static::ERROR_LOG_PATH."/logs_".$time.".zip");
			$pRes = $packarc->Pack($arPackFiles);
			
			// Удаляем что уже не нужно
			unlink($tmpPath.static::ERROR_LOG_FILE);
			unlink($file_path);
			rmdir($tmpPath);
			
		}
		
	}
	
	
}