<?

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;


AddEventHandler("iblock", "OnBeforeIBlockElementAdd",
	array("\AOptima\Project\iSubscribe", "before_add_handler")
);
AddEventHandler("iblock", "OnAfterIBlockElementAdd",
	array("\AOptima\Project\iSubscribe", "after_add_handler")
);
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate",
	array("\AOptima\Project\iSubscribe", "before_update_handler")
);

AddEventHandler("iblock", "OnBeforeIBlockElementDelete",
	array("\AOptima\Project\iSubscribe", "before_delete_handler")
);






class iSubscribe {
	
	static function before_add_handler($arFields){
		if( $arFields['IBLOCK_ID'] == project\site::ISUBSCRIBES_IBLOCK_ID ){
			if( $_REQUEST['sessid'] ){
				$props = static::getProps($arFields);
				if( $props['user_id'] && $props['illustrator_id'] ){
					$ex_id = static::getEl($props['illustrator_id'], $props['user_id']);
					if( $ex_id ){
						global $APPLICATION;
						$APPLICATION->throwException("Такая подписка уже есть");
						return false;
					}
				}
			}
		}
	}
	static function after_add_handler($arFields){
		if( $arFields['IBLOCK_ID'] == project\site::ISUBSCRIBES_IBLOCK_ID ){
			$props = static::getProps($arFields);
			if( $props['illustrator_id'] ){
				BXClearCache(true, "/subscribesForIllustrator/".$props['illustrator_id'].'/');
			}
		}
	}
	static function before_update_handler($arFields){
		if( $arFields['IBLOCK_ID'] == project\site::ISUBSCRIBES_IBLOCK_ID ){
			global $APPLICATION;
			$APPLICATION->throwException("Редактировать подписки нельзя");
			return false;
		}
	}
	static function before_delete_handler($ID){
		$iblock_id = tools\el::getIblock($ID);
		if( $iblock_id == project\site::ISUBSCRIBES_IBLOCK_ID ){
			$subscribe = tools\el::info($ID);
			BXClearCache(true, "/subscribesForIllustrator/".$subscribe['PROPERTY_ILLUSTRATOR_VALUE'].'/');
		}
	}
	static function getProps($arFields){
		$user_id = false;   $illustrator_id = false;
		foreach ( $arFields['PROPERTY_VALUES'] as $prop_id => $ar ){
			$prop_code = tools\prop::getCode( $prop_id );
			if( $prop_code == 'USER' ){
				$user_id = $ar[array_keys($ar)[0]]['VALUE'];
			} else if( $prop_code == 'ILLUSTRATOR' ){
				$illustrator_id = $ar[array_keys($ar)[0]]['VALUE'];
			}
		}
		return array('user_id' => $user_id, 'illustrator_id' => $illustrator_id);
	}
	
	
	
	
	
	// Рассылка подписок
	static function send($illustrator_id, $tov_id){
		if( intval($illustrator_id) > 0 ){
			$illustrator = tools\user::info($illustrator_id);
			if( intval($illustrator['ID']) > 0 ){
				$subscribes = static::getListForIllustrator($illustrator['ID']);
				if( count($subscribes) > 0 ){
					foreach( $subscribes as $subscribe_id ){
						$subscribe = tools\el::info($subscribe_id);
						if( intval($subscribe['ID']) > 0 ){
							$user_id = $subscribe['PROPERTY_USER_VALUE'];
							$user = tools\user::info($user_id);
							$tovar = tools\el::info($tov_id);
							if( intval($user['ID']) > 0 ){
								// отправка инфы на почту
								$arEventFields = Array(
									"ILLUSTRATOR" => $illustrator["NAME"],
									"EMAIL" => $user["EMAIL"],
									"LINK" => '<a href="'.$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].$tovar['DETAIL_PAGE_URL'].'">'.$tovar['NAME'].'</a>'
								);
								\CEvent::SendImmediate("ISUBSCRIBE_SEND", "s1", $arEventFields);
								
								$_SESSION['arEventFields'] = $arEventFields;
								
							}
						}
					}
				}
			}
		}
	}
	

	
	
	
	// Новая подписка
	static function add(){
		global $USER;
		if ($USER->IsAuthorized()){
			// Инфо об иллюстраторе
			$user = tools\user::info(param_post('illustrator'));
			if( $user ){
				$subscribe_id = static::getEl( param_post('illustrator'), $USER->GetID() );
				if( !$subscribe_id ){
					$PROP = array();
					$PROP[78] = $USER->GetID();
					$PROP[79] = param_post('illustrator');
					$element = new \CIBlockElement;
					$fields = Array(
						"IBLOCK_ID"				=> project\site::ISUBSCRIBES_IBLOCK_ID,
						"PROPERTY_VALUES"		=> $PROP,
						"NAME"					=> 'Новая подписка',
						"ACTIVE"				=> "Y"
					);
					if( $element->Add($fields) ){
						BXClearCache(true, "/subscribesForIllustrator/".param_post('illustrator').'/');
						$subscribes = project\i_subscribe::getListForIllustrator(param_post('illustrator'));
						return array('status' => 'ok', 'cnt' => count($subscribes));
					} else {
						return array('status' => 'error', 'text' => 'Ошибка сохранения');
					}
				} else {
					return array('status' => 'exists', 'text' => 'Вы <u>уже подписаны</u> на этого иллюстратора<br>Хотите <u>отписаться</u>?');
				}
			} else {
				return array('status' => 'error', 'text' => 'Ошибка данных');
			}
		} else {
			return array('status' => 'needAuth');
		}
	}
		

	
	
	// проверка подписан/не подписан
	static function getEl($illustrator_id, $user_id){
		$el = false;
		$filter = Array(
			"IBLOCK_ID"=>project\site::ISUBSCRIBES_IBLOCK_ID, "ACTIVE"=>"Y",
			"PROPERTY_ILLUSTRATOR" => $illustrator_id,
			"PROPERTY_USER" => $user_id,
		);
		$fields = Array("ID", "PROPERTY_ILLUSTRATOR", "PROPERTY_USER");
		$dbElements = \CIBlockElement::GetList(
			Array("SORT"=>"ASC"), $filter, false, Array("nTopCount"=>1), $fields
		);
		while ($element = $dbElements->GetNext()){
			$el = $element['ID'];
		}
		return $el;
	}
	
	
	
	
	
	// Подписки пользователя
	static function getListForUser($user_id){
		$list = array();
		$filter = Array(
			"IBLOCK_ID"=>project\site::ISUBSCRIBES_IBLOCK_ID, "ACTIVE"=>"Y",
			"PROPERTY_USER" => $user_id,
		);
		$fields = Array("ID", "PROPERTY_USER");
		$subscribes = \CIBlockElement::GetList(
			Array("SORT"=>"ASC"), $filter, false, false, $fields
		);
		while ($subscribe = $subscribes->GetNext()){
			$list[] = $subscribe['ID'];
		}
		return $list;
	}
	
	
	
	// Подписки на иллюстратора
	static function getListForIllustrator($illustrator_id){
		$list = array();
		$obCache = new \CPHPCache();
		$cache_time = 30*24*60*60;
		$cache_id = 'subscribesForIllustrator_'.$illustrator_id;
		$cache_path = '/subscribesForIllustrator/'.$illustrator_id.'/';
		if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){
			$vars = $obCache->GetVars();   extract($vars);
		} elseif($obCache->StartDataCache()){
			$filter = Array(
				"IBLOCK_ID"=>project\site::ISUBSCRIBES_IBLOCK_ID, "ACTIVE"=>"Y",
				"PROPERTY_ILLUSTRATOR" => $illustrator_id,
			);
			$fields = Array("ID", "PROPERTY_ILLUSTRATOR");
			$subscribes = \CIBlockElement::GetList(
				Array("SORT"=>"ASC"), $filter, false, false, $fields
			);
			while ($subscribe = $subscribes->GetNext()){
				$list[] = $subscribe['ID'];
			}
		$obCache->EndDataCache(array('list' => $list));
		}
		return $list;
	}
	
	
	
	
	
	// Удаление подписки
	static function del(){
		global $USER;
		if ($USER->IsAuthorized()){
			// Инфо об иллюстраторе
			$user = tools\user::info(param_post('illustrator'));
			if( $user ){
				$subscribe_id = static::getEl( param_post('illustrator'), $USER->GetID() );
				if( $subscribe_id ){
					\CIBlockElement::Delete($subscribe_id);
					BXClearCache(true, "/subscribesForIllustrator/".param_post('illustrator')."/");
					$subscribes = project\i_subscribe::getListForIllustrator(param_post('illustrator'));
					return array('status' => 'ok', 'cnt' => count($subscribes));
				} else {
					return array('status' => 'exists', 'text' => 'Подписка не найдена');
				}
			} else {
				return array('status' => 'error', 'text' => 'Ошибка данных');
			}
		} else {
			return array('status' => 'error', 'text' => 'Для подписки необходимо авторизоваться');
		}
	}
	
	
	
	
	
}