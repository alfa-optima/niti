<? 

class site {
	
	const API_ACCESS_KEY = 'db6fea603cb04d02a3b5fb2ed514b750';
	
	const CATALOG_PRICE_ID = 1;
	
	const CATALOG_IBLOCK_ID = 3;
	const COUNTRIES_IBLOCK_ID = 4;
	const SKU_IBLOCK_ID = 5;
	const COLOR_IBLOCK_ID = 6;
	const COMMENTS_IBLOCK_ID = 7;
	const BUY_1_CLICK_IBLOCK_ID = 8;
	const BANNERS_IBLOCK_ID = 9;
	const COLLECTIONS_IBLOCK_ID = 10;
	const SKU_PROCESSING_IBLOCK_ID = 11;
	const MAPS_IBLOCK_ID = 12;
	const CONTACTS_IBLOCK_ID = 13;
	const FEEDBACK_IBLOCK_ID = 14;
	const DELIVERY_IBLOCK_ID = 15;
	const NEWS_IBLOCK_ID = 16;
	const MAIN_ADV_IBLOCK_ID = 17;
	const MAIN_LINKS_IBLOCK_ID = 18;
	const TEMP_FID_IBLOCK = 19;
	const PRINTS_IBLOCK_ID = 20;
	const SEO_IBLOCK_ID = 21;
	const VACANCIES_IBLOCK_ID = 23;
	const CONSTR_GROUPS_IBLOCK_ID = 24;
	const CONSTR_SKU_IBLOCK_ID = 28;
	const CONSTR_COLORS_IBLOCK_ID = 25;
	const CONSTR_TAGS_IBLOCK_ID = 26;
	const CONSTR_ITEMS_IBLOCK_ID = 27;
	
	const CONSTRUCTOR_NOTES = 32;
	
	const ISUBSCRIBES_IBLOCK_ID = 29;
	
	const PHONE_PREFIX = '+7 ';
	
	const NITI_COMMENTS_LOGO = SITE_TEMPLATE_PATH."/images/admin_avatar_img.png";
	
	const LATEST_ARRIVAL_CNT = 8;
	
	const USER_LIKES_SESSION = 'userLikes';

	static $user_pay_types = array(
		3 => 1,
		4 => 2
	);
	
	
	const DEFAULT_DELIVERY_PRICE = '-';
	static $calc_ds_list = array( 
		'delivSDEK' => 3,
		'delivBoxberry' => 4
	);
	

	
	
	
	protected $settingsPath = '/local/php_interface/site_settings/';
	
	// настройки по умолчанию
	protected $settings = array(
		'MAP_CENTER' => array(
			'NAME' => 'Координаты центра карты (магазины)',
			'VALUE' => '55.734435043607, 37.629476418501'
		),
		'MAP_ZOOM' => array(
			'NAME' => 'Масштаб карты (магазины)',
			'VALUE' => 16
		)
	);

	
	
	

	// Получение настроек по-умолчанию
	public function defaultSettings(){
		return $this->settings;
	}
	
	// Получение текущих настроек
	public function getSettings(){
		$settings = $this->defaultSettings();
		$settingsKeys = array_keys($settings);
		foreach ($settingsKeys as $key){
			$value = false;
			$file = $_SERVER['DOCUMENT_ROOT'].$this->settingsPath.$key.'.txt';
			if( file_exists($file) ){
				$value = file_get_contents($file);
				if(strlen($value) > 0){    $settings[$key]['VALUE'] = $value;    }
			}
		}
		return $settings;
	}
	
	// Редактирование настроек
	public function updateSettings($settings){
		$settingsPath = $_SERVER['DOCUMENT_ROOT'].$this->settingsPath;
		if( !file_exists($settingsPath) ){     mkdir($settingsPath, 0700);     }
		if(
			$settings
			&&
			is_array($settings)
			&&
			count($settings) > 0
		){
			$defaultSettings = $this->defaultSettings();
			$defaultSettingsKeys = array_keys($defaultSettings);
			foreach ( $settings as $key => $value ){
				if( in_array($key, $defaultSettingsKeys) ){
					$file_path = $settingsPath.$key.".txt";
					$file = fopen($file_path, "w"); 
					$res = fwrite($file,  $value);
					fclose($file);
				}
			}
		}
	}	
	
	
	
	
	
	
	
	
	
	// Удаление старых файлов
	static function deleteOldImages(){
		// Кешируем
		$obCache = new \CPHPCache();
		$cache_time = 24*60*60; // сутки
		$cache_id = 'deleteOldImages';
		$cache_path = '/deleteOldImages/';
		if( $obCache->InitCache($cache_time, $cache_id, $cache_path) ){   $vars = $obCache->GetVars();   extract($vars);   } elseif($obCache->StartDataCache()){      $date = date("d.m.Y", time());      $obCache->EndDataCache(array('date' => $date));    }
		$cur_date = date("d.m.Y", time());
		// Если даты отличаются, запускаем удаление старых временных файлов
		if ($cur_date != $date){
			// Выбираем элементы старше 1 суток
			global $DB;
			$elements = \CIBlockElement::GetList(Array(), Array("IBLOCK_ID"=>static::TEMP_FID_IBLOCK, '<DATE_ACTIVE_FROM' => date($DB->DateFormatToPHP(FORMAT_DATETIME), time()-86400)), false, false, Array("ID", "NAME", "ACTIVE_FROM"));
			while ($element = $elements->GetNext()){
				\CIBlockElement::Delete($element['ID']);
				\CFile::Delete(intval($element['NAME']));
			}
		}
	}
	
	
	
	// добавляем в спец.инфоблок id временного файла
	static function saveTempEl($fid){
		$el = new CIBlockElement;
		global $DB;
		$fields = Array(
		  "IBLOCK_ID"      => static::TEMP_FID_IBLOCK,
		  "NAME"           => $fid,
		  "ACTIVE_FROM"    => date($DB->DateFormatToPHP(FORMAT_DATETIME), time()),
		  "ACTIVE"         => "Y"
		);
		$el->Add($fields);
	}
	
	
	
	// удаление временного элемента
	static function deleteTempEl($fid){
		$dbElements = \CIBlockElement::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>static::TEMP_FID_IBLOCK, "=NAME"=>$fid), false, Array("nTopCount"=>1), Array("ID", "NAME"));
		while ($element = $dbElements->GetNext()){
			\CIBlockElement::Delete($element['ID']);
		}
	}
	
	
	
	
	
	
	
	

	
	
	
	
	
	
	
}