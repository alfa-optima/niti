<?php

$_SERVER["DOCUMENT_ROOT"] = "/home/bitrix/www";
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
set_time_limit(0);


\Bitrix\Main\Loader::includeModule('aoptima.project');
\Bitrix\Main\Loader::includeModule('aoptima.tools');
\Bitrix\Main\Loader::includeModule('iblock');


$to_delete_product = new \AOptima\project\to_delete_product();

// Получим перечень записей на удаление
$list = $to_delete_product->list();

foreach ( $list as $key => $item ){

    // Инфо об элементе инфоблока
    $el = false;
    $filter = [ "ID" => $item['UF_ID'] ];
    $fields = Array( "ID", "NAME", "DATE_CREATE", "DETAIL_PAGE_URL" );
    $dbElements = \CIBlockElement::GetList(
    	array("SORT"=>"ASC"), $filter, false, array("nTopCount"=>1), $fields
    );
    if ($element = $dbElements->GetNext()){   $el = $element;   }

    // Если есть элемент инфоблока с таким ID
    if( intval($el['ID']) > 0 ){

        // Инфоблок элемента
        $iblock_id = \AOptima\tools\el::getIblock($el['ID']);

        // Если это инфоблок каталога/ТП
        if(
            $iblock_id == \AOptima\project\site::CATALOG_IBLOCK_ID
            ||
            $iblock_id == \AOptima\project\site::SKU_IBLOCK_ID
        ){

            // Удалим элемент инфоблока (товар)
            $res = \CIBlockElement::Delete($item['UF_ID']);
            if( $res ){

                // Удалим запись
                $to_delete_product->delete($item['ID']);
            }

        // Если это НЕ инфоблок каталога/ТП
        } else {

            // Сразу удалим запись
            $to_delete_product->delete($item['ID']);
        }

    // Если НЕТ такого элемента инфоблока
    } else {

        // Сразу удалим запись
        $to_delete_product->delete($item['ID']);
    }

}





require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");