<? require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

header("Content-type: application/json");

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;

$result = project\search::request();

$json = json_encode($result['response'], JSON_HEX_TAG | JSON_NUMERIC_CHECK | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);

echo $json;

