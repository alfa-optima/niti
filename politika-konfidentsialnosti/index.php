<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Политика конфиденциальности");
?>


<div id="app_test">

	<div class="pure-g" style="margin-bottom: 100px">
		<div class="pure-u-1-1" style="margin-bottom: 40px">

			<form class="pure-form">
				<input type="text" class="pure-input-rounded" :value="info.query"/>
				<button type="submit" class="pure-button">Search</button>
			</form>

		</div>

		<div class="pure-u-1-4" v-if="info.categories">

			<h3>Категории</h3>

			<a v-for="item in info.categories"
				 :href="item.link"
				 class="pure-button"
				 style="margin: 10px"
			>{{item.name}}</a>

		</div>

		<div class="pure-u-1-4" v-if="info.illustrators">

			<h3>Иллюстраторы</h3>

			<a v-for="item in info.illustrators"
				 :href="item.link"
				 class="pure-button"
				 style="margin: 10px"
			>{{item.name}}</a>
		</div>

		<div class="pure-u-1-4" v-if="info.prints">

			<h3>Принты</h3>

			<a v-for="item in info.prints"
				 :href="item.link"
				 class="pure-button"
				 style="margin: 10px"
			>{{item.name}}</a>
		</div>

		<div class="pure-u-1-4" v-if="info.collections">

			<h3>Коллекции</h3>

			<a v-for="item in info.collections"
				 :href="item.link"
				 class="pure-button"
				 style="margin: 10px"
			>{{item.name}}</a>
		</div>

	</div>

	<div class="pure-g" style="border: 5px solid #dadada;padding: 40px;">

		<div class="pure-u-1-1">


		</div>


		<div v-if="info.filters" style="padding-right: 50px" :class="{'pure-u-1-4': true, 'filters_box': true, loading: loading}">

			<h2>Фильтры ({{ info.filters.appliedCount }})</h2>


			<br>
			<br>

			<form class="pure-form" style="margin-bottom: 70px">
				<h3>Категории</h3>

				<ul>
					<li>
						Все категории

						<ul style="padding-left: 30px">
							<li>
								{{ categoriesList.label }}

								<ul style="padding-left: 30px">
									<li v-for="item in categoriesList.options">

										<a :href="item.link">{{ item.label }}</a>

									</li>
								</ul>


							</li>

						</ul>

					</li>
				</ul>

			</form>


			<form class="pure-form" v-for="item in info.filters.filters" style="margin-bottom: 70px">

				<h3>{{ item.label }}</h3>

				<label :for="item.type + itemList.name" class="pure-radio"
							 v-for="itemList in item.items"
							 v-if="item.experiences[0].value === 'radio'"
				>
					<input type="radio"
								 :id="item.type + itemList.name"
								 :name="item.type"
								 :value="item.type + itemList.name"
								 :checked="itemList.applied"
								 :disabled="itemList.disabled"
								 v-on:change="go(itemList.link)"
					/>{{ itemList.label }}</label>


				<label :for="item.type + itemList.name" class="pure-checkbox"
							 v-for="itemList in item.items"
							 v-if="item.experiences[0].value === 'checkbox'"
				>
					<input type="checkbox"
								 :id="item.type + itemList.name"
								 :value="item.type + itemList.name"
								 :checked="itemList.applied"
								 :disabled="itemList.disabled"
								 v-on:change="go(itemList.link)"
					/>{{ itemList.label }}</label>


			</form>


		</div>

		<div v-if="info.metadata && info.filters" class="pure-u-3-4" style="margin-bottom: 300px">

			<div style="width: 100%; border-bottom: 2px solid #000; padding-bottom: 20px; margin-bottom: 30px">
				Вы искали <b>{{ info.metadata.title }}</b> <i style="margin-left: 20px">{{ info.metadata.resultCount }}
					результатов</i>
			</div>


			<div style="width: 100%; padding-bottom: 20px;">
				<a class="pure-button" style="margin: 5px" :href="item.link" v-for="item in info.filters.resets">{{item.label}} ×</a>

				<a class="pure-button pure-button-primary" :href="info.filters.resetUrl">Сбросить все фильтры</a>
			</div>


			<table class="pure-table" style="width: 100%">
				<thead>
				<tr>
					<th>#</th>
					<th>Название</th>
					<th>Цена</th>
					<th>Ссылка</th>
				</tr>
				</thead>
				<tbody>
				<tr v-for="item in info.products">
					<td>{{item.id}}</td>
					<td>{{item.name}}</td>
					<td>{{item.price}}</td>
					<td>{{item.link}}</td>
				</tr>

				</tbody>
			</table>

			<div v-if="info.pagination" style="margin-top:100px">


				<br>
				<br>

				<a class="pure-button pure-button-primary" :href="info.pagination.paginationLinks.previousPage">Назад</a>
				<a class="pure-button pure-button-primary" :href="info.pagination.paginationLinks.nextPage">Вперед</a>

				<br>
				<br>

				Показано с <b>{{ info.pagination.fromNumber }}</b> по <b>{{ info.pagination.toNumber }}</b> из <b>{{
					info.pagination.total }}</b> строк
			</div>


		</div>

	</div>

	<pre>{{ info }}</pre>

</div>

<script>
new Vue({
	data: function () {
		return {

			loading: false,

			info: {
				query: '',
				appliedCount: 0,
				filters: {
					appliedCount: 0
				},
				categories: [],
				products: [],
				pagination: null
			}
		}
	},
	computed: {
		categoriesList: function () {
			var index = 0
			if (typeof this.info.filters.staticFilters[index] !== 'undefined') {
				return this.info.filters.staticFilters[index]
			} else {
				return []
			}
		}
	},
	mounted: function () {
		axios
			//.get( 'https://21cf5605-d81f-4fac-9f2c-0ed29cf5ce7d.mock.pstmn.io' + '/search' )
			.get('http://new.niti-niti.ru/test/?query=%D1%81%D0%B5%D1%80%D0%B4%D1%86%D0%B5')
			//.then(response => (this.info = response.data))
			.then(response => {
				console.log(response)
				this.info = response.data
			})
	},
	methods: {

		setLocation: function (curLoc){
			location.href = curLoc;
			location.hash = curLoc;
		},

		search: function (url){

			this.loading = true

			let vm = this

			axios
				.get(url)
				.then(response => {
					console.log(response)
					this.info = response.data

					vm.loading = false

					// vm.setLocation(url);

				})
		},

		go: function (url){
			url = 'http://new.niti-niti.ru' + url
			this.search(url)
		}

	}
}).$mount('#app_test')
</script>

<style>
.pure-u-1-4.filters_box {
    transition: all 1.5s ease-in-out;
    filter: blur(0px);
    background: #fff;
}

.pure-u-1-4.loading {
    background: #ea0303;
    filter: blur(2px);
}
</style>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
