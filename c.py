﻿import shutil, os

def new_component():

    projectDir = os.getcwd()
    componentsDir = projectDir + "\\local\\components\\aoptima\\"

    compName = input('Component name: ')

    componentDir = componentsDir + compName + "\\"

    if not os.path.exists( componentDir ):
        os.makedirs( componentDir )

    f1 = open(componentDir+'component.php', 'tw', encoding='utf-8')
    f1.write("<? if(!defined(\"B_PROLOG_INCLUDED\") || B_PROLOG_INCLUDED!==true)die();\n/** @var array $arParams */\n/** @var array $arResult */\n\n\n\n\n\n\n\n\n$this->IncludeComponentTemplate();")
    f1.close()

    templatesDir = componentDir + "templates\\"
    if not os.path.exists( templatesDir ):
        os.makedirs( templatesDir )

    defaultTemplateDir = templatesDir + ".default\\"
    if not os.path.exists( defaultTemplateDir ):
        os.makedirs( defaultTemplateDir )

    f2 = open(defaultTemplateDir+'template.php', 'tw', encoding='utf-8')
    f2.write("<? if(!defined(\"B_PROLOG_INCLUDED\") || B_PROLOG_INCLUDED!==true)die();\n/** @var array $arParams */\n/** @var array $arResult */ ?>\n\n\n")
    f2.close()

    new_component()

new_component()