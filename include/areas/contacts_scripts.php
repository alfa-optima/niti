<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

// Получаем карты
$maps = getMaps();
if ( count($maps) > 0 ){ ?>

	<script src="http://api-maps.yandex.ru/2.0/?load=package.standard,package.geoObjects&lang=ru-RU"></script>

	<script>
		$(window).load(function(){
			
			function init(){
				
				<? // Перебираем карты
				foreach ( $maps as $map ){ ?>
				
					var myMap<?=$map['ID']?> = new ymaps.Map("map<?=$map['ID']?>", {
						center: ["<?=$map['UF_DOLGOTA']?>", "<?=$map['UF_SHIROTA']?>"],
						zoom: "<?=$map['UF_ZOOM']?>"
					})
					
					myMap<?=$map['ID']?>.controls.add('zoomControl', {
						size: "large"
					});
					
					<? // Получаем точки для карты
					$mapPoints = getMapPoints($map['ID']);
					if( count($mapPoints) > 0 ){ 
						// Перебираем точки карты
						foreach ($mapPoints as $mapPoint){ ?>
					
							myPlacemark = new ymaps.Placemark(["<?=$mapPoint['PROPERTY_DOLGOTA_VALUE']?>", "<?=$mapPoint['PROPERTY_SHIROTA_VALUE']?>"], {}, {
								iconImageHref: '/local/templates/my/images/map_marker.png',
								iconImageSize: [51, 51],
								iconImageOffset: [-26, -26]
							})

							myMap<?=$map['ID']?>.geoObjects.add(myPlacemark)
					
						<? }
					}
					
				} ?>
				
			}

			ymaps.ready(init)
		})
	</script>
	
<? } ?>