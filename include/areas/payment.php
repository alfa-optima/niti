<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

\Bitrix\Main\Loader::includeModule('sale');
use \Bitrix\Sale; ?>

<h3><? $APPLICATION->IncludeFile("/inc/pay_text.inc.php", Array(), Array("MODE"=>"html")); ?></h3>

<br>

<div class="methods">

	<? $cnt = 0;   
	$db_ptype = \CSalePaySystem::GetList(
		Array(
			"SORT"=>"ASC",
			"PSA_NAME"=>"ASC"
		),
		Array(
			"ACTIVE" => "Y",
			"!ID" => 6
		),
		false, false,
		array('ID', 'NAME', 'PSA_LOGOTIP', 'DESCRIPTION')
	);
	while ($ps = $db_ptype->GetNext()){ $cnt++; ?>
	
		<div class="item">
			<div class="icon">
				<? if( intval($ps['PSA_LOGOTIP']) > 0 ){ ?>
					<img src="<?=\CFile::GetPath($ps['PSA_LOGOTIP'])?>">
				<? } ?>
			</div>
			<div class="name"><?=$ps['NAME']?></div>
		</div>
		
	<? } ?>

</div>