<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use \Bitrix\Main,
    \Bitrix\Main\Localization\Loc as Loc,
    \Bitrix\Main\Loader,
    \Bitrix\Main\Config\Option,
    \Bitrix\Sale\Delivery,
    \Bitrix\Sale\PaySystem,
    \Bitrix\Sale,
    \Bitrix\Sale\Order,
    \Bitrix\Sale\DiscountCouponsManager,
    \Bitrix\Main\Context;
\Bitrix\Main\Loader::includeModule("main");   \Bitrix\Main\Loader::includeModule("iblock");
\Bitrix\Main\Loader::includeModule("catalog");   \Bitrix\Main\Loader::includeModule("sale");

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;


$APPLICATION->SetPageProperty("title", 'Мои принты');
$APPLICATION->AddChainItem('Мои принты', pureURL());

// Если авторизован
if( $USER->IsAuthorized() ){

	$user = tools\user::info($USER->GetID());
	if( $user['UF_USER_TYPE'] != 2 ){
		LocalRedirect("/personal/");
	}

	// top_menu
	$APPLICATION->IncludeComponent(
		"bitrix:menu", "personal_menu",
		array(
			"ROOT_MENU_TYPE" => "personal_menu",
			"MENU_CACHE_TYPE" => "N",
			"MENU_CACHE_TIME" => "3600",
			"MENU_CACHE_USE_GROUPS" => "N",
			"CACHE_SELECTED_ITEMS" => "N",
			"MENU_CACHE_GET_VARS" => array(
			),
			"MAX_LEVEL" => "1",
			"CHILD_MENU_TYPE" => "left",
			"USE_EXT" => "N",
			"DELAY" => "N",
			"ALLOW_MULTI_SELECT" => "N",
			"COMPONENT_TEMPLATE" => "footer_menu",
		),
		false
	); ?>

	<div class="lk_orders">

		<iframe id="upload___Frame" name="upload___Frame" style="display: none"></iframe>
		<form style="visibility:hidden; height:0" class="printUpload___form" action="/ajax/uploadPrintPhoto.php" target="upload___Frame" method="post" enctype="multipart/form-data">
			<input type="file" name="printPhotoInput" style="visibility:hidden; height:0">
		</form>

		<aside class="right"></aside>

		<section class="content left">
			<form class="form">

				<div class="lk_title">Создание нового принта</div>

				<div class="lines">

					<div class="line">
						<div class="name">Название принта:</div>
						<div class="field">
							<input type="text" name="print_name" placeholder="Название принта" class="input">
						</div>
						<div class="field">
							<button type="button" class="submit_btn addPrintButton to___process">Сохранить</button>
						</div>
					</div>

					<div class="line">
						<div class="name">Фото принта:</div>
						<div class="field">
							<? // print_photo_block
							$APPLICATION->IncludeComponent("my:print_photo_block", ""); ?>
						</div>
					</div>

				</div>

				<p class="error___p"></p>

			</form>

		</section>
		<div class="clear"></div>


		<? // Список принтов пользователя
		$catalogPrint = new project\catalog_print();
		$userPrints = $catalogPrint->getList($USER->GetID());

		if( count($userPrints) > 0 ){ ?>

			<section class="collections">
				<div class="cont">

					<? if( count($userPrints['ACTIVE']) > 0 ){ ?>

						<?//<div class="block_title"><div>Одобренные</div></div>?>

						<div class="hor_scroll">
							<div class="grid">

								<div class="item_wrap">

									<? foreach( $userPrints['ACTIVE'] as $print_id ){
										$print = tools\el::info($print_id);
										if( intval($print['ID']) > 0 ){ ?>

											<a class="item fancy_img" href="<?=rIMGG($print['PROPERTY_PHOTO_ID_VALUE'], 4, 800, 800)?>" class="fancy_img" data-fancybox="gallery">
												<img src="<?=rIMGG($print['PROPERTY_PHOTO_ID_VALUE'], 5, 270, 180)?>">
												<div class="name"><?=$print['NAME']?></div>
											</a>

											<? echo '</div><div class="item_wrap">';  ?>

										<? }
									} ?>

								</div>

							</div>
						</div>

					<? } ?>

					<? if( count($userPrints['NOT_ACTIVE']) > 0 ){ ?>

						<div class="block_title"><div>Ещё не одобренные</div></div>

						<div class="hor_scroll">
							<div class="grid">

								<div class="item_wrap">

									<? foreach( $userPrints['NOT_ACTIVE'] as $print_id ){
										$print = tools\el::info($print_id);
										if( intval($print['ID']) > 0 ){ ?>

											<a class="item fancy_img" href="<?=rIMGG($print['PROPERTY_PHOTO_ID_VALUE'], 4, 800, 800)?>" class="fancy_img" data-fancybox="gallery">
												<img src="<?=rIMGG($print['PROPERTY_PHOTO_ID_VALUE'], 5, 270, 180)?>">
												<div class="name"><?=$print['NAME']?></div>
											</a>

											<? echo '</div><div class="item_wrap">';  ?>

										<? }
									} ?>

								</div>

							</div>
						</div>

					<? } ?>

				</div>
			</section>


		<? } else { ?>

			<p class="no___count">Вы ещё не загрузили ни одного принта</p>

		<? } ?>

	</div>


<? // НЕ авторизован
} else {


	// personal_auth_block
	include $_SERVER["DOCUMENT_ROOT"].'/include/areas/personal_auth_block.php';


} ?>
