<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); 
use \Bitrix\Main,
    \Bitrix\Main\Localization\Loc as Loc,
    \Bitrix\Main\Loader,
    \Bitrix\Main\Config\Option,
    \Bitrix\Sale\Delivery,
    \Bitrix\Sale\PaySystem,
    \Bitrix\Sale,
    \Bitrix\Sale\Order,
    \Bitrix\Sale\DiscountCouponsManager,
    \Bitrix\Main\Context;
\Bitrix\Main\Loader::includeModule("main");   \Bitrix\Main\Loader::includeModule("iblock");
\Bitrix\Main\Loader::includeModule("catalog");   \Bitrix\Main\Loader::includeModule("sale");

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

\Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project;


$APPLICATION->SetPageProperty("title", 'Конструктор');
$APPLICATION->AddChainItem('Конструктор', pureURL());

// Если авторизован
if( $USER->IsAuthorized() ){
	
	if( !$USER->IsAdmin() && !in_array(6, CUser::GetUserGroup($USER->GetID())) ){
		LocalRedirect("/personal/");
	}

	// top_menu
	$APPLICATION->IncludeComponent(
		"bitrix:menu", "personal_menu", 
		array(
			"ROOT_MENU_TYPE" => "personal_menu",
			"MENU_CACHE_TYPE" => "N",
			"MENU_CACHE_TIME" => "3600",
			"MENU_CACHE_USE_GROUPS" => "N",
			"CACHE_SELECTED_ITEMS" => "N",
			"MENU_CACHE_GET_VARS" => array(
			),
			"MAX_LEVEL" => "1",
			"CHILD_MENU_TYPE" => "left",
			"USE_EXT" => "N",
			"DELAY" => "N",
			"ALLOW_MULTI_SELECT" => "N",
			"COMPONENT_TEMPLATE" => "footer_menu",
		),
		false
	);

	
	
	// Карточка
	if( intval(param_get('id')) > 0 ){ 
		// Инфо об элементе
		$item = tools\el::info(param_get('id')); 
		$print = tools\el::info($item['PROPERTY_PRINT_VALUE']);
	}
	if ( intval($item['ID']) > 0 ){ ?>
		
	
		<div class="configurator">
		
		
			<style type="text/css">
				#constructor_preview{
					position: relative;
					width: 100%;
					height: 100%;
					min-height: 539px;
					outline: 1px solid red;
					float: left;
				}
			</style>
			
			
			<div class="image left">
			
				<canvas id="constructor_preview"></canvas>

			</div>
			

			<div class="data right">
			
				<? // Группы товаров
				$groups = project\constructor::getGroups(); ?>

				<div class="step_info step_info1" style="display: block;">
				
					<div class="type">
						<? foreach ( $groups as $group ){ 
							if(
								count($group['elements']) > 0
								&&
								$item['PROPERTY_SECTION_VALUE']==$group['ID']
							){ ?>
						
								<input type="radio" name="section" id="group_<?=$group['ID']?>" <?=$item['PROPERTY_SECTION_VALUE']==$group['ID']?'checked':''?> value="<?=$group['ID']?>">
								
								<label for="group_<?=$group['ID']?>" data-content=".group_<?=$group['ID']?>"><?=$group['NAME']?></label>
							
							<? }
						} ?>
					</div>

					<? foreach ( $groups as $group ){
						if(
							count($group['elements']) > 0
							&&
							$item['PROPERTY_SECTION_VALUE']==$group['ID']
						){ ?>
					
							<div class="type_content group_<?=$group['ID']?>" <? if( $item['PROPERTY_SECTION_VALUE']==$group['ID'] ){ ?>style="display: block;"<? } ?>>
								<div class="grid">
								
									<? foreach ( $group['elements'] as $element_id ){ 
										$el = tools\el::info($element_id);
										if(
											intval($el['ID']) > 0
											&&
											$item['PROPERTY_ODEZHDA_VALUE']==$element_id
										){ ?>
									
											<input type="radio" name="odezhda" id="odezhda_<?=$el['ID']?>" <?=$item['PROPERTY_ODEZHDA_VALUE']==$element_id?'checked':''?> value="<?=$element_id?>">
											
											<label for="odezhda_<?=$el['ID']?>" class="set-background" data-src="<?=$_SERVER['REQUEST_SCHEME']?>://<?=$_SERVER['SERVER_NAME']?><?=\CFile::GetPath($el['PREVIEW_PICTURE'])?>">
												<div class="icon">
													<? $f_info = \CFile::GetFileArray($el['DETAIL_PICTURE']); ?>
													<div class="icon1" style="background: url(<?=\CFile::GetPath($el['DETAIL_PICTURE'])?>) 0 0 no-repeat; width:<?=$f_info['WIDTH']?>px; height: <?=round($f_info['HEIGHT']/2, 0)?>px"></div>
												</div>
												<div class="name"><?=$el['NAME']?></div>
											</label>
										
										<? }
									} ?>

								</div>
							</div>
							
						<? }
					} ?>
					

					<input type="hidden" name="print_id" value="<?=$item['PROPERTY_PRINT_VALUE']?>">
					<? $image = $_SERVER['DOCUMENT_ROOT'].\CFile::GetPath($print['PROPERTY_PHOTO_ID_VALUE']);
					$base64 = base64_from_image($image); ?>
					<input type="hidden" name="printBase64" value="<?=$base64?>">
					
					
					<div class="constructor_buttons_block" style="display:block">
						<button class="to___process" id="zoom-in" style="width: 40px;">+</button>
						<button class="to___process" id="zoom-out" style="width: 40px;">-</button>
						<button class="to___process" id="move-left" style="width: 40px;">←</button>
						<button class="to___process" id="move-right" style="width: 40px;">→</button>
						<button class="to___process" id="move-up" style="width: 40px;">↑</button>
						<button class="to___process" id="move-down" style="width: 40px;">↓</button>
					</div>
					

					<div class="line">
					
						<div class="name">Выбран принт: &nbsp; <span class="print_name_span"><?=$print['NAME'].' (ID '.$print['ID'].')'?></span></div>
						
						<div class="form" style="margin-bottom: 15px;">
							<div class="agree">
								<input type="checkbox" name="update_print_name" id="update_print_name" value="1">
								<label for="update_print_name">Изменить название принта</label>
							</div>
						</div>
						
						<div class="field update_print_name_block" style="display:none">
							<div class="name">Новое название принта:</div>
							<input type="text" name="print_name" class="input">
						</div>
						
					</div>

					
					<div class="line">
					
						<div class="name">Укажите теги <small>(не более 25)</small></div>
						
						<div class="name"><small><i>- введите тег и нажмите Enter</i></small></div>
						
						<div class="name"><small><i>- для удаления тега из списка кликните по нему</i></small></div>

						<div class="field">
							<input type="text" name="constructor_tag" class="input" placeholder="Тег">
						</div>

						<div class="tags constructor___tags">
							<div class="grid"><? if( count($item['PROPERTY_TAGS_VALUE']) > 0 ){
								foreach ( $item['PROPERTY_TAGS_VALUE'] as $tag ){
									echo '<a class="tag___item">'.trim($tag).'</a>';
								}
							} ?></div>
						</div>
						
					</div>
					
					
					<div class="line">
						<div class="name">Цена товара, руб.</small></div>
						<div class="field">
							<input type="text" name="price" class="input">
						</div>
					</div>
				
				
					<div class="line">
						<div class="name">Укажите варианты цветов для товара</div>
						<div class="color constructor_colors_block"></div>
						<script type="text/javascript">
						$(document).ready(function(){    getConstructorColors('<?=implode('|', $item['PROPERTY_COLORS_VALUE'])?>');    });
						</script>
					</div>

					
				
					<? // Коллекции
					$collections = project\collection::getList();
					if ( count($collections) > 0 ){ ?>
					
						<div class="line">
						
							<div class="name">Поиск коллекций</small></div>

							<div class="field">
								<input type="text" name="constr_coll_search" class="input search_input">
							</div>

							<div class="collections">
								<div class="scroll">
								
									<? foreach( $collections as $collection ){ ?>
									
										<div class="line">
											<input <? if( in_array($collection['ID'], $item['PROPERTY_COLLECTIONS_VALUE']) ){ ?>checked<? } ?> type="radio" name="collection" id="collection_<?=$collection['ID']?>" value="<?=$collection['ID']?>" coll_value="<?=strtolower($collection['NAME'])?>">
											<label for="collection_<?=$collection['ID']?>"><?=$collection['NAME']?></label>
										</div>
										
									<? } ?>

								</div>
							</div>
						</div>
						
					<? } ?>

					<p class="error___p"></p>
					
					<input type="hidden" name="item_id" value="<?=$item['ID']?>">
					<input type="hidden" name="x" value="<?=$item['PROPERTY_X_VALUE']?>">
					<input type="hidden" name="y" value="<?=$item['PROPERTY_Y_VALUE']?>">
					<input type="hidden" name="zoom" value="<?=$item['PROPERTY_ZOOM_VALUE']?>">
					<input type="hidden" name="user_id" value="<?=$item['PROPERTY_USER_VALUE']?>">
					
					
					<? // ТП элемента
					$dbElements = \CIBlockElement::GetList(
						Array("SORT"=>"ASC"),
						Array(
							"IBLOCK_ID" => project\site::CONSTR_SKU_IBLOCK_ID,
							"ACTIVE"=>"Y",
							"PROPERTY_CML2_LINK" => $item['PROPERTY_ODEZHDA_VALUE']
						), false, false, 
						Array(
							"ID", "NAME", "PREVIEW_PICTURE",
							"PROPERTY_COLOR_ID", "PROPERTY_CML2_LINK"
						)
					);
					while ($element = $dbElements->GetNext()){ ?>
						<input class="item___pics" type="hidden" color_id="<?=$element['PROPERTY_COLOR_ID_VALUE']?>" src="<?=$_SERVER['REQUEST_SCHEME']?>://<?=$_SERVER['SERVER_NAME']?><?=\CFile::GetPath($element['PREVIEW_PICTURE'])?>" value="<?=base64_from_image($_SERVER['DOCUMENT_ROOT'].\CFile::GetPath($element['PREVIEW_PICTURE']))?>">
						<input class="picBase64" type="hidden" color_id="<?=$element['PROPERTY_COLOR_ID_VALUE']?>">
					<? } ?>

					
					<div class="submit left">
						<button type="button" class="submit_btn cancelConstructorButton to___process">Отклонить / Удалить</button>
					</div>
					
					<div class="submit right">
						<button type="button" class="submit_btn approvalConstructorButton to___process">Согласовать</button>
					</div>

					<div class="clear"></div>
				</div>

			</div>
			
			<div class="clear"></div>
		</div>
			
		
	<? } else { ?>
		
		
		<? // Товары на соглсование
		$items = project\constructor::getFinishedNotes();
		if( count($items) > 0 ){
			
			$r_elements = new \CDBResult;
			$r_elements->InitFromArray($items);
			$r_elements->NavStart(2);
			$NAV_STRING = $r_elements->GetPageNavStringEx(
                $navComponentObject, false, false, false, false
            ); ?>
		
			<table class="constructor_approval_table">
				<tr>
					<th>ID записи</th>
					<th>Группа товаров</th>
					<th>Вид одежды</th>
					<th>Принт</th>
					<th>Иллюстратор</th>
					<th>Дата/время</th>
<!--					<th></th>-->
				</tr>
				
				<? while ($el_id = $r_elements->Fetch()){ 
				
					$el = tools\el::info($el_id); ?>
					
					<tr>
						<td><?=$el['ID']?></td>
						<td><?=tools\section::info($el['PROPERTY_SECTION_VALUE'])['NAME']?> [<?=$el['PROPERTY_SECTION_VALUE']?>]</td>
						<td><?=tools\el::info($el['PROPERTY_ODEZHDA_VALUE'])['NAME']?> [<?=$el['PROPERTY_ODEZHDA_VALUE']?>]</td>
						<td><?=tools\el::info($el['PROPERTY_PRINT_VALUE'])['NAME']?> [<?=$el['PROPERTY_PRINT_VALUE']?>]</td>
						<td><?=tools\user::info($el['PROPERTY_USER_VALUE'])['NAME']?><? if( strlen(tools\user::info($el['PROPERTY_USER_VALUE'])['LAST_NAME']) > 0 ){ echo ' '.tools\user::info($el['PROPERTY_USER_VALUE'])['LAST_NAME']; } ?> [<?=$el['PROPERTY_USER_VALUE']?>]</td>
						<td><?=$el['DATE_CREATE']?></td>
<!--						<td><a href="/personal_approval/?id=--><?//=$el['ID']?><!--">Смотреть</a></td>-->
					</tr>
					
				<? } ?>
				
			</table>
			
			<? echo $NAV_STRING; ?>
			
		<? } else { ?>
		
			<p class="no___count">Нет новых заявок на согласование</p>
		
		<? } ?>
		
	<? } ?>
	
	
<? // НЕ авторизован
} else { 


	// personal_auth_block
	include $_SERVER["DOCUMENT_ROOT"].'/include/areas/personal_auth_block.php';

	
} ?>