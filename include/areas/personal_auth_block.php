<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>


<div class="lk_personal">

	<aside class="right"></aside>


	<!-- Основная часть -->
	<section class="content left">
	
		<form class="form" method="post">
		
			<div class="lk_title">Авторизация:</div>

			<div class="lines">
			
				<div class="line">
					<div class="name">Email/логин</div>

					<div class="field">
						<input type="text" name="login" placeholder="Ваш Email/логин" class="input">
					</div>
				</div>


				<div class="line">
					<div class="name">Пароль</div>

					<div class="field">
						<input type="password" name="password" placeholder="Ваш пароль" class="input">
					</div>
				</div>

			</div>
			
			<p class="error___p"></p>

			<div class="submit">
				<button type="button" class="submit_btn auth___button to___process">Войти</button>
			</div>
			
		</form>


		
	</section>
	<div class="clear"></div>

</div>
