<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<div class="cont">

	<section class="contacts_info">
	
		<div class="block_title"><div>Контакты</div></div>

		<? // Получаем карты
		$maps = getMaps();
		if ( count($maps) > 0 ){ 
			// Перебираем карты
			foreach ( $maps as $map ){ ?>
				<div class="contacts___map" id="map<?=$map['ID']?>"></div>
			<? }
		}

		
		
		// Получаем группы контактных данных
		$сontactGroups = getContactGroups();
		if ( count($сontactGroups) > 0 ){ ?>
		
			<div class="info">
		
				<? // Перебираем группы контактных данных
				foreach ( $сontactGroups as $сontactGroup ){ ?>

					<div class="item_wrap">
						<div class="item">
							<div class="icon">
								<img src="<?=rIMGG($сontactGroup['PICTURE'], 4, 33, 33)?>">
							</div>

							<div class="title"><?=$сontactGroup['NAME']?></div>

							<? // Инфа группы
							$elements = getContactGroupElements($сontactGroup['ID']);
							if( count($elements) > 0 ){ ?>
							
								<div class="val">
								
									<? foreach ($elements as $element){
										
										if( count($elements) > 1 ){ ?><div><? }
										
										if( strlen( $element['PROPERTY_DESCR_VALUE'] ) > 0 ){
											echo $element['PROPERTY_DESCR_VALUE'].': ';
										} 
									
										if( strlen($element['PROPERTY_LINK_VALUE']) > 0 ){
											echo '<a rel="nofollow" target="blank" href="'.$element['PROPERTY_LINK_VALUE'].'">';
										}
									
										echo $element['NAME'];
										
										if( strlen($element['PROPERTY_LINK_VALUE']) > 0 ){
											echo '</a>';
										}
										
										if( count($elements) > 1 ){ ?></div><? }
										
									} ?>
									
								</div>
								
							<? } ?>
								
						</div>
					</div>			
				
				<? } ?>
				
			</div>
			
		<? } ?>

		
		


		<div class="feedback">
			<div class="title">Обратная связь</div>

			<form class="form">
				<div class="lines">
				
					<div class="line">
						<div class="name">Имя</div>

						<div class="field">
							<input type="text" name="name" class="input">
						</div>
					</div>

					<div class="line">
						<div class="name">Телефон</div>

						<div class="field">
							<div class="phone_code">+7</div>
							<input type="tel" name="phone" class="input">
						</div>
					</div>

					<div class="line">
						<div class="name">E-mail</div>

						<div class="field">
							<input type="email" name="email" class="input">
						</div>
					</div>
					
				</div>

				<div class="line">
					<div class="name">Текст сообщения</div>

					<div class="field">
						<textarea name="message"></textarea>
					</div>
				</div>

				<p class="error___p"></p>
				
				<div class="submit">
					<button type="button" class="submit_btn feedback___button">ОТправить</button>
				</div>
			</form>
			
		</div>
	</section>

</div>

