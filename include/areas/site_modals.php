<? \Bitrix\Main\Loader::includeModule('aoptima.project');
use AOptima\Project as project; ?>

<div class="modal modal___block" id="order_login_modal">
	<div class="modal_title">Авторизация</div>

	<form class="form" method="post">
	
		<div class="line">
			<p class="order_auth_p">Указанный Вами Email уже зарегистрирован на сайте.</p>
			<p class="order_auth_p">Введите Ваш пароль!</p>
		</div>
		
		<div class="line">
			<div class="field">
				<input type="password" name="password" class="input" placeholder="Пароль">
			</div>
		</div>
		
		<p class="error___p"></p>

		<div class="submit">
			<button type="button" class="submit_btn left order_auth___button to___process">Авторизоваться</button>
		</div>
		
	</form>

</div>





<div class="modal modal___block" id="recovery_modal">

	<div class="modal_title">Забыли пароль?</div>

	<form class="form" method="post">
	
		<p style="margin-bottom:20px">На Ваш Email придёт ссылка для восстановления пароля</p>
	
		<div class="line">
			<div class="field">
				<input type="text" name="email" class="input" placeholder="Ваш Email">
			</div>
		</div>
		
		<p class="error___p"></p>

		<div class="submit">
			<button type="button" class="submit_btn right recovery___button to___process">Запрос</button>
		</div>
		
	</form>

</div>





<div class="modal modal___block" id="login_modal">

	<div class="modal_title">Вход</div>

	<form class="form" method="post">
	
		<div class="line">
			<div class="field">
				<input type="text" name="login" class="input" placeholder="Ваш Email">
			</div>
		</div>

		<div class="line">
			<a href="#recovery_modal" class="recovery_link modal_link right">Напомнить пароль</a>
			<div class="clear"></div>

			<div class="field">
				<input type="password" name="password" class="input" placeholder="Пароль">
			</div>
		</div>

		<!--
		<div class="line">
			<input type="checkbox" name="remember" id="remember_check" value="1">
			<label for="remember_check">Запомнить меня</label>
		</div>
		-->
		
		<p class="error___p"></p>

		<div class="submit">
			<button type="button" class="submit_btn left auth___button to___process">Войти</button>

			<a href="#register_modal" class="register_link modal_link right">Регистрация</a>
			<div class="clear"></div>
		</div>
	</form>

	<!--<div class="soc_auth">
		<div class="title">Войти с помощью социальной сети</div>

		<div class="links">
			<a href="#" class="soc1">
				<img src="<?=SITE_TEMPLATE_PATH?>/images/ic_soc_auth1.png" alt="">
			</a>

			<a href="#" class="soc2">
				<img src="<?=SITE_TEMPLATE_PATH?>/images/ic_soc_auth2.png" alt="">
			</a>

			<a href="#" class="soc3">
				<img src="<?=SITE_TEMPLATE_PATH?>/images/ic_soc_auth3.png" alt="">
			</a>

			<a href="#" class="soc4">
				<img src="<?=SITE_TEMPLATE_PATH?>/images/ic_soc_auth4.png" alt="">
			</a>
		</div>
	</div>--->
	
</div>



<div class="modal modal___block" id="register_modal">
	<div class="modal_title">Регистрация</div>

	<div class="form">
	
		<!--<div class="line">
			<div class="type">
				<input type="radio" name="reg_payer_type" id="pay_type_check1" value="fiz">
				<label for="pay_type_check1">Физ. лицо</label>

				<input type="radio" name="reg_payer_type" id="pay_type_check2" value="yur">
				<label for="pay_type_check2">Юр. лицо</label>
				<div class="clear"></div>
			</div>
		</div>-->

		<form class="reg_fiz_form">
		
			<div class="line">
				<div class="type">
					<input type="radio" name="user_type" id="reg_type_check1" value="1" checked>
					<label for="reg_type_check1">Покупатель</label>

					<input type="radio" name="user_type" id="reg_type_check2" value="2">
					<label for="reg_type_check2">Иллюстратор</label>
					<div class="clear"></div>
				</div>
			</div>
		
			<input type="hidden" name="UF_PAYER_TYPE" value="fiz">
			
			<div class="line">
				<div class="field">
					<input type="text" name="NAME" class="input" placeholder="Имя">
				</div>
			</div>

			<div class="line">
				<div class="field">
					<input type="email" name="EMAIL" class="input" placeholder="E-mail">
				</div>
			</div>

			<div class="line">
				<div class="field">
					<input type="password" name="PASSWORD" class="input" placeholder="Пароль">
				</div>
			</div>

			<div class="line">
				<div class="field">
					<input type="password" name="CONFIRM_PASSWORD" class="input" placeholder="Повторите пароль">
				</div>
			</div>

			<div class="line">
				<div class="gender">
					<div class="item">
						<input type="radio" name="PERSONAL_GENDER" id="gender_check1" value="M" checked>
						<label for="gender_check1">Мужчина</label>
					</div>

					<div class="item">
						<input type="radio" name="PERSONAL_GENDER" value="F" id="gender_check2">
						<label for="gender_check2">Женщина</label>
					</div>
				</div>
			</div>
			
			<div class="agree">
				<input type="checkbox" name="agree" id="agree_check22" value="1">
				<label for="agree_check22">Регистрируясь, вы соглашаетесь с условиями <a href="/oferta/" target="_blank">оферты</a> и <a href="/politika-konfidentsialnosti/" target="_blank">политики конфиденциальности</a>.</label>
			</div>

			<p class="error___p"></p>

			<div class="submit">
				<button type="button" class="submit_btn reg___button to___process">зарегистрироваться</button>
			</div>
			
		</form>

	</div>
</div>



<div class="modal modal___block" id="register_modal_yur">
	<div class="modal_title">Регистрация</div>

	<div class="form">
	
		<!--<div class="line">
			<div class="type">
				<input type="radio" name="reg_payer_type" id="pay_type_check1" value="fiz">
				<label for="pay_type_check1">Физ. лицо</label>

				<input type="radio" name="reg_payer_type" id="pay_type_check2" value="yur">
				<label for="pay_type_check2">Юр. лицо</label>
				<div class="clear"></div>
			</div>
		</div>-->
		
		<form class="reg_yur_form">
		
			<div class="line">
				<div class="type">
					<input type="radio" name="user_type" id="reg_type_check11" value="1" checked>
					<label for="reg_type_check11">Покупатель</label>

					<input type="radio" name="user_type" id="reg_type_check22" value="2">
					<label for="reg_type_check22">Иллюстратор</label>
					<div class="clear"></div>
				</div>
			</div>
		
			<input type="hidden" name="UF_PAYER_TYPE" value="yur">
			
			<div class="line">
				<div class="field">
					<input type="text" name="UF_COMPANY_NAME" class="input" placeholder="Название компании">
				</div>
			</div>
			
			<div class="line">
				<div class="field">
					<input type="text" name="NAME" class="input" placeholder="Имя контактного лица">
				</div>
			</div>

			<div class="line">
				<div class="field">
					<input type="email" name="EMAIL" class="input" placeholder="E-mail">
				</div>
			</div>

			<div class="line">
				<div class="field">
					<input type="password" name="PASSWORD" class="input" placeholder="Пароль">
				</div>
			</div>

			<div class="line">
				<div class="field">
					<input type="password" name="CONFIRM_PASSWORD" class="input" placeholder="Повторите пароль">
				</div>
			</div>
			
			<div class="agree">
				<input type="checkbox" name="agree" id="agree_check20" value="1">
				<label for="agree_check20">Регистрируясь, вы соглашаетесь с условиями <a href="/oferta/" target="_blank">оферты</a> и <a href="/politika-konfidentsialnosti/" target="_blank">политики конфиденциальности</a>.</label>
			</div>

			<p class="error___p"></p>

			<div class="submit">
				<button type="button" class="submit_btn reg___button to___process">зарегистрироваться</button>
			</div>
			
		</form>
		
	</div>
</div>






<div class="modal modal___block" id="buy_1_click_modal">

	<div class="modal_title">Купить в 1 клик</div>

	<form class="form">
	
		<div class="line">
			<div class="field">
				<input type="text" name="name" class="input" placeholder="Ваше имя">
			</div>
		</div>

		<div class="line">
			<div class="field">
				<input type="text" name="email" class="input" placeholder="Ваш E-mail">
			</div>
		</div>
		
		<div class="line">
			<div class="field">
				<div class="phone_code"><?=project\site::PHONE_PREFIX?></div>
				<input type="tel" name="phone" class="input" placeholder="Ваш телефон">
			</div>
		</div>

		<p class="error___p"></p>

		<div class="submit">
			<button type="button" class="submit_btn buy_1_click___button to___process">Отправить</button>
		</div>

		<div class="agree">
			<input type="checkbox" name="agree" id="agree_check2" value="1">
			<label for="agree_check2">Я согласен с условиями <a href="/oferta/" target="_blank">оферты</a> и <a href="/politika-konfidentsialnosti/" target="_blank">политики конфиденциальности</a>.</label>
		</div>
		
	</form>
</div>



<div class="modal success" id="successWindow">
	<img src="<?=SITE_TEMPLATE_PATH?>/images/ic_success_modal.png">
	<div class="title"></div>
</div>


<div class="modal success" id="messageWindow">
	<div class="title"></div>
</div>


<div class="modal success" id="errorWindow">
	<div class="title"></div>
</div>


<div class="modal success" id="modalNewMarkup1">
	<img src="<?=SITE_TEMPLATE_PATH?>/images/ic_success_modal.png">
	<div class="title">Вы успешно авторизованы</div>
</div>


<div class="modal success" id="modalNewMarkup2">
	<img src="<?=SITE_TEMPLATE_PATH?>/images/ic_success_modal.png">
	<div class="title">Спасибо за заказ</div>
</div>


<div class="modal success" id="iSubscribeNeedAuth">
	<div class="title">Для управления подписками необходимо <a onclick="$('.login___link').trigger('click')" style="text-decoration:underline; cursor:pointer">авторизоваться</a></div>
</div>


<div class="modal modal___block" id="cart_success">
	<img src="<?=SITE_TEMPLATE_PATH?>/images/ic_success_modal2.png">
	<div class="product"></div>
	добавлено в корзину
	<div class="links">
		<a onclick="$.fancybox.close();" style="cursor:pointer" class="close">В магазин</a>
		<a href="/basket/">В корзину</a>
	</div>
</div>


