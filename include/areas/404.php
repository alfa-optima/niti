<? include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

$url = $_SERVER['REQUEST_URI'];

$to_redirect = false;

$ar = array('.html', '.htm', '.php');
$get_string = explode('?', $url)[1];
$pureURL = explode('?', $url)[0];
$is_page = false;
foreach( $ar as $item ){  if( substr_count($pureURL, $item) ){    $is_page = true;    }  }
if(  !$is_page  &&  $pureURL[strlen($pureURL)-1] != '/'  ){
	$pureURL .= '/';
	if( strlen($get_string) > 0 ){
		$url = $pureURL.'?'.$get_string;
	} else {
		$url = $pureURL;
	}
	$to_redirect = true;
}

if( substr_count($url, '//') ){
	while( substr_count($url, '//') ){    $url = str_replace('//', '/', $url);    }
	$to_redirect = true;
}

if( $to_redirect ){
	LocalRedirect($url , false, '301 Moved permanently');
}


CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");
$APPLICATION->SetTitle("404 Not Found"); ?>

<h1>Страница не найдена</h1>