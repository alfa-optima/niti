<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); 

\Bitrix\Main\Loader::includeModule('aoptima.tools');
use AOptima\Tools as tools;

$APPLICATION->SetPageProperty("title", 'Личные данные');
$APPLICATION->AddChainItem('Личные данные', pureURL());

// Если авторизован
if( $USER->IsAuthorized() ){ 

	// Инфо о пользователе
	$arUser = tools\user::info($USER->GetID());
	
	$all_countries = all_countries();

	// top_menu
	$APPLICATION->IncludeComponent(
		"bitrix:menu", "personal_menu", 
		array(
			"ROOT_MENU_TYPE" => "personal_menu",
			"MENU_CACHE_TYPE" => "N",
			"MENU_CACHE_TIME" => "3600",
			"MENU_CACHE_USE_GROUPS" => "N",
			"CACHE_SELECTED_ITEMS" => "N",
			"MENU_CACHE_GET_VARS" => array(
			),
			"MAX_LEVEL" => "1",
			"CHILD_MENU_TYPE" => "left",
			"USE_EXT" => "N",
			"DELAY" => "N",
			"ALLOW_MULTI_SELECT" => "N",
			"COMPONENT_TEMPLATE" => "footer_menu",
		)
	); ?>

	<div class="lk_personal">
	
		<iframe id="upload___Frame" name="upload___Frame" style="display: none"></iframe>
		<form style="visibility:hidden; height:0" class="persUpload___form" action="/ajax/uploadPersPhoto.php" target="upload___Frame" method="post" enctype="multipart/form-data">
			<input type="file" name="persPhotoInput" style="visibility:hidden; height:0">
		</form>

		<aside class="right">
			<div class="avatar persPhotoBlock">
				<? // pers_photo_block
				$APPLICATION->IncludeComponent(
					"my:pers_photo_block", "",
					array('arUser' => $arUser)
				); ?>
			</div>
		</aside>


		<section class="content left">
		
		
			<form class="form">
			
				<div class="lk_title">Персональная информация</div>

				<div class="lines">
				
					<div class="line">
						<div class="name">Имя</div>

						<div class="field">
							<input type="text" name="NAME" placeholder="Ваше имя" value="<?=$arUser['NAME']?>" class="input">
						</div>
					</div>
					
					<div class="line">
						<div class="name">Фамилия</div>

						<div class="field">
							<input type="text" name="LAST_NAME" placeholder="Ваша фамилия" value="<?=$arUser['LAST_NAME']?>" class="input">
						</div>
					</div>

					<div class="line">
						<div class="name">Email</div>

						<div class="field">
							<input type="email" name="EMAIL" placeholder="Ваш Email" value="<?=$arUser['EMAIL']?>" class="input">
						</div>
					</div>

					<div class="line">
						<div class="name">Номер телефона</div>

						<div class="field">
							<div class="phone_code">+7</div>
							<input type="tel" name="PERSONAL_PHONE" placeholder="Ваш телефон" value="<?=$arUser['PERSONAL_PHONE']?>" class="input">
						</div>
					</div>
					
				</div>

				<p class="error___p"></p>

				<div class="submit">
					<button type="button" class="submit_btn personal_edit_button_1 to___process">Сохранить</button>
				</div>
			</form>

			
			<form action="" class="form">
				<div class="lk_title">Адрес доставки</div>

				<div class="lines">

					<? // Блок выбора страны
					if( count($all_countries) > 0 ){ ?>
					
						<div class="line">
							<div class="name">Страна</div>

							<div class="field">
								<select name="UF_COUNTRY">
									<option value="error"></option>
									<? foreach ( $all_countries as $country ){ ?>
										<option <? if( strtolower(trim($country['NAME'])) == strtolower(trim($arUser['UF_COUNTRY'])) ){ ?>selected<? } ?> value="<?=$country['NAME']?>"><?=$country['NAME']?></option>
									<? } ?>
								</select>
							</div>
						</div>
						
					<? } ?>

					<div class="line">
						<div class="name">Город</div>
						<div class="field">
							<input type="text" name="PERSONAL_CITY" value="<?=$arUser['PERSONAL_CITY']?>" class="input">
						</div>
					</div>

					<div class="line">
						<div class="name">Индекс</div>

						<div class="field">
							<input type="text" name="PERSONAL_ZIP" value="<?=$arUser['PERSONAL_ZIP']?>" class="input">
						</div>
					</div>
					
				</div>


				<div class="lines">
				
					<div class="line">
						<div class="name">Улица</div>

						<div class="field">
							<input type="text" name="PERSONAL_STREET" value="<?=$arUser['PERSONAL_STREET']?>" class="input">
						</div>
					</div>

					<div class="line">
						<div class="lines">
						
							<div class="line">
								<div class="name">Дом</div>

								<div class="field">
									<input type="text" name="UF_HOUSE" value="<?=$arUser['UF_HOUSE']?>" class="input">
								</div>
							</div>

							<div class="line">
								<div class="name">Корпус</div>

								<div class="field">
									<input type="text" name="UF_KORPUS" value="<?=$arUser['UF_KORPUS']?>" class="input">
								</div>
							</div>
							
						</div>
					</div>

					<div class="line">
						<div class="lines">
						
							<div class="line">
								<div class="name">Подъезд</div>

								<div class="field">
									<input type="text" name="UF_PODYEZD" value="<?=$arUser['UF_PODYEZD']?>" class="input">
								</div>
							</div>

							<div class="line">
								<div class="name">Квартира</div>

								<div class="field">
									<input type="text" name="UF_KVARTIRA" value="<?=$arUser['UF_KVARTIRA']?>" class="input">
								</div>
							</div>
							
						</div>
					</div>
					
				</div>

				<p class="error___p"></p>

				<div class="submit">
					<button type="button" class="submit_btn personal_edit_button_2 to___process">Сохранить</button>
				</div>
			</form>


			<!--
			<form action="" class="form">
				<div class="lk_title">Социальные сети</div>

				<div class="socials">
					<a href="#" class="active">
						<div class="icon"><div class="vk_icon"></div></div>
						<div class="name">Вконтакте</div>
					</a>

					<a href="#">
						<div class="icon"><div class="fb_icon"></div></div>
						<div class="name">Facebook</div>
					</a>

					<a href="#">
						<div class="icon"><div class="twitter_icon"></div></div>
						<div class="name">Twitter</div>
					</a>

					<a href="#">
						<div class="icon"><div class="insta_icon"></div></div>
						<div class="name">Instagram</div>
					</a>

					<a href="#">
						<div class="icon"><div class="google_icon"></div></div>
						<div class="name">Google +</div>
					</a>
				</div>
			</form>
			-->


			<form class="form">
				<div class="lk_title">Смена пароля</div>

				<div class="lines">

					<div class="line">
						<div class="name">Новый пароль</div>

						<div class="field">
							<input type="password" name="PASSWORD" class="input">
						</div>
					</div>

					<div class="line">
						<div class="name">Повтор нового пароля</div>

						<div class="field">
							<input type="password" name="CONFIRM_PASSWORD" class="input">
						</div>
					</div>
					
				</div>

				<p class="error___p"></p>

				<div class="submit">
					<button type="button" class="submit_btn personal_edit_button_3 to___process">Сменить</button>
				</div>
				
			</form>
			
		</section>
		
		<div class="clear"></div>

	</div>

	
<? // НЕ авторизован
} else { 


	// personal_auth_block
	include $_SERVER["DOCUMENT_ROOT"].'/include/areas/personal_auth_block.php';

	
} ?>