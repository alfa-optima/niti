<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Помощь");
?><div class="text_page" style="width:100%">
  
                    <script>

                        function compareOrder(a, b) {
                            return a.order - b.order;
                        }

                        function checkAddress() {

                            cityFunc.reset();
                            cityFunc.hide();


                            var url = 'https://geocode-maps.yandex.ru/1.x/',
                                geocodeStr = $('input[name^="city_"]').val();

                            //console.log(geocodeStr.length)

                            if (geocodeStr.length == 0) return false;


                            $.ajax({
                                dataType: "jsonp",
                                url: url,
                                data: {
                                    format: 'json',
                                    geocode: geocodeStr,
                                    kind: 'locality',
                                    results: 5,
                                    callback: '?'
                                },
                                success: function (data) {

                                    //console.log(data.response.GeoObjectCollection);
                                    var list = data.response.GeoObjectCollection.featureMember;
                                    $('.suggest').html(data.response.GeoObjectCollection.metaDataProperty.GeocoderResponseMetaData.suggest);
                                    $('.city_list_order > ul').html('');

                                    for (i in list) {

                                        var t = {};
                                        t.kind = list[i].GeoObject.metaDataProperty.GeocoderMetaData.kind;
                                        t.text = list[i].GeoObject.metaDataProperty.GeocoderMetaData.text;
                                        t.countryName = list[i].GeoObject.metaDataProperty.GeocoderMetaData.AddressDetails.Country.CountryName;
                                        t.countryCode = list[i].GeoObject.metaDataProperty.GeocoderMetaData.AddressDetails.Country.CountryNameCode;
                                        t.nameCity = list[i].GeoObject.name;


                                        if (t.kind != 'locality') continue;

                                        $('.city_list_order > ul').append('<li data-name-city="' + t.nameCity + '" data-country="' + t.countryCode + '">\
                        <div class="country" data-code="' + t.countryCode + '" data-name="' + t.countryName + '"></div>\
                        <div class="text">' + t.text + '</div>\
                    </li>');
                                        //console.log(t);

                                    }

                                    cityFunc.show();


                                }
                            });

                        }

                        $(document).on({
                            keyup: function () {
                                checkAddress();
                            }
                        }, 'input[name^="city_"]');

                        $(document).on({
                            change: function () {
                                var city = $("select option:selected").val(),
                                    text = $("select option:selected").text();

                                console.log(city);

                                $.post(conf.root + 'script.php', {get_info_store: city}).done(
                                    function (data) {

                                        //console.log(data);
                                        console.log(data.info.dostavka);
                                        $('.dostavka').remove();

                                        if (data.info.dostavka == null) {
                                            $('.text_page').append('<div class="dostavka">' + text + ': информация о доставке отсутствует.</div>');

                                        } else {
                                            $('.text_page').append(data.info.dostavka);
                                        }


                                    }).fail(function (a, b, c) {
                                        console.log(a, b, c);
                                    }).always(function (a, b, c) {
                                        //console.log(a,b,c);
                                    });

                            }
                        }, '#dostavka_city');

                        $(document).on({
                            click: function () {

                                var cityName = $(this).data('name-city'),
                                    cityFullName = $(this).find('.text').text(),
                                    country = $(this).data('country');

                                $('.city_desc').text(cityFullName);
                                $('input[name^="city_"]').val(cityName);

                                cityFunc.hide();

                                checkDelivery(cityName, country);

                            }
                        }, '.city_list_order > ul > li');


                        function checkDelivery(cityName, countryCode) {

                            console.log({
                                get_info_store_text: cityName,
                                country_code: countryCode
                                //type: 'all'
                            });

                            if(cityName != 'Москва'){
                                $('.shema').hide()
                            } else{
                                $('.shema').show()
                            }

                            $.post(conf.root + 'script.php', {
                                get_info_store_text: cityName,
                                country_code: countryCode,
                                //type: 'all'
                            }).done(function (data) {

                                console.log(data);

                                //data.info.payment

                                var delivery_options = [];
                                for (i in data.info.delivery.options) {
                                    delivery_options.push(data.info.delivery.options[i]);
                                }
                                delivery_options.sort(compareOrder);


                                //$('table.delivery').before('<div class=free_delivery__h1>Бесплатная доставка от ' + data.info.delivery.free_delivery.formatMoney() + ' ' + GLOBAL_SETTINGS.currency_name + '</div>');

                                $('table.delivery').append('<tr>\
                                       <th>Тип</th>\
                                       <th>Цена, ' + GLOBAL_SETTINGS.currency_name + '</th>\
                                       <th>Способ оплаты</th>\
                                       <!--<th>Бесплатная доставка, ' + GLOBAL_SETTINGS.currency_name + '</th>-->\
                                    </tr>');


                                for (i in delivery_options) {

                                    var el = delivery_options[i],
                                        id = el.id,
                                        info_delivery = '',
                                        payment_method = '';

                                    console.log(el.id, el);

                                    for (i_p in el.payment_options.list) {

                                        var info_payment = '';
                                        if (data.info.payment[el.payment_options.list[i_p]].info != undefined) {
                                            info_payment = '<div class="info_icon">?<div class="text_tooltip top__left">' + data.info.payment[el.payment_options.list[i_p]].info + '</div></div>';
                                        }

                                        payment_method += '<li>- ' + data.info.payment[el.payment_options.list[i_p]].name + info_payment + '</li>';

                                        //console.log(el.payment_options.list[i_p], $.inArray(el.payment_options.list[i_p], [2,3]));

                                        if ($.inArray(el.payment_options.list[i_p], [2, 3]) >= 0) {
                                            $('.pa_info__dostavka_page').show();
                                        }

                                    }
                                    if (!(payment_method == '')) payment_method = '<ul class=payment_method>' + payment_method + '</ul>';

                                    if (el.payment_options.fixed === false) el.price = 'Не фиксированная';
                                    else el.price = el.price.formatMoney();

                                    //var text_free_delivery = 'от ' + data.info.delivery.free_delivery + '';
                                    //if(el.id == 1) text_free_delivery = '-';

                                    //<!--<td>' + text_free_delivery + '</td>-->\


                                    $('table.delivery').append('<tr>\
                                        <td><div class=h1_delivery>' + el.name + '</div>\
                                        <div class=full_description>' + el.full_decription + '</div>\
                                        </td>\
                                        <td><div class=price_delivery>' + el.price + '</div></td>\
                                        <td>' + payment_method + '</td>\
                                    </tr>');

                                }


                            }).fail(function (a, b, c) {
                                console.log(a, b, c);
                            }).always(function (a, b, c) {
                                //console.log(a,b,c);
                            });

                        }

                        var cityFunc = {
                            show: function () {
                                $('.city_list_order').show();
                            },
                            hide: function () {
                                $('.city_list_order').hide();
                                $('.suggest').html('');
                            },
                            reset: function () {
                                $('.suggest').html('');
                                $('.city_desc').text('');
                                $('input[name="id_store"]').val(1);
                                $('table.delivery').html('');
                                $('.free_delivery__h1').remove();
                                $('.pa_info__dostavka_page').hide();

                            }
                        };


                        $(function () {

                            
                        });

                    </script>
                    <div class="shema">

                        <div class="h">Схема проезда в главный офис компании Нити Нити</div>

                        <iframe src="https://api-maps.yandex.ru/frame/v1/-/CZHJYY9M" width="100%" height="400" frameborder="0"></iframe>

                        <p>Станция метро «Дмитровская». Один выход в город. Из стеклянных дверей направо. По лестнице
                            налево. Идем прямо до первого поворота налево примерно 250 метров. Поворачиваем налево и по
                            дороге идем до железнодорожных путей. Аккуратно по светофору переходим железнодорожные пути.
                            И оказываемся около высокого здания «Молодая гвардия».</p>
                        <p>Доходим до конца здания и
                            поворачиваем налево. Заходим в центральный вход здания строения №2. Ориентир Фитнес клуб
                            «Манхеттон». На охране при предъявлении паспорта выписываем пропуск. Вам будет удобно найти
                            нас пройдя через правый турникет. На лифте поднимаетесь на 2-й этаж. По коридору налево,
                            налево и у Вас перед глазами большая оранжевая дверь. Мы рады увидеть Вас в нашем офисе!</p>


                            <p>Если у Вас возникли вопросы, или Вы потерялись – звоните по телефону 8 800 555 32 03. Мы
                            поможем Вам сориентироваться!</p>

                        <p></p>

                    </div>
                                </div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>